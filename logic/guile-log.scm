(define-module (logic guile-log)
  #:use-module (compat racket misc)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log macros)
  #:use-module (logic guile-log interleave)
  #:use-module (logic guile-log tools)
  #:use-module (logic guile-log run)
  #:use-module (logic guile-log prompts)
  #:use-module (logic guile-log undovar)
  #:re-export (gp-cons! gp-lookup gp-var! gp->scm gp-unify! gp-unify-raw! gp-m-unify!)
  #:export (umatch))

(define-syntax umatch (syntax-rules () ((_ . l) (**um** . l))))

(define log-module
  (resolve-module
   '(logic guile-log)))

(re-export-all (logic guile-log macros))
(re-export-all (logic guile-log interleave))
(re-export-all (logic guile-log tools))
(re-export-all (logic guile-log run))
(re-export-all (logic guile-log prompts))
(re-export-all (logic guile-log undovar))
