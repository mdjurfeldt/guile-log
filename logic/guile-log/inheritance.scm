(define-module (logic guile-log inheritance)
  #:use-module (logic guile-log dynlist)
  #:use-module (ice-9 pretty-print)
  #:use-module (logic guile-log vset)
  #:use-module (logic guile-log umatch)
  #:use-module (ice-9 match)
  #:export 
  (
   bits-to-is get-high-bit get-rest-bits

   make-set-theory
   *current-set-theory*

   new-set a->b a->b-o set->f
   print-theory print-set
   compile-sup-sub
   compile-set-representation
   mktree 
   find-matching-sets
   fold-matching-sets
   get-translation-data
   order-the-set
   inh-comb
   mk-get-set
   mk-get-set!
   set<
   assoc=>tree

   lookup
   reverse-lookup
   ))

;; Syntax helpers
(define-syntax-rule (aif (v) p x y)
  (let ((v p)) (if v x y)))

(define-syntax alet* 
  (syntax-rules ()
    ((_ ((a b) . q) code e)
     (aif (a) b (alet* q code e) e))
    ((_ () code e) code)))

#|
  Bit twiggling framework
|#

(define-inlinable (get-high-bit x) 
  (ash 1 (- (integer-length x) 1)))

(define-inlinable (bits-to-is set)
  (let lp ((ih set))
      (if (= ih 0)
	  '()
	  (let ((high (get-high-bit ih)))
	    (cons high (lp (logand ih (lognot high))))))))

(define-inlinable (get-rest-bits x)
  (logand x (lognot (get-high-bit x))))


#|
 - The set theory infrastructure -

This is prepared to make it functional, but currently we mutate
|#
(define make-set-theory
  (case-lambda
    (() (make-set-theory #f))
    ((super)
     (vector 0 
	     1
             (vosetx-union (fluid-ref *current-stack*))
	     (make-hash-table)
	     (make-hash-table)
	     (make-hash-table)
	     '()
	     super
	     (make-hash-table)
	     (make-hash-table)
             (make-hash-table)))))

(define-inlinable (get-set     x)     (vector-ref  x 0))
(define-inlinable (get-new     x)     (vector-ref  x 1))
(define-inlinable (get-set->i  x)     (vector-ref  x 2))
(define-inlinable (get-i->j    x)     (vector-ref  x 10))
(define-inlinable (get-i->set  x)     (vector-ref  x 3))
(define-inlinable (get-j->i    x)     (vector-ref  x 3))
(define-inlinable (get-i->subs x)     (vector-ref  x 4))
(define-inlinable (get-i->sups x)     (vector-ref  x 5))
(define-inlinable (get-parent  x)     (vector-ref  x 7))
(define-inlinable (get-i->i    x)     (vector-ref  x 8))
(define-inlinable (get-i->f    x)     (vector-ref  x 9))
(define-inlinable (set-set     x v)   (vector-set! x 0 v))
(define-inlinable (set-new     x v)   (vector-set! x 1 v))
(define-inlinable (set-set->i  x v)   (vector-set! x 2 v))
(define-inlinable (set-i->set  x v)   (vector-set! x 3 v))
(define-inlinable (set-i->subs x v)   (vector-set! x 4 v))

(define-inlinable (put-set->i x k v)
  (vosetx-union (fluid-ref *current-stack*) (mk-kvx k v)
                (get-set->i x)))

(define-inlinable (put-i->set x k v)
  (hash-set! (get-i->set x) k v))
(define-inlinable (put-i->subs x k v)
  (hash-set! (get-i->subs x) k v))
(define-inlinable (put-i->sups x k v)
  (hash-set! (get-i->sups x) k v))

(define-inlinable (get-i    ->i i)
  (hash-ref  ->i i #f))

(define-inlinable (get-si ->i i)
  (let ((r (vsetx-member i ->i)))
    (if r
        (get-v r)
        r)))

(define-inlinable (get-0    ->i i)   (hash-ref  ->i i 0))
(define-inlinable (get-list ->i i)   (hash-ref  ->i i '()))
(define-inlinable (update   ->i i v) (begin (hash-set! ->i i v) ->i))

(define-syntax-rule (update-set-theory th . l)
  (let ((ll (list . l)))
    (aif (r) (member #:->i ll)
         (set-set->i th (cadr r))
         #f)
    (aif (r) (member #:set ll)
	 (set-set th (cadr r))
	 #f)
    (aif (r) (member #:new ll)
	 (set-new th (cadr r))
	 #f)
    th))

#|
  Enable a current-set-theory interface
|#

(define *current-set-theory* (make-fluid (make-set-theory)))
(define (get-current-set-theory) (fluid-ref *current-set-theory*))


(define-syntax-rule (mk-get-set set)
  (if set
      (lambda (s)
	(let* ((i (get-si (get-set->i (fluid-ref *current-set-theory*)) s)))
	  (set! set (logior set i))
	  i))
      (lambda (s) 0)))

(define-syntax-rule (mk-get-set! set)
  (if set
      (lambda (s)
	(let* ((i (get-si (get-set->i (fluid-ref *current-set-theory*)) s)))
	  (set! set (logior set i))
	  i))
      (lambda (s) 0)))

#|
Basic construction and removal of set and set graph relationship,
|#

(define print-set
  (case-lambda 
    (() (print-set (fluid-ref *current-set-theory*)))
    ((theory)
     (define (tr x)
       (let lp ((x x) (th theory))
	 (aif (it) (get-parent th)
	      (lp (get-i (get-j->i th) x) it)
	      (get-i (get-i->set th) x))))
     (pk
      (let lp ((l (bits-to-is (- (get-new theory) 1))))
	(if (pair? l)
	    (cons (tr (car l)) (lp (cdr l)))
	    '()))))))

(define print-theory
  (case-lambda 
    (() (print-theory (fluid-ref *current-set-theory*)))
    ((theory)
     (define out '())
     (define (tr x)
       (let lp ((x x) (th theory))
	 (aif (it) (get-parent th)
	    (lp (get-i (get-j->i th) x) it)
	    (get-i (get-i->set th) x))))

     (hash-for-each
      (lambda (i s)
	(define target '())
	(set! out
	  (cons
	   (list (tr i) '->
		 (let lp ((l (bits-to-is (get-i (get-i->subs theory) i))))
		   (if (pair? l)
		       (cons
			(tr (car l))
			(lp (cdr l)))
		       '())))
	   out)))
      (get-i->set theory))
     (pretty-print out))))

(define set->f 
  (case-lambda
    ((set f) (set->f (fluid-ref *current-set-theory*) set f))
    ((theory set f)
     (update-set-theory theory
       #:i->f (update (get-i->f theory) 
		      (get-si (get-set->i theory) set) 
		      f)))))

(define new-set 
  (case-lambda
    ((set)
     (fluid-set! *current-set-theory*
		 (new-set (fluid-ref *current-set-theory*) set)))
    ((theory set)
     (let ((j (get-new theory)))
       (update-set-theory theory
	 #:set (logior (get-set theory) j)
         #:new (* 2 j)
	 #:->i   (put-set->i  theory set j)
	 #:->set (put-i->set  theory j   set)
	 #:subs  (put-i->subs theory j   j)
	 #:sups  (put-i->sups theory j   j))))))

(define a->b
  (case-lambda*
    ((set-a set-b)
     (fluid-set! *current-set-theory*
		 (a->b (fluid-ref *current-set-theory*) set-a set-b)))

    ((theory set-a set-b)
     (let* ((set->i (get-set->i theory))
	    (i->i   (get-i->i   theory))
	    (a      (get-si set->i set-a))
	    (b      (get-si set->i set-b)))
       (update-set-theory theory
	  #:i->i (update i->i a (cons b (get-i i->i a))))))))

(define a->b-o 
  (case-lambda*
    ((set-a set-b)
     (fluid-set! *current-set-theory*
		 (a->b (fluid-ref *current-set-theory*) set-a set-b)))

    ((theory set-a set-b)
     (let* ((->si    (get-set->i  theory))
	    (->subs  (get-i->subs theory))
	    (->sups  (get-i->sups theory)))
       (alet* ((a  (get-si ->si    set-a))
	       (b  (get-si ->si    set-b))
	       (ah (get-i ->subs a))
	       (bh (get-i ->subs b))
	       (ap (get-i ->sups a)))
	 (begin
	   (when (not (= (logand bh a) 0))
	     (error (format #f "(a->b ~a ~a) created a circle" set-a set-b)))
	   (let lp ((l (bits-to-is ap)) (->subs ->subs))
	     (if (pair? l)
		 (let ((c (car l)))
		   (aif (ch) (get-i ->subs c)
			(lp (cdr l) (update ->subs c (logior bh ch)))
			(error "a->b set is not locatable in hash")))
		 (let lp ((l (bits-to-is bh)) (->sups ->sups))
		   (if (pair? l)
		       (let ((c (car l)))
			 (aif (cp) (get-i ->sups c)
			      (lp (cdr l) (update ->sups c (logior ap cp)))
			      (error "a->b set is not locatable in hash")))
		       (update-set-theory theory
					  #:subs ->subs
					  #:sups ->sups))))))
	 (error "a->b set is not locatable in hash"))))))
     
#|
Each function has it's own subspace of to how do we know that a set s is 
if S->S', then we just move on with S'
S->IH IH n World, so this works but is really less efficient, but we would of
cause cache this set for later faster lookup
|#

#|
This function introduce a new coding which order the sets and also introduce
a natural generational mapping to help in constructing a match tree.
|#

(define compile-sup-sub
  (case-lambda 
    (()
     (fluid-set! *current-set-theory*
		 (compile-sup-sub (fluid-ref *current-set-theory*))))
    ((theory)
     (let ((i->subs (get-i->subs theory))
	   (i->sups (get-i->sups theory))
	   (i->i    (get-i->i    theory)))

       (define (find-leafs)
	 (let lp ((l (bits-to-is (get-set theory))) (leafs 0))
	   (if (pair? l)
	       (if (null? (get-list i->i (car l)))
		   (lp (cdr l) (logior leafs (car l)))
		   (lp (cdr l) leafs))		 
	       leafs)))

       (define (new-generation old)
	 (let lp ((l (bits-to-is (get-set theory))) (new 0))
	   (if (pair? l)
	       (if (= (car l) (logand (car l) old))
		   (lp (cdr l) new)
		   (if (let lp2 ((ll (get-list i->i (car l))))
			 (if (pair? ll)
			     (if (= (car ll) (logand (car ll) old))
				 (lp2 (cdr ll))
				 #f)
			     #t))
		       (lp (cdr l) (logior new (car l)))
		       (lp (cdr l) new)))
	       new)))
       
       (let ((leafs (find-leafs)))
	 (define (init ->)
	   (let lp ((l (bits-to-is leafs)) (-> ->))
	     (if (pair? l)
		 (lp (cdr l) (update -> (car l) (car l)))
		 ->)))
	 (let lp ((new leafs) (subs leafs) 
		  (i->subs (init i->subs)) (i->sups (init i->sups)))
	   (if (not (= new 0))
	       (let lp2 ((l (bits-to-is new)) 
			 (i->subs i->subs) (i->sups i->sups))
		 (if (pair? l)
		     (let lp3 ((ll      (get-list i->i (car l))) 
			       (i->subs i->subs) 
			       (i->sups i->sups))
		       (if (pair? ll)
			   (lp3 (cdr ll)
				(update i->subs (car l)
					(logior (get-i i->subs (car l))
						(get-i i->subs (car ll))))
				(update i->sups (car ll)
					(logior (get-i i->sups (car ll))
						(car l))))

			   (lp2 (cdr l) i->subs i->sups)))
		     (let ((subs (logior new subs)))
		       (lp (new-generation subs) subs i->subs i->sups))))

	       (update-set-theory theory
		  #:i->subs i->subs
		  #:i->sups i->sups))))))))

(define compile-set-representation 
  (case-lambda
    (()
     (compile-set-representation (fluid-ref *current-set-theory*) ))

    ((setbits-or-theory)
     (if (number? setbits-or-theory)
	 (compile-set-representation (fluid-ref *current-set-theory*) 
				     setbits-or-theory)
	 (compile-set-representation setbits-or-theory
				     (- (get-new setbits-or-theory)
					1))))
     
    ((theory setbits)
     (if (= setbits 0)
	 #f
	 (let ((deps (get-i->subs theory))
	       (sups (get-i->sups theory)))
	   (define (find-all-leafs)
	     (let lp ((ss (bits-to-is setbits)) (leafs 0))
	       (if (pair? ss)
		   (let ((i (car ss)))
		     (aif (ih) (get-i deps i)
			  (if (= i (logand setbits ih))
			      (lp (cdr ss) (logior leafs i))
			      (lp (cdr ss) leafs))
             (error "compile-set-representatoin has nonmatched bit")))
		   leafs)))

           (define (find-all-newcombers downbits new-downbits)
             (let lp1 ((ss (bits-to-is new-downbits)) (news 0))
               (if (pair? ss)
                   (let ((i (car ss)))
                     (aif (hi) (get-i sups i)
                          (let lp2 ((js   (bits-to-is (logand setbits hi)))
                                    (news news))
                            (if (pair? js)
                                (let ((j (car js)))	   
                                  (if (and (not (= j i))
                                           (= (logand j downbits) 0))
                                      (aif (jh) (get-i deps j)
                                           (if (= (logand jh setbits)
                                                  (logand (logand jh setbits) 
                                                          (logior j downbits)))
                                               (lp2 (cdr js) (logior j news))
                                               (lp2 (cdr js) news))
                                         (error "missing set registratoin in"))
                                      (lp2 (cdr js) news)))
                                (lp1 (cdr ss) news)))
                          (error "missing set registratoin in")))
                   news)))
  
       (define new-theory (make-set-theory theory))
       (define (register-new newbits)
	 (let ((l (bits-to-is newbits))
	       (s (get-set  new-theory)))
	   (let lp ((ss   l)
		    (m    (get-new  new-theory))
		    (i->j (get-i->j new-theory)) 
		    (j->i (get-j->i new-theory)))
	     (if (pair? ss)
		 (let ((i (car ss)))
		   (let ((j m))
		     (lp (cdr ss)
			 (* 2 m)
			 (update i->j i j)
			 (update j->i j i))))
		   (update-set-theory new-theory
		      #:set (logior s newbits)
		      #:new m
		      #:i->j i->j
		      #:j->i j->i)))))
       
       (let ((leafs (find-all-leafs)))
	 (let lp ((downbits leafs) (new-downbits leafs))
	   (if (not (= new-downbits 0))
	       (begin
		 (set! new-theory (register-new new-downbits))
		 (let ((newcoms (find-all-newcombers downbits new-downbits)))
		   (lp (logior newcoms downbits) newcoms))))))
       
       (let ((s       (get-set     new-theory))
	     (i->j    (get-i->j    new-theory))
	     (j->subs (get-i->subs new-theory))
	     (j->sups (get-i->sups new-theory))
	     (p->subs (get-i->subs theory))
	     (p->sups (get-i->sups theory)))
	 (let lp ((l (bits-to-is s)) (j->subs j->subs) (j->sups j->sups))
	   (if (pair? l)
	       (let lp2 ((ll (bits-to-is (logand s (get-i p->subs (car l)))))
			 (sub 0))
		 (if (pair? ll)
		     (lp2 (cdr ll) (logior (get-i i->j (car ll)) sub))
		     (let lp3 ((ll 
				(bits-to-is (logand s (get-i p->sups (car l)))))
			       (sup 0))
		       (if (pair? ll)
			   (lp3 (cdr ll) (logior (get-i i->j (car ll)) sup))
			   (lp (cdr l) 
			       (update j->subs (get-i i->j (car l)) sub)
			       (update j->sups (get-i i->j (car l)) sup))))))
	       (set! new-theory
		 (update-set-theory new-theory
			  #:subs j->subs
			  #:sups j->sups)))))
		     
       new-theory)))))
  
#|
    Balanced binary tree compilation

    You take a set i, finds it's subs sub and if sub | m, then dive

    each if we have a set s and would like to lookup the matches
  
    
|#	 

(define (inh-comb i->f)
  (case-lambda
    ((x)
     (values x 
	     (vector x (get-i i->f x))))
    ((x y)
     (logior x y))
    (() 0)))
	
(define (assoc=>tree theory a)
  (let ((p      (get-parent theory))
	(set->i (get-set->i theory))
	(i->j   (get-i->j   theory)))
    (let lp ((a a) (i->f (make-hash-table)))
      (if (pair? a)
	  (let ((key (get-i i->j (caar a))))
	    (lp (cdr a) (update i->f key
				(logior (cdar a) (get-0 i->f key)))))
	  (mktree theory i->f)))))

(define mktree 
  (case-lambda* 
    ((i->f #:key (setbits (- (get-new (fluid-ref *current-set-theory*)) 1)))
     (mktree (fluid-ref *current-set-theory*) i->f #:setbits setbits))
    ((theory i->f #:key (setbits (- (get-new theory) 1)))
     (define (clusterize setbits)
       (let ((i->subs  (get-i->subs theory)))
	 (let lp ((ss (bits-to-is setbits)) (done 0))
	   (if (pair? ss)
	       (let* ((i  (car ss)))
		 (if (= (logand done i) 0)
		     (let ((ih (logand setbits (get-i i->subs i))))
		       (cons ih (lp (cdr ss) (logior done ih))))
		     (lp (cdr ss) done)))
	       '()))))

     (define (mktree1 setbits)
       (if (= setbits 0)
	   '()
	   (let ((cls (clusterize setbits)))
	     (map 
	      (lambda (cluster-bits)
		(let ((a (get-high-bit  cluster-bits))
		      (t (get-rest-bits cluster-bits)))
		  (cons a (mktree1 t))))
	      cls))))

     (define (linearize tree)
       (let lp ((r tree))
	 (match r
	   ((x . y) (append (linearize x) (linearize y)))
	   (()      '())
	   (x       (list x)))))

     (define (p x)
       (map (lambda (x) (reverse-lookup theory x)) x)
       x)

     (define setlist (linearize (mktree1 setbits)))

     (define comb (inh-comb i->f))
     (define tree
       (let lp ((l (reverse setlist)) (tree (make-dynlist)))
	 (if (pair? l)
	     (let ((i (car l)))
	       (lp (cdr l)
		   (dynlist-add tree i comb)))
	     tree)))
     tree)))
     
#|
Algorithm to lookup the matching sets iiis, preliminary this will be called
from c-land in the indexer.
|#

(define reverse-lookup
  (case-lambda
    ((set)
     (reverse-lookup (fluid-ref *current-set-theory*) set))
    ((theory set)
     (define (tr x)
       (let lp ((x x) (th theory))
	 (aif (it) (get-parent th)
	      (lp (get-i (get-j->i th) x) it)
	      (get-i (get-i->set th) x))))
     (tr set))))

(define lookup
  (case-lambda
    ((set)
     (lookup (fluid-ref *current-set-theory*) set))
    ((theory set)
     (aif (it) (get-parent theory)
	  (get-i (get-i->j theory) (lookup it set))
	  (get-si (get-set->i theory) set)))))


(define (find-matching-sets theory set tree)
  (let ((ih (get-i (get-i->subs theory) (lookup theory set))))
    (let lp ((l (bits-to-is (fold-dynlist-lr 
			     (lambda (x seed)
			       (logior (vector-ref x 0) seed))
			     tree 0  
			     (lambda (x)
			       (not (= (logand x ih) 0)))))))
      (if (pair? l)
	  (cons (reverse-lookup theory (car l)) (lp (cdr l)))
	  '()))))

(define fold-matching-sets
  (case-lambda
    ((f seed set tree)
     (fold-matching-sets f seed (fluid-ref *current-set-theory*) set tree))
    ((f seed theory set tree)
     (let ((ih (get-i (get-i->subs theory) (lookup theory set))))
       (fold-dynlist-lr 
	(lambda (x seed)
	  (f (reverse-lookup theory (vector-ref x 0))
	     (vector-ref x 1) seed))
	tree 0  
	(lambda (x)
	  (not (= (logand x ih) 0))))))))

(define get-translation-data
  (case-lambda 
    (() (get-translation-data (fluid-ref *current-set-theory*)))
    ((theory)
     (let lp ((theory theory) (l '()))
       (let ((p (get-parent theory)))
	 (if p
	     (lp p (cons* (get-set theory) (get-i->j theory) 
			  (get-i->subs theory) l))
	     (cons (get-set->i theory) l)))))))
	     

(define order-the-set
  (case-lambda
    (()
     (fluid-set! *current-set-theory*
		 (order-the-set (fluid-ref *current-set-theory*)))) 
    ((theory)
     (compile-set-representation theory))))

(define set<
  (case-lambda
    ((x y)
     (set< (fluid-ref *current-set-theory*) x y))
    ((theory x y)
      (let ((i->subs (get-i->subs theory)))
       (> (logand (get-i i->subs y)
		  x)
	  0)))))
