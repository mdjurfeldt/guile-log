(define-module (logic guile-log ck)
  #:use-module (ice-9 pretty-print)
  #:export (ck ck-pk))

(define-syntax ck-pk
  (lambda (x)
    (syntax-case x (quote)
      ((_ s 'x)
       (begin
         (pretty-print (syntax->datum #'x))
         #'(ck s 'x))))))
       
;; This is the classic ck macro copied from guile master
(define-syntax ck
  (syntax-rules (quote)
    ((ck () 'v) v)                      ; yield the value on empty stack

    ((ck (((op ...) ea ...) . s) 'v)    ; re-focus on the other argument, ea
     (ck-arg s (op ... 'v) ea ...))

    ((ck s (op ea ...))                 ; Focus: handling an application;
     (ck-arg s (op) ea ...))))          ; check if args are values

(define-syntax ck-arg
  (syntax-rules (quote)
    ((ck-arg s (op va ...))             ; all arguments are evaluated,
     (op s va ...))                     ; do the redex

    ((ck-arg s (op ...) 'v ea1 ...)     ; optimization when the first ea
     (ck-arg s (op ... 'v) ea1 ...))    ; was already a value

    ((ck-arg s (op ...) ea ea1 ...)     ; focus on ea, to evaluate it
     (ck (((op ...) ea1 ...) . s) ea))))
;; -----------------------------------------------------------------------
