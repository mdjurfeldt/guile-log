(define-module (logic guile-log parsing scanner)
  #:export (make-guile-log-scanner-tools))

(define-syntax-rule (make-guile-log-scanner-tools
		      <p-lambda>
		      <fail>
		      <p-cc>		     
		      <succeds>
		      ..
		      (X ...)
		      (c ...)
		      (d ...)
		      s-false
		      s-true
		      s-mk-seq
		      s-mk-and
		      s-mk-or)

  (begin
    (define s-false (<p-lambda> (c ...)  <fail>))
    (define s-true  (<p-lambda> (c ...) (<p-cc> c ...)))
    
    
    (define-syntax-rule (s-mk-seq s-seq <and> )
      (define-syntax-rule (s-seq f ((... ...) (... ...)))
	(<p-lambda> (c ...)
	   (<and>
	    (.. (c ...) (f c ...))
	    ((... ...) (... ...))
	    (<p-cc> c ...)))))
  
    (define-syntax-rule (s-mk-and s-and <and>)
      (define-syntax-rule (s-and f ((... ...) (... ...)) fend)
	(<p-lambda> (c ...)
	  (<and>
	   (<succeds> 
	    (<and>
	     (.. (d ...) (f c ...))))
	   ((... ...) (... ...))
	   (.. (fend c ...))))))

    (define-syntax-rule (s-mk-or s-or <or>)
      (define-syntax-rule (s-or f ((... ...) (... ...)))
	(<p-lambda> (c ...) (<or> (f X ... c ...) ((... ...) (... ...))))))))

