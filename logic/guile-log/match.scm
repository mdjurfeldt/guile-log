(define-module (logic guile-log match)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log type)
  #:use-module (logic guile-log umatch)
  #:export (compile-match mockalambda))

(define compile-prolog #f)

(define-syntax-rule (aif (it) p a b) (let ((it p)) (if it a b)))

(define (repr s stx x)
  (define (mkn x)
    (let ((r  (hash-ref tag2i x #f)))
      (if r
	  #`(list-ref taglist #,r)
	  x)))

  (define (mks x) (datum->syntax stx (procedure-name x)))

  (let ((x (gp-lookup x s)))
    (cond
     ((and (pair? x) (number? (car x)))   ;; internal variable
      (car x))
     ((number? x) ;;Instruction
      (mkn x))
     ((procedure? x)
      (mks x))
     ((null? x)
      #`(quote #,x))
     (else x))))

(define taglist (reverse (gp-get-taglist)))
(define tag2i (let ((m (make-hash-table)))
		(define i 0)
		(for-each (lambda (x)
			    (hash-set! m x i)
			    (set! i (+ i 1)))
			  taglist)
		m))
		 	  
(define pp #f)
(define* (tr x #:optional (m #f))
  (define-syntax-rule (tr-it x (i j nm) ...)
    (cond 
     ((eq? x 'nm) (if (or (not m) (eq? m '+))
		      (list-ref taglist i)
		      (list-ref taglist j)))
     ...))

  (if pp
      x
  
  (tr-it x
	 (0  0  match_var)
	 (1  1  match_evar)
	 (2  18 unify_scm)
	 (3  19 unify_internal)
	 (4  20 unify_external)
	 (5  6  cons_plus)
	 (6  6  cons_minus)
	 (7  8  vector_plus) 
	 (8  8  vector_minus)
	 (9  9  pop_vec)
	 (10 10 pop)
	 (11 11 and_tag)
	 (12 12 or_tag)
	 (13 13 next_or)
	 (14 14 last_or)
	 (15 15 fail)
	 (16 16 pop_frame)
	 (17 17 leave)
	 (18 18 unify_scm_minus)
	 (19 19 unify_internal_minus)
	 (20 20 unify_external_minus))))


(define table #f)
  
(define (term-variables s x pred)
  (define vars (make-hash-table))
  (let lp ((x x))
    (match x
      (#(xx ...)
       (lp xx))
      ((a . b)
       (lp a) 
       (lp b))
      
      (a
       (if (and (gp-var? a s) (pred a))
	   (aif (it) (hashq-ref vars a #f)
		(hashq-set! vars a (+ 1 it))		    
		(hashq-set! vars a 0))))))
  vars)
		    
      
(define external-vars (map (lambda (x) (make-fluid #f)) (iota 256)))
(define and-tag 'and)
(define or-tag  'or)
(define not-tag 'not)
(define fail-tag 'fail)

(define (m x) (lambda (y) (eq? x y)))
(define-syntax-rule (mk n) 
  (lambda () 
    (let ((r n))
      (set! n (+ n 1))
      r)))

(define (compile-match source? s exp res)  
  (define labi 0)
  (define labels (mk labi))

  (define ie 0)
  (define ne (mk ie))

  (define ii 0)
  (define ni (mk ii))
  
  (define touch   (make-hash-table))
  (define extvars (make-hash-table))

  (define (diff x y)
    (define res '())

    (hash-for-each
     (lambda (k v)
       (if (not (hashq-ref y k #f))
	   (set! res (cons k res))))
     x)

    res)
  
  (define true (lambda x #t))
  
  (let ((iv (term-variables s exp true))
	(ev (term-variables s res true)))

    (define epred 
      (lambda (x) 
	(hashq-ref ev x #f)))

    (define (tr-orvars stx? x)
      (define (trv x)
	(if stx?
	    #`(list-ref external-vars #,x)
	    (list-ref external-vars x)))

      (let lp ((x (append x (list (tr 'leave)))))
	(match x
	   (((and a (? (lambda (x) (member x '(or-tag next-or)))))
	     x l . u)
	    `(,a ,x ,(map (lambda (x) (trv (hashq-ref touch x #f)))
			  l) 
		 ,@(lp u)))

	   (((and a (? (lambda (x) (member x '(last-or)))))
	     l . u)
	    `(,a ,(map (lambda (x) (trv (hashq-ref touch x #f)))
		       l) 
		 ,@(lp u)))

	   ((x . l)
	    (cons x (lp l)))

	   (x x))))
    
    (define (tr-gotos x)
      (define map (make-hash-table))
      (cons
      (list->vector
      (let ((n (length x)))
	(reverse
	 (let lp ((x (reverse x))
		  (i 0))
	   (match x
	    (((#:label . x) . u)
	     (hashq-set! map x i)
	     (lp u i))
	    (((#:goto . x) . l)
	     (cons (- n (hashq-ref map x 10000000) 2) (lp l (+ i 1))))
	    ((x . l)
	     (cons x (lp l (+ i 1))))
	    (x x))))))
      extvars))
	    
    (tr-gotos
    (tr-orvars source?
    (let lp ((exp exp) (mode '+))
      (match exp
	 (((? (m or-tag)))
	   (tr 'fail))

	 (((? (m or-tag)) x)
	  (lp x mode))

	 (((? (m or-tag)) x . l)
	  (let* ((lb    (labels))
		 (all   (term-variables s exp epred))
		 (this  (term-variables s x   epred))
		 (other (diff all this)))
	    `(,(tr 'or-tag) ,(cons #:goto lb) ,other ,@(lp x mode)
	      ,(cons #:label lb)
		  ,@(let lp2 ((l l))
		      (match l
			((x)		       
			 `(,(tr 'last-or) ,(diff all 
						 (term-variables s x epred))
			   ,@(lp x mode)))
			((x . l)		       
			 (let ((lb     (labels))
			       (other  (diff all (term-variables s x epred))))
			   `(,(tr 'next-or) ,(cons #:goto lb) 
			     ,other
			     ,@(lp x mode) 
			     ,(cons #:label lb) ,@(lp2 l)))))))))
	 

	 ((? (m fail-tag))
	  (list (tr 'fail)))
	 
	 ((? (m '_))
	  (list #f))

	 (((? (m not-tag)) x)
	  (lp `((,or-tag (,and-tag ,x ,fail-tag) _)) mode))

	 (((? (m and-tag)) x)
	  (lp x mode))

	 (((? (m and-tag)) x . l)
	  `(,(tr 'and) ,@(lp x mode)
	    ,@(let lp2 ((l l))
		(match l
		  ((x)
		   `(,(tr 'redo) ,@(lp x mode) ,(tr 'pop)))
		  ((x . l)
		   `(,(tr 'redo) ,@(lp x mode) ,@(lp2 l)))))))
	 
	 (((? (m '+)) x)
	  (lp x '+))

	 (((? (m '-)) x)
	  (lp x '-))

	 ((x . l)
	  `(,(tr 'cons_plus mode) ,@(lp x mode) ,(tr 'pop) ,@(lp l mode)))

	 (#(x ...)
	  `(,(tr 'vector_plus mode) ,(length x)
	    ,@(match x
		(()
		 '())
		((x)
		 (lp x mode))
		((x . l)		      
		 `(,@(lp x mode) ,(tr 'next-vec) 
		   ,@(let lp2 ((l l))
		       (match l
			 ((x)
			  `(,@(lp x mode) ,(tr 'pop-vec)))
			 ((x . l)	 
			  `(,@(lp x mode) ,(tr 'next-vec) ,@(lp2 l))))))))))
	 
	 (x
	  (let ((x (gp-lookup x s)))
	    (if (gp-var? x s)
	      (if (hashq-ref ev x #f)
		  (aif (r) (hashq-ref touch x #f)
		       (list (tr 'unify_external mode)
			     (if source?
				 #`(list-ref external-vars #,r)
				 (list-ref external-vars r)))
		       (let ((next-e (ne)))
			 (hashq-set! touch   x next-e)
			 (hashq-set! extvars x next-e)
			 (list (tr 'match_evar) 
			       (if source?
				   #`(list-ref external-vars #,next-e)
				   (list-ref external-vars next-e)))))
		  (aif (r) (hashq-ref touch x #f)
		       (list (tr 'unify_internal mode) r)
		       (let ((next-i (if source? (list (ni)) (ni))))
			 (hashq-set! touch x next-i)
			 (list (tr 'match_var) next-i))))
	      (list (tr 'unify_scm mode) x))))))))))
			 
	 
(define mu (make-fluid '()))
(define (mapi lam n l)
  (let lp ((i n) (l l))
    (if (> i 0)
	(cons (lam (car l)) (lp (- i 1) (cdr l)))
	'())))

(define (for-eachi lam n l)
  (let lp ((i n) (l l))
    (if (> i 0)
	(begin (lam (car l)) (lp (- i 1) (cdr l)))
	(if #f #f))))

(define delayers (@@ (logic guile-log code-load) *delayers*))

(define-syntax-rule (wrap (cut s p cc) x comp code)
  (let ((old (fluid-ref delayers))
	(s (gp-match x comp s)))
    (if s
	(dls-match (cut s p cc) old code)
	(p))))
	    

(define (mockalambda_ source? s pat code)
  (define (get-extvars table)
    (define temp '())
    (hash-for-each 
     (lambda x (set! temp (cons x temp)))
     table)
    temp)

  (define (pu x) #;(pretty-print (syntax->datum x)) x)
  (define (rep comp)
    #`((@ (guile) vector) #,@(pu (map (lambda (co) (repr s source? co))
			  (vector->list comp)))))

;  (set! pp #t)
;  (pretty-print `(compiled ,(compile-match source? s pat code)))
;  (set! pp #f)
  (let* ((comp.table (compile-match source? s pat code))
	 (comp       (car comp.table))
	 (table      (cdr comp.table))
	 (extvars    (sort (get-extvars table) (lambda (x y)
						 (< (cadr x) 
						    (cadr y)))))
	 (vars       (map car  extvars))
	 (ivars      (map cadr extvars))
	 (fvars      (map (lambda (i) (list-ref external-vars i))
			   ivars))
	 (n          (let lp ((l ivars) (i -1))
		       (if (pair? l)
			   (lp (cdr l) (max (car l) i))
			   i)))
	 (oth        (compile-prolog s pat  code source? (list #t #t)))
	 (lam        (compile-prolog s vars code source? (list #t #f))))
    (if source?
    #`(let ((o #,oth))
	(list
	 (car o)
	 (lambda ()
	  #,(case (length fvars)
	     ((0)
	      #`(let ((co #,(rep comp)))
		  (lambda (s p cc cut scut x)
		    (wrap (cut s p cc) x co
		      (#,lam cut scut)))))

	     ((1)
	      #`(let ((co #,(rep comp)))
		  (lambda (s p cc cut scut x)
		    (wrap (cut s p cc) x co
		      (<let*> ((f (car external-vars))
			       (v (fluid-ref f)))
			 (<code> (fluid-set! f #f))
			 (#,lam cut scut v))))))

	     ((2)
	      #`(let ((co #,(rep comp)))
		  (lambda (s p cc cut scut x)
		    (wrap (cut s p cc) x co
		      (<let*> ((f1 (car external-vars))
			       (v1 (fluid-ref f1))
			       (f2 (cadr external-vars))
			       (v2 (fluid-ref f2)))
		       (<code>
			(fluid-set! f1 #f)
			(fluid-set! f2 #f))
		       (#,lam cut scut v1 v2))))))
	     (else
	      #`(let ((co #,(rep comp)))
		  (lambda (s p cc cut scut x)
		    (wrap (cut s p cc) x co
		      (<let> ((vs (mapi (lambda (x) (fluid-ref x))
					#,(length fvars) external-vars)))
			(<code>
			 (for-eachi (lambda (x) (fluid-set! x #f)) 
				    #,(length fvars) external-vars))
			(<apply> #,lam cut scut vs))))))))
	 (cadr o)))

    (list
     (car oth)
     (lambda ()
     (case (length fvars)
       ((0)
	(let ((co comp))
	  (lambda (s p cc cut scut x)
	    (wrap (cut s p cc) x co
	      (lam cut scut)))))

       ((1)
	(let ((co comp))
	  (lambda (s p cc cut scut x)
	    (wrap (cut s p cc) x co
	      (<let*> ((f (car fvars))
		       (v (fluid-ref f)))
		(<code> (fluid-set! f #f))
		(lam cut scut v))))))

       ((2)
	(let ((co comp))
	  (lambda (s p cc cut scut x)
	    (wrap (cut s p cc) x co
	      (<let*> ((f1 (car fvars))
		       (v1 (fluid-ref f1))
		       (f2 (cadr fvars))
		       (v2 (fluid-ref f2)))
	         (<code> (fluid-set! f1 #f)
			 (fluid-set! f2 #f))
		 (lam cut scut v1 v2))))))

      (else
       (let ((co comp))
	 (lambda (s p cc cut scut x)
	   (wrap (cut s p cc) x co
	     (<let> ((vs (map (lambda (x) (fluid-ref x)) fvars)))
	       (<code> (for-each (lambda (x) (fluid-set! x #f)) fvars))
	       (<apply> lam cut scut vs))))))))

     (cadr oth)))))

(define (mockalambda source? s pat code)
  (mockalambda_   source? s pat code))
    
