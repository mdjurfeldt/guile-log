(define-module  (logic guile-log examples einstein)
  #:use-module (srfi srfi-11)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log vlist)
  #:use-module (ice-9 pretty-print)
  #:export     (run))


;(use-modules (language prolog kanren))

#;(define memb gp-member)


(<define> (memb x l)
   (<match> () (l)
     ((,x . l) <cc>)
     ((_  . l) (memb x l))
     (_        <fail>)))

#;(define (memb s p cc x l)
  (let ((f (gp-newframe s)))
    (letrec ((next (lambda (l)
                     (let loop ((l l))
                     (gp-unwind f)
                       (let-values (((a as s) (gp-fpair!? l s)))
                         (if s
                             (let ((s (gp-unify-raw! a x s)))
                               (if s
                                   (cc s (lambda () (next as)))
                                   (loop as)))
                             (p)))))))
      (next l))))



(<define> (on-right i j l)
  (<match> () (l)
    ((,i ,j . _) <cc>)
    ((_ . l)     (<cut> (on-right i j l)))
    (()          <fail>)
    (_           <fail>)))


#;(define on-right gp-right-of)

#;(define (on-right s p cc i j l)
  (let ((f (gp-newframe s)))
    (letrec ((next (lambda (l)
                     (let loop ((l l))
                       (gp-unwind f)
                       (let-values (((ii ll s) (gp-fpair!? l f)))
                         (if s
                             (let-values (((jj kk s) (gp-fpair!? ll s)))
                               (if s
                                   (let ((s (gp-unify-raw! ii i s)))
                                     (if s
                                         (let ((s (gp-unify-raw! jj j s)))
                                           (if s
                                               (cc s (lambda () (next ll)))
                                               (loop ll)))
                                         (loop ll)))
                                   (p)))
                             (p))))))

             (loop (lambda (l)
                     (gp-unwind f)
                     (let-values (((ii ll s) (gp-fpair!? l f)))
                       (if s
                           (let-values (((jj kk s) (gp-fpair!? ll s)))
                             (if s
                                 (let ((s (gp-unify-raw! ii i s)))
                                   (if s
                                       (let ((s (gp-unify-raw! jj j s)))
                                         (if s
                                             (cc s (lambda () (next ll)))
                                             (loop ll)))
                                       (loop ll)))
                                 (p)))
                           (p))))))
      (loop l))))
                                          
  
#;(define next-to gp-next-to)   
(<define> (next-to item1 item2 rest)
  (<or> (on-right item1 item2 rest)
        (on-right item2 item1 rest)))

        
(define-syntax __
  (lambda (x)
    (syntax-case x ()
      ((x . l) #'((gp-var! *current-stack*) . l))
      (_ #'(gp-var! *current-stack*)))))

(define a1  #f)
(define a2  #f)
(define a3  #f)
(define a4a #f)
(define a4b #f)
(define a5  #f)
(define a6  #f)
(define a7  #f)
(define a8a #f)
(define a8b #f)
(define a9  #f)
(define a10a #f)
(define a10b #f)
(define a11a #f)
(define a11b #f)
(define a12  #f)
(define a13  #f)
(define a14a  #f)
(define a14b  #f)
(define a15   #f)

(define (analyze s)
  (let ((x (cdr s)))
    (if (vlist? x)
	(let ((a (struct-ref x 0))
	      (b (struct-ref x 1)))
	  (format #t "<vhash> offset = ~a, " b)
	  (let ((block (vector-ref a 0))
		(off   (vector-ref a 2))
		(size  (vector-ref a 3))
		(free  (vector-ref a 4)))
	    (format #t " size ~a, free ~a~%" size free)
	    (let lp ((i b))
	      (if (>= i 0)
		  (let* ((next (number->string
				(logand #xffffffff 
					(vector-ref block (+ (* size 3) i)))
				16))
			 (back (ash
				(vector-ref block (+ (* size 3) i))
				-32))
			 (hash (vector-ref block (+ (* size 2) back)))
			 (v    (object-address (vector-ref block i)))) 
				      
		    (format #t "~a: next ~a, back ~a hashv ~a key ~a~%" 
			    i next back 
			    hash (number->string v 16))
		    (lp (- i 1)))))))

	(format #t "<assoc>~%"))))
	


(<define> (einstein h)
  (<code>
   (set! a1    `(brit  ,__  ,__ ,__    red))   
   (set! a2    `(swede dog ,__ ,__    ,__ ))
   (set! a3    `(dane  ,__  ,__ tea  ,__  ))
   (set! a4a   `(,__ ,__ ,__ ,__ green))
   (set! a4b   `(,__ ,__ ,__ ,__ white))
   (set! a5    `(,__    ,__  ,__ cofee green))
   (set! a6    `(,__    bird pallmall ,__ ,__))
   (set! a7    `(,__    ,__   dunhill ,__ yellow))
   (set! a8a   `(,__ ,__ dunhill ,__ ,__))
   (set! a8b   `(,__ horse ,__ ,__ ,__))
   (set! a9    `(,__ ,__ ,__ milk ,__))
   (set! a10a  `(,__ ,__ marlboro ,__ ,__))
   (set! a10b  `(,__ cat ,__ ,__ ,__))
   (set! a11a  `(,__ ,__ marlboro ,__ ,__))
   (set! a11b  `(,__ ,__ ,__ water ,__))
   (set! a12   `(,__ ,__ winfield beer ,__))
   (set! a13   `(german ,__ rothmans ,__ ,__))
   (set! a14a  `(norwegian ,__ ,__ ,__ ,__))
   (set! a14b  `(,__ ,__ ,__ ,__ blue))
   (set! a15   `(,__ fish ,__ ,__ ,__)))
  (<and>
;   (<logical++>)
   (<=> h ,`((norwegian ,__ ,__ ,__ ,__) ,__ (,__ ,__ ,__ milk ,__) ,__ ,__))
   (memb  a1 h)
   (memb  a2 h)
;   (<code> (analyze S))
   (memb  a3 h)
;   (<code> (analyze S))
   ;(<code> (analyze S))
   (on-right a4a a4b h)
   ;(<code> (analyze S))
   ;(<stall>)
   (memb a5 h)
   (memb a6 h)
   (memb a7 h)
   (next-to  a8a a8b h)
   (memb a9 h)
   (next-to  a10a a10b h)
   (next-to  a11a a11b h)
   (memb  a12 h)
   (memb  a13 h)
   (next-to a14a a14b h)
   (memb a15 h)
   ;(<pp> S)
   ;(<code> (analyze S))
   ))


(define (run N)
  (let loop ((n N))
    (cond
     ((zero? n) 'done)
     (else 
      (gp-clear *current-stack*)
      (pretty-print (<run> 1 (h) (einstein h)))
      (loop (- n 1))))))
