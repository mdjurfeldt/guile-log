(define-module  (logic guile-log examples kanren  einstein)
  #:use-module (logic guile-log kanren)
  #:use-module (ice-9 pretty-print)
  #:export     (run))

;(use-modules (language prolog kanren))

(define memb 
  (relation (head-let item lst) 
    (any (== lst `(,item . ,_))
      (exists (rest)
	(if-only (== lst `(,_ . ,rest)) (memb item rest))))))


(define next-to
  (relation (head-let item1 item2 rest)
    (any (on-right item1 item2 rest) (on-right item2 item1 rest))))

(define on-right
  (extend-relation (a0 a1 a2)
    (fact (item1 item2) item1 item2 `(,item1 ,item2 . ,_))
    (relation ((once item1) (once item2) rest)
      (to-show item1 item2 `(,_ . ,rest))
      (on-right item1 item2 rest))))
        
(define einstein
  (relation (head-let h)
     (all!
      (== h `((norwegian ,_ ,_ ,_ ,_) ,_ (,_ ,_ ,_ milk ,_) ,_ ,_))
      (memb `(brit  ,_  ,_ ,_    red) h)
      (memb `(swede dog ,_ ,_    ,_  ) h)
      (memb `(dane  ,_  ,_ tea  ,_  ) h)
      (on-right `(,_ ,_ ,_ ,_ green) `(,_ ,_ ,_ ,_ white) h)
      (memb `(,_    ,_  ,_ cofee green) h)
      (memb `(,_    bird pallmall ,_ ,_) h)
      (memb `(,_    ,_   dunhill ,_ yellow) h)
      (next-to `(,_ ,_ dunhill ,_ ,_) `(,_ horse ,_ ,_ ,_) h)
      (memb `(,_ ,_ ,_ milk ,_) h)
      (next-to `(,_ ,_ marlboro ,_ ,_) `(,_ cat ,_ ,_ ,_) h)
      (next-to `(,_ ,_ marlboro ,_ ,_) `(,_ ,_ ,_ water ,_) h)
      (memb `(,_ ,_ winfield beer ,_) h)
      (memb `(german ,_ rothmans ,_ ,_) h)
      (next-to `(norwegian ,_ ,_ ,_ ,_) `(,_ ,_ ,_ ,_ blue) h)
      (memb `(,_ fish ,_ ,_ ,_) h)
     )))


(define (run N)
  (let loop ((n N))
    (cond
     ((zero? n) 'done)
     (else (pretty-print (solution (h) (einstein h)))
           (loop (- n 1))))))
