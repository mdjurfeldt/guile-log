(use-modules (ice-9 match))
(use-modules (ice-9 pretty-print))
(use-modules (logic guile-log))
(use-modules (logic guile-log parser))
(use-modules (logic guile-log parsing operator-parser))

(define p-term #f)
(define application (s-seq p-term (s-char #\)) tok-ws*))
		     

(define ops (make-opdata))
(add-operator ops 'xfy "," 6 tok-ws*)
(add-operator ops 'xfy '=  5 tok-ws*)
(add-operator ops 'xfy '+  4 tok-ws*)
(add-operator ops 'xfy '-  4 tok-ws*)
(add-operator ops 'xfy '/  3 tok-ws*)
(add-operator ops 'xfy '*  3 tok-ws*)
(add-operator ops 'fy  '-  2 tok-ws*)
(add-operator ops 'yf  "(" 1 application)

(define end-level 10)
(define op-match
  (letrec* ((int       (s+ (s-reg! "[0-9]")))
	    (dec       (s-seq int (s-char! #\.) int))
	    (exp       (s-seq (s-or dec int)
			      (s-char! #\e) 
			      (s-or (s-char! #\+)
				    (s-char! #\-)
				    s-true)
			      int))
	    (num-token (p-freeze
			(mk-token (s-or! exp dec int))
			(lambda (s cin cout)
			  (list #:number cout))))

	    (id-tok    (let ((tok (s-seq (s-reg! "[_a-zA-Z]")
					 (s* (s-reg! "[_a-zA-Z0-9]")))))
			 (p-freeze (mk-token tok)		       
				   (lambda (s cin cout)
				     (list #:identifier cout)))))
		    
	    (par-token (let ((tok (s-seq (s-char #\() s-term (s-char #\)))))
			 (p-freeze tok
				   (lambda (s cin cout) cout))))

	    (atom      (let ((tok (s-seq
				   tok-ws*
				   (s-or! par-token id-tok  num-token)
				   tok-ws*)))
			 (p-freeze tok
				   (lambda (s cin cout) cout))))
	   
	    (term      (mk-operator-expression atom ops))
	    (s-term    (s-seq s-ws*
			      (<p-lambda> (c) (.. (term end-level)))
			      s-ws*))
	   (op-match  (s-seq s-term s-eof)))
 (set! p-term s-term)
  op-match))

(define (make-args x f)
  (let lp ((x x))
    (match x
      ((('xfy _ "," _) x y   n m)
       (cons (f x) (lp y)))
      (x (list (f x))))))

(define (show x)
  (match x
    ((#:identifier a) (string->symbol a))
    ((#:number     n) (string->number n))
    ((('yf _ "(" val) x . _)
     (let ((args (make-args val show)))	    
       (cons (show x) args)))
    ((((or 'xfy 'yfx 'xfx) _ op . _) x y . _)
     `(,op ,(show x) ,(show y)))
    ((((or 'xf 'yf 'fx 'fy) _ op . _) x . _)
     `(,op ,(show x)))
    ((_ . _)
     (map show x))
    (x x)))

(define (calc x vars)
  (let lp ((x x))
    (match x
      ((('xfy _ '= _) (#:identifier id) y n m)
       (let* ((id  (string->symbol id))
	      (t   (assoc id vars))
	      (val (lp y)))
	 (if t
	     (begin (set-cdr! t val) val)
	     (begin (set! vars (cons (cons id val) vars)) val))))
      ((('xfy _ '= _) _ _   n m)
       (error (format #f "At (row ~a,col ~a) l.h.s of = is not an identifier"
		      n m)))
      ((('xfy _ "," _) x y   n m)
       (lp x) 
       (lp y))

      ((('xfy _ '+ _) x y _ _)
       (+ (lp x) (lp y)))
      
      ((('xfy _ '- _) x y _ _)
       (- (lp x) (lp y)))
      
      ((('xfy _ '* _) x y _ _)
       (* (lp x) (lp y)))

      ((('xfy _ '/ _) x y _ _)
       (/ (lp x) (lp y)))

      ((('fy _ '- _) x _ _)
       (- (lp x)))
      
      ((('yf _ "(" y) (#:identifier id) n m)
       (let* ((id  (string->symbol id))
	      (t   (assoc id vars)))
	 (if t
	     (apply (cdr t) (make-args y lp))
	     (error (format 
		     #f 
	   "At (row ~a,col ~a) l.h.s of = is not an identifier of a function"
		     n m)))))

      ((#:number x) (string->number x))
      ((#:identifier id)
       (let* ((id  (string->symbol id))
	      (t   (assoc id vars)))
	 (if t (cdr t)	     
	     (error (format 
		     #f 
	   "~a is not an identifier of a variable" id))))))))



      
       
      
   
(define (p-calc str) 
  (let ((p (parse str op-match)))
    (pretty-print (show (car p)))
    (calc (car p) (list (cons 'pi  3.1416)
		        (cons 'sin sin)
			(cons 'cos cos)
			(cons 'tan tan)
			(cons 'exp exp)
			(cons 'log log)))))
