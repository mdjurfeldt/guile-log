(define-module (logic guile-log tools)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log macros)
  #:use-module (logic guile-log interleave)
  #:use-module (logic guile-log undovar)
  #:export (<f-vector> <vector> <fold> <fold-step> <fix-fold>
		       <fix-fold-pre> <fix-fold-sorted> <fix-fold-step> 
		       <member> <uniq> m= <yield-at-change>
		       <with-generators> <next-generator-value>
		       <fold-step-e>))


(define-guile-log <with-generators>
  (syntax-rules ()
    ((_ w ((x i) ...) code ...)
     (<let-with-lr-guard> w wind lg rg ((x i) ...)
	(lg (</.> code ...))))))

(define-guile-log <next-generator-value>
  (syntax-rules ()
    ((_ w kons v x)
     (begin
       (set! v (kons (<scm> x) v))
       (parse<> w <cc>)))))

(<define> (<member> X L)
  (<match> (#:name '<member>) (L)
    ((Y . _)  (<=> X Y))
    ((_  . U) (<cut> (<member> X U)))
    (_        (<cut> <fail>))))

(define tag (gensym "eq-tagg"))


(define (m= s x y)
  (define i 0)
  (let* ((fr  (gp-newframe s))
         (ret ((</.>
                (<recur> lp ((x x) (y y))
                  (<match> (#:mode - #:name tools.u=) (x y)
                    ((? <var?> a) (? <var?> b)
                     (<cut>
                      (<and>
                       (<=> a b)
                       (<=> a ,(cons tag i))
                       (<code> (set! i (+ i 1))))))
       
                    ((x . l) (y . ll)
                     (<cut>
                      (<and>
                       (lp x y)
                       (lp l ll))))
                    ((? vector?) (? vector?)                     
                     (<cut> 
                      (lp (vector->list (gp-lookup x S))
                          (vector->list (gp-lookup y S)))))
                    (x y
                      (<cut> (<==> x y))))))
               s (lambda () #f) (lambda x #t))))
    (gp-unwind-tail fr)
    ret))
    

(<define> (<umember> X L)
  (<match> (#:name '<umember>) (L)
    ((Y . _)  (if (m= S X Y) <cc> <fail>))
    ((_  . U) (<cut> (<umember> X U)))
    (_        (<cut> <fail>))))
    

(<define> (new-machine) <cc>)

(<define> (<yield-at-change> test code)
       (<let-with-lr-guard> wind lguard rguard 
			    ((old-state #f) 
			     (old-p     #f)
			     (change?   #f)
			     (start?    #t) 
			     (val       #f))
	 (lguard
	  (</.>
	   (<or>
	    (code (lambda (s) start?))
	    <cc>)
	   (if start?
	       (<and>
		(<code> (set! start?    #f) 
			(set! val       (<cp> test))
			(set! old-p     P)
			(set! old-state (gp-store-state S)))
		<fail>)	       
	       (if (m= S test val)
		   (<and> 
		    (<code> 
		     (set! old-p     P)
		     (set! old-state (gp-store-state S)))
		    <fail>)
		   (<and>
		    (<code> 
		     (set! start? #t)
		     (set! val (<cp> test)))
		    (<code> (gp-restore-wind old-state
					     (gp-rebased-level-ref wind)))
		    <cut>
		    (<or>
		     <cc>
		     (<ret> (old-p))))))
		     
	   (rguard
	    (</.> <cc>))))))

(<define> (<fold> kons knil Lam X L . E)
    (<let> ((p P))
    (<let-with-lr-guard> wind lguard rguard ((l (cons '() knil)))
      (lguard
       (</.>
	(<or>
	 (<and>
	  (new-machine)
	  (<funcall> Lam)
	  (if (pair? E) ((car E) (cdr l)) <cc>)
	  (<code>
	   (let* ((a (<cp> X))
		  (b (ref-attribute-constructors S)))
	     (set! l (cons (append b (car l))
			   (kons   a (cdr l))))))
	  <fail>)
	 (rguard
	  (</.>
	   (<and>      
	    (do-attribute-constructors (car l))
	    (<with-fail> p (<=> ,(cdr l) L)))))))))))

(<define> (<fold-e> kons knil Lam F X L . E)
    (<let> ((p P))
    (<let-with-lr-guard> wind lguard rguard ((l (cons '() knil)))
      (lguard
       (</.>
	(<or>
	 (<and>
	  (new-machine)
	  (<funcall> Lam)
	  (if (pair? E) ((car E) (cdr l)) <cc>)
	  (<code>
	   (let* ((a (<cp> X))
		  (b (ref-attribute-constructors S)))
	     (set! l (cons (append b (car l))
			   (kons   a (cdr l))))))
	  (F))
	 (rguard
	  (</.>
	   (<and>      
	    (do-attribute-constructors (car l))
	    (<with-fail> p (<=> ,(cdr l) L)))))))))))



(<define> (<fold-step-e> kons knil Lam X L . E)
   (<let-with-lr-guard> wind lguard rguard ((l (cons '() knil)))
      (lguard
       (</.>
	(<and>
	 (new-machine)
	 (<funcall> Lam)
	 (if (pair? E) ((car E) (cdr l)) <cc>)
	 (<code>
	   (let* ((a (<cp> X))
		  (b (ref-attribute-constructors S)))
	     (set! l 
		   (cons (append b (car l))
			 (kons  S a (cdr l))))))
	 (do-attribute-constructors (car l))
	 (<=> ,(cdr l) L)
	 (rguard (</.> <cc>)))))))

(<define> (<fold-step> kons knil Lam X L . E)
   (<let-with-lr-guard> wind lguard rguard ((l (cons '() knil)))
      (lguard
       (</.>
	(<and>
	 (new-machine)
	 (<funcall> Lam)
	 (if (pair? E) ((car E) (cdr l)) <cc>)
	 (<code>
	   (let* ((a (<cp> X))
		  (b (ref-attribute-constructors S)))
	     (set! l 
		   (cons (append b (car l))
			 (kons   a (cdr l))))))
	 (do-attribute-constructors (car l))
	 (<=> ,(cdr l) L)
	 (rguard (</.> <cc>)))))))
       
(<define> (<uniq> Lam Y)
  (<let-with-lr-guard> wind lguard rguard ((l '()))
     (lguard 
      (</.> 
       (Lam)
       (<let> ((y (<cp> Y)))
          (<not> (<umember> y l))
          (<code> (set! l (cons y l)))
         (do-attribute-constructors)
         (rguard (</.> <cc>)))))))
            
;; This is a slow n² algorithm using the functional database
;; Indexing Framework, it can be designed to be essentially n¹
;; It has it merrits though in getting bagof for infinit sets.
;; But then a full prolog bagof algorithm is impossible without a major
;; rework of the underlying engine
(<define> (<fix-fold> kons knil Lam X Y L . E)
  (if (pair? (<lookup> Y))
      (<var> (Z)
       (<call> ((Z Y)) 
	  (<uniq> (<lambda> () (Lam)) Y))

       (<and!>   
	 (<apply> <fold> kons knil 
		  (<lambda> () 
		    (Lam)
		    (if (m= S Y Z) <cc> <fail>))
		  X L E))
       (<=> Y Z))
      (<var> (Z)
	 (<and!>
	  (<or>   
	   (<apply> <fold> kons knil Lam X L E)
	   (<=> knil L))))))

(<define> (<fix-fold-pre> kons knil Lam X Y L . E)
  (if (pair? (<lookup> Y))
      (<var> (Z)
       (<call> ((Y Y)) 
	  (<uniq> (<lambda> () (Lam)) Y))
       (<and!> (<apply> <fold> kons knil Lam X L E)))
      (<var> (Z)
	 (<and!>
	  (<or>   
	   (<apply> <fold> kons knil Lam X L E)
	   (<=> knil L))))))

(<define> (<fix-fold-sorted> kons knil Lam X Y L . E)
   (if (pair? (<lookup> Y))
     (<yield-at-change> Y 
       (<lambda> (start?)
         (<let> ((Kons (lambda (s x in)
			 (if (start? s)
			     (kons x knil)
			     (kons x in)))))
	    (<apply> <fold-step-e> Kons knil Lam X L E))))
     (<var> (Z)
       (<and!>
	(<or>   
	 (<apply> <fold> kons knil Lam X L E)
	 (<=> knil L))))))

   

(<define> (<fix-fold-step> kons knil Lam X Y L . E)
  (<var> (Z)
    (<call> ((Z Y)) 
       (<uniq> Lam Y))
    (<apply> <fold-step> kons knil 
		 (<lambda> ()
		    (Lam) 
		    (if (m= S Y Z) 
			<cc> 
			<fail>))
		 X L E)))

(<define> (<f-vector> f . x)
  (<recur> loop ((x x) (l '()))
    (if (pair? x)
	(<var> (y)
	  (<=> y ,(car x))
	  (loop (cdr x) (cons y l)))
	(f (apply vector (reverse l))))))

(<define> (<vector> r . x)
  (<recur> loop ((x x) (l '()))
    (if (pair? x)
	(<var> (y)
	  (<=> y ,(car x))
	  (loop (cdr x) (cons y l)))
	(<=> r ,(apply vector (reverse l))))))
