/*
  We need a special variable
 */

static int gp_variable_gc_kind;

static struct GC_ms_entry *
gp_mark_variable (GC_word *addr, struct GC_ms_entry *mark_stack_ptr,
	   struct GC_ms_entry *mark_stack_limit, GC_word env)
{
  register SCM cell;
  //printf("var");fflush(stdout);
  cell = PTR2SCM (addr);
  if (!SCM_NIMP(cell) || SCM_TYP7 (cell) != scm_tc7_smob)
    /* It is likely that the GC passed us a pointer to a free-list element
       which we must ignore (see warning in `gc/gc_mark.h').  */
    {
      //printf("/var%p\n",*addr);fflush(stdout);      
      return mark_stack_ptr;
    }
  
  if(SCM_SMOB_PREDICATE(gp_stack_type,cell))
    {
      //printf(" 1 ");fflush(stdout);
      mark_stack_ptr = gp_stack_mark(cell, mark_stack_ptr, mark_stack_limit);
    }  
  else
    {   
      //printf(" 2 ");fflush(stdout);
      SCM x = SCM_CELL_OBJECT_1 (cell);
#ifdef HAS_GP_GC
      if(GP_GC_ISMARKED(SCM_UNPACK(SCM_CELL_OBJECT_0 (cell))))
      {
	//printf(" 3 ");fflush(stdout);
	mark_stack_ptr = GC_MARK_AND_PUSH (SCM2PTR (x),
					   mark_stack_ptr,
					   mark_stack_limit, NULL);
	
	
	{
	  scm_t_bits i = GP_COUNT(cell);
	  if(i > 0)
	    {
	      scm_t_bits thid = GP_ID(cell);
	      SCM vec = weak_thread_keys[thid];
	      SCM x   = scm_c_weak_vector_ref(vec, i);

	      mark_stack_ptr = GC_MARK_AND_PUSH (SCM2PTR (x),
						 mark_stack_ptr,
						 mark_stack_limit, NULL);
	    }
	}
	 
	if(GP_CONS(GP_GETREF(cell)))
	  {
	    //printf(" 4 ");fflush(stdout);
	    mark_stack_ptr = 
	      GC_MARK_AND_PUSH (SCM2PTR (SCM_CELL_OBJECT_2 (cell)),
				mark_stack_ptr,
				mark_stack_limit, NULL);  
	  }
      }
      //else
      //printf("no-marked\n");
#else
      mark_stack_ptr = GC_MARK_AND_PUSH (SCM2PTR (x),
					   mark_stack_ptr,
					   mark_stack_limit, NULL);  
      if(GP_CONS(GP_GETREF(cell)))
	{
	  mark_stack_ptr = 
	    GC_MARK_AND_PUSH (SCM2PTR (SCM_CELL_OBJECT_2 (cell)),
			      mark_stack_ptr,
			      mark_stack_limit, NULL);  
	}
#endif

    }  
  //printf("/var\n");fflush(stdout);
  return mark_stack_ptr;
}


SCM gp_make_variable()
{
  struct gp_stack *gp = maybe_get_gp();

  int id = 0;
  if(gp)
    id = gp->id;
  
  SCM ret = PTR2SCM (GC_generic_malloc (2 * sizeof (scm_t_cell), 
                                        gp_variable_gc_kind));
  
  SCM tc = SCM_PACK(GP_SETID(GP_MK_FRAME_UNBD(gp_type), id));
  SCM_SET_CELL_WORD_1 (ret, SCM_UNBOUND);
  SCM_SET_CELL_WORD_0 (ret, tc);

  return ret;
}

SCM gp_make_variable_id(int id)
{
  SCM ret = PTR2SCM (GC_generic_malloc (2 * sizeof (scm_t_cell), 
                                        gp_variable_gc_kind));

  SCM tc = SCM_PACK(GP_SETID(GP_MK_FRAME_UNBD(gp_type), id));
  SCM_SET_CELL_WORD_1 (ret, SCM_UNBOUND);
  SCM_SET_CELL_WORD_0 (ret, tc);

  return ret;
}

SCM gp_make_cons()
{
  SCM ret = PTR2SCM (GC_generic_malloc (3 * sizeof (scm_t_cell), 
                                        gp_variable_gc_kind));
  SCM tc =  SCM_PACK(GP_MK_FRAME_CONS(gp_type));
  SCM_SET_CELL_WORD_2 (ret, SCM_UNBOUND);
  SCM_SET_CELL_WORD_1 (ret, SCM_UNBOUND);
  SCM_SET_CELL_WORD_0 (ret, tc);
  return ret;
}

SCM gp_make_cons_id(int id)
{
  SCM ret = PTR2SCM (GC_generic_malloc (3 * sizeof (scm_t_cell), 
                                        gp_variable_gc_kind));
  SCM tc =  SCM_PACK(GP_MK_FRAME_CONS(gp_type));
  SCM_SET_CELL_WORD_2 (ret, SCM_UNBOUND);
  SCM_SET_CELL_WORD_1 (ret, SCM_UNBOUND);
  SCM_SET_CELL_WORD_0 (ret, tc);
  return ret;
}

void init_variables()
{
#ifdef HAS_GP_GC
  static int x = 0xffff;
  SCM        y = GP_UNREF(&x);
  gp_variable_gc_kind 
    = GC_new_kind_adv (GC_new_free_list (),
                       GC_MAKE_PROC (GC_new_proc (gp_mark_variable), 0),
                       0,1,(GC_word) GPI_SCM_M, SCM_TYP7(y),scm_tc7_smob);
#else
  gp_variable_gc_kind 
    = GC_new_kind(GC_new_free_list (),
		  GC_MAKE_PROC (GC_new_proc (gp_mark_variable), 0),
		  0,1);

#endif
}

SCM gp_variable_ref(SCM x)
{
  return GP_GETREF(x)[1];
}
