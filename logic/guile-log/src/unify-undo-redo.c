//#define DB(X)

#define STATE_LOGICAL   0
#define STATE_DYNSTACK  1
#define STATE_DYNLENGTH 2
#define STATE_RGUARDS   3
#define STATE_N         4
#define STATE_PATH      5
#define CONTROL_PATH    6
#define HANDLERS        7
#define NSTATE          8

#include "dynstack.c"
static inline SCM gp_store_state(struct gp_stack *gp, int allp);
static void gp_restore_state(SCM data, struct gp_stack *gp, SCM K);

SCM gp_state_token = SCM_BOOL_F;
static inline SCM gp_get_state_token()
{
  return scm_fluid_ref(gp_state_token);
}

static inline void check_ci_store(SCM x)
{
  if(SCM_I_INUMP(x))
    scm_misc_error("unwind","unwind store a number in car ~a",scm_list_1(x));
}

static inline void gp_new_state_token()
{
  SCM x = scm_fluid_ref(gp_state_token);
  scm_variable_set_x(x,SCM_BOOL_T);
  scm_fluid_set_x(gp_state_token,scm_make_variable(SCM_BOOL_F));
}


static SCM inline get_cs(SCM v)
{
  if(SCM_UNLIKELY(SCM_CONSP(v)))
    {
      switch (SCM_UNPACK(SCM_CAR(v)))
        {
        case gp_redo_tag: 
          gp_debug0("redo\n");
          v = SCM_CDDR(v);
          break;
	  
        case gp_save_tag: 
          gp_debug0("save\n");
          v = SCM_CDR(v);
          break;

        default:
          scm_misc_error("unpacking","cs value not correctly wraped ~a",
			 scm_list_1(SCM_CAR(v)));
        }
    }

  return v;
}

static inline int GP_NUMTAG(SCM x)
{
  if(SCM_CONSP(x))
    {
      if(SCM_UNPACK(SCM_CAR(x)) == gp_save_tag)
	return 2 & SCM_UNPACK(SCM_CDR(x));
      if(SCM_UNPACK(SCM_CAR(x)) == gp_redo_tag)
	return 2 & SCM_UNPACK(SCM_CDDR(x));
      return 0;
    }

  return 2 & SCM_UNPACK(x);
}

static inline SCM GP_GETNUMTAG(SCM x)
{
  if(SCM_CONSP(x))
    {
      if(SCM_UNPACK(SCM_CAR(x)) == gp_save_tag)
	return SCM_CDR(x);
      if(SCM_UNPACK(SCM_CAR(x)) == gp_redo_tag)
	return SCM_CDDR(x);
    }

  return x;
}

static SCM inline do_nongp(int state, SCM item)
{
  SCM cdr = SCM_CDR(item);
  gp_debug0("a call tag\n");
  if(scm_is_false(cdr))
    scm_call_1(SCM_CAR(item),SCM_BOOL_F);
  else if(scm_is_true(scm_procedure_p(cdr)))
    scm_call_1(SCM_CDR(item),(state == gp_store) 
	       ? SCM_BOOL_T : SCM_BOOL_F);
  gp_debug0("called\n");	  

  return item;
}

static inline SCM gp_handle(SCM item, int ret, SCM gp_unbd, int state)
{
  SCM x = SCM_BOOL_F,q,a,b,*id;

  gp_debug0("handle\n");

  if(SCM_CONSP(item))
    {
      gp_debug0("CONSP item\n");
      if(GP(SCM_CAR(item)))
	{
          gp_debug0("GP car\n");
 
	  id   = GP_GETREF(SCM_CAR(item));		

	  q    = SCM_CDR(item);
	  a    = SCM_CAR(q);
	  b    = SCM_CDR(q);

	  if(ret)
	    {
	      SCM_SETCAR(q,SCM_I_MAKINUM(SCM_PACK(id[0])));
	      SCM_SETCDR(q,id[1]);
	    }
	  id[0] = SCM_PACK(SCM_I_INUM(a));
	  id[1] = b;

	  return scm_cons(SCM_CAR(item), SCM_CDR(item));
	}

      gp_debug0("non GP car\n");
      return do_nongp(state, item);      
    }
  else if(GP(item))
    {
      if(GP_FRAME_VAR(item))
	return item;
      else
	{
	  id = GP_GETREF(item);
      
	  if(ret)
	    x = scm_cons(GP_UNREF(id),
			 scm_cons(SCM_I_MAKINUM(SCM_UNPACK(id[0])),id[1]));
	  id[0] = gp_unbd;
	  id[1] = SCM_UNBOUND;
	  return x;
	}
    }
  else if(scm_is_false(item))
    {
      return item;
    }
  else
    scm_misc_error("gp_handle","scheme object that is not a number on gp_cstack got ~a"
                   ,scm_list_1(item));
}

static inline SCM gp_handle_fr(SCM item, int ret, SCM gp_unbd, int state)
{
  return item;
}


static inline int is_advanced_tag(SCM *pt)
{
  return SCM_CONSP(*pt) && SCM_I_INUMP(SCM_CAR(*pt));
}

static inline int gp_advanced_fr(SCM item, int state, SCM *old, SCM gp_unbd)
{
  SCM redo;
  scm_t_bits tag = SCM_UNPACK(SCM_CAR(item));

  gp_debug1("inum => advanced tag = %x\n", tag);

  switch(tag)
    {
    case gp_save_tag:
      switch(state)
	{
	case gp_redo:
	  /*
	  *old = SCM_CDR(*old);
	  gp_handle(SCM_CDR(item), 1, gp_unbd, gp_redo);
	  return gp_redo;
	  */
	case gp_store: 
	  if(SCM_CONSP(*old))
	    SCM_SETCDR(*old, item);
	  else
	    scm_misc_error("unwind","a",SCM_EOL);

	case 0:
	  *old = item;
	  if(!SCM_CONSP(*old))
	    scm_misc_error("unwind","b",SCM_EOL);
	  if(!SCM_CONSP(item))
	    scm_misc_error("unwind","c",SCM_EOL);
	  item = SCM_CDR(item);
	  SCM_SETCAR(*old, gp_handle_fr(item, 1, gp_unbd, gp_store));
	  SCM_SETCDR(*old, SCM_EOL);
	  return gp_store;
	}

    case gp_redo_tag:
      if(!SCM_CONSP(item))
	scm_misc_error("unwind","d",SCM_EOL);
      if(!SCM_CONSP(*old) && state == gp_store)
	scm_misc_error("unwind","e",SCM_EOL);
	  
      item = SCM_CDR(item);
      redo = SCM_CAR(item);
      if(state == gp_store)
	SCM_SETCDR(*old, redo);
      gp_handle_fr(SCM_CDR(item), 0, gp_unbd, state);
      *old = redo;
      return gp_redo;
    default:
      scm_misc_error("gp-unwind","not a correct tag ~%~a",
		     scm_list_1(SCM_PACK(item)));
      return 0;
    }
}
static inline int gp_advanced(SCM item, int state, SCM *old, SCM gp_unbd)
{
  SCM redo;
  scm_t_bits tag = SCM_UNPACK(SCM_CAR(item));

  gp_debug1("inum => advanced tag = %x\n", tag);

  switch(tag)
    {
    case gp_save_tag:
      switch(state)
	{
	case gp_redo:
	  /*
	  *old = SCM_CDR(*old);
	  gp_handle(SCM_CDR(item), 1, gp_unbd, gp_redo);
	  return gp_redo;
	  */
	case gp_store:        
	  SCM_SETCDR(*old, item);

	case 0:
	  *old = item;
	  item = SCM_CDR(item);
	  SCM x = gp_handle(item, 1, gp_unbd, gp_store);
	  check_ci_store(x);
	  SCM_SETCAR(*old,x); 
	  SCM_SETCDR(*old, SCM_EOL);
	  return gp_store;
	}

    case gp_redo_tag:
      item = SCM_CDR(item);
      redo = SCM_CAR(item);
      if(state == gp_store)
	SCM_SETCDR(*old, redo);
      gp_handle(SCM_CDR(item), 0, gp_unbd, state);
      *old = redo;
      return gp_redo;
    default:
      return 0;
    }
}

/*
item
'(id id[0] id[1])
 (redo . undo)
 (i . l)
 */

static inline int gp_do_cons_fr(SCM item, int state, SCM *old, SCM gp_unbd)
{
  return gp_advanced_fr(item,state,old,gp_unbd);
}

static inline int gp_do_cons(SCM item, int state, SCM *old, SCM gp_unbd)
{
  SCM q,a,b,*id,tag;
  
  gp_debug0("do_cons\n");
  
  tag = SCM_CAR(item);

  if(SCM_I_INUMP(tag))
    return gp_advanced(item,state,old,gp_unbd);

  gp_debug0("not inum tag\n");

  if(!GP(tag))
    { 
      gp_debug0("not a GP tag\n");
      if(state)
	switch(state)
	  {
	  case gp_store:
	    {
	      SCM newold = scm_cons(item,SCM_EOL);
	      check_ci_store(item);
	      SCM_SETCDR(*old, newold);
	      *old = newold;
	      break;
	    }
	  case gp_redo:
	    *old = SCM_CDR(*old);
	  }

      do_nongp(state, item);
            
      return state;
    }

  gp_debug0("a GP tag\n");
  DB(gp_format1("GP before ~a~%", tag));
  id = GP_GETREF(tag);

  q  = SCM_CDR(item);
  a  = SCM_CAR(q);
  b  = SCM_CDR(q);

  if(state)
    {
      switch(state)
	{
	case gp_store:
	  {
	    SCM_SETCAR(q, SCM_I_MAKINUM(id[0]));
	    SCM_SETCDR(q, id[1]);
	    item = scm_cons(SCM_CAR(item), q);
	    check_ci_store(item);
	    item = scm_cons(item,SCM_EOL);
	    if(SCM_CONSP(*old))
	      SCM_SETCDR(*old,item);
	    *old = item;
            break;
	  }

	case gp_redo:
	  *old = SCM_CDR(*old);	  
	}
    }
  
  id[0] = SCM_PACK(SCM_I_INUM(a));
  id[1] = b;

  DB(gp_format1("GP after ~a~%", tag));

  return state;
}

#define MASK_OFF()				\
  if(state)					\
    {						\
      switch(state)				\
	{					\
	case gp_store:				\
	  {					\
	    val = scm_cons(*i,SCM_EOL);		\
	    if(SCM_CONSP(old))			\
	      SCM_SETCDR(old,val);		\
	    old = val;				\
	    break;				\
	  }					\
	case gp_redo:				\
	  old = SCM_CDR(old);			\
	}					\
    }                   

#define MASK_OFF_CI()				\
  if(state)					\
    {						\
      switch(state)				\
	{					\
	case gp_store:				\
	  {					\
	    val = scm_cons(*i,SCM_EOL);		\
	    check_ci_store(*i);                 \
	    if(SCM_CONSP(old))			\
	      SCM_SETCDR(old,val);		\
	    old = val;				\
	    break;				\
	  }					\
	case gp_redo:				\
	  old = SCM_CDR(old);			\
	}					\
    }                   

void vector_state(int state, SCM *old, SCM l, SCM vec);
void unwind_all_in_branch(SCM l); 
void unwind_in_new_branch(SCM p, SCM l, SCM path, SCM lpath);

SCM unwind_hooks = SCM_BOOL_F;
//#define DB(X) X
static void gp_unwind0(SCM *fr, SCM *ci, SCM *si, SCM *cs,  struct gp_stack *gp, SCM path, SCM lpath)
{
  SCM val, old = SCM_EOL;
  SCM *i, *fr_old, *ci_old, *id;   
  int state = 0;
  SCM gp_unbd;

  scm_fluid_set_x(unwind_hooks, SCM_EOL);

  mask_on(gp->id,&gp_unbd,SCM_PACK(GP_MK_FRAME_UNBD(gp_type)));

  DB(printf("unwind> fi %x gp_fi %x\n",fr - gp->gp_frstack, 
	    gp->gp_fr - gp->gp_frstack));

  if(gp->gp_fr < fr)
    {
      return;
      scm_misc_error("gp_unwind","wrong unwind forward in time (fr)",SCM_EOL);
    }

  ci_old = gp->gp_ci;
  fr_old = gp->gp_fr;

  if(si > gp->gp_si        ) si = gp->gp_si;
  if(cs > gp->gp_cs        ) cs = gp->gp_cs;
  if(si < gp->gp_stack     ) si = gp->gp_stack;
  if(cs < gp->gp_cons_stack) cs = gp->gp_cons_stack;
  
  if(ci < gp->gp_cstack + 1)
    scm_misc_error("unwind",
		   "Strange value in ci ~a",
		   scm_list_1(scm_from_ulong(ci - gp->gp_cstack)));

// In order to prevent gc issues we must store the SCM value
#define FF(x) GP_UNREF(NUM2PTR(x))


  for(i = fr_old-1; i >= fr;)
    {
      gp_debug2("fr= %d. i = %d\n",fr - gp->gp_frstack, i - fr);
      // ------------------- Code for stack pointers -----------------
      if(SCM_I_INUMP(*i) || scm_is_false(*i))
	{              
	  gp_debug0("unwind a fr frame ints\n");
	  int j;
	  for(j = 0; j < GP_FRAMESIZE ; j++)
	    {
	      MASK_OFF();
	      i--;
	    }
	  continue;
	}        
      else if(SCM_CONSP(*i))
	{
	  gp_debug0("unwind a frame cmplx\n");

	  state = gp_do_cons_fr(*i, state, &old, gp_unbd);
	  i--;

	  int j;
	  for(j = 0; j < GP_FRAMESIZE - 1 ; j++)
	    {
	      MASK_OFF();
	      i--;
	    }	  
	}
      else
	scm_misc_error("unwind","wrong frame format on stack",SCM_EOL);
    } 

  gp->gp_fr  = fr;

  if(state)
    switch(state)
      {
      case gp_store:
        gp_debug0("gp_store\n");
	if(gp->gp_fr == gp->gp_frstack)
	  {
	    SCM_SETCDR(old,SCM_EOL);
            gp_debug0("return\n");
	    goto out0;
	  }

	if(SCM_CONSP(gp->gp_fr[-1]))
	  {
            gp_debug0("a cons\n");
	    SCM q = SCM_CAR(gp->gp_fr[-1]);
	    
	    switch(SCM_UNPACK(q))
	      {
	      case gp_save_tag:
		SCM_SETCDR(old,gp->gp_fr[-1]);
		gp_debug0("return\n");
		goto out0;
	      case gp_redo_tag:
		SCM_SETCDR(old,SCM_CADR(gp->gp_fr[-1]));
		gp_debug0("return\n");
		goto out0;
	      }	  
	  }
	    
        gp_debug0("store / no cons\n");
	gp->gp_fr[-1] = scm_cons(SCM_PACK(gp_save_tag),gp->gp_fr[-1]);
	SCM_SETCDR(old,gp->gp_fr[-1]);
        gp_debug0("return\n");
	goto out0;

      case gp_redo:
        gp_debug0("gp_redo\n");
	if(gp->gp_fr == gp->gp_frstack)
          {
            gp_debug0("return\n");
            goto out0;
          }
	
	if(SCM_CONSP(gp->gp_fr[-1]))
	  {
	    SCM q = SCM_CAR(gp->gp_fr[-1]);
	    
	    switch(SCM_UNPACK(q))
	      {
	      case gp_save_tag:
		SCM_SETCDR(old,gp->gp_fr[-1]);
		gp_debug0("return\n");
		goto out0;
	      case gp_redo_tag:
		SCM_SETCDR(old,SCM_CADR(gp->gp_fr[-1]));
		gp_debug0("return\n");
		goto out0;
	      }
	  }
	  
        gp_debug0("last linkage\n");
	gp->gp_fr[-1] = scm_cons(SCM_PACK(gp_redo_tag),
				 scm_cons(SCM_CDR(old), gp->gp_fr[-1]));
      }  

 out0:

  DB(printf("unwind> ci %x gp_ci %x\n",ci - gp->gp_cstack, 
	    gp->gp_ci - gp->gp_cstack));

  if(gp->gp_ci < ci)
    {
      return;
      scm_misc_error("gp_unwind","wrong unwind forward in time (ci)",SCM_EOL);
    }

  state = 0;
  old   = SCM_EOL;

  for(i = ci_old-1; i >= ci; i-=1)
    {
      gp_debug1("ci i> %d\n",(int)(i - ci));

      if(i == gp->gp_cstack)
	scm_misc_error("unwind",
		       "unwinding the first protected ci stack element",
		       SCM_EOL);
      
      if(SCM_CONSP(*i)) 
	{
	  state = gp_do_cons(*i, state, &old, gp_unbd);
	  *i = SCM_BOOL_F;
	  continue;
	}

      if(GP(*i) && gp_is_free(i))
	{	 
	  *i = SCM_BOOL_F;
	  continue;
	}

      if(GP(*i) && GP_FRAME_VAR(*i))
	{
	  MASK_OFF_CI();
	  *i = SCM_BOOL_F;
	  continue;
	}

      if(scm_is_false(*i))
	  continue;

      /*
        current_path => '((sk  . ek) ... (s1 . ek))
        path         => '((s1' . ek) ... (sk* . ek*))
  
        We assume that we cons unwind path onto current path
       */
      if(SCM_I_IS_VECTOR(*i))
        {
          SCM l = SCM_SIMPLE_VECTOR_REF(*i,0);
          vector_state(state, &old, l, *i);
          
          if(!SCM_CONSP(path))
            {
              unwind_all_in_branch(l);
            }
          else            
            {
              SCM p = SCM_CAR(path);          
              int found = 0;
              SCM ll = l;
              for(;SCM_CONSP(ll);ll=SCM_CDR(ll))
                {
                  SCM x = gp_variable_ref(SCM_CAR(ll));
                  if(scm_is_eq(x, p))
                    {
                      found = 1;
                      fflush(stdout);
                      unwind_in_new_branch(p, l, SCM_CDR(path), lpath);
                      *i = SCM_BOOL_F;
                      break;
                    }
                }
              if(!found)
                {
                  unwind_all_in_branch(l);
                }
              else
                break;
            }

          *i = SCM_BOOL_F;
          continue;
        }

      if(!GP(*i))
        {
          // ------------- Rest case, just keep (handlers) sloppy version
          //

	  if(SCM_VARIABLEP(*i) || SCM_NULLP(*i))
	    {
	      // HANDLERS
	      MASK_OFF_CI();
	      *i = SCM_BOOL_F;
	      continue; 
	    }

          //--------------------------------------------------
	  scm_misc_error
            (
             "in unwinding",
             "should have a GP value or CONS in unwinding of cstack ~a",
	     scm_list_1(*i)
	     );
	}
      
      // Base case, prolog variables
      if(!GP(*i))
	{
	  scm_misc_error("gp-unwind",
			 "got non handled object in ci stack ~%~a~%",
			 scm_list_1(*i));
	}

      id  = GP_GETREF(*i);      
      {
	SCM *f = GP_GETREF(*i);
	scm_t_bits head = SCM_UNPACK(f[0]);
	if(!GP_GC_ISMARKED(head) && GP_GC_ISCAND(head))
	  {	    
	    *i = SCM_BOOL_F;
	    continue;
	  }
      }

      if(state)
	{
	  switch(state)
	    {
	    case gp_store:
	      {
		val = scm_cons(scm_cons(*i, scm_cons(SCM_I_MAKINUM(id[0]),
						     id[1])),
			       SCM_EOL);
		if(SCM_CONSP(old))
		  SCM_SETCDR(old,val);
		old = val;
		break;
	      }
	    case gp_redo:
	      old = SCM_CDR(old);
	    }
	}

      id[0] = gp_unbd;      
      id[1] = SCM_UNBOUND;

      *i = SCM_BOOL_F;
    }

  gp_debug0("finished looping\n");
  gp_debug2("i = %x ci = %x\n",i - gp->gp_cstack, ci - gp->gp_cstack);

  //We delay this because we can have reentrance and we should be able
  //To use the stack from there
  gp->gp_ci  = ci;


  //si part
  int si_store = 0;

  gp_debug0("si handling\n");
  for(i = gp->gp_si - 1; i >= si; i--)
    {
      gp_debug2("si iter %x %x\n",i - si, i - gp->gp_stack);

      if(si_store)
        {
          gp_debug0("store\n");
	  *i = SCM_BOOL_F;
        }
      else          
        {
	  if(GP(*i))
	    {
	      if(GP_GC_ISQAND(SCM_UNPACK(GP_GETREF(*i)[0])))
		{
		  *i = SCM_BOOL_F;
		}
	      else
		{
		  GP_GETREF(*i)[1] = SCM_UNBOUND;
		}
	    }
          else if(scm_is_eq(*i,SCM_BOOL_T))
            {
              *i = SCM_BOOL_F;
              si_store = 1;
              continue;
            }          
        }          
    }

  gp_debug1("finishing si %x\n",gp->gp_si - si);
    
  if(si_store)
    {
      *si = SCM_BOOL_T;
      gp->gp_si = si + 1;
    }
  else
    gp->gp_si = si;

  //cs part
  int cs_store = 0;

  gp_debug0("cs handling\n");
  for(i = gp->gp_cs - 1; i >= cs; i--)
    {
      gp_debug2("cs iter %x %x\n",i - cs, i - gp->gp_cons_stack);

      if(cs_store)
        {
          gp_debug0("store\n");
	  *i = SCM_BOOL_F;
        }
      else          
        {
          if(scm_is_eq(*i,SCM_BOOL_T))
            {
              *i = SCM_BOOL_F;
              cs_store = 1;
              continue;
            } else if(GP(*i))
	    {	      
	      GP_GETREF(*i)[1] = SCM_UNBOUND;
	      GP_GETREF(*i)[2] = SCM_UNBOUND;	      
	    }
        }      
    }

  gp_debug1("finishing cs %x\n",gp->gp_cs - cs);
    
  if(cs_store)
    {
      *cs = SCM_BOOL_T;
      gp->gp_cs = cs + 1;
    }
  else
    gp->gp_cs = cs;
  
  gp_debug1("last routines %x\n",gp->gp_ci - gp->gp_cstack);
  if(state)
    switch(state)
      {
      case gp_store:
        gp_debug0("gp_store\n");
	if(gp->gp_ci == gp->gp_cstack)
	  {
	    SCM_SETCDR(old,SCM_EOL);
            gp_debug0("return\n");
	    goto out;
	  }

	if(SCM_CONSP(gp->gp_ci[-1]))
	  {
            gp_debug0("a cons\n");
	    SCM q = SCM_CAR(gp->gp_ci[-1]);
	    if(SCM_I_INUMP(q))
	      {
		switch(SCM_UNPACK(q))
		  {
		  case gp_save_tag:
		    SCM_SETCDR(old,gp->gp_ci[-1]);
                    gp_debug0("return\n");
		    goto out;
		  case gp_redo_tag:
		    SCM_SETCDR(old,SCM_CADR(gp->gp_ci[-1]));
                    gp_debug0("return\n");
		    goto out;
		  }
	      }
	  }
	    
        gp_debug0("store / no cons\n");
	gp->gp_ci[-1] = scm_cons(SCM_PACK(gp_save_tag),gp->gp_ci[-1]);
	SCM_SETCDR(old,gp->gp_ci[-1]);
        gp_debug0("return\n");
	goto out;

      case gp_redo:
        gp_debug0("gp_redo\n");
	if(gp->gp_ci == gp->gp_cstack)
          {
            gp_debug0("return\n");
            goto out;
          }
	
	if(SCM_CONSP(gp->gp_ci[-1]))
	  {
	    SCM q = SCM_CAR(gp->gp_ci[-1]);
	    if(SCM_I_INUMP(q))
	      {
		switch(SCM_UNPACK(q))
		  {
		  case gp_save_tag:
		    SCM_SETCDR(old,gp->gp_ci[-1]);
                    gp_debug0("return\n");
		    goto out;
		  case gp_redo_tag:
		    SCM_SETCDR(old,SCM_CADR(gp->gp_ci[-1]));
                    gp_debug0("return\n");
		    goto out;
		  }
	      }
	  }
	  
        gp_debug0("last linkage\n");
	gp->gp_ci[-1] = scm_cons(SCM_PACK(gp_redo_tag),
				 scm_cons(SCM_CDR(old), gp->gp_ci[-1]));
      }  

 out:
  {
    SCM l = scm_fluid_ref(unwind_hooks);
    if(!SCM_NULLP(l))
      {
	l = scm_reverse(l);
	while(!SCM_NULLP(l))
	  {
	    scm_call_0(SCM_CAR(l));
	    l = SCM_CDR(l);
	  }
      }  
  }
}
//#define DB(X)

void vector_state(int state, SCM *old, SCM l, SCM vec)
{
   if(state)
    {
      switch(state)
        {
        case gp_store:
          {
            SCM xx = scm_c_make_vector(2, SCM_BOOL_F);
            SCM u = SCM_EOL;
            for(; SCM_CONSP(l); l = SCM_CDR(l))
              {
                SCM x = SCM_CAR(l);
                if(GP(x) && GP_UNBOUND(GP_GETREF(x)))
                  {
                    u = scm_cons(SCM_BOOL_F, u);
                    continue;
                  }
                if(GP(x))
                  x = GP_GETREF(x)[1];
                else
                  scm_misc_error
                    ("gp-unwind",
                     "element in engine datalist not a gp variable - ~a~%",
                     scm_list_1(x));
                if(SCM_CONSP(x))
                  {
                    SCM s = SCM_CAR(x);
                    SCM e = SCM_CDR(x);

                    SCM olde = scm_fluid_ref(gp_current_stack);
                    scm_fluid_set_x(gp_current_stack, e);
                    gp_newframe(s);
                    SCM r    = scm_cons(gp_store_state(get_gp(),0),
                                        scm_cons(e , s));
                    scm_fluid_set_x(gp_current_stack, olde);

                    u = scm_cons(r, u);
                  }
                else
                  scm_misc_error("gp-unwind","s . engine not a cons - ~a~%",
                                 scm_list_1(x));
              }

            scm_c_vector_set_x(xx, 0, vec);
            scm_c_vector_set_x(xx, 1, u  );
            SCM val = scm_cons(xx,SCM_EOL);	    
            if(SCM_CONSP(*old))
              SCM_SETCDR(*old,val);
            *old = val;
            break;
          }
        case gp_redo:
          *old = SCM_CDR(*old);
        }
    }
}

void unwind_all_in_branch(SCM l)
{
  { // Unwind the different branches
        
    SCM old_engine = scm_fluid_ref(gp_current_stack);

    for(; SCM_CONSP(l); l = SCM_CDR(l))
      {
        SCM x          = gp_variable_ref(SCM_CAR(l));
        if(SCM_CONSP(x))
          {
            SCM new_engine = SCM_CDR(x);
            scm_fluid_set_x(gp_current_stack, new_engine);
            gp_clear(SCM_BOOL_F);      
          }
      }
    

    scm_fluid_set_x(gp_current_stack, old_engine);
  }
}

static inline void gp_unwind_(SCM s   , int ncons, int nvar, int nci, 
                              SCM path, SCM lpath);

void unwind_in_new_branch(SCM p, SCM l, SCM path, SCM lpath)
{
  SCM engine = SCM_CDR(p);

  //printf("in new engine unwind: e = %p\n", (void *) SCM_UNPACK(engine));
  
  scm_fluid_set_x(gp_current_stack, engine);

  gp_unwind_(SCM_CAR(p), 0, 0, 0, path, lpath);  
}

  /*
    path = [bk, ..., a0, x, ...]
    l    = [ak, ..., a0, x, ...]
   */
static inline void gp_unwind_(SCM s   , int ncons, int nvar, int nci, 
                              SCM path, SCM lpath)
{
  SCM lt = SCM_EOL;
  SCM spath = gp_store_path;
  SCM paths = gp_paths;

  if(scm_is_false(lpath))
    {
      lt  = gp_gp_cdr(s,s);
      if(SCM_CONSP(lt))
        {
          SCM ltpaths  = SCM_CDR(lt);
          if(SCM_CONSP(ltpaths))
            {
              path   = SCM_CAR(ltpaths);
              spath  = SCM_CDR(ltpaths);
            }
          else
            {
              path  = gp_engine_path,
              spath = gp_store_path;
            }
        }
      else
        {
          path  = gp_engine_path;
          spath = gp_store_path;
        }

      if(SCM_CONSP(path))
        {
        SCM l = gp_engine_path;
        SCM b = SCM_EOL;

        int na = scm_to_int(scm_length(l));
        int nb = scm_to_int(scm_length(path));

        while(na > nb)
          {
            if(SCM_CONSP(gp_store_path))
              {
                SCM ls = SCM_CAR(gp_store_path);
                unwind_all_in_branch(ls);
                gp_store_path = SCM_CDR(gp_store_path);
                l = SCM_CDR(l);
                na--;
              }
            else
              scm_misc_error("gp-unwind","gp_store_path of wrong length",
                             SCM_EOL);
          }

        while(nb > na)
          {
            b = scm_cons(SCM_CAR(path), b);
            path = SCM_CDR(path);
            nb--;
          }
        
        while(SCM_CONSP(l) && !scm_is_eq(SCM_CAR(l), SCM_CAR(path)))
          {
            if(SCM_CONSP(gp_store_path))
              {
                SCM ls = SCM_CAR(gp_store_path);
                unwind_all_in_branch(ls);
                gp_store_path = SCM_CDR(gp_store_path);
                l = SCM_CDR(l);
                na--;
              }
            else
              scm_misc_error("gp-unwind","gp_store_path of wrong length",
                             SCM_EOL);

            b = scm_cons(SCM_CAR(path), b);
            path = SCM_CDR(path);
          }

        gp_engine_path = l;
        scm_fluid_set_x(gp_current_stack, SCM_CDAR(l));
        
        path = b;
        }
    }

  if(SCM_CONSP(path))
    {
      if(scm_is_false(lpath))
        {
          lpath = scm_list_4(s,
                             scm_from_int(ncons), 
                             scm_from_int(nvar), 
                             scm_from_int(nci));
        }

      s = SCM_CAAR(path);

      ncons = 0;
      nvar  = 0;
      nci   = 0;
    }
  else
    {
      if(scm_is_true(lpath))
        {
          
          s = SCM_CAR(lpath);

          paths = SCM_CDR(gp_gp_cdr(s,s));
          spath = SCM_CDR(paths);
          gp_engine_path = SCM_CAR(paths);
          gp_store_path  = spath;
          gp_paths       = paths;
          scm_fluid_set_x(gp_current_stack, SCM_CDAR(gp_engine_path));
          
          lpath = SCM_CDR(lpath);
          ncons = scm_to_int(SCM_CAR(lpath));
          lpath = SCM_CDR(lpath);
          nvar  = scm_to_int(SCM_CAR(lpath));
          lpath = SCM_CDR(lpath);
          nci   = scm_to_int(SCM_CAR(lpath));
        }
    }
        
    
  struct gp_stack *gp = get_gp();
  SCM *fr, *ci,*si,*cs;
  scm_t_bits dyn_n;
  SCM ha, tag = SCM_EOL;

  if(GP_CONSP(s) || SCM_CONSP(s))
    {
      SCM cars = gp_car(s, s);
      if(GP_CONSP(cars) || SCM_CONSP(cars))
        {
          tag = gp_car(cars,s);
          lt  = gp_gp_cdr(s,s);
          if(SCM_CONSP(lt))
            lt = SCM_CAR(lt);
          
          fr  = gp->gp_frstack + GP_GET_SELF_TAG(tag);
          if(vlist_p(lt))
            {
              vhash_truncate_x(lt);      
            }
        }
      else
        {
          fr = gp->gp_fr;
        }
    }
  else
    {
      fr = gp->gp_fr;
    }


  
  ha    = GP_GET_HANDLERS(fr);
  dyn_n = GP_GET_DLENGTH(fr);
  
  if(GP_VAL_UNBOUND(fr))
    {
      scm_misc_error("unwind","got not a number at fr1 ~a fr2 ~a fr4 ~a",
		     scm_list_3(fr[-1],fr[-2],fr[-4]));
    }
  ci    = gp->gp_cstack + GP_GET_VAL_VAL(fr);

  if(!SCM_CONSP(fr[-1]) && scm_is_true(fr[-1]) && 
     !scm_is_eq(*ci,GP_GET_VAL(fr)))
    {
      SCM u = gp_gp_cdr(gp_car(s , s), s);
      SCM v = GP_GETREF(u)[1];
      gp_print_stack(s,SCM_BOOL_T);
      scm_misc_error("gp-unwind",
		     "ci self 1 entry is not the same~%~a at ~a,~a,~a~%",
		     scm_list_4(*ci, 
				scm_from_ulong(gp->gp_ci - gp->gp_cstack),
				scm_from_ulong(ci - gp->gp_cstack),
				v));
    }

  ci += 1 - nci;

  si    = gp->gp_stack      + GP_GET_VAR(fr)  - nvar;
  cs    = gp->gp_cons_stack + GP_GET_CONS(fr) - ncons;
  
  gp_debug2("cs> %x %x\n",cs - gp->gp_stack,cs - gp->gp_cons_stack);
  gp_debug2("si> %x %x\n",si - gp->gp_stack,si - gp->gp_cons_stack);
  
  gp->handlers = ha;
  
  gp_unwind0(fr - GP_FRAMESIZE*nci,ci, si, cs, gp, path, lpath);

  gp_unwind_dynstack(gp, dyn_n);
  
  gp->handlers = ha;

  gp_debug0("leaving unwind\n");
}

static void inline falsify_entries(SCM *ci,struct gp_stack *gp)
{
  return;
  SCM *i = gp->gp_ci - 1;
  int action = 1;
  for(;i >= ci; i--)
    {
      if(GP(*i) && GP_FRAME_VAR(*i) && action)
	*i = SCM_BOOL_F;
      if(SCM_CONSP(*i))
	{
	  SCM tag = SCM_CAR(*i);
	  if(SCM_I_INUMP(*i) && SCM_UNPACK(tag) == gp_redo_tag)
	    {
	      action = 1;
	    }
	  else
	    action = 0;
	}
    }
}

// Use this functionto prune the control stack and also perhaps clear
// The stacks completely.
// Todo make this work for sub engines currently we do nothing between engines
static inline void gp_prune(SCM s, int tailp)
{
  struct gp_stack *gp = get_gp();
  SCM *fr, *ci,*si,*cs,lt;
  SCM tag = SCM_EOL;

  if(GP_CONSP(s) || SCM_CONSP(s))
    {
      tag = gp_car(gp_car(s, s),s);
      lt  = gp_gp_cdr(s,s);
      if(SCM_CONSP(lt))
        {
          SCM pth = SCM_CDR(lt);
          lt = SCM_CAR(lt);
          if(!(SCM_CONSP(gp_engine_path) && SCM_CONSP(pth) &&
               scm_is_eq(SCM_CAR(pth), SCM_CAR(gp_engine_path))))
            return; // Between engines we do nothing
        }
      fr  = gp->gp_frstack + GP_GET_SELF_TAG(tag);
      if(vlist_p(lt))
	{
	  vhash_truncate_x(lt);      
	}
    }
  else
    {
      return;
    }

  if(fr > gp->gp_fr) return;

  ci    = gp->gp_cstack     + GP_GET_VAL_VAL(fr);

  int failci_p = 0;
  if(!scm_is_eq(*ci,GP_GET_VAL(fr)))
    failci_p = 1;

  ci +=  + 1;

  si    = gp->gp_stack      + GP_GET_VAR(fr);
  cs    = gp->gp_cons_stack + GP_GET_CONS(fr);


  int vp = 0;
  if(ci == gp->gp_ci && tailp && !failci_p)
    {
      ci--;
      vp = 1;
    }
  else
    {
      if(!failci_p)
	falsify_entries(ci,gp);
      ci = gp->gp_ci;
    }

  if(0 && si == gp->gp_si && vp && tailp)
    {
      si-=2;
    }
  else
    {
      si = gp->gp_si;
    }

  if(0 && cs == gp->gp_cs && tailp)
    {
      cs -= 2;
    }
  else
    cs = gp->gp_cs;

  gp_debug2("2 cs> %x %x\n",cs - gp->gp_stack,cs - gp->gp_cons_stack);
  gp_debug2("2 si> %x %x\n",si - gp->gp_stack,si - gp->gp_cons_stack);


  gp_unwind0(fr - (tailp?GP_FRAMESIZE:0),ci, si, cs, gp, 
             SCM_BOOL_F, SCM_BOOL_F);
}


static inline void gp_unwind(SCM fr)
{
  gp_unwind_(fr,0,0,0, SCM_EOL, SCM_BOOL_F);
}

static inline void gp_unwind_soft(int ncons)
{
  struct gp_stack *gp = get_gp();    
  gp->gp_cs -= ncons;
}

static inline void gp_unwind_ncons(SCM fr, int ncons)
{
  gp_unwind_(fr,-ncons,0,0, SCM_EOL, SCM_BOOL_F);
}

static inline void gp_unwind_tail(SCM fr)
{
  gp_unwind_(fr,2,2,1, SCM_EOL, SCM_BOOL_F);
  //gp_unwind_(fr,0,0,0, SCM_EOL, SCM_BOOL_F););
}

SCM_DEFINE(gp_gp_unwind, "gp-unwind", 1, 0, 0, (SCM fr), 
	   "unwinds the prolog stack till frame refered by the argument")
#define FUNC_NAME s_gp_gp_unwind
{
  gp_no_gc();
  gp_unwind(fr);
  gp_do_gc();
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_prune, "gp-prune", 1, 0, 0, (SCM fr), 
	   "unwinds the prolog stack till frame refered by the argument")
#define FUNC_NAME s_gp_gp_unwind
{
  gp_no_gc();
  gp_prune(fr, 0);
  gp_do_gc();
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_prune_tail, "gp-prune-tail", 1, 0, 0, (SCM fr), 
	   "unwinds the prolog stack till frame refered by the argument")
#define FUNC_NAME s_gp_gp_unwind
{
  gp_no_gc();
  gp_prune(fr, 1);
  gp_do_gc();
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_unwind_tail, "gp-unwind-tail", 1, 0, 0, (SCM fr), 
	   "unwinds the prolog stack till frame refered by the argument")
#define FUNC_NAME s_gp_gp_unwind_tail
{
  gp_no_gc();
  gp_unwind_tail(fr);
  gp_do_gc();
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_unwind_ncons, "gp-unwind-ncons", 1, 0, 0, (SCM fr, int ncons), 
	   "unwinds the prolog stack till frame refered by the argument")
#define FUNC_NAME s_gp_gp_unwind_tail
{
  gp_no_gc();
  gp_unwind_ncons(fr, ncons);
  gp_do_gc();
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_unwind_soft, "gp-unwind-soft", 1, 0, 0, (int ncons), 
	   "unwinds the prolog stack till frame refered by the argument")
#define FUNC_NAME s_gp_gp_unwind_tail
{
  //gp_no_gc();
  gp_unwind_soft(ncons);
  //gp_do_gc();
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_get_stack, "gp-get-stack", 1, 0, 0, (SCM s), 
	   "yields the stack as a list")
#define FUNC_NAME s_gp_get_stack
{
  struct gp_stack *gp = get_gp();
  SCM* i;
  SCM ret = SCM_EOL;
  gp_no_gc();
  for(i = gp->gp_cstack + 1; i < gp->gp_ci; i++)
    {
      ret = scm_cons(*i,ret);
    }
  gp_do_gc();
  return ret;
}
#undef FUNC_NAME

//#define DB(X) X
static inline SCM gp_store_state(struct gp_stack *gp, int allp)
{
  SCM head, data;
  
  //Mark the cons stack to be stored (#t)
  if(gp->gp_cs > gp->gp_cons_stack && gp->gp_cs[-1] != SCM_BOOL_T)
    {
      gp->gp_cs[0] = SCM_BOOL_T;
      gp->gp_cs ++;
    }

  if(gp->gp_si > gp->gp_stack && gp->gp_si[-1] != SCM_BOOL_T)
    {
      gp->gp_si[0] = SCM_BOOL_T;
      gp->gp_si ++;
    }
  
  data = gp->gp_ci[-1];
  
  head = SCM_EOL;  
  if(SCM_CONSP(data) && SCM_I_INUMP(SCM_CAR(data)))
    {
      gp_debug1("store state, got tag %x\n",SCM_UNPACK(SCM_CAR(data)));
      switch(SCM_UNPACK(SCM_CAR(data)))
	{
	case gp_save_tag:
          gp_debug0("save-tag\n");
	  head = data;
	  break;
	case gp_redo_tag:
          gp_debug0("redo-tag\n");
	  head = SCM_CADR(data);
          break;
	}
    }
  else
    {
      head = scm_cons(SCM_PACK(gp_save_tag),data);
      gp->gp_ci[-1] = head;
    }

  data = gp->gp_fr[-1];
  
  SCM headfr = SCM_EOL;  
  if(SCM_CONSP(data) && SCM_I_INUMP(SCM_CAR(data)))
    {
      gp_debug1("store state, got tag %x\n",SCM_UNPACK(SCM_CAR(data)));
      switch(SCM_UNPACK(SCM_CAR(data)))
	{
	case gp_save_tag:
          gp_debug0("save-tag\n");
	  headfr = data;
	  break;
	case gp_redo_tag:
          gp_debug0("redo-tag\n");
	  headfr = SCM_CADR(data);
          break;
	}
    }
  else
    {
      headfr = scm_cons(SCM_PACK(gp_save_tag),data);
      gp->gp_fr[-1] = headfr;
    }

  gp_debug0("store state, to dynwind\n");


  gp_debug0("store state, to yield data\n");  
  {
    SCM  vnew_ = scm_c_make_vector(NSTATE, SCM_BOOL_F);
    SCM *vnew  = SCM_I_VECTOR_WELTS(vnew_);

    vnew[STATE_LOGICAL]   = SCM_I_MAKINUM(gp->_logical_);
    vnew[STATE_DYNSTACK]  = gp->dynstack;
    vnew[STATE_DYNLENGTH] = SCM_PACK(gp->dynstack_length);
    vnew[STATE_RGUARDS]   = make_rguards(gp->rguards,allp);
    vnew[STATE_N]         = SCM_I_MAKINUM(gp->gp_fr - gp->gp_frstack);    
    vnew[STATE_PATH]      = head;
    vnew[CONTROL_PATH]    = headfr;
    vnew[HANDLERS]        = gp->handlers;
    
    return vnew_;
  }
}
//#define DB(X)
SCM gp_store_(SCM s, int allp);

SCM_DEFINE(gp_gp_store_state, "gp-store-state", 1, 0, 0, (SCM s), 
	   "store a continuation point at the current state")
#define FUNC_NAME s_gp_gp_store_state
{
  return gp_store_(s,0);
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_store_state_all, "gp-store-state-all", 1, 0, 0, (SCM s), 
	   "store a continuation point at the current state")
#define FUNC_NAME s_gp_gp_store_state_all
{
  return gp_store_(s,1);
}
#undef FUNC_NAME

//#define DB(X) X
SCM gp_store_(SCM s, int allp)
{
  SCM ret;
  // We must make sure that we guard the state if the vlist
  // Else trunction will make it fail.
  gp_no_gc();
  
  if(GP_CONSP(s))
    {
      SCM l = GP_GETREF(s)[2];
      if(vlist_p(l))
	vlist_incref(l);
    }

  gp_new_state_token();

  ret = scm_cons(gp_store_state(get_gp(),allp),
                 scm_cons(scm_fluid_ref(gp_current_stack) , s));

  
  SCM lpath  = gp_store_path;
  SCM llpath = SCM_EOL;
  SCM k      = gp_engine_path;
  int first  = 1;

  
  for(;SCM_CONSP(lpath); lpath=SCM_CDR(lpath))
    {
      SCM l    = SCM_CAR(lpath);
      SCM ll   = SCM_EOL;
      
      SCM tag = SCM_CAR(k);

      k = SCM_CDR(k);
      
      for(;SCM_CONSP(l);l = SCM_CDR(l))
        {
          SCM x = SCM_CAR(l);

          if(GP(x) && GP_UNBOUND(GP_GETREF(x)))
            ll = scm_cons(SCM_BOOL_F, ll);
          else
            {
              SCM xx   = GP_GETREF(x)[1];

              if(first && scm_is_eq(xx, tag))
                {
                  first = 0;
                  ll    = scm_cons(ret, ll);
                  continue;
                }
              
              SCM e    = SCM_CDR(xx);
              SCM s    = SCM_CAR(xx);
              SCM olde = scm_fluid_ref(gp_current_stack);
              scm_fluid_set_x(gp_current_stack, e);
              gp_newframe(s);
              SCM r    = scm_cons(gp_store_state(get_gp(),0),
                                  scm_cons(e , s));
              scm_fluid_set_x(gp_current_stack, olde);
              
              ll  = scm_cons(r, ll);              
            }
        }
      
      SCM vecout = scm_reverse(ll);
      llpath = scm_cons(vecout, llpath);
    }

  gp_do_gc();

  
  return scm_cons(gp_paths,
                  scm_cons(ret, llpath));    
}


int check_pt_fr(SCM pt,SCM *fr)
{
  if(SCM_CONSP(*fr))
    {
      scm_t_bits tag = SCM_UNPACK(SCM_CAR(*fr));
      if(tag == gp_save_tag)
	{
	  if(scm_is_eq(*fr, pt))
	    {
	      return 1;
	    }
	} else
	{
	  if(scm_is_eq(pt, SCM_CADR(*fr)))
	    {
	      return 1;
	    }
	}
    }
  return 0;
}
  
static inline SCM * gp_get_branch(SCM *p, SCM *fr, struct gp_stack *gp)
{
  SCM pt = *p;
  while(fr > gp->gp_frstack + GP_FRAMESIZE)
    {
      gp_debug1("Try find a new taged frame (%d)\n",fr - gp->gp_frstack);
      for(  ; fr > gp->gp_frstack + GP_FRAMESIZE && SCM_I_INUMP(*fr) 
	    ; fr -= GP_FRAMESIZE)
	{
	  int i = 0;
	  for(i=0;i<GP_FRAMESIZE;i++)
	    {
	      if(SCM_CONSP(pt))
		pt = SCM_CDR(pt);
	      else		
		{
		  scm_misc_error("gp-unwind/get_branch",
				 "reched eol prematurely  1 ~a ~a ~a",
				 scm_list_3(scm_from_int(i),fr[0],fr[1]));
		  return fr;
		}
	    }
	}

      gp_debug0("Got a new taged frame\n");
      //These two checks can be disable when everything is debugged
      if(fr < gp->gp_fr - 1 && check_pt_fr(pt,fr + 1))
	{
	  scm_misc_error("rewind","get branch of by fr + 1",SCM_EOL);
	}

      if(fr > gp->gp_frstack && check_pt_fr(pt,fr - 1))
	{
	  scm_misc_error("rewind","get branch of by fr - 1",SCM_EOL);
	}

      if(check_pt_fr(pt,fr))
	{
	  *p = pt;
	  return fr;
	}
      
      if(fr > gp->gp_frstack + GP_FRAMESIZE)
	{
	  int i = 0;
	  for(i=0;i<GP_FRAMESIZE;i++)
	    {
	      if(SCM_CONSP(pt))
		pt = SCM_CDR(pt);
	      else
		{
		  scm_misc_error("gp-unwind/get_branch",
				 "reched eol prematurely 2 ~a",
				 scm_list_1(scm_from_int(i)));
		  return fr;
		}
	    }
	  fr -= GP_FRAMESIZE;
	}
      else
	{
	  gp_format2("fr ground: fr = ~a; pt = ~a~%",
		     scm_from_uint(fr - gp->gp_frstack),
		     pt);
	}
    }

  *p = pt;
  return fr;
}
//#define DB(X) X

/*
  We start with the tree edge and go against the root, when we 
  find the root we do the actual rewinding.
 */
//#define DB(X) X
static int gp_rewind(SCM pp, SCM pend, struct gp_stack *gp, SCM K)
{
  SCM *id,q,stack[51];
  int sp;

  gp_debug0("entering a rewind frame\n");

  if( pp  == SCM_EOL) return 0;
  if(pend == pp)      return 0;

  sp = 0;
  while(1)
    {
      if(pend == pp)
	{
	  break;
	}
      if(pp == SCM_EOL)
	scm_misc_error("gp_rewind","eol encounterd too early",SCM_EOL);

      stack[sp++] = pp;
      
      if(sp >= 50)
	{
          gp_debug0("A new rewind frame\n");
	  gp_rewind(SCM_CDR(pp), pend, gp, K);
	  break;
	}
      
      if(SCM_CONSP(pp))
	pp = SCM_CDR(pp);
      else
	scm_misc_error("restore_state/rewind_ci",
		       "not an eol or cons ~a",
		       scm_list_1(pp));
    }
  
  gp_debug0("entering a do rewind frame\n");

  if(gp->gp_ci == gp->gp_cstack + 1)
    gp->gp_ci[-1] = pend;
  
  sp--;
  while(sp >= 0)
    {
      gp_debug1("iter> ci = %p\n", gp->gp_ci - gp->gp_cstack);
      pp = stack[sp];
      
      q  = SCM_CAR(pp);

      if(GP(q) && GP_FRAME_VAR(q))
	{
	  gp->gp_ci[0] = q;
	  gp->gp_ci++;
	  GP_GETREF(q)[1] = scm_from_ulong(gp->gp_ci - gp->gp_cstack - 1);
	  sp--;
	  continue;
	}
      else if(SCM_CONSP(q))
        {
          gp_debug0("consp\n");	  
	  if(GP(SCM_CAR(q)))
            {
	      gp_debug0("gp\n");
	      id = GP_GETREF(SCM_CAR(q));
	      q  = SCM_CDR(q);
	      
	      gp_store_var_2(id,1,gp);
	      
	      id[0] = SCM_PACK(SCM_I_INUM(SCM_CAR(q)));
	      id[1] = SCM_CDR(q);      
            }
          else
	    {
	      gp_debug0("call\n");
	      SCM cdr = SCM_CDR(q);
	      if(scm_is_false(cdr))
		scm_call_1(SCM_CAR(q),SCM_BOOL_T);
	      else if(scm_is_true(scm_procedure_p(cdr)))
		scm_call_0(SCM_CAR(q));
	      
	      gp->gp_ci[0] = q;
	      gp->gp_ci ++;
	    }
	  
	  sp--;
	  continue;
	}
      else if(scm_is_eq(q, SCM_BOOL_F) || SCM_VARIABLEP(q))
        {
	  gp->gp_ci[0] = q;
	  gp->gp_ci ++;	      
	  sp--;
	  continue;
        }
      else if(SCM_I_IS_VECTOR(q))
        {
          
          gp->gp_ci[0] = scm_c_vector_ref(q,0);
          gp->gp_ci ++;	      
	  sp--;

          SCM l = scm_c_vector_ref(q,1);
          for(;SCM_CONSP(l);l = SCM_CDR(l))
            {
              SCM dcont = SCM_CAR(l);
              if(scm_is_false(dcont))
                continue;
              SCM olde  = scm_fluid_ref(gp_current_stack);
              scm_fluid_set_x(gp_current_stack, SCM_CADR(dcont));
              struct gp_stack *gp2 = get_gp();
              gp_restore_state(SCM_CAR(dcont), gp2, K);
              scm_fluid_set_x(gp_current_stack, olde);              
            }
        }
      else
        scm_misc_error("restore-state/ci rewinding",
                       "Got unhandle object ci -> ~%~a",
                       scm_list_1(q));
    }
  gp_debug1("finish> ci = %p\n", gp->gp_ci - gp->gp_cstack);
  gp_debug0("leaving a rewind frame\n");
  return 1;
}

static int gp_rewind_fr(SCM pp, SCM pend, struct gp_stack *gp)
{
  SCM q,stack[51 + GP_FRAMESIZE];
  int sp;

  gp_debug0("entering a rewind frame\n");

  if( pp  == SCM_EOL) return 0;
  if(pend == pp)      return 0;
  
  sp = 0;
  while(1)
    {
      if(scm_is_eq(pend,pp))
	{
	  break;
	}
      if(!SCM_CONSP(pp))
	scm_misc_error("gp_rewind","eol encounterd too early ~a",
		       scm_list_1(pp));

      int i;
      for(i = 0; i < GP_FRAMESIZE; i++)
	{
	  stack[sp++] = pp;
	  if(SCM_CONSP(pp))
	    pp = SCM_CDR(pp);
	  else
	    scm_misc_error("gp-rewind", "premature eol att pos ~a",
			   scm_list_1(scm_from_int(i)));
	}
      
      if(sp >= 50)
	{
          gp_debug0("A new rewind frame\n");
	  gp_rewind_fr(pp, pend, gp);
	  break;
	}
    }
  
  gp_debug0("entering a do rewind frame\n");
  
  if(gp->gp_fr == gp->gp_frstack + GP_FRAMESIZE)
    {
      gp->gp_fr[-1] = pp;
    }

  sp--;
  while(sp >= 0)
    {
      gp_debug1("iter> fr = %p\n", gp->gp_fr - gp->gp_cstack);
      pp = stack[sp];      
      
      q  = SCM_CAR(pp);
      int i = 0;
      for(i = 0; i < GP_FRAMESIZE; i++, sp--)
	{
	  gp->gp_fr[i] = SCM_CAR(stack[sp]);
	}
      
      gp->gp_fr += GP_FRAMESIZE;
      int ch = GP_GET_CHOICE(gp->gp_fr);
      GP_SET_VAR(gp->gp_fr, gp->gp_si, ch, gp);
      GP_SET_CONS(gp->gp_fr, gp->gp_cs, gp);

      set_self(gp);

      if(scm_is_true(q))
	{
	  set_self(gp);
	}    
    }
  gp_debug1("finish> fr = %p\n", gp->gp_fr - gp->gp_cstack);
  gp_debug0("leaving a rewind frame\n");
  return 1;
}

//#define DB(X)

void POP(SCM * pp){
  if(SCM_CONSP(*pp))
    {
      *pp = SCM_CDR(*pp);
      return;
    }
  scm_misc_error("get_si_cs","cannot unwind stored pp list",SCM_EOL);
}

#define GP_MIN(x,y) (((x) < (y))? (x) : (y))
#define GP_MAX(x,y) (((x) > (y))? (x) : (y))

SCM gp_scan_tail(SCM path, SCM *ci, struct gp_stack *gp)
{
  SCM pt = path;
  int error = 0;
  SCM x;
  int begining = 0;
  if(SCM_CONSP(ci[-1]))
    {
      SCM tag = SCM_CAR(ci[-1]);
      if(SCM_I_INUMP(tag))
	{
	  if(SCM_UNPACK(tag) == gp_redo_tag)
	    x = SCM_CADR(ci[-1]);
	  else
	    x = ci[-1];
	}
      else if(ci != gp->gp_cstack + 1)
	error = 1;
      else
	begining = 1;
    }
  else
    {
      if(ci == gp->gp_cstack + 1)
	begining = 1;
      else
	error = 2;
    }

  if(error)
    {
      scm_misc_error("restore-state/scan_tail","ERROR TAG code ~a",
		     scm_list_1(scm_from_int(error)));
    }
  else
    {
      SCM parent = pt;
      while(1)
	{
	  if(SCM_NULLP(pt))
	    return pt;
	  
	  if(!begining)	    
	    if(scm_is_eq(pt, x))
	      return pt;
      

	  if(SCM_CONSP(pt))
	    {
	      parent = pt;
	      pt = SCM_CDR(pt);
	    }
	  else
	    {
	      if(begining)
		return parent;
	      else
		scm_misc_error("restore-state/scan-ci",
			       "Cant find stop tag and not a complete state",
			       SCM_EOL);
	    }
	}
    }
}

void check(char * str, SCM* fr, SCM pp_x, SCM path)
{
  int fail = 0;
  SCM x  = fr[-1];
  if(SCM_CONSP(x))
    {
      if(SCM_UNPACK(SCM_CAR(x)) == gp_save_tag)
	{
          if(pp_x != x)
	    fail = 1;
	}
      else if(SCM_UNPACK(SCM_CAR(x)) == gp_redo_tag)
	{
	  if(pp_x != SCM_CADR(x))
	    fail = 2;
	}    
      else if(!SCM_CONSP(SCM_CAR(pp_x)))
	{
	  scm_misc_error("gp_restore_state","branch point is wrong ~a"
			 , scm_list_1(pp_x));
	}
      else if(SCM_CAAR(pp_x) != SCM_CAR(x))
	fail = 5;
    }
  else
    if(0 && SCM_CAR(pp_x) != x)
      fail = 3;

  if(fail)
    {
      scm_misc_error("gp_restore_state","branch point is wrong ~a ci ~a ~a ~a",
                     scm_list_4(SCM_I_MAKINUM(fail), x, pp_x, path));
    }
}

SCM scm_length2(SCM pt)
{
  int i;
  for(i = 0; SCM_CONSP(pt);pt=SCM_CDR(pt),i++)
    {
    }

  return scm_from_int(i);
}


// #define DB(X) X
static void gp_restore_state(SCM data, struct gp_stack *gp, SCM K)
{
  SCM *si, path, pathfr;
  int n, m;
  SCM *ci,*fr, *fr_x,*cs, pp_x;
  scm_t_bits dynstack_length;
  SCM gp_unbd, dynstack;
  SCM rguards;
  SCM *v;
  mask_on(gp->id,&gp_unbd,SCM_PACK(GP_MK_FRAME_UNBD(gp_type)));

  gp_debug0("to restore\n");

  if(SCM_I_IS_VECTOR(data) && SCM_I_VECTOR_LENGTH(data) == NSTATE)
    {
      v               = SCM_I_VECTOR_WELTS(data);
      dynstack        = v[STATE_DYNSTACK];
      dynstack_length = SCM_UNPACK(v[STATE_DYNLENGTH]);
      rguards         = v[STATE_RGUARDS];
      n               = SCM_I_INUM(v[STATE_N]);
      path            = v[STATE_PATH];
      pathfr          = v[CONTROL_PATH];
      m               = gp->gp_fr - gp->gp_frstack;
    }
  else
    {
      scm_misc_error("gp_restore_state","wrong input format",SCM_EOL);
      return;
    }
  
  scm_t_bits nfini = n;

  gp_debug2("make paths equal length m = %d, n = %d\n",m,n);
  fr_x = gp->gp_fr - 1;
  pp_x = pathfr;

  gp_format2("npath ~a npath_fr ~a~%",
	     scm_length2(path),
	     scm_length2(pathfr));

  if(m > n) 
    {
      gp_debug0("m > n\n");
      fr_x = gp->gp_fr - (m - n) - 1;
      m = n;
    }

  if(n > m)
    {
      gp_debug2("n > m %d > %d\n",n,m);
      pp_x = pathfr;
      for(;n > m; n--)
	{
          if(SCM_CONSP(pp_x))
	    pp_x = SCM_CDR(pp_x);
          else
            scm_misc_error("gp-restore-state",
                           "encountered a non cons as a pp_x ~a ~a  > ~a"
                           ,scm_list_3(pp_x, 
                                       SCM_I_MAKINUM(n), SCM_I_MAKINUM(m)));
	}
      gp_debug2("restore n == m %d > %d\n",n,m);
    }


  if(n != m)
    scm_misc_error("gp-restore-state","m != n ~a != ~a", 
		   scm_list_2(SCM_I_MAKINUM(m),SCM_I_MAKINUM(n)));

  gp_format1("pp_x ~a~%", scm_length2(pp_x));
	    

  gp_debug0("get-branch\n");
  fr = gp_get_branch(&pp_x, fr_x, gp) + 1;

  if(SCM_CONSP(fr[-1]))
    gp_format0("fr has a tag\n");
  
  gp_format2("pp_x ~a dfr ~a~%", 
	     scm_length2(pp_x),
	     scm_from_long(fr_x-fr));

  gp_debug1("got-branch - will check (check fr: %d == 0)\n",
	    (fr - gp->gp_frstack) % GP_FRAMESIZE);

  check("frame", fr, pp_x, pathfr);
  
  gp_debug0("checked - will extract frame data\n");

  si = gp->gp_stack      + GP_GET_VAR     (fr);
  cs = gp->gp_cons_stack + GP_GET_CONS    (fr);
  ci = gp->gp_cstack     + GP_GET_VAL_VAL (fr) + 1;

  
  if(!SCM_CONSP(ci[-1]))
    if(SCM_UNPACK(ci[-1]) != SCM_UNPACK(GP_GET_VAL(fr)))
      scm_misc_error("restore-state",
		     "ci self entry is not the same~%~a ~a ~a ~a",
		     scm_list_4(ci[-2],ci[-1],ci[0],
				GP_GET_VAL(fr)));

  if(si > gp->gp_si) si = gp->gp_si;
  if(cs > gp->gp_cs) cs = gp->gp_cs;

  gp_debug4("fr %d ci %d si %d cs %d - will maybe unwind\n",
	    gp->gp_fr - fr,
	    gp->gp_ci - ci,
	    gp->gp_si - si,
	    gp->gp_cs - cs);

  gp_unwind0(fr, ci, si, cs, gp, SCM_BOOL_F, SCM_BOOL_F);

  gp_debug0("scan ci stack\n");

  SCM pp_c = gp_scan_tail(path, ci, gp);
  gp_format1("pp_c ~a~%", scm_length2(pp_c));

  gp_debug0("check ci stack\n");
  check("valstack", ci, pp_c, path);

  gp_format2("=========pp_c=======~%~a~%========pp_x========~%~a~%",
	  pp_c, pp_x);

  gp_debug0("rewind ci\n");
  gp_rewind(path, pp_c, gp, K); 

  gp_debug0("rewind fr\n");
  gp_rewind_fr(pathfr,pp_x, gp);    

  if(nfini !=  gp->gp_fr - gp->gp_frstack)
    scm_misc_error("gp_restore_state",
		   "final length is wrong by (theo - cur) ~a",
		   scm_list_1(SCM_I_MAKINUM(nfini - 
					    (gp->gp_fr - gp->gp_frstack))));

  if(GP_GET_VAL_VAL(gp->gp_fr) != gp->gp_ci - gp->gp_cstack - 1)
    {
      scm_misc_error("gp-restore-state",
		     "ci stack is not tuned right frame says :~a gp_ci says~a",
		     scm_list_2(scm_from_uint(GP_GET_VAL_VAL(gp->gp_fr)),
				scm_from_uint(gp->gp_ci - gp->gp_cstack - 1)));
    }
  gp_debug0("check restored\n");
  
  if(gp->gp_fr > gp->gp_frstack)
    {
      SCM x = gp->gp_fr[-1];
      if(!(SCM_CONSP(x) && 2 & SCM_UNPACK(SCM_CAR(x))))
	{
	  //gp_format1("chained the end with ~a~%",x);
	  gp->gp_fr[-1] = scm_cons(SCM_PACK(gp_redo_tag),
				   scm_cons(pathfr, x));
	}
    }

  if(gp->gp_ci > gp->gp_cstack)
    {
      SCM x = gp->gp_ci[-1];
      if(!(SCM_CONSP(x) && 2 & SCM_UNPACK(SCM_CAR(x))))
	{
	  //gp_format1("chained the end with ~a~%",x);
	  gp->gp_ci[-1] = scm_cons(SCM_PACK(gp_redo_tag),
				   scm_cons(path, x));
	}
    }
  
  gp_debug0("reinstate dynstack\n");
  reinstate_dynstack(gp, dynstack, dynstack_length, K, rguards);
  gp_debug0("leave reinstate\n");

  //To be sure to get the absolutly right state, we use this
  gp->_logical_   = SCM_I_INUM(v[STATE_LOGICAL]);
  gp->handlers    = v[HANDLERS];
}

//#define DB(X) 

/*
we have the current state as [a,...,b,...]
we have the restore state as [c,...,b,...]

So we need to unwind to [b,...] first

then we need to resore all paths back to c

and finally restore the current engine
 */
SCM_DEFINE(gp_gp_restore_state, "gp-restore-state", 2, 0, 0, (SCM cont, SCM K), 
	   "restore a continuation point")
#define FUNC_NAME s_gp_gp_restore_state
{
  gp_no_gc();

  // Unpack level 1
  SCM paths  = SCM_CAR(cont);
  SCM pathsl = SCM_CDR(cont);
  SCM spath  = SCM_CDR(paths);
  SCM epath  = SCM_CAR(paths);
  SCM lpath  = SCM_CDR(pathsl);
      cont   = SCM_CAR(pathsl);

  //First we clear the head of the states

  /*
  int ncur = scm_to_int(scm_length(gp_engine_path));
  int nnew = scm_to_int(scm_length(epath));
  printf("restore 3 %d %d\n",ncur,nnew);fflush(stdout);
  // make ncur == nnew
  while(ncur > nnew)
    {
      SCM x = SCM_CAR(gp_engine_path);
      SCM e = SCM_CDR(x);
      SCM s = SCM_CAR(x);
      scm_fluid_set_x(gp_current_stack, e);
      gp_clear(s);
      
      gp_engine_path = SCM_CDR(gp_engine_path);
      ncur--;
    }

  printf("restore 4 %d %d\n",ncur,nnew);fflush(stdout);
  
  while(nnew > ncur)
    {
      epath = SCM_CDR(epath);
      nnew--;
    }

  printf("restore 5 %d %d\n",ncur, nnew);fflush(stdout);
  
  // Unwind all stack frames untill the common b,...
  while(ncur > 0)
    {
      
      if(scm_is_eq(SCM_CDAR(gp_engine_path), SCM_CDAR(epath)))
        break;

      SCM x = SCM_CAR(gp_engine_path);
      SCM e = SCM_CDR(x);
      SCM s = SCM_CAR(x);
      scm_fluid_set_x(gp_current_stack, e);
      gp_clear(s);
      
      gp_engine_path = SCM_CDR(gp_engine_path);
      epath = SCM_CDR(epath);
      ncur--;      
    }

  printf("restore 6 %d %d\n", ncur, scm_to_int(scm_length(lpath)));
  fflush(stdout);
  */
  
  for(;SCM_CONSP(lpath); lpath = SCM_CDR(lpath))
    {
      SCM ll = SCM_CAR(lpath);

      for(;SCM_CONSP(ll); ll = SCM_CDR(ll))
        {
          SCM dcont = SCM_CAR(ll);

          if(scm_is_false(dcont))
            continue;

          if(scm_is_eq(dcont, cont))
            continue;
          else
            {
              scm_fluid_set_x(gp_current_stack, SCM_CADR(dcont));
              struct gp_stack *gp = get_gp();
              gp_restore_state(SCM_CAR(dcont), gp, K);
            }
        }
    }

  {
    scm_fluid_set_x(gp_current_stack, SCM_CADR(cont));
    struct gp_stack *gp2 = get_gp();
    gp_restore_state(SCM_CAR(cont), gp2, K);
  }

  gp_engine_path = epath;
  gp_store_path  = spath;
  gp_paths       = paths;
  
  gp_do_gc();
    
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

//#define DB(X) 

SCM make_rguards(SCM, int allp);

SCM_DEFINE(gp_store_engine_guards, "gp-store-engine-guards", 0, 0, 0, (), 
             "store a engine continuation guards")
#define FUNC_NAME s_gp_store_engine_guards
{
   struct gp_stack *gp = get_gp();

   SCM ret = make_rguards(gp->rguards,0);
   return ret;
}
#undef FUNC_NAME

SCM_DEFINE(gp_restore_engine_guards, "gp-restore-engine-guards", 1, 0, 0, 
             (SCM guard_data), 
             "restore a engine continuation point")
#define FUNC_NAME s_gp_restore_engine_guards
{
  eval_rguards(guard_data, SCM_BOOL_T);  
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME


SCM_DEFINE(gp_clear_frame_x, "gp-clear-frame!", 1, 0, 0, (SCM s), 
	   "if s points to a numbered frame, then we will clear it")
#define FUNC_NAME s_gp_cear_frame_x
{
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_mark_permanent, "gp-mark-permanent", 1, 0, 0, (SCM x), 
	   "marks a variable as permanent and hence will be removed from stacks")
#define FUNC_NAME s_gp_mark_permanent_x
{
  SCM *id = GP_GETREF(x);
  if(GP(x) && GP_UNBOUND(id))
    {
      scm_t_bits p = SCM_UNPACK(id[0]);
      GP_GC_QAND(p);
      id[0] = SCM_PACK(p);
      return SCM_BOOL_T;
    }
  return SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_add_unwind_hook, "gp-add-unwind-hook", 1, 0, 0, (SCM x), 
	   "marks a variable as permanent and hence will be removed from stacks")
#define FUNC_NAME s_gp_add_unwind_hook_x
{
  scm_fluid_set_x(unwind_hooks, scm_cons(x, scm_fluid_ref(unwind_hooks)));
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME


//#define DB(X)
