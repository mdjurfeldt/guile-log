#include<libguile.h>
//#include "vlist.h"
#include "vlist.c"

/*
  vhash-fold* vhash-foldq* vhash-foldv*
  alist->vhash))
*/


SCM_DEFINE(scm_vhash_to_assoc, "vhash->assoc", 1, 0, 0, (SCM vhash), 
           "")
#define FUNC_NAME s_scm_vhash_to_assoc
{
  if(vhash == vlist_null) return SCM_EOL;
  
  assert_vhash(vhash);
  return vhash_to_assoc(vhash);
}
#undef FUNC_NAME


#define KEYVAL(key,val)							\
  (scm_is_eq(val, SCM_UNSPECIFIED) ? SCM_BOOL_F : scm_cons(key,val))

SCM_DEFINE(scm_vhash_fold, "vhash-fold", 3, 0, 0, (SCM f, SCM seed, SCM vhash), 
           "")
#define FUNC_NAME s_scm_vhash_fold
{
  if(vhash == vlist_null) return seed;
  assert_vhash(vhash);
  return vhash_fold_data(f, vhash_fold_stub, seed, vhash);
}
#undef FUNC_NAME

SCM_DEFINE(scm_vhash_fold_right, "vhash-fold-right", 3, 0, 0, 
           (SCM f, SCM seed, SCM vhash), 
           "")
#define FUNC_NAME s_scm_vhash_fold_right
{
  if(vhash == vlist_null) return seed;
  assert_vhash(vhash);
  return vhash_fold_right_data(f, vhash_fold_stub, seed,vhash);
}
#undef FUNC_NAME

SCM_DEFINE(scm_vlist_ref_inc, "vlist-refcount++", 1, 0, 0, (SCM vlist), 
           "")
#define FUNC_NAME s_scm_vlist_ref_inc
{
  if(vlist == vlist_null) return SCM_UNSPECIFIED;
  assert_vlist(vlist);
  vlist_incref(vlist);
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(scm_vlist_ref_dec, "vlist-refcount--", 1, 0, 0, (SCM vlist), 
           "")
#define FUNC_NAME s_scm_vlist_ref_dec
{
  if(vlist == vlist_null) return SCM_UNSPECIFIED;

  assert_vlist(vlist);
  vlist_decref(vlist);
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME


SCM_DEFINE(scm_vlist_truncate_x, "vlist-truncate!", 1, 0, 0, (SCM vlist), 
           "")
#define FUNC_NAME s_scm_vlist_truncate_x
{
  if(vlist == vlist_null) return SCM_UNSPECIFIED;

  assert_vlist(vlist);
  vlist_truncate_x(vlist);
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(scm_vhash_truncate_x, "vhash-truncate!", 1, 0, 0, (SCM vhash), 
           "")
#define FUNC_NAME s_scm_vhash_truncate_x
{
  if(vhash == vlist_null) return SCM_UNSPECIFIED;

  assert_vhash(vhash);
  vhash_truncate_x(vhash);
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME


SCM_DEFINE(scm_vhash_del, "vhash-delete", 2, 0, 0, (SCM k, SCM vhash), 
           "")
#define FUNC_NAME s_scm_vhash_del
{
  if(vhash == vlist_null) return vlist_null;

  assert_vhash(vhash);
  return vhash_del(k,vhash);
}
#undef FUNC_NAME

SCM_DEFINE(scm_vhash_delq, "vhash-delq", 2, 0, 0, (SCM k, SCM vhash), 
           "")
#define FUNC_NAME s_scm_vhash_delq
{
  if(vhash == vlist_null) return vlist_null;

  assert_vhash(vhash);
  return vhash_delq(k,vhash);
}
#undef FUNC_NAME

SCM_DEFINE(scm_vhash_delv, "vhash-delv", 2, 0, 0, (SCM k, SCM vhash), 
           "")
#define FUNC_NAME s_scm_vhash_delv
{
  if(vhash == vlist_null) return vlist_null;

  assert_vhash(vhash);
  return vhash_delv(k,vhash);
}
#undef FUNC_NAME


SCM_DEFINE(scm_vhash_assoc, "vhash-assoc", 2, 0, 0, (SCM k, SCM vhash), 
           "")
#define FUNC_NAME s_scm_vhash_assoc
{
  if(vhash != vlist_null)
    assert_vhash(vhash);

  SCM val = vhash_assoc(k,vhash);
  return KEYVAL(k,val);
}
#undef FUNC_NAME

SCM_DEFINE(scm_vhash_assa, "vhash-assa", 2, 0, 0, (SCM k, SCM vhash), 
           "")
#define FUNC_NAME s_scm_vhash_assa
{
  if(vhash != vlist_null)
    assert_vhash(vhash);

  SCM val = vhash_assa(k,vhash);
  return KEYVAL(k,val);
}
#undef FUNC_NAME

SCM_DEFINE(scm_set_x, "vhash-set!", 3, 0, 0, (SCM k, SCM v, SCM vhash), 
           "")
#define FUNC_NAME s_scm_vhash_set_x
{
  if(vhash != vlist_null)
    assert_vhash(vhash);
  return vhash_set_x(k,v,vhash);
}
#undef FUNC_NAME
SCM_DEFINE(scm_setq_x, "vhash-setq!", 3, 0, 0, (SCM k, SCM v, SCM vhash), 
           "")
#define FUNC_NAME s_scm_vhash_setq_x
{
  if(vhash != vlist_null)
    assert_vhash(vhash);
  return vhash_setq_x(k,v,vhash);
}
#undef FUNC_NAME
SCM_DEFINE(scm_setv_x, "vhash-setv!", 3, 0, 0, (SCM k, SCM v, SCM vhash), 
           "")
#define FUNC_NAME s_scm_vhash_setv_x
{
  if(vhash != vlist_null)
    assert_vhash(vhash);
  return vhash_setv_x(k,v,vhash);
}
#undef FUNC_NAME

SCM_DEFINE(scm_vhash_assq, "vhash-assq", 2, 0, 0, (SCM k, SCM vhash), 
           "")
#define FUNC_NAME s_scm_vhash_assq
{
  if(vhash != vlist_null)
    assert_vhash(vhash);
  SCM val = vhash_assq(k,vhash);
  return KEYVAL(k,val);
}
#undef FUNC_NAME

SCM_DEFINE(scm_vhash_assv, "vhash-assv", 2, 0, 0, (SCM k, SCM vhash), 
           "")
#define FUNC_NAME s_scm_vhash_assv
{
  if(vhash != vlist_null)
    assert_vhash(vhash);
  SCM val = vhash_assv(k,vhash); 
  return KEYVAL(k,val);
}
#undef FUNC_NAME


SCM_DEFINE(scm_vhash_cons, "vhash-cons", 3, 0, 0, (SCM k,  SCM v, SCM vhash), 
           "")
#define FUNC_NAME s_scm_vhash_cons
{
  if(vhash != vlist_null)
    assert_vhash(vhash);
  return vhash_cons(k,v,vhash);
}
#undef FUNC_NAME

SCM_DEFINE(scm_vhash_consq, "vhash-consq", 3, 0, 0, (SCM k,  SCM v, SCM vhash), 
           "")
#define FUNC_NAME s_scm_vhash_consq
{
  if(vhash != vlist_null)
    assert_vhash(vhash);
  return vhash_consq(k,v,vhash);
}
#undef FUNC_NAME

SCM_DEFINE(scm_vhash_consa, "vhash-consa", 3, 0, 0, (SCM k,  SCM v, SCM vhash), 
           "")
#define FUNC_NAME s_scm_vhash_consa
{
  if(vhash != vlist_null)
    assert_vhash(vhash);
  return vhash_consa(k,v,vhash);
}
#undef FUNC_NAME

SCM_DEFINE(scm_vhash_consv, "vhash-consv", 3, 0, 0, (SCM k,  SCM v, SCM vhash), 
           "")
#define FUNC_NAME s_scm_vhash_consv
{
  if(vhash != vlist_null)
    assert_vhash(vhash);
  return vhash_consv(k,v,vhash);
}
#undef FUNC_NAME

SCM_DEFINE(scm_vlist_for_each, "vlist-for-each", 1, 0, 0, (SCM f, SCM vlist), 
           "")
#define FUNC_NAME s_scm_vlist_for_each
{
  if(vlist == vlist_null) return SCM_UNSPECIFIED;
  assert_vlist(vlist);
  vlist_for_each_data(f, vlist_map_stub, vlist);
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME


SCM_DEFINE(scm_list_to_vlist, "list->vlist", 1, 0, 0, (SCM list), "")
#define FUNC_NAME s_scm_list_to_vlist
{
  return list_to_vlist(list);
}
#undef FUNC_NAME

SCM_DEFINE(scm_vlist_delete, "vlist-delete", 2, 0, 0, (SCM x, SCM vlist), "")
#define FUNC_NAME s_scm_vlist_filter
{
  if(vlist == vlist_null) return vlist_null;
  assert_vlist(vlist);
  return vlist_delete(x , vlist);
}
#undef FUNC_NAME

SCM_DEFINE(scm_vlist_filter, "vlist-filter", 2, 0, 0, (SCM f, SCM vlist), "")
#define FUNC_NAME s_scm_vlist_filter
{
  if(vlist == vlist_null) return vlist_null;
  assert_vlist(vlist);
  return vlist_filter_data(f, vlist_map_stub, vlist);
}
#undef FUNC_NAME

SCM_DEFINE(scm_vlist_reverse, "vlist-reverse", 1, 0, 0, (SCM vlist), "")
#define FUNC_NAME s_scm_vlist_reverse
{
  if(vlist == vlist_null) return vlist_null;
  assert_vlist(vlist);
  return vlist_reverse(vlist);
}
#undef FUNC_NAME


SCM_DEFINE(scm_vlist_length, "vlist-length", 1, 0, 0, (SCM vlist), "")
#define FUNC_NAME s_scm_vlist_length
{
  if(vlist == vlist_null) return scm_from_int(0);
  assert_vlist(vlist);
  return scm_from_int(vlist_length(vlist));
}
#undef FUNC_NAME

SCM_DEFINE(scm_vlist_take, "vlist-take", 2, 0, 0, (SCM vlist, SCM count), "")
#define FUNC_NAME s_scm_vlist_take
{
  if(vlist == vlist_null) return vlist_null;
  assert_vlist(vlist);
  return vlist_take(vlist, scm_to_int(count));
}
#undef FUNC_NAME

SCM_DEFINE(scm_vlist_drop, "vlist-drop", 2, 0, 0, (SCM vlist, SCM count), "")
#define FUNC_NAME s_scm_vlist_drop
{
  if(vlist == vlist_null) return vlist_null;
  assert_vlist(vlist);
  return vlist_drop(vlist, scm_to_int(count));
}
#undef FUNC_NAME


SCM_DEFINE(scm_vlist_map, "vlist-map", 2, 0, 0, 
	   (SCM f, SCM vlist),
	   "")
#define FUNC_NAME s_scm_vlist_map
{
  if(vlist == vlist_null) return vlist_null;
  assert_vlist(vlist);
  return vlist_map_data(f, vlist_map_stub, vlist);
}
#undef FUNC_NAME




SCM_DEFINE(scm_vlist_fold_right, "vlist-fold-right", 3, 0, 0, 
	   (SCM f, SCM seed, SCM vlist),
	   "")
#define FUNC_NAME s_scm_vlist_fold_right
{
  if(vlist == vlist_null) return seed;
  assert_vlist(vlist);
  return vlist_fold_right_data(f, vlist_fold_stub, seed, vlist);
}
#undef FUNC_NAME

// --------------------------


SCM_DEFINE(scm_vlist_fold, "vlist-fold", 3, 0, 0, (SCM f, SCM seed, SCM vlist),
	   "")
#define FUNC_NAME s_scm_vlist_fold
{
  if(vlist == vlist_null) return seed;
  assert_vlist(vlist);
  return vlist_fold_data(f, vlist_fold_stub, seed, vlist);
}
#undef FUNC_NAME

// --------------------------

// TODO make sure that we walk inside the vlist here the correct way
SCM_DEFINE(scm_vlist_ref, "vlist-ref", 2, 0, 0, (SCM vlist, SCM i), 
	   "")
#define FUNC_NAME s_scm_vlist_ref
{
  int j = scm_to_int(i);
  assert_vlist(vlist);
  return vlist_ref(vlist, j);
}
#undef FUNC_NAME

// --------------------------

SCM_DEFINE(scm_vlist_set_x, "vlist-set!", 3, 0, 0, (SCM vlist, SCM i, SCM val), 
	   "")
#define FUNC_NAME s_scm_vlist_ref
{
  int j = scm_to_int(i);
  assert_vlist(vlist);
  return vlist_set_x(vlist, j, val);
}
#undef FUNC_NAME

// --------------------------


SCM_DEFINE(scm_vlist_to_list, "vlist->list", 1, 0, 0, (SCM vlist), 
	   "")
#define FUNC_NAME s_scm_vlist_to_list
{
  assert_vlist(vlist);
  return vlist_to_list(vlist);
}
#undef FUNC_NAME

// --------------------------



SCM_DEFINE(scm_vlist_p, "vlist?", 1, 0, 0, (SCM vlist), 
	   "check is argument is a vlist")
#define FUNC_NAME s_scm_vlist_p
{
  if(vlist_p(vlist))
    return SCM_BOOL_T;
  else
    return SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(scm_vhash_p, "vhash?", 1, 0, 0, (SCM vlist), 
	   "check is argument is a vlist")
#define FUNC_NAME s_scm_vhash_p
{
  if(vhash_p(vlist))
    return SCM_BOOL_T;
  else
    return SCM_BOOL_F;
}
#undef FUNC_NAME

// --------------------------


SCM_DEFINE(scm_vlist_cons, "vlist-cons", 2, 0, 0, (SCM item, SCM vlist), 
	   "")
#define FUNC_NAME s_scm_vlist_cons
{
  assert_vlist(vlist);
  return vlist_cons(item, vlist);
}
#undef FUNC_NAME

// --------------------------


SCM_DEFINE(scm_vlist_head, "vlist-head", 1, 0, 0, (SCM vlist), 
	   "")
#define FUNC_NAME s_scm_vlist_head
{
  assert_vlist(vlist);
  return vlist_head(vlist);
}
#undef FUNC_NAME

// --------------------------


SCM_DEFINE(scm_vlist_tail, "vlist-tail", 1, 0, 0, (SCM vlist), 
	   "")
#define FUNC_NAME s_scm_vlist_tail
{
  assert_vlist(vlist);
  return vlist_tail(vlist);
}
#undef FUNC_NAME

// --------------------------


SCM_DEFINE(scm_vlist_null_p, "vlist-null?", 1, 0, 0, (SCM vlist), 
	   "")
#define FUNC_NAME s_scm_vlist_tail
{
  if(vlist_null_p(vlist))
    return SCM_BOOL_T;
  else
    return SCM_BOOL_F;
}
#undef FUNC_NAME

// --------------------------


SCM_DEFINE(scm_vlist_setup, "setup-vlist", 1, 0, 0, (SCM type), 
	   "")
#define FUNC_NAME s_scm_vlist_setup
{
  return setup(type);
}
#undef FUNC_NAME

SCM_DEFINE(scm_test_vlist, "test-vlist", 2, 0, 0, (SCM n, SCM vlist), 
	   "")
#define FUNC_NAME s_scm_test_vlist
{
  int i;
  int N = scm_to_int(n);
  SCM _0 = scm_from_int(2000);
  for(i = 0; i < N; i++)
    vhash_assq(_0,vlist);
  return vhash_assq(_0,vlist);
}
#undef FUNC_NAME


void vlist_init()
{  
  //#include  "vlist.x"

  thread_seq_number = scm_make_fluid();
  scm_fluid_set_x(thread_seq_number, scm_from_ulong(ZERO));

  thread_id = scm_make_fluid();
  scm_fluid_set_x(thread_id, scm_from_ulong(ZERO));

  thread_inc = scm_from_long(1L<<TAGN);

  block_growth_factor = scm_make_fluid();
  scm_fluid_set_x(block_growth_factor, scm_from_int(2));

  init_block_size = scm_make_fluid();    
  scm_fluid_set_x(init_block_size, scm_from_int(1));

  vhash_cache  = scm_make_fluid();    
  
  block_null          = make_block(SCM_BOOL_F, 0, 0, 0);
}
