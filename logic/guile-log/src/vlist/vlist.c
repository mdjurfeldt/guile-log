#include<libguile.h>
#include<stdio.h>
#define VLIST 0
#define VHASH 1
// This code only works on 64 bit systems

void vhash_truncate_x(SCM vhash);
void vhash_block_clear(SCM *base);

#define S(x,n) SCM_STRUCT_SLOT_REF(x,n)

#define ACONS(ret,x,y)				\
do {						\
  SCM *pt = (SCM*) alloca(2*sizeof(SCM));	\
  pt[0]   = x;                                  \
  pt[1]   = y;					\
  ret = SCM_PACK((scm_t_bits) pt);		\
 } while(0)

#define ACONS2(ret,k,v,y)                       \
do {						\
  SCM *pt = (SCM*) alloca(2*sizeof(SCM));	\
  pt[0]   = scm_cons(k,v);                      \
  pt[1]   = y;					\
  ret = SCM_PACK((scm_t_bits) pt);		\
 } while(0)

/*
  For multithreaded applications and referenced ideoms we have an issue
  with severe pressure on array allocations, for small arrays this is not
  a problem, but for large one we can expect serious detoriation of 
  performance therefore some kind of caching scheme has to be employed.
  The idea is to keep a fluid with the last touched vhash and use that at
  each assoq.
*/

SCM vhash_cache = SCM_BOOL_F;

//TODO fix this on 32bit platforms
#define my_scm_to_int(x)      ((SCM_UNPACK(x)>>2))
#define my_scm_from_int(x)    (SCM_PACK((scm_t_bits) (((x) << 2) + 2)))
#define my_scm_to_ulong(x)   (SCM_UNPACK(x)>>2)
#define my_scm_from_ulong(x) (SCM_PACK((scm_t_bits) (((x) << 2) + 2)))

#define TAGN     32
#define TAGL(n)  ((1L<<n) - 1L)
#define TAGU(n)  (TAGL(n) << n)
#define LOW(x)    (((ulong) x) & TAGL(TAGN))
#define HIGH(x)  ((((ulong) x) & TAGU(TAGN)) >> TAGN)
#define COMB(low,high) (((ulong) (high)) | (ulong) (low))
#define SIZE(x) (TAGL(TAGN) & (x))
#define OFFSET(x) (TAGL(TAGN) & (x))
#define ZERO 0L
#define SEQ(x) ((x) & TAGU(TAGN))
#define THR(x) ((x) & TAGU(TAGN))
#define BACKREF_REF(x) ((int) HIGH(x))
#define NEXT_REF(x) ((int) LOW(x))
#define COMB_REFS(low,high) ((((ulong) high) << TAGN) | ((ulong) ((uint) low)))
#define NEXTFREE(x) ((int) LOW(x))
#define INCREF(x)   ((ulong) (x + (1L << TAGN)))
#define DECREF(x)   ((ulong) (x - (1L << TAGN)))
#define REFCOUNT(x) ((ulong) ((x) & TAGU(TAGN)))
SCM thread_seq_number;
SCM thread_id;
SCM thread_inc;
SCM block_growth_factor;
SCM init_block_size;

#define NULLBLOCK(Block) scm_is_eq(Block, block_null)

SCM make_block(SCM base, int offset, int size, int hash_tab_p)
{
  SCM first;
  SCM ret = scm_c_make_vector(5, SCM_BOOL_F);
  SCM *elts = SCM_I_VECTOR_WELTS(ret);

  int s = size;
  if(hash_tab_p)
    s *= 4;
  
  first = scm_c_make_vector(s, SCM_BOOL_F);
  
  ulong seq = my_scm_to_ulong(scm_fluid_ref(thread_seq_number));
  ulong thr = my_scm_to_ulong(scm_fluid_ref(thread_id));

  elts[0] = first;
  elts[1] = base;
  elts[2] = my_scm_from_ulong(COMB(offset, seq));
  elts[3] = my_scm_from_ulong(COMB(size,   thr));
  elts[4] = my_scm_from_int(0);

  return ret;
}


#define dB(Base) SCM_I_VECTOR_WELTS(Base)   
#define block_content(block)    (block[0])
#define block_base(block)                               \
  (scm_is_false(block[1]) ? (SCM *)0 : dB(block[1]))
#define block_the_base(block)   (block[1])
#define block_offset_seq(block) (my_scm_to_ulong(block[2]))
#define block_size_thr(block)   (my_scm_to_ulong(block[3]))
#define block_next_free(block)  (my_scm_to_ulong(block[4]))
#define block_next_free_ref(block)  (block[4])
#define block_offset(x) OFFSET(block_offset_seq(x))
#define block_size(x)   SIZE(block_size_thr(x))

int block_hash_table_p(SCM *block)
{
  return (block_size(block)			
	  <							
	  my_scm_to_int(scm_vector_length(block_content(block))));
}

void set_block_next_free_s
(SCM* block, ulong next_free)
{ 
  block[4] = my_scm_from_ulong(++next_free);
}


static inline int block_append_s(SCM* block, SCM value, int offset, int hashp)
{
  ulong st = block_size_thr(block);
  ulong os = block_offset_seq(block);
  ulong nextfree = block_next_free(block);

  ulong seq = my_scm_to_ulong(scm_fluid_ref(thread_seq_number));
  ulong thr = my_scm_to_ulong(scm_fluid_ref(thread_id));
 
  
  if ((offset <  block_size(block))
      && (!hashp  || (THR(st) == thr && SEQ(os) == seq))  &&
      (offset == NEXTFREE(nextfree)))
    {
      set_block_next_free_s(block, nextfree);
      scm_vector_set_x(block_content(block), my_scm_from_int(offset), value);
        return 1;
    }

    return 0;
}
// Return the item at slot OFFSET.
static inline SCM block_ref(SCM *content, int offset)
{
  return content[offset];
}

// Return the item at slot OFFSET.
static inline SCM block_set_x(SCM *content, int offset, SCM val)
{
  return content[offset] = val;
}

// Return the offset of the next item in the hash bucket, after the one
// at OFFSET.
static inline SCM block_hash_table_next_offset
(SCM *content, int size, int offset)
{  
  return content[(size*3) + offset];
}


// Save the offset of the next item in the hash bucket, after the one
// at OFFSET.
static inline void block_hash_table_set_next_offset_s
(SCM  *content, int size, int  offset, int khash, int next_offset)
{
  // Mask on a backref in order to be able to truncate the hash
  ulong x = COMB_REFS(next_offset, khash);

  SCM next_offset_s = my_scm_from_ulong(x);
  content[(size*3) + offset] = next_offset_s;
}

// Returns the index of the last entry stored in CONTENT with
// SIZE-modulo hash value KHASH.
static inline SCM block_hash_table_ref(SCM *content, int size, int khash)
{
  return content[size*2 + khash];
}

static inline void block_hash_table_set_s
(SCM *content, int size, int khash, int offset)
{
  if(offset < 0)
    content[size*2 + khash] = SCM_BOOL_F;
  else 
    content[size*2 + khash] = my_scm_from_int(offset); 
}

// Add hash table information for the item recently added at OFFSET,
// with SIZE-modulo hash KHASH.
static inline void block_hash_table_add_s
(SCM *content, int size, int khash, int offset)
{
  SCM oldval = block_hash_table_ref(content, size, khash);
  int val;
  if(scm_is_false(oldval))
    val = -1;
  else
    val = my_scm_to_int(oldval);

  block_hash_table_set_next_offset_s(content, size, offset, khash, val);
  block_hash_table_set_s(content, size, khash, offset);
}

SCM block_null;

SCM search_cached_block(SCM * cblock);

SCM make_block_l(SCM base, int offset, int size, int hash_tab_p, int *truncable)
  {
    SCM ret = SCM_BOOL_F; //search_cached_block(dB(base));
    
    if(scm_is_false(ret))
      {
	*truncable = 0;
	SCM ret = make_block(base, offset, size, hash_tab_p);
	set_block_next_free_s(dB(ret),0UL);      
	return ret;
      }

  
  *truncable = 1;
  SCM *elts = SCM_I_VECTOR_WELTS(ret);
  vhash_block_clear(elts);

  size      = block_size(elts);
    
  ulong seq = my_scm_to_ulong(scm_fluid_ref(thread_seq_number));

  elts[1] = base;
  elts[2] = my_scm_from_ulong(COMB(offset, seq));
 

  return ret;
}

/*
;;;
;;; VLists.
;;;
*/



SCM vlist_type;

static inline SCM make_vlist(SCM Base, int offset)
{  
  SCM ret =  scm_c_make_struct(vlist_type, 
			       0,
			       2,
			       SCM_UNPACK(Base), 
			       SCM_UNPACK(my_scm_from_int(offset)));
  return ret;
}

static inline SCM  vlist_the_base(SCM vlist)
{
  return S(vlist,0);
}

static inline SCM * vlist_base(SCM vlist)
{
  return SCM_I_VECTOR_WELTS(S(vlist, 0));
}

static inline int vlist_offset(SCM vlist)
{
  return my_scm_to_int(S(vlist, 1));
}

static inline int vlist_p(SCM vlist)
{
  return scm_is_true(scm_struct_p(vlist)) && 
    scm_is_eq(scm_struct_vtable(vlist), vlist_type);
}


SCM vlist_null;

// Asserting that something is a vlist is actually a win if your next
// step is to call record accessors, because that causes CSE to
// eliminate the type checks in those accessors.
//
static inline void assert_vlist(SCM val)
{
  if(!vlist_p(val))
    scm_misc_error("wrong-type-arg", 
		   "Not a vlist: ~S",
		   scm_list_1(val));
}

static inline void vlist_truncate_x(SCM vlist)
{
  SCM*  base    = vlist_base(vlist);
  int   offset  = vlist_offset(vlist);
  ulong freeref = my_scm_to_ulong(block_next_free_ref(base));
  ulong count   = REFCOUNT(freeref);
  
  ulong seq = my_scm_to_ulong(scm_fluid_ref(thread_seq_number));
  ulong thr = my_scm_to_ulong(scm_fluid_ref(thread_id));
  ulong st = block_size_thr(base);
  ulong os = block_offset_seq(base);
  
  SCM *bc = dB(block_content(base));

  freeref = NEXTFREE(freeref);

  if(!count && seq == SEQ(os) && thr == THR(st))
    {
      for(--freeref; freeref > offset; freeref--)
	bc[freeref] = SCM_BOOL_F;

      block_next_free_ref(base) = my_scm_from_int(offset + 1);    
    }
}

static inline int ALLOWED_BLOCK(SCM *block, ulong seq, ulong thr)
{
  ulong st = block_size_thr(block);
  ulong os = block_offset_seq(block);
  ulong freeref = my_scm_to_ulong(block_next_free_ref(block));
  int   count   = REFCOUNT(freeref);

  return THR(st) == thr && SEQ(os) == seq && !count;
}

SCM search_cached_block(SCM *cbase)
{
  SCM cached = scm_fluid_ref(vhash_cache);
  ulong seq = my_scm_to_ulong(scm_fluid_ref(thread_seq_number));
  ulong thr = my_scm_to_ulong(scm_fluid_ref(thread_id));
  SCM   Block = S(cached,0);
  SCM  *block = dB(Block);
  SCM   ret = SCM_BOOL_F;
 retry:
  if(!NULLBLOCK(Block) && ALLOWED_BLOCK(block, seq, thr) && block != cbase)
    {
      ret = Block;
      Block = block_the_base(block);
      block = dB(Block);
      goto retry;
    }

  return ret;
}

SCM block_cons(SCM item, SCM vlist, int hash_tab_p, int truncp)
{
  SCM  Base   = vlist_the_base(vlist); 
  SCM* base   = dB(Base);
  int  offset = 1 + vlist_offset(vlist);
  
  if (block_append_s(base, item, offset, hash_tab_p))
    {
      // Fast path: We added the item directly to the block.
      return  make_vlist(Base, offset);
    }
  else
    {
      // Slow path: Allocate a new block.
      int  size = block_size(base);
      int  s;

      if(size == 0)
	s = my_scm_to_int(scm_fluid_ref(init_block_size));
      else if (offset < size)
	s = my_scm_to_int(scm_fluid_ref(init_block_size));
      else
	s = my_scm_to_int(scm_fluid_ref(block_growth_factor)) * size;
      
      //printf("Alloctes block of size %d\n",s);
      
      SCM ret;
      if(truncp)
	{
          int truncable = 0;
	  Base = make_block_l(Base,
			      offset - 1,
			      s,
			      hash_tab_p,
                              &truncable);
          base = dB(Base);

	  ret =  make_vlist(Base, 0);
          if(truncable)
            vhash_truncate_x(ret);
          else
            set_block_next_free_s(base, 0);

	  scm_c_vector_set_x(block_content(base), 0, item);
	}
      else
	{
	  Base = make_block(Base,
			    offset - 1,
			    s,
			    hash_tab_p);


	  base = dB(Base);

          set_block_next_free_s(base, 0);	  
	  scm_c_vector_set_x(block_content(base), 0, item);
      
	  ret =  make_vlist(Base, 0);
	}
      return ret;
    }
}



void block_cons_l(SCM item, SCM *l, SCM *base, int off, int hash_tab_p)
{
  SCM  Base;
  int  offset = 1 + off;
  
  if (block_append_s(base, item, offset, hash_tab_p))
    {
      // Fast path: We added the item directly to the block.
      l[2] = SCM_PACK((scm_t_bits) offset);
      return;
    }
  else
    {
      // Slow path: Allocate a new block.
      int  size = block_size(base);
      int  s;
     
      if(size == 0)
	s = my_scm_to_int(scm_fluid_ref(init_block_size));
      else if (offset < size)
	s = my_scm_to_int(scm_fluid_ref(init_block_size));
      else
	s = my_scm_to_int(scm_fluid_ref(block_growth_factor)) * size;

      int truncable = 0;
      Base = make_block_l(S(l[0],0),
			offset - 1,
			s,
                        hash_tab_p,
                        &truncable);

      
      base = dB(Base);
      l[1] = GP_UNREF(base);

      
      SCM ret =  make_vlist(Base, 0);
      if(truncable)
        vhash_truncate_x(ret);
      else
        set_block_next_free_s(base, 0);

      scm_c_vector_set_x(block_content(base), 0, item);
      
      l[0] = ret; 
      l[2] = SCM_PACK(0);
    }
}
	
SCM vlist_cons(SCM item, SCM vlist)
{
  /*
  "Return a new vlist with ITEM as its head and VLIST as its
tail."
  ;; Note: Although the result of `vlist-cons' on a vhash is a valid
  ;; vlist, it is not a valid vhash.  The new item does not get a hash
  ;; table entry.  If we allocate a new block, the new block will not
  ;; have a hash table.  Perhaps we can do something more sensible here,
  ;; but this is a hot function, so there are performance impacts.
  */

  return block_cons(item, vlist, 0, 0);
}

SCM vlist_head(SCM vlist)
{
  //  "Return the head of VLIST."
  SCM* base   = vlist_base(vlist);
  int  offset = vlist_offset(vlist);
    
  return block_ref(dB(block_content(base)), offset);
}

SCM vlist_tail(SCM vlist)
{
  //  "Return the tail of VLIST."
  SCM  Base   = vlist_the_base(vlist);
  SCM *base   =  dB(Base);
  int  offset = vlist_offset(vlist);
  if (offset > 0)
    return make_vlist(Base, (offset - 1));
  else
    return make_vlist(block_the_base(base), block_offset(base));
}

int vlist_null_p(SCM vlist)
{
  //"Return true if VLIST is empty."
  SCM *base = vlist_base(vlist);
 
  return !((scm_t_bits) block_base(base)) && 0 == block_size(base);
}

/*
;;;
;;; VList Utilities.
;;;
*/


SCM vlist_fold(SCM (*proc)(SCM, SCM), SCM init, SCM vlist)
{
  //  "Fold over VLIST, calling PROC for each element."
  //  ;; FIXME: Handle multiple lists.
  SCM  Base   = vlist_the_base(vlist);
  SCM *base   = dB(Base);
  int  offset = vlist_offset(vlist);
  SCM  result = init;
  
  while(Base != block_null)
    {
      int next   = offset - 1;
      int done_p = next < 0;

      result = proc(block_ref(dB(block_content(base)), offset), result);
      offset = done_p ? block_offset(base)   : next;
      Base   = done_p ? block_the_base(base) : Base;
      base   = done_p ? dB(Base)             : base;
    }

  return result;
}


#define vlist_fold_mac(ret, proc, init, vlist)                    \
  {                                                               \
    SCM  Base   = vlist_the_base(vlist);                          \
    SCM *base   = dB(Base);                                       \
    int  offset = vlist_offset(vlist);                            \
    ret         = init;                                           \
                                                                  \
    while(Base != block_null)                                     \
      {                                                           \
	int next   = offset - 1;                                  \
	int done_p = next < 0;                                    \
  	                                                          \
	proc(ret, block_ref(dB(block_content(base)), offset), ret);     \
	offset = done_p ? block_offset(base)     : next;		\
        Base   = done_p ? block_the_base(base)   : Base;                \
        base   = done_p ? dB(Base)               : base;                \
      }									\
  }

#define vlist_fold_mac2(ret, proc, init, vlist)                   \
  {                                                               \
    SCM  Base   = vlist_the_base(vlist);                          \
    SCM *base   = dB(Base);                                       \
    int  size   = block_size(base);                               \
    int  offset = vlist_offset(vlist);                            \
    ret         = init;                                           \
                                                                  \
    while(Base != block_null)                                     \
      {                                                           \
	int next   = offset - 1;                                  \
	int done_p = next < 0;                                    \
  	                                                          \
	proc(ret, block_ref(dB(block_content(base)), offset),           \
             block_ref(dB(block_content(base)), offset + size),ret);    \
	offset = done_p ? block_offset(base)     : next;		\
        Base   = done_p ? block_the_base(base)   : Base;                \
        base   = done_p ? dB(Base)               : base;                \
      }									\
  }

SCM vlist_fold_data(SCM data, SCM (*proc)(SCM, SCM, SCM), SCM init, SCM vlist)
{
#define FOLDDATA(ret, x, seed) {ret = proc(data, x, seed);}
  SCM ret;
  vlist_fold_mac(ret, FOLDDATA, init, vlist);
  return ret;
}

#define list_fold_mac(ret, proc, init, list)		\
do {							\
  ret = init;						\
							\
 lp_list:						\
  if(SCM_NULLP(list))					\
    {break;}						\
							\
  proc(ret, SCM_CAR(list), ret);			\
  list = SCM_CDR(list);					\
  goto lp_list;						\
 } while(0)

// This is a very fast fold_right, but depends on a vast amopunt of stack memory
SCM vlist_fold_right( SCM (*proc)(SCM,SCM), SCM init, SCM vlist)
{
  //  "Fold over VLIST, calling PROC for each element, starting from
  //  the last element."
  SCM ret;
  vlist_fold_mac(ret, ACONS, SCM_EOL, vlist);
  //ret = vlist_fold(scm_cons, SCM_EOL, vlist);
  while(!SCM_NULLP(ret))
    {
      init = proc(SCM_CAR(ret), init);
      ret = SCM_CDR(ret);
    }
  
  return init;
}

#define vlist_fold_right_mac(ret, proc, init, vlist)			\
{									\
  SCM ret1;								\
  SCM i = init;								\
  vlist_fold_mac(ret1, ACONS, SCM_EOL, vlist);				\
									\
  while(!SCM_NULLP(ret1))						\
    {									\
      proc(i,SCM_CAR(ret1), i);						\
      ret1 = SCM_CDR(ret1);						\
    }									\
									\
  ret = i;								\
}

#define vlist_fold_right_mac2(ret, proc, init, vlist)			\
{									\
  SCM ret1;								\
  SCM i = init;								\
  vlist_fold_mac2(ret1, ACONS2, SCM_EOL, vlist);                        \
									\
  while(!SCM_NULLP(ret1))						\
    {									\
      proc(i,SCM_CAR(ret1), i);						\
      ret1 = SCM_CDR(ret1);						\
    }									\
									\
  ret = i;								\
}


SCM vlist_fold_right_data(SCM data, SCM (*proc)(SCM,SCM,SCM), 
			  SCM init, SCM vlist)
{
  //  "Fold over VLIST, calling PROC for each element, starting from
  //  the last element."

  SCM ret;
  vlist_fold_mac(ret, ACONS, SCM_EOL, vlist);

  while(!SCM_NULLP(ret))
    {
      init = proc(data, SCM_CAR(ret), init);
      ret = SCM_CDR(ret);
    }
  
  return init;
}

SCM vlist_reverse(SCM vlist)
{
//  "Return a new VLIST whose content are those of VLIST in reverse
// order."
  return vlist_fold(vlist_cons, vlist_null, vlist);
}

SCM list_to_vlist(SCM lst)
{
  //  "Return a new vlist whose contents correspond to LST."
  SCM it = SCM_EOL;
  while(SCM_CONSP(lst))
    {
      ACONS(it,SCM_CAR(lst),it);
      lst = SCM_CDR(lst);
    }
  
  SCM ret = vlist_null;
  while(!SCM_NULLP(it))
    {
      ret = vlist_cons(SCM_CAR(it), ret);
      it = SCM_CDR(it);
    }

  return ret;
}

SCM vlist_map(SCM (*proc)(SCM), SCM vlist)
{
  //  "Map PROC over the elements of VLIST and return a new vlist."
#define PROC(R,X,Y) (R  = vlist_cons(proc(X), Y))
  SCM ret1,ret2;
  vlist_fold_mac(ret1, ACONS, SCM_EOL, vlist);
  list_fold_mac(ret2, PROC, vlist_null, ret1);

  return ret2;
}

SCM vlist_map_data(SCM data, SCM (*proc)(SCM, SCM), SCM vlist)
{
  //  "Map PROC over the elements of VLIST and return a new vlist."
#define PROCD(R,X,Y) (R  = vlist_cons(proc(data, X), Y))
  SCM ret1,ret2;
  vlist_fold_mac(ret1, ACONS, SCM_EOL, vlist);
  list_fold_mac(ret2, PROCD, vlist_null, ret1);

  return ret2;
}

SCM vlist_to_list(SCM vlist)
{
  //"Return a new list whose contents match those of VLIST."
  return vlist_fold_right(scm_cons, SCM_EOL, vlist);
}


SCM vlist_ref(SCM vlist, int index)
{
  // "Return the element at index INDEX in VLIST."
  SCM *base   = vlist_base(vlist);
  int  offset = vlist_offset(vlist);
 lp:
  if (index <= offset)
    return block_ref(dB(block_content(base)),  (offset - index));
  
  index  = index - offset - 1;
  offset = block_offset(base);
  base   = block_base(base);
  goto lp;
}              

SCM vlist_set_x(SCM vlist, int index, SCM val)
{
  // "Return the element at index INDEX in VLIST."
  SCM *base   = vlist_base(vlist);
  int  offset = vlist_offset(vlist);
 lp:
  if (index <= offset)
    block_set_x(dB(block_content(base)),  (offset - index), val);
  
  index  = index - offset - 1;
  offset = block_offset(base);
  base   = block_base(base);
  goto lp;
}              

SCM vlist_drop(SCM vlist, int count)
{
  //  "Return a new vlist that does not contain the COUNT first elements of
  //   VLIST."
  SCM  Base   = vlist_the_base(vlist);
  SCM *base   = dB(Base);
  int  offset = vlist_offset(vlist);
 lp:
  if (count <= offset)
    return make_vlist(Base, (offset - count));
  
  count  = count - offset - 1;
  offset = block_offset(base);
  Base   = block_the_base(base);
  base   = dB(Base);
  goto lp;
}

SCM vlist_take(SCM vlist, int count)
{
  //  "Return a new vlist that contains only the COUNT first elements of
  //  VLIST."

  //SCM *base   = vlist_base(vlist);
  //int  offset = vlist_offset(vlist);
  SCM  result = vlist_null;
 lp:
  if (0 == count)
    return vlist_reverse(result);

   count  = count - 1;
   vlist  = vlist_tail(vlist);
   result = vlist_cons(vlist_head(vlist), result);
   
   goto lp;
}

SCM vlist_filter( int (*pred)(SCM),  SCM vlist)
{
  //  "Return a new vlist containing all the elements from VLIST that
  //   satisfy PRED."
#define F(r,e,v) (r = (pred(e) ? vlist_cons(e,v) : v))
  SCM ret;
  vlist_fold_right_mac(ret, F, vlist_null, vlist);
  return ret;
}

SCM vlist_filter_data(SCM data, SCM (*pred)(SCM, SCM),  SCM vlist)
{
  //  "Return a new vlist containing all the elements from VLIST that
  //   satisfy PRED."
#define FilterD(r,e,v) (r = (scm_is_true(pred(data, e)) ? vlist_cons(e,v) : v))
  SCM ret;
  vlist_fold_right_mac(ret, FilterD, vlist_null, vlist);
  return ret;
}

#define Filter(pred,r,e,v) (r = (pred(e) ? vlist_cons(e,v) : v))

SCM vlist_delete(SCM x, SCM  vlist)
{
  // "Return a new vlist corresponding to VLIST without the elements
  // EQUAL? to X."
#define TEST(e) scm_is_false(scm_equal_p(e,x))
#define DELTEST(r,e,v) Filter(TEST,r,e,v)
  SCM ret;
  vlist_fold_right_mac(ret, DELTEST, vlist_null, vlist);	
  return ret;
}

int vlist_length(SCM vlist)
{
  // "Return the length of VLIST."
  
  SCM *base   = vlist_base(vlist);
  int  offset = vlist_offset(vlist);
  int  len    = offset;

  while(base)
    {
      len  = len + 1 + block_offset(base);
      base = block_base(base);
    }
  
  return len - 1;
}         

void vlist_incref(SCM vlist)
{
  // "Return the length of VLIST."
  
  SCM *base   = vlist_base(vlist);

  ulong seq = my_scm_to_ulong(scm_fluid_ref(thread_seq_number));
  ulong thr = my_scm_to_ulong(scm_fluid_ref(thread_id));

  while(base)
    {
      ulong st = block_size_thr(base);
      ulong os = block_offset_seq(base);
      if(SEQ(os) == seq && THR(st) == thr)
        {
          block_next_free_ref(base) 
            = scm_from_ulong(INCREF(block_next_free(base)));
          base = block_base(base);
        }
    }
}         

void vlist_decref(SCM vlist)
{
  // "Return the length of VLIST."
  
  SCM *base   = vlist_base(vlist);

  ulong seq = my_scm_to_ulong(scm_fluid_ref(thread_seq_number));
  ulong thr = my_scm_to_ulong(scm_fluid_ref(thread_id));

  while(base)
    {
      ulong st = block_size_thr(base);
      ulong os = block_offset_seq(base);
      if(SEQ(os) == seq && THR(st) == thr)
        {
          block_next_free_ref(base) 
            = scm_from_ulong(DECREF(block_next_free(base)));
          base = block_base(base);
        }
    }
}         

SCM vlist_append(SCM vlists)
{
  // "Append the given lists."
  if (SCM_NULLP(vlists))
    return  vlist_null;

#define FAPPEND(r, vlist, result)			\
  (r = vlist_fold_right(vlist_cons, result, vlist))

  SCM ret;

  vlist_fold_right_mac(ret, FAPPEND, vlist_null, vlists);

  return ret;
}

SCM vlist_for_each_data (SCM data, SCM (*f) (SCM, SCM), SCM vlist)
{
  SCM ret;
#define FOREACH(r,x,v) f(data, x)
  vlist_fold_mac(ret,FOREACH,SCM_EOL,vlist);  
  return ret;
}

/*
;;;
;;; Hash Lists, aka. `VHash'.
;;;

;; Assume keys K1 and K2, H = hash(K1) = hash(K2), and two values V1 and V2
;; associated with K1 and K2, respectively.  The resulting layout is a
;; follows:
;;
;;             ,--------------------.
;;            0| ,-> (K1 . V1)      | Vlist array
;;            1| |                  |
;;            2| |   (K2 . V2)      |
;;            3| |                  |
;;        size +-|------------------+
;;            0| |                  | Hash table
;;            1| |                  |
;;            2| +-- O <------------- H
;;            3| |                  |
;;    size * 2 +-|------------------+
;;            0| `-> 2              | Chain links
;;            1|                    |
;;            2|    #f              |
;;            3|                    |
;;    size * 3 `--------------------'
;;
;; The backing store for the vhash is partitioned into three areas: the
;; vlist part, the hash table part, and the chain links part.  In this
;; example we have a hash H which, when indexed into the hash table
;; part, indicates that a value with this hash can be found at offset 0
;; in the vlist part.  The corresponding index (in this case, 0) of the
;; chain links array holds the index of the next element in this block
;; with this hash value, or #f if we reached the end of the chain.
;;
;; This API potentially requires users to repeat which hash function and
;; which equality predicate to use.  This can lead to unpredictable
;; results if they are used in consistenly, e.g., between `vhash-cons'
;; and `vhash-assoc', which is undesirable, as argued in
;; http://savannah.gnu.org/bugs/?22159 .  OTOH, two arguments can be
;; made in favor of this API:
;;
;;  - It's consistent with how alists are handled in SRFI-1.
;;
;;  - In practice, users will probably consistenly use either the `q',
;;    the `v', or the plain variant (`vlist-cons' and `vlist-assoc'
;;    without any optional argument), i.e., they will rarely explicitly
;;    pass a hash function or equality predicate.
*/


int vhash_p(SCM obj)
{
  // "Return true if OBJ is a hash list."
  if(vlist_p(obj))
    if(block_hash_table_p(vlist_base(obj)))
      return 1;
  return 0;
}

void vhash_truncate_x(SCM vhash)
{
  int  offset  = vlist_offset(vhash);
  SCM *base    = vlist_base(vhash);
  SCM *content = dB(block_content(base));

  ulong freeref = my_scm_to_ulong(block_next_free_ref(base));
  ulong count   = REFCOUNT(freeref);

  ulong seq = my_scm_to_ulong(scm_fluid_ref(thread_seq_number));
  ulong thr = my_scm_to_ulong(scm_fluid_ref(thread_id));
  ulong st = block_size_thr(base);
  ulong os = block_offset_seq(base);

  int size = SIZE(st);

  if(!count && THR(st) == thr && SEQ(os) == seq)
    {
      for(--freeref; freeref > offset; freeref--)
	{
          freeref = NEXTFREE(freeref);
	  ulong os = my_scm_to_ulong
	    (block_hash_table_next_offset(content, size, freeref));
	  int off  = NEXT_REF(os);
	  int khsh = BACKREF_REF(os);
	  content[size + freeref] = SCM_BOOL_F;
	  content[       freeref] = SCM_BOOL_F;
	  block_hash_table_set_s(content, size, khsh, off);
	}  
      block_next_free_ref(base) = my_scm_from_int(offset + 1);    
    }
}

void vhash_block_clear(SCM *base)
{
  SCM *content = dB(block_content(base));

  ulong freeref = my_scm_to_ulong(block_next_free_ref(base));

  ulong st = block_size_thr(base);
  int size = SIZE(st);

  for(--freeref; freeref >= 0; freeref--)
    {
      content[2*size + freeref] = SCM_BOOL_F;
      content[size   + freeref] = SCM_BOOL_F;
      content[         freeref] = SCM_BOOL_F;
    }  
  block_next_free_ref(base) = my_scm_from_int(0);    
}

SCM vhash_cons_adv(SCM key, SCM value, SCM vhash, SCM (*hash)(SCM, SCM),
		   int cachep)
{
  // "Return a new hash list based on VHASH where KEY is associated
  //  with VALUE.  Use HASH to compute KEY's hash."

  
  //;; We should also assert that it is a hash table.  Need to check the
  //;; performance impacts of that.  Also, vlist-null is a valid hash
  //;; table, which does not pass vhash?.  A bug, perhaps.
 
  SCM  vhash2   = block_cons(key, vhash, VHASH, cachep);
  SCM  *base    = vlist_base(vhash2);
  int  offset   = vlist_offset(vhash2);
  int  size     = block_size(base);
  int  khash    = my_scm_to_int(hash(key, my_scm_from_int(size)));
  SCM  *content = dB(block_content(base));
    
  content[size + offset] = value;

  block_hash_table_add_s(content, size, khash, offset);
    
  return vhash2;
}

#define vhash_cons_adv_mac(ret,key, value, vhash, hash, cachep)		\
{									\
  SCM  vhash2   = block_cons(key, vhash, VHASH, cachep);		\
  SCM  *base    = vlist_base(vhash2);					\
  int  offset   = vlist_offset(vhash2);					\
  int  size     = block_size(base);					\
  int  khash    = hash(key, size);					\
  SCM  *content = dB(block_content(base));				\
									\
  content[size + offset] = value;					\
									\
									\
  block_hash_table_add_s(content, size, khash, offset);			\
									\
  ret = vhash2;								\
}

#define vhash_cons_adv_mac_l(key, value, l, hash)			\
{									\
  SCM *base  = GP_GETREF(l[1]);						\
  int offset = SCM_UNPACK(l[2]);					\
  block_cons_l(key, l,  base, offset, VHASH);				\
  offset = SCM_UNPACK(l[2]);						\
  base   = GP_GETREF(l[1]);						\
  int  size     = block_size(base);					\
  int  khash    = hash(key, size);					\
  SCM  *content = dB(block_content(base));				\
									\
  content[size + offset] = value;					\
									\
  block_hash_table_add_s(content, size, khash, offset);			\
}


SCM vhash_cons(SCM key, SCM value, SCM vhash)
{
  return vhash_cons_adv(key, value, vhash, scm_hash, 0);
}

#define EQ(x,y)   scm_is_eq(x,y)
#define HASHQ_UNIFY(x,n) ((SCM_UNPACK (x) >> 4) % n)
#define HASHQ(x,n)       ((SCM_UNPACK (x) >> 1) % n)
SCM hashq(SCM x, SCM n)
{
  return my_scm_from_int((SCM_UNPACK(x) >> 1) % scm_to_int(n));
}

SCM vhash_consq(SCM key, SCM value, SCM vhash)
{
  SCM retval;
  vhash_cons_adv_mac(retval, key, value, vhash, HASHQ, 0);
  return retval;
}

static inline int gp_is_eqa(SCM x, SCM y)
{
 retry:
  if(SCM_CONSP(x))
    {
      if(SCM_CONSP(y))
	{
	  if(scm_is_eq(SCM_CAR(x),SCM_CAR(y)))
	    {
	      //printf("eq,");
	      x = SCM_CDR(x);
	      y = SCM_CDR(y);
	      goto retry;
	    }
	  else
	    {
	      //printf("fail1\n");
	      return 0;
	    }
	}
      else
	{
	  //printf("fail2\n");
	  return 0;
	}
    }
  else if(SCM_CONSP(y))
    {
      //printf("fail3\n");
      return 0;
    }
  else
    {
      //printf("\n");
      return 1;
    }
}

static inline scm_t_bits hasha(SCM x, scm_t_bits n)
{
  scm_t_bits ret = 0;
 retry:
  if(SCM_CONSP(x))
    {
      scm_t_bits new = HASHQ(SCM_CAR(x), n);
      ret = ((ret ^ new)  + (ret << 4) + (ret << 8)) % n;
      x = SCM_CDR(x);
      goto retry;
    }
  //printf("hasha %d\n", ret);
  return ret;
}

static inline scm_t_bits hasha_unify(SCM x, scm_t_bits n)
{
  scm_t_bits ret = 0;
 retry:
  if(SCM_CONSP(x))
    {
      scm_t_bits new = HASHQ_UNIFY(SCM_CAR(x), n);
      ret = ((ret ^ new) + (ret << 4) + (ret << 8)) % n;
      x = SCM_CDR(x);
      goto retry;
    }

  return ret;
}

#define EQA(x,y)         (gp_is_eqa(x,y))
#define HASHA_UNIFY(x,n) (hasha_unify(x,n))
#define HASHA(x,n)       (hasha(x,n))

SCM vhash_consa(SCM key, SCM value, SCM vhash)
{
  SCM retval;
  vhash_cons_adv_mac(retval, key, value, vhash, HASHA, 0);
  return retval;
}

/*
We will keep a possibly cleared super block in cache in case we shrink so that 
we do not suffer at block border.
*/

void touch_cache_work(SCM vh, SCM cvh, SCM *vh_base, SCM *cvh_base)
{
  int off   = vlist_offset(cvh);
  int touch = 0;
  int found = 0;
  int  vh_s = block_size( vh_base);
  int cvh_s = block_size(cvh_base);
  
  while(cvh_s > vh_s)
    {
      SCM *base = block_base(cvh_base);
      
      if(base == vh_base) 
	{
	  found = 1;
	  break;
	}

      off      = block_offset(cvh_base);
      cvh_base = base;
      touch    = 1;
      cvh_s    = block_size(cvh_base);
    }

  if(!found)
    {
      scm_fluid_set_x(vhash_cache, vh);
      return;
    }

  {
    ulong seq = my_scm_to_ulong(scm_fluid_ref(thread_seq_number));
    ulong thr = my_scm_to_ulong(scm_fluid_ref(thread_id));

    if(off != 0 && ALLOWED_BLOCK(cvh_base, seq, thr))
      {

	SCM new_vh = make_vlist(*cvh_base, 0);
	vhash_block_clear(cvh_base);
	scm_fluid_set_x(vhash_cache, new_vh);
	return;
      }

    if(!ALLOWED_BLOCK(cvh_base, seq, thr))
      {
	if(!ALLOWED_BLOCK(vh_base, seq, thr))
	  scm_fluid_set_x(vhash_cache, vlist_null);
	else
	  scm_fluid_set_x(vhash_cache, vh);
	
	return;
      }
	
    if(touch)
      {
	SCM new_vh = make_vlist(*cvh_base, 0);
	scm_fluid_set_x(vhash_cache, new_vh);
	return;
      }
    

    if(!ALLOWED_BLOCK(vh_base, seq, thr))
      scm_fluid_set_x(vhash_cache, vlist_null);
    else
      scm_fluid_set_x(vhash_cache, vh);
    return;
  }
}

static inline void touch_cache(SCM vh)
{
  SCM   cvh       = scm_fluid_ref(vhash_cache);
  SCM  *cvh_base  = vlist_base(cvh);
  SCM   *vh_base  = vlist_base(vh);
  
  return;

  if(cvh_base == vh_base) return;

  touch_cache_work(vh, cvh, vh_base, cvh_base);
}

SCM vhash_consq_unify(SCM key, SCM value, SCM vhash)
{
  SCM retval;
  vhash_cons_adv_mac(retval, key, value, vhash, HASHQ_UNIFY, 1);
  touch_cache(retval);
  return retval;
}

SCM vhash_consq_l(SCM key, SCM value, SCM *l)
{
  vhash_cons_adv_mac_l(key, value, l, HASHQ_UNIFY);
  touch_cache(l[0]);
  return SCM_UNSPECIFIED;
}

SCM vhash_consv(SCM key, SCM value, SCM vhash)
{
  return vhash_cons_adv(key, value, vhash, scm_hashv, 0);
}

SCM vhash_fold_exp(SCM (*proc)(SCM, SCM), SCM init, SCM key, SCM vhash, 
		   SCM (*equal)(SCM,SCM), SCM (*hash)(SCM, SCM))
{
  if (block_size(vlist_base(vhash)) <= 0)
    return init;

  //  ;; Fold over all the values associated with KEY in VHASH.
  SCM *base       = vlist_base(vhash);
  int  max_offset = vlist_offset(vhash);
  int size,khash;
  SCM *content, offset_s;

 lp:
  size    = block_size(base);
  content = dB(block_content(base));
  khash   = my_scm_to_int(hash(key, my_scm_from_int(size)));

  offset_s = block_hash_table_ref(content, size, khash);

  offset_s = block_hash_table_ref(content, size, khash);  
  int offset;
  if(scm_is_true(offset_s))
    offset = -1;
  else
    offset = NEXT_REF(my_scm_to_ulong(offset_s));
  
 loop:
  if(offset >= 0)
    {
      if ((offset <= max_offset) &&
	  scm_is_true(equal(key,block_ref(content, offset))))
	{
	  init = proc(block_ref(content,offset + size), init);
	}
      
      offset_s = block_hash_table_next_offset(content, size, offset);
      offset = my_scm_to_int(offset_s);
      goto loop;
    }
  else
    {      
      SCM* next_block = block_base(base);
      if (block_size(next_block) > 0)
	{
	  max_offset = block_offset(base);
	  base       = next_block;
	  goto lp;
	}
    }

  return init;
}

SCM vhash_fold_all_exp(SCM data, SCM (*proc)(SCM, SCM, SCM, SCM), 
                       SCM seed,SCM vhash)
{
  SCM *base       = vlist_base(vhash);
  int  size       = block_size(base);
  if (size <= 0)
    return seed;

  //  ;; Fold over all the values associated with KEY in VHASH.
  
  int  offset     = vlist_offset(vhash);
 lp:
  {
    SCM *content    = dB(block_content(base));
    while(offset >= 0)
      {
        SCM key = content[offset       ];
        SCM val = content[offset + size];
        seed = proc(key,val,seed,data);
        offset--;
      }

    offset = block_offset(base);
    base   = block_base(base);
    if (block_size(base) > 0)
      {
        size = block_size(base);
        goto lp;
      }  
    
    return seed;
  }
}

SCM vhash_fold(SCM (*proc)(SCM,SCM), SCM init, SCM key, SCM vhash)
{
  return vhash_fold_exp(proc, init, key, vhash, scm_equal_p, scm_hash);
}

SCM vhash_foldq(SCM (*proc)(SCM,SCM), SCM init, SCM key, SCM vhash)
{
  //"Same as ‘vhash-fold*’, but using ‘hashq’ and ‘eq?’."
  return vhash_fold_exp(proc, init, key, vhash, scm_eq_p, scm_hashq);
}

SCM vhash_foldv(SCM (*proc)(SCM,SCM), SCM init, SCM key, SCM vhash) 
{
  //"Same as ‘vhash-fold*’, but using ‘hashv’ and ‘eqv?’."
  return vhash_fold_exp(proc, init, key, vhash, scm_eqv_p, scm_hashv);
}

SCM vhash_assoc_exp(SCM key, SCM vhash,
		    SCM (*equal)(SCM,SCM), SCM (*hash) (SCM, SCM))
{

  // A specialization of `vhash-fold*' that stops when the first value
  // associated with KEY is found or when the end-of-list is reached.  Static Inline to
  // make sure `vhash-assq' gets to use the `eq?' instruction instead of calling
  // the `eq?'  subr.
  
  if (block_size(vlist_base(vhash)) <= 0)
    return SCM_UNSPECIFIED;

  //  ;; Fold over all the values associated with KEY in VHASH.
  SCM *content, offset_s;
  int size, khash, offset;

  int max_offset = vlist_offset(vhash);
  SCM *base      = vlist_base(vhash);
 lp:
  size    = block_size(base);
  content = dB(block_content(base));
  khash   = my_scm_to_int(hash(key, my_scm_from_int(size)));

  offset_s = block_hash_table_ref(content, size, khash);  

  if(scm_is_false(offset_s))
    offset = -1;
  else
    offset = NEXT_REF(my_scm_to_ulong(offset_s));

 loop:
  if(offset >= 0)
    {      
      SCM key2  = block_ref(content, offset);
      if ((offset <= max_offset) &&
	  scm_is_true(equal(key,key2)))
	{
	  return block_ref(content, offset + size);
	}
      else
        {
	  offset_s = block_hash_table_next_offset(content, size, offset);
          offset = NEXT_REF(my_scm_to_ulong(offset_s));      
	  goto loop;
	}
    }
  else
    {      
      SCM* next_block = block_base(base);
      if (next_block && block_size(next_block) > 0)
	{
	  max_offset = block_offset(base);
	  base       = next_block;
	  goto lp;
	}
      else
        return SCM_UNSPECIFIED;
    }
}

SCM vhash_set_exp_x(SCM key, SCM val, SCM vhash,
		    SCM (*equal)(SCM,SCM), SCM (*hash) (SCM, SCM))
{

  // A specialization of `vhash-fold*' that stops when the first value
  // associated with KEY is found or when the end-of-list is reached.  Static Inline to
  // make sure `vhash-assq' gets to use the `eq?' instruction instead of calling
  // the `eq?'  subr.
  
  if (block_size(vlist_base(vhash)) <= 0)
    {
      return vhash_cons_adv(key,val,vhash,hash,0);
    }

  //  ;; Fold over all the values associated with KEY in VHASH.
  SCM *content, offset_s;
  int size, khash, offset;

  int max_offset = vlist_offset(vhash);
  SCM *base      = vlist_base(vhash);
 lp:
  size    = block_size(base);
  content = dB(block_content(base));
  khash   = my_scm_to_int(hash(key, my_scm_from_int(size)));

  offset_s = block_hash_table_ref(content, size, khash);  

  if(scm_is_false(offset_s))
    offset = -1;
  else
    offset = NEXT_REF(my_scm_to_ulong(offset_s));

 loop:
  if(offset >= 0)
    {      
      SCM key2  = block_ref(content, offset);
      if ((offset <= max_offset) &&
	  scm_is_true(equal(key,key2)))
	{
	  block_set_x(content, offset + size, val);
	  return vhash;
	}
      else
        {
	  offset_s = block_hash_table_next_offset(content, size, offset);
          offset = NEXT_REF(my_scm_to_ulong(offset_s));      
	  goto loop;
	}
    }
  else
    {      
      SCM* next_block = block_base(base);
      if (next_block && block_size(next_block) > 0)
	{
	  max_offset = block_offset(base);
	  base       = next_block;
	  goto lp;
	}
      else
	return vhash_cons_adv(key,val,vhash,hash,0);
    }
}

#define vhash_assoc_exp_mac(key, vhash, equal, hash)                    \
{                                                                       \
                                                                        \
  if (block_size(vlist_base(vhash)) <= 0)                               \
    return SCM_UNSPECIFIED;						\
                                                                        \
  SCM *content, offset_s;                                               \
  int size, khash, offset;                                              \
                                                                        \
  int max_offset = vlist_offset(vhash);                                 \
  SCM *base      = vlist_base(vhash);                                   \
 lp:                                                                    \
  size    = block_size(base);                                           \
  content = dB(block_content(base));                                    \
  khash   = hash(key, size);                                            \
                                                                        \
  offset_s = block_hash_table_ref(content, size, khash);                \
  if(scm_is_false(offset_s))						\
    offset = -1;                                                        \
  else                                                                  \
    offset = NEXT_REF(my_scm_to_ulong(offset_s));                       \
                                                                        \
loop:                                                                   \
  if(offset >= 0)							\
    {                                                                   \
      SCM key2  = block_ref(content, offset);				\
                                                                        \
      if ((offset <= max_offset) &&                                     \
	  equal(key,key2))						\
	{                                                               \
          return block_ref(content,size + offset);			\
	}                                                               \
      else                                                              \
	{                                                               \
	  offset_s = block_hash_table_next_offset(content, size, offset);\
          offset = NEXT_REF(my_scm_to_ulong(offset_s));                 \
	  goto loop;                                                    \
	}                                                               \
    }                                                                   \
  else                                                                  \
    {                                                                   \
      SCM* next_block = block_base(base);                               \
      if (next_block && block_size(next_block) > 0)                     \
	{                                                               \
	  max_offset = block_offset(base);                              \
	  base       = next_block;                                      \
	  goto lp;                                                      \
	}                                                               \
      else                                                              \
        return SCM_UNSPECIFIED;						\
    }                                                                   \
}

#define vhash_assoc_exp_mac_l(key, base, off, equal, hash)		\
{                                                                       \
                                                                        \
  if (block_size(base) <= 0)						\
    return SCM_UNSPECIFIED;						\
  SCM *content, offset_s;                                               \
  int size, khash, offset;                                              \
                                                                        \
  int max_offset = off;							\
									\
 lp:                                                                    \
  size    = block_size(base);                                           \
  content = dB(block_content(base));                                    \
  khash   = hash(key, size);                                            \
                                                                        \
  offset_s = block_hash_table_ref(content, size, khash);                \
  if(scm_is_false(offset_s))						\
    offset = -1;                                                        \
  else                                                                  \
    offset = NEXT_REF(my_scm_to_ulong(offset_s));                       \
                                                                        \
loop:                                                                   \
  if(offset >= 0)							\
    {                                                                   \
      SCM key2  = block_ref(content, offset);				\
                                                                        \
      if ((offset <= max_offset) &&                                     \
	  equal(key,key2))						\
	{                                                               \
          return block_ref(content,size + offset);			\
	}                                                               \
      else                                                              \
	{                                                               \
	  offset_s = block_hash_table_next_offset(content, size, offset);\
          offset = NEXT_REF(my_scm_to_ulong(offset_s));                 \
	  goto loop;                                                    \
	}                                                               \
    }                                                                   \
  else                                                                  \
    {                                                                   \
      SCM* next_block = block_base(base);                               \
      if (next_block && block_size(next_block) > 0)			\
	{                                                               \
	  max_offset = block_offset(base);                              \
	  base       = next_block;                                      \
	  goto lp;                                                      \
	}                                                               \
      else                                                              \
        return SCM_UNSPECIFIED;						\
    }                                                                   \
}

SCM vhash_assoc(SCM key, SCM vhash)
{
  // "Return the first key/value pair from VHASH whose key is equal to
  return vhash_assoc_exp(key, vhash, scm_equal_p, scm_hash);
}

SCM vhash_set_x(SCM key, SCM val, SCM vhash)
{
  // "Return the first key/value pair from VHASH whose key is equal to
  return vhash_set_exp_x(key, val, vhash, scm_equal_p, scm_hash); 
}

SCM vhash_assq(SCM key, SCM vhash)
{
  // "Return the first key/value pair from VHASH whose key is equal to
  //vhash_assoc_exp_mac(key, vhash, equal, hash)
  vhash_assoc_exp_mac(key, vhash, EQ, HASHQ);
}

SCM vhash_setq_x(SCM key, SCM val, SCM vhash)
{
  // "Return the first key/value pair from VHASH whose key is equal to
  //vhash_assoc_exp_mac(key, vhash, equal, hash)
  return vhash_set_exp_x(key, val, vhash, scm_eq_p, hashq);
}

SCM vhash_assa(SCM key, SCM vhash)
{
  // "Return the first key/value pair from VHASH whose key is equal to
  //vhash_assoc_exp_mac(key, vhash, equal, hash)
  vhash_assoc_exp_mac(key, vhash, EQA, HASHA);
}

SCM vhash_assq_unify(SCM key, SCM vhash)
{
  // "Return the first key/value pair from VHASH whose key is equal to
  //vhash_assoc_exp_mac(key, vhash, equal, hash)
  vhash_assoc_exp_mac(key, vhash, EQ, HASHQ_UNIFY);
}

SCM vhash_assq_l(SCM key, SCM *l, SCM *base, int off)
{
  // "Return the first key/value pair from VHASH whose key is equal to
  //vhash_assoc_exp_mac(key, vhash, equal, hash)
 
  touch_cache(*l);
  vhash_assoc_exp_mac_l(key, base, off, EQ, HASHQ_UNIFY);
}

SCM vhash_assv(SCM key, SCM vhash)
{
  // "Return the first key/value pair from VHASH whose key is equal to
  return vhash_assoc_exp(key, vhash, scm_eqv_p, scm_hashv);
}

SCM vhash_setv_x(SCM key, SCM val, SCM vhash)
{
  // "Return the first key/value pair from VHASH whose key is equal to
  //vhash_assoc_exp_mac(key, vhash, equal, hash)
  return vhash_set_exp_x(key, val, vhash, scm_eqv_p, scm_hashv);
}

SCM vhash_delete_exp(SCM key, SCM vhash, SCM(*equal)(SCM,SCM),
		     SCM (*hash)(SCM, SCM))
{
  //  "Remove all associations from VHASH with KEY, comparing keys
  //   with EQUAL?."
  SCM pair = vhash_assoc_exp(key,vhash,equal,hash);
  if (scm_is_true(pair))
    {
#define HDELETE(ret,kv,seed)                            \
      {                                                 \
	SCM k = SCM_CAR(kv);                            \
	SCM v = SCM_CDR(kv);                            \
	if(scm_is_true(equal(k, key)))                  \
	  {                                             \
	    ret = seed;                                 \
	  }                                             \
	else                                            \
	  {                                             \
	    ret = vhash_cons_adv(k,v,seed,hash,0);	\
	  }                                             \
      }

      SCM ret;
      vlist_fold_mac(ret, HDELETE, vlist_null, vhash);
      return ret;
    }

  return vhash;
}

SCM vhash_del(SCM key, SCM vhash)
{
  return vhash_delete_exp(key,vhash, scm_equal_p, scm_hash);
}

SCM vhash_delq(SCM key, SCM vhash)
{
  return vhash_delete_exp(key,vhash, scm_eq_p, scm_hashq);
}

SCM vhash_delv(SCM key, SCM vhash)
{
  return vhash_delete_exp(key,vhash, scm_eqv_p, scm_hashv);
}

SCM vhash_fold_data(SCM data, SCM (*proc)(SCM,SCM,SCM,SCM), SCM init, SCM vhash)
{
  
  // "Fold over the key/pair elements of VHASH from left to right, with
  // each call to PROC having the form ‘(PROC key value result)’,
  // where RESULT is the result of the previous call to PROC and
  // INIT the value of RESULT for the first call to PROC."
  
#define VHFOLD(ret,kv,seed)				\
  {							\
    ret = proc(data, SCM_CAR(kv), SCM_CDR(kv), seed);	\
  }

  SCM ret;
  vlist_fold_mac(ret, VHFOLD, init, vhash);
  return ret;
}

SCM vhash_fold_right_data(SCM data, SCM (*proc)(SCM,SCM,SCM,SCM), SCM init, 
                          SCM vhash)
{
  
  //  "Fold over the key/pair elements of VHASH from right to left, with
  // each call to PROC having the form ‘(PROC key value result)’,
  // where RESULT is the result of the previous call to PROC and
  // INIT the value of RESULT for the first call to PROC."
   
#define VHFOLD2(ret,kv,seed)				\
  {							\
    ret = proc(data, SCM_CAR(kv), SCM_CDR(kv), seed);	\
  }

  SCM ret;
  vlist_fold_right_mac(ret, VHFOLD2, init, vhash);

  return ret;
}

SCM VHFOLD_ASSOC(SCM k, SCM v, SCM seed, SCM bin_there)
{
  SCM is_referenced = scm_hash_ref(bin_there, k,                      
                                   SCM_BOOL_F);                       
  if(scm_is_true(is_referenced))                                      
    return seed;                                                       
  else                                                                
    { 
      scm_hash_set_x(bin_there, k, SCM_BOOL_T);        
      return scm_cons(scm_cons(k,v), seed);                                   
    }                                                                 
}


SCM vhash_to_assoc(SCM vhash)
{
  int n         = vlist_length(vhash);
  SCM bin_there = scm_c_make_hash_table(2*n);
  SCM ret = vhash_fold_all_exp(bin_there, VHFOLD_ASSOC, SCM_EOL, vhash);
  return scm_reverse_x(ret,SCM_EOL);
}

SCM vlist_fold_stub(SCM F, SCM X, SCM S)
{
  return scm_call_2(F,X,S);
}

SCM vhash_fold_stub(SCM F, SCM K, SCM V,  SCM S)
{
  return scm_call_3(F,K,V,S);
}

SCM vlist_map_stub(SCM F, SCM X)
{
  return scm_call_1(F,X);
}

static inline void assert_vhash(SCM val)
{
  if(!(vhash_p(val)))
    scm_misc_error("wrong-type-arg", 
		   "Not a vhash: ~S",
		   scm_list_1(val));
}

SCM setup(SCM type)
{
  vlist_type = type;
  vlist_null = make_vlist(block_null, 0);
  scm_fluid_set_x(vhash_cache, vlist_null);  
  return scm_cons(vlist_null,
         scm_cons(block_growth_factor,
         scm_cons(init_block_size,
         scm_cons(thread_seq_number, 
         scm_cons(thread_id, 
                  scm_cons(thread_inc, SCM_EOL)))))); 
}

//-----------------------------------------------------------------
