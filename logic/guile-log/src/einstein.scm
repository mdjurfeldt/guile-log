(use-modules (language clambda clambda))
(use-modules (language clambda scm))
(use-modules (language clambda logic))
(use-modules (language clambda fmt))

(define gp-unify!        (mk-var 'coid '_gp_unify     #f 'no-gensym))
(define gp-unify-raw!    (mk-var 'coid '_gp_unify_raw #f 'no-gensym))
(define gp-m-unify       (mk-var 'coid '_gp_m_unify   #f 'no-gensym))

(define gp-unwind        (mk-var 'coid '_gp_unwind    #f 'no-gensym))
(define gp-newframe      (mk-var 'coid '_gp_newframe  #f 'no-gensym))
(define gp-var!          (mk-var 'coid '_gp_mkvar     #f 'no-gensym))

(define gp-lookup        (mk-var 'coid '_gp_lookup    #f 'no-gensym))
(define gp-pair!?        (mk-var 'coid '_gp_pair_bang #f 'no-gensym))
(define gp-pair?         (mk-var 'coid '_gp_pair      #f 'no-gensym))
(define gp-null!?        (mk-var 'coid '_gp_null_bang #f 'no-gensym))
(define gp-null?         (mk-var 'coid '_gp_null      #f 'no-gensym)) 
(define gp-car           (mk-var 'coid '_gp_car       #f 'no-gensym))
(define gp-cdr           (mk-var 'coid '_gp_cdr       #f 'no-gensym))
(define gp->scm          (mk-var 'coid '_gp_2_scm     #f 'no-gensym))
(define gp-pair+         (mk-var 'coid '_gp_pair_plus  #f 'no-gensym))
(define gp-pair*         (mk-var 'coid '_gp_pair_star  #f 'no-gensym))
(define gp-pair-         (mk-var 'coid '_gp_pair_minus #f 'no-gensym))

(init-clambda-scm)
(auto-defs)

(<global> SCM member   (<scm> #f))
(<global> SCM right    (<scm> #f))
(<global> SCM einstein (<scm> #f))
(<global> SCM next-to  (<scm> #f))
(<global> SCM ein2     (<scm> #f))

(<global> SCM a1    (<scm> #f))
(<global> SCM a2    (<scm> #f))
(<global> SCM a3    (<scm> #f))
(<global> SCM a4a   (<scm> #f))
(<global> SCM a4b   (<scm> #f))
(<global> SCM a5    (<scm> #f))
(<global> SCM a6    (<scm> #f))
(<global> SCM a7    (<scm> #f))
(<global> SCM a8a   (<scm> #f))
(<global> SCM a8b   (<scm> #f))
(<global> SCM a9    (<scm> #f))
(<global> SCM a10a  (<scm> #f))
(<global> SCM a10b  (<scm> #f))
(<global> SCM a11a  (<scm> #f))
(<global> SCM a11b  (<scm> #f))
(<global> SCM a12   (<scm> #f))
(<global> SCM a13   (<scm> #f))
(<global> SCM a14a  (<scm> #f))
(<global> SCM a14b  (<scm> #f))
(<global> SCM a15   (<scm> #f))


(:define: (memb x l)
  (:match: (l)
    ((x . _)  (:cc:))
    ((_ . ,l) (:call: member x l))))

(:define: (righ x y l)
  (:match: (l)
    ((x y .  _) (:cc:))
    ((_   . ,l) (:call: right x y l))))

(:define: (nextto item1 item2 rest)
  (:or: (:call: right item1 item2 rest)
        (:call: right item2 item1 rest)))

#|
(:define: (ein h)
 (:code:
  (<=> a1    (<scm> `(brit  ,_  ,_ ,_    red)))
  (<=> a2    (<scm> `(swede dog ,_ ,_    ,_ )))
  (<=> a3    (<scm> `(dane  ,_  ,_ tea  ,_  )))
  (<=> a4a   (<scm> `(,_ ,_ ,_ ,_ green)))
  (<=> a4b   (<scm> `(,_ ,_ ,_ ,_ white)))
  (<=> a5    (<scm> `(,_    ,_  ,_ cofee green)))
  (<=> a6    (<scm> `(,_    bird pallmall ,_ ,_)))
  (<=> a7    (<scm> `(,_    ,_   dunhill ,_ yellow)))
  (<=> a8a   (<scm> `(,_ ,_ dunhill ,_ ,_)))
  (<=> a8b   (<scm> `(,_ horse ,_ ,_ ,_)))
  (<=> a9    (<scm> `(,_ ,_ ,_ milk ,_)))
  (<=> a10a  (<scm> `(,_ ,_ marlboro ,_ ,_)))
  (<=> a10b  (<scm> `(,_ cat ,_ ,_ ,_)))
  (<=> a11a  (<scm> `(,_ ,_ marlboro ,_ ,_)))
  (<=> a11b  (<scm> `(,_ ,_ ,_ water ,_)))
  (<=> a12   (<scm> `(,_ ,_ winfield beer ,_)))
  (<=> a13   (<scm> `(german ,_ rothmans ,_ ,_)))
  (<=> a14a  (<scm> `(norwegian ,_ ,_ ,_ ,_)))
  (<=> a14b  (<scm> `(,_ ,_ ,_ ,_ blue)))
  (<=> a15   (<scm> `(,_ fish ,_ ,_ ,_))))
 (:=: h (<scm> `((norwegian ,_ ,_ ,_ ,_) ,_ (,_ ,_ ,_ milk ,_) ,_ ,_)))
 (:call: member a1 h)   
 (:call: member  a2 h)
 (:call: member  a3 h)
 (:call: right a4a a4b h)
 (:call: member a5 h)
 (:call: member a6 h)
 (:call: ein2 h))

(:define: (e2 h)
  (:call: member a7 h)
  (:call: next-to  a8a a8b h)
  (:call: member a9 h)
  (:call: next-to  a10a a10b h)
  (:call: next-to  a11a a11b h)
  (:call: member  a12 h)
  (:call: member  a13 h)
  (:call: next-to a14a a14b h)
  (:call: member a15 h)
  )
|#

(<define> void init-einstein ()          
  (<=> next-to   (yield-log nextto))
  (<=> member    (yield-log memb))
  (<=> right     (yield-log righ))
  ;(<=> einstein (yield-log ein))
  ;(<=> ein2     (yield-log e2))
  (auto-inits))

(<define> SCM make-api-list ()
   (<call> init-einstein)
   (<scm> `((gp-unify!          . ,(yield-log gp-unify!))
            (gp-unify-raw!      . ,(yield-log gp-unify-raw!))
            (gp-m-unify         . ,(yield-log gp-m-unify))
            (gp-member          . ,member)
            (gp-right           . ,right)
            (gp-next-to         . ,next-to)
            (gp-unwind          . ,(yield-log gp-unwind))
            (gp-newframe        . ,(yield-log gp-newframe))
            (gp-var!            . ,(yield-log gp-var!))
            (gp-lookup          . ,(yield-log gp-lookup))
            (gp-pair!?          . ,(yield-log gp-pair!?))
            (gp-pair?           . ,(yield-log gp-pair?))
            (gp-null!?          . ,(yield-log gp-null!?))
            (gp-null?           . ,(yield-log gp-null?))
            (gp-car             . ,(yield-log gp-car))
            (gp-cdr             . ,(yield-log gp-cdr))
            (gp->scm            . ,(yield-log gp->scm))
            (gp-pair*           . ,(yield-log gp-pair*))
            (gp-pair-           . ,(yield-log gp-pair-))
            (gp-pair+           . ,(yield-log gp-pair+))
            )))
            
(clambda->c "einstein.c")


