
struct mapped_elf_image
{
  char *start;
  char *end;
  char *frame_maps;
};

SCM_DEFINE(gp_find_elf_relative_adress, "gp-bv-address", 1, 0, 0, 
	   (SCM bv),
	   "dives down in a gp variable one time")
#define FUNC_NAME s_gp_find_elf_relative_adress
{
  scm_t_uint64 ref = (scm_t_uint64) SCM_BYTEVECTOR_CONTENTS(bv);
  return scm_from_uintptr_t(ref);
}
#undef FUNC_NAME
