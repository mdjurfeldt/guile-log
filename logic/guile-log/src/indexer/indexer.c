#include <gmp.h>
#include <stdio.h>
#include "../dynlist.c" 
#include "../inh.c"

#if ((__GNUC__ >= 4) || ((__GNUC__ == 3) && (__GNUC_MINOR__ >= 4)))
    #define bsf(r, i)             \
        do {                      \
            r = __builtin_ctzl(i); \
        } while(0)
#else
    #define bsf(r, i)                                \
        do {                                         \
            asm("bsfl %1, %0" : "=r" (r) : "rm" (i)); \
        } while(0)
#endif


static inline int getNsol(scm_t_bits x)
{
  int i;
  bsf(i,x);
  if(i < 0)                   return 0;
  if( (x & ~(1UL << i)) == 0) return 1;
  return 2;
}

static inline SCM make_indexer()
{
  SCM ret = scm_c_make_vector(5, SCM_BOOL_F);
  scm_c_vector_set_x(ret, 2, my_scm_from_int(0));
  scm_c_vector_set_x(ret, 4, my_scm_from_int(0));
  return ret;
}

static inline SCM get_car(SCM* v)
{
  return v[0];
}

static inline SCM get_cdr(SCM *v)
{
  return v[1];
}

static inline SCM get_vars(SCM *v)
{
  return v[2];
}

static inline SCM get_atoms(SCM *v)
{
  return v[3];
}

static inline SCM get_all(SCM *v)
{
  return v[4];
}

static inline SCM get_in_strings(SCM *v)
{
  return v[5];
}

static inline SCM get_strings(SCM *v)
{
  return v[6];
}

static inline SCM get_bitmap(SCM *x)
{
  return x[0];
}

/*
inline SCM tree_union(SCM x, SCM y)
{
  int l1 = get_level(x);
  int l2 = get_level(y);
  
  if(l2 > l1)
    {
      SCM z = x;
      x = y;
      y = z;
      
      ulong lz = l1;
      l1 = l2;
      l2 = lz;
      goto less;
    }
  
  if(l1 < l2)
    {
    less:
      ulong bitmap = get_bitmap(x1);
      if(bitmap & 1)
	{
	  return consq(0, tree_union(assq(0, x2), x1), x2) ;
	}
      else
	{
	  return consq(0, make_level(l2 - 1, x2) , x1)
	}
    }

  //l1 = l2
  if(l1 > 0)
    {
      ulong b1 = get_bitmap(x1);
      ulong b2 = get_bitmap(x2);

      while(1)
	{
	  int n1 = first_bit(b1);
	  int n2 = first_bit(b2);
	  if(n1 > 1000 && n2 > 1000)
	    break;
	cont:
	  if(n1 > n2)
	    {
	      n2 = first_bit(b2);
	      goto cont:
	    }
	  if(n1 < n2)
	    {	    
	      x2 = consq(n1, assq(n1, x1));
	      n1 = first_bit(b1);
	      goto cont;
	    }
	  x2 = consq(n2, tree_union(assq(n1, x1), assq(n2, x2)), x2);
	}
      return x2;
    }

  return bit_union(x1,x2);
}

static inline SCM tree_intersection(SCM x, SCM y)
{
  int l1 = get_level(x);
  int l2 = get_level(y);
  
  if(l2 > l1)
    {
      SCM z = x;
      x = y;
      y = z;
      
      ulong lz = l1;
      l1 = l2;
      l2 = lz;
      goto less;
    }
  
  if(l1 < l2)
    {
    less:
      ulong bitmap = get_bitmap(x1);
      if(bitmap & 1)
	{
	  return consq(0, tree_intersection(assq(0, x2), x1), x2) ;
	}
      else
	{
	  return x2;
	}
    }

  //l1 = l2
  if(l1 > 0)
    {
      ulong b1 = get_bitmap(x1);
      ulong b2 = get_bitmap(x2);

      while(1)
	{
	  int n1 = first_bit(b1);
	  int n2 = first_bit(b2);
	  if(n1 > 1000 && n2 > 1000)
	    break;
	cont:
	  if(n1 > n2)
	    {
	      x2 = clearq(n2, x2);
	      n2 = first_bit(b2);
	      goto cont:
	    }
	  if(n1 < n2)
	    {	    
	      n1 = first_bit(b1);
	      goto cont;
	    }
	  x2 = consq(n2, tree_intersection(assq(n1, x1), assq(n2, x2)), x2);
	}
      return x2;
    }
  
  return bit_intersection(x1,x2);
}
*/

#define MK_ACC(dlink_car, get_car)              \
  static inline SCM dlink_car(SCM *dlink, int mod)     \
  {                                             \
    SCM v = get_car(dlink);                     \
    if(mod)                                     \
      if(scm_is_true(v))                        \
        return v;                               \
      else                                      \
        return make_indexer();                  \
    else                                        \
      return v;                                 \
  }
MK_ACC(dlink_car, get_car)
MK_ACC(dlink_cdr, get_cdr)

static inline SCM get_fs_from_atoms(SCM a, SCM *dlink)
{
  SCM r = SCM_BOOL_F;
  if(scm_is_true(scm_string_p(a)))
    r = get_strings(dlink);
  else
    r = get_atoms(dlink);
  
  SCM w = my_scm_from_int(0);
  if(scm_is_true(r))
    {      
      SCM ww  = vhash_assoc(a,r);
      if(!scm_is_eq(ww, SCM_UNSPECIFIED))
	w = ww;
    }
  
  if(scm_is_true(scm_procedure_p(a)))
    {
      SCM rr = get_in_strings(dlink);
      if(scm_is_true(rr))
	{
	  SCM aa  = gp_procedure_name(a);
          if(scm_is_false(aa))
            {
            }
          else
            {
              aa = scm_symbol_to_string(aa);
              SCM ww  = vhash_assoc(aa,rr);
              if(!scm_is_eq(ww, SCM_UNSPECIFIED))
                {
                  w = scm_logior(w, ww); 
                }
            }
	}
    }

  return w;
}

/* 
// Nice, slow and easy, really, this funciton does not need to be in C
SCM get_index_set(SCM s, SCM f, SCM e, SCM db)
{  
  if(scm_is_false(db)) return f;

  if(SCM_CONSP(e))
    {
      SCM x  = SCM_CAR(e);
      SCM l  = SCM_CDR(e);
      SCM a1 = get_index_set(s,x,f,dlink_car(db,0));
      SCM a2 = get_index_set(s,l,a2,dlink_cdr(db,0));
      SCM vr = scm_logand(f, get_vars(db));
      return scm_logior(a2,vr);      
    }

  if(scm_is_true(gp_varp(e,s)))
    return scm_logand(f, get_all(db));

  return scm_logand(f, scm_logior(get_vars(db), get_fs_from_atoms(e, db)));    
}
*/

static inline int MAX(int i, int j)
{
  if(i < j)
    return j;
  else
    return i;
}

static inline int MIN(int i, int j)
{
  if(i > j)
    return j;
  else
    return i;
}

SCM and_tag_p        = SCM_BOOL_F;
SCM or_tag_p         = SCM_BOOL_F;
SCM not_tag_p        = SCM_BOOL_F;
SCM predicate_tag_p  = SCM_BOOL_F;
SCM plus_tag_p       = SCM_BOOL_F;
SCM minus_tag_p      = SCM_BOOL_F;
SCM set_tag_p        = SCM_BOOL_F;

typedef int (*predicate_t)(SCM,SCM);

predicate_t predicates[1000];

SCM gp_type_attribute_tag  = SCM_BOOL_F;

// #define DB(X) X
// Fast but horribly hard really, this need to be in C
void get_index_set_0(SCM s, SCM e, SCM db_, int *n, ulong *data, int isPlus, SCM trset)
{
  ulong data2[200];
  int n2;
  SCM *db;
  
 redo:
  e = gp_gp_lookup(e,s);


  if(SCM_VARIABLEP(e))
    {
      e = SCM_VARIABLE_REF(e);
      goto redo;
    }
  
  //printf("n=%d, data[0] = %p datat[1]=%p\n", *n, data[0], data[1]);
  //gp_format2("index for ~a db ~a~%", e, db_);fflush(stdout);
  
  db = dB(db_);

  if(scm_is_false(db_)) 
    {
      data[0] = 0UL;
      *n = 1;
      return;
    }

  if(SCM_I_IS_VECTOR(e))
    {
      if (scm_c_vector_length (e) == 1)
        e = scm_c_vector_ref(e,0);
      else
        e = scm_vector_to_list(e);
    }
  
  if(SCM_CONSP(db_))
    {
      SCM x  = SCM_CAR(db_);
      if(scm_is_eq(x, minus_tag_p))     goto minus_tag;
      if(scm_is_eq(x, predicate_tag_p)) goto predicate_tag;
      if(scm_is_eq(x, not_tag_p))       goto not_tag;
      if(scm_is_eq(x, and_tag_p))       goto and_tag;
      if(scm_is_eq(x, plus_tag_p))      goto plus_tag;
      if(scm_is_eq(x, set_tag_p))       goto set_tag;
    }

 retry_cons:
  if(scm_is_true(gp_pair(e,s)))
    {      
      SCM x  = gp_car(e,s);
      SCM l  = gp_gp_cdr(e,s);
      SCM v  = get_vars(db);
      int i;
      // printf("CONSP\n");fflush(stdout);
      // gp_format3("vars: ~a\n e: ~a\n db: ~a~%",v,e,db_);fflush(stdout);

      if(SCM_I_INUMP (v))
        {          
          ulong nv = my_scm_to_ulong(v);
          //printf("NIMP v %p\n", nv);fflush(stdout);
          data2[0] = *n ? (data[0] & nv) : nv;
          n2 = 1;
        }
      else
        {
          mpz_t *mpv = &(SCM_I_BIG_MPZ(v));          
          int n3 = (*mpv)->_mp_size;
          ulong *data3 = (*mpv)->_mp_d;
          // printf("MPZ v\n");fflush(stdout);
          if(*n)
            {
              n2 = MIN(n3,*n);
              for(i = 0; i < n2; i++)
                data2[i] = data[i] & data3[i];
            }
          else
            {
              n2 = n3;
              for(i = 0; i < n2; i++)
                data2[i] = data3[i];
            }
        }
      SCM dcar = dlink_car(db,0);
      SCM dcdr = dlink_cdr(db,0);
      //printf("sub v\n");fflush(stdout);
      if(scm_is_true(dcar))
	{
	  //printf("CAR\n");
	  get_index_set_0(s,x,dcar, n, data, isPlus, trset);
	}
      
      int nsol = 0;
      for(i = 0; i < *n && nsol < 2; i++)
	{
	  nsol += getNsol(data[i]);
	}

      if(nsol > 1 && scm_is_true(dcdr))
	{
	  //printf("CDR\n");
	  get_index_set_0(s,l,dcdr, n, data, isPlus, trset);
	}
      
      // printf("finish v\n");fflush(stdout);
      {
        int N = *n;
        *n = MAX(*n, n2);
        for(i = 0; i < n2; i++)
          if(i < N)
            data[i] = data[i] | data2[i];
          else
            data[i] = data2[i];
      }
      return;
    }

  if(scm_is_true(gp_attvar(e,s)))
    {
      if(isPlus)
	{ 
	  // printf("VAR\n");fflush(stdout);
	  SCM v = get_all(db);
	  // printf("VAR\n");fflush(stdout);
	  if(SCM_I_INUMP (v))
	    {
	      ulong nv = my_scm_to_ulong(v);
	      data[0] = *n ? data[0] & nv : nv;
	      //printf("nv %p, data[0] = %p\n",nv,data[0]);
	      *n = 1;
	    }
	  else
	    {
	      int i;
	      mpz_t *mpv = &(SCM_I_BIG_MPZ(v));          
	      int n3 = (*mpv)->_mp_size;
	      ulong *data3 = (*mpv)->_mp_d;
	      
	      if(*n)
		{
		  *n = MIN(n3,*n);
		  for(i = 0; i < *n; i++)
		    data[i] = data[i] & data3[i];
		}
	      else
		{
		  *n = n3;
		  for(i = 0; i < *n; i++)
		    data[i] = data3[i];
		}
	    }
	  return;
	}
    }

#define SGET(e) SCM_PACK(SCM_STRUCT_DATA_REF(e,3))
  
  if(GP_ITEMSET_P(e))
    {
      e = SGET(e);
    }
  
  if(GP_ITEMMAP_P(e))
    {
      SCM s0 = SGET(SCM_PACK(SCM_STRUCT_DATA_REF(e,0)));      
      SCM s2 = SGET(SCM_PACK(SCM_STRUCT_DATA_REF(e,2)));
      SCM s3 = SCM_PACK(SCM_STRUCT_DATA_REF(e,3));

      if(scm_is_false(s3))
        {
          e = scm_logxor(s0,s2);
        }
      else
        {         
          e = scm_cons(s2,s3);
          goto retry_cons;
        }
    }

  if(scm_is_true(gp_att_rawvar(e,s)))
    {
      SCM ddata = gp_attdata(e,s);

      e = SCM_EOL;

      for(;SCM_CONSP(ddata);ddata = SCM_CDR(ddata))
        {
          SCM xx = gp_gp_lookup(SCM_CAR(ddata),s);
          e = scm_cons(scm_cons(gp_gp_lookup(SCM_CAR(xx),s),
                                gp_gp_lookup(SCM_CDR(xx),s)),
                       e);
        }

      ddata = e;
      e = SCM_EOL;

      int nn = scm_to_int(scm_length(ddata));
      SCM item = SCM_BOOL_F;

      for(int ii = 0; ii < nn; ii++)
        {
          SCM xx = SCM_BOOL_F;
          SCM lj = ddata;
          for(int jj = 1; jj < nn; jj++, lj = SCM_CDR(lj))
            {
              if(!scm_is_false(item))
                {
                  if(SCM_UNPACK(SCM_CAR(item)) > SCM_UNPACK(SCM_CAAR(lj)))
                    continue;
                }

              if(scm_is_false(xx))
                xx=SCM_CAR(lj);
              else
                {
                  if(SCM_UNPACK(SCM_CAR(xx)) < SCM_UNPACK(SCM_CAAR(lj)))
                    xx = SCM_CAR(lj);
                }
            }

          item = xx;
          e = scm_cons(xx,e);            
        }
      
      e = scm_reverse(e);
      if(SCM_CONSP(e) && !SCM_CONSP(SCM_CDR(e)))
        {
          e = SCM_CAR(e);          
          if(scm_is_eq(SCM_CAR(e),gp_set_tag))
            e = SCM_CDR(e);
          else
            goto retry_cons;                  
        }
      else
        goto retry_cons;            
    }

  {
    
    SCM     v = get_vars(db);
    SCM     a = get_fs_from_atoms(e, db);
    
    //printf("ATOM\n");fflush(stdout);
    if(SCM_I_INUMP (v))
      {
        ulong nv = my_scm_to_ulong(v);
        n2 = 1;
        data2[0] = *n ? (data[0] & nv) : nv;
        //printf("nv = %p, data2[0]= %p\n", nv, data2[0]);
      }
    else
      {
        mpz_t *mpv = &(SCM_I_BIG_MPZ(v));          
        int n3 = (*mpv)->_mp_size;
        ulong *data3 = (*mpv)->_mp_d;

        int i,n2 = *n ? MIN(n3, *n) : n3;
        if(*n)
          {
            for(i = 0; i < n2; i++)
              data2[i] = data[i] & data3[i];
          }
        else
          {
            for(i = 0; i < n2; i++)
              data2[i] = data3[i];
          }
      }
    
    
    if(SCM_I_INUMP (a))
      {
        ulong na = my_scm_to_ulong(a);
        int i;
        data[0] = data2[0] | (*n ? (data[0] & na) : na);
        *n = MAX(n2,1);
        for(i = 1; i < *n; i++)
          data[i] = data2[i];
	//printf("na = %p, data[0]= %p\n", na, data[0]);
      }
    else
      {	
        mpz_t *mpa = &(SCM_I_BIG_MPZ(a));          
        int n4 = (*mpa)->_mp_size;
        ulong *data4 = (*mpa)->_mp_d;

        n4 = *n ?  MIN(n4,*n)  : n4;
        
        if(*n)
          {
            int i;
            *n = MAX(n2, n4);
            for(i = 0; i < n4; i++)
              data[i] = data[i] & data4[i];
            for(i = 0; i < n2; i++)
              if(i < n4)
                data[i] = data[i] | data2[i];
              else
                data[i] = data2[i];
          }
        else
          {
            int i;
            *n = MAX(n2, n4);
            int nk =  MIN(n4,n2);
            for(i = 0; i < nk; i++)
              data[i] = data[2] | data4[i];
            if(n2 < n4)
              for(i = nk; i < n4; i++)
                data[i] = data4[i];
            if(n4 < n2)
              for(i = nk; i < n2; i++)
                data[i] = data[2];
          }
      }
    // printf("ret n=%d,data[0]=%p, data[1]=%p",*n,data[0],data[1]);
    return;
}

 and_tag:
  {
    SCM a = SCM_CADR(db_);
    SCM b = SCM_CADDR(db_);
    SCM c = SCM_CADDDR(db_);


    ulong data2[100];
    int i;
    int n2 = *n;
    for(i = 0; i < *n; i++)
      data2[i] = data[i];
    
    get_index_set_0(s, e, a, &n2, data2, isPlus, trset);
    get_index_set_0(s, e, b, n, data, isPlus, trset);
    get_index_set_0(s, e, c, n, data, isPlus, trset);
    
    if(*n < n2)
      {
	for(i = 0; i < *n ; i++)
	  data[i] = data[i] | data2[i];
	for(;i<n2; i++)
	  data[i] = data2[i];

	*n = n2;
      }
    else
      {
	for(i = 0; i < n2 ; i++)
	  data[i] = data[i] | data2[i];
      }
    return;
  }

 minus_tag:
  {
    SCM a = SCM_CADR(db_);
    SCM b = SCM_CADDR(db_);

    if(scm_is_false(a))
      return get_index_set_0(s, e, b, n, data, 0, trset);

    
    int n2 = *n;
    ulong data2[100];
    int i;
    for(i = 0; i < n2; i++)
      {
	data2[i]=data[i];
      }
    get_index_set_0(s, e, b, &n2, data2, 0, trset);    
    get_index_set_0(s, e, a, n, data, isPlus, trset);

    if(n2<*n)
      {
	for(i = 0; i < n2; i++)
	  data[i] = data[i] | data2[i];
      }
    else
      {
	for(i = 0; i < *n; i++)
	  data[i] = data[i] | data2[i];
	for(;i<n2;i++)
	  data[i] = data2[i];
	*n = n2;
      }
    
    return;
  }

 set_tag:
  {    
    {
      int i;
      n2 = *n;
      for(i = 0; i < *n; i++)
	{
	  data2[i] = data[i];
	}
    }

    if(scm_is_true(gp_att_rawvar(e,s)))
      {
	SCM adata = gp_get_attr(e, gp_type_attribute_tag, s);
	adata = gp_gp_lookup(adata, s);
	//data = (set . args)

	if(scm_is_true(adata))
	  {
	    SCM set  = SCM_CAR(adata);
	    SCM args = SCM_CDR(adata);
	
	    db = dB(db_);
	    if(SCM_CONSP(db_))
	      {
		SCM tree  = SCM_CDR(SCM_CADDR(db_));
		SCM bits  = get_bits_from_set(set, trset);
		SCM v     = get_set(tree, bits);
		if(SCM_I_INUMP (v))
		  {
		    ulong nv = my_scm_to_ulong(v);
		    data2[0] = (n2 == 0 ? nv : nv & data2[0]);
		    n2 = MAX(1, n2);
		  }
		else
		  {
		    int i;
		    mpz_t *mpv = &(SCM_I_BIG_MPZ(v));          
		    int n3 = (*mpv)->_mp_size;
		    ulong *data3 = (*mpv)->_mp_d;
	      
		    if(n2)
		      {
			n2 = MIN(n3,n2);
			for(i = 0; i < n2; i++)
			  data2[i] = data2[i] & data3[i];
		      }
		    else
		      {
			n2 = n3;
			for(i = 0; i < n2; i++)
			  data2[i] = data3[i];
		      }
		  }

		SCM db2 = SCM_CADDDR(db_);
		get_index_set_0(s, args, db2, &n2, data2, isPlus, trset);
	      }
	  }
      }
    else
      {
	SCM v = SCM_CADR(SCM_CDDDR(db_));
	if(SCM_I_INUMP (v))
	  {
	    ulong nv = my_scm_to_ulong(v);
	    data2[0] = (n2 == 0 ? nv : nv & data2[0]);
	    n2 = MAX(1, n2);
	  }
	else
	  {
	    int i;
	    mpz_t *mpv = &(SCM_I_BIG_MPZ(v));          
	    int n3 = (*mpv)->_mp_size;
	    ulong *data3 = (*mpv)->_mp_d;
	      
	    if(*n)
	      {
		n2 = MIN(n3,n2);
		for(i = 0; i < n2; i++)
		  data2[i] = data2[i] & data3[i];
	      }
	    else
	      {
		n2 = n3;
		for(i = 0; i < n2; i++)
		  data2[i] = data3[i];
	      }
	  }
      }
    
    db_ = SCM_CADR(db_);
    get_index_set_0(s, e, db_, n, data, isPlus, trset);

    {
      int i;
      if(n2 < *n)
	{
	  for(i = 0; i < n2; i++)
	    data[i] = data[i] | data2[i];
	}
      else
	{
	  for(i = 0; i < *n; i++)
	    data[i] = data[i] | data2[i];
	  for(; i < n2; i++)
	    data[i] = data2[i];
	  
	  *n = n2;
	}
    }
    
    return;
  }

 plus_tag:
  {
    SCM a = SCM_CADR(db_);
    SCM b = SCM_CADDR(db_);

    if(scm_is_false(a))
      return get_index_set_0(s, e, b, n, data, 1, trset);

    
    int n2 = *n;
    ulong data2[100];
    int i;
    for(i = 0; i < n2; i++)
      {
	data2[i]=data[i];
      }
    get_index_set_0(s, e, b, &n2, data2, 1, trset);    
    get_index_set_0(s, e, a, n, data, isPlus, trset);

    if(n2<*n)
      {
	for(i = 0; i < n2; i++)
	  data[i] = data[i] | data2[i];
      }
    else
      {
	for(i = 0; i < *n; i++)
	  data[i] = data[i] | data2[i];
	for(;i<n2;i++)
	  data[i] = data2[i];
	*n = n2;
      }
    
    return;
  }

 not_tag:
  {
    SCM a  = SCM_CADR(db_);
    SCM b  = SCM_CADDR(db_);
    SCM v  = SCM_CADDDR(db_);

    ulong data2[100];
    ulong *data3;
    ulong i,di;
    int n3,n2=0;
    if(SCM_I_INUMP (v))
      {
	n3 = 1;
	di = SCM_UNPACK(v) >> 2;
	data3 = &di;
      }
    else
      {
	mpz_t *mpv = &(SCM_I_BIG_MPZ(v));          
	n3 = (*mpv)->_mp_size;
	data3 = (*mpv)->_mp_d;
      }
      
    get_index_set_0(s, e, b, &n2, data2, isPlus, trset);
    get_index_set_0(s, e, a, n, data, isPlus, trset);

    if(n2 < n3)
      {
	for(i = 0; i < n2; i++)
	  data3[i] = data3[i] & ~data2[i];
      }
    else
      {
	for(i=0; i < n3; i++)
	  data3[i] = data3[i] & ~data2[i];       
      }
    if(n3 < *n)
      {
	for(i = 0; i < n3; i++)
	  data[i] = data[i] | data3[i];
      }
    else
      {
	for(i = 0; i < *n; i++)
	  data[i] = data[i] | data3[i];
	for(;i < n3; i++)
	  data[i] = data3[i];
	*n = n3;
      }
  }
  return;

 predicate_tag:
  {
    SCM a = SCM_CADR(db_);
    SCM l = SCM_CDDR(db_);
    get_index_set_0(s, e, a, &n2, data, isPlus, trset);
    
    while(SCM_CONSP(l))
      {
	scm_t_bits tag = SCM_UNPACK(SCM_CAAR(l));
	SCM        p   = SCM_CDAR(l);
	
	if(predicates[tag](e,s))
	  {
            
	    ulong *data4;
	    int   n4;

	    if(SCM_I_INUMP (p))
	      {
		ulong data4_ =  my_scm_to_ulong(p);
		data4 = & data4_;
		n4    = 1;
	      }
	    else
	      {
		mpz_t *mpa   = &(SCM_I_BIG_MPZ(p));          
		n4           = (*mpa)->_mp_size;
		data4        = (*mpa)->_mp_d;
	      }

	    if(n4 < *n)
	      {
		int i;
		for(i = 0; i < n4; i++)
		  data[i] = data[i] | data4[i];
	      }
	    else
	      {
		int i;
		for(i = 0; i < *n; i++)
		  data[i] = data[i] | data4[i];
		for(;i<n4;i++)
		  data[i] = data4[i];
	      }
	  }

	l = SCM_CDR(l);
      }
  }
}

//#define DB(X) 
SCM get_index_set(SCM s, SCM e, SCM db, int isPlus, SCM trset)
{
  ulong data[200];
  int   i,j,b,n = 0;
  SCM ret;

  get_index_set_0(s, e, db, &n, data, isPlus, trset);

  //printf("Got %p %p\n",data[0],data[1]);
  
  ret = SCM_EOL;
  
  for(b = 0, i = 0; i < n; i++, b+=64)
    {
      ulong x = data[i];
      //printf("data(%d) %p\n",i, data[i]);
      for(j = 0; j<32; j++)
        {       
          if(x == 0) break;
          int l;   
          //printf("x = %p\n",x);
          bsf(l,x);
          //printf("found %d\n",l);
          if(l < 0) break;
          ret = scm_cons(scm_from_int(b + l), ret);
          x = x & ~(1UL<<l);          
        }      
    }
  
    
  return ret;
  

  //return scm_from_int(10);
}

SCM_DEFINE(scm_get_index_set, "get-index-set", 4, 0, 0, 
           (SCM s, SCM e, SCM db, SCM trset), "")
#define FUNC_NAME s_scm_get_index_set
{
  if(!scm_is_false(trset))
    {
      SCM parent     = scm_c_vector_ref(trset , 7);
      SCM mask       = scm_c_vector_ref(trset , 0);
      SCM set_to_i   = scm_c_vector_ref(parent, 2);
      SCM i_to_j     = scm_c_vector_ref(trset,  2);
      SCM j_to_inh   = scm_c_vector_ref(trset , 4);
      SCM i_to_inh   = scm_c_vector_ref(parent, 4);
      SCM v = scm_c_make_vector(5, SCM_BOOL_F);
      scm_c_vector_set_x(v, 0, mask);
      scm_c_vector_set_x(v, 1, set_to_i);
      scm_c_vector_set_x(v, 2, i_to_j);
      scm_c_vector_set_x(v, 3, j_to_inh);
      scm_c_vector_set_x(v, 4, i_to_inh);
      
      trset = v;
    }

  return get_index_set(s, e, db, 1, trset);
}
#undef FUNC_NAME

SCM_DEFINE(scm_get_index_test, "get-index-test", 4, 0, 0, 
           (SCM s, SCM e, SCM db, SCM n_), "")
#define FUNC_NAME s_scm_get_index_test
{
  int n = scm_to_int(n_);
  for(;n>0;n--)
    get_index_set(s, e, db, 1, SCM_EOL);

  return get_index_set(s, e, db, 1, SCM_EOL);
}
#undef FUNC_NAME

#define MKSIMPLE(gp_,gp_gp)	      \
  int gp_(SCM x, SCM s)		      \
  {				      \
    return scm_is_true(gp_gp(x));     \
  }				  
#define MKSIMPLE2(gp_,gp_gp)	      \
  int gp_(SCM x, SCM s)		      \
  {				      \
    return scm_is_true(gp_gp(x,s));    \
  }				  

MKSIMPLE(gp_     ,gp_gp)
MKSIMPLE(integer__,scm_integer_p)
MKSIMPLE(string_ ,scm_string_p)
MKSIMPLE(id_     ,scm_procedure_p)
MKSIMPLE(number_ ,scm_number_p)
MKSIMPLE(float_  ,scm_inexact_p)

MKSIMPLE2(var_    , gp_attvar);
MKSIMPLE2(attvar_ , gp_att_rawvar);
MKSIMPLE2(varvar_ , gp_attvar);
int integer_(SCM x, SCM s)
{
  return scm_is_true(scm_integer_p(x));
}


#define scm_list_6(a,b,c,d,e,f) scm_cons(a,scm_list_5(b,c,d,e,f))

SCM_DEFINE(scm_get_index_tags, "get-index-tags", 0, 0, 0, (), "") 
#define FUNC_NAME s_scm_get_tags
{
  return scm_list_6(and_tag_p, not_tag_p, predicate_tag_p,
		    minus_tag_p, plus_tag_p, set_tag_p);
}
#undef FUNC_NAME

SCM_DEFINE(gp_set_type_attribute, "set-type-attribute!", 1, 0, 0, (SCM x), "")
#define FUNC_NAME s_gp_set_type_attribute
  { 
    gp_type_attribute_tag = x;
    return SCM_UNSPECIFIED;
  }
#undef FUNC_NAME

void init_indexer()
{
  and_tag_p       = scm_cons(SCM_BOOL_F, SCM_BOOL_F);
  or_tag_p        = scm_cons(SCM_BOOL_F, SCM_BOOL_F);
  not_tag_p       = scm_cons(SCM_BOOL_F, SCM_BOOL_F);
  predicate_tag_p = scm_cons(SCM_BOOL_F, SCM_BOOL_F);
  minus_tag_p     = scm_cons(SCM_BOOL_F, SCM_BOOL_F);
  plus_tag_p      = scm_cons(SCM_BOOL_F, SCM_BOOL_F);
  set_tag_p       = scm_cons(SCM_BOOL_F, SCM_BOOL_F);

  predicates[0] = &gp_;
  predicates[1] = &integer__;
  predicates[2] = &string_;
  predicates[3] = &id_;
  predicates[4] = &number_;
  predicates[5] = &float_;
  predicates[6] = &var_;
  predicates[7] = &attvar_;
  predicates[8] = &varvar_;
}
