/*
 	Copyright (C) 2009, 2010 Free Software Foundation, Inc.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include <libguile.h>
#include <stdio.h>
#include "../../../config.h"
#include "unify.h"

#include <gc/gc_mark.h>
#include <gc/gc.h>

#include "libguile/smob.h"
#define VECTOR_HEADER_SIZE 2

SCM gp_set_tag = SCM_BOOL_F;

SCM_DEFINE(gp_set_set_tag,"set-set-tag!",1,0,0,(SCM item),
	   "set the cp-attributes fluid")
#define FUNC_NAME s_gp_set_set_tag
{
  gp_set_tag = item;
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME


SCM gp_itemset_id = SCM_BOOL_F;
SCM gp_itemmap_id = SCM_BOOL_F;
#define GP_ITEMSET_P(x)                                         \
  (SCM_STRUCTP(x) && (SCM_STRUCT_VTABLE(x) == gp_itemset_id))
#define GP_ITEMMAP_P(x)                                         \
  (SCM_STRUCTP(x) && (SCM_STRUCT_VTABLE(x) == gp_itemmap_id))

SCM_DEFINE(gp_set_setmap_struct,"set-setmap-struct!",2,0,0,(SCM set, SCM map),
	   "set the setmap struct variable")
#define FUNC_NAME s_gp_set_setmap_struct
{
  gp_itemset_id = set;
  gp_itemmap_id = map;

  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME


static inline struct gp_stack * get_gp();

SCM* gp_lookup_l_1 (SCM *x, SCM l); 
SCM* gp_lookup_ll_1(SCM *x, SCM *l); 

SCM tester = SCM_BOOL_F;

SCM static inline get_cs(SCM v);

SCM gp_engine_path = SCM_EOL;
SCM gp_store_path  = SCM_EOL;
SCM gp_paths       = SCM_EOL;

SCM gp_current_stack = SCM_BOOL_F;
SCM current_stack = SCM_BOOL_F;
int do_gp_mark = 1;

#define gp_store    1
#define gp_redo     2
#define gp_redo_tag 10
#define gp_save_tag 14

SCM_DEFINE(gp_get_current_stack, "gp-current-stack-ref", 0, 0, 0, (), 
	   "takes cdr a prolog pair or scheme pair")
#define FUNC_NAME s_gp_get_current_stack
{
  return gp_current_stack;
}
#undef FUNC_NAME


#define check_cs(cs,gp,str)                                             \
  do                                                                    \
  {                                                                     \
    DB(if(cs < gp->gp_cons_stack || cs > gp->gp_cons_stack + 1000)      \
         {                                                              \
           scm_misc_error(str,"cs got wrong value",SCM_EOL);            \
         })                                                             \
  } while(0)

scm_t_bits gp_type;

SCM closure_tag;

#define gp_format0(str)				\
  DB(scm_simple_format(SCM_BOOL_T,		\
		  scm_from_locale_string(str),	\
		       SCM_EOL))

#define gp_format1(str,x)			\
  DB(scm_simple_format(SCM_BOOL_T,		\
		  scm_from_locale_string(str),	\
		       scm_list_1(x)))

#define gp_format2(str,x,y)			\
  DB(scm_simple_format(SCM_BOOL_T,		\
		  scm_from_locale_string(str),	\
		       scm_list_2(x,y)))

#define gp_format3(str,x,y,z)			\
  DB(scm_simple_format(SCM_BOOL_T,		\
		  scm_from_locale_string(str),	\
		       scm_list_3(x,y,z)))

#define gp_format4(str,x,y,z,w)				\
  DB(scm_simple_format(SCM_BOOL_T,			\
		       scm_from_locale_string(str),	\
		       scm_list_4(x,y,z,w)))

#define format0(str)				\
  (scm_simple_format(SCM_BOOL_T,		\
		  scm_from_locale_string(str),	\
		       SCM_EOL))

#define format1(str,x)				\
  (scm_simple_format(SCM_BOOL_T,		\
		  scm_from_locale_string(str),	\
		       scm_list_1(x)))

#define format2(str,x,y)			\
  (scm_simple_format(SCM_BOOL_T,		\
		  scm_from_locale_string(str),	\
		       scm_list_2(x,y)))

#define format3(str,x,y,z)			\
  (scm_simple_format(SCM_BOOL_T,		\
		  scm_from_locale_string(str),	\
		       scm_list_3(x,y,z)))

#define format4(str,x,y,z,w)			\
  (scm_simple_format(SCM_BOOL_T,		\
		  scm_from_locale_string(str),	\
		     scm_list_4(x,y,z,w)))


SCM gp_name_sym = SCM_BOOL_F;
SCM gp_procedure_name(SCM f)
{
  SCM ret = scm_procedure_property(f, gp_name_sym);
  if(scm_is_false(ret))
    {
      ret = scm_procedure_name(f);
      format1("non simple name > ~a~%",ret);
      return ret;
    }
  return ret;
}


#define DB(X)
#define DS(X)
#define gp_debug0(s)        DB(printf(s)      ; fflush(stdout))
#define gp_debug1(s,a)      DB(printf(s,a)    ; fflush(stdout))
#define gp_debug2(s,a,b)    DB(printf(s,a,b)  ; fflush(stdout))
#define gp_debug3(s,a,b,c)  DB(printf(s,a,b,c); fflush(stdout))
#define gp_debug4(s,a,b,c,d)  DB(printf(s,a,b,c,d); fflush(stdout))
#define gp_debug5(s,a,b,c,d,e)  DB(printf(s,a,b,c,d,e); fflush(stdout))
#define gp_debus0(s)        DS(printf(s)      ; fflush(stdout))
#define gp_debus1(s,a)      DS(printf(s,a)    ; fflush(stdout))
#define gp_debus2(s,a,b)    DS(printf(s,a,b)  ; fflush(stdout))
#define gp_debus3(s,a,b,c)  DS(printf(s,a,b,c); fflush(stdout))

// SPECIAL UNIVY VAR GC TIDBITS



/*
   000 - A ref to a new object
   010 - A cons ref
   110 - A cons rest
   100 - A rest

 1x11xx - unbounded
 1x01xx - eq
 0x11xx - val

  unbounded is a null reference aka X & 0b000  == 0
*/

/*
  We will work with three stacks.
  stack  - the variable stack
  cstack - storage stack to undo setted values
  wstack - frame stack in order to do batched backtracking
*/


scm_t_bits gp_smob_t;

// Assumes stack_a and stack_b are allocated in order they appear
#define GP(scm) (SCM_NIMP((scm)) && SCM_SMOB_PREDICATE (gp_type, (scm)))
#define GP_STAR(scmp) (GP(GP_UNREF(scmp)))
/*
  Tagging
  xx00  Pointer
  xx01  cons
  xx10  val
  xx11  unbound
   1    attr flag
  1     eq   flag
  
 */
//TODO: make use of smob flags macros for better stability
#define B(c) ((scm_t_bits) c)
#define GPM_BASE    B(0x30000)
#define GP_CLEAN    B(0x00000)
#define GPI_PTR     B(0x00000)
#define GPI_CONS    B(0x10000)
#define GPI_VAL     B(0x20000)
#define GPI_UNBD    B(0x30000)

#define GPI_ATTR    B(0x100000)
#define GPI_GUARD   B(0x200000)
#define GPI_WGUARD  B(0x40000)
#define GPI_EQ      B(0x80000)
#define GPQ_EQ      B(0xa0000)

#define GPI_SCM_M   B(0x400000)
#define GPI_SCM_C   B(0x800000)
#define GPI_SCM_Q   B(0x1000000)
#define GPI_FRAME   B(0x2000000)
#define GPM_PTR     B(0x0ffff)
#define GPM_CONS    B(0x1ffff)
#define GPM_VAL     B(0x2ffff)
#define GPM_UNBD    B(0x3ffff)
#define GPM_EQ      B(0xaffff)

#define GPI         B(0xf0000)
#define GPM         B((unsigned long) 0xfffff)

#define GPID              B(0xffb000000)
#define GPHA        B(0xffffff000000000)

#define GPRENEW(x)       ((x) & ~GPHA)
#define GP_MARK(x)       (x & GPI_GL_M)
#define GP_SET_MARK(x)   (x | GPI_GL_M)
#define GP_CLEAR_MARK(x) (x & (~GPI_GL_M))

//#define GP_HASH(x)  ((SCM_UNPACK(x) & GPHA) >> H_BITS)
#define GP_ID(x)          ((SCM_UNPACK(x) & GPID) >> N_BITS)
#define GP_SETID(x,id)    (((unsigned long) (id)) << N_BITS | (x))
#define GP_COUNT(X)       ((scm_t_bits) ((SCM_UNPACK(X) & GPHA) >> H_BITS))
#define GP_SETCOUNT(X,i)  (((SCM_UNPACK(X) & (~GPHA))   \
			    | ((i) << H_BITS)))
#define GP_FLAGS(x) ((x)[0])
#define GP_SCM(x)   ((x)[1])

#define GP_IS_EQ(x ) ((SCM_IMP(x) && !SCM_I_INUMP(x)) || scm_is_true(scm_symbol_p(x)))

#define GP_UNBOUND(ref) ( (SCM_UNPACK(GP_FLAGS(ref)) & GPM_BASE) == GPI_UNBD)
#define GP_POINTER(ref) ( (SCM_UNPACK(GP_FLAGS(ref)) & GPM_BASE) == GPI_PTR)
#define GP_CONS(ref)    ( (SCM_UNPACK(GP_FLAGS(ref)) & GPM_BASE) == GPI_CONS)
#define GP_VAL(ref)     ( (SCM_UNPACK(GP_FLAGS(ref)) & GPM_BASE) == GPI_VAL)

#define GP_EQ(ref)      ( SCM_UNPACK(GP_FLAGS(ref)) & GPI_EQ)
#define GP_ATTR(ref)    ( SCM_UNPACK(GP_FLAGS(ref)) & GPI_ATTR)
#define GP_WGUARD(x)    (SCM_UNPACK(GP_FLAGS(x)) & GPI_WGUARD)
#define GP_GUARD(x)     (SCM_UNPACK(GP_FLAGS(x)) & GPI_GUARD)

#define GP_MK_FRAME_VAL(fr)  ((fr) | GPI_VAL)
#define GP_MK_FRAME_PTR(fr)  ((fr) | GPI_PTR)
#define GP_MK_FRAME_CONS(fr) ((fr) | GPI_CONS)
#define GP_MK_FRAME_UNBD(fr) ((fr) | GPI_UNBD)

#define GP_MK_FRAME_EQ(fr)   ((fr) | GPQ_EQ)

#define SCM_TO_FI(x) SCM_I_INUM(x)
#define FI_TO_SCM(x) SCM_I_MAKINUM(x)

#define PTR2NUM(x) SCM_PACK((((scm_t_bits) x) | 2))
#define NUM2PTR(x) ((SCM *) (SCM_UNPACK(x) & ~2))

#define GP_CAR(id)  GP_GETREF((*((id) + 1)))
#define GP_CDR(id)  GP_GETREF((*((id) + 2)))

#define GP_GETREF(x) ((SCM *) (x))

#define GP_UNREF(x)  ((SCM)   (x))

#define N_BITS 27
#define H_BITS 37

#define GP_ATTR_IT(x)        ((x) = ((x) | GPI_ATTR))

#define GP_GC_MARK(x)         ((x) = ((x) | GPI_SCM_M))
#define GP_GC_ISMARKED(x)     ((x) & GPI_SCM_M)
#define GP_GC_CLEAR(x)        ((x) = (x) & ~(GPI_SCM_M))
#define GP_GC_CAND(x)         ((x) = ((x) | GPI_SCM_C))
#define GP_GC_ISCAND(x)       ((x) & GPI_SCM_C)
#define GP_GC_CLEARCAND(x)    ((x) = (x) & ~(GPI_SCM_C))
#define GP_GC_QAND(x)         ((x) = ((x) | GPI_SCM_Q))
#define GP_GC_ISQAND(x)       ((x) & GPI_SCM_Q)
#define GP_GC_CLEARQAND(x)    ((x) = (x) & ~(GPI_SCM_Q))

#define SET_FRAME(x) ((x) = (x) | GPI_FRAME)
#define FRAMEP(x)    ((x) & GPI_FRAME)
#define GP_CONSP(x) (x && GP(x) && GP_CONS(GP_GETREF(x)))
static inline int GP_FRAME_VAR(SCM x)
{
  if(GP(x))
    return FRAMEP(SCM_UNPACK(GP_GETREF(x)[0]));
  else
    return 0;
}

static inline int gp_is_free(SCM *pt)
{
  scm_t_bits head = SCM_UNPACK(GP_GETREF(*pt)[0]);
  return (!GP_GC_ISMARKED(head) && GP_GC_ISCAND(head));
}

static inline SCM GP_IT(SCM* id)
{
  return GP_UNREF(id);
}


static inline SCM* UN_GP(SCM scm)
{
  return GP_GETREF(scm);
}

SCM gp_unbound_sym;
SCM gp_unbound_str;
SCM gp_unwind_fluid;
SCM gp_cons_sym;
SCM gp_cons_str;

#include "state.c"

#define UNBD_ATTR(id) GP_UNBOUND(GP_GETREF(SCM_CAR(id[1])))
#define RAW_ATTR(id) GP_GETREF(SCM_CAR(id[1]))

static inline SCM gp_make_vector(int n, struct gp_stack *gp)
{
  SCM *vec = gp_alloc_data(n + VECTOR_HEADER_SIZE,gp);
  ((scm_t_bits *) vec)[0] = (n << 8) | scm_tc7_vector;
  ((scm_t_bits *) vec)[1] = 0;
  return PTR2SCM(vec);
}

static inline SCM gp_make_closure(int n, SCM **closure, SCM s)
{
  struct gp_stack *gp = get_gp();
  SCM vec  = gp_make_vector(n,gp);
  *closure = SCM_I_VECTOR_WELTS(vec);
  gp_alloc_cons(gp,1);
  return gpa_cons(closure_tag, vec, gp);
}

static inline SCM gp_make_closure_heap(int n, SCM **closure)
{
  SCM vec  = scm_c_make_vector(n,SCM_BOOL_F);
  *closure = SCM_I_VECTOR_WELTS(vec);
  return scm_cons(closure_tag, vec);
}

#include "vlist/vlist-wrap.c"
#include "logical.c"

#define GET_L(l) (SCM_CONSP(l) ? SCM_CAR(l) : SCM_EOL)

#define UNPACK_S(ll,l,gp,s,err)					\
  {								\
    SCM a;							\
    a = scm_fluid_ref(gp_current_stack);			\
    if(!GP_STACKP(a))						\
      {                                                         \
        scm_misc_error("unpack_s2",err,scm_list_2(s,a));        \
      }                                                         \
    gp = GET_GP(a);						\
    gp_debug0(err);						\
    if(GP_CONSP(s))                                             \
      {                                                         \
	ll   = GP_GETREF(s)[2];                                 \
        l    = GET_L(ll);                                       \
      }                                                         \
    else if(SCM_CONSP(s))                                       \
      {                                                         \
	ll   = SCM_CDR(s);                                      \
        l    = GET_L(ll);                                       \
      }                                                         \
    else                                                        \
      {                                                         \
	l   = SCM_EOL;                                          \
        ll  = scm_cons(SCM_EOL,SCM_EOL);                        \
      }                                                         \
  }

#define UNPACK_S0(l,s,err)					\
  {								\
    SCM a;							\
    a = scm_fluid_ref(gp_current_stack);			\
    if(!GP_STACKP(a))						\
      scm_misc_error("unpack_s0",err,SCM_EOL);			\
    if(GP_CONSP(s))						\
      {								\
	l   = GET_L(GP_GETREF(s)[2]);                           \
      }								\
    else if(SCM_CONSP(s))					\
      {								\
        l   = GET_L(SCM_CDR(s));                                \
      }								\
    else							\
      {								\
	l   = SCM_EOL;						\
      }								\
  }

#define UNPACK_ALL(fr,ll,l,ggp,gp,s,err)                 \
  {							 \
    gp_format1("s: ~a~%",s);                             \
    ggp = scm_fluid_ref(gp_current_stack);		 \
    if(!GP_STACKP(ggp))					 \
      scm_misc_error("unpack_a2",err,SCM_EOL);		 \
    gp  = GET_GP(ggp);					 \
    if(GP_CONSP(s))					 \
      {							 \
	ll  = GP_GETREF(s)[2];                           \
        l   = GET_L(ll);                                 \
	fr  = GP_GETREF(s)[1];				 \
      }							 \
    else if (SCM_CONSP(s))				 \
      {							 \
	ll  = SCM_CDR(s);                                \
        l   = GET_L(ll);                                 \
	fr  = SCM_CAR(s);				 \
      }							 \
    else						 \
      {							 \
	fr  = PTR2NUM(gp->gp_fr);			 \
        ll  = scm_cons(SCM_EOL,SCM_EOL);                 \
	l   = SCM_EOL;					 \
      }							 \
  }

#define UNPACK_ALL0(l,ggp,gp,s,err)			 \
  {							 \
    ggp = scm_fluid_ref(gp_current_stack);		 \
    if(!GP_STACKP(ggp))					 \
      scm_misc_error("unpack_a2",err,SCM_EOL);		 \
    gp  = GET_GP(ggp);					 \
    if(GP_CONSP(s))					 \
      {							 \
	l   = GET_L(GP_GETREF(s)[2]);                    \
      }							 \
    else if(SCM_CONSP(s))				 \
      {							 \
	l   = GET_L(SCM_CDR(s));                         \
      }							 \
    else						 \
      {							 \
	l   = SCM_EOL;					 \
      }							 \
  }

#define UNPACK_ALL00(ggp,gp,s,err)			 \
  {							 \
    ggp = scm_fluid_ref(gp_current_stack);		 \
    if(!GP_STACKP(ggp))					 \
      scm_misc_error("unpack_a2",err,SCM_EOL);		 \
    gp  = GET_GP(ggp);					 \
  }

#define PACK_ALL(fr,ll,l,lnew,gp,s)			\
        {                                               \
    if(!scm_is_eq(l,lnew))				\
      s = scm_cons(fr,scm_cons(lnew,SCM_CDR(ll)));      \
  }

static inline SCM gp_make_s(SCM frci, SCM *l)
{
  SCM ll;
  if(vlist_p(l[0]))
    {
      ll = make_vlist(S(l[0],0),SCM_UNPACK(l[2]));
    } else
    {
      ll = l[0];
    }
  return scm_cons(frci,ll);  
}

static inline void gp_store_var_2(SCM *id, int simple, struct gp_stack *gp)
{
  GP_TEST_CSTACK;

  gp_gc_inc(gp);

  if(!GP(GP_UNREF(id)))
    scm_misc_error("gp_store_var_2"," got non gp variable to set ~a",
                   scm_list_1(GP_UNREF(id)));

  if(GP_UNBOUND(id))
    {
      *(gp->gp_ci) = GP_UNREF(id);
    }
  else
    { 
#ifdef HAS_GP_GC		       
      SCM *pt = (SCM *) scm_gc_malloc_pointerless(2*sizeof(SCM), "mute cons");
#else
      SCM *pt = (SCM *) scm_gc_malloc(2*sizeof(SCM), "cons");
#endif
      
      pt[0] = GP_UNREF(id);
      pt[1] = scm_cons(SCM_I_MAKINUM(id[0]),id[1]);
      *(gp->gp_ci) = GP_UNREF(pt);
      /*
      *(gp->gp_ci) = scm_cons(GP_UNREF(id),
			      scm_cons(SCM_I_MAKINUM(id[0]),id[1]));
      */
    }
  
  gp_debug1("stored> %x\n",SCM_UNPACK(gp->gp_ci[0]));
  gp->gp_ci += 1;
}

static inline void mask_on(int stack_nr, SCM *id, SCM flags)
{
  scm_t_bits nr = ((((scm_t_bits) stack_nr) & 0xffff) << N_BITS);
  *id =  SCM_PACK((SCM_UNPACK(*id) & GP_CLEAN)| SCM_UNPACK(flags) | nr);
  gp_debug2("tag> %x %x\n",SCM_UNPACK(*id),nr);
}

static inline SCM handle(SCM *id, SCM flags, SCM v, SCM l, struct gp_stack *gp, int k, int bang)
{ 

  if(!GP(GP_UNREF(id)))
    scm_misc_error("unify.c: handle"," got non gp variable to set ~a",
                   scm_list_1(GP_UNREF(id)));

  if(!bang && gp->_logical_) return logical_add2(GP_UNREF(id),v,l);

  if(!bang && GP_ID(*id) < gp->id && gp->_thread_safe_)
    {
      gp_debug0("logical add\n");
      return logical_add2(GP_UNREF(id),v,l);
    }
 
  gp_debug1("set var... bang!(%d)",bang);
  
  if(!bang) gp_store_var_2(id,k,gp);

  mask_on(gp->id,id,flags);
  *(id + 1) = v;

  return l;
}

static inline SCM handle_l(SCM *id, SCM flags, SCM v, SCM *l, struct gp_stack *gp, int k, int bang)
{ 

  if(!GP(GP_UNREF(id)))
    scm_misc_error("unify.c: handle_l"," got non gp variable to set ~a",
                   scm_list_1(GP_UNREF(id)));

  if(gp->_logical_) return logical_add2_l(GP_UNREF(id),v,l);

  
  if(GP_ID(*id) < gp->id && gp->_thread_safe_)
    {
      gp_debug0("logical add\n");
      return logical_add2_l(GP_UNREF(id),v,l);
    }

  gp_debug1("set var... bang!(%d)",bang);
  
  if(!bang) gp_store_var_2(id,k,gp);

  mask_on(gp->id,id,flags);
  *(id + 1) = v;

  return SCM_BOOL_T;
}

static inline void handle_force(SCM *id, SCM flags, SCM v)
{ 

  if(!GP(GP_UNREF(id)))
    scm_misc_error("unify.c: handle force"," got non gp variable to set ~a",
                   scm_list_1(GP_UNREF(id)));
  int i = SCM_UNPACK(id[0]) >> N_BITS;
  mask_on(i, id, flags);
  *(id + 1) = v;
}

static inline SCM * set_fr(SCM *fr, struct gp_stack *gp)
{
  SCM *f = get_gp_var(gp);

  SCM flags =  SCM_PACK(GP_MK_FRAME_EQ(gp_type));

  handle_force(f, flags, scm_from_ulong(gp->gp_fr - gp->gp_frstack));
  GP_SET_SELF(fr,GP_UNREF(f));
  return f;
}

// We will add f to ci stack, hence it must be one larger
static inline SCM * set_ci(SCM *fr, struct gp_stack *gp)
{
  SCM *f = get_gp_var(gp);

  scm_t_bits tflags = GP_MK_FRAME_EQ(gp_type);

  SET_FRAME(tflags);
  
  SCM flags = SCM_PACK(tflags);

  handle_force(f, flags, scm_from_ulong(gp->gp_ci - gp->gp_cstack));

  GP_SET_VAL(fr,GP_UNREF(f));
  return f;
}


static inline SCM * set_fr_scm(SCM *fr, struct gp_stack *gp)
{
  SCM *f = GP_GETREF(gp_make_variable());

  SCM flags =  SCM_PACK(GP_MK_FRAME_EQ(gp_type));

  handle_force(f, flags, scm_from_ulong(gp->gp_fr - gp->gp_cstack));
  GP_SET_SELF(fr,GP_UNREF(f));
  return f;
}

static inline SCM gp_set_val(SCM *id, SCM v, SCM l, struct gp_stack *gp)
{
  SCM flags;
  if(GP_IS_EQ(v))
    flags = SCM_PACK(GP_MK_FRAME_EQ(gp_type));
  else
    flags = SCM_PACK(GP_MK_FRAME_VAL(gp_type));

  return handle(id, flags, v, l, gp, 0, 0);
}
static inline SCM gp_set_ref(SCM *id, SCM ref, SCM l, struct gp_stack *gp);
static inline SCM gp_set_attr_val(SCM *id, SCM v, SCM l, struct gp_stack *gp, scm_t_bits);
static inline SCM gp_set_attr_ref(SCM *id, SCM ref, SCM l, struct gp_stack *gp, scm_t_bits bits)
{
  SCM flags;

  if(!GP(ref) && !SCM_VARIABLEP(ref))  
    return gp_set_attr_val(id,ref,l,gp, bits);
  scm_misc_error("gp_set_attr_ref","this fkn should not be used setting attributed content", SCM_EOL);
  flags = SCM_PACK(GP_MK_FRAME_PTR(gp_type));
 
  scm_t_bits f = SCM_UNPACK(flags);
  GP_ATTR_IT(f);
  flags = SCM_PACK((f|bits));

  if(gp->_logical_ || (GP_ID(*id) != gp->id && gp->_thread_safe_))
    {
      SCM *u = GP_GETREF(gp_make_variable());

      mask_on(gp->id, u, flags);
      u[1] = ref;
      return gp_set_ref(id, GP_UNREF(u), l, gp);
    }
  else
    return handle(id, flags, ref, l, gp, 0, 0);
}

static inline SCM gp_set_attr_ref_force(SCM *id, SCM ref, SCM l, struct gp_stack *gp, scm_t_bits bits)
{
  SCM flags;

  if(!GP(ref)  && !SCM_VARIABLEP(ref))  
    return gp_set_attr_val(id,ref,l,gp, bits);
  scm_misc_error("gp_set_attr_ref","this fkn should not be used setting attributed content", SCM_EOL);
  flags = SCM_PACK(GP_MK_FRAME_PTR(gp_type));
 
  scm_t_bits f = SCM_UNPACK(flags);
  GP_ATTR_IT(f);
  flags = SCM_PACK((f|bits));
 
  handle_force(id, flags, ref);
}
static inline SCM gp_set_attr_val(SCM *id, SCM v, SCM l, struct gp_stack *gp, scm_t_bits bits)
{
  SCM flags;
  
  if(GP_IS_EQ(v))
    flags =  SCM_PACK(GP_MK_FRAME_EQ(gp_type));
  else
    flags =  SCM_PACK(GP_MK_FRAME_VAL(gp_type));

  {
    scm_t_bits f = SCM_UNPACK(flags);
    GP_ATTR_IT(f);
    flags = SCM_PACK(f | bits);
  }

  if(gp->_logical_ || (GP_ID(*id) != gp->id && gp->_thread_safe_))
    {
      SCM *u = GP_GETREF(gp_make_variable());

      mask_on(gp->id, u, flags);
      u[1] = v;
      return gp_set_ref(id, GP_UNREF(u), l, gp);
    }
  else
    return handle(id, flags, v, l, gp, 0, 0);
}

static inline SCM gp_set_val_l(SCM *id, SCM v, SCM *l, struct gp_stack *gp)
{
  SCM flags;
  
  if(GP_IS_EQ(v))
    flags =  SCM_PACK(GP_MK_FRAME_EQ(gp_type));
  else
    flags =  SCM_PACK(GP_MK_FRAME_VAL(gp_type));

  return handle_l(id, flags, v, l, gp, 0, 0);
}

static inline SCM gp_set_val_bang(SCM *id, SCM v, SCM l, struct gp_stack *gp)
{
  SCM flags; 

  if(GP_IS_EQ(v))
    flags =  SCM_PACK(GP_MK_FRAME_EQ(gp_type));
  else
    flags =  SCM_PACK(GP_MK_FRAME_VAL(gp_type));
  
  return handle(id, flags, v, l, gp, 0, 1);
}

static inline void gp_set_val_force(SCM *id, SCM v)
{
  SCM flags; 

  if(GP_IS_EQ(v))
    flags =  SCM_PACK(GP_MK_FRAME_EQ(gp_type));
  else
    flags =  SCM_PACK(GP_MK_FRAME_VAL(gp_type));
  
  handle_force(id, flags, v);
}

static inline SCM gp_set_ref(SCM *id, SCM ref, SCM l, struct gp_stack *gp)
{
  SCM flags;

  if(!GP(ref) && !SCM_VARIABLEP(ref))  return gp_set_val(id,ref,l,gp);
  
  flags = SCM_PACK(GP_MK_FRAME_PTR(gp_type));
  
  return handle(id, flags, ref, l, gp, 0, 0);
}

static inline SCM gp_set_ref_l(SCM *id, SCM ref, SCM *l, struct gp_stack *gp)
{
  SCM flags;

  if(!GP(ref)  && !SCM_VARIABLEP(ref))  return gp_set_val_l(id,ref,l,gp);
  
  flags = SCM_PACK(GP_MK_FRAME_PTR(gp_type));
  
  return handle_l(id, flags, ref, l, gp, 0, 0);
}

static inline SCM gp_set_ref_bang(SCM *id, SCM ref, SCM l, struct gp_stack *gp)
{
  SCM flags;
  if(!GP(ref)  && !SCM_VARIABLEP(ref))  return gp_set_val_bang(id,ref,l,gp);

  flags = SCM_PACK(GP_MK_FRAME_PTR(gp_type));

  return handle(id, flags, ref, l, gp, 0, 1);
}

static inline void gp_set_ref_force(SCM *id, SCM ref)
{
  SCM flags;
  if(!GP(ref)  && !SCM_VARIABLEP(ref))  return gp_set_val_force(id,ref);

  flags = SCM_PACK(GP_MK_FRAME_PTR(gp_type));

  handle_force(id, flags, ref);
}

static inline SCM gp_set_eq(SCM *id, SCM v, SCM l, struct gp_stack *gp)
{
  SCM flags;

  flags = SCM_PACK(GP_MK_FRAME_EQ(gp_type));

  return handle(id, flags, v, l, gp, 0, 0);
}

static inline SCM gp_set_eq_bang(SCM *id, SCM v, SCM l, struct gp_stack *gp)
{

  SCM flags;

  flags = SCM_PACK(GP_MK_FRAME_EQ(gp_type));

  return handle(id, flags, v, l, gp, 0, 1);
}


static inline SCM gp_set_unbound(SCM *id, SCM l, struct gp_stack *gp)
{
  SCM flags ;

  flags = SCM_PACK(GP_MK_FRAME_UNBD(gp_type));

  return handle(id, flags, SCM_UNBOUND, l, gp, 0, 0);
}

static inline SCM gp_set_unbound_bang(SCM *id, SCM l, struct gp_stack *gp)
{
  SCM flags ;

  flags = SCM_PACK(GP_MK_FRAME_UNBD(gp_type));

  return handle(id, flags, SCM_UNBOUND, l, gp, 0, 1);
}
  
#define gp_lookup_l(i1,i2,l)						\
  do {									\
    if(SCM_UNLIKELY(!scm_is_eq(*l,SCM_EOL)))				\
      {									\
	gp_debug0("l 1\n");						\
	i1 = gp_lookup_ll(i2,l);					\
      }									\
    else if(GP_STAR(i2) || SCM_VARIABLEP(GP_UNREF(i2)))			\
      {									\
	while(1)							\
	  {								\
            if(GP_STAR(i2) && GP_POINTER(i2))                           \
	      {								\
		gp_debug0("l 2\n");					\
                i2 = GP_GETREF(GP_SCM(i2));				\
                continue;						\
	      }								\
	    else if (SCM_VARIABLEP(GP_UNREF(i2)))			\
	      {								\
		gp_debug0("l 3\n");					\
		i2 = GP_GETREF(SCM_VARIABLE_REF(GP_UNREF(i2)));		\
		continue;						\
	      }								\
	    break;							\
	  }								\
      }									\
  } while(0)							


static inline SCM * gp_lookup(SCM *id, SCM l)
 {  
  gp_debug0("lookup l>\n");
  
  
 retry:
  if(!SCM_NULLP(l)) goto advanced;

  if(SCM_VARIABLEP(GP_UNREF(id)))
    {
      id = GP_GETREF(SCM_VARIABLE_REF(GP_UNREF(id)));
      goto retry;
    }

 
  if(GP_STAR(id) && GP_POINTER(id))
    {
      id = GP_GETREF(GP_SCM(id));
      goto retry;
    }

  if(GP_STAR(id) && GP_UNBOUND(id) && !scm_is_eq(l,SCM_EOL) 
     && !scm_is_eq(l,SCM_UNBOUND)) goto advanced;
  
  gp_debug0("exit simple\n");
  gp_debug2("lookup> %x 0 val = %x\n",id,SCM_UNPACK(*id)) ;
  return id;


 advanced:
  gp_debug0("lookup> advanced\n");
  id = GP_GETREF(logical_lookup3(GP_UNREF(id),l));
  gp_debug0("lookup> /2\n");

  if(SCM_VARIABLEP(GP_UNREF(id)))
    {
      id = GP_GETREF(SCM_VARIABLE_REF(GP_UNREF(id)));
      goto advanced;
    }

  if(!GP_STAR(id)) 
    {
      gp_debug0("lookup> no star\n");
      return id;
    }

  gp_debug0("lookup> /3\n");
  
  if(GP_STAR(id) && GP_POINTER(id))
    {
      id = GP_GETREF(GP_SCM(id));
      goto retry;
    }

  gp_debug2("lookup> %x 0 val = %x\n",id,SCM_UNPACK(*id)) ;
  return id;
}

static inline SCM * gp_lookup2(SCM *id, SCM l)
 {  
  gp_debug0("lookup>\n");


  
 retry:
  if(!SCM_NULLP(l) && !scm_is_eq(l,SCM_UNBOUND)) goto advanced;

  if(SCM_VARIABLEP(GP_UNREF(id)))
    {
      id = GP_GETREF(SCM_VARIABLE_REF(GP_UNREF(id)));
      goto retry;
    }

  if(GP_POINTER(id))
    {
      id = GP_GETREF(GP_SCM(id));
      goto retry;
    }


  // if(!scm_is_eq(l,SCM_EOL)) goto advanced;
  gp_debug2("lookup> %x 0 val = %x\n",id,SCM_UNPACK(*id)) ; 
  return id;


 advanced:
  id = GP_GETREF(logical_lookup4(GP_UNREF(id),l));
  gp_debug0("lookup> /2\n");

  if(SCM_VARIABLEP(GP_UNREF(id)))
    {
      id = GP_GETREF(SCM_VARIABLE_REF(GP_UNREF(id)));
      goto advanced;
    }
  
  if(!GP_STAR(id)) 
    {
      gp_debug0("lookup> no star\n");
      return id;
    }

  gp_debug0("lookup> /3\n");

  
  if(GP_POINTER(id))
    {
      id = GP_GETREF(GP_SCM(id));
      goto retry;
    }


  gp_debug2("lookup> %x 0 val = %x\n",id,SCM_UNPACK(*id)) ;
  return id;
}



static inline SCM * gp_lookup_ll(SCM *id, SCM *l)
 {  
  gp_debug0("lookup>\n");
  
  
 retry:  
  if(!SCM_NULLP(*l) && !scm_is_eq(*l,SCM_UNBOUND)) goto advanced;
  
  if(SCM_VARIABLEP(GP_UNREF(id)))
    {
      gp_debug0("variable\n");
      id = GP_GETREF(SCM_VARIABLE_REF(GP_UNREF(id)));
      goto retry;
    }

  if(GP_STAR(id) && GP_POINTER(id))
    {
      gp_debug0("pointer\n");
      id = GP_GETREF(GP_SCM(id));
      goto retry;
    }

  if(!scm_is_eq(*l,SCM_EOL) && GP_STAR(id) && GP_UNBOUND(id) && 
     !scm_is_eq(*l,SCM_UNBOUND)) 
    goto advanced;

  return id;


 advanced:
  gp_debug0("lookup> /1\n");
  id = GP_GETREF(logical_lookup_l(GP_UNREF(id),l));
  gp_debug0("lookup> /2\n");

  if(SCM_VARIABLEP(GP_UNREF(id)))
    {
      id = GP_GETREF(SCM_VARIABLE_REF(GP_UNREF(id)));
      goto retry;
    }

  if(!GP_STAR(id)) 
    {
      gp_debug0("lookup> no star\n");
      return id;
    }

  gp_debug0("lookup> /3\n");

  if(GP_STAR(id) && GP_POINTER(id))
    {
      id = GP_GETREF(GP_SCM(id));
      goto retry;
    }

  gp_debug2("lookup> %x 0 val = %x\n",id,SCM_UNPACK(*id)) ;
  return id;
}

/*
The datastructure for a state is

'({{fr-ref} . {ci-ref}} . assoc-binddings)

The frame on the fr stack is
...
HANDLE
DLENGTH
{fr-ref}
ci-len
{ci-ref}*
cs-len

and the addition in the ci stack is
...
{ci-ref}*

{a . b} is a gp cons
{a}     is a pg var
{a}     is a frame marked gp var

With this we can auto adjust the ci stack and gc quite well even if there
is a stored frame onto the stack which greatly improves gc-ability.

The fr stack has stricter rules regarding clearing at gc e.g. we store the 
length at state save of the fr stack which means that we can have a o(N)
algorithm when searching for a common ancestor else the optimal one takes
generally a o(N^2) algorithm. Because the fr stack automatically gc without
doing a proper gc graph search we know that this restriction is way less
critical than if we combined the fr stack and the ci stack that was thre case
before we did the recent refactorisation (2016-04-27). The downside is that
we need quite a lot of data in order to create a frame currently a newframe
takes 2 gp-cons, 2 gp-vars, 6 locations on fr stack and 1 location on the ci
stack. Thats 17 64bit numbers. Quite a lot of data and mungeling needs to be
done.
 */

//#define DB(X) X
static inline SCM gp_newframe(SCM s)
{
  SCM  l;
  struct gp_stack *gp = get_gp();
  gp_debug0("newframe\n");


  if(SCM_CONSP(s))
    {
      l  = SCM_CDR(s);
    }
  else if (GP_CONSP(s))
    {
      l = GP_GETREF(s)[2];
    }
  else
    {
      s = SCM_PACK(0);
      l = scm_cons(SCM_EOL, gp_paths);
    }

  if(scm_is_eq(l, SCM_UNBOUND))
    l = scm_cons(SCM_EOL, gp_paths);

  {
    SCM ret;
    SCM *f1, *f2, *cons1, *cons2;
    SCM *fr;

    GP_TEST_CSTACK;
    GP_TEST_FRSTACK;
    GP_TEST_CCSTACK;
    GP_TEST_STACK;

    gp_gc_inc(gp);

    fr = gp->gp_fr + GP_FRAMESIZE;
    gp->gp_fr = fr;

    l = scm_is_false(l) ? SCM_EOL : l;      
    GP_SET_HANDLERS(fr,gp);
    GP_SET_DLENGTH(fr,gp);
    f1 = set_fr(fr,gp);    
    f2 = set_ci(fr,gp);    
    gp->gp_ci[0] = GP_UNREF(f2);
    gp->gp_ci++;
    
    cons1 = get_gp_cons_pure(gp);
    cons2 = get_gp_cons_pure(gp);
   
    cons2[1] = GP_UNREF(f1);
    cons2[2] = GP_UNREF(f2);
        
    GP_SET_VAR(fr,gp->gp_si,0,gp);
    GP_SET_CONS(fr,gp->gp_cs,gp);
    cons1[1] = GP_UNREF(cons2);
    cons1[2] = l;
    
    ret = GP_UNREF(cons1);
    
    gp_debug0("return\n");
    return ret;
  }    
}

static inline SCM gp_newframe_choice(SCM s)
{
  SCM  l;
  struct gp_stack *gp = get_gp();

  if(SCM_CONSP(s))
    {
      l  = SCM_CDR(s);
    }
  else if(GP_CONSP(s))
    {
      l = GP_GETREF(s)[2];
    }
  else
    {
      s = SCM_PACK(0);
      l  = scm_cons(SCM_EOL,gp_paths);
    }

  if(scm_is_eq(l, SCM_UNBOUND))
    l = scm_cons(SCM_EOL,gp_paths);
  
  {
    SCM ret;
    SCM *f1, *f2, *cons1, *cons2;
    SCM *fr;
    gp_debug0("newframe\n");
    GP_TEST_CSTACK;
    GP_TEST_FRSTACK;
    GP_TEST_CCSTACK;
    GP_TEST_STACK;

    gp_gc_inc(gp);
    
    fr = gp->gp_fr + GP_FRAMESIZE;
    gp->gp_fr = fr;

    l = scm_is_false(l) ? SCM_EOL : l;      
    GP_SET_HANDLERS(fr,gp);
    GP_SET_DLENGTH(fr,gp);
    f1 = set_fr(fr,gp);    
    f2 = set_ci(fr,gp);    
    gp->gp_ci[0] = GP_UNREF(f2);
    gp->gp_ci++;
    
    cons1 = get_gp_cons_pure(gp);
    cons2 = get_gp_cons_pure(gp);
   
    cons2[1] = GP_UNREF(f1);
    cons2[2] = GP_UNREF(f2);
    
    GP_SET_VAR(fr,gp->gp_si,1,gp);
    GP_SET_CONS(fr,gp->gp_cs,gp);
    cons1[1] = GP_UNREF(cons2);
    cons1[2] = l;
    
    ret = GP_UNREF(cons1);
    
    gp_debug0("return\n");
    return ret;
  }    
}

//#define DB(X)



static inline  SCM* gp_mk_var(SCM s)
{
  SCM *ret;
  struct gp_stack *gp = get_gp();  

  gp_debug0("got a gp\n");
  if(gp->_logical_) return GP_GETREF(make_logical());
    
  GP_TEST_STACK;

  gp_gc_inc(gp);

  ret = get_gp_var(gp);

  mask_on(gp->id,ret,SCM_PACK(GP_MK_FRAME_UNBD(gp_type))); 
  *(ret + 1) = SCM_UNBOUND;
  
  gp_debug1("returning from mk_var %x\n",ret);
  gp_format1("~a~%",ret);
  gp_debug1("returning from mk_var %x\n",ret);
  return ret;
}



static inline SCM gp_mk_cons(SCM s) 
{
  SCM *ret;
  struct gp_stack *gp;
  gp = get_gp();
  gp_debug0("gp_mk_cons\n");
  if(gp->_logical_) 
    {
      SCM x = make_logical();
      SCM y = make_logical();
      return scm_cons(x, y);
    }



  GP_TEST_STACK;
  GP_TEST_CSTACK;

  gp_gc_inc(gp);

  ret = get_gp_cons(gp);
  
  gp_debug0("in cons is %x\n");


  return GP_UNREF(ret);
}

#define gp_struct_ref(scm,i) SCM_PACK(SCM_STRUCT_DATA(scm)[i])

SCM closure_struct = SCM_BOOL_F;
#define GP_MK_CLOSURE(x,y,z,w)                                          \
  scm_c_make_struct(closure_struct, 0,                                  \
                    SCM_UNPACK(x),SCM_UNPACK(y),SCM_UNPACK(z),SCM_UNPACK(w));

#define GP_CLOSURE_P(x) (SCM_STRUCT_VTABLE(x) == closure_struct)

SCM_DEFINE(gp_set_closure_struct,"set-closure-struct!",1,0,0,(SCM scm),
	   "set the struct variable")
#define FUNC_NAME s_gp_set_closure_struct
{
  closure_struct = scm;
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM closed_error_fkn = SCM_BOOL_F;
SCM throw_closed_p;

SCM_DEFINE(gp_setup_closed, "setup-closed",1,0,0,(SCM err),
"err is the error function called when unifying closures, returns a fluid that controls the throwing of error or not")
#define FUNC_NAME s_setup_closed
{  
  closed_error_fkn = err;
  return throw_closed_p;
}
#undef FUNC_NAME


SCM namespace_fkn    = SCM_BOOL_F;
SCM namespace_struct = SCM_BOOL_F;
#define GP_NAMESPACE_P(x) (SCM_STRUCT_VTABLE(x) == namespace_struct)
#define GP_MK_NAMESPACE(x,y,z,w)                                        \
  scm_c_make_struct(namespace_struct, 0,                                \
                    SCM_UNPACK(x),SCM_UNPACK(y),SCM_UNPACK(z),SCM_UNPACK(w));

SCM_DEFINE(gp_setup_namespace, "setup-namespace",2,0,0,(SCM record, SCM nsfkn),
"supplies the record for the namespace struct and the name space unification function")
#define FUNC_NAME s_setup_namespace
{  
  namespace_fkn   = nsfkn;
  namespace_struct = record;
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

int gp_recurent(SCM *id1, SCM *id2, SCM *l)
{  
  SCM scm;

  gp_debug0("recurent>\n");

  if(!GP_STAR(id2)) goto non_gp;

  gp_lookup_l(id2,id2,l);  

  if(!GP_STAR(id2)) goto non_gp;

  gp_debug0("recurent> looked up data\n");
  if(id1 == id2  )  
    {
      gp_debug0("recurent> Found a recurence!!\n");
      return 1;  
    }
  
  if(GP_CONS(id2))
    {
      gp_debug0("recurent> got a cons\n");      
      return gp_recurent(id1,GP_CAR(id2),l) || gp_recurent(id1,GP_CDR(id2),l);      
    }

  if(GP_UNBOUND(id2)) return 0;

  if(GP_ATTR(id2))
    {
      return 0;
    }


  
  scm = GP_SCM(id2);

  gp_debug0("linked scm\n");
 retry:
  if(SCM_CONSP(scm))
    {
      gp_debug0("cons\n");
      return 
        (gp_recurent(id1,GP_GETREF(SCM_CAR(scm)),l) ||
         gp_recurent(id1,GP_GETREF(SCM_CDR(scm)),l));
    }

  if(SCM_I_IS_VECTOR(scm))
    {
      gp_debug0("vector\n");
      scm = scm_vector_to_list(scm);
      goto retry;
    }


  if(SCM_STRUCTP(scm))
    {
      if(GP_CLOSURE_P(scm))
        {
          scm = gp_struct_ref(scm,2);
          goto retry;
        } 
      else if (GP_NAMESPACE_P(scm))
        {
          scm = gp_struct_ref(scm,0);
          goto retry;
        }

      return 0;
    }

  if(SCM_FLUID_P(scm))
    {
      scm = scm_fluid_ref(scm);
      goto retry;
    }


  gp_debug0("recurent> atom!\n");
  return 0;

 non_gp:
  {
    SCM scm = GP_UNREF(id2);
    gp_debug0("non gp\n");
  retryII:
    if(SCM_CONSP(scm))
      {
        gp_debug0("cons\n");
        return 
          gp_recurent(id1,GP_GETREF(SCM_CAR(scm)), l) ||
          gp_recurent(id1,GP_GETREF(SCM_CDR(scm)), l);
      }

    if(SCM_I_IS_VECTOR(scm))
      {
        gp_debug0("vector\n");
        scm = scm_vector_to_list(scm);
        goto retryII;
      }
  }
  
  return 0;
}

static SCM smob2scm_gp(SCM *id, SCM s)
{
  SCM scm;
 
  if(GP_UNBOUND(id) || GP_ATTR(id))
    return GP_UNREF(id);
  
  scm = GP_SCM(id);
  if(SCM_CONSP(scm))
    {	  
      SCM car = smob2scm(SCM_CAR(scm), s);
      SCM cdr = smob2scm(SCM_CDR(scm), s);
      if(car == SCM_CAR(scm) && cdr == SCM_CDR(scm))
	return scm;
      return scm_cons(car,cdr);
    }
  else
    return scm;
}

int _gp_2_scm(SCM **spp, int nargs, SCM *cl, SCM *max)
{
  SCM *sp = *spp;
  if(nargs != 2)
    scm_misc_error("gp->scm","wrong number of arguments", SCM_EOL);
  
  sp[-2] = smob2scm(sp[-1],sp[0]);
 
  *spp = sp - 2;
  return -1;
}


SCM_DEFINE( smob2scm, "gp->scm", 2, 0, 0, (SCM scm, SCM s),
	    "creates a scheme representation of a gp object")
#define FUNC_NAME s_smob2scm
{
  gp_debus0("gp->scm>\n");
  if(GP(scm))
    {
      SCM *id;
      id = UN_GP(scm);
      
      id = GP_GETREF(gp_gp_lookup(scm, s));
      if(!GP_STAR(id))
	{
	  scm = GP_UNREF(id);
	  goto do_scm;
	}

      if(GP_CONS(id))
	{
	  return scm_cons(smob2scm(SCM_PACK(GP_CAR(id)), s),
			  smob2scm(SCM_PACK(GP_CDR(id)), s));
	}      
      return smob2scm_gp(id, s);
    }
  else 
    {
    do_scm:
      if(SCM_CONSP(scm))
	{	  
	  SCM car = smob2scm(SCM_CAR(scm), s);
	  SCM cdr = smob2scm(SCM_CDR(scm), s);
	  if(car == SCM_CAR(scm) && cdr == SCM_CDR(scm))
	    return scm;
	  return scm_cons(car,cdr);
	}
      
      if(SCM_STRUCTP(scm))
        {
          if(GP_CLOSURE_P(scm))
            {
              SCM args = smob2scm(gp_struct_ref(scm,2), s);
              SCM f    = gp_struct_ref(scm,1);
                
              return scm_apply_0(f,args);
            }

          if(GP_NAMESPACE_P(scm))            
            return GP_MK_NAMESPACE(smob2scm(gp_struct_ref(scm,0), s),
                                   gp_struct_ref(scm,1),
                                   gp_struct_ref(scm,2),
                                   gp_struct_ref(scm,3));
        }
      if(SCM_I_IS_VECTOR(scm))
	{
	  SCM l = scm_vector_to_list(scm);
	  SCM u = smob2scm(l, s);
	  return scm_vector(u);
	}
      return scm;
    }
}
#undef FUNC_NAME

int len(SCM x, SCM *l)
{
  int i = 0;
  SCM *id = GP_GETREF(x);

 retry:
  gp_lookup_l(id,id,l);
  
  if(GP_STAR(id))
    {
      if(GP_UNBOUND(id))
        return -1;

      if(GP_CONS(id))
        {
          i++;
          id = GP_CDR(id);
          goto retry;
        }
    }
  else
    {
      x = GP_UNREF(id);
      if(SCM_CONSP(x))
        {
          i++;
          id = GP_GETREF(SCM_CDR(x));          
          goto retry;
        }
    }
  
  return i;
}

#define QCDR(x)   GP_GETREF(SCM_CDR(GP_UNREF(x))) 
#define QCAR(x)   GP_GETREF(SCM_CAR(GP_UNREF(x))) 
#define QCONSP(x) SCM_CONSP(GP_UNREF(x))

#define DO_NAMESPACE(scm2, iid2, iid1, attr)                            \
  {                                                                     \
    SCM scm2 = GP_STAR(iid2)?GP_SCM(iid2):GP_UNREF(iid2);               \
    /*gp_format1("(do_ns ~a)~%",scm2);*/				\
    if(SCM_STRUCTP(scm2))                                               \
      {                                                                 \
        if(GP_NAMESPACE_P(scm2))                                        \
          {                                                             \
            SCM bang = SCM_BOOL_T;                                      \
            if(!gp_plus_unify)                                          \
              {                                                         \
                bang = SCM_BOOL_F;                                      \
              }                                                         \
            {                                                           \
              SCM s = gp_make_s(fr,l);                                  \
              s = scm_call_4(namespace_fkn,s,scm2,GP_UNREF(iid1),bang); \
              if(scm_is_false(s))                                       \
                return (SCM) 0;                                         \
                                                                        \
              SCM ll = SCM_CDR(s);                                      \
              if(vlist_p(ll))                                           \
                {                                                       \
                  l[1] = GP_UNREF((SCM_I_VECTOR_WELTS(S(ll,0))));       \
                  l[2] = SCM_PACK(my_scm_to_int(S(ll,1)));              \
                }                                                       \
              else                                                      \
                l[0] = ll;                                              \
            }                                                           \
                                                                        \
            U_NEXT;                                                     \
          }                                                             \
      }                                                                 \
  }
//#define DB(X) X
// unify under + means unification - means just match

SCM trampoline = SCM_BOOL_F;
SCM delayers   = SCM_BOOL_F;
SCM_DEFINE(gp_set_attributed_trampoline, "gp-set-attribute-trampoline", 2, 0, 0, (SCM x, SCM d), 
	   "check to see if variable is an attributed variable")
#define FUNC_NAME s_gp_set_attributed_trampoline
{
  trampoline = x;
  delayers   = d;
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

//#define DB(X) X
static SCM gp_unify(SCM *id1, SCM *id2, int raw, int gp_plus_unify, SCM *l, struct gp_stack *gp, SCM fr)  
{ 
  SCM * stack[110];
  int   sp;
  sp = 0;
  if(scm_is_eq(*l,SCM_UNBOUND))
    scm_misc_error("unify",
		   "using an unwinded frame as s (gp-unify! ~a ~a) : l = ~a~%",
		   scm_list_3(GP_UNREF(id1),GP_UNREF(id2),*l));
#define U_NEXT					\
  {						\
  if(SCM_UNLIKELY(sp==0))			\
    {						\
      return *l;				\
    }						\
  else						\
    {						\
      id2 = stack[--sp];			\
      id1 = stack[--sp];			\
      goto retry;				\
    }						\
}

 retry:
  gp_debug0("unify>\n");  

  if(id1 == id2)  U_NEXT;
  
  if(SCM_CONSP(GP_UNREF(id1)) && SCM_CONSP(GP_UNREF(id2)))
    goto uu00;

  gp_lookup_l(id2,id2,l);
  gp_lookup_l(id1,id1,l);

  if(GP_STAR(id1))
    {
      if(GP_STAR(id2))
	{
	  gp_debug0("11>\n");	  
	  if(id1 == id2) U_NEXT;

	  if(GP_ATTR(id1))
	    goto retry_attr;
	  if(GP_ATTR(id2)) 
	    goto retry_attr_rev;

	  if(GP_UNBOUND(id1)) goto unbd_id1;
	  if(GP_UNBOUND(id2)) goto unbd_id2;

	  if(SCM_CONSP(GP_SCM(id1)))
	    {
	      id1 = GP_GETREF(GP_SCM(id1));              
	      if(SCM_CONSP(GP_SCM(id2)))
		{
		  id2 = GP_GETREF(GP_SCM(id2));
		  goto u00;
		}
	      else
		{
                  DO_NAMESPACE(scm2, id2, id1, 1);
                  goto u01;
                }
	    }
	  else
	    {
	      if(SCM_CONSP(GP_SCM(id2)))
		{
		  id2 = GP_GETREF(GP_SCM(id2));
                  DO_NAMESPACE(scm1, id1, id2, 1);
                  goto u10;
		}
	      //u11 falls through
	    }
	}
      else
	{
	  gp_debug0("10>\n");
	  if(GP_ATTR(id1)) goto retry_attr;
	  if(GP_UNBOUND(id1)) goto unbd_id1;
	  gp_debug0("10> lookup__\n");
	  if(SCM_CONSP(GP_SCM(id1)))
	    {
	      id1 = GP_GETREF(GP_SCM(id1));
              DO_NAMESPACE(scm2, id2, id1, 1);
	      goto u00;
	    }

	  goto u10;
	}
    }
  else
    {
      if(GP(GP_UNREF(id2)))
	{
	  gp_debug0("01>\n");
	  if(GP_ATTR(id2)) goto retry_attr_rev;
	  if(GP_UNBOUND(id2)) goto unbd_id2;

	  if(SCM_CONSP(GP_SCM(id2)))
	    {
	      id2 = GP_GETREF(GP_SCM(id2));
              DO_NAMESPACE(scm1, id1, id2, 1);
	      goto u00;
	    }

	  goto u01;
	}

      gp_debug0("00>\n");     
      goto u00;
    }
  
  // u11 Has unbounded variables
  gp_debug0("unify> looked up with u11\n");
  if(GP_ATTR(id1))
    {
    retry_attr:
      {
        SCM scm_plus;

        if(gp_plus_unify)
          scm_plus = SCM_BOOL_T;
        else
          scm_plus = SCM_BOOL_F;

        scm_fluid_set_x(delayers, 
                        scm_cons(scm_list_4(scm_variable_ref(trampoline),
                                            GP_UNREF(id2),
                                            GP_UNREF(id1),
                                            scm_plus),
                                 scm_fluid_ref(delayers)));
        
        U_NEXT;
      }
    }
  else if (GP_ATTR(id2))
    {
    retry_attr_rev:
      {
	SCM *temp = id1;
	id1 = id2;
	id2 = temp;
	goto retry_attr;
      }
    }

 unbd_id1:
  if(GP_UNBOUND(id1))
    {
      if(id1 == id2)
	{
	  gp_debug0("unify> var == var");
	  U_NEXT;
	}
      else
	{
	  if(gp_plus_unify) 
            {
              DO_NAMESPACE(scm1, id2, id1, 1);
	      if(GP_STAR(id2))
		goto  unbound1;
	      else
		goto  unbound_10;
            }
	  else
	    return (SCM) 0;
	}
    }
  
 unbd_id2:
  if(GP_UNBOUND(id2))
    {
      if(id1 == id2)
	{
	  gp_debug0("unify> var == var");
	  U_NEXT;
	}
      else
	{
	  if(gp_plus_unify)
            {
              DO_NAMESPACE(scm1, id1, id2, 1);
	      if(GP_STAR(id1))
		goto  unbound2;
	      else
		{
		  SCM * temp = id1;
		  id1 = id2;
		  id2 = temp;
		  goto  unbound_10;
		}
            }
	  else
	    return (SCM) 0;
	}
    }

  //any conses?
#define DO_CONS						\
  {							\
    gp_debug0("unify> cons\n");				\
    if(SCM_UNLIKELY(sp >= 100))				\
      {							\
        SCM ret = gp_unify(GP_CAR(id1), GP_CAR(id2)           \
                           , raw, gp_plus_unify,l,gp,fr);     \
        if(!ret) return (SCM) 0;                          \
	id1 = GP_CDR(id1);			     	\
	id2 = GP_CDR(id2);				\
	goto retry;					\
      }							\
    else						\
      {							\
	stack[sp++] = GP_CDR(id1);			\
	stack[sp++] = GP_CDR(id2);			\
	id1 = GP_CAR(id1);				\
	id2 = GP_CAR(id2);				\
	goto retry;					\
      }							\
  }

  //Cons check
  if(GP_CONS(id1))
    {
      if(GP_CONS(id2))
	{
	  DO_CONS;
	}
      else
        DO_NAMESPACE(scm2, id2, id1, 1);
	return (SCM) 0;
    }

  if(GP_CONS(id2))
    {
      DO_NAMESPACE(scm1, id1, id2, 1);
      return (SCM) 0;
    }

  //has to be an equality check
  if(GP_EQ(id1) || GP_EQ(id2))
    {
      gp_debug2("unify> (eq ~x ~x)\n", GP_SCM(id1), GP_SCM(id2));
      if(scm_is_eq(GP_SCM(id1), GP_SCM(id2)))
	{U_NEXT;}
      else
	return (SCM) 0;
    }
    
  SCM scm1 = GP_SCM(id1);
  SCM scm2 = GP_SCM(id2);

 scm_check: 

  //printf("scn_check\n");
  if(SCM_I_IS_VECTOR(scm1) && SCM_I_IS_VECTOR(scm2))
    {
      int n = SCM_I_VECTOR_LENGTH(scm1);
      if(n == SCM_I_VECTOR_LENGTH(scm2))
	{
          if(n == 1)
            {
              int n1 = len(SCM_SIMPLE_VECTOR_REF(scm1,0), l);
              int n2 = len(SCM_SIMPLE_VECTOR_REF(scm2,0), l);
              if(n1 >= 0 && n2 >= 0 && n1 != n2)
                {
                  return (SCM) 0;
                }
            }
          
	  if(2*n      > 100) return (SCM) 0;
	  if(2*n + sp > 100)
	    {
	      SCM ret = gp_unify(GP_GETREF(scm1), GP_GETREF(scm2), 
				 raw, gp_plus_unify,l,gp,fr);	
	      if(!ret) return (SCM) 0;
	      U_NEXT;
	    }
	  
	  int i = 0;
	  for(; i<n ; i++)
	    {
	      stack[sp++] = GP_GETREF(SCM_SIMPLE_VECTOR_REF(scm1,i));
	      stack[sp++] = GP_GETREF(SCM_SIMPLE_VECTOR_REF(scm2,i));
	    }

	  U_NEXT;
	}
      else
	return (SCM) 0;
    }

  if(SCM_NUMBERP(scm1)  || SCM_NUMBERP(scm2)) 
    gp_format2("unify> (equal/= ~a ~a)\n", scm1, scm2);

  if(SCM_NUMBERP(scm1))
    {      
      if(SCM_NUMBERP(scm2))
        {
          if(SCM_INEXACTP(scm1))
            if(SCM_INEXACTP(scm2))
              {
              num_retry:
              if(SCM_REALP(scm1) && SCM_REALP(scm2))
                {
                  double r1 = scm_to_double (scm1);
                  double r2 = scm_to_double (scm2);

                  if(r1 < 0 && r2 < 0)
                    {
                      r1 = -r1;
                      r2 = -r2;
                    }

                  if (r1 < 1.000000000001 * r2 && r2 < 1.000000000001 * r1)
                    {U_NEXT;}
                  else
                    return
                      (SCM) 0;
                }
              else
                {
                  if(SCM_REALP(scm1) || SCM_REALP(scm2))
                    return (SCM) 0;
                  
                  double r1 = scm_c_real_part(scm1);
                  double r2 = scm_c_real_part(scm2);
                  double c1 = scm_c_imag_part(scm1);
                  double c2 = scm_c_imag_part(scm2);
                  if(r1 < 0 && r2 < 0)
                    {
                      r1 = -r1;
                      r2 = -r2;
                    }
                  if(c1 < 0 && c2 < 0)
                    {
                      c1 = -c1;
                      c2 = -c2;
                    }

                  if (r1 < 1.000000000001 * r2 && r2 < 1.000000000001 * r1 &&
                      c1 < 1.000000000001 * c2 && c2 < 1.000000000001 * c1)
                    {U_NEXT;}
                  else
                    return (SCM) 0;
                }
              }
            else
              if(SCM_FRACTIONP(scm2))
                {
                  scm2 = scm_exact_to_inexact (scm2);
                  goto num_retry;
                }
              else
                return (SCM) 0;
          else
            if(SCM_INEXACTP(scm2))
              if(SCM_FRACTIONP(scm1))
                {
                  scm1 = scm_exact_to_inexact (scm1);
                  goto num_retry;
                }
              else
                return (SCM) 0;
            else
              if(scm_is_true(scm_num_eq_p(scm1, scm2)))
                {U_NEXT;}
              else
                return (SCM) 0;
        }
      else
        return (SCM) 0;
    }
  
  if(scm_is_true(scm_string_p (scm1)))
    {
      if(scm_is_true(scm_procedure_p (scm2)))
        {
          scm2 = gp_procedure_name(scm2);
          if(scm_is_true (scm2))
            {
              scm2 = scm_symbol_to_string (scm2);
              goto equalp;
            }
          return (SCM) 0;
        }
    }
  
  if(scm_is_true(scm_string_p (scm2)))
    {
      if(scm_is_true(scm_procedure_p (scm1)))
        {
          scm1 = gp_procedure_name(scm1);
          if(scm_is_true (scm1))
            {
              scm1 = scm_symbol_to_string (scm1);
              goto equalp;
            }
          
          return (SCM) 0;
        }
    }

  if(SCM_STRUCTP(scm1))
    {
      if(GP_CLOSURE_P(scm1))
        {
          if(SCM_STRUCTP(scm2))
            {
              if(GP_NAMESPACE_P(scm2))
                {
                  if(!gp_plus_unify)
                    {
                      scm_t_bits *bits1 = SCM_STRUCT_DATA(scm1);
                      scm1 = SCM_PACK(bits1[0]);
                      goto scm_check;
                    }
                  else
                    {
                      SCM s = gp_make_s(fr,l);
                      SCM bang = SCM_BOOL_T;                                  
                      if(!gp_plus_unify)                                      
                        {                                                     
                          bang = SCM_BOOL_F;                                  
                        }                                                     

                      s = scm_call_4(namespace_fkn,s,scm2,scm1,bang);
                      if(scm_is_false(s))
                        return (SCM) 0;
                      
                      {
                        SCM ll = SCM_CDR(s);
                        if(vlist_p(ll))
                          {
                            l[1] = GP_UNREF((SCM_I_VECTOR_WELTS(S(ll,0))));
                            l[2] = SCM_PACK(my_scm_to_int(S(ll,1)));
                          } 
                        else
                          l[0] = ll;
                      }
                    }
                
                  U_NEXT;
                }
              else
              
              if(GP_CLOSURE_P(scm2))
                {
                  scm_t_bits *bits1 = SCM_STRUCT_DATA(scm1);
                  scm_t_bits *bits2 = SCM_STRUCT_DATA(scm2);
                  if(bits1[1] == bits2[1])
                    {
                      stack[sp++] = GP_GETREF(SCM_PACK(bits1[2])); 
                      stack[sp++] = GP_GETREF(SCM_PACK(bits2[2])); 
                      U_NEXT;
                    }
                  else
                    {
                      if(scm_is_true(GP_UNREF(bits1[3])) || 
                         scm_is_true(GP_UNREF(bits2[3])))
                        {
                          if(scm_is_true(scm_fluid_ref(throw_closed_p)))
                            {
                              scm_call_2(closed_error_fkn, scm1, scm2);
                            }                 
                        }
                      return (SCM) 0;
                    }
                }
            }
          else
            return (SCM) 0;
        }
      else 
        if(GP_NAMESPACE_P(scm1))
          {
            if(!gp_plus_unify)
              {
                scm_t_bits *bits1 = SCM_STRUCT_DATA(scm1);
                scm1 = SCM_PACK(bits1[0]);
              goto scm_check;
            }
            else
              {
                SCM s = gp_make_s(fr,l);
                SCM bang = SCM_BOOL_T;                                  
                if(!gp_plus_unify)                                      
                  {                                                     
                    bang = SCM_BOOL_F;                                  
                  }                                                     

                s = scm_call_4(namespace_fkn,s,scm1,scm2,bang);
                if(scm_is_false(s))
                  return (SCM) 0;

                {
                  SCM ll = SCM_CDR(s);
                  if(vlist_p(ll))
                    {
                      l[1] = GP_UNREF((SCM_I_VECTOR_WELTS(S(ll,0))));
                      l[2] = SCM_PACK(my_scm_to_int(S(ll,1)));
                    } 
                  else
                    l[0] = ll;
                }
                
                U_NEXT;
              }
          }
    }

  
  if(SCM_STRUCTP(scm2))
    {
      if(GP_NAMESPACE_P(scm2))
          {
            if(!gp_plus_unify)
              {
                scm_t_bits *bits1 = SCM_STRUCT_DATA(scm1);
                scm1 = SCM_PACK(bits1[0]);
                goto scm_check;
              }
            else
              {
                SCM s = gp_make_s(fr,l);
                SCM bang = SCM_BOOL_T;                                  
                if(!gp_plus_unify)                                      
                  {                                                     
                    bang = SCM_BOOL_F;                                  
                  }                                                     

                s = scm_call_4(namespace_fkn,s,scm2,scm1,bang);
                if(scm_is_false(s))
                  return (SCM) 0;

                {
                  SCM ll = SCM_CDR(s);
                  if(vlist_p(ll))
                    {
                      l[1] = GP_UNREF((SCM_I_VECTOR_WELTS(S(ll,0))));
                      l[2] = SCM_PACK(my_scm_to_int(S(ll,1)));
                    } 
                  else
                    l[0] = ll;
                }
          
                U_NEXT;
              }
          }
    }

  
  if(SCM_FLUID_P(scm1))
    {
      if(SCM_FLUID_P(scm2))
        {
          scm1 = scm_fluid_ref(scm1);
          scm2 = scm_fluid_ref(scm2);
          goto  scm_check;
        }
      else
        return (SCM) 0;
    }

 equalp:  
  if(scm_is_true(scm_equal_p(scm1, scm2)))
    {U_NEXT;}
  else
    return (SCM) 0;
 
 unbound1: 
  gp_debug0("unify> unbound1\n");
  {
    if(!raw && GP_CONS(id2))
      if(gp_recurent(id1,id2,l))  
	return (SCM) 0;
    
    gp_lookup_l(id2,id2,l);
    gp_set_ref_l(id1,GP_UNREF(id2),l,gp);
    U_NEXT;
  }

 unbound2: 
  gp_debug0("unify> unbound2\n");
  if(!raw && GP_CONS(id1))
    if(gp_recurent(id2,id1,l)) 
      return (SCM) 0;
    
  gp_lookup_l(id1,id1,l);
  gp_set_ref_l(id2,GP_UNREF(id1),l,gp);
  U_NEXT;

 u00:
  gp_debug0("unify> looked up with u00\n");

  //conses!!
#define DO_CONS2		       			\
  {							\
    gp_debug0("unify> cons\n");				\
    if(SCM_UNLIKELY(sp >= 100))						\
      {                                                                 \
        SCM ret = gp_unify(QCAR(id1),QCAR(id2),raw, gp_plus_unify, l, gp, fr); \
        if(!ret)                                                          \
          return (SCM) 0;                                               \
        id1 = QCDR(id1);                                                \
        id2 = QCDR(id2);                                                \
        goto retry;                                                     \
      }                                                                 \
    else                                                                \
      {							\
	stack[sp++] = QCDR(id1);			\
	stack[sp++] = QCDR(id2);			\
	id1 = QCAR(id1);				\
	id2 = QCAR(id2);				\
	goto retry;					\
      }							\
  }


  if(QCONSP(id1))
    {
      if(QCONSP(id2))
	{
	  //printf("scm cons unify\n");
        uu00:
	  DO_CONS2;
	}
      else
        {
          DO_NAMESPACE(scm2, id2, id1, 1);
          return (SCM) 0;
        }
    }

  if(QCONSP(id2)) 
    {
      DO_NAMESPACE(scm2, id1, id2, 1);
      return (SCM) 0;
    }
  
  scm1 = GP_UNREF(id1);
  scm2 = GP_UNREF(id2);
  goto scm_check;
  
 u01:
  {
    SCM *x;
    gp_debug0("unify> looked up with u01>\n");
    x = id1;
    id1 = id2;
    id2 = x;
  }

 u10:
  gp_debug0("unify> looked up with u10>\n");
  if(GP_UNBOUND(id1)) 
    {
      DO_NAMESPACE(scm2, id2, id1, 1);
      goto  unbound_10;
    }

  //conses!!
#define DO_CONS3					\
  {							\
    gp_debug0("unify> cons\n");				\
    if(SCM_UNLIKELY(sp >= 18))				\
      {							\
        SCM ret = gp_unify(GP_CAR(id1), QCAR(id2)		\
			   ,raw, gp_plus_unify, l, gp, fr);        \
        if(!ret)  return (SCM) 0;				\
	id1 = GP_CDR(id1);					\
	id2 = QCDR(id2);				\
	goto retry;					\
      }							\
    else						\
      {							\
	stack[sp++] = GP_CDR(id1);			\
	stack[sp++] = QCDR(id2);			\
	id1 = GP_CAR(id1);				\
	id2 = QCAR(id2);				\
	goto retry;					\
      }							\
  }


  if(GP_CONS(id1))
    {
      if(QCONSP(id2))
	{
	  DO_CONS3;
	}
      else
        {
          DO_NAMESPACE(scm2, id2, id1, 1);
          return (SCM) 0;
        }
    }
  if(QCONSP(id2)) 
    {
      DO_NAMESPACE(scm2, id1, id2, 1);
      return (SCM) 0;
    }

  //printf("scm eq 10\n");
  if(GP_EQ(id1))
    {
      if(scm_is_eq(GP_SCM(id1),GP_UNREF(id2)))
	{U_NEXT;}
      else
	return (SCM) 0;
    }

  scm1 = GP_SCM(id1);
  scm2 = GP_UNREF(id2);

//printf("scm check 10\n");
  goto scm_check;
  
 unbound_10: 
  gp_debug0("unify> unbound1\n");

  if(!raw && (QCONSP(id2) || SCM_I_IS_VECTOR(GP_UNREF(id2)))
     && gp_recurent(id1,id2,l)) return (SCM) 0;    
  if(GP(GP_UNREF(id2))) 
    scm_misc_error ("unify 01 / 10 error", "unify variable at 0 place", 
		    scm_list_1 (GP_UNREF(id2)));
  if(gp_plus_unify)
    {
      gp_set_val_l(id1,GP_UNREF(id2),l,gp);
      U_NEXT;
    }
  return (SCM) 0;
}
//#define DB(X) 
int _gp_unify(SCM **spp, int nargs, SCM *cl, SCM *max)
{
  SCM fr, ll, *sp, s,v1,v2,*vv1, *vv2, ret, l[3], ggp, old, oldi;
  struct gp_stack *gp;
  
  gp_debug0("unify\n");
  sp = *spp;
  s  = sp[0];
  v2 = sp[-1];
  v1 = sp[-2];

  if(SCM_UNLIKELY(nargs != 3))
    scm_misc_error("gp-unify!","wrong number of arguments", SCM_EOL);

  UNPACK_ALL(fr,ll, l[0],ggp,gp,s,"failed to unpack in gp_gp_unify");

  old  = l[0];

  if(vlist_p(l[0]))
    {
      l[1] = GP_UNREF((SCM_I_VECTOR_WELTS(S(l[0],0))));
      l[2] = SCM_PACK(my_scm_to_int(S(l[0],1)));
      oldi = l[2];
    }
  else
    oldi = SCM_BOOL_F;

  gp_debus0("gp-unify!>\n");
  vv1 = UN_GP(v1);
  vv2 = UN_GP(v2);
  ret = gp_unify(vv1,vv2,0,1,l,gp,fr);

  gp_debus0("/gp-unify!>\n");
  if(ret)
    {
      if(vlist_p(l[0]) && oldi != l[2])
        {
          l[0] = make_vlist(S(l[0],0),SCM_UNPACK(l[2]));
        }
         
      PACK_ALL(fr,ll,old,l[0],ggp,s);    

      sp[-3] = s;
      *spp = sp - 3;
      return -1;
    }
  
  sp[-3] = SCM_BOOL_F;
  *spp = sp - 3;
  return -1;  
}

SCM_DEFINE(gp_gp_unify,"gp-unify!",3,0,0,(SCM v1, SCM v2, SCM s),
	   "unifies two gp variables")
#define FUNC_NAME s_gp_gp_unify
{
  SCM fr, ll, *vv1, *vv2, ret, l[3], ggp, old, oldi;
  struct gp_stack *gp;
  gp_debus0("gp-unify!>\n");

  UNPACK_ALL(fr,ll, (l[0]),ggp,gp,s,"failed to unpack in gp_gp_unify");


  vv1  = UN_GP(v1);
  vv2  = UN_GP(v2);
  old  = l[0];
 
  if(vlist_p(l[0]))
    {
      l[1] = GP_UNREF((SCM_I_VECTOR_WELTS(S(l[0],0))));
      l[2] = SCM_PACK(my_scm_to_int(S(l[0],1)));
      oldi = l[2];
    }
  else
    oldi = SCM_BOOL_F;

  //format1("unify> ~a~%",l[0]);
 
  ret = gp_unify(vv1,vv2,0,1,l,gp,fr);
  gp_debus0("/gp-unify!>\n");
  if(ret)
    {
      if(vlist_p(l[0]) && (oldi != l[2] || old != l[0]))
        {
          l[0] = make_vlist(S(l[0],0),SCM_UNPACK(l[2]));
        }
      
      //format2("unify/end> ~a ~a~%",l[0],old);

      PACK_ALL(fr,ll, old, l[0], ggp,s);

      //printf("true\n");
      return s;
    }
  if(vlist_p(old)) vhash_truncate_x(old);
    
  //printf("false\n");

  return SCM_BOOL_F;
}
#undef FUNC_NAME
//#define DB(X) 

int _gp_unify_raw(SCM **spp, int nargs, SCM *cl, SCM *max)
{
  SCM fr, ll, *sp, s,v1,v2,*vv1, *vv2, ret, l[3], ggp, old, oldi;
  struct gp_stack *gp;
  
  sp = *spp;
  s  = sp[0];
  v2 = sp[-1];
  v1 = sp[-2];

  gp_debus0("gp-unify _raw!>\n");
  if(SCM_UNLIKELY(nargs != 3))
    scm_misc_error("gp-unify-raw!","wrong number of arguments", SCM_EOL);
  
  UNPACK_ALL(fr,ll, l[0],ggp,gp,s,"failed to unpack in gp_gp_unify");

  old  = l[0];

  if(vlist_p(l[0]))
    {
      l[1] = GP_UNREF((SCM_I_VECTOR_WELTS(S(l[0],0))));
      l[2] = SCM_PACK(my_scm_to_int(S(l[0],1)));
      oldi = l[2];
    }
  else
    oldi = SCM_BOOL_F;

  gp_debus0("gp-unify!>\n");
  vv1 = UN_GP(v1);
  vv2 = UN_GP(v2);
  ret = gp_unify(vv1,vv2,1,1,l,gp,fr);

  gp_debus0("/gp-unify!>\n");
  if(ret)
    {
      if(vlist_p(l[0]) && (oldi != l[2] || old != l[0]))
        {
	 l[0] = make_vlist(S(l[0],0),SCM_UNPACK(l[2]));
        }

      PACK_ALL(fr, ll, old, l[0], ggp,s);
      
      sp[-3] = s;
      *spp = sp - 3;
      return -1;
    }
  
  sp[-3] = SCM_BOOL_F;
  *spp = sp - 3;
  return -1;  
}

SCM_DEFINE(gp_gp_unify_raw,"gp-unify-raw!",3,0,0,(SCM v1, SCM v2, SCM s),
	   "unifies two gp variables")
#define FUNC_NAME s_gp_gp_unify
{
  SCM fr, ll, *vv1, *vv2, ret, l[3], ggp, old, oldi;
  struct gp_stack *gp;
  gp_debus0("gp-unify-raw!>\n");

  UNPACK_ALL(fr,ll, l[0],ggp, gp,s,"failed to unpack in gp_gp_unify_raw");

  old  = l[0];

  vv1 = UN_GP(v1);
  vv2 = UN_GP(v2);
  if(vlist_p(l[0]))
    {
      l[1] = GP_UNREF((SCM_I_VECTOR_WELTS(S(l[0],0))));
      l[2] = SCM_PACK(my_scm_to_int(S(l[0],1)));
      oldi = l[2];
    }
  else
    oldi = SCM_BOOL_F;

  ret =  gp_unify(vv1,vv2,1,1,l,gp,fr);
  gp_debus0("/gp-unify-raw!>\n");
  if(ret)
    {
      if(vlist_p(l[0]) && oldi != l[2])
        {
	 l[0] = make_vlist(S(l[0],0),SCM_UNPACK(l[2]));
        }

      PACK_ALL(fr,ll,old,l[0],ggp,s);

      //printf("true\n");
      return s;
    }

  //printf("false\n");
  return SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_next_budy, "gp-budy", 1, 0, 0, (SCM x),
	   "Assumes that v1 and v2 is allocated consecutively returns v2 when feeded by v1")
#define FUNC_NAME s_gp_next_budy
{
  scm_misc_error ("budy error", "cannot budy - not supprted use attributed variables instead", 
		  scm_list_1 (x));
  return SCM_BOOL_F;
}

#undef FUNC_NAME

int _gp_mkvar(SCM **spp, int nargs, SCM *cl, SCM *max)
{
  SCM *sp = *spp;
  if(nargs != 1)
    scm_misc_error("gp-var!","wrong number of arguments", SCM_EOL);
  
  sp[-1] = GP_IT(gp_mk_var(sp[0]));
 
  *spp = sp - 1;
  return -1;
}

SCM_DEFINE(gp_mkvar, "gp-var!", 1, 0, 0, (SCM s),
	   "makes a unbounded gp variable")
#define FUNC_NAME s_gp_mkvar
{
  return GP_IT(gp_mk_var(s));
}
#undef FUNC_NAME

#define GP_VARP(id) (GP_STAR(id) && GP_UNBOUND(id))

SCM_DEFINE(gp_varp,"gp-var?",2,0,0,(SCM x, SCM s),
	   "Test for an unbound variable.")
#define FUNC_NAME s_gp_varp
{  
  SCM *id;
  if(GP(x))
    {
      id = GP_GETREF(gp_gp_lookup(x, s));
      return (GP_STAR(id) && GP_UNBOUND(id)) ? SCM_BOOL_T : SCM_BOOL_F;
    }
  return SCM_BOOL_F;
}
#undef FUNC_NAME


SCM_DEFINE(gp_atomicp,"gp-atomic?",2,0,0,(SCM x, SCM s),
	   "Test for a atomic gp")
#define FUNC_NAME s_gp_atomicp
{
  SCM *id;
  if(GP(x))
    {
      id = GP_GETREF(gp_gp_lookup(x,s));
      if(GP_STAR(id))
        return (GP_VAL(id) && !SCM_CONSP(GP_SCM(id)))
          ? SCM_BOOL_T : SCM_BOOL_F;
      
      return SCM_CONSP(GP_UNREF(id)) ? SCM_BOOL_F : SCM_BOOL_T;
    }
  return scm_is_pair(x) ? SCM_BOOL_F : SCM_BOOL_T;
}
#undef FUNC_NAME

static void gp_unwind0(SCM *fr, SCM *ci, SCM *si, SCM *cs, struct gp_stack *gp, SCM path, SCM lpath);

void gp_unwind_dynstack(struct gp_stack *gp, scm_t_bits dyn_n);


SCM_DEFINE(gp_set_current_stack_x, "gp-set-current-stack", 1, 0, 0, (SCM x),
	   "resets the unifyier stacks")
#define FUNC_NAME gp_set_current_stack_x
{ 
  current_stack = x;
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_clear, "gp-clear", 1, 0, 0, (SCM s),
	   "resets the unifyier stacks")
#define FUNC_NAME s_gp_clear
{
  struct gp_stack *gp = get_gp();
  gp_debug0("clear\n");
  gp_unwind0(gp->gp_frstack + GP_FRAMESIZE,
	     gp->gp_cstack + 1,
	     gp->gp_stack, 
	     gp->gp_cons_stack, gp, SCM_BOOL_F, SCM_BOOL_F);
  gp_debug0("clear 2\n");
  gp_unwind_dynstack(gp, 2);
  gp->rguards  = SCM_EOL;
  gp->dynstack = SCM_EOL;
  gp->dynstack_length = 2;
  gp->handlers = SCM_EOL;
  gp->_logical_ = 0;
  gp_debug0("clear end\n");
  
  scm_fluid_set_x(current_stack, 
		  scm_cons(scm_cons(GP_GET_SELF(gp->gp_fr),
				    GP_GET_VAL (gp->gp_fr)),
			   
			   scm_cons(SCM_EOL, gp_paths)));
  
  return SCM_BOOL_T;
}
#undef FUNC_NAME



SCM_DEFINE(gp_gp, "gp?", 1, 0, 0, (SCM scm), "")
#define FUNC_NAME s_gp_gp
{
  return GP(scm) ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME


static inline SCM ggp_set(SCM var, SCM val, SCM s)
{
  SCM *id,l,ll,ret,ggp,fr;
  struct gp_stack *gp;
  UNPACK_ALL(fr,ll,l,ggp,gp,s,"failed to unpack s in ggp_set");

  if(GP(var))
    {
      id = GP_GETREF(var);

      id = gp_lookup2(id,l);
      
      if(GP_STAR(id))
        {
          if(GP(val))
            {
              ret = gp_set_ref(id,GP_UNREF(gp_gp_lookup(val,s)),l,gp);
              PACK_ALL(fr, ll, l, ret, ggp, s);
              return s;
            }
          else
            {
              ret = gp_set_val(id,val, l, gp);
              PACK_ALL(fr, ll, l, ret, ggp, s);
              return s;
            }
        }
    }    
  scm_misc_error("gp-set!","wrong type of the variable to set",SCM_EOL);
  return SCM_BOOL_F;
}

static inline SCM ggp_set2(SCM var, SCM val, SCM s)
{
  SCM *id,l,lnew,ll;
  struct gp_stack *gp;
  UNPACK_S(ll,l,gp,s,"cannot unpack s in ggp_set2");

  if(GP(var))
    {
      id = GP_GETREF(var);

      if(GP_STAR(id))
        {
          if(GP(val))
            {
              lnew = gp_set_ref(id,GP_UNREF(gp_gp_lookup(val,s)),l,gp);
            }
          else
            {
              lnew = gp_set_val(id, val, l, gp);
            }
          PACK_ALL(0,ll,l,lnew,gp,s);
          return s;	  
        }
    }    
  scm_misc_error("gp-set!","wrong type of the variable to set",SCM_EOL);
  return SCM_BOOL_F;
}

int _gp_newframe(SCM **spp, int nargs, SCM *cl, SCM *max)
{
  SCM *sp = *spp;
  if(nargs != 1)
    scm_misc_error("gp-newframe","wrong number of arguments", SCM_EOL);
  
  sp[-1] = gp_newframe(sp[0]);
   
  *spp = sp - 1;
  return -1;
}

SCM_DEFINE (gp_gp_newframe, "gp-newframe",1,0,0,(SCM s),
	    "Created a prolog frame to backtrack from")
#define FUNC_NAME s_gp_gp_newframe
{
  return gp_newframe(s);
}
#undef FUNC_NAME


SCM_DEFINE (gp_gp_newframe_choice, "gp-newframe-choice",1,0,0,(SCM s),
	    "Created a prolog frame to backtrack from")
#define FUNC_NAME s_gp_gp_newframe
{
  return gp_newframe_choice(s);
}
#undef FUNC_NAME

SCM_DEFINE(gp_set, "gp-set!", 3, 0, 0, (SCM var, SCM val, SCM s), 
	   "set gp var var to val")
#define FUNC_NAME s_gp_set
{
  return ggp_set(var,val,s);
}
#undef FUNC_NAME

SCM_DEFINE(gp_set2, "gp-set-1!", 3, 0, 0, (SCM var, SCM val, SCM s), 
	   "set gp var var to val")
#define FUNC_NAME s_gp_set2
{
  return ggp_set2(var,val,s);
}
#undef FUNC_NAME


SCM_DEFINE(gp_print, "gp-print", 1, 0, 0, (SCM pr), 
	   "print a val")
#define FUNC_NAME s_gp_print
{
  if(GP(pr))
    printf("GP_SCM> %p,%p\n",
	   (void *) SCM_UNPACK(*GP_GETREF(pr)),
	   (void *) SCM_UNPACK(*(GP_GETREF(pr) + 1)));
  else
    printf("NO GP!\n");

  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_print_stack, "gp-print-stack", 2, 0, 0, (SCM s, SCM allp), 
	   "print info about supplied gp stack")
#define FUNC_NAME s_gp_print
{
  SCM *i;
  struct gp_stack *gp = get_gp();
  printf("\nfr: %ld\nci: %ld\nsi: %ld\ncs: %ld\nlogical: %d\n"
         ,gp->gp_fr - gp->gp_frstack
         ,gp->gp_ci - gp->gp_cstack
	 ,gp->gp_si - gp->gp_stack
         ,gp->gp_cs - gp->gp_cons_stack
         ,gp->_logical_);  
  for(i = gp->gp_frstack; i < gp->gp_fr; i++)
    {

	if(scm_is_true(allp))   
	  {
	    format2("<fr:~a> = ~a~%",scm_from_ulong(i - gp->gp_frstack),*i);
	  }
	else
	  printf("i=%ld fr=%lx\n",i - gp->gp_frstack,SCM_UNPACK(*i));
    }
  for(i = gp->gp_cstack; i < gp->gp_ci; i++)
    {

      if(scm_is_true(allp))   
	{
	  format2("<ci:~a> = ~a~%",scm_from_ulong(i - gp->gp_cstack),*i);
	}
      else
	printf("i=%ld ci=%lx\n",i - gp->gp_cstack,SCM_UNPACK(*i));
    }

  for(i = gp->gp_stack; i < gp->gp_si; i++)
    {
      if(scm_is_true(allp))
	{
	  format1("<si> = ~a~%",*i);
	}
      else
	printf("%ld v %lx\n",i - gp->gp_stack,SCM_UNPACK(*i));
	
    }
  for(i = gp->gp_cons_stack; i < gp->gp_cs; i++)
    {
      if(scm_is_true(allp))    
	{ 
	  format1("<cs> = ~a~%",*i);
	}
      else
	printf("%ld cons %lx\n",i - gp->gp_cons_stack,SCM_UNPACK(*i));
    }
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_ref_set, "gp-ref-set!", 3, 0, 0, (SCM var, SCM val, SCM s), 
	   "set gp var reference to val")
#define FUNC_NAME s_gp_ref_set
{
  SCM *id,l,ll,lnew;
  struct gp_stack *gp;
  if(GP(var))
    {
      if(GP(val) || SCM_VARIABLEP(val))
	{
          UNPACK_S(ll,l,gp,s,"cannot unpack s in gp_ref_set");
	  lnew = gp_set_ref(GP_GETREF(var),val,l,gp);
	}
      else
	{
          UNPACK_S(ll,l,gp,s,"cannot unpack s in gp_ref_set");
	  id = gp_lookup(GP_GETREF(var), l);
	  lnew = gp_set_val(id,val,l,gp);
	}
      PACK_ALL(0,ll,l,lnew,gp,s);
      
      return s;      
    }
  scm_misc_error("gp-ref-set!", "wrong type to set", SCM_EOL);
  return SCM_BOOL_F;
}
#undef FUNC_NAME

 SCM_DEFINE(gp_ref_attr_set, "gp-ref-attr-set", 3, 0, 0, (SCM var, SCM val, SCM s, SCM bits), 
	   "set gp var reference to val")
#define FUNC_NAME s_gp_ref_set
{
  SCM *id,l,ll,lnew;
  struct gp_stack *gp;
  if(GP(var))
    {
      if(GP(val))
	{
          UNPACK_S(ll,l,gp,s,"cannot unpack s in gp_ref_set");
	  lnew = gp_set_attr_ref(GP_GETREF(var),val,l,gp, SCM_UNPACK(bits));
	}
      else
	{
          UNPACK_S(ll,l,gp,s,"cannot unpack s in gp_ref_set");
	  id = gp_lookup(GP_GETREF(var), l);
	  lnew = gp_set_attr_val(id,val,l,gp, SCM_UNPACK(bits));
	}

      PACK_ALL(0,ll,l,lnew,gp,s);

      return s;      
    }
  scm_misc_error("gp-ref-set!", "wrong type to set", SCM_EOL);
  return SCM_BOOL_F;
}
#undef FUNC_NAME

 SCM_DEFINE(gp_ref_attr_set_force, "gp-ref-attr-set!", 3, 0, 0, (SCM var, SCM val, SCM s, SCM bits), 
	   "set gp var reference to val")
#define FUNC_NAME s_gp_ref_set
{
  SCM l,ll;
  struct gp_stack *gp;
  if(GP(var))
    {
      if(GP(val) || SCM_VARIABLEP(val))
	{
          UNPACK_S(ll,l,gp,s,"cannot unpack s in gp_ref_set");
	  gp_set_attr_ref_force(GP_GETREF(var),val,l,gp, SCM_UNPACK(bits));
	}
      else
	{
          scm_misc_error("gp-ref-attr-set!","Not a variable to do a ref"
			 ,SCM_EOL);
	}


      return s;
    }
  scm_misc_error("gp-ref-set!", "wrong type to set", SCM_EOL);
  return SCM_BOOL_F;
}
#undef FUNC_NAME


/* new api so that pure ffi will work */
SCM_DEFINE(gp_cons_bang, "gp-cons!", 3, 0, 0, (SCM car, SCM cdr, SCM s), 
	   "crates a prolog variable cons pair")
#define FUNC_NAME s_gp_cons_bang
{
  SCM *cons;
  SCM *id = (SCM*)0;
  struct gp_stack *gp = get_gp();
   
  if(gp->_logical_) return scm_cons(car,cdr);

  gp_debus0("gp-cons>\n");
  cons = GP_GETREF(gp_mk_cons(s));
  DS(smob2scm(car, s));
  DS(smob2scm(cdr, s));

  if(GP(car) || SCM_VARIABLEP(car))    
    { 
      id = GP_GETREF(gp_gp_lookup(car, s));
      //gp_set_ref(GP_CAR(cons),GP_UNREF(id), SCM_EOL, gp);
      gp_set_ref_force(GP_CAR(cons),GP_UNREF(id));
    }
  else
    {
      gp_debus0("atom car>\n");
      //gp_set_val(GP_CAR(cons),car, SCM_EOL, gp);
      gp_set_val_force(GP_CAR(cons),car);
    }

  if(GP(cdr) || SCM_VARIABLEP(cdr))    
    { 
      id = GP_GETREF(gp_gp_lookup(cdr, s));
      //gp_set_ref(GP_CDR(cons),GP_UNREF(id), SCM_EOL, gp);
      gp_set_ref_force(GP_CDR(cons),GP_UNREF(id));
    }
  else
    {
      gp_debus0("atom cdr>\n");
      //gp_set_val_force(GP_CDR(cons),cdr, SCM_EOL, gp);
      gp_set_val_force(GP_CDR(cons),cdr);
    }

  return GP_UNREF(cons);
}
#undef FUNC_NAME


int _gp_pair_bang(SCM **spp, int nargs, SCM *cl, SCM *max)
{
  SCM *sp = *spp;
  if(nargs != 2)
    scm_misc_error("gp-unwind","wrong number of arguments", SCM_EOL);
  
  sp[-2] = gp_pair_bang(sp[-1],sp[0]);
 
  *spp = sp - 2;
  return -1;
}


SCM_DEFINE(gp_pair_bang, "gp-pair!?", 2, 0, 0, (SCM x, SCM s), 
	   "checks for a prolog or scheme pair and if a prolog variable creates a cons")
#define FUNC_NAME s_gp_pair_bang
{  
  SCM * y,l,ll,ret,ggp,fr;
  struct gp_stack *gp;

  UNPACK_ALL(fr,ll, l,ggp,gp,s,"failed to unpack s in gp_pair_bang");
  gp_debus0("gp-pair!?>\n");
 retry:
  if(GP(x) || SCM_VARIABLEP(x))
    x = gp_gp_lookup(x,s);
  if(GP(x))
    {
      if(SCM_UNLIKELY(gp->_logical_))
        {
          SCM cons = scm_cons(make_logical(),make_logical());
          SCM lnew = logical_add2(x, cons, l);
          PACK_ALL(fr,ll,l,lnew,ggp,s);
          return s;
        }
      else
        {
          y = GP_GETREF(x);
          if(GP_UNBOUND(y) && !GP_ATTR(y))
            {
              SCM *cons = GP_GETREF(gp_mk_cons(s));
	      gp_debus0("gp-pair!?> unbd mk cons\n");
              ret = gp_set_ref(y,GP_UNREF(cons),l,gp);
              PACK_ALL(fr,ll,l,ret,ggp,s);
              return s;
            }
          else
            {
              if(GP_CONS(y) && !GP_ATTR(y))
                {
                  return s;
                }
            }      
        }
      
      return SCM_BOOL_F;
    }

  if(SCM_STRUCTP(x) && GP_NAMESPACE_P(x))
    {
      x = gp_struct_ref(x,0);
      goto retry;
    }

  return SCM_CONSP(x) ? s : SCM_BOOL_F;
}
#undef FUNC_NAME


int _gp_pair(SCM **spp, int nargs, SCM *cl, SCM *max)
{
  SCM *sp = *spp;
  if(nargs != 2)
    scm_misc_error("gp-pair?","wrong number of arguments", SCM_EOL);
  
  sp[-2] = gp_pair(sp[-1],sp[0]);
 
  *spp = sp - 2;
  return -1;
}

SCM_DEFINE(gp_consp, "gp-cons?", 1, 0, 0, (SCM x), "")
{
  if(GP_CONSP(x))
    return SCM_BOOL_T;
  else
    return SCM_BOOL_F;
}

SCM_DEFINE(gp_pair, "gp-pair?", 2, 0, 0, (SCM x, SCM s), 
	   "checks for a prolog pair or scheme pair")
#define FUNC_NAME s_gp_pair
{
  gp_debus0("gp-pair?>\n");

 retry:
  if(GP(x) || SCM_VARIABLEP(x))
    x = gp_gp_lookup(x,s);

  if(GP(x))
    {
      DB(printf("pair gp-addr %x, val %x\n",SCM_UNPACK(x),
                SCM_UNPACK(*GP_GETREF(x))));
      if(GP_CONS(GP_GETREF(x)) && !GP_ATTR(GP_GETREF(x)))
	{
	  DB(printf("return> s#f\n"));
	  return s;
	}
      DB(printf("return> #f\n"));
      return SCM_BOOL_F;
    }

  DB(printf("struct>\n"));
  if(SCM_STRUCTP(x) && GP_NAMESPACE_P(x))
    {
      x = gp_struct_ref(x,0);
      goto retry;
    }

  DB(printf("last>\n"));
  return SCM_CONSP(x) ? s : SCM_BOOL_F;
}
#undef FUNC_NAME

int _gp_null(SCM **spp, int nargs, SCM *cl, SCM *max)
{
  SCM *sp = *spp;
  if(nargs != 2)
    scm_misc_error("gp-null?","wrong number of arguments", SCM_EOL);
  
  sp[-2] = gp_null(sp[-1],sp[0]);
 
  *spp = sp - 2;
  return -1;
}

SCM_DEFINE(gp_null, "gp-null?", 2, 0, 0, (SCM x, SCM s), 
	   "checks for a prolog pair or scheme pair")
#define FUNC_NAME s_gp_null
{
  gp_debus0("gp-null?>\n");

 retry:
  if(GP(x))
    x = gp_gp_lookup(x,s);

  if(GP(x))
    {
      SCM *id = GP_GETREF(x);
      if(GP_VAL(id))
	{
	  return SCM_NULLP(GP_SCM(id)) ? s : SCM_BOOL_F;
	}
      return SCM_BOOL_F;
    }

  if(SCM_STRUCTP(x) && GP_NAMESPACE_P(x))
    {
      x = gp_struct_ref(x,0);
      goto retry;
    }

  return SCM_NULLP(x) ? s : SCM_BOOL_F;
}
#undef FUNC_NAME

int _gp_null_bang(SCM **spp, int nargs, SCM *cl, SCM *max)
{
  SCM *sp = *spp;
  if(nargs != 2)
    scm_misc_error("gp_null!?","wrong number of arguments", SCM_EOL);
  
  sp[-2] = gp_null_bang(sp[-1],sp[0]);
 
  *spp = sp - 2;
  return -1;
}

SCM_DEFINE(gp_null_bang, "gp-null!?", 2, 0, 0, (SCM x, SCM s), 
	   "checks for a prolog pair or scheme pair")
#define FUNC_NAME s_gp_null_bang
{
  SCM * y,l,ll,ggp,ret,fr;
  struct gp_stack *gp;

  gp_debus0("gp-null!?>\n");

  UNPACK_ALL(fr,ll,l,ggp,gp,s,"failed to unpack s in gp_null_bang");


 retry:
  if(GP(x) || SCM_VARIABLEP(x))
    x = gp_gp_lookup(x,s);

  if(GP(x))
    {
      y = GP_GETREF(x);
      if (GP_UNBOUND(y))
	{
	  ret = gp_set_eq(y,SCM_EOL,l,gp);
          PACK_ALL(fr,ll,l,ret,ggp,s);
	  return s;
	}
      else 
	if(GP_VAL(y))
	  {
	    return SCM_NULLP(GP_SCM(y)) ? s : SCM_BOOL_F;
	  }
      return SCM_BOOL_F;
    }
  
  if(SCM_STRUCTP(x) && GP_NAMESPACE_P(x))
    {
      x = gp_struct_ref(x,0);
      goto retry;
    }

  return SCM_NULLP(x) ? s : SCM_BOOL_F;
}
#undef FUNC_NAME

int _gp_lookup(SCM **spp, int nargs, SCM *cl, SCM *max)
{
  SCM *sp = *spp;
  if(nargs != 2)
    scm_misc_error("gp-lookup","wrong number of arguments", SCM_EOL);
  
  sp[-2] = gp_gp_lookup(sp[-1],sp[0]);
 
  *spp = sp - 2;
  return -1;
}


SCM_DEFINE(gp_gp_lookup, "gp-lookup", 2, 0, 0, (SCM x, SCM s), 
	   "lookup a  chain fro a prolog variable")
#define FUNC_NAME s_gp_gp_null_bang
{
  SCM * id,l;

  UNPACK_S0(l,s,"failed to unpack s in gp_gp_lookup");

  if(GP(x) || SCM_VARIABLEP(x))
    {
      //printf("lookup> gp\n");
      gp_debug0("gp-lookup\n");
      id = gp_lookup(GP_GETREF(x),l);
      if(!GP_STAR(id)) return GP_UNREF(id);
      if(GP_UNBOUND(id) || GP_CONS(id) || GP_ATTR(id))
	return GP_UNREF(id);
      else
	return GP_SCM(id);
    }
  //printf("lookup> scm\n");
  return x;
}
#undef FUNC_NAME

int _gp_m_unify(SCM **spp, int nargs, SCM *cl, SCM *max)
{
  SCM *sp, s,v1,v2,*vv1, *vv2, ret,ll, l[3], ggp, fr;
  struct gp_stack *gp;
  
  sp = *spp;
  s  = sp[0];
  v2 = sp[-1];
  v1 = sp[-2];

  if(SCM_UNLIKELY(nargs != 3))
    scm_misc_error("gp-m-unify","wrong number of arguments", SCM_EOL);
  
  UNPACK_ALL(fr,ll,l[0],ggp,gp,s,"failed to unpack in gp_gp_unify");
  if(vlist_p(l[0]))
    {
      l[1] = GP_UNREF((SCM_I_VECTOR_WELTS(S(l[0],0))));
      l[2] = SCM_PACK(my_scm_to_int(S(l[0],1)));
    }

  gp_debus0("gp-unify!>\n");
  vv1 = UN_GP(v1);
  vv2 = UN_GP(v2);
  ret = gp_unify(vv1,vv2,1,0,l,gp,fr);
  gp_debus0("/gp-unify!>\n");
  if(ret)
    {
      PACK_ALL(fr,ll,l[0],ret,ggp,s);
      sp[-3] = s;
      *spp = sp - 3;
      return -1;
    }
  
  sp[-3] = SCM_BOOL_F;
  *spp = sp - 3;
  return -1;  
}

SCM_DEFINE(gp_m_unify, "gp-m-unify!", 3, 0, 0, (SCM x, SCM y, SCM s), 
	   "checks for a prolog pair or scheme pair")
#define FUNC_NAME s_gp_m_unify
{
  SCM ret, ll, l[3], ggp, fr;
  struct gp_stack *gp;
  //printf("gp-unify-m!>\n");
  UNPACK_ALL(fr,ll,l[0],ggp,gp,s,"failed to unpack in gp_m_unify");
  if(vlist_p(l[0]))
    {
      l[1] = GP_UNREF((SCM_I_VECTOR_WELTS(S(l[0],0))));
      l[2] = SCM_PACK(my_scm_to_int(S(l[0],1)));
    }

  //Todo this is a ugly hack.
  ret = gp_unify(GP_GETREF(x),GP_GETREF(y),1,0,l,gp,fr);
  
  if(ret)
    {
      PACK_ALL(fr,ll,l[0],ret,ggp,s);
      return s;
    }
  return SCM_BOOL_F;
}
#undef FUNC_NAME


int _gp_car(SCM **spp, int nargs, SCM *cl, SCM *max)
{
  SCM *sp = *spp;
  if(nargs != 2)
    scm_misc_error("gp-car","wrong number of arguments", SCM_EOL);
  
  sp[-2] = gp_car(sp[-1],sp[0]);
 
  *spp = sp - 2;
  return -1;
}

SCM_DEFINE(gp_car, "gp-car", 2, 0, 0, (SCM x, SCM s), 
	   "takes car a prolog pair or scheme pair")
#define FUNC_NAME s_gp_car
{
  gp_debus0("gp-car?>\n");
  if(GP(x) || SCM_VARIABLEP(x))
    x = gp_gp_lookup(x,s);

  if(GP(x))
    {
      return GP_UNREF(GP_CAR(GP_GETREF(x)));
    }
  else
    {
      return SCM_CAR(x);
    }
}
#undef FUNC_NAME

int _gp_cdr(SCM **spp, int nargs, SCM *cl, SCM *max)
{
  SCM *sp = *spp;
  if(nargs != 2)
    scm_misc_error("gp-cdr","wrong number of arguments", SCM_EOL);
  
  sp[-2] = gp_gp_cdr(sp[-1],sp[0]);
 
  *spp = sp - 2;
  return -1;
}

SCM_DEFINE(gp_gp_cdr, "gp-cdr", 2, 0, 0, (SCM x, SCM s), 
	   "takes cdr a prolog pair or scheme pair")
#define FUNC_NAME s_gp_gp_cdr
{
  gp_debus0("gp-cdr>\n");
  if(GP(x) || SCM_VARIABLEP(x))
    {
      x = gp_gp_lookup(x,s);
    }
  if(GP(x))
    {
      return GP_UNREF(GP_CDR(GP_GETREF(x)));
    }
  else
    {
      if(SCM_CONSP(x))
        return SCM_CDR(x);
      else
        scm_misc_error("gp-cdr","cdr of non cons/gp-cons ~a",scm_list_1(x));
    }
}
#undef FUNC_NAME

SCM_DEFINE(gp_var_number, "gp-var-number", 2 , 0, 0, (SCM x, SCM s), 
	   "calculates var id number")
#define FUNC_NAME s_gp_gp_cdr
{
  struct gp_stack *gp = get_gp();

  if(GP(x))
    {
      SCM i,sid;
      scm_t_bits n = (scm_t_bits) GP_GETREF(x);
      if(n >= (scm_t_bits) gp->gp_stack && n <= (scm_t_bits) gp->gp_nns)
	{
	  i   = SCM_I_MAKINUM((n - (scm_t_bits) gp->gp_stack)/16);
	}
      else
	i = SCM_I_MAKINUM(((unsigned long) GP_GETREF(x))/16);
      
      sid  = SCM_I_MAKINUM(GP_ID(*GP_GETREF(x)));

      return scm_cons(sid,i);
    }
  else
    return scm_cons(SCM_I_MAKINUM(-1),SCM_I_MAKINUM(-1));
}
#undef FUNC_NAME

SCM gp_save_mark_sym;

#include "unify-undo-redo.c"

SCM unify_env_smob;
scm_t_bits unify_env_smob_t;

SCM this_module;
static int gp_printer(SCM x, SCM port, scm_print_state *spec)
{
  scm_call_2 (scm_variable_ref  
	      (scm_c_module_lookup 
	       (this_module, "gp-printer")), 
	      port, x);
  return 0;
}

SCM_DEFINE(gp_soft_init, "gp-module-init", 0, 0, 0, (), 
	   "makes sure to record current module")
#define FUNC_NAME s_gp_soft_init
{

  this_module = scm_current_module ();
 
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_make_nvariable, "gp-make-nvar", 1, 0, 0, (SCM id), 
	   "makes a gp n variable")
#define FUNC_NAME gp_make_nvariable
{
  return gp_make_variable_id(scm_to_int(id));
}
#undef FUNC_NAME

SCM_DEFINE(gp_make_fluid, "gp-make-var", 0, 0, 0, (), 
	   "makes a gp fluid variable")
#define FUNC_NAME s_gp_make_fluid
{
  gp_debug0("gp-make-var\n");
  SCM ret,l=SCM_BOOL_F;
  struct gp_stack *gp = get_gp();
  int old = gp->_logical_;
  gp->_logical_ = 0;
  ret = gp_make_variable();

  //printf("%p %p\n", (SCM_UNPACK(*(GP_GETREF(ret)))), gp_type);fflush(stdout);
  gp_set_unbound_bang(GP_GETREF(ret), l, gp);
  gp->_logical_ = old;
  gp_debug0("return from gp-make-var\n");
  return ret;
}
#undef FUNC_NAME


SCM_DEFINE(gp_fluid_force_bang, "gp-var-set!", 3, 0, 0, 
	   (SCM f, SCM v, SCM s), 
	   "set! a gp fluid variable")
#define FUNC_NAME s_gp_fluid_set_bang
{
  SCM *id, l,ll;
  struct gp_stack *gp;
  
  UNPACK_S(ll,l,gp,s,"failed to unpack s in gp_fluid_force_bang ~a~%");
  int old = gp->_logical_;
  gp->_logical_ = 0;

  if(!GP(f))
    {
      gp->_logical_ = old;
      scm_misc_error ("gp fluid error", "variable is not a fluid, ~a", 
                      scm_list_1 (f));
    }
  
  id = gp_lookup2(GP_GETREF(f),l);
  if(GP(v))
    gp_set_ref_bang(id,v,l,gp);
  else
    gp_set_val_bang(id,v,l,gp);
  
  gp->_logical_ = old;
  gp_format2("new var ~a from val ~a ~%",f,v);
  gp_debug0("returning from gp-var-set!\n");
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_fluid_set_bang, "gp-var-set", 3, 0, 0, (SCM f, SCM v, SCM s), 
	   "set! a gp fluid variable in a backtracked way")
#define FUNC_NAME s_gp_fluid_set_bang
{
  SCM *id, l, ll;
  struct gp_stack *gp;
  
  UNPACK_S(ll,l,gp,s,"failed to unpack s in gp_fluid_set_bang ~a");
  int old = gp->_logical_;
  gp->_logical_ = 0;

  if(!GP(f))
    {
      gp->_logical_ = old;
      scm_misc_error ("gp fluid error", "variable is not a fluid, ~a", 
                      scm_list_1 (f));
    }
  
  id = GP_GETREF(f);
  if(GP(v))
    gp_set_ref(id,v,l,gp);
  else
    gp_set_val(id,v,l,gp);
  
  gp->_logical_ = old;
  gp_debug0("returning from gp-var-set\n");
  return s;
}
#undef FUNC_NAME

SCM_DEFINE(gp_dynwind, "gp-dynwind", 3, 0, 0, (SCM in, SCM out, SCM s), 
	   "ad a dynwind to the action stack")
#define FUNC_NAME s_gp_dynwind
{
  gp_debug0("dynwind...");  
  struct gp_stack *gp = get_gp();
  
  if(scm_is_false( scm_procedure_p(in))
     || !(scm_is_true(scm_procedure_p(out)) || scm_is_false(out)))
    {
      scm_misc_error("gp-dynwind error",
        "Wrong type of argument (in, out, s)) in gp-dynwind got ~a,~a,s", 
	scm_list_2(in,out));
    }

  GP_TEST_CSTACK;
  gp_gc_inc(gp);

  gp->gp_ci[0] = scm_cons(in,out);
  gp->gp_ci ++;
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_prompt, "gp-prompt", 2, 0, 0, (SCM tag, SCM lam), 
	   "Add a prompt (fkn . tag) pair in the stack")
#define FUNC_NAME s_gp_prompt
{
  struct gp_stack *gp = get_gp();
  if (scm_is_false(tag) 
      || scm_is_true(scm_procedure_p(tag))
      || scm_is_false(scm_procedure_p(lam)))
    {
      scm_misc_error("gp-prompt error",
      "Wrong type of argument (tag, lam) in gp_prompt got ~a,~a",
      scm_list_2(tag,lam));
    }

  gp->gp_ci[0] = scm_cons(lam,tag);
  gp->gp_ci ++;

  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_handlers_ref, "gp-handlers-ref", 0, 0, 0, (), 
	   "Get handlers reference")
#define FUNC_NAME s_gp_handlers_ref
{
  struct gp_stack *gp = get_gp();
  
  return gp->handlers;
}
#undef FUNC_NAME

SCM_DEFINE(gp_handlers_set_x, "gp-handlers-set!", 1, 0, 0, (SCM h), 
	   "Get handlers reference")
#define FUNC_NAME s_gp_handlers_set_x
{
  struct gp_stack *gp = get_gp();
  gp->handlers = h;
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

/*
SCM_DEFINE(gp_copy,"gp-copy",1,0,0, (SCM x),
	   "make a fresh copy of a gp structure")
#define FUNC_NAME s_gp_copy
{
  if(scm_is_true(gp_pair(x)))
    return gp_cons_bang(gp_copy(gp_car(x)),gp_copy(gp_gp_cdr(x)));
  return gp_gp_lookup(x);
}
#undef FUNC_NAME
*/

int _gp_pair_plus(SCM **spp, int nargs, SCM *cl, SCM *max)
{
  SCM *sp = *spp, ret, s, x;
  if(nargs != 2)
    scm_misc_error("gp-pair+","wrong number of arguments", SCM_EOL);

  x = sp[-1];
  s = sp[0];
  
  ret = gp_pair_bang(x,s);

  if(scm_is_false(ret)) 
    {
      sp[-2] = SCM_BOOL_F;
      sp[-1] = SCM_BOOL_F;
    }
  else
    {
      sp[-2] = gp_car(x,ret);
      sp[-1] = gp_gp_cdr(x,ret);
    }

  sp[0] = ret;

  return -3;
}

int _gp_pair_minus(SCM **spp, int nargs, SCM *cl, SCM *max)
{
  SCM *sp = *spp, ret, s, x;
  if(nargs != 2)
    scm_misc_error("gp-pair-","wrong number of arguments", SCM_EOL);

  x = sp[-1];
  s = sp[0];
  
  ret = gp_pair(x,s);

  if(scm_is_false(ret)) 
    {
      sp[-2] = SCM_BOOL_F;
      sp[-1] = SCM_BOOL_F;
    }
  else
    {
      sp[-2] = gp_car(x,ret);
      sp[-1] = gp_gp_cdr(x,ret);
    }

  sp[0] = ret;

  return -3;
}

int _gp_pair_star(SCM **spp, int nargs, SCM *cl, SCM *max)
{
  SCM *sp = *spp, s, x;
  if(nargs != 2)
    scm_misc_error("gp-pair*","wrong number of arguments", SCM_EOL);

  x = sp[-1];
  s = sp[0];
  
  if(!SCM_CONSP(x)) 
    {
      sp[-2] = SCM_BOOL_F;
      sp[-1] = SCM_BOOL_F;
      sp[ 0] = SCM_BOOL_F;
    }
  else
    {
      sp[-2] = SCM_CAR(x);
      sp[-1] = SCM_CDR(x);
      sp[ 0] = s;
    }
  
  return -3;
}

//#include "util.c"


SCM* gp_lookup_l_1(SCM *x, SCM l) 
{
  if(SCM_NULLP(l))
    {
    retry:
      if(GP_STAR(x))
	if(!GP_UNBOUND(x))
	  return GP_GETREF(x[1]);
      return x;
    }
  else
    {
      if(SCM_CONSP(l))
	{
	  for(;SCM_CONSP(l);l=SCM_CDR(l))
	    {
	      if(scm_is_eq(SCM_CAAR(l), GP_UNREF(x)))
		return GP_GETREF(SCM_CDAR(l));
	    }
	  goto retry;
	}
      else
      {
	SCM v = vhash_assq_unify(GP_UNREF(x),l);
  
	if(scm_is_eq(v, SCM_UNSPECIFIED))
	  goto retry;
	
	return GP_GETREF(v);
      }
    }
  
  return x;
}

SCM* gp_lookup_ll_1(SCM *x, SCM *l)
{
  return gp_lookup_l_1(x, l[0]);
}

SCM_DEFINE(gp_lookup_1, "gp-lookup-1", 2, 0, 0, (SCM x, SCM s), 
	   "dives down in a gp variable one time")
#define FUNC_NAME s_gp_lookup_1
{
  SCM *ref = GP_GETREF(x);
  SCM l;
  UNPACK_S0(l,s,"cannot unpack s in gp_ref_set");

  return GP_UNREF(gp_lookup_l_1(ref, l));
}
#undef FUNC_NAME
SCM_DEFINE(gp_get_id_data, "gp-get-id-data", 1, 0, 0, (SCM x), 
	   "get the gp id tag")
#define FUNC_NAME s_gp_get_id_data
{
  SCM *ref = GP_GETREF(x);
  return SCM_PACK((SCM_UNPACK(ref[0])&~0x7f) | 2);
}
#undef FUNC_NAME

SCM_DEFINE(gp_code_to_int, "code-to-int", 1, 0, 0, (SCM x), 
	   "get the gp id tag")
#define FUNC_NAME s_gp_code_to_int
{
  scm_t_bits y = SCM_UNPACK(x);
  if((y & 7) == 4)
    return SCM_PACK((y&~7) | 2);
  else
    return SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_int_to_code, "int-to-code", 1, 0, 0, (SCM x), 
	   "get the gp id tag")
#define FUNC_NAME s_gp_int_to_code
{
  scm_t_bits y = SCM_UNPACK(x);
  return SCM_PACK((y&~7) | 4);
}
#undef FUNC_NAME

SCM_DEFINE(gp_get_var_var, "gp-get-var-var", 1, 0, 0, (SCM x), 
	     "take out the gp var data part")
#define FUNC_NAME s_gp_get_var_var
{
  SCM *ref = GP_GETREF(x);
  return ref[1];
}
#undef FUNC_NAME

SCM_DEFINE(gp_clobber_var, "gp-clobber-var", 3, 0, 0, (SCM var,  SCM id, SCM val), 
	     "reuse a variable and make a new one")
#define FUNC_NAME s_gp_clobber_var
{
  SCM *ref = GP_GETREF(var);
  ref[0] = SCM_PACK((SCM_UNPACK(id) & ~0x7f) | gp_type);
  ref[1] = val;
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_make_null_procedure, "gp-make-null-procedure", 2, 0, 0, (SCM n, SCM def), 
	     "reuse a variable and make a new one")
#define FUNC_NAME s_gp_make_null_procedure
{
  int i,nfree;
  scm_t_bits *x;
  scm_t_uintptr a = scm_to_uintptr_t(def);
  nfree = scm_to_int(n);
  x     = (scm_t_bits *) scm_gc_malloc (sizeof(SCM)*(nfree + 2), "program");  
  x[0] = scm_tc7_program;
  x[1] = a;
  for(i = 0; i < nfree; i++)
    {
      x[2+i] = SCM_UNPACK(SCM_UNSPECIFIED);
    }
  return GP_UNREF(x);
}
#undef FUNC_NAME

SCM_DEFINE(gp_fill_null_procedure, "gp-fill-null-procedure", 3, 0, 0, (SCM proc, SCM addr, SCM l), 
	     "reuse a variable and make a new one")
#define FUNC_NAME s_gp_fill_null_procedure
{
  scm_t_uintptr a = scm_to_uintptr_t(addr);
  int i = 0;
  SCM_SET_CELL_WORD_1 (proc, a);
  for(;SCM_CONSP(l);l = SCM_CDR(l),i++)
    {
      SCM_PROGRAM_FREE_VARIABLE_SET(proc,i,SCM_CAR(l));
    }
  return SCM_UNSPECIFIED;
}

#undef FUNC_NAME
SCM_DEFINE(gp_make_struct, "gp-make-struct", 2, 0, 0, 
	   (SCM vtable_data, SCM n), 
	   "")
#define FUNC_NAME s_gp_make_struct
{
  SCM ret;
  int i,nn = scm_to_int(n);
  ret = scm_words ((scm_t_bits)vtable_data | scm_tc3_struct, scm_to_int(n) + 2);
  SCM_SET_CELL_WORD_1 (ret, (scm_t_bits)SCM_CELL_OBJECT_LOC (ret, 2));
  for(i = 0; i < nn; i++)
    GP_GETREF(ret)[i+2] = SCM_UNSPECIFIED;
  return ret;
}
#undef FUNC_NAME

#define FUNC_NAME s_gp_set_struct
SCM_DEFINE(gp_set_struct, "gp-set-struct", 2, 0, 0, 
	   (SCM s, SCM l), 
	   "")
{
  int i;
  SCM *v = GP_GETREF(s);
  SCM vtable = SCM_CAR(l);
  l = SCM_CDR(l);
  v[0] = SCM_PACK(((scm_t_bits) SCM_STRUCT_DATA(vtable)) | scm_tc3_struct);
  v    = GP_GETREF(v[1]);
  for(i=0;SCM_CONSP(l);l=SCM_CDR(l),i++)
    {
      v[i] = SCM_CAR(l);
    }
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

#define PROGRAM_CODE(x) ((scm_t_uint32 *) (((SCM*) (x))[1]))
SCM_DEFINE(gp_custom_fkn, "gp-custom-fkn",3,0,0,
	   (SCM custom_vm_fkn, SCM a, SCM b),"")
#define FUNC_NAME s_gp_custom_fkn
{  
  SCM fkn = scm_words(scm_tc7_program | (3<<16),5);
  ((SCM*) fkn)[1] = (SCM) PROGRAM_CODE(custom_vm_fkn);
  SCM_PROGRAM_FREE_VARIABLE_SET
    (fkn,0,
     SCM_PROGRAM_FREE_VARIABLE_REF(custom_vm_fkn,0));
  SCM_PROGRAM_FREE_VARIABLE_SET(fkn, 1, a);
  SCM_PROGRAM_FREE_VARIABLE_SET(fkn, 2, b);
  
  return fkn;
}
#undef FUNC_NAME






#include "indexer/indexer.c"
#include "attributed.c"
#include "matcher.c"
#include "guile-2.2.c"

#define SCM_ASYNC_TICK_WITH_GUARD_CODE(thr, pre, post)                  \
  do                                                                    \
    {                                                                   \
      if (SCM_UNLIKELY (thr->pending_asyncs))                           \
        {                                                               \
          pre;                                                          \
          scm_async_tick ();                                            \
          post;                                                         \
        }                                                               \
    }                                                                   \
  while (0)

#define AREF(a,i) ((a)[i])

#define NOOP()

#define CACHE_SP()				\
  do						\
    {						\
      if(vp->fp != fp)				\
	{					\
	  long diff = fp - vp->fp + 1;		\
	  fp = fp - diff;			\
	  sp = sp - diff;			\
	}					\
    } while(0)

#define INTERUPT()						\
  SCM_ASYNC_TICK_WITH_GUARD_CODE (thread, NOOP(), CACHE_SP ())



SCM gp_copy_vector(SCM **vector, int nvar)
{
  SCM newvec = scm_c_make_vector(nvar,SCM_BOOL_F);
  SCM *newp  = SCM_I_VECTOR_WELTS(newvec);
  SCM *vecp  = *vector;
  int i;
  for(i = 0; i < nvar; i++)
    {
      newp[i] = vecp[i];
    }
  newp[3] = gp_get_state_token();
  *vector = newp;
  return newvec;
}

static SCM inline gp_cons_simple(SCM x, SCM y, SCM s)
{
  struct gp_stack *gp = get_gp();
  SCM *f = get_gp_cons_pure(gp);
  f[1] = x;
  f[2] = y;

  return GP_UNREF(f);
}

SCM static inline car(SCM x)
{
  return GP_GETREF(x)[1];
}

SCM static inline cdr(SCM x)
{
  return GP_GETREF(x)[2];
}

int static inline consp(SCM x)
{
  return GP_CONSP(x);
}

SCM static inline lookup(SCM x, SCM s)
{
  if(GP(x) || SCM_VARIABLEP(x))
    return gp_gp_lookup(x,s);
  else
    return x;
}

ulong static inline scm2ulong(SCM x)
{
  return SCM_UNPACK(x) >> 2;
}

int static inline scm2int(SCM x)
{
  return (int) (((long) SCM_UNPACK(x)) >> 2);
}

typedef struct gp_vm 
{
  scm_t_uint32 *ip;		/* instruction pointer */
  SCM *sp; /* stack pointer */
  SCM *fp; /* frame pointer */
} vp_t;

/*
#include "prolog-vm.c"


SCM_DEFINE(gp_pack_start,"pack-start",5,0,0, 
	   (SCM nvar,
	    SCM nstack,
	    SCM instructions,
	    SCM contants,
	    SCM tvars),
	   "")
#define FUNC_NAME s_gp_pack_start
{
  return pack__start(nvar,nstack,instructions,contants,tvars);
}
#undef FUNC_NAME


SCM_DEFINE(gp_setup_prolog_vm_env, "gp-setup-prolog-vm-env",9,0,0,
	   (SCM dls               , 
	    SCM delayers          ,
	    SCM unwind_hooks      ,
	    SCM unwind_parameters ,
	    SCM true              ,
	    SCM false             ,
	    SCM gp_not_n          ,       
	    SCM gp_is_delayed     ,
	    SCM model_lambda),"")

#define FUNC_NAME s_gp_setup_prolog_vm_env
{
  init__vm__data (dls               , 
		  delayers          ,
		  unwind_hooks      ,
		  unwind_parameters ,
		  true              ,
		  false             ,
		  gp_not_n          ,       
		  gp_is_delayed     ,
		  model_lambda);
return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

MK_CUSTOM_FKN_MODEL(vm__raw,gp_make_vm_model,"gp-make-vm-model");
*/

SCM_DEFINE(gp_is_deterministic, "gp-deterministic?", 1, 0, 0, (SCM s0)
	   , "is there no choice points after tag")
{  
  struct gp_stack *gp = get_gp();

  SCM tag = gp_car(s0, s0);
  tag = gp_car(tag, s0);
  SCM *fr_tag = gp->gp_frstack + GP_GET_SELF_TAG(tag);
  SCM *pt;
  
  int found = 0;
  for(pt = gp->gp_fr; pt > fr_tag; pt -= GP_FRAMESIZE)
    {
      if(GP_GET_CHOICE(pt))
	{
	  found = 1;
	  break;
	}
    }
  
  if(found)
    return SCM_BOOL_F;
  else
    return SCM_BOOL_T;
}

int ok(SCM x, int i)
{
  if(GP_GETREF(x)[i] != 0)
    return 1;
  else
    return 0;
}

	   
SCM_DEFINE(gp_verify_gp, "gp-ok?",1,0,0,(SCM x),"")
{
  if(GP(x))
    {
      if(GP_CONSP(x))
	{
	  if(ok(x,1) && ok(x,2))
	    return SCM_BOOL_T;
	  else
	    return SCM_BOOL_F;
	}
      if(ok(x,1))
	return SCM_BOOL_T;
      else
	return SCM_BOOL_F;
    }
  
  return SCM_BOOL_T;
}

SCM_DEFINE(gp_make_pure_cons, "gp-make-pure-cons",0,0,0,(),"")
{
  return GP_UNREF(gp_make_cons());
}

SCM_DEFINE(gp_cons_set_1_x, "gp-cons-set-1!",2,0,0,(SCM c, SCM x),"")
{
  GP_GETREF(c)[1] = x;
  return SCM_UNSPECIFIED;
}

SCM_DEFINE(gp_cons_set_2_x, "gp-cons-set-2!",2,0,0,(SCM c, SCM x),"")
{
  GP_GETREF(c)[2] = x;
  return SCM_UNSPECIFIED;
}
SCM_DEFINE(gp_cons_ref_1, "gp-cons-ref-1",1,0,0,(SCM c),"")
{
  return GP_GETREF(c)[1];
}

SCM_DEFINE(gp_cons_ref_2, "gp-cons-ref-2",1,0,0,(SCM c),"")
{
  return GP_GETREF(c)[2];
}

void gp_init()
{  
#include  "unify.x"

  throw_closed_p = scm_make_fluid_with_default(SCM_BOOL_F);

  gp_unbound_str = scm_from_locale_string ("<gp>");
  gp_save_mark_sym = scm_from_latin1_symbol("mark");
  gp_unbound_sym = scm_string_to_symbol (gp_unbound_str);
  gp_unbound_str = scm_from_locale_string ("gp-unwind-token");
  gp_unwind_fluid = scm_make_fluid();
  scm_c_define("gp-token-fluid", gp_unwind_fluid);

  unwind_hooks   = scm_make_fluid();

  gp_cons_str    = scm_from_locale_string ("<gp-cons>");
  gp_cons_sym    = scm_string_to_symbol (gp_cons_str);

  gp_type = scm_make_smob_type("unify-variable",0);

  gp_state_token 
    = scm_make_fluid_with_default( scm_make_variable( SCM_BOOL_F ) );

  scm_set_smob_print(gp_type, gp_printer);

  {
    SCM name_str     = scm_from_locale_string ("name");
    gp_name_sym = scm_string_to_symbol (name_str);
  }

  gp_current_stack = scm_make_fluid();

  gp_module_stack_init();

  vlist_init();

  gp_init_stacks();


  init_gpgc();

  init_variables();

  init_indexer();

  init_weak();

  init_matching();

  //  init_prolog_vm();  
} 


/*
  Local Variables:
  c-file-style: "gnu"
  End:
*/

