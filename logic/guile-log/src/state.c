#define gp_N2   100000
#define gp_N3   100000

struct GC_ms_entry *gp_stack_mark(SCM pt,
				  struct GC_ms_entry *ptr,
				  struct GC_ms_entry *limit);
scm_t_bits gp_stack_type;

#include "weak.c"
#include "variable.c"

inline SCM get_touched_tag(SCM x)
{
 if(SCM_CONSP(x))
    {
      SCM tag = SCM_CAR(x);
      if(SCM_UNPACK(tag) == gp_save_tag)
	{
	  return SCM_CDR(x);
	}
      else
	{
	  return SCM_CDDR(x);
	}
    }
    
 return x;
}


int is_gc_locked();


#define GP_STACKP(scm) (SCM_NIMP(scm) && SCM_SMOB_PREDICATE(gp_stack_type,scm))

#define GP_FRAMESIZE 6
#define CHOICE_BIT (1UL<<60)
#define GP_GET_HANDLERS(fr) ((fr)[-6])
#define GP_GET_DLENGTH(fr)  SCM_UNPACK(((fr)[-5]))
#define GP_GET_SELF(fr)     ((fr)[-4])
#define GP_GET_VAR(fr)      (scm_to_ulong(((fr)[-3])) & ~CHOICE_BIT)
#define GP_GET_VAL(fr)      ((fr)[-2])
#define GP_SET_SELF(fr,c)      ((fr)[-4] = (c))
#define GP_SET_HANDLERS(fr,gp) ((fr)[-6] = ((gp)->handlers))
#define GP_SET_DLENGTH(fr,gp)  ((fr)[-5] = SCM_PACK(gp->dynstack_length))
#define GP_SET_VAL(fr,c)       ((fr)[-2] = (c))

inline int GP_VAL_UNBOUND(SCM *fr)
{
  SCM  x = GP_GET_VAL(fr);

  if(scm_is_true(x))
    {
      SCM *f = GP_GETREF(x);
      if(*f)
	{
	  if(scm_is_eq(f[1],SCM_UNBOUND))
	    return 1;
	  else
	    return 0;
	}
      else
	return 1;
    }
  return 1;
}

inline ulong GP_GET_CHOICE(SCM *fr)   
{
  SCM   a = fr[-3];
  ulong x = scm_to_ulong(a);
  return (x & CHOICE_BIT);
}

inline void set_self(struct gp_stack *gp)
{
  SCM val = scm_from_uint(gp->gp_fr - gp->gp_frstack);
  SCM *f  = GP_GETREF(GP_GET_SELF(gp->gp_fr));
  f[1] = val;
}

inline ulong GP_GET_SELF_TAG(SCM tag)
{
  return scm_to_ulong(GP_GETREF(tag)[1]);
}

inline ulong GP_GET_SELF_SELF(SCM *fr)
{
  SCM* f = GP_GETREF(GP_GET_SELF(fr));
  return scm_to_ulong(f[1]);
}

inline ulong GP_GET_VAL_VAL(SCM *fr)
{
  SCM* f = GP_GETREF(GP_GET_VAL(fr));
  return scm_to_ulong(f[1]);
}

inline void GP_SET_CONS_LIVE(SCM *fr, SCM *c, struct gp_stack *gp)   
{
  SCM x = scm_from_ulong(c - gp->gp_cons_stack);
  SCM f = fr[-1];
  if(f && SCM_CONSP(f))
    {
      SCM g = SCM_CDR(f);
      if(SCM_CONSP(g))
	SCM_SETCDR(g,x);
      else
	SCM_SETCDR(f,x);
    }
  else
    fr[-1] = x;
}

inline void GP_SET_CONS(SCM *fr, SCM *c, struct gp_stack *gp)   
{
  SCM x = scm_from_ulong(c - gp->gp_cons_stack);
  fr[-1] = x;
}

inline void GP_SET_VAR(SCM *fr, SCM *c, scm_t_bits ch, struct gp_stack *gp)
{
  ulong pos = c - gp->gp_stack;

  if(ch)
    fr[-3] = (scm_from_ulong(pos | CHOICE_BIT));
  else
    fr[-3] = scm_from_ulong(pos);
}

inline ulong GP_GET_CONS(SCM *fr)
{
  return scm_to_ulong(get_touched_tag(fr[-1]));
}

#define GP_TEST_FRSTACK if(gp->gp_fr > gp->gp_nnfr - GP_FRAMESIZE) scm_out_of_range(NULL, SCM_I_MAKINUM(gp->gp_nfr))

#define GP_TEST_CSTACK if(gp->gp_ci > gp->gp_nnc) scm_out_of_range(NULL, SCM_I_MAKINUM(gp->gp_nc))

#define GP_TEST_CCSTACK if(gp->gp_cs > gp->gp_nncs) scm_out_of_range(NULL, SCM_I_MAKINUM(gp->gp_ncs))

#define GP_TEST_STACK if(gp->gp_si > gp->gp_nns) scm_out_of_range(NULL, SCM_I_MAKINUM(gp->gp_ns))

#define GET_GP(scm) ((struct gp_stack *) GP_GETREF(scm)[1])

inline struct gp_stack *get_gp()
{
  SCM gp = scm_fluid_ref(gp_current_stack);
  if(GP_STACKP(gp))
    return (struct gp_stack *) GET_GP(gp);

  
  scm_misc_error("get_gp","could not find stacks",SCM_EOL);
  return (struct gp_stack *)0;
}

inline struct gp_stack *maybe_get_gp()
{
  SCM gp = scm_fluid_ref(gp_current_stack);
  if(GP_STACKP(gp))
    return (struct gp_stack *) GET_GP(gp);
 
  return (struct gp_stack *) 0;
}

inline void init_gp_var(SCM *cand)
{
  cand[0] = SCM_PACK(GP_MK_FRAME_UNBD(gp_type));
  cand[1] = SCM_UNBOUND;
}

inline SCM *get_gp_var(struct gp_stack *gp)
{
  SCM cand;
  GP_TEST_STACK;
  cand = *(gp->gp_si);
  if(scm_is_false(cand) || scm_is_eq(cand, SCM_BOOL_T))
    {
      cand = gp_make_variable();
      gp->gp_si[0] = cand;      
    }
  gp->gp_si ++;
  init_gp_var(GP_GETREF(cand));
  return GP_GETREF(cand);
}

static inline void init_gp_cons(SCM *cand)
{
  gp_debug0("init_gp_cons\n");
  if(!GP_CONS(cand))
    {
      gp_debug0("get_gp_cons no cons\n");
      cand[0] =  SCM_PACK(GP_MK_FRAME_CONS(gp_type));
    }

  cand[1] = SCM_BOOL_F;
  cand[2] = SCM_BOOL_F;
}

static inline SCM *get_gp_cons(struct gp_stack *gp)
{
  SCM *cand;
  gp_debug0("get_gp_cons\n");
  GP_TEST_CCSTACK;
  
  SCM candi = *(gp->gp_cs);

  if(!GP(candi))
    {
      candi = gp_make_cons();
      gp->gp_cs[0] = candi;      
    }

  cand  = GP_GETREF(candi);  

  gp->gp_cs ++;
  init_gp_cons(cand);

  SCM *v1 = get_gp_var(gp);
  SCM *v2 = get_gp_var(gp);

  cand[1] = GP_UNREF(v1);
  cand[2] = GP_UNREF(v2);

  return cand;  
}

static inline SCM *get_gp_cons_pure(struct gp_stack *gp)
{
  SCM cand;
  gp_debug0("get_gp_cons\n");
  GP_TEST_CCSTACK;
  
  cand = *(gp->gp_cs);
  if(!GP(cand))
    {
      cand = gp_make_cons();
      gp->gp_cs[0] = cand;      
    }
  gp->gp_cs ++;

  init_gp_cons(GP_GETREF(cand));
  return GP_GETREF(cand);  
}

pthread_mutex_t gp_stacks_lock = PTHREAD_MUTEX_INITIALIZER;
#define LOCK_STACKS   pthread_mutex_lock(&gp_stacks_lock)
#define UNLOCK_STACKS pthread_mutex_unlock(&gp_stacks_lock)

SCM gp_stacks = SCM_BOOL_F;

static inline SCM *make_gp_stack(int id, int nthread, int nc, int ns, int ncs, int nfr, struct gp_stack **ggp)
{
  int i;

  *ggp = (struct gp_stack *) scm_gc_malloc_pointerless(sizeof(struct gp_stack),"struct gp_stack");
   
  struct gp_stack *gp = *ggp;

  if(!gp) goto error1;

  gp->gp_cstack = 
    (SCM *) scm_gc_malloc_pointerless(sizeof(SCM) * nc,"gp->gp_cstack");
  
  if(!gp->gp_cstack) goto error2;

  scm_gc_protect_object(GP_UNREF(gp->gp_cstack));
  
  gp->gp_stack = 
    (SCM *) scm_gc_malloc_pointerless(sizeof(SCM) * ns,"gp->gp_stack");

  if(!gp->gp_stack) goto error3;

  scm_gc_protect_object(GP_UNREF(gp->gp_stack));
  
  {
    SCM *pt, *end = gp->gp_stack + ns;
    for(pt = gp->gp_stack; pt < end; pt++)
      {
	*pt = SCM_BOOL_F;
      }
  }


  gp->gp_cons_stack =
    (SCM *) scm_gc_malloc_pointerless(sizeof(SCM) * ncs,"gp->gp_cons_stack");

  if(!gp->gp_cons_stack) goto error4;

  scm_gc_protect_object(GP_UNREF(gp->gp_cons_stack));

  {
    SCM *pt, *end = gp->gp_cons_stack + ncs;
    for(pt = gp->gp_cons_stack; pt < end; pt++)
      {
	*pt = SCM_BOOL_F;
      }
  }


  gp->gp_frstack = 
    (SCM *) scm_gc_malloc_pointerless(sizeof(SCM) * nfr,"gp->gp_frstack");

  if(!gp->gp_frstack) goto error5;

  scm_gc_protect_object(GP_UNREF(gp->gp_frstack));



  gp->dynstack = SCM_EOL;
  gp->dynstack_length = 2; //2 = SCM_UNPACK(scm_from_int(0));
  gp->rguards = SCM_EOL;
  gp->handlers = SCM_EOL;

  gp->gp_nfr = nfr;
  gp->gp_nc  = nc;
  gp->gp_ns  = ns;
  gp->gp_ncs = ncs;

  gp->gp_fr = gp->gp_frstack;
  gp->gp_si = gp->gp_stack;
  gp->gp_ci = gp->gp_cstack;
  gp->gp_cs = gp->gp_cons_stack;

  gp->gp_nns  = gp->gp_stack      + gp->gp_ns  - 10;
  gp->gp_nnc  = gp->gp_cstack     + gp->gp_nc  - 10;
  gp->gp_nncs = gp->gp_cons_stack + gp->gp_ncs - 10;
  gp->gp_nnfr = gp->gp_frstack    + gp->gp_nfr - 10;

  gp->_logical_ = 0;
  gp->_thread_safe_ = 1;

  gp->thread_id = nthread;
  gp->nrem = 0;
  gp->n  = 0;
  gp->id = id;
  SCM ret;


  ret = PTR2SCM (GC_generic_malloc (2 * sizeof (scm_t_cell), 
                                        gp_variable_gc_kind));
  GP_GETREF(ret)[0] = SCM_PACK(gp_stack_type);
  GP_GETREF(ret)[1] = GP_UNREF((SCM *) gp);
  
  
  for(i = 0; i < gp->gp_ncs; i++)
    {
      gp->gp_cs[i] = SCM_BOOL_F;
    }

  for(i = 0; i < gp->gp_ncs && i < 10000; i++)
    {
      gp->gp_cs[i] = gp_make_cons_id(id);
    }
  

  for(i = 0; i < gp->gp_ns; i++)
    {
      gp->gp_si[i] = SCM_BOOL_F;
    }

  for(i = 0; i < gp->gp_ns && i < 10000; i++)
    {
      SCM x = gp_make_variable_id(id);
      gp->gp_si[i] = x;      
    }

  scm_gc_unprotect_object(GP_UNREF(gp->gp_cstack));
  scm_gc_unprotect_object(GP_UNREF(gp->gp_stack));
  scm_gc_unprotect_object(GP_UNREF(gp->gp_cons_stack));
  
  //TODO, make sure to use a datastructure that can gc non used stacks
  {
    SCM *out = GP_GETREF(ret);

    {
      LOCK_STACKS;
      scm_hashq_set_x(gp_stacks, ret, SCM_BOOL_T);
      UNLOCK_STACKS;
    }
    
    return out;
  }

 error5:
  scm_gc_unprotect_object(GP_UNREF(gp->gp_cons_stack));

 error4:
  scm_gc_unprotect_object(GP_UNREF(gp->gp_stack));

 error3:
  scm_gc_unprotect_object(GP_UNREF(gp->gp_cstack));

 error2: 
 error1:
  return (SCM *)0;  
}

#define GC_MARK(x) (mark_stack_ptr = GC_MARK_AND_PUSH (SCM2PTR (x), mark_stack_ptr, mark_stack_limit, NULL))
#define GC_MARK_NT(x) (mark_stack_ptr = GC_MARK_AND_PUSH_NO_TOUCH (SCM2PTR (x), mark_stack_ptr, mark_stack_limit, NULL))
void gp_sweep_handle(SCM in);
struct GC_ms_entry *gp_stack_mark(SCM pt,
				  struct GC_ms_entry *ptr,
				  struct GC_ms_entry *limit);
void gp_clear_marks(SCM in, int isBefore);
#include "gc.c"

void gp_clean_pairs(struct gp_stack *gp)
{
  //printf("clean-pairs!\n");
  SCM *pt;
  scm_t_bits i = 1;

  for(pt = gp->gp_cstack + 1; pt < gp->gp_ci; pt++)
    {
      if(*pt && SCM_CONSP(*pt))
	{
	  SCM v = SCM_CAR(*pt);
	  if(GP(v) && !GP_FRAME_VAR(v))
	    {
	      SCM        vv = GP_GETREF(v)[0];
	      scm_t_bits b  = SCM_UNPACK(vv);

	      b = GP_SETCOUNT(vv,(scm_t_bits) 0);
	      GP_GETREF(v)[0] = SCM_PACK(b);
	    }
	}

      if(*pt && GP(*pt) && !GP_FRAME_VAR(*pt))
	{
	  SCM        vv = GP_GETREF(*pt)[0];
	  scm_t_bits b  = SCM_UNPACK(vv);

	  b = GP_SETCOUNT(vv,(scm_t_bits) 0);
	  GP_GETREF(*pt)[0] = SCM_PACK(b);
	}
    }


  //printf("-------\n");
  SCM *pfr;
  SCM *ptu;
  pt = gp->gp_cstack + 1;
  i = 1;
  for(pfr = gp->gp_frstack + GP_FRAMESIZE ; 
      pfr <= gp->gp_fr; pfr += GP_FRAMESIZE, i++)
    {
      if(GP_VAL_UNBOUND(pfr))   continue;

      ptu =  gp->gp_cstack + GP_GET_VAL_VAL(pfr);

      if(!SCM_CONSP(*ptu) && !scm_is_eq(*ptu,GP_GET_VAL(pfr)))
	continue;

      while(pt < ptu)
	{
	  if(*pt && GP(*pt) && !GP_FRAME_VAR(*pt))
	    {
	      SCM        v  = *pt;
	      SCM        vv = GP_GETREF(v)[0];
	      scm_t_bits id = GP_COUNT(vv);
	      scm_t_bits b  = SCM_UNPACK(vv);
	      if(i > id)
		{
		  b = GP_SETCOUNT(vv,i);
		  GP_GETREF(v)[0] = SCM_PACK(b);
		}
	      else
		*pt=SCM_BOOL_F;

	      pt ++;
	      continue;
	    }

	  if(*pt && SCM_CONSP(*pt))
	    {
	      SCM v = SCM_CAR(*pt);
	      if(GP(v) && !GP_FRAME_VAR(v))
		{
		  SCM        vv = GP_GETREF(v)[0];
		  scm_t_bits id = GP_COUNT(vv);
		  scm_t_bits b  = SCM_UNPACK(vv);
		  if(i > id)
		    {
		      b = GP_SETCOUNT(vv,i);
		      GP_GETREF(v)[0] = SCM_PACK(b);
		    }
		  else
		    {		      
		      *pt=SCM_BOOL_F;
		    }
		}
	    }
	  
	  pt++;
	}
    }
  
  for(pt = gp->gp_cstack + 1; pt < gp->gp_ci; pt++)
    {
      if(*pt && SCM_CONSP(*pt))
	{
	  SCM v = SCM_CAR(*pt);
	  if(GP(v) && !GP_FRAME_VAR(v))
	    {
	      SCM        vv = GP_GETREF(v)[0];
	      scm_t_bits b  = SCM_UNPACK(vv);

	      b = GP_SETCOUNT(vv,(scm_t_bits) 0);
	      GP_GETREF(v)[0] = SCM_PACK(b);
	    }
	}

      if(*pt && GP(*pt) && !GP_FRAME_VAR(*pt))
	{
	  SCM        vv = GP_GETREF(*pt)[0];
	  scm_t_bits b  = SCM_UNPACK(vv);

	  b = GP_SETCOUNT(vv,(scm_t_bits) 0);
	  GP_GETREF(*pt)[0] = SCM_PACK(b);
	}
    }
  
  //printf("leave\n");
}

static struct GC_ms_entry *
gp_stack_mark0(SCM obj, int unlocked,
	       struct GC_ms_entry *mark_stack_ptr,
	       struct GC_ms_entry *mark_stack_limit)
 { 
   struct gp_stack *gp = GET_GP(obj);
  int i;

  //printf("mark %d\n", gp_gc_p); 
  GC_MARK(GP_UNREF((SCM *) gp));

  GC_MARK(gp->dynstack);
  GC_MARK(gp->rguards);
  GC_MARK(gp->handlers);


  GC_MARK(GP_UNREF(gp->gp_frstack));
  GC_MARK(GP_UNREF(gp->gp_cstack));
  GC_MARK(GP_UNREF(gp->gp_stack));
  GC_MARK(GP_UNREF(gp->gp_cons_stack));

  //The first frame needs to be referenced
  GC_MARK(gp->gp_cstack[0]);

  GC_MARK(gp->gp_frstack[0]);
  GC_MARK(gp->gp_frstack[1]);
  GC_MARK(gp->gp_frstack[2]);
  GC_MARK(gp->gp_frstack[3]);
  GC_MARK(gp->gp_frstack[4]);
  GC_MARK(gp->gp_frstack[5]);
  
  int nvar  = 0;
  int nxvar = 0;
  int ne    = 0;

  //printf("mark-stack!\n");
  //if(!unlocked) printf("locked mark\n");
  for(i=1;i < gp->gp_ci - gp->gp_cstack; i++)
    {
      SCM *pt = gp->gp_cstack + i;
      if(*pt)
        {          
          if(GP(*pt))
            {
              SCM val = *pt;
	      nvar++;
              if(unlocked)
                {
                  scm_t_bits head;
                  head = SCM_UNPACK(GP_GETREF(val)[0]);
                  GP_GC_CAND(head);
                  GP_GETREF(val)[0] = SCM_PACK(head);                      
		  //printf("NTMARK(%p)\n",SCM_UNPACK(val));
#ifdef HAS_GP_GC
                  GC_MARK_NT(val);		  
#else
                  GC_MARK(val);		  
#endif
                }
		else
		  GC_MARK(val);
	    }
          else if(SCM_CONSP(*pt) && GP(SCM_CAR(*pt)))
	    {
		  GC_MARK(*pt);
		  nxvar++;
#ifdef HAS_GP_GC
		  SCM x = SCM_CAR(*pt);
		  scm_t_bits head = SCM_UNPACK(GP_GETREF(x)[0]);
		  GP_GC_CAND(head);
		  GP_GETREF(x)[0] = SCM_PACK(head);
		  GC_MARK(SCM_CDR(*pt));
		  if(unlocked)
		    GC_MARK_NT(x);		  
		  else
		    GC_MARK(x);		  
		    
#else
                  GC_MARK(SCM_CAR(*pt));
                  GC_MARK(SCM_CDR(*pt));
#endif
	    }
#ifdef HAS_GP_GC_
	    else if(SCM_CONSP(*pt))
	      {
	        ne++;
		GC_MARK(*pt);
		SCM x;
	      
		SCM tag = SCM_CAR(*pt);
		if(SCM_I_INUMP(tag))
		  {
	             if(SCM_UNPACK(tag) == gp_save_tag)
		       {
	                 x = SCM_CDR(*pt);
	               }
		     else
		       {
	                 x = SCM_CDDR(*pt);
	               }
		   
		     
		     SCM xx = SCM_CAR(x);
		     if(GP(x) && !GP_FRAME_VAR(x))
		       {
			 scm_t_bits head = SCM_UNPACK(GP_GETREF(x)[0]);
			 GP_GC_CAND(head);
			 GP_GETREF(xx)[0] = SCM_PACK(head);
			 GC_MARK(SCM_CDR(x));
			 if(unlocked)
			   GC_MARK_NT(xx);
			 else
			   GC_MARK(xx);		  
		       }
		  }
              }
#endif
	    else
	      {
	      ne++;
	      GC_MARK(*pt);
	    }
        }
    }  

  SCM *pt;
  for(pt = gp->gp_fr; 
      pt > gp->gp_frstack + GP_FRAMESIZE; 
      pt -= GP_FRAMESIZE)
    {
      GC_MARK(GP_UNREF(pt[-1]));

      if(scm_is_false(pt[-1]))
	continue;

      if(SCM_CONSP(pt[-1]))
	{
	  GC_MARK(GP_GET_VAL(pt));
	  GC_MARK(GP_GET_SELF(pt));
	}
      else
	{
      SCM *f = GP_GETREF(GP_GET_SELF(pt));
      scm_t_bits head = SCM_UNPACK(f[0]);
      if(unlocked)
	{
	  GP_GC_CAND(head);
	  f[0] = SCM_PACK(head);
#ifdef HAS_GP_GC
	  GC_MARK_NT(GP_UNREF(f));
#else
	  GC_MARK(GP_UNREF(f));
#endif
	}
      else
	GC_MARK(GP_UNREF(f));
        }
    }
  
  for(i=0;i < gp->gp_si - gp->gp_stack; i++)
    {
      SCM *pt = gp->gp_stack + i;
      if(GP(*pt))
        {
          if(unlocked)
            {
              scm_t_bits head = SCM_UNPACK(GP_GETREF(*pt)[0]);
              GP_GC_CAND(head);
              GP_GETREF(*pt)[0] = SCM_PACK(head);
#ifdef HAS_GP_GC
              GC_MARK_NT(*pt);
#else
	      GC_MARK(*pt);
#endif
            }
          else
            GC_MARK(*pt);
        }
    }  

  for(;i < gp->gp_nns - gp->gp_stack; i++)
    {
      SCM *pt = gp->gp_stack + i;
      if(GP(*pt))
        {
	  GP_GETREF(*pt)[1] = SCM_UNBOUND;
          GC_MARK(*pt);
        }
    }  

  for(i=0;i < gp->gp_cs - gp->gp_cons_stack; i++)
    {
      SCM *pt = gp->gp_cons_stack + i;
      if(GP(*pt))
        {
          if(unlocked)
            {
              scm_t_bits head = SCM_UNPACK(GP_GETREF(*pt)[0]);
              GP_GC_CAND(head);
              GP_GETREF(*pt)[0] = SCM_PACK(head);
	      	     	      
#ifdef HAS_GP_GC
	      SCM car = GP_GETREF(*pt)[1];
	      SCM cdr = GP_GETREF(*pt)[2];

              GC_MARK_NT(*pt);
	      if(GP(car))
		GC_MARK_NT(car);
	      else
		GC_MARK(car);

	      if(GP(cdr))
		GC_MARK_NT(cdr);
	      else
		GC_MARK(cdr);
#else
	      GC_MARK(*pt);
#endif
	    }
	  else
	      GC_MARK(*pt);
        }
    }  
  
  for(;i < gp->gp_nncs - gp->gp_cons_stack; i++)
    {
      SCM *pt = gp->gp_cons_stack + i;
#ifdef HAS_GP_GC
      if(GP(*pt))
        {
	  SCM car = GP_GETREF(*pt)[1];
	  SCM cdr = GP_GETREF(*pt)[2];
	  if(GP(car))	    
	    GC_MARK_NT(car);
	  if(GP(cdr))	    
	    GC_MARK_NT(cdr);          
	  GC_MARK_NT(*pt);
        }
#else
      GC_MARK(*pt);
#endif
    }  

  GC_MARK(gp->dynstack);
  GC_MARK(gp->rguards);
  GC_MARK(gp->handlers);
  
  //printf("end mark nv %d xv %d e %d\n",nvar,nxvar,ne);
  return mark_stack_ptr;
}

#define QQ(x) SCM_PACK(((scm_t_bits) x)*4+2)
static int gp_stack_printer(SCM x, SCM port, scm_print_state *spec)
{
  //struct gp_stack *gp = (struct gp_stack *) SCM_SMOB_DATA(x);
  scm_simple_format(port,scm_from_locale_string("<gp-stack>"),
                    SCM_EOL);
  return 0;
}

static inline void gp_alloc_cons(struct gp_stack *gp, int n)
{
  int i;
  if(!(gp->gp_cs + n <= gp->gp_nncs)) goto error;
  if(!SCM_CONSP(gp->gp_cs[n-1]))      goto allocate;
  return;

 allocate:
  for(i=0;i < 10000 && gp->gp_cs+i < gp->gp_nncs;i++)
    {
      if(!SCM_CONSP(gp->gp_cs[i]))
        gp->gp_cs[i] = gp_make_cons();
    }
  return;

 error:
  scm_misc_error("gp_alloc_cons","cons stack is full",SCM_EOL);
  return;
}

static inline SCM gpa_cons(SCM x, SCM y, struct gp_stack *gp)
{
  SCM *r = get_gp_cons(gp);
  r[1] = x;
  r[2] = y;
  return GP_UNREF(r);
}

static inline SCM* gp_alloc_data(int n, struct gp_stack *gp)
{
  if(!(gp->gp_si + n <= gp->gp_nns)) goto error;
  
  SCM * ret = gp->gp_si;
  gp->gp_si += n;

  return ret;


 error:
  scm_misc_error("gp_alloc_data","data stack is full",SCM_EOL);
  return (SCM *) 0;
}

SCM gp_nil_fr;
SCM gp_nil_ci;

SCM_DEFINE(gp_make_stack, "gp-make-stack", 6, 0, 0, 
	   (SCM id, SCM thread_id, SCM nc, SCM ns, SCM ncs, SCM nfr), 
	   "make logical stack for id, thread_id, and sizes nc, ns, ncs, nfr")
#define FUNC_NAME s_gp_make_stack
{
  SCM s;
  struct gp_stack *gp;
  if(!SCM_I_INUMP(id))        goto error2;
  if(!SCM_I_INUMP(thread_id)) goto error2;
  if(!SCM_I_INUMP(nc))        goto error2;
  if(!SCM_I_INUMP(ns))        goto error2;
  if(!SCM_I_INUMP(ncs))       goto error2;

  int i_id        = SCM_I_INUM(id);
  int i_thread_id = SCM_I_INUM(thread_id);
  int i_nc        = SCM_I_INUM(nc);
  int i_ns        = SCM_I_INUM(ns);
  int i_ncs       = SCM_I_INUM(ncs);
  int i_nfr       = SCM_I_INUM(nfr);

  SCM* sgp =  make_gp_stack(i_id,i_thread_id, i_nc, i_ns, i_ncs, i_nfr, &gp);
  if(!sgp) goto error;
  
  gp->gp_fr[0] = gp->handlers;
  gp->gp_fr[1] = SCM_PACK(gp->dynstack_length);
  gp->gp_fr[2] = gp_nil_fr;                     // Self link
  gp->gp_fr[3] = scm_from_int(0);               // Var  stack
  gp->gp_fr[4] = gp_nil_ci;                     // val  stack
  gp->gp_fr[5] = scm_from_int(0);               // cons stack
  
  gp->gp_fr   += GP_FRAMESIZE;

  gp->gp_ci[0] = gp_nil_ci;
  gp->gp_ci   += 1;

  s = GP_UNREF(sgp);

  return s;

 error:
  scm_misc_error("gp-make-stack","could not allocate stacks",SCM_EOL);
  return SCM_UNSPECIFIED;

 error2:
  scm_misc_error("gp-make-stack","wrong input type(s)",SCM_EOL);
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME
	 
static inline int is_advanced_tag(SCM *pt);

void gp_clear_marks(SCM in, int isBefore)
{
  //printf("clear-marks!\n");
  struct gp_stack *gp  = GET_GP(in);
  
  SCM *pt;
  //printf("clear\n");
  // Search for the first newframe stored  
  for(pt = gp->gp_ci - 1; pt >= gp->gp_cstack + 1; pt--)
    {
      if(*pt && GP(*pt))
        {
          SCM val = *pt;
          scm_t_bits head = SCM_UNPACK(GP_GETREF(val)[0]);
	  if(isBefore || !(!GP_GC_ISMARKED(head) && GP_GC_ISCAND(head)))
	    {
	      GP_GC_CLEAR(head);
	      GP_GC_CLEARCAND(head);
	      GP_GETREF(val)[0] = SCM_PACK(head);
	    }
          continue;
        }
      
      if(*pt && SCM_CONSP(*pt) && GP(SCM_CAR(*pt)))
	{
	  SCM x = SCM_CAR(*pt);
	  scm_t_bits head = SCM_UNPACK(GP_GETREF(x)[0]);
	  GP_GC_CLEAR(head);
	  GP_GC_CLEARCAND(head);
	  GP_GETREF(x)[0] = SCM_PACK(head);
	}
    }
  
  for(pt = gp->gp_fr; pt > gp->gp_frstack + GP_FRAMESIZE; 
      pt -= GP_FRAMESIZE)
    {
      if(scm_is_false(pt[-2]))
	continue;

      SCM val = GP_GET_SELF(pt);
      if(val && GP(val))
	{
	  scm_t_bits head = SCM_UNPACK(GP_GETREF(val)[0]);
	  if(isBefore || !(!GP_GC_ISMARKED(head) && GP_GC_ISCAND(head)))
	    {
	      GP_GC_CLEAR(head);
	      GP_GC_CLEARCAND(head);
	      GP_GETREF(val)[0] = SCM_PACK(head);
	    }
	}
    }

  //printf("sweep1 %d %d\n",n,nrem);
  
  //printf("clear II");
  {
    for(pt = gp->gp_stack; pt < gp->gp_nns; pt++)
      {
        if(GP(*pt))
          {
            SCM *f = GP_GETREF(*pt);
            scm_t_bits head = SCM_UNPACK(f[0]);
	    if(pt >= gp->gp_si) 
	      f[1] = SCM_UNBOUND;

	    GP_GC_CLEAR(head);
	    GP_GC_CLEARCAND(head);
	    f[0] = SCM_PACK(head);
          }
      }
  }

  {
    for(pt = gp->gp_cons_stack; pt < gp->gp_nncs; pt++)
      {
        if(GP(*pt))
          {
            SCM *f = GP_GETREF(*pt);

            scm_t_bits head = SCM_UNPACK(f[0]);
	    GP_GC_CLEAR(head);
	    GP_GC_CLEARCAND(head);
	    f[0] = SCM_PACK(head);

	    SCM car = f[1];
	    SCM cdr = f[2];
	    if(GP(car))
	      {
		scm_t_bits head_car = SCM_UNPACK(GP_GETREF(car)[0]);
		GP_GC_CLEAR(head_car);
		GP_GC_CLEARCAND(head_car);
		GP_GETREF(car)[0] = SCM_PACK(head_car);
		/*if(pt > gp->gp_cs)
		  GP_GETREF(car)[1] = SCM_UNBOUND;*/
	      }

	    if(GP(cdr))
	      {
		scm_t_bits head_cdr = SCM_UNPACK(GP_GETREF(cdr)[0]);
		GP_GC_CLEAR(head_cdr);
		GP_GC_CLEARCAND(head_cdr);
		GP_GETREF(cdr)[0] = SCM_PACK(head_cdr);
		/*if(pt > gp->gp_cs)
		  GP_GETREF(cdr)[1] = SCM_UNBOUND;*/
		
	      }
	  }
      }
  }

  //printf("end clear\n");
}


void gp_sweep_handle(SCM in)
{
  struct gp_stack *gp  = GET_GP(in);
  int nrem = 0;
  int n    = 0;
  
  SCM *pt;

  //printf("sweep %d\n",gp_gc_p);

  // Search for the first newframe stored

  int nf  = 0;
  int df  = 0;
  int dff = 0;

  for(pt = gp->gp_ci - 1; pt >= gp->gp_cstack + 1; pt--)
    {
      n++;
      
      // If we find an unmarked smob then we 

      if(scm_is_false(*pt))
        {
          nrem ++;
          continue;
        }
            
      if(GP(*pt))
        {
          scm_t_bits head = SCM_UNPACK(GP_GETREF(*pt)[0]);
          if(!GP_GC_ISMARKED(head) && GP_GC_ISCAND(head))
            {
              *pt = SCM_BOOL_F;
              nrem++;
            }
          continue;
        }
      
      if(*pt && SCM_CONSP(*pt) && GP(SCM_CAR(*pt)))
	{
	  scm_t_bits head = SCM_UNPACK(GP_GETREF(SCM_CAR(*pt))[0]);
          if(!GP_GC_ISMARKED(head) && GP_GC_ISCAND(head))
            {
              *pt = SCM_BOOL_F;
              nrem++;
            }
          continue;
	}
      

    }
  
  //printf("sweep1 %d %d\n",n,nrem);
  
  {
    int vn   = 0;
    int vrem = 0;
    SCM tc =  SCM_PACK(GP_MK_FRAME_UNBD(gp_type));

    for(pt = gp->gp_fr; pt > gp->gp_frstack + GP_FRAMESIZE; pt -= GP_FRAMESIZE)
      {         

	n += GP_FRAMESIZE;
	nf++;

	if(scm_is_false(pt[-2]))
	  {
	    nrem += GP_FRAMESIZE;
	    continue;
	  }

	if(pt[-1] && !SCM_CONSP(pt[-1]))
	  {
	    SCM *f = GP_GETREF(GP_GET_SELF(pt));
	    scm_t_bits head = SCM_UNPACK(f[0]);

	    if(!GP_GC_ISMARKED(head) && GP_GC_ISCAND(head))
	      {
		nrem += GP_FRAMESIZE;
		int i;
		for(i=0;i<GP_FRAMESIZE;i++)
		  pt[-i-1] = SCM_BOOL_F;
		df++;
	      }
	  }

	if(pt[-1] && SCM_CONSP(pt[-1]))
	  {
	    continue;
	    SCM *f = GP_GETREF(GP_GET_SELF(pt));
	    scm_t_bits head = SCM_UNPACK(f[0]);

	    if(!GP_GC_ISMARKED(head) && GP_GC_ISCAND(head))
	      {
		SCM tag = SCM_CAR(pt[-1]);
	      
		if(SCM_I_INUMP(tag))
		  {
		    if(SCM_UNPACK(tag) == gp_save_tag)
		      {
			SCM_SETCDR(pt[-1], SCM_BOOL_F);
		      }
		    else
		      {
			SCM_SETCDR(SCM_CDR(pt[-1]), SCM_BOOL_F);
		      }	          
		    nrem += GP_FRAMESIZE;
		    int i;
  		    for(i=1;i<GP_FRAMESIZE;i++)
		      pt[-i-1] = SCM_BOOL_F;
		    df ++;
		    dff ++;
		  }
	      }        
	  }
      }
    //printf("sweep2 %d %d\n",n,nrem);

    for(pt = gp->gp_stack; pt < gp->gp_si; pt++)
      {
        vn++;
        if(GP(*pt))
          {
            SCM *f = GP_GETREF(*pt);
            scm_t_bits head = SCM_UNPACK(f[0]);
            if(!GP_GC_ISMARKED(head) && GP_GC_ISCAND(head))
	      {
		head = SCM_UNPACK(tc);
		GP_GC_CAND(head);
		
		f[0] = SCM_PACK(head);
		f[1] = SCM_UNBOUND;
		vrem++;
	      }
          }
      }
    if(vn > 100 && vrem*20 - vn > nrem * 20 - n)
      {
        nrem = vrem;
        n    = vn;
      }

    vn = 0;
    vrem = 0;

    for(pt = gp->gp_cons_stack; pt < gp->gp_cs; pt++)
      {
        vn++;
        if(GP(*pt))
          {
            SCM *f = GP_GETREF(*pt);
            scm_t_bits head = SCM_UNPACK(f[0]);
            if(!GP_GC_ISMARKED(head) && GP_GC_ISCAND(head))
	      {
		SCM car = f[1];
		SCM cdr = f[2];
		if(GP(car))
		  {
		    scm_t_bits head_car = SCM_UNPACK(GP_GETREF(car)[0]);
		    if(GP_GC_ISMARKED(head_car))
		      {
			GP_GC_MARK(head);
		      }
		  }
		if(GP(cdr))
		  {
		    scm_t_bits head_cdr = SCM_UNPACK(GP_GETREF(car)[0]);
		    if(GP_GC_ISMARKED(head_cdr))
		      {
			GP_GC_MARK(head);
		      }
		  }	       
		f[0] = SCM_PACK(head);
		
		if(!GP_GC_ISMARKED(head)) vrem++;
	      }
          }
      }

    if(vn > 100 && vrem*20 - vn > nrem * 20 - n)
      {
        nrem = vrem;
        n    = vn;
      }

    
    //printf("sweep2 vn1 %d vn2 %d vrem1 %d vrem %d\n",vn,vn,vrem,vrem);
  }
  
  gp_clean_pairs(gp);
  
  gp->n    = n;
  gp->nrem = nrem;

  //printf("end sweep nf: %d  delnf: %d delfn++: %d\n",nf,df, dff);
}


struct GC_ms_entry *gp_stack_mark(SCM pt,
				  struct GC_ms_entry *ptr,
				  struct GC_ms_entry *limit)
{
  struct GC_ms_entry * ret;

#ifdef HAS_GP_GC
    {
      pthread_mutex_lock(&gp_gc_lock);
      if(gp_gc_p)
	{
	  ret = gp_stack_mark0(pt,0,ptr,limit);
	  pthread_mutex_unlock(&gp_gc_lock);
	  return ret;
	}

      ret = gp_stack_mark0(pt,1,ptr,limit);
      pthread_mutex_unlock(&gp_gc_lock);
      return ret;
    }
#else
  ret = gp_stack_mark0(pt,0,ptr,limit);
  return ret;
#endif
}

static void gp_module_stack_init()
{
  gp_stack_type = scm_make_smob_type("unify-stacks",0);
  scm_set_smob_print(gp_stack_type,gp_stack_printer);
}

void gp_init_stacks()
{
  gp_stacks = scm_make_weak_key_hash_table(scm_from_int(128));

  gp_nil_fr = gp_make_variable();
  gp_nil_ci = gp_make_variable();
  
  scm_t_bits tag = GP_MK_FRAME_EQ(gp_type); 
  SET_FRAME(tag);
  GP_GETREF(gp_nil_fr)[0] = SCM_PACK(GP_MK_FRAME_EQ(gp_type));
  GP_GETREF(gp_nil_ci)[0] = SCM_PACK(tag);
  GP_GETREF(gp_nil_fr)[1] = scm_from_int(GP_FRAMESIZE);
  GP_GETREF(gp_nil_ci)[1] = scm_from_int(0);
}


SCM_DEFINE(gp_gc, "gp-gc", 0, 0, 0, (), "clean up the stack")
#define FUNC_NAME s_gp_gc
{
#ifdef HAS_GP_GC
  int mute = 0;
  int doit = 0;
  struct gp_stack *gp = get_gp();
 
  scan_weak_lists();

    /*
      if(gp->n > 100000)
        printf("gc0: %d %d\n",gp->n, gp->nrem);
    */
  if(gp->n > 100 && gp->nrem*20 > gp->n)
    {
      pthread_mutex_lock(&gp_gc_lock);
      if(!gp_gc_p)
	{

      //printf("gc: %d %d\n",gp->n, gp->nrem);
      if(gp->n > 100000) doit = 1;
      SCM *pt1,*pt2, *pt3, *pt4,
	*pt1_insert, *pt2_insert,*pt3_insert, *pt4_insert, 
	*last_redo = gp->gp_frstack,
        *last_save = gp->gp_frstack;

      for(pt1 = gp->gp_fr; pt1 > gp->gp_frstack; pt1-= GP_FRAMESIZE)
        {
          if(pt1[-1] && SCM_CONSP(pt1[-1]))
            {
              SCM tag = SCM_CAR(pt1[-1]);
              if(SCM_I_INUMP(tag) && SCM_UNPACK(tag) == gp_redo_tag)
                {
                  last_redo = pt1;
                  break;
                }
            }
        }

      for(pt1 = gp->gp_fr; pt1 >= gp->gp_frstack; pt1 -= GP_FRAMESIZE)
        {
          if(pt1[-1] && SCM_CONSP(pt1[-1]))
            {
              SCM tag = SCM_CAR(pt1[-1]);
              if(SCM_I_INUMP(tag) && SCM_UNPACK(tag) == gp_save_tag)
                {
                  last_save = pt1;
                  break;
                }
            }
        }
      

      pt1 = gp->gp_frstack + GP_FRAMESIZE;      
      pt2 = gp->gp_stack;
      pt3 = gp->gp_cons_stack;
      pt4 = gp->gp_cstack + 1;

      pt1_insert = pt1;
      pt2_insert = pt2;
      pt3_insert = pt3;
      pt4_insert = pt4;
      while(pt1 + GP_FRAMESIZE <= gp->gp_fr)
        {
          if(pt1 + GP_FRAMESIZE <= last_save || 
	     pt1 + GP_FRAMESIZE <= last_redo) 
            mute = 1;
          else
            mute = 0;
	  
          //Sections that start with a save tag and ends with          
          if(!mute)
	    {
	      if(GP_VAL_UNBOUND(pt1 + GP_FRAMESIZE))
		{
		  pt1 += GP_FRAMESIZE;              
		  continue;
		}
	    }
	  else
	    {
	      if(GP_VAL_UNBOUND(pt1 + GP_FRAMESIZE))
		{
		  int i;
		  if(pt1_insert < pt1)
		    for(i = 0; i < GP_FRAMESIZE; i++)
		      pt1_insert[i] = pt1[i];
		  
		  pt1_insert += GP_FRAMESIZE;
		  pt1        += GP_FRAMESIZE;
		
		  continue;
		}
	    }
	  
	  {
	    int i;
	    if(pt1_insert < pt1)
	      for(i = 0; i < GP_FRAMESIZE; i++)
		pt1_insert[i] = pt1[i];
	      
	    pt1_insert += GP_FRAMESIZE;
	    pt1        += GP_FRAMESIZE;
	      
	    /*
	      format4("1 ~a 2 ~a 3 ~a 4 ~a~%",
	      pt1[-1], pt1[-2], pt1[-3], pt1[-4]);
	    */

	    int choice_p = GP_GET_CHOICE(pt1);
	      
	    // Set the new value of the fr reference
	    {
	      SCM *f = GP_GETREF(GP_GET_SELF(pt1));
	      f[1] = scm_from_ulong(pt1_insert - gp->gp_frstack);
	    }

	    // compress the si and cs stack
	    {
	      SCM *cs = (SCM*)0, *si=(SCM*)0;
	      cs = GP_GET_CONS(pt1) + gp->gp_cons_stack;
	      si = GP_GET_VAR (pt1) + gp->gp_stack;
	      //printf("si = %x\n",GP_GET_VAR (pt1));fflush(stdout);
#define macro                                                           \
									\
	      {								\
		while(pt2 < si)						\
		  {							\
		    if(!mute && scm_is_false(*pt2))			\
		      {							\
			pt2++;						\
			continue;					\
		      }							\
		    if(GP(*pt2))					\
		      {							\
			SCM *f = GP_GETREF(*pt2);			\
			scm_t_bits head = SCM_UNPACK(f[0]);		\
			if(!GP_GC_ISMARKED(head) && GP_GC_ISCAND(head)) \
			  {						\
			    pt2++;					\
			    continue;					\
			  }						\
		      }							\
		      							\
		    {							\
		      SCM temp   = *pt2_insert;				\
		      *pt2_insert = *pt2;				\
		      *pt2 = temp;					\
		      							\
		      pt2 ++;						\
		      pt2_insert ++;					\
		    }							\
		  }							\
									\
		while(pt3 < cs)						\
		  {							\
		    if(!mute && scm_is_false(*pt3))			\
		      {							\
			pt3++;						\
			continue;					\
		      }							\
                                                                        \
		    if(GP(*pt3))					\
		      {							\
			SCM *f = GP_GETREF(*pt3);			\
			scm_t_bits head = SCM_UNPACK(f[0]);		\
			if(!GP_GC_ISMARKED(head) && GP_GC_ISCAND(head)) \
			  {						\
			    pt3++;					\
			    continue;					\
			  }						\
		      }							\
                                                                        \
		    {							\
		      SCM temp   = *pt3_insert;				\
		      *pt3_insert = *pt3;				\
		      *pt3 = temp;					\
                                                                        \
		      pt3 ++;						\
		      pt3_insert ++;					\
		    }							\
                                                                        \
		  }							\
	      }							
		
              macro;
	      GP_SET_VAR (pt1_insert, pt2_insert, choice_p, gp);
	      GP_SET_CONS_LIVE(pt1_insert, pt3_insert, gp);
    }
    }
 }
      gp->gp_fr = pt1_insert;

      {
        SCM *si=gp->gp_si;
        SCM *cs=gp->gp_cs;

        macro;

        gp->gp_si = pt2_insert;
	gp->gp_cs = pt3_insert;
      }

      while(pt4 < gp->gp_ci)
	{
	  if(SCM_CONSP(*pt4) && SCM_I_INUMP(SCM_CAR(*pt4)))
	    {
	      SCM x = get_touched_tag(*pt4);
	      if(x && GP(x) && GP_FRAME_VAR(x))
		{
		  SCM *f = GP_GETREF(x);
		  f[1] = scm_from_ulong(pt4_insert - gp->gp_cstack);
		}	      
	    }
	  else if(*pt4 && GP(*pt4) && GP_FRAME_VAR(*pt4))
	    {
	      SCM *f = GP_GETREF(*pt4);
	      f[1] = scm_from_ulong(pt4_insert - gp->gp_cstack);
	    }
	  else if(scm_is_false(*pt4))
	    {					
	      pt4++;
	      continue;
	    }
	  {
	    if(pt4 != pt4_insert) 
	      {
		*pt4_insert = *pt4;	    
		*pt4 = SCM_BOOL_F;
	      }
            
	    pt4        ++;
	    pt4_insert ++;
	  }							
	}
      gp->gp_ci = pt4_insert;
      
      //printf("end gc\n");fflush(stdout);      
     }

      gp->n    = 0;
      gp->nrem = 0;

      pthread_mutex_unlock(&gp_gc_lock);
    }

  if(0 && doit)
    scm_gc();  
#endif
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_clear_frame, "gp-clear-frame", 0, 0, 0, (), 
	   "if s points to a numbered frame, then we will clear it")
#define FUNC_NAME s_gp_cear_frame
{
  gp_no_gc();
  {
    struct gp_stack *gp = get_gp();
    SCM *pt = gp->gp_fr;
    if(gp->gp_fr > gp->gp_frstack + GP_FRAMESIZE && SCM_I_INUMP(pt[-1]))
      gp->gp_fr -= GP_FRAMESIZE;
  }
  gp_do_gc();
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_pop_engine, "gp-pop-engine", 0, 0, 0, (), 
	   "")
#define FUNC_NAME s_gp_pop_engine
{
  if(!SCM_CONSP(gp_engine_path))
    {
      return SCM_BOOL_F;
    }
  
  gp_debug0("Pop Engine>\n");

  SCM s_stack = SCM_CAR(gp_engine_path);

  //printf("pop\n");
  //format1("shall pop store_path: ~a~%",gp_store_path);
  if(SCM_CONSP(gp_store_path))
    gp_store_path  = SCM_CDR(gp_store_path);
  else
    scm_misc_error
      ("gp-pop-engine","gp_store_path not a nonempty list", SCM_EOL);
  
  if(SCM_CONSP(gp_engine_path))
    gp_engine_path  = SCM_CDR(gp_engine_path);
  else
    scm_misc_error
      ("gp-pop-engine","gp_engine_path not a nonempty list", SCM_EOL);
  
  gp_paths = scm_cons(gp_engine_path, gp_store_path);

  SCM e = SCM_CDR(SCM_CAR(gp_engine_path));

  scm_fluid_set_x(gp_current_stack, e);
  
  if(!SCM_CONSP(gp_engine_path))
    scm_misc_error("gp-pop-engine","poped to an empty list",SCM_EOL);

  return s_stack;
}
#undef FUNC_NAME

SCM_DEFINE(gp_peek_engine, "gp-peek-engine", 0, 0, 0, (), 
	   "")
#define FUNC_NAME s_gp_peek_engine
{
    if(!SCM_CONSP(gp_engine_path))
    {
      return SCM_BOOL_F;
    }

  SCM ret = SCM_CAR(gp_engine_path);

  return ret;
}
#undef FUNC_NAME

SCM_DEFINE(gp_push_engine, "gp-push-engine", 2, 0, 0, (SCM s, SCM e), 
	   "")
#define FUNC_NAME s_gp_push_engine
{
   SCM x = scm_cons(SCM_BOOL_F,e); 
   SCM cdr;
   
   gp_debug0("Push Engine>\n");
    
   struct gp_stack *gp = get_gp();
   
   //printf("push\n");
   
   int logical = gp->_logical_;
   SCM rguards = gp->rguards;
   
   gp_engine_path = scm_cons(x , gp_engine_path);

   gp_paths = scm_cons(gp_engine_path, SCM_EOL);
   
   scm_fluid_set_x(gp_current_stack,e);

   gp = get_gp();
   gp_clear(SCM_BOOL_F);
   gp->_logical_ = logical;
   gp->rguards   = rguards;

   if(gp->id > 0)
     {
       gp->_thread_safe_ = 1;
     }
   
   SCM ss = scm_fluid_ref(current_stack); //Sooo confusing TODO: FIXME

   SCM carss = gp_car(ss,ss);

   if(SCM_CONSP(s) || GP_CONSP(s))
   {
       cdr  = gp_gp_cdr(s,s);
   }
   else
   {
       cdr  = scm_cons(SCM_EOL,SCM_EOL);
   }

  if(!SCM_CONSP(cdr))  
  {
    cdr = scm_cons(SCM_EOL,SCM_EOL);
  }

  ss = scm_cons(carss , scm_cons(SCM_CAR(cdr), gp_paths)); 

  SCM_SETCAR(x, s);

  return ss;
}
#undef FUNC_NAME

SCM_DEFINE(gp_new_engine, "gp-new-engine", 1, 0, 0, (SCM e), 
	   "")
#define FUNC_NAME s_gp_new_engine
{
   SCM x = scm_cons(SCM_EOL,e); 
   SCM path = gp_paths;
  
   gp_debug0("Push Engine>\n");
   
   gp_engine_path = scm_cons(x , SCM_EOL);

   gp_paths = scm_cons(gp_engine_path, SCM_EOL);

   gp_store_path = SCM_EOL;
   
   scm_fluid_set_x(gp_current_stack,e);

   {
     gp_clear(SCM_BOOL_F);

     SCM ss = scm_fluid_ref(current_stack); //Sooo confusing TODO: FIXME

     SCM carss = gp_car(ss,ss);

     SCM cdr  = scm_cons(SCM_EOL, gp_paths);
   
     ss = scm_cons(carss , cdr); 

     return scm_cons(ss, path);
   }
}

#undef FUNC_NAME

SCM_DEFINE(gp_set_engine, "gp-set-engine", 1, 0, 0, (SCM paths), 
	   "")
#define FUNC_NAME s_gp_set_engine
{
   SCM e = SCM_CDR(SCM_CAR(SCM_CAR(paths)));
   SCM pathout = gp_paths;
   
   //printf("set\n");
  
   if(SCM_CONSP(paths))
     {
       gp_engine_path = SCM_CAR(paths);
       gp_store_path  = SCM_CDR(paths);
     }
   else
     scm_misc_error("gp-set-engine","paths not a cons ~a",scm_list_1(paths));

   gp_paths       = paths;
   
   scm_fluid_set_x(gp_current_stack,e);

  return pathout;
}
#undef FUNC_NAME

SCM_DEFINE(gp_combine_engines, "gp-combine-engines", 1, 0, 0, (SCM l), 
	   "")
#define FUNC_NAME s_gp_combine_engine
{
  SCM vec = scm_c_make_vector (1,l);
  struct gp_stack * gp = get_gp();

  GP_TEST_CSTACK;  
  
  gp->gp_ci[0] = vec;
  gp->gp_ci++;
  
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_combine_pop, "gp-combine-pop", 0, 0, 0, (), 
	   "")

#define FUNC_NAME s_gp_combine_pop
{
  //printf("combine pop\n");
  SCM ret = SCM_CAR(gp_store_path);
  gp_store_path = SCM_CDR(gp_store_path);
  return ret;
}
#undef FUNC_NAME

SCM_DEFINE(gp_combine_push, "gp-combine-push", 1, 0, 0, (SCM r), 
	   "")

#define FUNC_NAME s_gp_combine_push
{
  gp_store_path = scm_cons(r, gp_store_path);

  //printf("combine push\n");

  //format1("pushed store_path: ~a~%",gp_store_path);
  
  if(SCM_CONSP(gp_paths))
    SCM_SETCDR(gp_paths, gp_store_path);
  else
    scm_misc_error("gp-combine-push","gp_paths not a cons ~a~%",
                   scm_list_1(gp_paths));

  //format1("pushed store_path: ~a~%",gp_store_path);
    
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM variable_bindings        = SCM_BOOL_F;
SCM variable_bindings_found  = SCM_BOOL_F;
SCM all_variable_bindings    = SCM_BOOL_F;

SCM folder(SCM key, SCM val, SCM seed, SCM data)
{
  //printf("folder\n");fflush(stdout);
  //format2("key, val : ~a, ~a ~%",key,val);
  int level = scm_to_int(SCM_CDR(data));

  if(GP_ID(key) > level)
    return seed;

  SCM val2 = scm_hashq_ref(variable_bindings, key, SCM_BOOL_F);
  
  if(scm_is_true(val2))
    {
      return seed;
    }
  else
    {
      //printf("new\n");
      scm_hashq_set_x(variable_bindings, key, SCM_BOOL_T);
      SCM val3 = scm_hashq_ref(all_variable_bindings, key, SCM_BOOL_F);
      if(scm_is_true(val3))
        {
          //printf("registred\n");
          SCM val4 = scm_hashq_ref(variable_bindings_found, key, SCM_BOOL_F);
          if(scm_is_true(val4))
            {
              //printf("fat\n");
              return scm_cons(scm_cons(key,val),seed);
            }
          else
            {
              //printf("thin\n");
              scm_hashq_set_x(variable_bindings_found, key, SCM_BOOL_T);
              return scm_cons(scm_cons(key,val),
                              scm_cons(scm_cons(key, val3), seed));
            }
        }
      else
        {
          //printf("not registred\n");
          scm_hashq_set_x(all_variable_bindings, key, val);
          return seed;
        }
    }
}

SCM level1(SCM data, SCM ll, SCM ret);
SCM level0(SCM data, SCM l, SCM ret)
{
  //printf("level0\n");fflush(stdout);
  
  for(;SCM_CONSP(l);l = SCM_CDR(l))
    {
      ret = level1(data,SCM_CAR(l),ret);
    }

  return ret;
}

SCM level1(SCM data, SCM ll, SCM ret)
{
  SCM l0 = SCM_CAR(data);

  //printf("level1\n");fflush(stdout);
  
  for(;SCM_CONSP(ll);ll=SCM_CDR(ll))
    {
      if(scm_is_eq(ll, l0))
        break;

      {
        SCM x = SCM_CAR(ll);
        if(scm_is_true(scm_vlist_p(x)))
          {
            //printf("vlist\n");fflush(stdout);
            //format1("vlist : ~a~%", x);
            ret = vhash_fold_all_exp(data, folder, ret, x);
          }
        if(SCM_I_IS_VECTOR(x))
          {
            //printf("vector\n");fflush(stdout);
            ret = level0(data, scm_c_vector_ref(ret, 0), ret);
          }
        if(SCM_CONSP(x))
          {
            //printf("cons\n");fflush(stdout);
            ret = folder(SCM_CAR(x), SCM_CDR(x), ret, data);
          }
      }
    }

  return ret;
}


SCM get_all_conflicting_bindings(SCM l, SCM data)
{
  variable_bindings_found = scm_c_make_hash_table(100);
  all_variable_bindings   = scm_c_make_hash_table(100);

  //printf("conflict\n");fflush(stdout);
  
  SCM ret = SCM_EOL;
  for(;SCM_CONSP(l);l=SCM_CDR(l))
    {
      variable_bindings = scm_c_make_hash_table(100);
      ret = level1(data, SCM_CAR(l), ret);
    }
  
  return ret;
}

SCM_DEFINE(gp_combine_state, "gp-combine-state", 2, 0, 0, (SCM s, SCM l), 
	   "")
#define FUNC_NAME s_gp_combine_engine
{  
  SCM data = gp_gp_cdr(s,s);
  SCM l0   = SCM_CAR(data);

  //printf("combine state\n");fflush(stdout);
  
  struct gp_stack * gp = get_gp();
  
  SCM ll   = SCM_EOL;
  while(SCM_CONSP(l))
  { 
    SCM sx = SCM_CAR(l);
    SCM e  = SCM_CAR(gp_gp_cdr(sx,sx));
    if(!scm_is_eq(e,l0))
      {
        ll = scm_cons(e, ll);
      }
    l  = SCM_CDR(l);
  }  

  
  SCM datah = scm_cons(l0, scm_from_int(gp->id));
  
  SCM ret = get_all_conflicting_bindings(ll,datah);
  
  SCM sout;

  if(SCM_CONSP(ll))
    {
      ll = scm_reverse(ll);

      SCM vec = scm_c_make_vector (1,ll);

      sout = scm_cons(gp_car(s,s),scm_cons(scm_cons(vec,l0), SCM_CDR(data)));
    }
  else
    sout = s;
  
  return scm_cons(sout,ret);
}
#undef FUNC_NAME

SCM_DEFINE(gp_get_current_engine_path, "gp-current-engine-path", 0, 0, 0, (), 
	   "marks a variable as permanent and hence will be removed from stacks")
#define FUNC_NAME s_gp_get_current_engine_path
{
  return gp_engine_path;
}
#undef FUNC_NAME
