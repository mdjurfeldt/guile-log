SCM_DEFINE(gp_attvar, "gp-attvar?", 2, 0, 0, (SCM x, SCM s), 
	   "check to see if variable is an attributed variable")
#define FUNC_NAME s_gp_attvar
{
  SCM l;
  UNPACK_S0(l,s,"cannot unpack s in gp_ref_set");
  SCM *ref;
  if(GP(x))
  {
    ref = gp_lookup2(GP_GETREF(x), l);
    if(GP_STAR(ref) && (GP_ATTR(ref) || GP_UNBOUND(ref)))
      {
	return SCM_BOOL_T;
      }      
  }  
  return SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_attdata, "gp-att-data", 2, 0, 0, (SCM x, SCM s), 
	   "check to see if variable is an attributed variable")
#define FUNC_NAME s_gp_attdata
{
  SCM l;
  UNPACK_S0(l,s,"cannot unpack s in gp_ref_set");

  SCM *ref;
  if(GP(x))
  {
    ref = gp_lookup2(GP_GETREF(x), l);
    if(GP_STAR(ref) && GP_ATTR(ref))
      {
	return gp_gp_lookup(SCM_CDR(ref[1]), s);
      }
  }  
  return SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_mk_att_data, "gp-make-attribute-from-data", 2, 0, 0, (SCM model, SCM data), 
	   "check to see if variable is an attributed variable")
#define FUNC_NAME s_gp_mk_att_data
{
  SCM datapt = SCM_EOL;
  while(SCM_CONSP(data))
    {
      SCM x = SCM_CAR(data);
      SCM car = SCM_CAR(x);
      SCM cdr = SCM_CDR(x);
      SCM h = gp_make_fluid();
      SCM *hpt = GP_GETREF(h);
      hpt[1] = cdr;

      if(GP_STAR(cdr) || SCM_VARIABLEP(cdr))
	hpt[0] = SCM_PACK(GP_MK_FRAME_PTR(gp_type));
      else
	hpt[0] = SCM_PACK(GP_MK_FRAME_VAL(gp_type));

      datapt = scm_cons(scm_cons(car,h),datapt);
      data   = SCM_CDR(data);
    }
  
  data = datapt;

  {
    SCM x   = gp_make_fluid();
    SCM y   = gp_make_fluid();

    SCM *pt = GP_GETREF(x);

    scm_t_bits b= SCM_UNPACK(GP_GETREF(model)[0]);
    b = GPRENEW(b);

    pt[0]   = SCM_PACK(b);
    pt[1]   = scm_cons(y,data);
  
    return x;
  }
}
#undef FUNC_NAME

SCM_DEFINE(gp_att_rawvar, "gp-att-raw-var", 2, 0, 0, (SCM x, SCM s), 
	   "check to see if variable is an attributed variable")
#define FUNC_NAME s_gp_att_rawvar
{
  SCM l;
  UNPACK_S0(l,s,"cannot unpack s in gp_ref_set");

  SCM *ref;
  if(GP(x))
  {
    ref = gp_lookup2(GP_GETREF(x), l);
    if(GP_STAR(ref))
      {
	if(GP_ATTR(ref))
	  {
	    return SCM_CAR(ref[1]);
	  }
	else
	  {
	    if(GP_UNBOUND(ref))
	      {
		return x;
	      }
	    else
	      return SCM_BOOL_F;
	  }	
      }  
  return SCM_BOOL_F;
  }
 return SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_put_attdata, "gp-att-put-data", 3, 0, 0, (SCM x, SCM v, SCM s), 
	   "check to see if variable is an attributed variable")
#define FUNC_NAME s_gp_put_attdata
{
  SCM l;
  UNPACK_S0(l,s,"cannot unpack s in gp_ref_set");

  SCM *ref;
  if(GP(x))
  {
    ref = gp_lookup2(GP_GETREF(x), l);
    if(GP_STAR(ref) && GP_UNBOUND(ref))
      {
	SCM pt = v;
	while(SCM_CONSP(pt))
	  {
	    s = gp_put_attr(x, SCM_CAAR(pt), SCM_CDR(pt), s);
	    if(scm_is_false(s)) break;
	    pt = SCM_CDR(pt);
	  }
      }
  }  
  return s;
}
#undef FUNC_NAME

//#define DB(X) X
SCM_DEFINE(gp_attvar_raw, "gp-attvar-raw?", 2, 0, 0, (SCM x, SCM s), 
	   "check to see if variable is an raw attributed variable")
#define FUNC_NAME s_gp_attvar_raw
{
  SCM l;
  UNPACK_S0(l,s,"cannot unpack s in gp_ref_set");

  SCM *ref;
  if(GP(x))
  {

    ref = gp_lookup2(GP_GETREF(x), l);
    if(GP_STAR(ref) && (GP_ATTR(ref)))
      {
	return SCM_BOOL_T;
      }      
  }  
  return SCM_BOOL_F;
}

#undef FUNC_NAME
//#define DB(X)

SCM gp_put_attr_xx(SCM x, SCM lam, SCM val, SCM s, scm_t_bits bits, int bang)
{
  if(GP(x))
  {
    SCM l;
    UNPACK_S0(l,s,"cannot unpack s in gp_ref_set");

    SCM *ref;
    ref = gp_lookup2(GP_GETREF(x), l);

    if(GP(lam))
      lam = GP_UNREF(gp_lookup2(GP_GETREF(lam), l));

    if(GP(val))
      val = GP_UNREF(gp_lookup2(GP_GETREF(val), l));

    if(GP_STAR(ref))
      {
	if(GP_ATTR(ref) || GP_UNBOUND(ref))
	  {
	    if(!GP_ATTR(ref))
	      {
		SCM newvar1 =
                  (bang ? (make_logical()) : (GP_IT(gp_mk_var(s))));
		SCM newvar3 =
                  (bang ? (make_logical()) : (GP_IT(gp_mk_var(s))));

		s = gp_ref_attr_set(GP_UNREF(ref), scm_cons(GP_UNREF(ref),newvar3),
				    s, SCM_PACK(bits)); 

		s = gp_ref_set(newvar3, 
			       scm_cons(scm_cons(lam,newvar1),SCM_EOL), s); 
                
		s = gp_ref_set(newvar1, val, s);
	      }
	    else
	      {
		int found = 0;
		SCM tag = SCM_CDR(ref[1]);
		SCM l = gp_gp_lookup(tag, s);
		SCM it = l;

		while(SCM_CONSP(l))
		  {
		    if(SCM_CONSP(SCM_CAR(l)))
		      {
			if(scm_is_eq(SCM_CAAR(l), lam))
			  {
                            if(bang)
			      {
                                gp_set_ref_force(GP_GETREF(SCM_CDAR(l)), val);
                              }
			    else
			      { 
                                s = gp_set2(SCM_CDAR(l), val, s);
                              }
			    found = 1;
			    break;
			  }
			l = SCM_CDR(l);
		      }
		      else
			scm_misc_error("gp-put-attr","not an internal cons in the attribute list inside the attributed variable ~a~%",scm_list_1(l));
		  }
		if(!found)
		  {
		    SCM newvar =
                      (bang ? (make_logical()) : (GP_IT(gp_mk_var(s))));

                    if(bang)
                      {
                        gp_set_ref_force(GP_GETREF(tag),
                                         scm_cons(scm_cons(lam, newvar), it));
                        gp_set_ref_force(GP_GETREF(newvar), val);
                      }
                    else
                      {
                        s = gp_set2(tag, scm_cons(scm_cons(lam, newvar),
                                                  it), s);		    
                        s = gp_set2(newvar, val, s);
                      }

		  }
	      }
	  }
	else
	  scm_misc_error("gp-put-attr","not an attributed variable~a~%",
			 scm_list_1(x));
      }
    else
      scm_misc_error("gp-put-attr","not an attributed variable ~a~%",
		     scm_list_1(x));
    
  }  
  else
    scm_misc_error("gp-put-attr","not an attributed variable ~a~%",
		   scm_list_1(x));
  return s;
}

SCM_DEFINE(gp_put_attr, "gp-put-attr", 4, 0, 0, 
	     (SCM x, SCM lam, SCM val, SCM s), 
	   "put an attrubute on a variable with unification handler lambda")
#define FUNC_NAME s_gp_put_attr
{
  return gp_put_attr_xx(x, lam, val, s, (scm_t_bits)0, 0);
}
#undef FUNC_NAME

SCM_DEFINE(gp_put_attr_x, "gp-put-attr!", 4, 0, 0, 
	     (SCM x, SCM lam, SCM val, SCM s), 
	   "put an attrubute on a variable with unification handler lambda")
#define FUNC_NAME s_gp_put_attr_x
{
  return gp_put_attr_xx(x, lam, val, s, (scm_t_bits)0, 1);
}
#undef FUNC_NAME

SCM_DEFINE(gp_put_attr_guarded, "gp-put-attr-guarded", 4, 0, 0, 
	   (SCM x, SCM lam, SCM val, SCM s), 
	   "put an attrubute on a variable with unification handler lambda")
#define FUNC_NAME s_gp_put_attr_guarded
{
  return gp_put_attr_xx(x, lam, val, s, GPI_GUARD, 0);
}
#undef FUNC_NAME

SCM_DEFINE(gp_put_attr_guarded_x, "gp-put-attr-guarded!", 4, 0, 0, 
	   (SCM x, SCM lam, SCM val, SCM s), 
	   "put an attrubute on a variable with unification handler lambda")
#define FUNC_NAME s_gp_put_attr_guarded_x
{
  return gp_put_attr_xx(x, lam, val, s, GPI_GUARD, 1);
}
#undef FUNC_NAME

SCM_DEFINE(gp_put_attr_wguarded, "gp-put-attr-weak-guarded", 4, 0, 0, 
	   (SCM x, SCM lam, SCM val, SCM s), 
	   "put an attrubute on a variable with unification handler lambda")
#define FUNC_NAME s_gp_put_attr_wguarded
{
return gp_put_attr_xx(x, lam, val, s, GPI_WGUARD,0);
}
#undef FUNC_NAME

SCM_DEFINE(gp_put_attr_wguarded_x, "gp-put-attr-weak-guarded!", 4, 0, 0, 
	   (SCM x, SCM lam, SCM val, SCM s), 
	   "put an attrubute on a variable with unification handler lambda")
#define FUNC_NAME s_gp_put_attr_wguarded_x
{
return gp_put_attr_xx(x, lam, val, s, GPI_WGUARD,1);
}
#undef FUNC_NAME



SCM_DEFINE(gp_get_attr, "gp-get-attr", 3, 0, 0, (SCM x, SCM lam, SCM s), 
	   "get an attrubute on a variable with unification handler lambda")
#define FUNC_NAME s_gp_attvar
{
  if(GP(x))
    {
      SCM l;
      UNPACK_S0(l,s,"cannot unpack s in gp_ref_set");
      SCM *ref;
      ref = gp_lookup2(GP_GETREF(x), l);
      if(GP(lam))
	lam = GP_UNREF(gp_lookup2(GP_GETREF(lam), l));
      if(GP_STAR(ref))
	{
	  if(GP_ATTR(ref))
	    {
	      SCM it = gp_gp_lookup(SCM_CDR(ref[1]), s);
	      if(GP_UNBOUND(ref))
		{
		  return SCM_BOOL_F;
		}
	      else
		{
		  SCM l = it;
		  while(SCM_CONSP(l))
		    {
		      if(SCM_CONSP(SCM_CAR(l)))
			{
			  if(scm_is_eq(SCM_CAAR(l), lam))
			    {
			      return SCM_CDAR(l);
			    }
			  l = SCM_CDR(l);
			}
		      else
			scm_misc_error("gp-put-attr","not an internal cons in the attribute list inside the attributed variable ~a~%",scm_list_1(l));
		  }
		return SCM_BOOL_F;
	      }
	  }
	else
	  return SCM_BOOL_F;
      }
    else
      return SCM_BOOL_F;
    
  }  
  else
    return SCM_BOOL_F;
}
#undef FUNC_NAME



SCM_DEFINE(gp_del_attr, "gp-del-attr", 3, 0, 0, (SCM x, SCM lam, SCM s), 
	   "delete an attrubute on a variable with unification handler lambda")
#define FUNC_NAME s_gp_del_attr
{
  if(GP(x))
  {
    x   = gp_gp_lookup(x  , s);
    lam = gp_gp_lookup(lam, s);
    if(GP(x))
      {
	SCM *ref = GP_GETREF(x);
	gp_debug0("a gp\n");
	if(GP_ATTR(ref))
	  {
	    SCM it = SCM_CDR(ref[1]);
	    gp_debug0("a an attr\n");
	    if(GP_UNBOUND(ref))
	      {
		return s;
	      }
	    else
	      {
		SCM l = gp_gp_lookup(it, s);
		SCM p = SCM_EOL;
		SCM r = SCM_EOL;
		while(SCM_CONSP(l))
		  {
		    gp_format1("1: ~a~%",l);
		    if(SCM_CONSP(SCM_CAR(l)))
		      {
			if(scm_is_eq(SCM_CAAR(l), lam))
			  {
			    r = SCM_CDR(l);
			    break;
			  }
			else
			  {
			    p = scm_cons(SCM_CAR(l),p);
			    l = SCM_CDR(l);
			  }		       
		      }	
		  }
		
		if(SCM_NULLP(p) && SCM_NULLP(r))
		  {		 
		    s = gp_ref_set(GP_UNREF(ref), SCM_CAR(ref[1]),s);
		  }
		else
		  {
		    while(SCM_CONSP(p))
		      {
			r = scm_cons(SCM_CAR(p),r);
			p = SCM_CDR(p);
		      }
		    
		    gp_format1("tt: ~a~%",r);
		    s = gp_set2(it, r, s); 					
		  }
	      }
	  }
      }
  }  
  return s;
}
#undef FUNC_NAME

SCM_DEFINE(gp_del_attr_x, "gp-del-attr!", 3, 0, 0, (SCM x, SCM lam, SCM s), 
	   "delete an attrubute on a variable with unification handler lambda")
#define FUNC_NAME s_gp_del_attr_x
{
  if(GP(x))
  {
    x   = gp_gp_lookup(x  , s);
    lam = gp_gp_lookup(lam, s);
    if(GP(x))
      {
	SCM *ref = GP_GETREF(x);
	gp_debug0("a gp\n");
	if(GP_ATTR(ref))
	  {
	    SCM it = SCM_CDR(ref[1]);
	    gp_debug0("a an attr\n");
	    if(GP_UNBOUND(ref))
	      {
		return s;
	      }
	    else
	      {
		SCM l = it;
		SCM p = SCM_EOL;
		SCM r = SCM_EOL;
		while(SCM_CONSP(l))
		  {
		    gp_format1("1: ~a~%",l);
		    if(SCM_CONSP(SCM_CAR(l)))
		      {
			if(scm_is_eq(SCM_CAAR(l), lam))
			  {
			    r = SCM_CDR(l);
			    break;
			  }
			else
			  {
			    p = scm_cons(SCM_CAR(l),p);
			    l = SCM_CDR(l);
			  }		       
		      }	
		  }
		
		if(SCM_NULLP(p) && SCM_NULLP(r))
		  {		 
                     gp_set_ref_force(ref, SCM_CAR(ref[1]));
		  }
		else
		  {
		    scm_t_bits bits = SCM_UNPACK(ref[0])
		      & (GPI_GUARD | GPI_WGUARD);

		    while(SCM_CONSP(p))
		      {
			r = scm_cons(SCM_CAR(p),r);
			p = SCM_CDR(p);
		      }
		    
		    gp_format1("tt: ~a~%",r);
		    gp_ref_attr_set_force(x, scm_cons(SCM_CAR(ref[1]), r), 
					  SCM_PACK(bits), s);
		  }
	      }
	  }
      }
  }  
  return s;
}
#undef FUNC_NAME
