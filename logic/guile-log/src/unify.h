/*
 	Copyright (C) 2009, 2010 Free Software Foundation, Inc.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#define Q(x) ((((scm_t_bits) x) << 2) + 2)
#define Ix   (SCM_UNPACK(*pat))
#define i_end    Q(0)
#define i_cons   Q(1)
#define i_var    Q(2)
#define i_eq     Q(3)
#define i_pop    Q(4)
#define i_insert Q(5)
#define i_unify  Q(6)
#define i_match  Q(7)
#define i_scheme Q(8)
#define i_load   Q(9)
#define i_arbr   Q(10)

struct gp_stack
{
  scm_t_bits dynstack_length;
  SCM dynstack;
  SCM rguards;
  SCM handlers;

  int id;
  int thread_id;

  int _logical_;
  int _thread_safe_;

  int gp_ncs;

  SCM *gp_cons_stack;

  int gp_nc;
  SCM *gp_cstack;
  
  int gp_ns;
  SCM *gp_stack;

  int gp_nfr;
  SCM *gp_frstack;
  
  SCM *gp_cs;
  SCM *gp_si;
  SCM *gp_ci;
  SCM *gp_fr;

  SCM *gp_nnc;
  SCM *gp_nns;
  SCM *gp_nncs;
  SCM *gp_nnfr;

  int n;
  int nrem;
};

#include "vlist/vlist.h"
#include "indexer/indexer.h"
inline struct gp_stack *maybe_get_gp();

SCM vhash_fold_all_exp(SCM data, SCM (*proc)(SCM, SCM, SCM, SCM)
                       , SCM ret, SCM vhash);

SCM_API SCM gp_setup_namespace(SCM record, SCM nsfkn);
SCM_API SCM gp_setup_closed(SCM err);

SCM_API SCM gp_set_closure_struct(SCM scm);
SCM_API SCM gp_gp(SCM scm);

SCM_API SCM  gp_varp(SCM x, SCM s);
SCM_API SCM  gp_atomicp(SCM x, SCM s);
SCM_API SCM  gp_consp(SCM x);

SCM_API SCM  gp_set(SCM var, SCM val, SCM s);
SCM_API SCM  gp_set2(SCM var, SCM val, SCM s);
SCM_API SCM  gp_ref_set(SCM var, SCM val, SCM s);

SCM_API SCM  gp_clear(SCM s);
SCM_API SCM  gp_gp_newframe(SCM s);
SCM_API SCM  gp_gp_newframe_choice(SCM s);
SCM_API SCM  gp_mkvar(SCM s);
SCM_API SCM  gp_heap_var();
SCM_API SCM  smob2scm(SCM scm, SCM s);
SCM_API SCM  gp_gp_unify(SCM scm1, SCM scm2, SCM s);
SCM_API SCM  gp_gp_lookup(SCM scm, SCM s);
SCM_API SCM  gp_var_number(SCM x, SCM s);
SCM_API SCM  gp_soft_init();

SCM_API SCM  gp_next_budy(SCM x);

SCM_API SCM gp_cons_bang(SCM car, SCM cdr, SCM s);
SCM_API SCM gp_pair_bang(SCM x, SCM s);
SCM_API SCM gp_pair(SCM x, SCM s);
SCM_API SCM gp_null(SCM x, SCM s);
SCM_API SCM gp_null_bang(SCM x, SCM s);
SCM_API SCM gp_m_unify(SCM x, SCM y, SCM s);
SCM_API SCM gp_gp_cdr(SCM x, SCM s);
SCM_API SCM gp_car(SCM x, SCM s); 

SCM_API SCM gp_gp_unwind(SCM fr);
SCM_API SCM gp_gp_unwind_tail(SCM fr);
SCM_API SCM gp_gp_store_state(SCM s);
SCM_API SCM gp_gp_store_state_all(SCM s);
SCM_API SCM gp_gp_restore_state(SCM cont, SCM K);

SCM_API SCM gp_make_fluid();
SCM_API SCM gp_make_nvariable(SCM id);
               
SCM_API SCM gp_fluid_set_bang(SCM f, SCM v, SCM s);

SCM_API SCM gp_dynwind(SCM in, SCM out, SCM s);

//SCM_API SCM gp_copy(SCM x);

SCM_API SCM gp_logical_incr();
SCM_API SCM gp_logical_decr();

SCM_API SCM gp_make_stack(SCM id, SCM thread_id, SCM nc, SCM ns, SCM ncs,
			  SCM nfr);

SCM_API SCM gp_member(SCM x, SCM l, SCM s);
SCM_API SCM gp_right_of(SCM x, SCM y, SCM l, SCM s);

SCM_API SCM gp_make_member();
SCM_API SCM gp_make_right();
SCM_API SCM gp_set_closure_tag(SCM tag);

SCM_API SCM gp_set_thread_safe(SCM s, SCM bool);

SCM_API SCM gp_fluid_force_bang(SCM f, SCM v, SCM s);

SCM_API SCM undo_safe_variable_rguard(SCM var, SCM kind, SCM s);
SCM_API SCM undo_safe_variable_lguard(SCM var, SCM kind, SCM s);
SCM_API SCM undo_safe_variable_guard(SCM var, SCM kind, SCM s);
SCM_API SCM gp_with_fluid(SCM var, SCM kind);

SCM_API SCM gp_get_current_stack();
SCM_API SCM gp_get_rguards();

SCM_API SCM gp_set_tester(SCM x);

SCM_API SCM gp_prompt(SCM tag, SCM lam);
SCM_API SCM gp_abort(SCM old, SCM tag); 

SCM_API SCM gp_handlers_ref();
SCM_API SCM gp_handlers_set_x(SCM h);

SCM_API SCM gp_cont_ids_ref();
SCM_API SCM gp_cont_ids_set_x(SCM h);

SCM_API SCM gp_guard_vars(SCM s);

SCM_API SCM gp_clear_frame();
SCM_API SCM gp_clear_frame_x(SCM s);

SCM_API SCM gp_gc();

SCM_API SCM gp_mk_att_data(SCM model, SCM data);
SCM_API SCM gp_attvar(SCM x, SCM s);
SCM_API SCM gp_attvar_raw(SCM x, SCM s);
SCM_API SCM gp_attdata(SCM x, SCM s);
SCM_API SCM gp_put_attr(SCM x, SCM lam, SCM val, SCM s);
SCM_API SCM gp_put_attr_guarded(SCM x, SCM lam, SCM val, SCM s);
SCM_API SCM gp_put_attr_wguarded(SCM x, SCM lam, SCM val, SCM s);
SCM_API SCM gp_put_attr_x(SCM x, SCM lam, SCM val, SCM s);
SCM_API SCM gp_put_attr_guarded_x(SCM x, SCM lam, SCM val, SCM s);
SCM_API SCM gp_put_attr_wguarded_x(SCM x, SCM lam, SCM val, SCM s);
SCM_API SCM gp_get_attr(SCM x, SCM lam, SCM s);
SCM_API SCM gp_del_attr(SCM x, SCM lam, SCM s);
SCM_API SCM gp_del_attr_x(SCM x, SCM lam, SCM s);
SCM_API SCM gp_set_attributed_trampoline(SCM x, SCM d);
SCM_API SCM gp_put_attdata(SCM x, SCM v, SCM s);
SCM_API SCM gp_att_rawvar(SCM x, SCM s);

SCM_API SCM gp_mark_permanent(SCM x);
SCM_API SCM gp_add_unwind_hook(SCM x);

SCM_API SCM gp_make_ephermal_pair(SCM id, SCM v);

SCM_API SCM gp_lookup_1(SCM x, SCM s);

SCM_API SCM gp_get_taglist();
SCM_API SCM gp_match(SCM e, SCM run, SCM s);
SCM_API SCM gp_set_type_attribute(SCM x);

SCM_API SCM gp_find_elf_relative_adress(SCM ip);

SCM_API SCM gp_get_id_data(SCM x);
SCM_API SCM gp_get_var_var(SCM x);
SCM_API SCM gp_clobber_var(SCM var,  SCM id, SCM val);
SCM_API SCM gp_int_to_code(SCM x);
SCM_API SCM gp_code_to_int(SCM x);
SCM_API SCM gp_make_null_procedure(SCM n, SCM def);
SCM_API SCM gp_fill_null_procedure(SCM proc, SCM addr, SCM l);

SCM_API SCM gp_combine_pop();
SCM_API SCM gp_combine_push(SCM r);
SCM_API SCM gp_new_engine(SCM e); 
SCM_API SCM gp_set_engine(SCM path); 
SCM_API SCM gp_restore_engine_guards(SCM cont); 
SCM_API SCM gp_store_engine_guards(); 
SCM_API SCM gp_push_engine(SCM s, SCM engine);
SCM_API SCM gp_pop_engine();
SCM_API SCM gp_peek_engine();
SCM_API SCM gp_combine_engines(SCM l); 
SCM_API SCM gp_combine_state(SCM s, SCM l); 
SCM_API SCM gp_get_current_engine_path();
SCM_API SCM gp_set_struct(SCM a, SCM b);
SCM_API SCM gp_make_struct(SCM a, SCM b);
SCM_API SCM gp_custom_fkn(SCM custom_vm_fkn, SCM a, SCM b);
SCM_API SCM gp_gp_prune(SCM fr); 
SCM_API SCM gp_gp_prune_tail(SCM fr); 
SCM_API SCM gp_is_deterministic(SCM s0);
SCM_API SCM gp_set_setmap_struct(SCM set, SCM map);
SCM_API SCM gp_set_cp_attributes(SCM fluid);
SCM_API SCM gp_set_set_tag(SCM item);
/*
SCM_API SCM gp_setup_prolog_vm_env
(
 SCM dls               , 
 SCM delayers          ,
 SCM unwind_hooks      ,
 SCM unwind_parameters ,
 SCM true              ,
 SCM false             ,
 SCM gp_not_n          ,       
 SCM gp_is_delayed     ,
 SCM model_lambda);

SCM_API SCM gp_make_vm_model(SCM bytevector);

SCM_API SCM gp_pack_start
	   (SCM nvar,
	    SCM nstack,
	    SCM instructions,
	    SCM contants,
	    SCM tvars);


#define MK_CUSTOM_FKN_MODEL(ffkn,nm,nmstr)				\
  SCM_DEFINE(nm, nmstr,1,0,0, (SCM custom_code),"")			\
  {									\
    if(scm_is_true(custom_code))					\
      {									\
	SCM fkn = scm_words(scm_tc7_program | (3<<16),5);		\
	((char **) fkn)[1] = (char *) SCM_BYTEVECTOR_CONTENTS(custom_code); \
	SCM_PROGRAM_FREE_VARIABLE_SET(fkn,0,SCM_PACK((scm_t_bits)ffkn | 2)); \
	SCM_PROGRAM_FREE_VARIABLE_SET(fkn,1,SCM_BOOL_F);		\
	SCM_PROGRAM_FREE_VARIABLE_SET(fkn,2,SCM_BOOL_F);		\
	return fkn;							\
      }									\
  									\
    scm_misc_error(nmstr,"custom_code is not sat",SCM_EOL);		\
  }
*/
