(define-module (logic guile-log util)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log umatch)
  #:export (<pair?> <car> <cdr> s-lookup s-pair? s-pair!? s-car s-cdr last))

(define-syntax s-lookup 
  (syntax-rules ()
    ((_ x)   (gp-lookup x S))
    ((_ x S) (gp-lookup x S))))

(define-syntax s-pair?  
  (syntax-rules ()
    ((_ x)   (gp-pair?  (s-lookup x)   S))
    ((_ x S) (gp-pair?  (s-lookup x S) S))))

(define-syntax s-pair!? 
  (syntax-rules ()
    ((_ x)   (gp-pair!? (s-lookup x)   S))
    ((_ x S) (gp-pair!? (s-lookup x S) S))))

(define-syntax s-car
  (syntax-rules ()
    ((_ x)   (gp-car (s-lookup x)    S))
    ((_ x S) (gp-car (s-lookup x S ) S))))

(define-syntax s-cdr
  (syntax-rules ()
    ((_ x)   (gp-cdr (s-lookup x)    S))
    ((_ x S) (gp-cdr (s-lookup x S ) S))))

(define (<pair?> s p cc x)
  (let ((ss (s-pair!? x s)))
    (if ss
        (cc ss p)
        (p))))


(<define> (<car> x)
  (<pair?> x)
  (<cc> (s-car x)))
        

(<define> (<cdr> x)
  (<pair?> x)
  (<cc> (s-cdr x)))

(<define> (last x l)
  (<if> (<pair?> x)
        (<and>
         (<values> (y) (<cdr> x))
         (<if> (<pair?> y)
               (last y l)
               (<r=> x l)))
        (<=> () l)))
        
