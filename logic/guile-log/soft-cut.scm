(define-module (logic guile-log soft-cut)
  #:use-module (logic guile-log)
  #:use-module ((logic guile-log umatch) #:select
		(gp-deterministic?))
  #:use-module (logic guile-log interleave)
  #:export (<soft-if> 
	    soft-if-f
	    <setup-call-cleanup-once>))

#|
swi uses A *-> B ; C as a soft cut,
if A has at least one solution the result is A,B else \+A,C

The difficulty lies in that one need to mutate the failure depending if
one reaches A or not

note set! is magical in that it is macrofied to make sure it is guarded
and restored at reinstation of a state. the lgard adds it to the state list
and rgard removes it from the list going backwards reverses the actions.
|#

(define-guile-log <soft-if> 
  (syntax-rules ()
    ((_ w a b) (<and> w a b))
    ((_ w a b c)   
     (<let> w ((p0   P)
	       (s0   S)
	       (scut SCUT)
	       (cut1 CUT)
	       (cc   CC)
	       (fr (<newframe-choice>)))
        (<let-with-lr-guard> wind lguard rguard
			     ((rp (lambda ()                      
				    (<unwind-tail> fr)
				    (parse<> (cut1 s0 p0 cc) c))))
	     
	  (lguard
	   (</.> 
	    (<with-fail> (lambda () (rp))
	      (<with-cut> cut1 SCUT
		a
		(<code>
		 (if (gp-deterministic? fr)
		     (<prune-tail> fr)))
		(<code> (set! rp p0))
		(<let> ((scut2 SCUT)
			(cut2   CUT))
		 (rguard
		   (</.>
		    (<with-cut> cut2 scut2 b)))))))))))))

(<define> (soft-if-f a b c) (<soft-if> (a) (b) (c)))
(set-procedure-property! soft-if-f 'argkind 'with-cut)

(define-guile-log <setup-call-cleanup-once> 
  (syntax-rules ()
    ((_ w pre action cleanup)   
     (let w ((s0   S))
       (<let-with-lr-guard> wind lguard rguard
			    ((done? #f))	     
	 (lguard
	   (</.> 
	    pre
	    (<dynwind>
	     (lambda () 
	       (error "<setup-call-cleanup> is not reentrable"))
	     (lambda (x)
	       (if (not done?)
		   (set! done #t)
		   (<wrap-s> S cleanup))))
	    (<dynwind>
	     (<lambda> ())
	     (<lambda> ()))
	    (<let> ((s0 S))
	      action
	      (if (gp-deterministic? s0 S)
		  (<and>
		   (<code> (set! done? #t))
		   clenup
		   <cut>
		   <fail>)
		  <cc>)
	      (rguard
	       (</.> <cc>))))))))))
