(define-module (logic guile-log vset)
  #:use-module (logic guile-log vlist)
  #:use-module ((logic guile-log code-load) #:select
                (set-setmap-struct!))
  #:use-module ((logic guile-log)  #:select (<and> <==> <=> <pp> <lambda>))
  #:use-module (logic guile-log umatch)
  #:use-module (ice-9 set vhashx)
  #:use-module (ice-9 set set)
  #:use-module (ice-9 set complement)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:re-export (vhashx-null)
  #:export 
  (mk-kv mk-kvx mk-kx get-k get-v mk-kvx*
   x-acons xhash
   x-assoc
   
   vset-equal?
   vset-union
   vset-intersection
   vset-difference
   vset-addition
   vset-complement
   vset-subset<
   vset-subset<=

   voset-equal?
   voset-union
   voset-intersection
   voset-difference
   voset-addition
   voset-complement
   voset-subset<
   voset-subset<=

   vsetq-equal?
   vsetq-union
   vsetq-intersection
   vsetq-difference
   vsetq-addition
   vsetq-complement
   vsetq-subset<
   vsetq-subset<=
   vsetq-member

   vosetq-equal?
   vosetq-union
   vosetq-intersection
   vosetq-difference
   vosetq-addition
   vosetq-complement
   vosetq-subset<
   vosetq-subset<=

   vsetq-in   
   vsetq-fold
   vsetq-map
   vsetq-for-each
   vsetq-empty
   vsetq-world
   vsetq->list
   vsetq->assoc
   vsetq->kvlist

   vsetv-equal?
   vsetv-union
   vsetv-intersection
   vsetv-difference
   vsetv-addition
   vsetv-complement
   vsetv-subset<
   vsetv-subset<=

   vosetv-equal?
   vosetv-union
   vosetv-intersection
   vosetv-difference
   vosetv-addition
   vosetv-complement
   vosetv-subset<
   vosetv-subset<=
   
   vsetv-in   
   vsetv-fold
   vsetv-map
   vsetv-for-each
   vsetv->list
   vsetv->assoc
   vsetv->kvlist

   vsetx-equal?
   vsetx-union
   vsetx-intersection
   vsetx-difference
   vsetx-addition
   vsetx-complement
   vsetx-subset<
   vsetx-subset<=

   vosetx-equal?
   vosetx-union
   vosetx-intersection
   vosetx-difference
   vosetx-addition
   vosetx-complement
   vosetx-subset<
   vosetx-subset<=
   
   vsetx-in   
   vsetx-fold
   vsetx-map
   vsetx-for-each
   vsetx-empty
   vsetx-world
   vsetx->list
   vsetx->assoc
   vsetx->kvlist
   ->vsetx
   vsetx-member
   xequal?
   oxequal?
   mxequal?))


(define <<set>> (@ (ice-9 set complement) <set>))

(define-inlinable (id x) x)
(define-record-type <kv>
  (mk-kv k v)
  kv?
  (k kv-k)
  (v kv-v))

(set-record-type-printer! <kv>
  (lambda (vl port) 
    (format port "~a - ~a" (kv-k vl) (kv-v vl))))


(define-syntax-rule (mk-set . l)
  (call-with-values (lambda () (make-set-from-assoc . l))
    (lambda* (#:key member
              n- = =4 u  n  -  +  <  <=
	      o= o=4 ou on o- o+ o< o<=
	      in fold map for-each empty set->list set->assoc set->kvlist
	      make-one)
       (call-with-values (lambda ()
			   (make-complementable-set 
			    member empty u n - n- = =4 set-size set? make-one))
	 (lambda* (#:key world u n c + - = < <= ->set memb #:allow-other-keys ) 
	   (define c<  < )
	   (define c<= <=)
	   (define c=  = )

	    (call-with-values (lambda ()
				(make-complementable-set 
				 member 
                                 empty ou on o- n- o= o=4 
                                 set-size set? make-one))
	      (lambda* (#:key ou on oc o+ o- = < <= #:allow-other-keys )
		(define o<  < )
		(define o<= <=)
		(define o=  = )
                (define (set->list2   x) (set->list   (->set x)))
                (define (set->assoc2  x) (set->assoc  (->set x)))
                (define (set->kvlist2 x) (set->kvlist (->set x)))
		(values c= u  n  -  +  c  c< c<=
			o= ou on o- o+ oc o< o<=
			in fold map for-each empty world
			set->list2 set->assoc2 set->kvlist2 ->set memb))))))))

(define del (list 'deleted))
(define ele (list 'nonvalue))

(define-inlinable (mk-kv* x)
  (match x
    (($ <kv> k v)
     (cons k v))
    (_
     (cons x ele))))

(define-inlinable (v-acons  kv s) (vhash-cons (car kv) (cdr kv) s))
(define-inlinable (v-delete kv s) (vhash-cons (car kv) del      s))
(define-inlinable (v-assoc  kv s) 
  (let ((kv (vhash-assoc (car kv) s)))
    (if kv
	(if (eq? (cdr kv) del)
	    #f
	    kv)
	#f)))

(define-inlinable (v-hash   kv size) (hash   (car kv) size))
(define-inlinable (v-equal? x  y)    (equal? (car x) (car y)))
(define-inlinable (v-value? kv)      (not (eq? (cdr kv) ele)))
	    
(define-values
  (vset-equal?
   vset-union
   vset-intersection
   vset-difference
   vset-addition
   vset-complement
   vset-subset<
   vset-subset<=

   voset-equal?
   voset-union
   voset-intersection
   voset-difference
   voset-addition
   voset-complement
   voset-subset<
   voset-subset<=
   
   vset-in
   vset-fold
   vset-map
   vset-for-each
   vset-empty
   vset-world
   vset->list
   vset->assoc
   vset->kvlist
   ->vset
   vset-member)
  
  (mk-set vlist-null v-assoc v-acons 
	  v-delete v-hash mk-kv* kv? car cdr vlist-length 
	  v-value? (lambda (x) #t)
	  v-equal? id id))


(define-inlinable (vq-acons  kv s) (vhash-consq (car kv) (cdr kv) s))
(define-inlinable (vq-delete kv s) (vhash-consq (car kv) del      s))
(define-inlinable (vq-assoc  kv s) 
  (let ((kv (vhash-assq (car kv) s)))
    (if kv
	(if (eq? (cdr kv) del)
	    #f
	    kv)
	#f)))

(define-inlinable (vq-hash   kv size) (hashq   (car kv) size))
(define-inlinable (vq-equal? x  y)    (eq? (car x) (car y)))
	    
(define-values
  (vsetq-equal?
   vsetq-union
   vsetq-intersection
   vsetq-difference
   vsetq-addition
   vsetq-complement
   vsetq-subset<
   vsetq-subset<=

   vosetq-equal?
   vosetq-union
   vosetq-intersection
   vosetq-differenc
   vosetq-addition
   vosetq-complement
   vosetq-subset<
   vosetq-subset<=
   
   vsetq-in
   vsetq-fold
   vsetq-map
   vsetq-for-each
   vsetq-empty
   vsetq-world
   vsetq->list
   vsetq->assoc
   vsetq->kvlist
   ->vsetq
   vsetq-member)
  
  (mk-set vlist-null vq-assoc vq-acons 
	  vq-delete vq-hash mk-kv* kv? car cdr vlist-length 
	  v-value? (lambda (x) #t)
	  vq-equal? id id))


(define-inlinable (vv-acons  kv s) (vhash-consv (car kv) (cdr kv) s))
(define-inlinable (vv-delete kv s) (vhash-consv (car kv) del      s))
(define-inlinable (vv-assoc  kv s) 
  (let ((kv (vhash-assv (car kv) s)))
    (if kv
	(if (eq? (cdr kv) del)
	    #f
	    kv)
	#f)))

(define-inlinable (vv-hash   kv size) (hashv   (car kv) size))
(define-inlinable (vv-equal? x  y)    (eqv? (car x) (car y)))
	    
(define-values
  (vsetv-equal?
   vsetv-union
   vsetv-intersection
   vsetv-difference
   vsetv-addition
   vsetv-complement
   vsetv-subset<
   vsetv-subset<=

   vosetv-equal?
   vosetv-union
   vosetv-intersection
   vosetv-difference
   vosetv-addition
   vosetv-complement
   vosetv-subset<
   vosetv-subset<=
   
   vsetv-in
   vsetv-fold
   vsetv-map
   vsetv-for-each
   vsetv-empty
   vsetv-world
   vsetv->list
   vsetv->assoc
   vsetv->kvlist
   ->vsetv
   vsetv-member)
  
  (mk-set vlist-null vv-assoc vv-acons 
	  vv-delete vv-hash mk-kv* kv? car cdr vlist-length 
	  v-value? (lambda (x) #t)
	  vv-equal? id id))


(define sfluid (make-fluid #f))

(define (s) (fluid-ref *current-stack*))
(define (xhash x size)
  (let lp ((x x))
    (umatch (#:mode -) (x)
      (#(x)
       (lp x))

      (#(x y)
       (logxor (lp x) (lp y)))

      ((x . l)
       (logxor (lp x) (lp l)))

      (_
       (cond
        ((set? x)
         (modulo (set-hash x) size))
        
        ((cset? x)
         (match x
           (($ <<set>> a b c p)
            (if p
                (modulo (set-hash c) size)
                (modulo (logxor (set-hash a) (set-hash c))
                        size)))))

	((gp-attvar-raw? x (s))
	 (let lp ((l (gp-att-data x (s))) (v 0) (fail #f))
	   (if (pair? l)
	       ((caar l)
		(s)
		(lambda () (lp '() '() #t))
		(lambda (ss pp x) (lp (cdr l) (logxor v x) #f))
		(cdar l)
		size
		"xhash")
	       (if fail
		   (hash x size)
		   v))))
	
	((procedure? x)
	 (let* ((n (procedure-name x))
		(n (if n (symbol->string n) x)))
	   (hash n size)))
	
	(else
	 (hash x size)))))))

(define (xequal?- x y)
  (define (p x)
    (if (procedure? x)
        (symbol->string (procedure-name x))
        x))
  (cond
   ((eq? x y)
    #t)   
   ((and (or (set? x) (cset? x)) (or (set? x) (cset? y)))
    (let ((s (fluid-ref sfluid)))
      (and (or (set? x) (cset? x))
           (or (set? y) (cset? y))
           (vsetx-equal? s x y))))

   ((or (pair? x) (pair? y))
    (and (pair? x) (pair? y)
         (xequal?- (car x) (car y))
         (xequal?- (cdr x) (cdr y))))
   
   ((or (vector? x) (vector? y))
    (and (vector? x) (vector? y)
         (let ((n1 (vector-length x))
               (n2 (vector-length y)))
           (and (= n1 n2)
                (let lp ((i 0))
                  (if (< i n1)
                      (and
                       (xequal?- (vector-ref x i) (vector-ref y i))
                       (lp (+ i 1)))
                      #t))))))
    
   ((and (procedure? x)  (procedure? y))
    (equal? x y))
   
   ((or (string? x) (string? y))
    (equal? (p x) (p y)))
   
   (else
    (equal? x y))))

(define (xequal? x y z w) (xequal?- x y))

(define (oxequal?- x y)
  (let ((s (fluid-ref sfluid)))
    (let ((x (gp-lookup s x))
	  (y (gp-lookup s y)))
      (if (and (set? x) (set? y))
	  (vosetx-equal? #f x y)
	  ((<lambda> () (<==> x y))
	   s (lambda () #f) (lambda (p cc) #t))))))
(define (oxequal? x y z w) (oxequal?- x y))
	       
(define (mxequal? x y z w)
  (and (oxequal?- x y) (oxequal?- z w)))

(define-record-type <kvx->
  (mk-kvx- k v data)
  kvx-?
  (k    kvx--k)
  (v    kvx--v)
  (data kvx--data))

(define (mk-kvx k v . l)
  (mk-kvx- k v l))

(set-record-type-printer! <kvx->
  (lambda (vl port) 
    (format port "#<~a -> ~a>" (kvx--k vl) (kvx--v vl))))

(define-record-type <kvx>
  (mk-kvx+ k v e? h v? o?)
  kvx?
  (k    get-k)
  (v    get-v)
  (e?   get-e?)
  (h    get-h)
  (v?   get-v?)
  (o?   get-o?))


(set-record-type-printer! <kvx>
  (lambda (vl port)
    (let ((v (get-v vl)))
      (if (eq? v ele)
	  (format port "~a" (get-k vl))
	  (format port "(~a - ~a)" (get-k vl) v)))))

(set-object-property! <kvx> 'prolog-printer 
   (lambda (lp vl advanced?)
     (let ((v (get-v vl)))
      (if (eq? v ele)
	  (format #f "~a" (lp (get-k vl)))
	  (format #f "~a-~a" (lp (get-k vl)) (lp v))))))
    
                     

(define-record-type <kx>
  (mk-kx- k data)
  kx?
  (k    kx-k)
  (data kx-data))

(define (mk-kx k . l)
  (mk-kx- k l))

(set-record-type-printer! <kx>
  (lambda (vl port) 
    (format port "#<k:~a>" (kx-k vl))))

 
(define-inlinable (x-equal?  kx1 kx2) 
  (and ((get-e? kx1) (get-k kx1) (get-k kx2) (get-v kx1) (get-v kx2))
       (eq? (get-e? kx1) (get-e? kx2))))
       
  
(define-inlinable (x-hash kx s) (get-k kx) ((get-h kx) (get-k kx) s))


;; A new thread should spur a new hastable here
(define *weak-references* (make-fluid (make-weak-key-hash-table)))

(define (k-w->val k val)
  (hash-set! (fluid-ref *weak-references*) k val))

(define-inlinable (mk-kvx* x)
  (match x
    (($ <kvx-> k v data)
     (call-with-values (lambda () (apply values data))
       (lambda* (#:key (equal? xequal?) 
		       (hash   xhash) 
		       (value? #t) 
		       (order? #t))		      
		(mk-kvx+ k v equal? hash value? order?))))
 	 
    (($ <kx> k data)
     (call-with-values (lambda () (apply values data))
       (lambda* (#:key (equal? xequal?) 
		       (hash   xhash) 
		       (value? #f) 
		       (order? #t))
		(mk-kvx+ k ele equal? hash value? order?))))

    (x
     (mk-kvx+ x ele xequal? xhash #f #t))))

(define (del-kv kv)
  (match kv
   (($ <kvx> k _ e h v? o?)
    (mk-kvx+ k del e h v? o?))))

(define-inlinable (x-acons  kv s) (vhashx-cons kv          s x-hash))
(define-inlinable (x-delete kv s) (vhashx-cons (del-kv kv) s x-hash))
(define-inlinable (x-assoc  kv s) 
  (let ((kv (vhashx-assoc kv s x-equal? x-hash)))
    (if kv
	(if (eq? (get-v kv) del)
	    #f
	    kv)
	#f)))
	    
(define-values
  (vsetx-equal?-
   vsetx-union-
   vsetx-intersection-
   vsetx-difference-
   vsetx-addition-
   vsetx-complement-
   vsetx-subset<-
   vsetx-subset<=-

   vosetx-equal?-
   vosetx-union-
   vosetx-intersection-
   vosetx-difference-
   vosetx-addition-
   vosetx-complement-
   vosetx-subset<-
   vosetx-subset<=-
   
   vsetx-in-
   vsetx-fold-
   vsetx-map-
   vsetx-for-each-
   vsetx-empty
   vsetx-world-
   vsetx->list-
   vsetx->assoc-
   vsetx->kvlist-
   ->vsetx
   vsetx-member)
  
  (mk-set vhashx-null x-assoc x-acons 
	  x-delete x-hash mk-kvx* kv? get-k get-v vhashx-length 
	  get-v? get-o?
	  x-equal? id id))


(define-syntax-rule (wrap nm1 nm2)
  (define nm1 (lambda (s . x)
		(with-fluids ((sfluid s))
		  (apply nm2 x)))))

(wrap   vsetx-equal? vsetx-equal?-)
(wrap   vsetx-union  vsetx-union-)
(wrap   vsetx-intersection vsetx-intersection-)
(wrap   vsetx-difference vsetx-difference-)
(wrap   vsetx-addition vsetx-addition-)
(wrap   vsetx-complement vsetx-complement-)
(wrap   vsetx-subset< vsetx-subset<-)
(wrap   vsetx-subset<= vsetx-subset<=-)

(wrap   vosetx-equal? vosetx-equal?-)
(wrap   vosetx-union vosetx-union-)
(wrap   vosetx-intersection vosetx-intersection-)
(wrap   vosetx-difference vosetx-difference-)
(wrap   vosetx-addition vosetx-addition-)
(wrap   vosetx-complement vosetx-complement-)
(wrap   vosetx-subset< vosetx-subset<-)
(wrap   vosetx-subset<= vosetx-subset<=-)

(wrap   vsetx-in    vsetx-in-)
(wrap   vsetx-fold vsetx-fold-)
(wrap   vsetx-map vsetx-map-)
(wrap   vsetx-for-each vsetx-for-each-)
(wrap   vsetx-world vsetx-world-)
(wrap   vsetx->list vsetx->list-)
(wrap   vsetx->assoc vsetx->assoc-)
(wrap   vsetx->kvlist vsetx->kvlist-)

(set-setmap-struct! (@@ (ice-9 set set       ) <set>)
                    (@@ (ice-9 set complement) <set>))
