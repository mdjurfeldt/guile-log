(define-module (logic guile-log paralell)
  #:use-module (logic guile-log)
  #:use-module (srfi srfi-1)
  #:use-module (logic guile-log code-load)
  #:use-module (logic guile-log dynamic-features)
  #:use-module (logic guile-log umatch)
  #:export (<pand> <pzip> f test1 test2 test3 test4 next-level
                   conjunctioneur jack-the-zipper))

(define *cc* (@@ (logic guile-log run) *cc*))

(<define> (check-bindings check key.vals)
  (<recur> lp ((l key.vals))
    (if (pair? l)
        (let* ((x (car l))
               (k (car x))
               (v (cdr x)))
          (<if> (<=> k v)
                (lp (cdr l))
                (check)))
        <cc>)))

(define level      0)
(define next-level 0)

(define-syntax-rule (m x)
  (case-lambda
    (()  x)
    ((a) (set! x a))))

(define-guile-log <pand>
  (lambda (x)
    (syntax-case x ()
      ((_ w n check data exit cc ((d pd engine code ...) ...) body ...)
       (with-syntax (((v   ...) (gen x "v"   #'(d ...)))
                     ((l   ...) (gen x "l"   #'(d ...)))
                     ((cci ...) (gen x "cci" #'(d ...)))
                     ((st  ...) (gen x "st"  #'(d ...)))
                     ((e   ...) (gen x "e"   #'(d ...)))
                     ((se  ...) (gen x "se"  #'(d ...))))
         #'(<nvar> w (+ next-level 1) (v ...)
             (let ((i level) (n next-level) (p P))
               (<let-with-lr-guard>
                wind lguard rguard
                ((chk #f)
                 (l   level)           ...
                 (pd  p)               ...
                 (st  (lambda () (p))) ...
                 (cci #f)              ...
                 (se     #f)           ...
                 (e   #f)              ...)
                (lguard
                 (</.>
                 (<dynwind>
                  (lambda x #f)
                  (lambda x
                    (set! level i)
                    (set! next-level n)))                 
                 (let* ((data   (list v ...))
                        (frame  (<newframe>))
                        (d      (vector (m e) (m cci) (m st))) ...
                        (exit   P)
                        (s      frame))
                   (let ((cc     CC))
                     (<with-s> s
                      (<code> (set! st CC))
                      (<with-cc> (lambda (s p . l)
                                   (if (cci)
                                       (apply (cci) s p l)
                                       (apply CC s p l)))
                        (<with-fail> (lambda () ((e)))
                          (<with-s> (gp-push-engine frame engine)
                            (<code> (gp-combine-push data))
                            (<code> (gp-var-set v (gp-peek-engine) S))
                            (<dynwind>
                             (lambda x
                               (set! level      (+ 1 n))
                               (set! next-level (+ 1 n)))
                             (lambda x #f))
                            (<code>
                             (set! level      (+ 1 n))
                             (set! next-level (+ 1 n)))

                            code ...
                            
                            (<dynwind>
                             (lambda x (set! l next-level))
                             (lambda x (set! next-level l)))
                            (<code>
                             (set! l next-level))
                            (<code> (set! se S))
                            (<code> (set! pd P))        
                            (<code> (gp-pop-engine))))))

                     ...
                     (<code> (set! chk check)))

                   ;; The stub below is always executed last at a backtrack
                   
                   (<dynwind>
                    (lambda x
                      (set! next-level (max l ...))
                      (set! level      i))
                    (lambda x #f))
                   (<code>
                    (set! next-level (max l ...))
                    (set! level      i))

                   (<code> (gp-combine-engines data))
                   
                   (<values> (f next) (chk data))
                   (<code> (set! chk next))
                   (rguard
                    (</.>
                     (let ((s.bindings (gp-combine-state s (list se ...))))
                       (check-bindings f (cdr s.bindings))
                       (<with-s> (car s.bindings)
                         (<with-fail> f body ...))))))))))))))))
                   

(define (recur f) (<lambda> (data) (<cc> f (recur f))))

(eval-when (eval load compile)
  (define (gen w s c)
    (map (lambda (x)
           (datum->syntax w (gensym s)))
         (syntax->datum c))))

(define-guile-log <pzip>
  (lambda (x)
    (syntax-case x ()
      ((_ w (code ...) ...)
       (with-syntax (((p  ...) (gen x "p"  #'((code ...) ...)))
                     ((d  ...) (gen x "d"  #'((code ...) ...)))
                     ((pd ...) (gen x "pd" #'((code ...) ...))))
         (with-syntax (((zarg ...)
                        (fold-right cons* '() #'(d ...) #'(pd ...))))
           #'(<pand> w n (recur (jack-the-zipper zarg ...)) data exit cc
               ((d pd (gp-make-engine (+ n 1) 100) code ...)
                ...))))))))
                                                                  

(<define> (g x n)
  (<recur> lp ((i 0) (x x))
     (if (< i n)
         (<var> (l)
            (<or> (<=> x (1 . l))
                  (<=> x (2 . l)))
            (lp (+ i 1) l))
         (<=> x '()))))

(<define> (f x n)
 (<recur> lp ((i 0))
   (if (< i n)
       (<or> (<=> x i) (lp (+ i 1))))))


(<define> (test1 x y) (<pzip> ((f x 3)) ((f y 3))))

#|
(<define> (test2 x y)
  (<pzip> ((<member> 1 x)) ((<member> 2 y))))

(<define> (test3 x y)
  (<pzip> ((g x 10)) ((g y 10))))

(<define> (test4 x y z w)
   (<pzip> ((test3 x y)) ((test3 z w))))
|#

(define jack-the-zipper
  (case-lambda
    ((d1 p1)
     (values d1 p1))      

    ((d1 p1 d2 p2)
     (let* ((exit   (make-variable (lambda ()
                                (error "not hooked in"))))
       
            (exit-1  (vector-ref d1 0))
            (exit-2  (vector-ref d2 0))
            (cc-1    (vector-ref d1 1))
            (cc-2    (vector-ref d2 1))
            (start-1 (vector-ref d1 2))
            (start-2 (vector-ref d2 2))
            (start   (lambda (s p)
                       (variable-set! cc-1 start-2)
                       (start-1 s p)))
       
            (d       (vector exit cc-2 start)))

       (variable-set! exit-1
                      (lambda () ((variable-ref exit))))
       
       (variable-set! exit-2
                      (lambda () ((variable-ref exit))))

       (variable-set! cc-1
                      (lambda (s p) (p2)))

       (values d p1)))

    ((d1 p1 d2 p2 . l)
     (call-with-values (lambda () (apply jack-the-zipper d2 p2 l))
       (lambda (d2 p2)
         (jack-the-zipper d1 p1 d2 p2))))))

(define conjunctioneur
  (case-lambda
    ((d1 p1)
     (values d1 p1))      

    ((d1 p1 d2 p2)
     (let* ((exit-1  (vector-ref d1 0))
            (exit-2  (vector-ref d2 0))
            (cc-1    (vector-ref d1 1))
            (cc-2    (vector-ref d2 1))
            (start-1 (vector-ref d1 2))
            (start-2 (vector-ref d2 2))
            (start   (lambda (s p)
                       (vector-set! cc-1 1 start-2)
                       (start-1 s p)))
            (d      (vector exit-1 cc-2 0 start)))
       
       (variable-set! exit-2
                      (lambda ()
                        (vector-set! exit-2 0 exit-1)
                        (vector-set! cc-1   1 start-2)
                        (p1)))
       
       (values d p2)))
    
    ((d1 p1 d2 p2 . l)
     (call-with-values (lambda () (apply conjunctioneur d2 p2 l))
       (lambda (d2 p2)
         (conjunctioneur d1 p1 d2 p2))))))


(define (do-the-twist-and-fail exit cc d p)
  (let ((d-exit (vector-ref d 0))
        (d-cc   (vector-ref d 1)))
    (variable-set! d-exit exit)
    (variable-set! d-cc   cc)
    (p)))
