(<define> (mk-generator tag-thunk)
  (<var> (F)
    (<=> F
	 (<lambda> ()
	   (<let> ((tag (cons 'a 'tag)))
	     (<letrec> ((h (<lambda> (tag next kk arg)
			      (<set> F (<lambda> () (kk h)))
			      (<cc> arg))))
	        (<prompt> tag (tag-thunk tag) h)))))
    (<cc> F)))

(define-syntax-paramer <yield> 
   (lambda x (error "<yield> not in define-generator")))

(define-syntax-rule (define-gl-generator (f . a) . code)
  (define (f . a)
    (lambda (tag)
      (syntax-parameterize ((<yield> (syntax-rules ()
				       ((_ s p cc val)
					(<abort> s p cc tag 
						 (<lambda> () <cc>) val)))))
        (<lambda> () . code)))))