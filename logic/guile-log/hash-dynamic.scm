(define-module (logic guile-log hash-dynamic)
  #:use-module (logic guile-log functional-database)
  #:use-module (logic guile-log vlist)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log umatch)
  #:export (define-hash-dynamic! make_hash_dynamic make_generic_hash_dynamic))

(define-inlinable (split n x)
  (let lp ((n n) (l x) (r '()))
    (if (pair? l)
	(if (= n 0)
	    (values (reverse r) l)
	    (lp (- n 1) (cdr l) (cons (car l) r)))
	(if (= n 0)
	    (values (reverse r) l)
	    (error
	     "in hash-dynamic logic, number of arguments are missmatching "
	     x)))))

(define ops
  (call-with-values
      (lambda ()
	(make-functional-dynamic-db 'fkns))
    (lambda x (list->vector x))))

(define walk (vector-ref ops 0))
   
(define (make-hash-dynamic-db n)
  (let ((t (make-fluid (cons vlist-null 0))))

    (define (on-all f)
      (let ((h (make-hash-table)))
	(vhash-fold
	 (lambda (k v s) 
	   (if (hash-ref h k #f)
	       s
	       (begin
		 (vhash-set! h k #t)
		 (f v))))
	 #f (car (fluid-ref t)))))
   
    (define (on-all-new old f)
      (let ((h     (make-hash-table))
	    (old   (car old))
	    (table (car (fluid-ref t))))
	(vhash-fold-new 
	 (lambda (k v s) 
	   (if (hash-ref h k #f)
	       s
	       (let ((v (vhash-ref table k #f)))		 
		 (hash-set! h k #t)
		 (if v (f v)))))
	 #f old table)))

    (define (mk) (make-functional-dynamic-db 'env))
    
    (define (resize table i)
      (if (and (> i 10) (> (vlist-length table) (* 2 i)))
	  (let ((h (make-hash-table)))
	    (values
	     (vhash-fold (lambda (k v s)
			   (if (hash-ref h k #f)
			       s
			       (begin
				 (hash-set! h k #t)
				 (vhash-cons k v s))))
			 vlist-null table)
	     i))
	  (values table i)))
	    
    (define-syntax-rule (mod-it key i table env ienv code ...)
      (lambda ()
	(let* ((newenv
		(let ((ienv (if env env (mk))))
		  code ...))
	       (newtable 
		(vhash-cons key newenv table)))
	  (if env
	      (resize newtable i)
	      (resize newtable (+ i 1))))))

    (define-syntax-rule (rm-it key i table env ienv code ...)
      (lambda ()
	(if env
	    (let* ((newenv
		    (let ((ienv env))
		      code ...))
		   (newtable 
		    (vhash-cons key newenv table)))
	      (resize newtable i))
	    (values table i))))
		
    (define package-it 		  
      (lambda (table i)
	(fluid-set! t (cons table i))))

    (define-syntax-rule (prelude mod-it s a env ienv post code ...)
      (let* ((co    (fluid-ref t))
	     (table (car co))
	     (i     (cdr co)))
	(call-with-values (lambda () (split n a))
	  (lambda (pre post)
	    (let* ((key (gp->scm pre s))
		   (env (vhash-ref table key #f)))
	      (call-with-values 
		  (mod-it key i table env ienv code ...)
		package-it))))))

    (define-syntax-rule (skipper p s a env pre post setter code ...)
      (let* ((co    (fluid-ref t))
	     (table (car co))
	     (i     (cdr co)))
	(call-with-values (lambda () (split n a))
	  (lambda (pre post)
	    (let* ((key (gp->scm pre s))
		   (env (vhash-ref table key #f)))
	      (if env
		  (let ((setter (lambda (x)
				  (call-with-values 
				      (lambda () 
					(let ((newtable 
					       (vhash-cons key x table)))
					  (resize newtable i)))
				    package-it)
				  x)))
				  
		    (begin code ...))
		  (p)))))))

    (define (add-l s a f c)
      (prelude mod-it s a env ienv post
	 ((vector-ref ops 1) ienv s post f c)))

    (define (add-r s a f c)
      (prelude mod-it s a env ienv post
	 ((vector-ref ops 2) ienv s post f c)))
   
    (define (walk-lr s p a f)
      (skipper p s a env pre post setter 
         ((vector-ref ops 0) pre setter env s p post f)))

    (define (rm s a f one?)
      (prelude mod-it s a env ienv post setter
	 ((vector-ref ops 3) setter ienv s f one?)))
        
    (define (compile)       #f)
    (define (env-ref)       (fluid-ref t))
    (define (env-set!  x)   (fluid-set! t x))
    (define (compile-index) #f)
    (define (truncate! xold)
      (when (not (eq? xold (fluid-ref t)))
	(on-all-new xold  (lambda (env) ((vector-ref ops 6) env #f)))
	(vhash-truncate! (car (fluid-ref t)))))
    (define (ref++)
      (vlist-refcount++ (car (fluid-ref t)))
      (on-all (lambda (env) ((vector-ref ops 7) env))))
    (define (ref-- . X)
      (vlist-refcount-- (car (fluid-ref t)))
      (on-all (lambda (env) ((vector-ref ops 8) env))))
    
    (vector add-l
	    add-r
	    rm
	    compile
	    env-ref
	    env-set!
	    compile-index
	    truncate!
	    ref++
	    ref--
	    walk-lr
	    )))


(define-syntax-rule (get-f x) (vector-ref x 2))
(define (mk-hd f setter n)
  (let ((data (make-hash-dynamic-db n)))
    (define walk-lr (vector-ref data 10))
    (define g
      (lambda (s p cc . a)
	(let ((fr (gp-newframe-choice s)))
	  (walk-lr s p a
	     (lambda (p cut a vec last?)
	       (if (not last?)
		   (let ((p (lambda () (gp-unwind fr) (p))))
		     ((get-f vec) s p cc cut a))
		   (begin
		     (gp-unwind-tail fr)
		     ((get-f vec) s p cc cut a))))))))
    (setter g)
    (set-object-property! g 'dynamic-data data)
    (set-procedure-property! g 'name f)))




(define-syntax-rule (define-hash-dynamic! n f)
  (begin
    (define f #f)
    (mk-hd 'f (lambda (x) 	
		 ;(pk 'define (object-address x))
		 (set! f x))
	   n)))

(<define> (make_hash_dynamic n f)
  (<code>
   (let ((name (procedure-name (<lookup> f)) ))
     (mk-hd name
      (lambda (x)
	(module-set! (current-module) name x))      
      (<lookup> n)))))

(<define> (make_generic_hash_dynamic n f r)
  (<=> r
       ,(let ((name (gensym (symbol->string (procedure-name (<lookup> f))))))
	  (define ret #f)
	  (mk-hd name
		 (lambda (x)
		   (set! ret x))
		 (<lookup> n))
	  ret)))
