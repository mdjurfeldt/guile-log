(define-module (logic guile-log postpone)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log umatch)
  #:use-module (ice-9 match)
  #:export (postpone-frame postpone add-postpone
	    let-with-postpone-guard
	    let-with-lr-postpone-guard
            add-action postpone-finalize))


(define *limit*     (make-fluid #f))
(define *max-limit* (make-fluid #f))
(define *maxsize*   (make-fluid #f))
(define *conts*     (make-fluid #f))
(define *n*         (make-fluid #f))
(define *trim*      (make-fluid #f))
(define *redo*      (make-fluid #f))
(define *postpone?* (make-fluid #f))

(define-syntax let-with-postpone-guard
  (lambda (x)
    (syntax-case x ()
      ((_ state wind guard ((s v) ...) code ...)
       #'(begin
	   (gp-fluid-set s v) ...
	   (call-with-values 
	       (lambda () (gp-new-postpone-level state))
	     (lambda (state wind)	       
	       (let-syntax ((guard 
			     (syntax-rules ()
			       ((_ stat codee (... ...))
				(begin
				  (gp-undo-safe-variable-guard s (- wind 1)
							       stat)
				  ...				  
				  codee (... ...))))))
			    
		 (let-syntax ((s  (make-variable-transformer
                                 (lambda (x)
                                 (syntax-case x (set!)
                                   ((set! _ w)
                                    #'(fluid-set! s w))
                                   ((_ a (... ...))
                                    #'((fluid-ref s) a (... ...)))
                                   (var
                                    (identifier? #'var)
                                    #'(fluid-ref s))))))
			      ...)
			  
		   (let () code ...))))))))))

(define-syntax let-with-lr-postpone-guard
  (lambda (x)
    (syntax-case x ()
      ((_ state wind lguard rguard ((s v) ...) code ...)
       (with-syntax (((ss ...) (reverse #'(s ...))))
         #'(begin
             (gp-fluid-set s v) ...	   
             (call-with-values
                 (lambda () (gp-new-wind-level state))
               (lambda (state wind)	       
                 (let-syntax ((lguard 
			     (syntax-rules ()
			       ((_ stat codee (... ...))
				(begin
				  (gp-undo-safe-variable-lguard s (- wind 1)
							       stat)
				  ...				  
				  codee (... ...)))))

			    (rguard 
			     (syntax-rules ()
			       ((_ stat codee (... ...))
				(begin
				  (gp-undo-safe-variable-rguard ss (- wind 1)
								stat)
				  ...				  
				  (gp-undo-safe-variable-guard ss (- wind 1)
							       stat)
				  ...
				  codee (... ...))))))
			    
		 (let-syntax ((s  (make-variable-transformer
                               (lambda (x)
                                 (syntax-case x (set!)
                                   ((set! _ w)
                                    #'(fluid-set! s w))
                                   ((_ a (... ...))
                                    #'((fluid-ref s) a (... ...)))
                                   (var
                                    (identifier? #'var)
                                    #'(fluid-ref s))))))
			      ...)
			  
	       (let () code ...)))))))))))

(define (add-postpone c) (fluid-set! *conts* (cons c (fluid-ref *conts*))))

(define (rs s fl v . l)
  (let ((n (fluid-ref *n*)))
    (fluid-set! fl v)
    (if (pair? l)
        (gp-fluid-set! *n* (car l))
        (gp-fluid-set! *n* (+ 1 n)))))
  
(define (postpone-frame s p cc limit fact maxsize . maxretry)
  (define max-nr-of-retries (if (pair? maxretry) (car maxretry) maxsize))
  (gp-fluid-set *max-limit* (* 10 maxsize))
  (gp-fluid-set *maxsize* maxsize)
  (let-with-lr-postpone-guard s wind lguard rguard ((*n*      0) 
						    (*limit*  limit)
						    (*trim*   #f)
						    (*conts* '()))
      (gp-fluid-set *postpone?* #t)
      (lguard s
	      ((</.> (<or> <cc> (handle-postpones wind limit
						  fact maxsize max-nr-of-retries
						  `())))
	       s p cc))))


(define (post s p cc)
  (rs s *conts* 
      `((0 ,(gp-store-state s) ,cc) ,@(fluid-ref *conts*) s))
  (p))

(define (finalize l)
  (let lp ((l l))
    (if (pair? l)
        (begin
          ((list-ref (car l) 4))
          (lp (cdr l))))))

(define (handle-postpones s p cc wind limit fact maxsize maxretry lold)
  (define (get-min l)
    (let loop ((l l) (s 1000000))
      (if (pair? l)
          (loop (cdr l) (min s (caar l)))
          s)))
  (define (get-max l)
    (let loop ((l l) (s -1))
      (if (pair? l)
          (loop (cdr l) (max s (caar l)))
          s)))
  (let* ((l   (fluid-ref *conts*)))
    #;(format #t "nold : ~a, nnew : ~a, nfault ~a~%"
	    (length lold) (length l)
	    (let lp ((i 0) (x l))
	      (if (pair? x) 
		  (if (< (caar x) 0)
		      (lp (+ i 1) (cdr x))
		      (lp i       (cdr x)))
		  i)))
    (if (and (and-map (lambda (x) (< (car x) 0)) l)
	     (eq? (length l) (length lold)))
	
	(begin
	  (do-actions s)
	  (if (and (and-map (lambda (x) (< (car x) 0)) (fluid-ref *conts*))
		   (eq? (length (fluid-ref *conts*)) (length lold)))
	      (begin (finalize (fluid-ref *conts*)) (p))
	      (handle-postpones s p cc wind limit fact maxsize maxretry lold)))
	
	(if (pair? l)
	    (let ((n (length l))
		  (x-min (get-min l))
		  (x-max (get-max l)))
	      (if (> n maxretry)
		  (begin                
		    (gp-fluid-set! *trim* #t)
		    (if (fluid-ref *redo*)
			(set! l lold)
			(set! l (trim l maxretry)))
		    (do-gc)))
	  
	      (gp-fluid-set! *limit* (+ limit fact))
	      (gp-fluid-set! *n*      0)
	      (gp-fluid-set! *conts* '())
	      (do-actions s)
	      (do-on-all wind  s l 
			 (lambda () 
			   (handle-postpones s p cc wind 
					     (+ limit fact) fact maxsize 
					     maxretry l))
			 #t))
	    (p)))))

(define *actions* '())
(define (add-action f) (set! *actions* (cons f *actions*)))
(define (do-actions s)
  (let lp ((as *actions*) (s s))
    (if (pair? as)
        (lp (cdr as) (<wrap-s> (car as) s))
        s)))

(define (do-on-all wind s l f first)
  (define (do-on-all-1 ss x f)
    (match x
      ((v s cc _)
       (if (< (fluid-ref *n*) (fluid-ref *max-limit*))
           (begin
             (gp-restore-wind s wind)
             (cc ss (lambda () (begin (f)))))           
           (begin
             (let* ((l (trim (fluid-ref *conts*) 
                             (/ (fluid-ref *max-limit*) 2))))
               (do-gc)
               (rs ss *conts* l (/ (fluid-ref *max-limit*) 2))
               (gp-restore-wind s wind)
               (cc ss (lambda () (begin (f))))))))))
             
  (if (pair? l)
      (do-on-all-1 s (car l) (lambda () (do-on-all wind s (cdr l) f #f)))
      (f)))
              

(define (trim l maxsize)
  (let* ((fs (sort l (lambda (x y) (< (car x) (car y)))))
         (fs (let loop ((fs fs) (i 0) (r '()))
               (if (= i maxsize)
                   (reverse r)
                   (match fs
                     ((x . u)
                      (loop u (+ i 1) (cons x r)))
                     (()
                      r))))))
    fs))

(define *i* 0)
(define (do-gc)
  (define (f) (cdar (cddr (gc-stats) )))
  #f
#;
  (if (>= *i* 10)
      (begin (set! *i* 0)
             (let ((pre (f)))
               (gc)))
      (set! *i* (+ *i* 1))))

(define (pa k x) x)

(define postpone-finalize 
  (case-lambda 
   ((ss p cc f)
    (postpone-finalize ss p cc 0 -1 f))
   ((ss p cc v s f)
    (let ((v (gp-lookup v ss)))
      (if (not (number? v)) v)
      (if (not (fluid-ref *limit*)) *limit*)
      (if (< v (fluid-ref *limit*))
	(if (< (fluid-ref *n*) (fluid-ref *max-limit*))
	    (cc ss p)
	    (let* ((l (trim (fluid-ref *conts*) 
			    (/ (fluid-ref *max-limit*) 2))))            
	      (do-gc)
	      (rs ss *conts* l (/ (fluid-ref *max-limit*) 2))
	      (cc ss p)))
	(begin
	  (if (or (not (fluid-ref *redo*)) 
		  (< (fluid-ref *n*) 
		     (+ 10 (fluid-ref *maxsize*))))
	      (rs ss *conts*
                  (let ((c `(,s ,(gp-store-state ss) ,cc (lambda () (f c)))))
                    (pa 'to `(,c ,@(pa 'from (fluid-ref *conts*))))))
	      (let ((ll (trim (cons `(,s ,(gp-store-state ss) ,cc)
				    (fluid-ref *conts*)) 
			      (/ (fluid-ref *max-limit*) 2))))
		(rs ss *conts* ll (/ (fluid-ref *max-limit*) 2))))
	  (p)))))))

(define postpone 
  (case-lambda 
   ((ss p cc)
    (postpone ss p cc 0 -1))
   ((ss p cc v s)
    (let ((v (gp-lookup v ss)))
      (if (not (number? v)) v)
      (if (not (fluid-ref *limit*)) *limit*)
      (if (< v (fluid-ref *limit*))
	(if (< (fluid-ref *n*) (fluid-ref *max-limit*))
	    (cc ss p)
	    (let* ((l (trim (fluid-ref *conts*) 
			    (/ (fluid-ref *max-limit*) 2))))            
	      (do-gc)
	      (rs ss *conts* l (/ (fluid-ref *max-limit*) 2))
	      (cc ss p)))
	(begin
	  (if (or (not (fluid-ref *redo*)) 
		  (< (fluid-ref *n*) 
		     (+ 10 (fluid-ref *maxsize*))))
	      (rs ss *conts*
		 (pa 'to `((,s ,(gp-store-state ss) ,cc (lambda () #f))
                           ,@(pa 'from (fluid-ref *conts*)))))
	      (let ((ll (trim (cons `(,s ,(gp-store-state ss) ,cc)
				    (fluid-ref *conts*)) 
			      (/ (fluid-ref *max-limit*) 2))))
		(rs ss *conts* ll (/ (fluid-ref *max-limit*) 2))))
	  (p)))))))

