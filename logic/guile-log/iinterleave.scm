(define-module (logic guile-log iinterleave)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log postpone)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:use-module ((logic guile-log umatch)
		#:select 
		(gp-rebased-level-ref gp-restore-wind gp-store-state
				      gp-make-var gp-lookup))
  #:use-module (logic guile-log dynamic-features)
  #:replace (delay sleep)
  #:export (<or-ii> <or-ii-z> <or-2-ii>
		    <and-ii> <and-ii-z> <and-ii-top> <and-s>
		    init-machines new-machine new-machine-zero
                    skip-machine get-machine-wind
		    with-new-machine with-new-machine0
                    with-new-pot-machine new-machine-same-wind
		    init-pot-machines new-pot-machine or-ii-f
		    init-schedule-data schedule reschedule <and+> 
		    with-new-schedule reschedule-1* reschedule-1**
		    <once-ii> alternative  no-alternative))

(define restore gp-restore-wind)
(define gp-restore-wind
  (lambda (s w) (restore s w)))

(define machine
  (make-fluid (cons
	       (lambda (x) (error "no minikanren machine defined"))
	       0)))


(<wrap> add-fluid-dynamics machine)

(<define> (new-machine . l)
   (fluid-guard-dynamic-object machine)
   (<apply> machine-base  l))

(<define> (new-machine-same-wind . l)
   (fluid-guard-dynamic-object machine)
   (<apply> machine-base-same-wind l))

(<define> (new-machine-zero . l)
   (fluid-guard-dynamic-object machine)
   (<apply> machine-zero  l))

(<define> (init-machines . l)
   (state-guard-dynamic-object machine)
   (<apply> new-machine l))

(<define> (new-pot-machine pot . l)
   (fluid-guard-dynamic-object machine)
   (<apply> pot-machine-base pot l))

(<define> (init-pot-machines pot . l)
   (state-guard-dynamic-object machine)
   (<apply> new-pot-machine pot l))

(define (get-machine-wind)   (fluid-ref machine))
(define (set-machine-wind x) (fluid-set! machine x))
(define (get-machine      x) (car x))
(define *seq* 1)

(define (inv/seq-potential s . l)
  (let ((i *seq*))
    (set! *seq* i)
    (/ 1 i)))
(define (seq-potential s . l)
  (let ((i *seq*))
    (set! *seq* i)
    i))

(define *x* (gp-make-var 1))
(<define> (new-disjunction)
   (<set> *x* (+ 1 (<lookup> *x*))))

(define *i* 1)
(define (disj-pot s depth)
  (let* ((a (gp-lookup *x* s))
         (r (+ (* 1000 a) *i*)))
    (set! *i* (+ *i* 1))
    r))
      
    


(define (dep-potential s d . l) d)
(define (inv/dep-potential s d . l) (/ 1 (+ d 1)))

(define (null-potential s . l) 0)

(define (null-sum       v h t) 0)

(define (sum            v h t)
  (define (get x)
    (if (vector? x)
	(vector-ref x 2)
	(cdr x)))

  (let lp ((m v) (l h))
    (if (pair? l)
	(lp (min m (get (car l))) (cdr l))
	(let lp ((m m) (l t))
	  (if (pair? l)
	      (lp (min m (get (car l))) (cdr l))
	      m)))))

(<define-guile-log-rule> (with-new-machine old : code)
  (<let> ((old (get-machine-wind)))
    (new-machine (get-machine old))
    (code)
    (fluid-guard-dynamic-object machine)
    (<code> (set-machine-wind old))))

(<define-guile-log-rule> (with-new-machine0 old : code)
  (<let> ((old (get-machine-wind)))
    (new-machine-zero)
    (code)
    (fluid-guard-dynamic-object machine)
    (<code> (set-machine-wind old))))
    
(<define-guile-log-rule> (with-new-pot-machine old : code l ...)
  (<let*> ((old (get-machine-wind)))
    (new-machine l ... (get-machine old))
    (code)
    (fluid-guard-dynamic-object machine)
    (<code> (set-machine-wind old))))

(<define-guile-log-rule> (skip-machine mac) 
   (<code> ((get-machine mac) S 'skip)))

(<define> (pot-machine-base pot . l)
   (if (pair? l)
       (machine-base* pot sum (car l) #f)
       (machine-base* pot sum #f      #f)))

(<define> (machine-base . l)
   (if (pair? l)
       (machine-base* null-potential null-sum (car l) #f)
       (machine-base* null-potential null-sum #f      #f)))

(<define> (machine-base-same-wind . l)
   (if (pair? l)
       (machine-base* null-potential null-sum (car l) #t)
       (machine-base* null-potential null-sum #f      #t)))

(<define> (machine-zero . l)
   (if (pair? l)
       (machine-base* null-potential sum (car l) #f)
       (machine-base* null-potential sum #f      #f)))

(define-inlinable (machine? x) (variable? x))
(define-inlinable (get-mac  x) (variable-ref x))
(define-inlinable (wrap-machine x) (make-variable x))

(<define> (machine-base* potential sum previous same-wind?)
   (<let-guard> wind guard ((depth 0) (path '()) (pot 1e20) (head '())
                            (tail '()) (token #f))
    (guard
	(<lambda> ()
               (<let*>
                ((wi   (if same-wind? (- wind 1) wind))
		 (m    (make-hash-table))
                 (i    0)
                 (add  (lambda (x)
                         (let ((r (hashq-ref m x)))
                           (if r
                               r
                               (let ((r i))
                                 (set! i (+ i 1))
                                 (hashq-set! m x r)
                                 r)))))
                 (print (lambda (h t tok)
                          (pretty-print
                           (let lp ((h h) (t t) (tok tok))
                             (cons* (add tok) ':
                               (let lp2 ((l (append h (reverse t))))
                                 (if (pair? l)
                                   (let ((x (car l)))
                                     (if (vector? x)
                                         (cons (lp (vector-ref x 0)
                                                   (vector-ref x 1)
                                                   (vector-ref x 3))
                                               (lp2 (cdr l)))
                                         (cons (add (car l))
                                               (lp2 (cdr l)))))
                                   '())))))))
                 
                 (pack (lambda (stop-depth)
                         (define not-stop?
                           (if (pair? head) 
                               (vector? (car head))
                               (if (pair? tail)
                                   (vector? (car (reverse tail)))
                                   #f)))                        
                         (let lp ((h head) (t tail) (p path) (v pot) (d depth)
                                  (tok token))
                           (if (or (null? p) (if (integer? stop-depth)
                                                 (<= d stop-depth)
                                                 (eq? stop-depth tok)))
                               (begin
                                 (set! tail t)
                                 (set! head h)
                                 (set! path p)
                                 (set! pot  v)
                                 (set! depth d)
                                 (set! token tok)
                                 (if previous (previous 'next)))
                               
                               (let* ((x      (car p))
                                      (hnew   (vector-ref x 0)) 
                                      (tnew   (vector-ref x 1))
                                      (toknew (vector-ref x 3)))
                                 (if (and (not tok)
                                          (null? tnew)
                                          (null? (cdr hnew)))
                                     (if (or (pair? h) (pair? t))
                                         (lp h t (cdr p) v (- d 1) toknew)
                                         (lp (cdr hnew) tnew (cdr p) 
                                             (sum 1e40 hnew tnew)
                                             (- d 1) toknew))
                                    
                                     (if (or (pair? h) (pair? t))
                                         (let ((l (vector h t v tok)))
                                           (lp (cdr hnew)
                                               (cons* l tnew) (cdr p) 
                                               (sum v hnew tnew)
                                               (- d 1) toknew))
                                        
                                         (lp (cdr hnew) tnew (cdr p) 
                                             (sum 1e40 hnew tnew)
                                             (- d 1) toknew))))))))
                 
                 (alone?
                  (lambda (token-in)
                    (let lp ((h head) (t tail) (p path) (tok token) (first? #t))
                      (if (> (+ (length h) (length t)) (if first? 0 1))
                          (begin
                            #f)
                          (if (or (null? p) (eq? tok token-in))
                              (begin
                                #t)
                              (let* ((x      (car p))
                                     (hnew   (vector-ref x 0)) 
                                     (tnew   (vector-ref x 1))
                                     (toknew (vector-ref x 3)))
                                (lp hnew tnew (cdr p) toknew #f)))))))
                
		(pick-new
		 (lambda (lp h t p v d a b tok fail)
		   (if (pair? t)
		       (lp (reverse t) '() p v d a b tok)
		       (if (pair? p)
			   (let* ((x   (car p))
				  (h   (vector-ref x 0))
				  (t   (vector-ref x 1))
                                  (tok (vector-ref x 3))) 
			     (lp (cdr h) t
				 (cdr p) (sum 1e40 h t) (- d 1) p #f tok))
			   (fail)))))

		(cycle
		 (lambda (lp h t p v d a b tok)
		   (if (pair? h)
		       (lp (cdr h) (cons (car h) t) p v d a b tok)
		       (pick-new lp h t p v d a b tok
				 (lambda () (error "empty base"))))))
							  
				
		
		(dive (lambda (s)
			(let lp ((h head) (t tail) (p path) (v pot)
				 (d depth)
				 (m pot) (q #f) (tok token))
			  (cond			   
			   ((pair? h)
			    (let ((r (car h)))
			      (cond
			       ((machine? r)
				(let ((newmac (get-mac r)))
				  (if (newmac s 'empty?)
				      (lp (cdr h) t p v d m q #f)
				      (begin
					(set! head (cdr head))
					(set! tail (cons r tail))
					(newmac s 'dive)))))

			       ((eq? r q)
				(lp h t p m d m #f tok))

			       ((vector? r)
				(let ((vnew (vector-ref r 2)))
				  (if (or (<= vnew v))
				      (begin
					(lp (vector-ref r 0) (vector-ref r 1)
					    (cons (vector h t 0 tok) p)
					    vnew (+ d 1) 0 #f
                                            (vector-ref r 3)))
				      (if q
					  (cycle lp h t p v d
						 (min m vnew) q tok)
					  (cycle lp h t p v d vnew r tok)))))
				      
			       (else
				(if (or (<= (cdr r) v))
				    (begin
				      (set! head (cdr h))
				      (set! tail t)
				      (set! path p)
				      (set! depth d)
				      (set! pot  (sum 1e20 head tail))
                                      (set! token tok)
				      (if (and (null? (cdr h)) (null? t))
					  ((car r) #t)
					  ((car r) #f)))
				    (if q
					(cycle lp h t p v d (min m (cdr r)) q
                                               tok)
					(cycle lp h t p v d (cdr r) r tok)))))))
				
			   (else
			    (pick-new lp h t p v d m q tok (lambda () P)))))))

                (t!   (lambda (tok l)
                        (if (pair? (cdr l))
                            (cadr l)
                            #f)))
                
		(next (lambda (s)
			(pack 0)
			(dive s)))

		(mac (lambda (s kind . l)
		       (case kind
			 ((empty?)
			  (and (null? path) (null? head) (null? tail)))
			 
			 ((state)
                          (list path head tail))

                         ((alone?)
                          (alone? (car l)))
                         
			 ((pk)
			  (potential s #t))
                         
			 ((dive)
			  (dive s))
			 
			 ((simple)
			  (if (pair? head)
			      (pair? (car head))
			      (if (pair? tail)
				  (pair? (car (reverse tail)))
				  #f)))

			 ((depth)
			  depth)

			 ((pack)
			  (pack (car l)))

			 ((remove-level)
			  (let ((ret (append head (reverse tail))))
			    (set! head '())
			    (set! tail '())
			    ret))

			 ((next)
			  (if (pair? l) 
			      (let ((p (potential s depth)))
				(set! pot (min pot p))
				(set! tail (cons (cons (car l) p)
						 tail))))
			  (next s))
			 
			 ((pre)
			  (if (pair? l) 
			      (let ((p (potential s depth)))
				(set! pot (min pot p))
				(set! head (cons (cons (car l) p)
						 head))))
			  (next s))

			 ((prepend)
			  (let ((p (potential s depth)))
			    (set! pot (min pot p))
			    (set! head (cons (vector (map (lambda (x)
							    (cons x p))
							  (car l))
						     '() p (t! token l))
					     head))))

                         ((prepend-p)
			  (let ((p (car l)))
			    (set! pot (min pot p))
			    (set! head (cons (vector (map (lambda (x)
							    (cons x p))
							  (car l))
						     '() p (t! token l))
					     head))))

			 
			 ((append)
			  (let ((p (potential s)))
			    (set! pot (min pot p))
			    (set! tail (cons (vector (map (lambda (x)
							    (cons x p))
							  (car l))
						     '() p (t! token l))
					     tail))))

                         ((append-p)
			  (let ((p (car l)))
			    (set! pot (min pot p))
			    (set! tail (cons (vector (map (lambda (x)
							    (cons x p))
							  (car l))
						     '() p (t! token l))
					     tail)))))))
		
		(p   (lambda () ((mac 0 'next)))))

	   (<code>
	    (fluid-set! machine (cons mac wi)))
	   (<with-fail> p <cc>))))))

(define-guile-log <and-ii>
  (syntax-rules ()
    ((and-ii w)           (<and> w))
    ((and-ii w x)         (<and> w (and-ii* (<lambda> () x))))
    ((and-ii w x y l ...) (<and> w (<and-ii> (and-ii* (<lambda> () x)
						      (<lambda> () y))
					     l ...)))))

(define-guile-log <and-ii-top>
  (syntax-rules ()
    ((and-ii w)           (<and> w))
    ((and-ii w x)         (<and> w (and-ii-top* (<lambda> () x))))
    ((and-ii w x y l ...) (<and> w (<and-ii> (and-ii-top* (<lambda> () x)
						      (<lambda> () y))
					     l ...)))))

(define-guile-log <and-ii-z>
  (syntax-rules ()
    ((and-ii w)           (<and> w))
    ((and-ii w x)         (<and> w (and-ii-z* (<lambda> () x))))
    ((and-ii w x y l ...) (<and> w (<and-ii> (and-ii-z* (<lambda> () x)
						      (<lambda> () y))
					     l ...)))))


(define and-ii* 
  (<case-lambda>
    (()     <cc>)
   
    ((x)
     (let* ((mac.wind  (get-machine-wind))
            (mac       (car mac.wind))
            (depth     (+ (mac S 'depth) 1))	      
            (cc        CC)
            (wind      (cdr mac.wind))
            (state     (gp-store-state S))
            (f         (lambda (last?)
                         (lambda ()
                           (gp-restore-wind
                            state (gp-rebased-level-ref wind))
                           (x S P cc)))))
       (<code> (mac S 'prepend (list f)))
       (<ret> ((mac S 'next)))))

    ((x y)
     (let* ((mac.wind  (get-machine-wind))
            (mac       (car mac.wind))
            (depth     (+ (mac S 'depth) 1))
            (wind      (cdr mac.wind))
            (state     (gp-store-state S))
            (token     (list '*))
	    (pnext     (lambda ()
                         (if (mac S 'alone? token)
                             (P)
                             ((mac S 'next)))))
	    (cc2       (lambda (s p)
			 (let* ((state (gp-store-state s))
				(pnext (lambda ()
                                         (if (mac s 'alone? token)
                                             (P)
                                             ((mac s 'next)))))
				(f1     (lambda (last?)
                                          (lambda ()                                            
                                            (gp-restore-wind
                                             state
                                             (gp-rebased-level-ref wind))
                                            (p))))
				(f2     (lambda (last?)
                                          (lambda ()
                                            (CC s pnext)))))
			   (mac s 'prepend (list f2 f1))
			   ((mac s 'dive)))))
	    
            (cc        (lambda (s p)
                         (new-disjunction s p
                            (lambda (s p)                              
                              (let* ((state (gp-store-state s))
				     (pnext (lambda ()
                                              (if (mac s 'alone? token)
                                                  (P)
                                                  ((mac s 'next)))))
                                     (f1 (lambda (last?)
                                           (lambda ()
                                             (gp-restore-wind
                                              state
                                              (gp-rebased-level-ref wind))
                                             (p))))
                                     
                                     (f2 (lambda (last?)
                                           (lambda ()
                                             (y s pnext cc2)))))

                                (mac s 'prepend (list f2 f1))
                                ((mac s 'dive))))))))

       (<code>
        (mac S 'prepend
             (list (lambda (last?) (lambda () (x S pnext cc))))
             token))
       
       (<ret> ((mac S 'dive)))))))

(define and-ii-top* 
  (<case-lambda>
    (()     <cc>)
   
    ((x)
     (let* ((mac.wind  (get-machine-wind))
            (mac       (car mac.wind))
            (depth     (+ (mac S 'depth) 1))	      
            (cc        CC)
            (wind      (cdr mac.wind))
            (state     (gp-store-state S))
            (f         (lambda (last?)
                         (lambda ()
                           (gp-restore-wind
                            state (gp-rebased-level-ref wind))
                           (x S P cc)))))
       (<code> (mac S 'pack 0))
       (<code> (mac S 'prepend (list f)))
       (<ret> ((mac S 'next)))))

    ((x y)
     (let* ((mac.wind  (get-machine-wind))
            (mac       (car mac.wind))
            (depth     (+ (mac S 'depth) 1))
            (wind      (cdr mac.wind))
            (state     (gp-store-state S))
            (cc        (lambda (s p)
                         (new-disjunction s p
                            (lambda (s p)                              
                              (let* ((state (gp-store-state s))
                                     (f (lambda (last?)
                                          (lambda ()
                                            (gp-restore-wind
                                             state
                                             (gp-rebased-level-ref wind))                                            (y s p CC)))))
                                (mac S 'pack 0)
                                (mac S 'prepend (list f))
                                ((mac s 'next)))))))
                              
            (f         (lambda (last?)
                         (lambda ()
                           (gp-restore-wind
                            state (gp-rebased-level-ref wind))
                           (x S P cc)))))
       (<code> (mac S 'pack 0))
       (<code> (mac S 'prepend (list f)))
       (<ret> ((mac S 'next)))))))

(define and-ii-z* 
  (<case-lambda>
    (()     <cc>)
   
    ((x)
     (let* ((mac.wind  (get-machine-wind))
            (mac       (car mac.wind))
            (depth     (+ (mac S 'depth) 1))	      
            (cc        CC)
            (wind      (cdr mac.wind))
            (state     (gp-store-state S))
            (f         (lambda (last?)
                         (lambda ()
                           (gp-restore-wind
                            state (gp-rebased-level-ref wind))
                           (x S P cc)))))
       (<code> (mac S 'prepend-p (list f) 1))
       (<ret> ((mac S 'next)))))

    ((x y)
     (let* ((mac.wind  (get-machine-wind))
            (mac       (car mac.wind))
            (depth     (+ (mac S 'depth) 1))
            (wind      (cdr mac.wind))
            (state     (gp-store-state S))
            (cc        (lambda (s p)
                         (new-disjunction s p
                            (lambda (s p)                              
                              (let* ((state (gp-store-state s))
                                     (f (lambda (last?)
                                          (lambda ()
                                            (gp-restore-wind
                                             state
                                             (gp-rebased-level-ref wind))                                            (y s p CC)))))
                                (mac S 'prepend-p (list f) 1)
                                ((mac s 'next)))))))
                              
            (f         (lambda (last?)
                         (lambda ()
                           (gp-restore-wind
                            state (gp-rebased-level-ref wind))
                           (x S P cc)))))
       (<code> (mac S 'prepend-p (list f) 1))
       (<ret> ((mac S 'next)))))))

(define-guile-log <or-ii>
  (syntax-rules ()
    ((<or-ii> w)
     (<and> w <fail>))
    ((<or-ii> w x) 
     (<and-ii> w x))
    ((<or-ii> w x ...) (<and> w (or-ii* (<lambda> () x) ...)))))

(define-guile-log <or-ii-z>
  (syntax-rules ()
    ((<or-ii> w)
     (<and> w <fail>))
    ((<or-ii> w x) 
     (<and-ii-z> w x))
    ((<or-ii> w x ...) (<and> w (or-ii-z* (<lambda> () x) ...)))))


(define-guile-log <or-2-ii>
  (syntax-rules ()
    ((<or-ii> w)
     (<and> w <fail>))
    ((<or-ii> w x) 
     (<and> w x))
    ((<or-ii> w x y) 
     (<and> w (or-ii* (<lambda> () x) (<lambda> () y))))
    ((<or-ii> w x y . l) 
     (<and> w (or-ii* (<lambda> () x)
		      (<lambda> () (<or-2-ii> y . l)))))))

(define *depth* (gp-make-var 0))
(<define> (or-ii* . l)
  (<let*> ((state     (gp-store-state S))
	   (mac.wind  (get-machine-wind))
	   (wind      (cdr mac.wind))
	   (mac       (car mac.wind))
	   (pnext     (lambda () ((mac S 'next))))
	   (pdive     (lambda () ((mac S 'dive))))
	   (mk (lambda (f)
		 (lambda (last?)
		   (if (not last?)
		       (lambda ()
			 (gp-restore-wind
			  state (gp-rebased-level-ref wind))
			 (f S pnext CC))
		       (lambda ()
			 (gp-restore-wind
			  state (gp-rebased-level-ref wind))
			 (f S P CC))))))
		       
	   (l2    
	    (let lp ((l l) (n 0))
	      (match l
		(() '())
		((f . l)
		 (cons (mk f)		
		       (lp l (+ n 1))))))))
      (<with-s> (car state)
        (<code> 
          (mac S 'prepend l2 #f))
	(<ret> ((mac S 'next))))))

(<define> (or-ii-z* . l)
  (<let*> ((state     (gp-store-state S))
	   (mac.wind  (get-machine-wind))
	   (wind      (cdr mac.wind))
	   (mac       (car mac.wind))
	   (pnext     (lambda () ((mac S 'next))))
	   (pdive     (lambda () ((mac S 'dive))))
	   (mk (lambda (f)
		 (lambda (last?)
		   (if (not last?)
		       (lambda ()
			 (gp-restore-wind
			  state (gp-rebased-level-ref wind))
			 (f S pnext CC))
		       (lambda ()
			 (gp-restore-wind
			  state (gp-rebased-level-ref wind))
			 (f S P CC))))))
		       
	   (l2    
	    (let lp ((l l) (n 0))
	      (match l
		(() '())
		((f . l)
		 (cons (mk f)		
		       (lp l (+ n 1))))))))
      (<with-s> (car state)
        (<code> 
          (mac S 'prepend-p l2 1))
	(<ret> ((mac S 'next))))))

(define (or-ii-f s x y p)
  (let* ((state     (gp-store-state s))
	 (mac.wind  (get-machine-wind))
	 (wind      (cdr mac.wind))
	 (mac       (car mac.wind))
	 (pnext     (lambda () ((mac s 'next))))
	 (pdive     (lambda () ((mac s 'dive))))
	 (mk (lambda (f)
	       (lambda (last?)
		 (lambda ()
		   (gp-restore-wind
		    state (gp-rebased-level-ref wind))
		   (if (not last?)
		       (f pnext)
		       (f p))))))
		       
	 (l2    
	  (list (mk x) (mk y))))
    (mac (car state) 'prepend l2 #f)
    ((mac (car state) 'next))))
      

(define-syntax-rule (get-depth)
  (gp-lookup *depth* S))

(define (delay s p cc)
  (let* ((state     (gp-store-state s))
	 (mac.wind  (get-machine-wind))
	 (wind      (cdr mac.wind))
	 (mac       (car mac.wind))
	 (cc
	  (lambda (last?)
	    (lambda ()
	      (gp-restore-wind
	       state (gp-rebased-level-ref wind))
	      (cc s p)))))
      ((mac (car state) 'pre cc))))


(define empty-schedule (vector '() '() '() #f))
(define schedule-data (gp-make-var empty-schedule))
(<define-guile-log-rule> (set-schedule x) (<set> schedule-data x))



(define-syntax with-schedule-data
  (syntax-rules (:)
    ((with-schedule-data q arg : p h t o code ...)
     (<lambda> arg
       (<let*> ((w q)
		(p (vector-ref w 0))
		(h (vector-ref w 1))
		(t (vector-ref w 2))
		(o (vector-ref w 3)))
	  code ...)))

    ((with-schedule-data arg : p h t o code ...)
     (with-schedule-data (<lookup> schedule-data) arg : p h t o code ...))))

(<define-guile-log-rule> (with-schedule-data-o x _ l ...)
  ((with-schedule-data x () : l ...)))

(define *alternatives* (gp-make-var #f))
(define-syntax-rule (alternatives?) (<lookup> *alternatives*))
(<define-guile-log-rule> (alternative)
   (<set> *alternatives* #t))

(<define-guile-log-rule> (no-alternative)
   (<set> *alternatives* #f))

(<define-guile-log-rule> (alternative)
   (<set> *alternatives* #t))

(<define-guile-log-rule> (gen-schedule p h t o)
   (<set> schedule-data (vector p h t o)))

(<define> (init-schedule-data)
   (gen-schedule '() '() '() #f))

(<define-guile-log-rule> (new-schedule x)			 
      (gen-schedule '() '() '() x))

(<define-guile-log-rule> (with-new-schedule lam)
  (<let> ((old (<lookup> schedule-data)))
    (new-schedule old)
    (lam)
    (reschedule #t)
    (<set> schedule-data old)))

(<define-guile-log-rule> (set-schedule-data-from-data o)
  (with-schedule-data-o o : p h t o
    (gen-schedule p h t o)))

(define old-schedule (gp-make-var #f))
(define (schedule? x) (variable? x))
(define (unpack-schedule k) (variable-ref k))
(define prepend-sch
  (with-schedule-data (f) : p h t o
    (gen-schedule p (cons (vector f '()) h) t o)))

(define-inlinable (empty-space)
  (with-schedule-data () : p h t o
    (if (and (eq? p '()) (eq? t '()) (eq? h '()) (not o))
	<cc>
	<fail>)))

(define prepend-sch-1
  (with-schedule-data (f) : p h t o
    (gen-schedule p (cons f h) t o)))

(define append-sch
  (with-schedule-data (f) : p h t o
    (gen-schedule p h (cons (vector f '()) t) o)))

(define (empty-schedule? k)
  (and (null? (vector-ref k 0)) 
       (null? (vector-ref k 1)) 
       (null? (vector-ref k 2))))

(define schedule
   (with-schedule-data (always?) : p h t o
      (<recur> lp ((p p) (h h) (t t))
	 ;(<pp> `(schedule ,p ,h ,t))
	 (if (pair? h)
	     (<let> ((k (car h)))
	        (cond
		 ((schedule? k)
		  (<let> ((k  (unpack-schedule k)))
		    (if (empty-schedule? k)
			(lp p (cdr h) t)
			(<and>
			 (gen-schedule p (cdr h) (cons k t) o)
			 (set-schedule k)
			 (schedule always?)))))

		 ((vector? k)
		  (<let> ((hh (vector-ref k 0))
			  (tt (vector-ref k 1))
			  (pp (cons (vector (cdr h) t) p)))
		      (lp pp hh tt)))
		 (else
		  (gen-schedule p (cdr h) t o)
		  (no-alternative)
		  (k)
		  (if always?
		      (reschedule always?)
		      <cc>))))
	      (if (pair? t)
		  (lp p (reverse t) '())
		  (if (pair? p)
		      (<let*> ((k  (car p))
			       (pp (cdr p))
			       (hh (vector-ref k 0))
			       (tt (vector-ref k 1)))
			(lp pp hh tt))
		      (if o
			  (set-schedule-data-from-data o)
			  <cc>)))))))

(define up-the-schedule		     
  (with-schedule-data () : p h t o
     (<recur> lp ((p p) (h h) (t t))
	;(<pp> `(up ,p ,h ,t))
	(if (pair? p)
	    (<let*> ((k  (car p))
		     (pp (cdr p))
		     (hh (vector-ref k 0))
		     (tt (cons (vector h t) (vector-ref k 1))))
		(lp pp hh tt))
	    (if o
		(<and>
		  (set-schedule o)
		  (up-the-schedule))
		(gen-schedule p h t o))))))

(<define> (reschedule always?)
   ;(<pp> 'reschedule)
   (if (alternatives?)
       (up-the-schedule)
       <cc>)
   (schedule always?))

(define (reschedule-1*)
  (<lambda> ()
   (<let> ((cc CC))
      (<if> ((empty-space))
	    <cc>
	    (if (alternatives?)
		(<and>
		 (prepend-sch-1   
		  (<lambda> () (<ret> (cc S P))))
		 (reschedule #f))	   
		<cc>)))))

(define (reschedule-1**)
  (<lambda> ()
    (<if> ((empty-space))
	  <cc>
	  (<and> (alternative) ((reschedule-1*)) (no-alternative)))))
	   
(define-guile-log <and+>
  (syntax-rules ()
    ((<and+> w)   (<and> w <cc>))
    ((<and+> w f) (<and> w f))
    ((<and+> w f ...)
     (<and> w (and+ (<lambda> () f) ...)))))

(<define> (and+ . fs)
  (prepend-sch fs) 
  (schedule #f))
     
#|
Our version of once,
|#

(<define> (once-ii-machine x)
 (<let*> ((mac.wind  (get-machine-wind))
	  (mac       (car mac.wind))
	  (token     (list 'once))
	  (cc        (lambda (s p)
		       (mac s 'pack token)
		       (mac s 'remove-level)
		       (CC s p)))
	  (f         (lambda (bla) 
		       (lambda ()
			 ((<lookup> x) S P cc)))))
   (<code> (mac S 'prepend (list f) token))
   (<ret> ((mac S 'dive)))))

(<define> (once-ii x)
  (let ((p P))
    (once-ii-machine
      (<lambda> ()      
        (with-new-schedule
           (<lambda> ()
             (<with-fail> p (x))
             (reschedule #t)))))))

(<define-guile-log-rule> (<once-ii> x) (once-ii (<lambda> () x)))

(set! (@@ (logic guile-log tools) new-machine)  new-machine)
(set! (@@ (logic guile-log functional-database) or-ii-f) or-ii-f)
