(define-module (logic guile-log functional-database)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log ck)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log type)
  #:use-module (logic guile-log dynlist)
  #:use-module (logic guile-log indexer)
  #:use-module (logic guile-log inheritance)
  #:use-module ((ice-9 set complement) #:select (cset? get-set-hash))
  #:use-module ((ice-9 set set) #:select (set?))
  #:use-module (srfi srfi-60)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:use-module (logic guile-log vlist)
  #:re-export (get-index-set)
  #:export (define-dynamic! dynamic-push dynamic-prepend dynamic-compile
             dynamic-remove dynamic-env-ref dynamic-env-set! 
             dynamic-refcount++ dynamic-refcount-- dynamic-truncate!
             dynamic-compile-index tr-dynamic->
             dynamic-compile-index dynamic?
             dynamic-abolish mk-dyn define-dynamic
	     define-dynamic-f
             <with-dynamic-functions>
             <push-dynamic>
             <append-dynamic>
             <clause-dynamic> 
	     <clause-dynamic-3> 
             <retract-dynamic>
             <retract-all-dynamic>
             <lambda-dyn>
	     and-tag or-tag not-tag predicate-tag +tag -tag
	     <lambda-dyn-extended> 
	     <<case-lambda-dyn>>
	     <<case-lambda-dyn-extended>>
	     <<case-lambda-dyn-extended2>>
	     <<case-lambda-dyn-combined>> 
	     extended
	     interleaved
	     extended_interleaved
	     make_dynamic make_generic_dynamic
	     make-functional-dynamic-db))

(define or-ii-f       'not-define-or-ii-f)
(define tags          (get-index-tags))
(define and-tag       (list-ref   tags 0))
(define or-tag        (cons 'or  'tag))
(define =..tag        (cons '=.. 'tag))
(define not-tag       (list-ref tags 1))
(define predicate-tag (list-ref tags 2))
(define +-tag         (list-ref tags 4))
(define --tag         (list-ref tags 3))
(define set-tag       (list-ref tags 5))
(define (=..tag? x)        (eq? x =..tag))
(define (or-tag? x)        (eq? x or-tag))
(define (set-tag? x)       (eq? x set-tag))
(define (and-tag? x)       (eq? x and-tag))
(define (not-tag? x)       (eq? x not-tag))
(define (predicate-tag? x) (eq? x predicate-tag))
(define (+? x)             (eq? x +-tag))
(define (-? x)             (eq? x --tag))

(define set-unify #f)
(define (get-set-hash* x) (get-set-hash x))
(define (attr x)
  (if (and (pair? x) (not (pair? (cdr x))))
      (if (eq? (caar x) set-unify)
          (cdar x)
          x)
      x))
      
(define (has-not-predicate-tag a)
  (let lp ((a a))
    (if (pair? a)
	(if (eq? 'predicate-tag (syntax->datum (cdar a)))
	    #f
	    (lp (cdr a)))
	#t)))

(define (syntax-assoc stx l)
  (let lp ((l l))
    (if (pair? l)
	(let ((tag (caar l)))
	  (if (bound-identifier=? tag stx)
	      (car l)
	      (lp (cdr l))))
	#f)))

(define type? (lambda (x) (eq? x Type)))
      
#|
Action    dynlist array   indexed
          o       o       o
add/del   *       o       o      
===========================
          * 
run       *       vlist   *
===========================
          *       vlist   *
add/run   *       vlist   *
===========================
|#

(define do-print #f)
(define (pp . x) 
  (if do-print
      (pretty-print (syntax->datum x)))
  (car (reverse x)))
(define (ppp a x) (pretty-print (list a (syntax->datum x))) x)

(define (make-indexer) (vector #f #f 0 #f 0 vlist-null vlist-null))

(define-inlinable (get-car v)
  (vector-ref v 0))
(define-inlinable (get-cdr v)
  (vector-ref v 1))
(define-inlinable (get-vars v)
  (vector-ref v 2))
(define-inlinable (get-atoms v)
  (vector-ref v 3))
(define-inlinable (get-all v)
  (vector-ref v 4))
(define-inlinable (get-in-strings v)
  (vector-ref v 5))
(define-inlinable (get-strings v)
  (vector-ref v 6))

(define-inlinable (add-vars-all! v x y)
  (vector-set! v 2 x)
  (vector-set! v 4 y))

(define-inlinable (add-vars-all v x y)
  (vector
   (vector-ref v 0)
   (vector-ref v 1)
   x
   (vector-ref v 3)
   y
   (vector-ref v 5)
   (vector-ref v 6)))

(define-inlinable (add-atoms-all! v x y z w)
  (vector-set!  v 3 x)
  (vector-set!  v 4 y)
  (vector-set!  v 5 z)
  (vector-set!  v 6 w))

(define-inlinable (add-atoms-all v x y z w)
  (vector
   (vector-ref v 0)
   (vector-ref v 1)
   (vector-ref v 2)
   x
   y
   z
   w))

(define-inlinable (add-car-cdr-all v x y z)
  (vector
   x
   y
   (vector-ref v 2)
   (vector-ref v 3)
   z
   (vector-ref v 5)
   (vector-ref v 6)))

(define-inlinable (add-car-cdr! v x y)
  (vector-set! v 0 x)
  (vector-set! v 1 y))

(define-inlinable (add-all! v x)
  (vector-set! v 4 x))

(define-inlinable (dlink-car dlink !?)
  (let ((r (get-car dlink)))
    (if r 
        r
        (if !?
            (make-indexer)
            #f))))

(define-inlinable (dlink-car! dlink v)
  (vector-set! dlink 0 v))

(define-inlinable (dlink-cdr! dlink v)
  (vector-set! dlink 1 v))

(define-inlinable (dlink-cdr dlink !?)
  (let ((r (get-cdr dlink)))
    (if r 
        r
        (if !?
            (make-indexer)           
            #f))))

(define-syntax-parameter make-empty  
  (syntax-rules () 
    ((_) 0)))

(define-syntax-parameter make-all
  (syntax-rules () 
    ((_ tag) (- (ash tag 1) 1))))


(define-syntax-parameter union
  (syntax-rules () 
    ((_ x y) 
     (logior x y))))

(define-syntax-parameter difference
  (syntax-rules () 
    ((_ x y) 
     (let ((r (logand x (lognot y))))
       r))))

(define-syntax-parameter intersection
  (syntax-rules () 
    ((_ x y) 
     (logand x y))))

(define-syntax-parameter member?
  (syntax-rules ()
    ((member? x y)
     (> (logand x y) 0))))

(define-syntax-parameter realize-set
  (syntax-rules () 
    ((_ x) x)))

(define (make-bitmap-tag) 1)
(define-inlinable (next-bitmap-tag tag) (ash tag 1))

;; car cdr atoms vars
(begin
  (define (get-nlist-from-atom a dlink)
    (let* ((r1 (get-atoms dlink))
	   (r2 (get-in-strings dlink))
	   (r3 (get-strings dlink)))
      (cond 
       ((string? a)
	(if r2
	    (let ((x (if r2 (vhash-assoc a r2) r2))
		  (y (if r3 (vhash-assoc a r3) r3)))
	      (if x
		  (values r1 r2 r3 (cdr x) (if y (cdr y) (make-empty)))
		  (values r1 r2 r3 (make-empty) (if y (cdr y) (make-empty)))))
	    (if r3
		(let ((x (if r3 (vhash-assoc a r3) r3)))
		  (values r1 vlist-null r3 (make-empty) 
			  (if x (cdr x) (make-empty))))
		(values r1 vlist-null vlist-null (make-empty) (make-empty)))))

       ((procedure? a)
	(let ((s (symbol->string (procedure-name a))))
	  (if r1
	      (let ((x (if r1 (vhash-assoc a r1) r1))
		    (y (if r2 (vhash-assoc s r3) r2)))
		(if x
		    (values r1 r2 r3 (cdr x) (if y (cdr y) (make-empty)))
		    (values r1 r2 r3 (make-empty) (if y (cdr y) (make-empty)))))
	      (if r3
		  (let ((x (if r3 (vhash-assoc s r3) r3)))
		    (values vlist-null 
			    r2 r3
			    (make-empty)
			    (if x (cdr x) (make-empty))))
		  (values vlist-null 
			  r2 
			  vlist-null
			  (make-empty)
			  (make-empty))))))

       ((or (cset? a) (set? a))
        (get-nlist-from-atom (get-set-hash* a) dlink))
       
       (else
	(if r1
	    (let ((x (if r1 (vhash-assoc a r1) r1)))
	      (if x
		  (values r1 r2 r3 (cdr x)      #f)
		  (values r1 r2 r3 (make-empty) #f)))
	    (values vlist-null r2 r3 (make-empty) #f))))))
	    

  (define (get-nlist-from-atom! a dlink)
    (let* ((r1 (get-atoms dlink))
	   (r2 (get-in-strings dlink))
	   (r3 (get-strings dlink)))
      (define (make-empty) #f)
      (cond 
       ((string? a)
	(if r2
	    (let ((x (if r2 (vhash-assoc a r2) r2))
		  (y (if r3 (vhash-assoc a r3) r3)))
	      (if x
		  (values r1 r2 r3 (cdr x) (if y (cdr y) (make-empty)))
		  (values r1 r2 r3 (make-empty) (if y (cdr y) (make-empty)))))
	    (if r3
		(let ((x (if r3 (vhash-assoc a r3) r3)))
		  (values r1 vlist-null r3 (make-empty) 
			  (if x (cdr x) (make-empty))))
		(values r1 vlist-null vlist-null (make-empty) (make-empty)))))

       ((procedure? a)
	(let ((s (symbol->string (procedure-name a))))
	  (if r1
	      (let ((x (if r1 (vhash-assoc a r1) r1))
		    (y (if r3 (vhash-assoc s r3) r3)))
		(if x
		    (values r1 r2 r3 (cdr x) (if y (cdr y) (make-empty)))
		    (values r1 r2 r3 (make-empty) (if y (cdr y) (make-empty)))))
	      (if r3
		  (let ((x (if r3 (vhash-assoc s r3) r3)))
		    (values vlist-null 
			    r2 r3
			    (make-empty)
			    (if x (cdr x) (make-empty))))
		  (values vlist-null 
			  r2 
			  vlist-null
			  (make-empty)
			  (make-empty))))))

       ((or (cset? a) (set? a))
        (get-nlist-from-atom! (get-set-hash* a) dlink))

       (else
	(if r1
	    (let ((x (vhash-assoc a r1)))
	      (if x
		  (values r1 r2 r3 (cdr x)      #f)
		  (values r1 r2 r3 (make-empty) #f)))
	    (values vlist-null r2 r3 (make-empty) #f))))))

  (define (compile-inh s y theory)
    (match y
      (((? set-tag? x) a b c)
       (let ((a    (compile-inh s a theory))
	     (c    (compile-inh s c theory))
	     (tree (assoc=>tree theory (car b)))
	     (all  (let lp ((l (car b)) (ff 0))
		     (if (pair? l)
			 (lp (cdr l) (logior ff (cdar l)))
			 ff))))
	 (list x a (cons (car b) tree) c all)))
      ((x . l)
       (let ((xx (compile-inh s x theory))
	     (ll (compile-inh s l theory)))
	 (if (and (eq? x xx) (eq? l ll))
	     y
	     (cons xx ll))))
      (x
       (cond
        ((or (cset? x) (set? x))
         (get-set-hash* x))
        
        ((vector? x)
         (let ((h (compile-inh s (get-car x) theory))
               (t (compile-inh s (get-cdr x) theory)))
           (if (and (eq? h (get-car x)) (eq? t (get-cdr x)))
               x
               (begin (add-car-cdr! x h t) x))))
        
        (else
         x)))))
	 

  (define dive
    (case-lambda 
     ((dlink)
      (match dlink
	(((or (? +?) (? -?) (? set-tag?)
	      (? and-tag?) (? not-tag?) (? predicate-tag?)) x . u)
	 (dive x))
	(x x)))
     ((dlink tag)
      (match dlink
        ((,tag . l) 
	 dlink)
	(((or (? +?) (? -?) 
	      (? and-tag?) (? not-tag?) (? predicate-tag?)) x . u)
	 (dive x tag))
	(else
	 (case tag
	   ((set-tag)       (list set-tag dlink (cons '() #f) (make-indexer)))
	   ((+-tag)         (list +-tag dlink (make-indexer)))
	   ((--tag)         (list --tag dlink (make-indexer)))
	   ((and-tag)       (list and-tag dlink (make-indexer) (make-indexer)))
	   ((not-tag)       (list not-tag dlink (make-indexer) 0))
	   ((predicate-tag) (list predicate-tag dlink))))))))

  (define dive!
    (case-lambda 
     ((dlink tag)
      (match dlink
        (((? (lambda (x) (eq? x tag))) . l)
	 (cons #f dlink))

	(((or ,+-tag ,--tag ,and-tag ,not-tag ,predicate-tag ,set-tag) 
	  (and (set! s) x) . u)
	 (let ((r (dive! x tag)))
	   (if (car r)
	       (begin
		 (s (cdr r))
		 (cons #f (cdr r)))
	       r)))
	(else
	 (cons #t
	   (cond 
	    ((eq? tag set-tag)
	     (list set-tag dlink (cons '() #f) (make-indexer)))

	    ((eq? tag +-tag)
	     (list +-tag dlink (make-indexer)))

	    ((eq? tag --tag)
	     (list --tag dlink (make-indexer)))

	    ((eq? tag and-tag) 
	     (list and-tag 
		   dlink (make-indexer) (make-indexer)))

	    ((eq? tag not-tag) 
	     (list not-tag dlink (make-indexer) 0))

	    ((eq? tag predicate-tag) 
	     (list predicate-tag dlink)))))))))
	   

  (define (index-remove s tag e dlink)
    (if dlink
        (match e
	  (((? =..tag?) x)
	   (index-remove s (vector x) tag dlink))
	  	  
	  (((? or-tag?) . l)
	   (let lp ((l l) (dlink (dive dlink)))
	     (if (pair? l)
		 (lp (cdr l) (index-remove s tag (car l) dlink))
		 dlink)))

	  (((? and-tag?) x y)
	   (match (dive dlink and-tag)
	     (((? and-tag?) a b c)
	      (list and-tag a 
		    (index-remove s tag x b) 
		    (index-remove s tag y c)))))
	  
	  (((? not-tag?) x)
	   (match (dive dlink and-tag)
	     (((? not-tag?) a b v)
	      (list not-tag a (index-remove s tag x b)
		    (difference v tag)))))

	  (((? predicate-tag?) pred)
	   (match (dive dlink predicate-tag)
	      (((? predicate-tag?) x . l)
	       (let lp ((r '()) (l l))
		 (match l
		  (((,pred . p) . u)
		   (let ((pp (difference p tag)))
		     (if (= pp 0)
			 (if (and (null? u) (null? r))
			     x
			     (cons* predicate-tag x (append r u)))
			 (cons* predicate-tag (cons pred pp) (append r u)))))
		  ((x . u)
		   (lp (cons x r) u)))))))

          (#(ee ...)
           (if (= (length ee) 1)
               (index-remove s tag (car ee) (dive dlink))
               (index-remove s tag ee (dive dlink))))
	   
          ((x . l)
	   (let ((dlink (dive dlink)))
             (call-with-values 
		 (lambda () (index-remove s tag x (dlink-car dlink #t)))
	       (lambda (dlink-car) 
		 (call-with-values 
		     (lambda () (index-remove s tag l (dlink-cdr dlink #t)))
		   (lambda (dlink-cdr) 
		     (add-car-cdr-all dlink dlink-car dlink-cdr
				      (difference (get-all dlink) tag))))))))

          (a
           (cond
            ((or (set? a) (cset? a))
             (index-remove s tag (get-set-hash* a) dlink))
            
            ((gp-attvar-raw? a s)
             (let ((l (sort (gp-att-data a s)
                            (lambda (x y)
                              (< (object-address (gp-lookup
                                                  (car (gp-lookup x s)) s))
                                 (object-address (gp-lookup
                                                  (car (gp-lookup y s)) s)))))))
                                                  
               (index-remove s tag (attr l) dlink)))
            
            (else        
             (let ((dlink (dive dlink)))
               (if (gp-var? a s)
                   (add-vars-all dlink 
                                 (difference (get-vars dlink) tag)
                                 (difference (get-all  dlink) tag))
                   (call-with-values (lambda () (get-nlist-from-atom a dlink))
                     (lambda (r1 r2 r3 k1 k2)
                       (cond 
                        ((procedure? a)
                         (let ((s (symbol->string (procedure-name a))))
                           (add-atoms-all dlink 
                                          (vhash-cons a (difference k1 tag) r1)
                                          (difference (get-all dlink) tag)
                                          r2
                                          (vhash-cons s (difference k2 tag)
                                                      r3))))
                        ((string? a)		     
                         (add-atoms-all dlink 
                                        r1
                                        (difference (get-all dlink) tag)
                                        (vhash-cons a (difference k1 tag) r2)
                                        (vhash-cons a (difference k2 tag) r3)))
                        (else		     
                         (add-atoms-all dlink 
                                        (vhash-cons a (difference k1 tag) r1)
                                        (difference (get-all dlink) tag)
                                        r2 r3)))))))))))
	  
	#f))
    
  (define (bitmap-indexer-add s e f dlink get-set)
    (match e      
       (((? =..tag?) x)
	(bitmap-indexer-add s (vector x) f dlink get-set))
       
       (((? type?) x (set . data))
	(match (dive dlink set-tag)
	  (((? set-tag?) other set-data data-path)
	   (list set-tag other
		 (cons (cons (cons set f) (car set-data)) #f)
		 (bitmap-indexer-add s data f data-path get-set)))))

       (((? or-tag?) . l)
	(let lp ((l l) (dlink (dive dlink)))
	  (if (pair? l)
	      (lp (cdr l) (bitmap-indexer-add s (car l) f dlink get-set))
	      dlink)))

       (((? and-tag?) x y)
	(match (dive dlink and-tag)
	  (((? and-tag?) a b c)
	   (list and-tag a 
		 (bitmap-indexer-add s x f b get-set) 
		 (bitmap-indexer-add s y f c get-set)))))
	  
       (((? not-tag?) x)
	(match (dive dlink not-tag)
	  (((? not-tag?) a b v)
	   (list not-tag a (bitmap-indexer-add s x f b get-set)
		 (logior v f)))))

       (((? predicate-tag?) pred)
	(match (dive dlink predicate-tag)
	  (((? predicate-tag?) x . l)
	   (let lp ((r '()) (l l))
	     (match l
	       (((pred . p) . u)
		(let ((pp (union p f)))
		  (cons* predicate-tag x (append r u))))
	       ((x . u)
		(lp (cons x r) u))
	       (()
		(cons* predicate-tag x (cons pred f) r)))))))
       
       ((x . l)
        (let ((dlink (dive dlink)))
          (call-with-values 
              (lambda () (bitmap-indexer-add
                          s x f (dlink-car dlink #t) get-set))
            (lambda (dlink-car) 
              (call-with-values 
                  (lambda () (bitmap-indexer-add
                              s l f (dlink-cdr dlink #t) get-set))
                (lambda (dlink-cdr) 
                  (add-car-cdr-all dlink dlink-car dlink-cdr
                                   (union (get-all dlink) f))))))))

       (#(ee ...)
        (let ((dlink (dive dlink)))
          (if (= (length ee) 1)
              (bitmap-indexer-add s (car ee) f dlink get-set)
              (bitmap-indexer-add s ee f dlink get-set))))

       (a
        (cond
         ((or (set? a) (cset? a))
          (bitmap-indexer-add s (get-set-hash* a)
                              f dlink get-set))
         
         ((gp-attvar-raw? a s)
          (let ((l (sort (gp-att-data a s)
                         (lambda (x y)
                           (< (object-address (gp-lookup
                                               (car (gp-lookup x s)) s))
                              (object-address (gp-lookup
                                               (car (gp-lookup y s)) s)))))))
                                                  
            (bitmap-indexer-add s (attr l) f dlink get-set)))

         (else        
          (let ((dlink (dive dlink)))
            (if (gp-var? a s)
                (add-vars-all dlink 
                              (union f (get-vars dlink))
                              (union f (get-all dlink)))
                
                (call-with-values (lambda () (get-nlist-from-atom a dlink))
                  (lambda (r1 r2 r3 k1 k2)
                    (cond
                     ((procedure? a)		
                      (let ((s (symbol->string (procedure-name a))))
                        (add-atoms-all dlink                             
                                       (vhash-cons a (union k1 f) r1)
                                       (union f (get-all dlink))
                                       r2
                                       (vhash-cons s (union k2 f) r3))))
		
                     ((string? a)		
                      (add-atoms-all dlink                             
                                     r1
                                     (union f (get-all dlink))
                                     (vhash-cons a (union k1 f) r2)
                                     (vhash-cons a (union k2 f) r3)))
		
                     (else
                      (add-atoms-all dlink                             
                                     (vhash-cons a (union k1 f) r1)
                                     (union f (get-all dlink))
                                     r2 r3))))))))))))

  (define (pu e dlink x)
    ;(pk e dlink x)
    x)

  (define (bitmap-indexer-add! s e f dlink get-set)
    (match  (bitmap-indexer-add!- s e f dlink get-set)
      ((_ . a) a)))

  (define (bitmap-indexer-add!- s e f dlink get-set)
    (pu e dlink
    (match e
       (((? =..tag?) x)
	(bitmap-indexer-add!- s (vector x) f dlink get-set))

       (((? or-tag?) e)
	(bitmap-indexer-add!- s e f dlink get-set))

       (((? and-tag?) e)
	(bitmap-indexer-add!- s e f dlink get-set))

       (((? or-tag?) . l)
	(let lp ((l l))
	  (if (pair? l)
	      (begin
		(bitmap-indexer-add!- s (car l) f (dive dlink) get-set)
		(lp (cdr l)))))
	(cons #f dlink))
	    
       (((? and-tag?) x y)
	(match (dive! dlink and-tag)
	  ((#f . ((? and-tag?) a b c))
	   (bitmap-indexer-add!- s x f b get-set) 
	   (bitmap-indexer-add!- s y f c get-set)
	   (cons #f #f))
	  ((and xx (#t . ((? and-tag?) a b c)))
	   (bitmap-indexer-add!- s x f b get-set) 
	   (bitmap-indexer-add!- s y f c get-set)
	   xx)))
	  
       (((? not-tag?) x)
	(match (dive! dlink not-tag)
	  ((#f . ((? not-tag?) a b (and (set! sv) v)))
	   (sv (logior v f))
	   (list not-tag a (bitmap-indexer-add!- s x f b get-set))
	   (cons #f dlink))
	  ((and xx (#t . ((? not-tag?) a b (and (set! sv) v))))
	   (sv (logior v f))
	   (bitmap-indexer-add!- s x f b get-set)
	   xx)))

       (((? type?) x (set . data))
	(match (dive! dlink set-tag)
	  ((#f . ((? set-tag?) other (and (set! s) set-data) data-path))
	   (s (cons
	       (cons (cons (get-set set) f) (car set-data))
	       #f))
	   (bitmap-indexer-add!- s data f data-path get-set)
	   (cons #f dlink))
	  
	  ((and xx (#t . ((? set-tag?) other (and (set! s) set-data)
			  data-path)))
	   (s (cons
	       (cons (cons (get-set set) f) (car set-data))
		 #f))
	   (bitmap-indexer-add!- s data f data-path get-set)
	   xx)))
	  

       (((? predicate-tag?) pred)
	(match (dive! dlink predicate-tag)
	  ((#f . ((? predicate-tag?) x . (and l (set! ss))))
	   (let lp ((l l))
	     (match l
	       ((((? (lambda (x) (eq? x pred))) . (and p (set! s))) . u)
		(s (union p f)))
	       ((x . u)
		(lp u))
	       (()
		(ss (cons (cons pred f) l)))))
	   (cons #f #f))
	  ((and xx (#t . ((? predicate-tag?) x . (and l (set! ss)))))
	   (let lp ((l l))
	     (match l
	       ((((? (lambda (x) (eq? x pred))) . (and p (set! s))) . u)
		(s (union p f)))
	       ((x . u)
		(lp u))
	       (()
		(ss (cons (cons pred f) l)))))
	   xx)))


      ((x . l)
       (let ((dlink (dive dlink)))
	 (let ((dlcar (dlink-car dlink #t)))
	   (match (bitmap-indexer-add!-
		   s x f dlcar get-set)
	     ((#t . a)
	      (dlink-car! dlink a))
	     ((#f . a)
	      (if (not (get-car dlink))
		  (dlink-car! dlink a)))))

	 (let ((dlcdr (dlink-cdr dlink #t)))
	   (match (bitmap-indexer-add!-
		   s l f dlcdr get-set)
	     ((#t . a)
	      (dlink-cdr! dlink a))
	     ((#f . a)
	      (if (not (get-cdr dlink))
		  (dlink-cdr! dlink a)))))


	 (add-all! dlink (union (get-all dlink) f))

	 (cons #f dlink)))

      (#(ee ...)
       (let ((dlink (dive dlink)))
	 (if (= (length ee) 1)
	     (bitmap-indexer-add!- s (car ee) f dlink get-set)
	     (bitmap-indexer-add!- s ee       f dlink get-set))))
       
      (a
       (cond
        ((or (set? a) (cset? a))
         (bitmap-indexer-add!- s (get-set-hash* a)
                               f dlink get-set))

        ((gp-attvar-raw? a s)
         (let ((l (sort (gp-att-data a s)
                        (lambda (x y)
                          (< (object-address (gp-lookup
                                              (car (gp-lookup x s)) s))
                             (object-address (gp-lookup
                                              (car (gp-lookup y s)) s)))))))
           
           (bitmap-indexer-add!- s (attr l) f dlink get-set)))

       
        (else        
         (let ((dlink (dive dlink)))
           (if (gp-var? a s)
               (add-vars-all! dlink 
                              (union f (get-vars dlink))
                              (union f (get-all dlink)))
                                    
               (call-with-values (lambda () (get-nlist-from-atom! a dlink))
                 (lambda (r1 r2 r3 v1 v2)
                   (with-fluids ((init-block-size 128))
                     (cond 
                      ((procedure? a)
                       (let ((s (symbol->string (procedure-name a))))
                         (add-atoms-all! dlink
                                         (if v1
                                             (vhash-set! a (union f v1) r1)
                                             (vhash-set! a f r1))
                                         (union f (get-all dlink))
                                         r2
                                         (if v2
                                             (vhash-set! s (union f v2) r3)
                                             (vhash-set! s f r3)))))
		     
                      ((string? a)
                       (add-atoms-all! dlink
                                       r1
                                       (union f (get-all dlink))
                                       (if v1
                                           (vhash-set! a (union f v1) r2)
                                           (vhash-set! a f r2))
                                       (if v2
                                           (vhash-set! a (union f v2) r3)
                                           (vhash-set! a f r3))))
		     
                      (else
                       (add-atoms-all! dlink
                                       (if v1
                                           (vhash-set! a (union f v1) r1)
                                           (vhash-set! a f r1))
                                       (union f (get-all dlink))
                                       r2 r3))))))))
         (cons #f dlink)))))))
      

(define (get-fs-from-atoms a dlink)
  (let ((r (get-atoms dlink)))
    (if r
        (let ((w (vhash-assoc a r)))
          (if w 
              (cdr w) 
              (make-empty)))
        (make-empty))))

(define (bits->list bits)
  (let lp ((bits bits) (n 0) (res '()))
    (let ((d (first-set-bit bits)))
      (if (>= d 0)
          (let ((n (+ n d)))
            (lp (ash bits (- (+ d 1))) (+ n 1) (cons n res)))
          res))))
          
(define (vector->vlist a)
  (let ((n (vector-length a)))
    (let lp ((i 0) (r vlist-null))
      (if (< i n)
          (lp (+ i 1) 
              (with-fluids ((init-block-size (+ n n)))
                           (vlist-cons (vector-ref a i) r)))
          (cons n r)))))

(define (dynlist->vlist ar dyn)
  (if ar
      ar
      (fold-dynlist-rl
       vlist-cons
       dyn
       vlist-null)))

(define (my-list-add ar x)
  (if ar
      (with-fluids ((init-block-size 32))
	(if (vector? ar)
            (let ((n.vl (vector->vlist ar)))
              (cons (+ (car n.vl) 1) (vlist-cons x (cdr n.vl))))
            (if (pair? ar)
                (cons (+ (car ar) 1) 
                      (vlist-cons x (cdr ar)))
                (cons (+ (vlist-length ar) 1) 
                      (vlist-cons x ar)))))
      #f))

(define-syntax-rule (get-t x) (vector-ref x 0))
(define-syntax-rule (get-a x) (vector-ref x 1))
(define-syntax-rule (get-f x) (vector-ref x 2))
(define-syntax-rule (get-c x) (vector-ref x 3))
(define-syntax-rule (get-g x) (vector-ref x 4))
(define-syntax-rule (set-a x v) (vector-set! x 1 v))
(define-syntax-rule (set-f x v) (vector-set! x 2 v))
(define-syntax-rule (set-c x v) (vector-set! x 3 v))

(define-syntax-rule (get-tag    x) (vector-ref x 0))
(define-syntax-rule (get-dyn    x) (vector-ref x 1))
(define-syntax-rule (get-ar     x) (vector-ref x 2))
(define-syntax-rule (get-li     x) (vector-ref x 3))
(define-syntax-rule (get-theory x) 
  (and (>= (vector-length x) 5)
       (vector-ref x 4)))
(define delayers (@@ (logic guile-log code-load) *delayers*))

(define* (make-functional-dynamic-db #:optional (raw? #f))
  (define (make-1)
    (vector (make-bitmap-tag) (make-dynlist) #f #f))
  (define (make) (cons (make-1) (make-1)))
  
  (define (p x) (if (and (procedure? x)
			 (eq? (car (procedure-minimum-arity x)) 0))
		    (x)
		    x))

  (let ((env (if raw? (make) (make-fluid (make)))))
    (define (env-ref)    (fluid-ref env))
    (define (env-set! e) (fluid-set! env e))
    
    (define (add-raw s a f c e g)
      (let* ((t    (get-tag    e))
             (d    (get-dyn    e))
             (ar   (get-ar     e))
             (l    (get-li     e))
	     (set  (get-theory e))
	     (a    (if l (p a) a))
	     (f    (if l (p f) f))
	     (c    (if l (p c) c))
             (data (vector t a f c g)))
	(values
	 (vector (next-bitmap-tag t)
		 (dynlist-add d  data)
		 (my-list-add ar data) 
		 (if l (bitmap-indexer-add s a t l (mk-get-set set)) l)
		 set)
	 data)))
    
    (define (xxx-vlist vlist-truncate! x)
      (if (vlist? x)
          (vlist-truncate! x)))

    (define (xxx-hash vhash-truncate! x)
      (when x
        (xxx-hash vhash-truncate! (get-car x))
        (xxx-hash vhash-truncate! (get-cdr x))
        (let ((a (get-atoms x)))
          (when a (vhash-truncate! a)))))
    
    (define (xxx vlist-truncate! vhash-truncate!)
      (let* ((e  (fluid-ref env))
             (el (car e))
             (er (cdr e)))
        (xxx-hash  vhash-truncate! (get-li el))
        (xxx-hash  vhash-truncate! (get-li er))
        (xxx-vlist vlist-truncate! (get-ar el))
        (xxx-vlist vlist-truncate! (get-ar er))))
    
    (define (xxx-f e vlist-truncate! vhash-truncate!)
      (let* ((el (car e))
             (er (cdr e)))
        (xxx-hash  vhash-truncate! (get-li el))
        (xxx-hash  vhash-truncate! (get-li er))
        (xxx-vlist vlist-truncate! (get-ar el))
        (xxx-vlist vlist-truncate! (get-ar er))))

    (define (truncate! x) 
      (xxx vlist-truncate!  vhash-truncate!))

    (define (ref++)     (xxx vlist-refcount++ vlist-refcount++))
    (define (ref--)     (xxx vlist-refcount-- vlist-refcount--))

    (define (truncate!-f e x) 
      (xxx-f e vlist-truncate!  vhash-truncate!))

    (define (ref++-f e)     (xxx-f e vlist-refcount++ vlist-refcount++))
    (define (ref---f e)     (xxx-f e vlist-refcount-- vlist-refcount--))

    (define (add-l s a f c g)
      (let ((e (fluid-ref env)))
	(call-with-values (lambda () (add-raw s a f c (car e) g))
	  (lambda (v data)
	    (fluid-set! env (cons v (cdr e)))
	    data))))
    
    (define (add-l-f e s a f c g)
      (call-with-values (lambda () (add-raw s a f c (car e) g))
	(lambda (v data)
	  (values
	   (cons v (cdr e))
	   data))))

    (define (add-r s a f c g)
      (let ((e (fluid-ref env)))
	(call-with-values (lambda () (add-raw s a f c (cdr e) g))
	  (lambda (v data)
	    (fluid-set! env (cons (car e) v))
	    data))))

    (define (add-r-f e s a f c g)
      (call-with-values (lambda () (add-raw s a f c (cdr e) g))
	(lambda (v data)
	  (values
	   (cons (car e) v)
	   data))))
	  
    (define (rm-raw s f e one? g)
      (let* ((t     (get-tag e))
             (d     (get-dyn e))
             (ar    (get-ar  e))
             (l     (get-li  e))
             (rems '())
             (f     (lambda (x) 
                      (let ((r (f x)))
                        (if r 
                            (set! rems (cons x rems))
                            r)))))
                        
        (define (remove indexer l)
          (let lp ((l l) (i indexer))
            (if (null? l)
                i
                (if one?
                    (let ((data (car l)))
                      (index-remove s (get-t data) (get-a data) i))
                    (lp (cdr l) 
                        (let ((data (car l)))
                          (index-remove s (get-t data) (get-a data) i)))))))

        (call-with-values (lambda () (dynlist-remove f d one?))
          (lambda (tree change?)
            (fold-dynlist-rl (lambda (x e) (get-a x)) tree 0)
            (if change?
                (fold-dynlist-rl 
                 (lambda (x e)
                   (add-raw s (get-a x) (get-f x) (get-c x) e g))
                 tree
                 (make-1))
                (vector t tree ar (remove l rems)))))))

    (define (rm s f one? g)
      (let* ((e   (fluid-ref env))
             (rm? #f)
             (a1  (rm-raw s (lambda x 
                              (if (apply f x) 
                                  (begin (set! rm? one?) #t)
                                  #f))
                          (car e) one? g))
             (a2  (if rm? (cdr e) (rm-raw s f (cdr e) one? g))))
        (fluid-set! env (cons a1 a2))))

    (define (rm-f e s f one? g)
      (let* ((rm? #f)
             (a1  (rm-raw s (lambda x 
                              (if (apply f x) 
                                  (begin (set! rm? one?) #t)
                                  #f))
                          (car e) one? g))
             (a2  (if rm? (cdr e) (rm-raw s f (cdr e) one? g))))
        (cons a1 a2)))
                              
    (define (expose a)
      (set-a a (p (get-a a)))
      (set-f a (p (get-f a)))
      (set-c a (p (get-c a))))

    (define (compile-raw e)
      (let* ((l (get-dyn e))
             (m (fold-dynlist-lr (lambda (x seed)
				   (max seed (get-t x)))
                                 l 0))
             (a (make-vector (+ 1 (first-set-bit m)))))
            
        (fold-dynlist-lr (lambda (x seed)
			   (expose x)
                           (let ((t (get-t x)))
                             (vector-set! a (first-set-bit t) x)))
                         l 0)
        a))

    (define (compile)
      (let* ((e  (fluid-ref env))
             (el (car e))
             (er (cdr e)))
        (fluid-set! env (cons (vector (get-tag el) (get-dyn el) 
                                      (compile-raw el) (get-li el)
				      (get-theory el))
                              (vector (get-tag er) (get-dyn er) 
                                      (compile-raw er) (get-li er)
				      (get-theory er))))))
    (define (compile-f e)
      (let* ((el (car e))
             (er (cdr e)))
        (cons (vector (get-tag el) (get-dyn el) 
		      (compile-raw el) (get-li el))
	      (vector (get-tag er) (get-dyn er) 
		      (compile-raw er) (get-li er)))))
          
    (define (compile-index-raw s e)
      (let* ((d   (get-dyn e))
	     (set 0)
	     (dyn (fold-dynlist-lr
		   (lambda (x indexer)
		     (bitmap-indexer-add! s (p (vector-ref x 1))
					  (p (vector-ref x 0))
					  indexer (mk-get-set! set)))
		   d
		   (make-indexer))))
	(set! set (compile-set-representation set))
        (vector
         (get-tag e)
         d
         (dynlist->vlist (get-ar e) d)
	 (compile-inh s dyn set)
	 set)))
    
    (define (compile-index s)
      (let ((e (fluid-ref env)))
        (fluid-set! env (cons (compile-index-raw s (car e)) 
                              (compile-index-raw s (cdr e))))))

    (define (compile-index-f e s)
      (cons (compile-index-raw s (car e)) 
	    (compile-index-raw s (cdr e))))

    (define (walk-raw s p cut scut a F walk-dynlist e rev more?)
      (let* ((ar   (get-ar e))
             (db   (get-li e)))
        (let lp ((l (rev (get-index-set s a db (get-theory e)))))
          (if (not (pair? l))
	      (p)
	      (if (null? (cdr l))
		  (F p cut scut
		     a
		     (if (vector? ar)
			 (vector-ref ar (car l))
			 (vlist-ref (cdr ar) (- (car ar) (car l) 1)))
		     (and #t (not more?)))
		  (F (lambda () 
		       (lp (cdr l))) cut scut
		     a
		     (if (vector? ar)
			 (vector-ref ar (car l))
			 (vlist-ref (cdr ar) (- (car ar) (car l) 1)))
		     #f))))))

    (define (walk-raw-ii s p cut scut a F walk-dynlist e rev more?)
      (let* ((ar   (get-ar e))
             (db   (get-li e)))
        (let lp ((l (rev (get-index-set s a db (get-theory e)))) (p p))
          (if (not (pair? l))
	      (p)
	      (if (null? (cdr l))
		  (F p cut scut
		     a
		     (if (vector? ar)
			 (vector-ref ar (car l))
			 (vlist-ref (cdr ar) (- (car ar) (car l) 1)))
		     (and #t (not more?)))
		  (or-ii-f s
		   (lambda (p) 
		     (F p cut scut
			a
			(if (vector? ar)
			    (vector-ref ar (car l))
			    (vlist-ref (cdr ar) (- (car ar) (car l) 1)))
			#f))
		   (lambda (p)
		     (lp (cdr l) p))
		   p))))))

    (define (walk-raw-f pre s p cut scut a F walk-dynlist e rev more?)
      (let* ((ar (get-ar e))
             (db (get-li e)))	
        (let lp ((l (rev (get-index-set s a db (get-theory e)))))
          (if (not (pair? l))
	      (p)
	      (if (null? (cdr l))
		  (F p cut scut
		     (append pre a)
		     (if (vector? ar)
			 (vector-ref ar (car l))
			 (vlist-ref (cdr ar) (- (car ar) (car l) 1)))
		     (and #t (not more?)))
		  (F (lambda () (lp (cdr l))) cut scut
		     (append pre a)
		     (if (vector? ar)
			 (vector-ref ar (car l))
			 (vlist-ref (cdr ar) (- (car ar) (car l) 1)))
		     #f))))))
    
    (define (walk-lr s p a F)
      (walk-lr-e env s p a F))

    (define (walk-lr-e e s p a F)
      (if (not (get-li (car (fluid-ref e))))
          (begin
            (compile-index s)
            (compile)
            (walk-lr-e e s p a F))
          (let ((el (car (fluid-ref e)))
                (er (cdr (fluid-ref e))))
	    (if (null? (get-index-set s a (get-li er) (get-theory er)))
		(walk-raw s p p s a F walk-dynlist-lr el
			  (lambda (x) x) #f)

		(walk-raw s (lambda () (walk-raw s p p s a F walk-dynlist-rl er 
						 reverse! #f)) 
			  p s
			  a F walk-dynlist-lr el
			  (lambda (x) x) #t)))))

    (define (walk-lr-ii s p a F)
      (walk-lr-ii-e env s p a F))

    (define (walk-lr-ii-e e s p a F)
      (if (not (get-li (car (fluid-ref e))))
          (begin
            (compile-index s)
            (compile)
            (walk-lr-ii-e e s p a F))
          (let ((el (car (fluid-ref e)))
                (er (cdr (fluid-ref e))))
	    (if (null? (get-index-set s a (get-li er) (get-theory er))) ;; To avoid gc leak
		(walk-raw-ii s p p s a F walk-dynlist-lr el
			     (lambda (x) x) #f)

		(walk-raw-ii s 
			     (lambda () 
			       (walk-raw-ii
				s p p s a F walk-dynlist-rl er 
				reverse! #f)) p s
				a F walk-dynlist-lr el
				(lambda (x) x) #t)))))

    (define (walk-lr-f pre setter e s p a F)
      (if (not (get-li (car e)))
	  (walk-lr-f pre setter (setter (compile-f (compile-index-f e s)))
		     s p a F))
          (let ((el (car e))
                (er (cdr e)))
	    (if (null? (get-index-set s a (get-li er) (get-theory er)))
		(walk-raw-f pre s p p s a F walk-dynlist-lr el
			  (lambda (x) x) #f)

		(walk-raw-f pre s 
			    (lambda () (walk-raw-f pre
						   s p p s a F 
						   walk-dynlist-rl er 
						   reverse! #f))
			    p s
			    a F walk-dynlist-lr el
			    (lambda (x) x) #t))))
           
    (cond
     ((eq? raw? 'fkns)
      (values walk-lr-f add-l-f add-r-f rm-f compile-f compile-index-f 
	      truncate!-f ref++-f ref---f))
     ((eq? raw? 'env)
      env)
     
     (else
      (values walk-lr add-l add-r rm compile env-ref env-set! compile-index 
	      truncate! ref++ ref-- walk-lr-ii))))))
             
(define (mk-dyn f setter . v)
    (call-with-values make-functional-dynamic-db
      (lambda (walk-lr add-l add-r rm compile env-ref env-set! compi
                       truncate! ref++ ref-- walk-lr-ii)
        (define g
          (lambda (s p cc . a)
            (let ((fr  (gp-newframe-choice s))
		  (del (fluid-ref delayers)))
              (walk-lr s p a
                       (lambda (p cut scut a vec last?)
			 (if (not last?)
			     (let ((p (lambda () 
					(gp-unwind fr) 
					(fluid-set! delayers del)
					(p))))
			       ((get-f vec) s p cc cut scut a))
			     (begin
			       (gp-unwind-tail fr)
			       ((get-f vec) s p cc cut scut a))))))))
        (define k
          (lambda (s p cc . a)
            (let ((fr  (gp-newframe-choice s))
		  (del (fluid-ref delayers)))
              (walk-lr-ii s p a
                       (lambda (p cut scut a vec last?)
			 (if (not last?)
			     (let ((p (lambda () 
					(gp-unwind fr) 
					(fluid-set! delayers del)
					(p))))
			       ((get-f vec) s p cc cut scut a))
			     (begin
			       (gp-unwind-tail fr)
			       ((get-f vec) s p cc cut scut a))))))))

	(define gg (lambda x (apply g x)))
	(define kk (lambda x (apply k x)))
	(define sg (case-lambda (() g) ((h) (set! g h))))
	(define sk (case-lambda (() k) ((h) (set! k h))))
        (setter gg)
	(if (pair? v) ((car v) kk))
	(set-procedure-property! gg 'debug-fkn sg)
	(set-procedure-property! kk 'debug-fkn sk)

        (set-object-property! gg 'dynamic-data
                              (vector add-l add-r rm compile 
                                      env-ref env-set! compi
                                      truncate! ref++ ref-- walk-lr))
        (set-object-property! kk 'dynamic-data
                              (vector add-l add-r rm compile 
                                      env-ref env-set! compi
                                      truncate! ref++ ref-- walk-lr-ii))
        (set-procedure-property! gg 'name f)
        (set-procedure-property! kk 'name f)
        (set-procedure-property! gg 'module (module-name (current-module)))
        (set-procedure-property! kk 'module (module-name (current-module)))
        (set-procedure-property! gg 'shallow #t)
        (set-procedure-property! kk 'shallow #t))))


(define (mk-dyn-ii f setter)
  (define k #f)
  (mk-dyn f (lambda (x) (set! k x)) setter))

(define-syntax-rule (define-dynamic f)
  (begin
    (define f #f)
    (mk-dyn 'f (lambda (x) 	
		 ;(pk 'define (object-address x))
		 (set! f x)))))

(define-syntax-rule (define-dynamic-ii f)
  (begin
    (define f #f)
    (let ()
      (define k #f)
    (mk-dyn 'f 
	    (lambda (x) 	
	      (set! k x))
	    (lambda (x) 	
	      (set! f x))))))

(define (define-dynamic-f f)
  (module-define! (current-module) f #f)
  (mk-dyn f (lambda (x) 	
	      (module-set! (current-module) f x))))

(define-syntax-rule (define-dynamic! f)
  (begin
    (if (not (defined? 'f))
        (define! 'f #f))
    (mk-dyn 'f (lambda (x) 
		 ;(pk 'define (object-address x))
		 (set! f x)))))

(define (tr-dynamic-> f g)
  ;(pk 'translate-> (object-address f) (object-address g))
  (let ((r (object-property f 'dynamic-data)))
    (when r
      (set-object-property! g 'dynamic-data r))))

(define (dynamic? f)
  (let ((env (object-property f 'dynamic-data))) 
    (if env
        f
        #f)))
    
(define (dynamic-abolish f)
  (set-object-property! f 'dynamic-data #f)
  (if #f #f))

(define (dynamic-push s g a f c)
  (let ((env (object-property g 'dynamic-data)))
    (if env
        ((vector-ref env 0) s a f c g)
        (error "not a dynamic variable 1" g))))

(define (dynamic-append s g a f c)
  (let ((env (object-property g 'dynamic-data)))
    (if env
        ((vector-ref env 1) s a f c g)
        (error "not a dynamic variable 2" g))))

(define (dynamic-remove s f F one?)
  (let ((env (object-property f 'dynamic-data)))
    (if env
        ((vector-ref env 2) s F one? f)
        (error "not a dynamic variable 3" f))))

(define (dynamic-compile f)
  (let ((env (object-property f 'dynamic-data)))
    (if env
        ((vector-ref env 3))
        (error "not a dynamic variable 4" f))))

(define (dynamic-env-ref f)
  (let ((env (object-property f 'dynamic-data)))
    (if env
        ((vector-ref env 4))
        (error "not a dynamic variable 5" f))))

(define (dynamic-env-set! f state)
  (let ((env (object-property f 'dynamic-data)))
    (if env
        ((vector-ref env 5) state)
        (error "not a dynamic variable 6" f))))

(define (dynamic-compile-index s f)
  (let ((env (object-property f 'dynamic-data)))
    (if env
        ((vector-ref env 6) s)
        (error "not a dynamic variable 7 " f))))

(define (dynamic-truncate! f x)
  (let ((env (object-property f 'dynamic-data)))
    (if env
        ((vector-ref env 7) x)
        (error "not a dynamic variable 8" f))))

(define (dynamic-refcount++ f)
  (let ((env (object-property f 'dynamic-data)))
    (if env
        ((vector-ref env 8))
        (error "not a dynamic variable 9" f))))

(define (dynamic-refcount-- f)
  (let ((env (object-property f 'dynamic-data)))
    (if env
        ((vector-ref env 9))
        (error "not a dynamic variable 10" f))))

(define (dynamic-walk-lr f)
  (let ((env (object-property f 'dynamic-data)))
    (if env
        (vector-ref env 10)
        (error "not a dynamic variable 11" f))))

(define <clause-dynamic> 
  (lambda (s p cc f head body)
    (let ((fr (gp-newframe s)))
      ((dynamic-walk-lr f)
       s p (gp-cp head s)
       (lambda (p cut scut a vec last?)            
         (let ((p (lambda () (gp-unwind fr) (p))))
           ((<lambda> () 
              (<let> ((c (<cp> (get-c vec))))
                     (<=> c ,(cons head body))))
            s p cc)))))))

(define <clause-dynamic-3> 
  (lambda (s p cc f head body ref)
    (let ((ref (gp-lookup ref s)))
      (if (gp-attvar? ref s)
	  (let ((fr (gp-newframe s)))
	    ((dynamic-walk-lr f)
	     s p (gp-cp head s)
	     (lambda (p cut scut a vec last?)            
	       (let ((p (lambda () (gp-unwind fr) (p))))
		 ((<lambda> () 
		    (<let> ((c (<cp> (get-c vec))))
			   (<=> ref vec)
			   (<=> c ,(cons head body))))
		  s p cc)))))
	  (<and> (p s p cc)
	     (<=>  ,(<cp> (get-c ref)) ,(cons head body))
	     (<=>  f ,(get-g ref)))))))

  

(define-syntax ck-cons
  (syntax-rules (quote)
    ((_ s 'x 'l)
     (ck s '(x . l)))))

(define (pkk x) #;(pk 'pkk (syntax->datum x)) x)
(define parse-list
  (lambda (x)
    (pkk 
    (let lp ((x x))
      (syntax-case x (@@ quote unquote and)
	(#(x ...)
	 (apply vector (lp #'(x ...))))
	((and x y)
	 (lp #'y))
	((unquote x)
	 #'(unquote x))
	('x
	 #'x)
	((x . l)
	 (cons (lp #'x) (lp #'l)))
	(y
	 (if (symbol? (syntax->datum #'y))
	     (list #'unquote #'(gp-make-var))
	     #'y)))))))

(define parse-match
  (lambda (x)
    (pkk 
    (let lp ((x x))
      (syntax-case x (@@ quote unquote and logic guile-log type Type)
	((,(@@ (logic guile-log type) Type) var . _)
	 (lp #'var))
	(#(x ...)
	 (apply vector (lp #'(x ...))))
	((and x y)
	 (lp #'y))
	((unquote x)
	 #'(unquote x))
	('x
	 #'x)
	((x . l)
	 (cons (lp #'x) (lp #'l)))
	(y #'y))))))

(define parse-pat-extended
  (lambda (x1)
    (pp 'pat 
	(let lp ((x2 x1))
	  (syntax-case x2 ()
	    ((a2 x4 ...)
	     (let ((aa (syntax->datum #'a2))
		   (l  (map lp #'(x4 ...))))
	       (if (symbol? aa)
		   (cond
		    ((eq? aa 'and) (cons #'and l))
		    ((eq? aa 'or)  (cons #'or  l))
		    ((eq? aa 'not) (cons #'not l))
		    ((eq? aa '=..)
		     (vector (lp (car #'(x4 ...)))))
		    (else (cons #'a2 l)))
		   (cons (lp #'a2) l))))
		    

	    ((a2 . b)
	     (cons (lp #'a2) (lp #'b)))

	    (#(x3 ...)
	     (apply vector (lp #'(x3 ...))))

	    
	    (x5
	     #'x5))))))

(define parse-list-extended
  (lambda (x)
    (define imptag #f)
    (define (id? x) (symbol? (syntax->datum x)))
    (define (combine op l)
      (syntax-case l ()
	((f u ...)
	 (if (eq? (syntax->datum #'f) op)
	     (let lp ((l #'(u ...)))
	       (if (pair? l)
		   (if (id? (car l))
		       (lp (cdr l))
		       (append (combine op (car l)) (lp (cdr l))))
		   '()))
	     (list l)))
	(x (list #'x))))
       
    (pp 'liste
    (let lp ((x x))
      (syntax-case x (unquote quote)
	(#(xx ...)
	 (apply vector (lp #'(xx ...))))

	('x
	 #'x)

	((unquote x)
	 #'(unquote x))
	
	((a xx ...)
	 (let* ((aa (syntax->datum #'a))
		(l  #'(xx ...))
		(f  (lambda (id? op)
		      (map lp
			   (let lp ((l l))
			     (if (pair? l)
				 (if (id? (car l))
				     (lp (cdr l))
				     (append (combine op (car l)) 
					     (lp (cdr l))))
				 '()))))))			    

	   (if (symbol? aa)
	       (cond
		((eq? aa '=..)
		 (vector (lp (car #'(xx ...)))))
		((eq? aa 'and)
		 (cons (list #'unquote #'and-tag) (f id? 'and)))
		((eq? aa 'or)  
		 (cons (list #'unquote #'or-tag) (f (lambda x #f) 'or)))
		((eq? aa 'not)
		 (cons (list #'unquote #'not-tag) (map lp l)))
		(else 
		 (cons (lp #'a) (map lp l))))
	       (cons (lp #'a) (map lp l)))))  
		
	((a . b)
	 (cons (lp #'a) (lp #'b)))

	(y
	 (if (identifier? #'y)
	     (list #'unquote #'(gp-make-var))
	     #'y)))))))

(define (mk-varpat-extended x) #``#,(parse-list-extended x))
  
(define-syntax mk-varpat
  (lambda (x)
    (syntax-case x ()
      ((_  y) #``#,(parse-list #'y)))))
  

(define-syntax <lambda-dyn>
  (lambda (x)
    (syntax-case x ()    
      ((_ pat code) 
       (with-syntax ((patt (parse-match #'pat)))
          #'(list (lambda () (mk-varpat pat))
		  (lambda ()
		    (lambda (a b c cut scut x) 
		      ((<<lambda>> (patt  (<with-cut> cut scut code)))
		       a b c x)))
		  (lambda () "true"))))

      ((_ pat code y)
       (with-syntax ((patt (parse-match #'pat)))
          #'(list (lambda () (mk-varpat pat))
		  (lambda ()
		    (lambda (a b c cut scut x) 
		      ((<<lambda>> (patt (<with-cut> cut scut code)))
		       a b c x)))
		  (lambda () y))))

      ((_ (v ...) pat code y)
       (with-syntax ((patt (parse-match #'pat)))
          #'(list (lambda () (mk-varpat pat))
		  (lambda ()
		    (lambda (a b c cut scut x)
                      (let ((vv (gp-cp (list v ...) a)))
                        ((@ (ice-9 match) match) vv
                         ((v ...)
                          ((<<lambda>> (patt (<with-cut> cut scut code)))
                           a b c x))))))
                  (lambda () y))))
      
      ((_ pat)
       (with-syntax ((patt (parse-match #'pat)))
           #'(<lambda-dyn> patt <cc>))))))
  
(define-syntax <lambda-dyn-meta>
  (syntax-rules ()    
    ((_ pat) 
     (list (lambda () (mk-varpat pat))
           (lambda () "true")))

    ((_ pat y)
     (list (lambda () (mk-varpat pat))
           (lambda () y)))))

(define-syntax <lambda-dyn-extended>
  (lambda (x) 
  (syntax-case x ()    
    ((_ (pat ...) code)
     (with-syntax (((pat2 ...) (pp 'a (parse-pat-extended #'(pat ...))))) 
	(pp #`(list (lambda ()
			   #,(pp 'b (mk-varpat-extended #'(pat ...))))
		(lambda ()
		  (lambda (a b c cut scut x) 
		    (apply (<<lambda>> (pat2 ... (<with-cut> cut scut code)))
			   a b c x)))
		(lambda () 'true)))))

    ((_ (pat ...) code y)
     (with-syntax (((pat2 ...) (pp (parse-pat-extended #'(pat ...)))))
	(pp #`(list (begin 
			   #,(mk-varpat-extended #'(pat ...)))
		    (lambda ()		 
		      (lambda (a b c cut scut x) 
			(apply (<<lambda>> (pat2 ... (<with-cut> cut scut 
								 code)))
			       a b c x)))
		    (lambda () y)))))

    ((_ (v ...) (pat ...) code y)
     (with-syntax (((pat2 ...) (pp (parse-pat-extended #'(pat ...)))))
       (pp #`(list (begin 
                     #,(mk-varpat-extended #'(pat ...)))
                   (lambda ()                      
                     (lambda (a b c cut scut x)
                       (let ((vv (gp-cp (list v ...) a)))
                         ((@ (ice-9 match) match) vv
                          ((v ...)
                           (apply (<<lambda>> (pat2 ... (<with-cut> cut scut 
                                                                     code)))
                                  a b c x))))))
                   (lambda () y)))))

    ;; TODO this part needs hooks for attributed variables added
    ((_ (pat ...))
     (with-syntax (((pat2 ...) (pp (parse-pat-extended #'(pat ...)))))
       (pp #`(<lambda-dyn-extended> (pat2 ...) <cc>)))))))

(define (make-dyn mod name)
  (define g #f)
  (mk-dyn name (lambda (x) (set! g x)))
  (set-object-property! g 'module (datum->syntax #'make-dyn mod))
  g)

(<define> (create-if-missing f)
  (<let*> ((f   (<lookup> f)))
     (if (not (dynamic? f))
	 (<cc>
	  (let* ((mod.sym
		  (cond
		   ((procedure? f)
		    (let ((n   (procedure-name f))
			  (mod (syntax->datum
				(resolve-module
				 (syntax->datum
				  (procedure-property f 'module))))))
		      (if mod
			  (cons mod n)
			  (cons (current-module) n))))
		   ((string? f)
		    (cons (current-module) (string->symbol f)))
		   ((symbol? f)
		   (cons (current-module) f))
		   (else
		    (error "dynamic addition of non deducable object name"))))
		 (mod (car mod.sym))
		 (sym (cdr mod.sym)))
	    (if (module-defined? mod sym)
		(module-define! mod sym (make-dyn mod sym))
		(module-set! mod sym (make-dyn mod sym)))
	    (module-ref mod sym)))
	 (<cc> f))))

(define <push-dynamic>
  (<case-lambda> 
   ((f dyn-lambda)
    (<let> ((g f))
      (<values> (g) (create-if-missing g))
      (<code> (apply dynamic-push S g dyn-lambda))))
   ((f u dyn-lambda)
    (<let> ((g f))
      (<values> (g) (create-if-missing g))
      (<=> u ,(apply dynamic-push S g dyn-lambda))))))

(define <append-dynamic> 
  (<case-lambda>
   ((g dyn-lambda)
    (<let> ((f g))
     (<values> (f) (create-if-missing f))
     (<code> (apply dynamic-append S f dyn-lambda))))
   ((g u dyn-lambda)
    (<let> ((f g))
     (<values> (f) (create-if-missing f))
     (<=> u ,(apply dynamic-append S f dyn-lambda))))))

(<define-guile-log-rule> (<retract-dynamic> f y)
  (<code> (dynamic-remove 
           S f 
           (lambda (data) 
             (let ((fr (<newframe>)))
               (let* ((s (gp-unify! (get-c data) y S)))
		 (<unwind-tail> fr)
                 (if s #t #f))))
           #t)))

(<define-guile-log-rule> (<retract-all-dynamic> (f x ...) y)
  (<code> (dynamic-remove 
           S f 
           (lambda (data) 
             (let ((fr (<newframe>)))
               (let ((s (gp-unify! (get-c data) y S)))
                 (<unwind-tail> fr)
                 (if s #t #f))))
           #f)))

(define (wrapu code)
  (let* ((s1 (gp-newframe (fluid-ref *current-stack*)))
	 (s2 (gp-newframe s1)))
    (with-fluids ((*current-stack* s2))
      (let ((ret (code s1)))
	(gp-unwind-tail s1)
	ret))))

(define (wapu ff mk-dyn . lams)
  (define f #f)
  (if (and (module-locally-bound? (current-module) ff)
	   (dynamic? (module-ref (current-module) ff)))
      (set! f (module-ref (current-module) ff))
      (mk-dyn 'f (lambda (x) (set! f x))))
  (for-each 
   (lambda (lam) (wrapu (lam f)))
   lams)
  f)

(define-syntax lambda*
  (syntax-rules ()
    ((_ (x y) code ...) 
     (lambda (x) (lambda (y) code ...)))))

(define-syntax <<case-lambda-dyn>>
  (lambda (z)
    (syntax-case z ()
      ((_ ff ((pat ... code) ...) ...)
       #'(wapu 'ff mk-dyn 
	  (lambda* (f s) 
	    (apply dynamic-append s f (<lambda-dyn> (pat ...) code)))
	  ... ...)))))

(define-syntax <<case-lambda-dyn-ii>>
  (lambda (z)
    (syntax-case z ()
      ((_ ff ((pat ... code) ...) ...)
       #'(wapu 'ff mk-dyn-ii
	       (lambda* (f s) 
		 (apply dynamic-append s f (<lambda-dyn> (pat ...) code)))
	       ... ...)))))

(define-syntax <<case-lambda-dyn-extended>>
  (lambda (z)
    (syntax-case z ()
      ((_ ff ((pat ... code) ...) ...)
       #'(wapu 'ff mk-dyn
	       (lambda* (f s)
		 (apply dynamic-append s f 
			(<lambda-dyn-extended> (pat ...) code)))
	       ... ...)))))

(define-syntax <<case-lambda-dyn-extended-ii>>
  (lambda (z)
    (syntax-case z ()
      ((_ ff ((pat ... code) ...) ...)
       #'(wapu 'ff mk-dyn-ii
	       (lambda* (f s) 
		 (apply dynamic-append s f 
			(<lambda-dyn-extended> (pat ...) code)))
	       ... ...)))))

(define-syntax-parameter 
  <<case-lambda-dyn-combined>> 
  (syntax-rules ()
    ((_ . l) (<<case-lambda-dyn>> . l))))


(define-syntax-rule  (interleaved f)
  (syntax-parameterize ((<<case-lambda-dyn-combined>>
			 (syntax-rules ()
			   ((_ . l) (<<case-lambda-dyn-ii>> . l)))))
    f))


(<define> (make_dynamic f ff)
  (<let*> ((f    (<lookup> f))
	   (name (procedure-name f)))
   (if (and (not (dynamic? f)) (object-property f 'non-defined))
       (<and>
	(<code>
	 (mk-dyn name
		 (lambda (x)
		   (if (module-defined? (current-module) name)
		       (module-set! (current-module) name x)
		       (module-define! (current-module) name x)))))
	(<=> ff ,(module-ref (current-module) name)))
       (<=> ff f))))


(<define> (make_generic_dynamic f r)
  (<=> r
       ,(let ((name (gensym (symbol->string (procedure-name (<lookup> f))))))
	  (define ret #f)
	  (mk-dyn name
		  (lambda (x)
		    (set! ret x)))
	  ret)))


(define special-translators
  (make-fluid `((#{,}# . and) (#{;}# . or) (#{\\+}# . not))))
(define default-translators (fluid-ref special-translators))

(define parse-list-extended2
  (lambda (x)
    (define imptag #f)
    (define (id? x) (symbol? (syntax->datum x)))
    (define (combine op l)
      (syntax-case l ()
	((f u ...)
	 (if (eq? (syntax->datum #'f) op)
	     (let lp ((l #'(u ...)))
	       (if (pair? l)
		   (if (id? (car l))
		       (lp (cdr l))
		       (append (combine op (car l)) (lp (cdr l))))
		   '()))
	     (list l)))
	(x (list #'x))))
       
    (pp 'liste
    (let lp ((x x))
      (syntax-case x (unquote quote)
	(#(((unquote x) a ...))
	 (let ((xx (syntax->datum #'x)))
	   (let ((res (and (symbol? xx)
			   (assoc xx (fluid-ref special-translators)))))
	     (if res
		 (case (cdr res)
		   ((and)
		    (cons* (list #'unquote #'and-tag) (map lp #'(a ...))))
		   ((or)
		    (cons* (list #'unquote #'or-tag) (map lp #'(a ...))))
		   ((not)
		    (cons* (list #'unquote #'not-tag) (map lp #'(a ...))))
		   (else
		    (vector (cons*
			     (lp #'(unquote x)) (map lp #'(a ...))))))
		 (vector (cons* (lp #'(unquote x)) (map lp #'(a ...))))))))

	(#(xx ...)
	 (apply vector (lp #'(xx ...))))

	('x
	 #'x)

	((unquote x)
	 #'(unquote x))
			
	((a . b)
	 (cons (lp #'a) (lp #'b)))

	(y
	 (if (identifier? #'y)
	     (list #'unquote #'(gp-make-var))
	     #'y)))))))

(define (mk-varpat-extended2 x) #``#,(parse-list-extended2 x))

(define parse-pat-extended2
  (lambda (x1)
    (pp 'pat 
	(let lp ((x2 x1))
	  (syntax-case x2 (unquote)
	    ((a2 . b)
	     (cons (lp #'a2) (lp #'b)))

	    (#(((unquote x) a ...))
	     (let ((xx (syntax->datum #'x)))
	       (let ((res (and (symbol? xx)
			       (assoc xx (fluid-ref special-translators)))))
		 (if res
		     (case (cdr res)
		       ((and)
			(cons* #'and (map lp #'(a ...))))
		       ((or)
			(cons* #'or  (map lp #'(a ...))))
		       ((not)
			(cons* #'not (map lp #'(a ...))))
		       (else
			(vector (cons* 
				 (lp #'(unquote x)) 
				 (map lp #'(a ...))))))
		     (vector (cons*
			      (lp #'(unquote x))
			      (map lp #'(a ...))))))))
	    
	    (#(x3 ...)
	     (apply vector (lp #'(x3 ...))))
	     
	    (x5
	     #'x5))))))

(define-syntax <lambda-dyn-extended2>
  (lambda (x) 
  (syntax-case x ()    
    ((_ (pat ...) code)
     (with-syntax (((pat2 ...) (pp 'a (parse-pat-extended2 #'(pat ...))))) 
	(pp 'lam1 #`(list (lambda ()
			   #,(pp 'b (mk-varpat-extended2 #'(pat ...))))
		(lambda ()
		  (lambda (a b c cut scut x) 
		    (apply (<<lambda>> (pat2 ... (<with-cut> cut scut code)))
			   a b c x)))
		(lambda () 'true)))))

    ((_ (pat ...) code y)
     (with-syntax (((pat2 ...) (pp 'c (parse-pat-extended2 #'(pat ...)))))
	(pp 'lam2 #`(list (begin 
			   #,(pp 'd (mk-varpat-extended2 #'(pat ...))))
		    (lambda ()		 
		      (lambda (a b c cut scut x) 
			(apply (<<lambda>> (pat2 ... (<with-cut> cut scut 
								 code)))
			       a b c x)))
		    (lambda () y)))))

    ;; TODO this part needs hooks for attributed variables added
    ((_ (pat ...))
     (with-syntax (((pat2 ...) (pp (parse-pat-extended2 #'(pat ...)))))
       (pp #`(<lambda-dyn-extended> (pat2 ...) <cc>)))))))

(define-syntax <<case-lambda-dyn-extended2>>
  (lambda (z)
    (syntax-case z ()
      ((_ ff ((pat ... code) ...) ...)
       #'(wapu 'ff mk-dyn
	       (lambda* (f s)
		 (apply dynamic-append s f 
			(<lambda-dyn-extended2> (pat ...) code)))
	       ... ...)))))

(define-syntax <<case-lambda-dyn-extended2-ii>>
  (lambda (z)
    (syntax-case z ()
      ((_ ff ((pat ... code) ...) ...)
       #'(wapu 'ff mk-dyn-ii
	       (lambda* (f s) 
		 (apply dynamic-append s f 
			(<lambda-dyn-extended2> (pat ...) code)))
	       ... ...)))))

(define (change-symbols a)
  (define (tr a)
    (if (procedure? a)
	(procedure-name a)
	(if (string? a)
	    (string->symbol a)
	    a)))

  (define l (fluid-ref special-translators))

  (define (rest r)
    (let lp ((l l) (res '()))
      (if (pair? l)
	  (let* ((x (car l))
		 (l (cdr l))
		 (a (car x))
		 (b (cdr x)))
	    (let ((ru (assoc a r)))
	      (if ru
		  (lp l (cons (cons (cdr ru) b) res))
		  (lp l (cons x  res)))))
	  res)))
  
  (let lp ((a a) (r '()))
    (if (pair? a)
	(let ((x (tr (car a)))
	      (a (cdr a)))
	  (if (pair? a)
	      (let ((y (tr (car a)))
		    (a (cdr a)))
		(let ((as (assoc x l)))
		  (if as
		      (lp a (cons (cons x y) r))
		      (lp a r))))
	      (rest r)))
	(rest r))))

(define-syntax start
  (lambda (x)
    (syntax-case x ()
      ((start a)       
       (fluid-set! special-translators (change-symbols 
					(syntax->datum #'a)))
       #'(if #f #f)))))
	     
(define-syntax end
  (lambda (x)
    (fluid-set! special-translators default-translators)
    #'(if #f #f)))

(define-syntax-rule  (extended f . a)
  (begin
    (start a)
    (let ((res (syntax-parameterize 
		((<<case-lambda-dyn-combined>>
		  (syntax-rules ()
		    ((_ . l) (<<case-lambda-dyn-extended2>> . l)))))
		f)))
      (end)
      res)))

(define-syntax-rule  (extended_interleaved f . a)
  (begin
    (start a)
    (let ((res (syntax-parameterize 
		((<<case-lambda-dyn-combined>>
		  (syntax-rules ()
		    ((_ . l) (<<case-lambda-dyn-extended2-ii>> . l)))))
		f)))
      (end)
      res)))
