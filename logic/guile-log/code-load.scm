(define-module (logic guile-log code-load)
  #:use-module (system vm vm)
  #:use-module (ice-9 format)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:export  (gp-clear gp-unify!- gp-unify-raw!- 
		      gp-newframe 
		      gp-newframe-choice
                      gp->scm gp-print gp-heap-var! 
                      gp-c-system
		      gp-make-var
                      gp-budy gp-m-unify!-
                      gp-lookup gp-lookup-1
                      gp-var? gp-cons! gp-set! 
                      gp-var-number gp-print-stack
                      gp-car gp-cdr gp-pair? gp-pair* gp-pair- gp-pair+
                      gp-store-state gp-store-state-all gp-restore-state
                      gp-var-set gp-ref-set! gp-var-set!
		      gp-set-1!
                      gp-dynwind
                      gp-atomic? 
                      gp-logical-var?
                      gp-get-stack
		      gp-rguards-ref
                      gp->scm-
                      gp-logical++ gp-logical-- 
                      gp-make-stack
                      gp-pair!? gp-null!? gp-null?
                      gp-jumpframe-start gp-jumpframe-end gp?
                      gp-module-init                      
                      gp-thread-safe-set!
                      -gp-member -gp-right-of -next-to -einstein
		      gp-deterministic?
		      gp-current-stack-ref
		      gp-undo-safe-variable-guard
		      gp-undo-safe-variable-rguard
		      gp-undo-safe-variable-lguard
		      gp-prompt gp-abort
		      gp-fluid-set
		      gp-prune 
		      gp-prune-tail 

		      gp-handlers-ref
		      gp-handlers-set!
		      gp-cont-ids-ref
		      gp-cont-ids-set!
                      
		      gp-guard-vars


                      gp-clear-frame
                      gp-clear-frame!
                      
                      gp-gc

		      gp-mark-permanent
                      
                      vlist? vlist-cons vlist-head vlist-tail vlist-null?
                      vlist-null list->vlist vlist-ref vlist-set!
                      vlist-fold vlist-fold-right 
                      vlist-last-val vlist-cons* vlist-pair?
                      test-vlist
                      vlist-drop vlist-take
                      vlist-length  vlist-map
                     ;vlist-unfold vlist-unfold-right vlist-append
                      vlist-reverse vlist-filter vlist-delete vlist->list
                      vlist-for-each
                      vlist-truncate! vhash-truncate!
                      vlist-thread-inc vlist-new-thread
                      vlist-refcount-- vlist-refcount++
		      
		      vhash-set! vhash-setq! vhash-setv!
                      vhash? vhash-cons vhash-consq vhash-consa vhash-consv
                      vhash-assoc vhash-assq vhash-assa vhash-assv
                      vhash-delete vhash-delq vhash-delv
                      vhash-fold vhash-fold-right
                     ;vhash-fold* vhash-foldq* vhash-foldv*
                     ;alist->vhash
                      vhash->assoc
                      vhashq->assoc
                      vhashv->assoc
		     block-growth-factor init-block-size
                     
                     get-index-set get-index-test get-index-tags


                     prolog-closure?
                     make-prolog-closure
                     prolog-closure-closure
                     prolog-closure-parent
                     prolog-closure-state
                     prolog-closure-closed?
                     
                     setup-closed

		     make-namespace
		     namespace?
		     namespace-val
		     namespace-ns                     
                     namespace-local?
                     namespace-lexical?
                     setup-namespace
                     <namespace-type>

		     gp-make-attribute-from-data
		     gp-attvar?
		     gp-attvar-raw?
		     gp-att-data
		     gp-put-attr
		     gp-put-attr-guarded
		     gp-put-attr-weak-guarded
		     gp-put-attr!
		     gp-put-attr-guarded!
		     gp-put-attr-weak-guarded!
		     gp-get-attr
		     gp-del-attr
		     gp-del-attr!
		     gp-att-put-data
		     gp-set-attribute-trampoline
		     gp-att-raw-var
		     gp-add-unwind-hook
		     multibute
		     
		     gp-make-ephermal-pair

		     gp-get-taglist
		     gp-match

		     gp-bv-address
		     
		     gp-get-id-data
		     gp-get-var-var
		     gp-clobber-var

		     int-to-code
		     code-to-int

		     gp-make-null-procedure
		     gp-fill-null-procedure

		     gp-make-struct
		     gp-set-struct

                     gp-store-engine-guards
                     gp-restore-engine-guards
                     gp-new-engine
                     gp-set-engine
                     gp-pop-engine
                     gp-push-engine
                     gp-peek-engine
                     gp-combine-engines
                     gp-combine-pop
                     gp-combine-push                     
                     gp-combine-state
                     gp-current-engine-path

                     set-setmap-struct!
;                    set-cp-attributes!
                     ))

;; Tos silence the compiler, those are fetched from the .so file
(define setup-vlist         #f)
(define set-closure-struct! #f)
(define gp-unify! #f)
(define gp-unify-raw! #f)
(define gp-m-unify! #f)
;;need to add modded,  

(catch #t
  (lambda ()
    (load-extension "libguile-log" "gp_init"))
  (lambda x      
    (let ((file  
           (%search-load-path "logic/guile-log/src/.libs/libguile-log.so")))
      (if 
       file
       (catch #t
         (lambda ()
           (load-extension file "gp_init"))
         (lambda x
           (warn
            "libguile-log is not loadable!")))
       (warn 
        "libguile-unify is not present, did you forget to make it?")))))

(define gp-unify!- gp-unify!)
(define gp-unify-raw!- gp-unify-raw!)
(define gp-m-unify!- gp-m-unify!)

;(define (gp-clear-frame . x) #t)
(define-syntax-rule  (definek x val)
  (module-define! (current-module) 'x val))

(define wrap #f)
(define-syntax-rule (define3 nm r)
  (definek nm (let ((rr r))
                (if wrap
                    (lambda (x y z) (rr x y z))
                    rr))))

(define-syntax-rule (define2 nm r)
  (definek nm (let ((rr r))
               (if wrap
                   (lambda (x y) (rr x y))
                   rr))))

(define-syntax-rule (define1 nm r)
  (definek nm (let ((rr r))
               (if wrap                   
                   (lambda (x) (rr x))
                   r))))

(define (gp-pair+ x s)
  (let* ((x (gp-lookup x s))
	 (s (gp-pair!? x s)))
    (if s
        (values (gp-car x s) 
                (gp-cdr x s)
                s)
        (values #f #f #f))))

(define (gp-pair- x s)
  (let* ((x (gp-lookup x s))
	 (s (gp-pair? x s)))
    (if s
        (values (gp-car x s) 
                (gp-cdr x s)
                s)
        (values #f #f #f))))

(define (gp-pair* x s)
  (if (pair? x)
      (values (car x) 
              (cdr x)
              s)
      (values #f #f #f)))

(definek   gp-c-system #f)
(definek  -gp-member    #f)
(definek  -gp-right-of  #f)
(definek  -next-to      #f)

(define -einstein #f)

(define-record-type <vlist>
  ;; A vlist is just a base+offset pair pointing to a block.

  ;; XXX: Allocating a <vlist> record in addition to the block at each
  ;; `vlist-cons' call is inefficient.  However, Bagwell's hack to avoid it
  ;; (Section 2.2) would require GC_ALL_INTERIOR_POINTERS, which would be a
  ;; performance hit for everyone.
  (make-vlist base offset)
  vlist?
  (base    vlist-base)
  (offset  vlist-offset))

(define-record-type <closure-type>
  ;; A vlist is just a base+offset pair pointing to a block.

  ;; XXX: Allocating a <vlist> record in addition to the block at each
  ;; `vlist-cons' call is inefficient.  However, Bagwell's hack to avoid it
  ;; (Section 2.2) would require GC_ALL_INTERIOR_POINTERS, which would be a
  ;; performance hit for everyone.
  (make-prolog-closure closure parent state closed?)
  prolog-closure?
  (closure prolog-closure-closure)
  (parent  prolog-closure-parent)
  (state   prolog-closure-state)
  (closed? prolog-closure-closed?))

(set-closure-struct! <closure-type>)

(set-record-type-printer! <vlist>
                          (lambda (vl port)
                            (cond ((vlist-null? vl)
                                   (format port "#<vlist ()>"))
                                  ((vhash? vl)
                                   (format port "#<vhash ~x ~a pairs>"
                                           (object-address vl)
                                           (vlist-length vl)))
                                  (else
                                   (format port "#<vlist ~a>"
                                           (vlist->list vl))))))

(define-record-type <namespace-type>
  (make-namespace_ val ns local? lexical?)
  namespace?
  (val      namespace-val)
  (ns       namespace-ns)
  (local?   namespace-local?)
  (lexical? namespace-lexical?))

(define (make-namespace a l b q)
  (let ((l (map (lambda (x)
                  (if (string? x)
                      (string->symbol x)
                      x))
                l)))
    (make-namespace_ a l b q)))

(set-record-type-printer! 
 <namespace-type>
 (letrec ((f (lambda (vl port)
               (let ((li (namespace-ns     vl))
                     (l? (namespace-local? vl))
                     (x  (namespace-val    vl)))
                 (if (namespace? x)
                     (f x port)
                     (if l?
                         (format port "~a@@~a" x li)
                         (format port "~a@~a" x li)))))))
   f))

(define x                   (setup-vlist <vlist>))
(define vlist-null          (list-ref x 0))
(define block-growth-factor (list-ref x 1))
(define init-block-size     (list-ref x 2))
(define thread-seq          (list-ref x 3))
(define thread-nr           (list-ref x 4))
(define thread-inc          (list-ref x 5))
(define (vlist-pair? x) (and (vlist? x) (not (vlist-null? x))))

(define (vlist-thread-inc)
  (fluid-set! thread-seq (+ thread-inc (fluid-ref thread-seq))))

(define (vlist-new-thread)
  (fluid-set! thread-nr  (+ thread-inc (fluid-ref thread-nr)))
  (fluid-set! thread-seq thread-inc))

(define (vlist-last-val x)
  (if (vlist-pair? x)
      (vlist-ref x (- (vlist-length x) 1))
      #f))

(define (vlist-cons* . l)
  (let ((r (vlist-last-val l)))
    (if r
        (let lp ((l l))
          (if (eq? l r)
              r
              (vlist-cons (car l) (lp (cdr l)))))
        r)))

(define gp->scm- gp->scm)

(define delayed-id (cons 'delayed 'unifier))
(define *delayers* (make-fluid '()))
(define (multibute f)
  (set-object-property! f delayed-id #t)
  f)
(define attributeU (make-variable #f))

(gp-set-attribute-trampoline attributeU *delayers*)


