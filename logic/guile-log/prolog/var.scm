(define-module (logic guile-log prolog var)
  #:use-module (logic guile-log prolog pre)
  #:use-module (logic guile-log prolog goal)
  #:use-module (logic guile-log prolog pre)
  #:use-module (logic guile-log prolog operators)
  #:use-module (logic guile-log prolog symbols)
  #:use-module (logic guile-log prolog namespace)
  #:use-module (logic guile-log prolog modules)
  #:use-module ((logic guile-log umatch)
                #:select (gp-newframe gp-unwind gp-make-var))
  #:use-module (ice-9 eval-string)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:use-module ((logic guile-log) 
		#:select (procedure-name
			  (<_> . GL:_) <define> <lambda>
			  <code> <cc> <lookup> CUT S))
  #:export (arg pat-match var term term-init-variables v-variables
                term-get-variables-list term-get-variables))

(define do-print #f)
(define pp
  (case-lambda
   ((s x)
    (when do-print
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when do-print
      (pretty-print (syntax->datum x)))
    x)))

(define ppp
  (case-lambda
   ((s x)
    (when #t
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when #t
      (pretty-print (syntax->datum x)))
    x)))

(define (metah lam  n . l)
  (if (null? l)
      (let ((f (hashq-ref (fluid-ref *closure-creations*) n #f)))
	(if f
	    f
	    (error "closure references failed could not find stored ref of already made closure")))
      (begin
	(let ((f (apply lam l)))
	  (hashq-set! (fluid-ref *closure-creations*) n f)
	  f))))

(define arg #f)
(define arg-goal #f)
(define compile-lambda #f)
(define do-print #f)
(define pp
  (case-lambda
   ((s x)
    (when do-print
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when do-print
      (pretty-print (syntax->datum x)))
    x)))

(define uq (list #'CUT))
(define get-double-quote-flag-fkn #f)

(define v-variables (make-fluid '()))

(define (@wrapper v li ? s)
  (cond
   ((eq? li #:current-module)
    (make-namespace v (map
		       (lambda (x) (if (symbol? x) (symbol->string x) x))
		       (module-name (current-module))) 
		    ? #t))
   (else
    (make-namespace v li ? #t))))


(define *variables* (make-fluid (make-hash-table)))
(define *var-list*  (make-fluid '()))

(define (term-get-variables)
  (define h (make-hash-table))
  (let lp ((l (reverse (fluid-ref *var-list*))))
    (if (pair? l)
        (if (hash-ref h (car l) #f)
            (lp (cdr l))
            (begin
              (hash-set! h (car l) #t)
              (cons (car l) (lp (cdr l)))))
        '())))

(define (term-get-variables-list)
  (reverse (fluid-ref *var-list*)))

(define (term-init-variables)
  (fluid-set! *var-list*   '())
  (fluid-set! *variables* (make-hash-table)))

(define (eval-scheme f) (f))
(<define> (do-eval-scheme   f) (<code> (f S)))
(<define> (when-eval-scheme f) (if (f S) <cc>))

(define found-scm (make-fluid #f))

(define (mk-scheme stx s l)
  (let* ((e   (with-input-from-string l read))
         (w   (datum->syntax stx e))
         (lam (lambda (x) x))
         (v   #`(lambda (s)
                  (syntax-parameterize ((S (identifier-syntax s)))
                      #,w))))
    (case s
      ((do)
       (lam #`(vector (list (@@ (logic guile-log prolog var) do-eval-scheme)
                            #,v))))
      ((when)
       (lam #`(vector (list (@@ (logic guile-log prolog var) when-eval-scheme)
                            #,v))))
      ((v var)
       (fluid-set! v-variables (cons e (fluid-ref v-variables)))
       (lam w))
      ((s scm)
       (lam w)))))

(define-syntax-rule (mk-mk-arg mk-arg -var- x_x -list- -eval- -term- -*term-
			       -termvar- -*termvar- -vector-)
(define* (mk-arg goal?)
  (letrec ((arg      (lambda x (apply (mk-arg #f) x)))
	   (arg-goal (lambda x (apply (mk-arg #t) x)))
	   (arg0
	    (lambda* (stx x #:optional (mod #f) (local? #f) #:key (first? #t))
  (define (fget  x . l) (apply arg      stx x mod local? l))
  (define (fgoal x) (arg-goal stx x mod local?))
  (match (pp 'mk-arg x)  
    (()                   '())
    ((#:group          x)  (fget x))
    ((#:variable '_  . _)  x_x)
    ((#:variable v   . _)  (-var- (datum->syntax stx v)))
    ((#:list     ()  . _)  (-list- '()))
    ((#:keyword   str . _)  (-eval- str))
    ((#:string    str . _)  (-eval- ((get-double-quote-flag-fkn) str)))
    ((#:list     v   . _)   (-list- (get-c fget v)))
    ((#:fknfkn (#:group a) (#:group b))
     (let* ((a (fget a))
	    (b (fget `(#:term (#:atom write #f #f 0 0) ,b #f 0 0))))
       (match b
	 (#((_ . l))
	  (vector (cons a l)))
	 ((q (vector (li w . l)))
	  (list q (list vector (cons* li  #``#,a  l)))))))
    ((#:scm-term (#:atom s . _) l _ _)
     (fluid-set! found-scm #t)
     (-eval- (mk-scheme stx s l)))
    ((#:@ v type li)
     (set! mod 1)
     (if (eq? type '@@) 
	 (set! local? #t)
	 (set! local? #f))
     (let ((li 
	    (if (keyword? li)
		li
		(map
		 (lambda (x) (if (symbol? x) (symbol->string x) x))
		 (if (vector? li)
		     (vector->list li)
		     (map (lambda (x)
			    (match x
			      ((x) (list-ref x 1))
			      (x   (list-ref x 1))))
			  (get.. "," li)))))))
       (case type
         ((@)  (-eval- #`(@wrapper `#,(fget v) '#,li #f S)))
         ((@@) (-eval- #`(@wrapper `#,(fget v) '#,li #t S))))))

    ((#:lam-term (or (#:atom s . _) (and #f s)) l closed? _ _)
     (if s
	 (-eval- (compile-lambda stx l s closed?))
	 (if l
             (if (eq? l 'null)
                 (-vector- #:brace #:null)
                 (-vector- #:brace (arg stx l)))
	     (-vector- #:brace))))
    	       
    ((#:term     (and atom (#:atom f _ _ n m)) () #f . _) 
     (add-sym mod local? atom)
     (-eval- (car (f->stxfkn #f mod f local? atom arg #f stx #f n m '()))))

    ((#:term     (and atom (#:atom f _ _ n m)) () meta . _)
     (add-sym mod local? atom)
     (let ((meta (map fget (get.. "," meta))))
       (-eval- 
	#`(metah #,@(f->stxfkn #f mod f local? atom arg #f stx #f n m '())
		 #,@meta))))

    ((#:term     (and atom (#:atom f _ _ n m)) ((_ _ "|" _) x y _ _) #f . _)
     (add-sym mod local? atom)
     (-*term-
      (f->stxfkn #f mod f local? atom arg #f stx #f n m 
		 (get.. "," x) y)))

    ((#:term     (and atom (#:atom f _ _ n m)) ((_ _ "|" _) y _ _) #f . _)
     (add-sym mod local? atom)
     (-*term-
      (f->stxfkn #f mod f local? atom arg #f stx #f n m 
		 '() y)))

    ((#:term     (and atom (#:atom f _ _ n m)) x  #f . _) 
     (add-sym mod local? atom)
     (-term-
      (f->stxfkn #f mod f local? atom arg #f stx #f n m 
		 (get.. "," x))))
    
    ((#:term     (and atom (#:atom f _ _ n m)) x  meta . _) 
     (add-sym mod local? atom)
     (let ((meta (map fget (get.. "," meta))))
       (-term-
	#`((metah #,@(f->stxfkn #f mod f local? atom arg #f stx #f n m '())
		  #,@meta)
	   #,@(map fget (get.. "," x))))))

    ((#:termvar  v _ () . _) 
     (-var- (datum->syntax stx v)))

    ((#:termvar  v _ ((_ _ "|" _) x y _ _)  . _) 
     (-*termvar-
      (append
       (cons (datum->syntax stx v)
	     (map fget (get.. "," x)))
       (list (fget y)))))

    ((#:termvar  v _ ((_ _ "|" _) y _ _)  . _) 
     (-*termvar-
       (list
	(datum->syntax stx v)
	(fget y))))
      
    ((#:termvar  v _ x . _) 
     (-termvar-
      (cons (datum->syntax stx v)
	    (map fget (get.. "," x)))))

    ((and atom (#:atom f _ _ n m))
     (add-sym mod local? atom)
     (-eval- (car (f->stxfkn #f mod f local? atom arg #f stx #f n m 
			     '()))))

    ((#:termstring (#:string str _ _) l _ _)
     (-term- (cons str (map fget (get.. "," l)))))

    ((#:number n     . _) 
     (-eval- n))

    (((_ _ "-" _) (#:number n _ _) _ _)
     (- n))

    ((or ((tp _ op _) x y n m)	 
         (#:termstring (#:string (and tp op) _ _) (x y) n m))
     (add-sym mod local? `(#:atom ,(string->symbol op) #f #f ,n ,m)) 
     (-term-
      (f->stxfkn #f #f op #f #f arg #f stx 2 n m (list x y))))

    ((or ((tp _ op _) x n m)
         (#:termstring (#:string (and tp op) _ _) (x) n m))
     (add-sym mod local? `(#:atom ,(string->symbol op) #f #f ,n ,m)) 
     (-term- 
      (f->stxfkn #f #f op #f #f arg #f stx 1 n m (list x))))

    ((x) (fget x)))))) 
    arg0)))

;----------------------------------
(define (var-arg  x)  #`,#,x)
(define x_x           #',GL:_)
(define (eval-arg x)  #`,#,x)
(define (list-arg x)  x)
(define (term-arg x)
  #`(unquote (vector (list
		      #,(car x)
		      #,@(map (lambda (x) #``#,x)
			      (cdr x))))))

(define (*term-arg x)
  (pp (syntax-case x ()
    ((f a ...)
     #`(unquote (vector (cons*
			 f
			 #,@(map (lambda (x) #``#,x) #'(a ...)))))))))
(define (termvar-arg x)
  #`(unquote (vector (list
		      #,(car x)
		      #,@(map (lambda (x) #``#,x)
			      (cdr x))))))

(define (*termvar-arg x)
  #`(unquote (vector (cons*
		      #,(car x)
		      #,@(map (lambda (x) #``#,x)
			      (cdr x))))))
(define vec-arg  
  (case-lambda
   ((tag x)
    #`,(vector #,tag `#,x))
   ((tag)
    #`,(vector #,tag))))

(mk-mk-arg mk-arg var-arg x_x list-arg eval-arg term-arg *term-arg 
	   termvar-arg *termvar-arg vec-arg)

(define arg      (mk-arg #f))
(define arg-goal (mk-arg #t))
; --------------------------------------------------

(define (var-match  x)  x)
(define x_match         #'_)
(define (eval-match x)    #`,#,x)
(define (list-match x)    x)
(define (term-match x)
  #`#((,#,(car x)
       #,@(map (lambda (x) x)
	       (cdr x)))))

(define (*term-match x)
  (pp 'match
  #`#((,#,(car x)
	  #,@(map (lambda (x) x) (butlast (cdr x)))
	  . #,(last x)))))

(define (varterm-match x)
  #`#((#,(car x)
       #,@(map (lambda (x) x)
	       (cdr x)))))

(define (butlast x) (reverse (cdr (reverse x))))
(define (last    x) (car (reverse x)))

(define (*varterm-match x)
  (pp #`#((#,(car x)
          #,@(map (lambda (x) x)
	          (butlast (cdr x)))
       .    #,(last x)))))

(define vec-match  
  (case-lambda
   ((tag x)
    #`#(#,tag #,x))
   ((tag)
    #`#(#,tag))))

(mk-mk-arg mk-match var-match x_match list-match eval-match 
	   term-match    *term-match
	   varterm-match *varterm-match
	   vec-match)

(define pat-match      (mk-match #f))
(define pat-match-goal (mk-match #t))

; ------------------------------------------------------

(define (var-var  x)  #`,#,x)
(define x_var         #`,GL:_)
(define (eval-var x)    #`,#,x)
(define (list-var x)    x)
(define (term-var x)
  #`,(vector (list #,(car x)
		   #,@(map (lambda (x) #``#,x)
			   (cdr x)))))
(define (*term-var x)
  (pp 'var (match x
    ((f a ...)
     #`,(vector (cons* #,f
		       #,@(map (lambda (x) #``#,x)
			       a)))))))
		       
(define (termvar-var x)
  #`,(vector (list #,(car x)
		   #,@(map (lambda (x) #``#,x)
			   (cdr x)))))
(define (*termvar-var x)
  #`,(vector (cons* #,(car x)
		   #,@(map (lambda (x) #``#,x)
			   (cdr x)))))
(define vec-var  
  (case-lambda 
   ((tag x)
    #`,(vector #,tag `#,x))
   ((tag)
    #`,(vector #,tag))))


(mk-mk-arg mk-var var-var x_var list-var eval-var term-var *term-var 
	   termvar-var *termvar-var vec-var)


(define var       (mk-var #f))
(define var-goal  (mk-var #t))

; --------------------------------------------------------------
(define (var-term  x) 
  (let ((v (syntax->datum x)))
    (fluid-set! *var-list* (cons v (fluid-ref *var-list*)))
    (hashq-set! (fluid-ref *variables*) v #t)
    #`,#,x))

(define x_term         #',(gp-make-var))
(define (eval-term x)  #`,#,x)
(define (list-term x)  x)
(define (term-term x)
  #`(unquote (vector (list
		      #,(car x)
		      #,@(map (lambda (x) #``#,x)
			      (cdr x))))))

(define (*term-term x)
  (match x
    ((f a ...)
     #`(unquote (vector (cons*
			 #,f
			 #,@(map (lambda (x) #``#,x) a)))))))

(define (termvar-term x)
  (let ((v (syntax->datum (car x))))
    (fluid-set! *var-list* (cons v (fluid-ref *var-list*)))
    (hashq-set! (fluid-ref *variables*) v #t))  
  #`(unquote (vector (list
		      #,(car x)
		      #,@(map (lambda (x) #``#,x)
			      (cdr x))))))

(define (*termvar-term x)
  (let ((v (syntax->datum (car x))))
    (fluid-set! *var-list* (cons v (fluid-ref *var-list*)))
    (hashq-set! (fluid-ref *variables*) v #t))  
  #`(unquote (vector (cons*
		      #,(car x)
		      #,@(map (lambda (x) #``#,x)
			      (cdr x))))))

(define vec-term 
  (case-lambda
   ((tag x)
    #`,(vector #,tag `#,x))
   ((tag)
    #`,(vector #,tag))))

(mk-mk-arg mk-term var-term x_term list-term eval-term term-term  *term-term 
	   termvar-term *termvar-term vec-term)


(define term      (mk-term #f))
(define term-goal (mk-term #t))
(set! (@@ (logic guile-log prolog operators) arg) arg)
(set! (@@ (logic guile-log prolog parser) term) term)
(set! (@@ (logic guile-log prolog parser) term-init-variables)
      term-init-variables)
(set! (@@ (logic guile-log prolog parser) term-get-variables)
      term-get-variables)
(set! (@@ (logic guile-log prolog parser) found-scm)
      found-scm)
