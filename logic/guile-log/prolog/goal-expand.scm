(define-module (logic guile-log prolog goal-expand)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log iso-prolog)
  #:export (goal_expand))

(define goal_expand #f)


(<define> (goal_exp x exps y)
  (<values> (xx) (goal_exp0 x exps))
  (if (eq? xx x)
      <fail>
      (<=> y xx)))

(<define> (goal_exp0 xin exps)
  (<<match>> (#:mode - #:name goal_exp) (xin)
    (#(("," x y))
     (<and>
      (<values> (xx) (goal_exp0 x exps))
      (<values> (yy) (goal_exp0 y exps))
      (if (and (eq? xx x) (eq? yy y))
	  (<cc> xin)
	  (<cc> (vector (list #{,}# xx yy))))))

    (#((",," x y))
     (<and>
      (<values> (xx) (goal_exp0 x exps))
      (<values> (yy) (goal_exp0 y exps))
      (if (and (eq? xx x) (eq? yy y))
	  (<cc> xin)
	  (<cc> (vector (list #{,,}# xx yy))))))

    (#((";" x y))
     (<and>
      (<values> (xx) (goal_exp0 x exps))
      (<values> (yy) (goal_exp0 y exps))
      (if (and (eq? xx x) (eq? yy y))
	  (<cc> xin)
	  (<cc> (vector (list #{;}# xx yy))))))

    (#((";;" x y))
     (<and>
      (<values> (xx) (goal_exp0 x exps))
      (<values> (yy) (goal_exp0 y exps))
      (if (and (eq? xx x) (eq? yy y))
	  (<cc> xin)
	  (<cc> (vector (list #{;;}# xx yy))))))
 
    (#(("->" x y))
     (<and>
      (<values> (xx) (goal_exp0 x exps))
      (<values> (yy) (goal_exp0 y exps))
      (if (and (eq? xx x) (eq? yy y))
          (<cc> xin)
          (<cc> (vector (list #{->}# xx yy))))))
    
    (#(("-i>" x y))
     (<and>
      (<values> (xx) (goal_exp0 x exps))
      (<values> (yy) (goal_exp0 y exps))
      (if (and (eq? xx x) (eq? yy y))
          (<cc> xin)
          (<cc> (vector (list #{-i>}# xx yy))))))

    (#(("\\+" x))
     (<and>
      (<values> (xx) (goal_exp0 x exps))
      (if (eq? xx x)
          (<cc> xin)
          (<cc> (vector (list #{\\+}# xx))))))
    (x
     (<var> (r)
            (<or>
             (<and>
              (goal_exp4 x exps exps r)
              <cut>
              (<cc> r))
             (<cc> xin))))))
                        
(compile-prolog-string
"
goal_expand(_,[],_) :- !, fail.

goal_expand((X :- Y),Exps,(X :- YY)) :- !,
   goal_exp(Y,Exps,YY).

goal_expand((:- Y),Exps,(:- YY)) :- !,
   goal_exp(Y,Exps,YY).

goal_expand((?- Y),Exps,(?- YY)) :- !,
   goal_exp(Y,Exps,YY).

goal_exp4(X,[E|EE],EEE, Res) :- 
    nonvar(X) ->
    (
      E(X,Res2) -> (goal_exp4(Res2,EEE,EEE,Res) -> true ; Res=Res2) ;
       goal_exp4(X,EE,EEE,Res)
    ).
")

(set! (@@ (logic guile-log prolog parser) goal-expand) goal_expand)
