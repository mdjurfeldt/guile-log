(define-module (logic guile-log prolog directives)
  #:use-module (logic guile-log prolog names)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log dynamic-features)
  #:use-module (logic guile-log prolog dynamic)
  #:use-module (logic guile-log prolog load)
  #:use-module (logic guile-log prolog parser)
  #:use-module (logic guile-log prolog run)
  #:use-module (logic guile-log prolog modules)
  #:use-module (logic guile-log prolog pre)
  #:use-module ((logic guile-log prolog util)
                #:select ((member . pr-member)))
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log prolog order)
  #:use-module (logic guile-log prolog var)
  #:use-module (logic guile-log prolog symbols)
  #:use-module (logic guile-log attributed)
  #:use-module (logic guile-log hash-dynamic)
  #:use-module ((logic guile-log) 
                #:select (procedure-name
			  <define> <let> <scm> <var?> <pp> <fail>
				   <lookup> <match> <<match>> <cut>
				   <or> <=> <recur> <code> <cc>))
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:replace (include)
  #:export  (dynamic multifile discontiguous op set_prolog_flag get-flag 
                     set-flag init-flags
                     do-character-convert
                     current_op
                     current_prolog_flag
                     initialization local_initialization
                     clear-directives is-dynamic?
                     get_prolog_flags_handle
		     module
		     meta_predicate
		     predicate_options
		     public volatile noprofile
		     create_prolog_flag thread_local
		     add_term_expansion
		     add_term_expansion_temp
		     add_goal_expansion
		     add_goal_expansion_temp
		     add_attribute_cstr
		     add_attribute_projector
		     translate-directive
                     ))

(define-syntax-rule (id stx x) 
  (datum->syntax stx (procedure-name (<lookup> x))))


(<define> (translate-directive stx head)
  (<<match>> (#:mode -) (head)
     (#(("add_attribute_cstor" x y))
      (<cc> #`(<code> (set-attribute-cstor! #,(id stx x) #,(id stx y)))))
     (#(("add_attribute_projector" x y))
      (<cc> #`(<code> (set-attribute-projector! #,(id stx x) #,(id stx y)))))
     (_ <fail>)))

	  
(define do-print #f)
(define pp
  (case-lambda
    ((s x)
     (when do-print
       (pretty-print `(,s ,(syntax->datum x))))
     x)
    ((x)
     (when do-print
       (pretty-print (syntax->datum x)))
     x)))

(define ppp
  (case-lambda
    ((s x)
     (when #t
       (pretty-print `(,s ,(syntax->datum x))))
     x)
    ((x)
     (when #t
       (pretty-print (syntax->datum x)))
     x)))

(define-parser-directive (module stx l n m)
  (module-mac stx l n m))

(define-parser-directive (meta_predicate stx l n m)
  #f)

(define-parser-directive (public stx l n m)
  #f)

(define-parser-directive (volatile stx l n m)
  #f)

(define-parser-directive (noprofile stx l n m)
  #f)

(define-parser-directive (create_prolog_flag stx l n m)
  #f)

(define-parser-directive (thread_local stx l n m)
  #f)

(<define> (add_term_expansion x)
  (<code>
   (fluid-set! *term-expansions* 
	       (cons (<lookup> x) 				 
		     (fluid-ref *term-expansions*)))))
	  
(define-parser-directive-onfkn add_term_expansion (term-spc stx l N M)
  (match l
    ((#:atom nm . _)
     #`(eval-when (compile eval load)
	   (fluid-set! *term-expansions* 
		       (cons (module-ref (current-module) 
					 '#,(datum->syntax stx nm))
			     (fluid-ref *term-expansions*)))))))

(define-parser-directive (add_term_expansion_temp stx l n m)
  (match l
    ((#:atom nm . _)
     #`(eval-when (compile)
		  (fluid-set! *term-expansions* 
			      (cons (module-ref (current-module) 
						'#,(datum->syntax stx nm))
				    (fluid-ref *term-expansions*)))))))

(define-parser-directive (add_attribute_cstr stx l n m)
  (match l
    ((_ (#:atom nm1 . _) (#:atom nm2 . _))
     #`(set-attribute-cstor! #,(datum->syntax stx nm1)
			     #,(datum->syntax stx nm2)))))

(define-parser-directive (add_attribute_projector stx l n m)
  (match l
    ((_ (#:atom nm1 . _) (#:atom nm2 . _))
     #`(set-attribute-projector! #,(datum->syntax stx nm1)
				 #,(datum->syntax stx nm2)))))
			     

(<define> (add_goal_expansion x)
  (<code>
   (fluid-set! *goal-expansions* 
	       (cons (<lookup> x) 				 
		     (fluid-ref *goal-expansions*)))))

(define-parser-directive-onfkn add_goal_expansion (goal-spc stx l N M)
  (match l
    ((#:atom nm . _)
     #`(eval-when (compile load eval)
		  (fluid-set! *goal-expansions* 
			      (cons (module-ref (current-module) 
						'#,(datum->syntax stx nm))
				    (fluid-ref *goal-expansions*)))))))


(define-parser-directive (add_goal_expansion_temp stx l n m)
  (match l
    ((#:atom nm . _)
     #`(eval-when (compile)
	   (fluid-set! *goal-expansions* 
		       (cons (module-ref (current-module) 
					 '#,(datum->syntax stx nm))
			     (fluid-ref *goal-expansions*)))))))

	  
(define-parser-directive-onfkn use_module (use-module-stx stx l N M)
  (use-module-mac stx l N M))

(define do-character-convert #f)
        
(define (acar x) x)

(define (str x)
  (if (symbol? x)
      (symbol->string x)
      x))

(define-parser-directive (include stx l N M)
  (match l
    ((or (#:string fname _ _) (#:atom fname . _))
     `(#:include ,(str fname)
       ,(acar
         (with-input-from-file (str fname)
           (lambda ()
             (with-fluids ((*prolog-file* (str fname)))
               (prolog-parse stx)))))))))


(define (partial-list? x)  
  (match x
    ((_ . l)
     (let lp ((l l))
       (match l
         ((_ . l) (lp l))
         (()      #f)
         (_       #t))))
    (_ #f)))
   
        
(define (parse-PI err N M)
  (letrec ((PI (lambda (x module)
		 (match x
		   (((_ _ ":" _) (#:atom mod . _) x . _)
		    (PI x mod))
                   (((_ _ "/" _) (#:atom f . _) (#:number n _ _) _ _)
                    (list (cons* module f n)))
		   (((_ _ "//" _) (#:atom f . _) (#:number n _ _) _ _)
                    (list (cons* module f n)))
		   ((#:atom f . _) 
                    (list (list #f f)))
		   ((#:list a _ _) 
		    (apply append (map (lambda (x) (PI x #f))
				       (get.. "," a))))
		   (_  (err N M))))))
    PI))

(define *dynamics* (make-fluid '()))
(define-syntax-rule (define-PI dynamic define-dynamic! i-define-dynamic! err)
  (define-parser-directive (dynamic stx l N M)
    (let lp ((l l) (numbered? #f))
      (match l
	(((_ _ ","  _) (#:number n . _)  y _ _)
	 (lp y n))
	(_
	 #`(begin
	     #,@(map 
		 (lambda (f)
		   (match f
		      ((mod f . n)
		       (begin
			 (fluid-set! *dynamics* (cons (cons mod f) 
						      (fluid-ref *dynamics*)))
			 (if mod
			     #f
			     (begin
			       (define-dynamic-f f)
			       (if numbered?
				   #`(i-define-dynamic!
				      #,numbered? 
				      #,(datum->syntax stx f))
				   #`(define-dynamic! 
				       #,(datum->syntax stx f)))))))
		      ((mod f)
		       (begin
			 (fluid-set! *dynamics* (cons (cons mod f) 
						      (fluid-ref *dynamics*)))
			 (if mod
			     #f
			     (begin
			       (define-dynamic-f f)
			       (if numbered?
				   #`(i-define-dynamic!
				      #,numbered? 
				      #,(datum->syntax stx f))
				   #`(define-dynamic! 
				       #,(datum->syntax stx f)))))))))
		 ((parse-PI err N M) l #f))))))))

(define is-dynamic? 
  (case-lambda 
   ((f)
    (member (cons #f f)  (fluid-ref *dynamics*)))
   ((mod f)
    (member (cons mod f) (fluid-ref *dynamics*)))))
   

(define (clear-directives)
  (fluid-set! *dynamics* '()))

(define spec-list (list fx fy yf xf xfx xfy yfx))

(define (proj x)
  (cond 
   ((procedure? x)
    (symbol->string (procedure-name x)))
   (else
    x)))

(<define> (current_op prio spec op)
   (<let> ((op   (proj (<lookup> op)))
           (prio (<lookup> prio))
           (spec (<lookup> spec)))
     (cond
      ((not (or (<var?> prio) (integer? prio)))
       (domain_error operator_priority prio))
      ((not (or (<var?> spec) (member spec spec-list)))
       (domain_error operator_specifier (pk spec)))
      ((not (or (<var?> op) (procedure? op) (string? op)))
       (domain_error atom op))
      (else
       (<let> ((spec (string->symbol (proj spec))))
        (<recur> lp ((x (get-ops)))
          (if (pair? x)
              (<recur> lp2 ((xx (car x)))
                 (<match> (#:mode * #:name current_op) (xx)
                   ((((-op . -spec) z1 -prio . z2) . l)
                    (<cut>
                     (<or>
                      (<=> (op prio spec) (-op -prio -spec))
                      (lp2 l))))
                   (()
                    (<cut> (lp (cdr x))))
                   (_
                    (<cut> (lp2 (cdr xx)))))))))))))
              
     
(<define> (op prio op_spec operator)
  (<let> ((operator (proj (<scm> operator)))
          (prio     (<scm> prio))
          (op_spec  (<scm> op_spec)))
  (cond 
   ((or (<var?> prio) (<var?> op_spec) 
        (partial-list? operator) 
        (and (list? operator) (or-map (lambda (x) (<var?> x)) operator)))
    (instantiation_error))

   ((not (integer? prio))
    (type_error integer prio))

   ((not (atom? op_spec))
    (type_error atom op_spec))

   ((not (or (list? operator) (atom? operator)))
    (type_error list operator))
   
   ((and (pair? operator) (not (and-map (lambda (x) (atom? x)) operator)))
    (type_error atom (let lp ((l operator)) (if (not (atom? (car l))) 
                                                 (car l)
                                                 (lp (cdr l))))))
   
   ((or (< prio 0) (> prio 1200))
    (domain_error operator_priority prio))
   
   ((not (member op_spec spec-list))
    (domain_error operator_specifier (pk op_spec)))

   ((and (atom? operator) (equal? operator ","))
    (permission_error modify operator ","))

   ((and (pair? operator) (or-map (lambda (x) (equal? x ",")) operator))
    (permission_error modify operator ","))

   (else
    (<code> 
     (if (= prio 0)
         (if (pair? operator)
             (for-each (lambda (x) (rem-op (procedure-name op_spec)
                                           x)) operator)
             (rem-op (procedure-name op_spec) operator))

         (if (pair? operator)
             (for-each (lambda (x) (add-op prio (procedure-name op_spec) x)) 
                       operator)
             (add-op prio (procedure-name op_spec) operator))))))))

(define (make-spec x)
  (cond
   ((eq? x 'xf) xf)
   ((eq? x 'fx) fx)
   ((eq? x 'yf) yf)
   ((eq? x 'fy) fy)
   ((eq? x 'xfx) xfx)
   ((eq? x 'yfx) yfx)
   ((eq? x 'xfy) xfy)
   (else x)))
	 

(define-parser-directive-onfkn op (op-spc stx l N M)
  (match (get.. "," l)
    (((#:number prio _ _)
      (#:atom spec . _)
      (or (#:string x _ _) (#:atom x . _) (#:symbolic x _ _)))
     (prolog-run-* (op prio (make-spec spec) x))
     #f)

    (((#:number prio _ _)
      (#:atom spec . _)
      (#:list li _ _))
     (match (get.. "," li)
       (((or (#:atom x . _) (#:string x _ _) (#:symbolic x _ _)) ...)
        (prolog-run-* (op prio (make-spec spec) x))
        #f)
       (_ (format #t "Bad op/3 directive at ~a~%" (get-refstr N M)) #t)))
    
    (_ (format #t "COMPILE ERROR: Bad op/3 directive at ~a~%" (get-refstr N M))
       #t)))




(define-parser-directive-onfkn ensure_loaded (op-spc stx l N M)
  (match l
    ((#:string str _ _)
     #`(load (ensure_loaded_ #,str)))
    ((#:atom atm . _)
     #`(load (ensure_loaded_ #,(symbol->string atm))))
    (_ (format #t "COMPILE ERROR: Bad ensure_loaded/1 directive at ~a~%" 
               (get-refstr N M)) #t)))

(define (str x)
  (cond
   ((symbol? x)
    (symbol->string x))
   (else
    x)))
                      
(<define> (local_initialization) <cc>)
(define (initialization . x)
  (error "initializattion is not for dynamic evaluation"))
(define-parser-directive-onfkn initialization (initialization-spec stx goal N M)
  `(#:init ,goal))

(define *prolog-flags* (make-fluid '()))
(add-fluid-dynamics (fluid-ref *current-stack*)
                    (lambda x #f)
                    (lambda x #t)
                    *prolog-flags*)
(<define> (get_prolog_flags_handle x) (<=> x *prolog-flags*))


(define (set-flag key val)
  (fluid-set! *prolog-flags* (cons (cons key val) (fluid-ref *prolog-flags*))))

(define (get-flag key)
  (let ((ret (assq key (fluid-ref *prolog-flags*))))
    (if ret
        (cdr ret)
        ret)))

(define *flags* #f)
(define (init-flags)
  (set! *flags* 
        `((,bounded                   #f ,false ,true ,false)
          (,auto_sym                  #f ,on    ,on   ,off)
          (,max_integer               #f ,(ash 1 60))
          (,min_integer               #f ,(- (ash 1 60)))
          (,integer_rounding_function #f ,towards_zero ,down 
                                             ,towards_zero)
          (,char_conversion           #t ,on ,on ,off)
          (,debug                     #t ,off ,on ,off)
          (,max_arity                 #f ,1024)
          (,unknown                   #t ,error ,error ,fail ,warning)
          (,double_quotes             #t ,atom ,chars ,codes ,atom)
	  (,generate_debug_info       #t ,false ,true ,false)
	  (,clpfd_monotonic           #t ,false ,true ,false)
	  (,clpfd_goal_expansion      #t ,true  ,true ,false)
	  (,clpfd_propagation         #t ,false ,full ,false)
	  (,clpb_validation           #t ,false ,true ,false)
	  (,debug_term_position       #t ,false ,true ,false)))
  (for-each 
   (lambda (x)
     (match x
       ((k _ default . _) (set-flag k default))))
   *flags*))
  

(set! do-character-convert
      (lambda ()
        (eq? (get-flag char_conversion) on)))

;; Setting up the default flags

(define p (lambda x #f))
(define cc (lambda x #t))

(define (check-num x)
  (if (integer? x)
      (let ((minI (get-flag min_integer))
            (maxI (get-flag max_integer)))
        (if (< x minI)
            (evaluation_error (fluid-ref *current-stack*) p cc))
        (if (> x maxI)
            (evaluation_error (fluid-ref *current-stack*) p cc))
        x)
      x))
  
(define (is-a-num? x)
  (cond
   ((number? x)
    x)
   (else
    (evaluation_error (fluid-ref *current-stack*) p cc))))

(define (check-flags k v static)
  (let ((r (assq k *flags*)))
    (if r
        (if (or static (list-ref r 1))
            (let ((r (cdddr r)))
              (if (null? r)
                  (number? v)
                  (member v r)))
            #f)
        #f)))

(<define> (set_prolog_flag tag value)
  (<let> ((tag   (<scm> tag))
          (value (<scm> value)))
    (cond
     ((<var?> tag)
      (instantiation_error))

     ((check-flags tag value #f)
      (<code> (set-flag tag value)))
    
     ((not (procedure? tag))
      (type_error atom tag))
     
     (else
      (<let> ((data (assq tag *flags*)))
        (cond
         ((not data)
          (domain_error prolog_flag tag))
         ((not (list-ref data 1))
          (permission_error modify flag tag))
         (else
          (<let> ((data (cdddr data)))
            (cond
             ((and (null? data) (not (number? value)))
              (type_error number value))
             ((and (pair? data) (not (member value data)))
              (domain_error flag_value (vector (list plus tag value)))))))))))))

(define-parser-directive-onfkn set_prolog_flag (prolog-flag stx l N M)
  (match (get.. "," l)
    (((#:atom k . _)
      (or (#:atom   v . _)
          (#:number v _ _)))
     (let ((k (module-ref (current-module) k))
           (v (if (number? v) v (module-ref (current-module) v))))
       (if (check-flags k v #t)
           (set-flag k v)
           (format #t "COMPILE-ERROR: wrong prolog flag ~a,~a at ~a~%"
                   k v (get-refstr N M))))))
  #f)

(define (mk-err nm) 
  (lambda (N M) 
    (format #f "at ~a in ~a , not a PI list" (get-refstr N M) nm)))

(define-PI dynamic       define-dynamic!       
  define-hash-dynamic! (mk-err "dynamic"))
(define-PI multifile     define-multifile!     
  define-hash-multifile! (mk-err "multifile"))
(define-PI discontiguous define-discontiguous! 
  define-hash-discontiguous! (mk-err "discontiguous"))

;; Do not do anything for these ones
(define-syntax-rule (define-multifile!          f) (define-dynamic! f))
(define-syntax-rule (define-discontiguous!      f) (define-dynamic! f))
(define-syntax-rule (define-hash-multifile!     f) (define-hash-dynamic! f))
(define-syntax-rule (define-hash-discontiguous! f) (define-hash-dynamic! f))

(<define> (current_prolog_flag flag value)
  (<let> ((flag (<lookup> flag)))
    (cond
     ((<var?> flag)
      (pr-member flag (map car *flags*))
      (<=> value ,(get-flag (<scm> flag))))
     ((or (procedure? flag) (char? flag) (string? flag))
      (if (get-flag flag)
          (<=> value ,(get-flag flag))
          (domain_error prolog_flag flag)))
     (else
      (type_error atom flag)))))

(set! (@@ (logic guile-log prolog names) is-a-num?) is-a-num?)
(set! (@@ (logic guile-log prolog names) check-num) check-num)
(set! (@@ (logic guile-log prolog names) get-flag ) get-flag)
(set! (@@ (logic guile-log prolog modules) op) op)
