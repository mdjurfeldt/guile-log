(define-module (logic guile-log prolog closed)
  #:use-module ((logic guile-log code-load) #:select (setup-closed))
  #:use-module (logic guile-log)
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log prolog names)
  #:use-module (logic guile-log dynamic-features)
  #:export(closed_closure error_at_closed_p_handle close_error_true close_error_false error-when-closed? closed-err))

(mk-sym closed_closure)

(define (err x y) (<wrap> permission_error closed_closure "=" (list x y)))
(<define> (closed-err x y) 
   (permission_error closed_closure "=" (list x y)))

(define error-when-closed? #f)
(if #t
    (begin
      (set! error-when-closed? (setup-closed err))
      (<wrap> add-fluid-dynamics error-when-closed?)))
      

(<define> (error_at_closed_p_handle x) (<=> x ,error-when-closed?))

(<define> (close_error_true)  (<code> (fluid-set! error-when-closed? #t)))
(<define> (close_error_false) (<code> (fluid-set! error-when-closed? #f)))
