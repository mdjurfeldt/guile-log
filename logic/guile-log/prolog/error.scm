(define-module (logic guile-log prolog error)
  #:use-module ((logic guile-log) 
                #:select (procedure-name
			  </.> <abort> <define> <match> <cut> <let> <cp>
			  S <pp> <lookup> <var?> <cc> <fail> <lambda> <fail>))
  #:use-module (ice-9 match)
  #:use-module (logic guile-log prompts)
  #:use-module (logic guile-log umatch)
  #:use-module (system repl error-handling)

  #:replace (error)
  #:export (type_error instantiation_error domain_error existence_error
                       permission_error list/plist? existence_error
                       *call-expression* representation_error syntax_error
                       scheme-wrapper evaluation_error throw-it system_error
		       user-exception-hook redo-wrapper))

(define-syntax fkn-it
  (syntax-rules (quote)
    ((_ 'x) 'x)
    ((_ (f x ...))
     (vector `(,f ,(fkn-it x) ...)))
    ((_ x) x)))

(define *debug* #t)
(define (call-with-eh th . l)
  (if *debug*
      (call-with-error-handling th)
      (th)))

(define error (lambda x (error "symbol is not defined")))

(define non-reentrant 
  (letrec ((f (</.> (<abort> 'prolog f 'reentrant_error))))
    f))

(define tag (make-prompt-tag))
(define G #f)
(define H #f)
(<define> (user-exception-hook x err) (err))

(define (ecp x s) (gp-cp x s))

(<define> (abort1 code)
   (user-exception-hook code
      (<lambda> ()
	;(if (<lookup> code) (<pp> `(abort ,code)) <cc>)
	(<abort> 'prolog non-reentrant (ecp code S)))))
	  
(define-syntax-rule (define-error (nm a ...) code)
  (define (nm s p cc a ...)
    (abort-to-prompt tag
      (lambda ()
        (G
         (lambda ()
           (catch #t
             (lambda ()
               (call-with-eh
                (lambda () 
		  (abort1 s p cc (fkn-it code)))))
             H)))))))

(define evaluation_error
  (case-lambda
    ((s p cc)
     (abort-to-prompt tag
       (lambda ()
         (G
          (lambda ()
            (catch #t
              (lambda ()
                (call-with-eh
                 (lambda () (<abort> 
                             s p cc 
                             'prolog non-reentrant 
                             (fkn-it (error evaluation_error 'iso-prolog))))))
              H))))))
    ((s p cc x)
     (abort-to-prompt tag
       (lambda ()
         (G
          (lambda ()
            (catch #t
              (lambda ()
                (call-with-eh
                 (lambda () (<abort> s p cc 
                                     'prolog non-reentrant 
                                     (fkn-it (error (evaluation_error x) 
                                                    'iso-prolog))))))
              H))))))))

(define-error (instantiation_error)    
  (error instantiation_error
         'iso-prolog))

(define-error (type_error a b)         
  (error (type_error a b)
         'iso-prolog))

(define-error (domain_error a b)       
  (error (domain_error a b)
         'iso-prolog))

(define-error (permission_error a b c) 
  (error (permission_error a b c)
         'iso-prolog))

(define-error (existence_error a b) 
  (error (existence_error a b)
         'iso-prolog))

(define-error (representation_error a)
  (error (representation_error a)
         'iso-prolog))

(define-error (syntax_error a)
  (error (syntax_error a)
         'iso-prolog))

(define-error (system_error)
  (error system_error
         'iso-prolog))

(define-error (throw-it a) a)

#;
(define-error (evaluation_error a )
  (error (evaluation_error a)
         'iso-prolog))
    
(<define> (list/plist? l)
  (<match> (#:mode - #:name list/plist?) (l)
    ((x . l) 
     (<cut> (list/plist? l)))
    (x
     (<let> ((x (<lookup> x)))
       (if (or (<var?> x) (null? x))
           <cc>
           (<cut> <fail>))))))
                             
;;This does not work with assoc lists e.g. can't lookup  
(define cc (lambda x #t))
(define p  (lambda x #f))
(define ariths '("=" "floor" "+" "-" "*" "/" "truncate/" "remainder"
                 "<" "<=" ">=" ">" "abs" "floor" "round" "ceiling" "truncate"
                 "inexact->exact"))
(define (id x) (x))

(define arith-ints '("ash" "logior" "logand" "lognot"  "modulo"))

(define number #f)
(define integer #f)
(define source_sink #f)

(define (scheme-wrapper- call-with-eh)
  (let ()
    (letrec ((g  (lambda (fkn)                  
                   (call-with-prompt tag
                      fkn
                      (lambda (k f)
                        (g f)))))
             
             (h  (lambda x
		   (define-syntax-rule (wrap f)
                     (abort-to-prompt tag
                                      (lambda ()
                                        (catch #t
                                          (lambda () 
                                            (call-with-eh f))
                                          h))))

                   (let ((s (fluid-ref *current-stack*)))                     
                     (match x
                       ;; To avoid an inifinite recursion
                       (('misc-error _ _ (_ 123) _)
                        x)

                       #;(('wrong-type-arg #f . _))
                       (('system-error "open-file" pat
                                       (_ file) (2))
                        (wrap (existence_error s p cc source_sink file)))

                       (('numerical-overflow . _)
                        (wrap (evaluation_error s p cc 'num)))

                       (('wrong-type-arg fkn str ("exact integer" val) . _)
                        (wrap (type_error s p cc integer val)))

                       (('wrong-type-arg fkn str (arg val) . _)
                        (let ((val (gp-lookup val s)))
                          (cond                         
                           ((gp-var? val s)
                            (wrap (instantiation_error s p cc)))

                           ((member fkn ariths)
                            (wrap (type_error s p cc number val)))

                           ((member fkn arith-ints)
                            (wrap (type_error s p cc integer val)))

                           (else
                            (wrap (syntax_error s p cc (format #f "~s" x)))))))

                       (_ (wrap (syntax_error s p cc (format #f "~s" x)))))))))

      (set! G g)
      (set! H h)
      (lambda (thk) (g (lambda () 
                         (catch #t 
                           (lambda () 
                             (call-with-eh thk))
                           h)))))))
(define scheme-wrapper (scheme-wrapper- call-with-eh))
(define redo-wrapper   (scheme-wrapper- (lambda (f) (f))))

(define *call-expression* (gp-make-var #f))
