(define-module (logic guile-log prolog base)
  #:use-module (logic guile-log parsing operator-parser)
  #:use-module (logic guile-log guile-log-pre)
  #:use-module (ice-9 eval-string)
  #:use-module ((system base compile) #:select ((compile . scm-compile)))
  #:use-module (ice-9 time)
  #:use-module (logic guile-log guile-prolog closure)
  #:use-module (logic guile-log prolog order)
  #:use-module ((srfi srfi-1) #:select (lset-union 
                                        lset-intersection 
                                        lset-difference))
  #:use-module (logic guile-log functional-database)
  #:use-module (logic guile-log prolog pre)
  #:use-module (logic guile-log prolog run)
  #:use-module (logic guile-log prolog var)
  #:use-module (logic guile-log prolog goal)
  #:use-module (logic guile-log prolog directives)
  #:use-module (logic guile-log prolog parser)
  #:use-module (logic guile-log prolog symbols)
  #:use-module (logic guile-log prolog varstat)
  #:use-module (logic guile-log prolog modules)
  #:use-module (logic guile-log prolog dynamic)
  #:use-module ((logic guile-log prolog names)
                #:select (make-unbound-fkn end_of_file make-sym))
  #:use-module (logic guile-log umatch)
  #:use-module ((logic guile-log) #:renamer (lambda (x)
					      (if (eq? x '<_>)
						  'GL:_
						  (if (eq? x 'umatch)
						      'umatch-guile-log
						      x))))
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:use-module (logic guile-log parser)
  #:export (compile-prolog-string compile-prolog-file 
				  trace-level 
				  trace read-prolog-term save-operator-table
				  Trace Level trace-level
				  eval_when))


(define-syntax-rule (stp x) 
  (syntax-parameterize ((S (lambda (z) #'(fluid-ref *current-stack*))))
    x))
						
(define do-print #f)
(define pp
  (case-lambda
   ((s x)
    (when do-print
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when do-print
      (pretty-print (syntax->datum x)))
    x)))

(define ppp
  (case-lambda
   ((s x)
    (when #t
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when #t
      (pretty-print (syntax->datum x)))
    x)))

(define-syntax-rule (with-fluids2 p ((q v w) ...) code ...)
  (let ((q (fluid-ref v)) ...)
    (dynamic-wind
      (lambda () #f)
      (lambda () code ...)
      (lambda ()
	(if (not p)
	    (begin
	      (fluid-set! v q)
	      ...))))))
	    
      

(define-syntax-rule (G x) #'(@ (guile) x))
(define (mk-scheme stx s l unq?)
  (let* ((sym (eval-string (string-append "'" l) #:lang 'scheme))
         (lam (if unq? (lambda (x) #`(unquote #,x)) (lambda (x) x)))
         (w   (datum->syntax stx sym))
         (v   #`(lambda () #,w)))
    (case s
      ((do)
       (lam #`(#,(G vector) 
	       (#,(G list)
		(@@ (logic guile-log prolog var) do-eval-scheme)
		#,v))))
      ((when)
       (lam #`(#,(G vector)
	       (#,(G list)
		(@@ (logic guile-log prolog var) when-eval-scheme)
		#,v))))
      ((v var)
       (fluid-set! v-variables (union (list sym) (fluid-ref v-variables)))
       (lam w))
      ((s scm)
       (lam w)))))

(define (add x y z n m)
  (if z
      `((xfy _ "," _) ,x ((xfy _ "," _) ,y ,z ,n ,m) ,n ,m)
      ` ((xfy _ "," _) ,x ,y ,n ,m)))

(define (goal stx x)
  ((@ (logic guile-log prolog goal) goal) stx x))

(define (union        x y) (lset-union        eq? x y))
(define (intersection x y) (lset-intersection eq? x y))
(define (diff         x y) (lset-difference   eq? x y))

(define (vvdiff l) (let ((r (diff (fluid-ref v-variables) l)))
		     (fluid-set! v-variables r)
		     r))

(define trace-level 0)
(define *trace* (make-fluid #f))
(define *eval-only* #f)

(define (maybe_add_call z)
  (if *eval-only*
      `(#:term (#:atom call  #f #f 0 0) ,z #f 0 0)
      z))

(<define> (trace-fkn b f lev . l)
  (if (<= lev trace-level )
      (<and>
       (<pp-dyn> `(,b enter ,f) `(,b leave ,f))
       ((@ (logic guile-log iso-prolog) write) (list b 'trace f l))
       ((@ (logic guile-log iso-prolog) nl)))
      <cc>))

(define-syntax-parameter Trace (lambda (x) #f))
(define-syntax-parameter Level (lambda (x)  0))
(define-syntax-parameter Fkn   (lambda (x) #''unbound-fkn))

(define-syntax trace
  (syntax-rules ()
    ((trace f level)
     (syntax-parameterize ((Trace (lambda (x) #'#t))
			   (Level (lambda (x) #'level)))
	(mktr Fkn f))) 		  
    ((trace f)
     (syntax-parameterize ((Trace (lambda (x) #'#t)))
        (mktr Fkn f)))))

(define (tr-meta f fnew)     
  (set-object-properties! fnew (object-properties f))
  (if (procedure? f)
      (set-procedure-properties! fnew (procedure-properties f)))
  fnew)

(define-syntax-rule (mktr f xx)
  (let ((ff f))
    (if Trace
      (tr-meta
       ff
       (<lambda> x                       
	  (<apply> trace-fkn 'in ff Level x)
	  (<apply> xx x)
	  (<apply> trace-fkn 'out ff Level x)))
      xx)))

(define (define-or-set! xxf fkn)
  (let* ((bd?    (module-locally-bound? (current-module) fkn))
	 (sf     (case-lambda 
		   (()  xxf)
		   ((f) (set! xxf f)))))
    (set-procedure-property! xxf 'debug-fkn sf)
    
    (if bd?
	(module-set! (current-module) fkn xxf)
	(define! fkn xxf))

    (set-procedure-property! xxf 'module (module-name (current-module)))
    (set-procedure-property! xxf 'shallow #t)
    (set-procedure-property! xxf 'name fkn)))

(define (-define-or-set! xxf fkn) (define-or-set! xxf fkn))

(define (define-or-set-fkn! f xxf)
  (let* ((bd?  (module-locally-bound? (current-module) f))
	 (m    (current-module))
	 (sf     (case-lambda 
		   (()   (module-ref m f))
		   ((fk) (module-set! m f fk)))))
      (set-procedure-property! xxf 'debug-fkn sf)
      (if bd?
	  (module-set! (current-module) f xxf)
	  (define! f xxf))
      (set-procedure-property! xxf 'module (module-name (current-module)))
      (set-procedure-property! xxf 'shallow #t)
      (set-procedure-property! xxf 'name f)))

        

(define lambdas (make-fluid '()))
(define functors '())
(define (add-lambda x)
  (define (g n1)
    (let lp ((l (fluid-ref lambdas)))
      (match l
        (((n2 a2 b3) . l)
         (if (eq? n1 n2)
             #f
             (lp l)))
        ((#(n2 a2) . l)
         (if (eq? n1 n2)
             #f
             (lp l)))
        (()
         (fluid-set! lambdas (cons x (fluid-ref lambdas)))))))

  (match x
    ((n1 a1 b1)
     (g n1))
    (#(n1 a1)
     (g n1))))

(eval-when (compile)
  (define-syntax-rule (wrapu s1 code ...)
    (let* ((s1 (gp-newframe (fluid-ref *current-stack*)))
	   (s2 (gp-newframe s1)))
      (with-fluids ((*current-stack* s2))
	(let ((ret (begin code ...)))
	  (gp-unwind-tail s1)
	  ret)))))

(define* (compile stx l #:optional (name #f) (lam? #f) (closed? #f))
  (define (less x y)
    (match (pp 'less-x x)
      ((_ #:translated n x) 
       (match (pp 'less-y y)
         ((_ #:translated m y)
          (< n m))
         (_ #t)))

      ((_ #f #f _ _)
       #t)      

      ((_ f xa xb _)
       (match (pp 'less-y y)
	 ((_ #:translated _ _)
	  #f)

	 ((_ #f #f _ _)
	  #f)

	 ((_ g ya yb _)
	  (let ((fstr (symbol->string f))
		(gstr (symbol->string g)))
	    (if (string< fstr gstr)
		#t
		(if (string= fstr gstr)
		    (< (length xa) (length ya))
		    #f))))))))

  (define (flatten x)
    (match (pp 'flatt x)
      (((#:include i) . l)
       (append i (flatten l))) 
      ((x . l)
       (cons x (flatten l)))
      (() '())
      (x  (error "match error in flatten"))))
       
  (define simple-lam #f)

  (define (ext f)
    #f)

  (define (get-fu stx v l)
    (let ((l (get.. "," l)))
      (cons
       (pat-match stx v)
       (if (and (pair? l) (not (null? (car l))))
	   (map (lambda (x) (pat-match stx x)) l)
	   '()))))
  
  
  (define (assert-source x ext)
    (let ((res (wrapu s
		 (assertz-source s		
		    (lambda () (error "failed to compile assertz"))
		    (lambda (s p x) x)
		    stx
		    (let lp ()
		      (catch #t
			(lambda ()
			  (term-init-variables)
			  (let ((code (pp (term stx x)))
				(vs   (map
				       (lambda (x)
					 (list (datum->syntax stx x)
					       #`((@ (logic guile-log umatch)
						     gp-make-var))))
				       (term-get-variables))))
			    (pp (eval
				 (pp `((@@ (logic guile-log prolog base) stp)
				       ((@ (guile) let-syntax)
					((f ((@ (guile) lambda) (z)
					     ,#`(let #,vs `#,code))))
					f)))
				 (current-module)))))
			(lambda x
			  (match x
			    (('unbound-variable _ _ (nm) _)
			     (make-sym (current-module) nm)
			     (lp))
			    (_
			     (error x))))))
		    ext))))
      (pp #`(let* ((fr1 (gp-newframe (fluid-ref *current-stack*)))
		   (fr2 (gp-newframe fr1)))
	      (with-fluids ((*current-stack* fr2))
		 ((<lambda> () #,res) fr2 (lambda () #f) (lambda x #t))
		 (gp-unwind-tail fr1))))))
		
  (define (top x i)    
    (match (pp 'top x)
      ((('fy _ "-" _) (and sym (#:atom v . _)) n m)
       (add-sym #f #f sym)
       (set! functors (cons (get-fu stx sym '()) functors))
       #f)

      ((('fy _ "-" _) (#:term (and sym (#:atom v . _))  w . _) n m)
       (add-sym #f #f sym)
       (set! functors (cons (get-fu stx sym w) functors))
       #f)

      ((('fx _ ":-" _) _ n m)
       (warn "COPMILE ERROR: now known :- directive at ~a" (get-refstr n m))
       #f)

      ((('xfy _ "|"  _) args rs n m)
       (set! simple-lam #t)
       (top `(((xfx _ ":-" _) 
	       (#:term (#:atom simple-1276 #f #f ,n ,m) ,args #f ,n ,m) 
	       ,rs ,n ,m)) i))

      ((('xfx _ "-->" _) (#:term (and sym (#:atom v . _)) y . l) z n m)
       (let ((In  (list #:variable (gensym "In")  n m))
	     (Out (list #:variable (gensym "Out") n m)))	 
	 (top
	  `((xfx _ ":-" _)
	    (#:term ,sym ,(add In Out y n m) ,@l)
	    ,(dcg In Out z) 
	    ,n ,m) i)))
       
      (((and opp ('xfx _ ":-" _)) ((_ _ (and op (not ":")) _) a b n2 m2) z n m)
       (top (list opp (list #:term (list #:atom (string->symbol op) #f #f n2 m2)
			    (add a b #f n2 m2) #f n2 m2) z n m) i))

      ((('xfx _ ":-" _) (#:term (and sym (#:atom v . _)) y . _) z n m)
       (add-sym #f #f sym)
       (if (is-dynamic? v)
	   (let ((f functors))
	     (set! functors '())
	     `(,i #:translated 1 ,(assert-source x (ext f))))
           (let ((fu (pp v functors)))
	     (set! functors '())
	     (match y
		    (()
		     (list i v '() (maybe_add_call z) fu))
		    (_
		     (list i v (get.. "," y) (maybe_add_call z) fu)))
	     )))

      (((and op ('xfx _ ":-" _)) ((_ _ ":" _) (#:atom mod . _)
				 (and x
				      (#:term (and sym 
						   (#:atom v _ _ na ma))
					      y . l))
				 . _) z n m)
       (if (is-dynamic? mod v)
	   (let ((x (list op
			  (cons* #:term 
				 (list #:atom 
				       v '@@ 
				       (ref-module-name mod)
				       na ma)
				 y l)
			  z n m)))
				       
	     `(,i #:translated 
	       1 ,#`(prolog-run-* 
		     (namespace-switch (ref-module '#,(datum->syntax stx mod))
		        (<lambda> ()
			  #,(mk-rhs stx
				    `(#:term (#:atom assertz #f #f ,n ,m)
					     ,x #f ,n ,m)))))))
	   (error "Can't define a non multifile goal in different files"
		  mod sym)))

      ((('xfx _ "-->" _) (and sym (#:atom v . _)) z n m)
       (let ((In  (list #:variable (gensym "In")  n m))
	     (Out (list #:variable (gensym "Out") n m)))
	 (top
	  ` ((xfx _ ":-" _)
	     (#:term ,sym ,(add In Out #f n m) #f ,n ,m)
	     ,(dcg In Out z) 
	     ,n ,m) i)))

      ((('xfx _ ":-" _) (and sym (#:atom v . _)) z n m)
       (add-sym #f #f sym)
       (if (is-dynamic? v)
	   (let ((f functors))
	     (set! functors '())
	     `(,i #:translated 1 ,(assert-source x (ext f))))
	   (let ((fu (pp v functors)))
	     (set! functors '())
	     (list i v '() z fu))))
	   
      ((('xfx _ ":-" _) (((_ _ op _) a b _ _)) z _ _)
       (if (is-dynamic? (string->symbol op))
           (error "dynamic operator not supprted")
	   (let ((fu functors))
	     (set! functors '())
	     (list i (string->symbol op) (list a b) (maybe_add_call z) fu))))


      ((('xfx _ ":-" _) (((_ _ op _) a _ _)) z _ _)
       (if (is-dynamic? (string->symbol op))
           (error "dynamic operator not supported")
	   (let ((fu functors))
	     (set! functors '())
	     (list i (string->symbol op) (list a) z fu))))

      ((('fx _ ":-" _) z . _)
       (let ((fu functors))
	 (set! functors '())
	 (list i #f #f z fu)))

      ((#:translated n (#:init (x)))
       (list
	i
	#:translated n
	(list
	 i
	 #:init
	 #`(prolog-run-* #,(mk-rhs stx x)))))
      
      ((#:translated n (#:init x))
       (list
	i
	#:translated n
	(list
	 #:init
	 #`(prolog-run-* #,(mk-rhs stx x)))))

      ((#:translated n (#:include fn x))
       (set! functors '())
       (let ((res (list i #:include (map top x))))
	 (set! functors '())
	 res))

      ((#:translated _ _)
       (cons i x))
      
      ((and sym (#:atom v _ _ n m))
       (add-sym #f #f sym)
       (if (is-dynamic? v)
           `(,i #:translated  1 ,(assert-source x #f))
           (list i v '() '() '())))

      (((_ _ ":" _) (#:atom mod . _) (#:term (#:atom v _ _ na ma) y . l)
	n m)
       (if (is-dynamic? mod v)
	   (let ((x (cons* #:term 
			   (list #:atom
				 v
				 '@@
				 (ref-module-name mod)
				 na ma)
			   y  l)))
	     
	     `(,i #:translated 
	       1 ,#`(prolog-run-* 
		     (namespace-switch (ref-module #,(datum->syntax stx mod))
			(<lambda> ()
			      #,(mk-rhs stx
				    `(#:term (#:atom assertz #f #f ,n ,m)
					     ,x #f ,n ,m)))))))
	   (error "Can't define a non multifile goal in different files II"
		  mod v)))
          	  
      (((_ _ (and op (not ":")) _) x y n m)
       (top (list #:term 
		  (list #:atom (string->symbol op) #f #f n m)
		  (add x y #f n m) #f n m) i))

      ((#:term (and sym (#:atom v . _)) y _ n m)
       (add-sym #f #f sym)
       (if (is-dynamic? v)
           `(,i #:translated 1 ,(assert-source x #f))
	   (let ((fu functors))
	     (set! functors '())
	     (list i v (get.. "," y) '() fu))))
      
      ((_) (top (car x) i))
      #;((rs)
       (set! simple-lam #t)
       (top `(((xfx _ ":-" _) 
                (#:term (#:atom simple-1277 #f #f 0 0) () #f 0 0) 
                (,rs) 0 0))))

      #;(rs
       (set! simple-lam #t)
       (top `(((xfx _ ":-" _) 
                (#:term (#:atom simple-1277 #f #f 0 0) () #f 0 0) 
                (,rs) 0 0))))))
  
  (define (remfalse x)
    (let ((x x))
     (match x
      ((#f . l) (remfalse l))
      ((x  . l) (cons x (remfalse l)))
      (()       '()))))

  (define (order x)
    (map cdr (stable-sort x (lambda (x y) (< (car x) (car y))))))
     

  (define-syntax-rule (wi x) (if lam? x (with-fluids ((lambdas '())) x)))

  (define (mcar x) (if x x '()))
  (pp 'compile-1 l)
  (if l (begin
  (with-fluids2 lam? ((vv v-variables '()) (la lambdas '()))			
   (let* ((l-r (pp 'l-r (stable-sort 
			 (remfalse 
			  (pp 'toped 
			      (flatten
			       (map top (pp 'compile-2 
					    (mcar l))
				    (iota (length (mcar l)))))))
			 less)))

	  (in.r (pp 'in.r
		    (let lp ((l-r l-r) (def '()) (r  '()) (mod '()))
		      (match (pp 'ini l-r)
			     (((_ #:translated _ (#:module x)) . l)
			      (lp l def r mod))

			     (((i #:translated _ (#:init x)) . l)
			      (lp l def (cons (cons i x) r) mod))
		     
			     (((i #:translated n x) . l)
			      (lp l (cons (cons i x) def) r mod))
			     (x 
			      (cons* def r x mod))))))

	  (ini  (reverse (car  in.r)))
	  (evl  (reverse (cadr in.r)))
	  (l-r  (caddr in.r))
	  (mod  (cdddr in.r))
	  (com (pp 'com 
		   (let lp ((f #f) (xx #f) (l l-r) (r '()) (rl '()) (res '()) 
		       (fuu '()))
		(define (next l x y fu)
		  (if (= (length x) (length xx))
		      (lp f x l (cons (cons x y) r) 
			  rl                    res (pp f (append fu fuu)))
		      (lp f x l (list (cons x y))   
			  (cons (reverse r) rl) res (pp f (append fu fuu)))))
		
		(match (pp 'com l)
		 (((i v x y fu) . l)
		  (if f
		      (if (eq? v (cdr f))
			  (next l x y fu)
			  (if (pair? r)
			      (lp (cons i v) x l (list (cons x y)) '()
				  (cons (cons* f fuu
					      (reverse (cons (reverse r) rl)))
					res) fu)
			      (lp (cons i v) x l (list (cons x y)) '()
				  (cons (cons* f fuu (reverse rl))
					res) fu)))
		      (lp (cons i v) x l (list (cons x y)) rl res
			  (append fu fuu))))
		 (()
		  (if f
		      (let* ((rl_ (if (null? r) 
				      (cons* f fuu (reverse rl))
				      (cons* f fuu (reverse 
						    (cons (reverse r) rl)))))
			     (res_ (if (null? rl_)
				       (reverse res)
				       (reverse (cons rl_ res)))))
			
			res_)
		      '())))))))	  

    (let* ((last-name #f)           
           (nm        (if name name
                          (gensym "closure-")))

           (l         (map (gen-fkn stx lam? (if simple-lam nm #f)
                                    (lambda (x) (set! last-name x)))
                           (pp 'com com)))

           (name      (if simple-lam
                          nm
                          (if name name
                              (if last-name last-name
                                  nm)))))
                          
      (if lam?     
          (let* ((nm   (datum->syntax stx name))
                 (vs   (fluid-ref v-variables))
                 (vs   (let lp ((vs vs))
                         (match vs
                           ((x . l)
                            (if (memq x l)
                                (lp l)
                                (cons x (lp l))))
                           (() '()))))
                 (vstx (map (lambda (x) 
                              (datum->syntax stx x))
                            vs)))
            (add-lambda (list name
                              vs
                              (pp 'closure
				  #`(letrec 
					((parent
					  (lambda #,vstx
					    ((@@ (logic guile-log prolog base) twist)
					      #,@(order (append l evl))
					      (make-prolog-closure
					       #,nm parent 
					       (#,(G list)
						#,@vstx) #,closed?)))))
				      parent))))
            (pp 'res #`(let () #,@ini (#,nm #,@vstx))))
          (with-syntax (((lam-def ...)
                         (let lp ((l (fluid-ref lambdas)))
                           (match l
                             (((nm vs lam) . l)
                              (cons (with-syntax ((nmm (datum->syntax stx nm)))
                                      #`(syntax-parameterize
                                         ((Fkn (lambda (x) #''nmm)))
                                         #,(mk-lam stx nm lam)))
                                    (lp l)))
                             ((#(nm x) . l)
                              (cons x (lp l)))
                             (() '())))))

	     (let* ((syms (get-syms))
		    (syms (union syms syms)))
	       (pp 'res #`(begin
                            #,@mod
			    (eval-when (compile load eval)
				       (add-non-defined 
					(quote #,(datum->syntax stx syms))))
			    lam-def ... #,@(order (append l ini evl)))))))))))
      #''compile-error))

(define-syntax save-operator-table
  (lambda (x)
    (syntax-case x ()
      ((_)
       #`(assq->ops #,(ops->assq))))))

(define-syntax-rule (map2 f x ...)
  (let ((F f))
    (map (lambda (x ...) (map F x ...)) x ...)))


(define (mk-rhs stx rhs)
  (with-varstat
   (let* ((var       (get-variables (list rhs)))
	  (l.v.code  ((get-rhs stx) '() var rhs))
	  (loc       (list-ref l.v.code 0))
	  (var       (list-ref l.v.code 1))
	  (code      (list-ref l.v.code 2)))
     #`(<let> #,(map (lambda (v) #`(#,(datum->syntax stx v) #f))
		     '() #;loc)
	(<var> #,(map (lambda (x) (datum->syntax stx x)) (append var loc))
	   #,code)))))
    
(define (mk-lam stx nm lam)
  (add-sym #f #f nm)
  (with-syntax ((dir (datum->syntax stx (module-name (current-module))))
                (nm2 (datum->syntax stx nm)))
    #`(begin
        (define-or-set! #,lam Fkn)
        (set-procedure-property! nm2 'module 'dir)
        (set-procedure-property! nm2 'shallow #t)
	Fkn)))

(define-syntax apply-fu
  (syntax-rules (unquote)
    ((_ ((fu a ...) . l) code)
     (`fu (apply-fu l code) `a ...))
    ((_ () code) 
     code)))

;; This is matched from the toplevel, got syntax issues e.g. eval-when does
;; Not match on symbols, but syntaxes
(define-syntax eval_when 
  (lambda (x)
    (syntax-case x ()
      ((eval_when f a ...)
       #'f))))

(define (gen-fkn stx lam? nm nm-store)
  (lambda (com)
   (match com
    (((i . f) fu . (((l . r) ...) ...))
     (define lams '())
     (define vs   '())
     (define ret
       (with-fluids ((lambdas '()))
         (let* ((f     (if nm nm f))
		(fu    (reverse fu))
                (v-l   (pp 'v-l   (map2 get-variables l)))
                (v-r   (pp 'v-r   (map2 (lambda (x)
                                          (get-variables (list x))) r)))
                (v-new (pp 'v-new (map2 difference v-r v-l)))
                (v-all (union v-l v-r))
                (lhs   (pp 'lhs   (map2 (get-lhs stx) l)))
                (loc.v.rhs (pp 'rhs   (map2 (get-rhs stx) v-l v-new r)))
		(loc   (map2 (lambda (x)
			       (map (lambda (x)
				      (datum->syntax stx x))
				    (list-ref x 0)))
			     loc.v.rhs))
		(v-new (map2 (lambda (x)
			       (map (lambda (x)
				      (datum->syntax stx x))
				    (list-ref x 1)))
			     loc.v.rhs))
		(rhs   (map2 (lambda (x) (list-ref x 2)) loc.v.rhs)))
           
           (let lp ((ls (fluid-ref lambdas)))
             (match ls
               (((nm vs lam) . ls)
                (let* ((v-int (intersection vs v-all))
                       (v-out (diff         vs v-int)))
                  (set! vs (union v-out vs))
                  (set! lams (cons (vector nm
                                           (with-syntax
                                               ((nmm (datum->syntax stx nm)))
                                             #`(syntax-parameterize
                                                ((Fkn (lambda (x) #''nmm)))
                                                #,(mk-lam stx nm lam))))
                                   lams))
                  (lp ls)))
                           
               ((x .lams)
                (set! lams (cons x lams)))

               (() #t)))

           (count-variables (map (lambda (x) (cons x 0)) v-all) (cons l r))
           (with-syntax (((((lhs ...) ...) ...) lhs)
                         (((rhs  ...) ...)      rhs)
			 ((((loc ...) ...) ...) loc)
                         ((((v   ...) ...) ...) v-new)
                         (fstx                 (datum->syntax stx f)))

	     (when (member f (string->list ",;"))
	       (warn "redifing , or ;, mayby . => , is needed"))
		     
				 
             (if lam?
                 (begin
                   (nm-store f)
                   (pp 'fkn
                   #`(syntax-parameterize ((Fkn (lambda (x) #''fstx)))
		      (define fstx (apply-fu #,fu
                       (<<case-lambda-dyn-combined>> fstx
                        ((lhs ...       
			      (<let> ((loc #f) ...)
                                 (<var> (v ...) 
                                     rhs))) ...)
                        ...)))
		      #f)))
             
		   (let* ((fuu (syntax-case fu (eval_when unquote)
			       (((,eval_when . a) . f)
				#'f)
			       (x #'x)))
			(ret 
			 #`(syntax-parameterize ((Fkn (lambda (x) #''fstx)))
			     (define-or-set! 
                               (apply-fu #,fuu
				 (<<case-lambda-dyn-combined>> fstx
				   ((lhs ...
					 (<let> ((loc #f) ...)
					   (<var> (v ...) 
					     rhs))) ...)
				   ...))
                               'fstx)
			     #f)))
      
		   (syntax-case fu (eval_when unquote)
		     (((,eval_when ,args ...) . l)
		      #`(eval-when #,(map (lambda (x)
					    (datum->syntax #'eval_when 
					      (syntax->datum x)))
					  #'(args ...))
			     #,ret))
		     (else
		      ret))))))))
			
     (let lp ((l lams))
       (match l
         ((x . l)
          (add-lambda x)
          (lp l))
         (() #t)))

     (when (pair? vs)
       (if 
        lam?
        (fluid-set! v-variables (union vs (fluid-ref v-variables)))
        (error 
   "v[.] used by first level function - not supported or use scm[.] in stead")))
     (cons i ret)))))




           

(define (union v1 v2)
  (define tab  (make-hash-table))
  (define (fold k v l) (cons k l))
  (for-each (lambda (x) (hashq-set! tab x #t)) v1)
  (for-each (lambda (x) (hashq-set! tab x #t)) v2)
  (hash-fold fold '() tab))
  
(define difference  
  (lambda (x y)
    (define x-tab (make-hash-table))
    (define (fold k v l)
      (if v
	  (cons k l)
	  l))
    (define (f x p)
      (for-each
       (lambda (x) (hash-set! x-tab x p))
       x))

    (f x #t)
    (f y #f)
    (hash-fold fold '() x-tab)))

(define (count-variables vs l)
  (define tab (make-hash-table))
  (define (coll x) (hashq-set! tab x 0))
  (for-each coll vs)
  (let lp ((l l))
    (match l
      ((#:variable x . _)
       (hashq-set! tab x (+ (hashq-ref tab x 0) 1)))
      ((x . l)
       (lp x) (lp l))
      (_ #t)))
  (let lp ((l l))
    (match l
      ((#:variable x n m)
       (when (and (not (eq? x '_)) (= (hashq-ref tab x 0) 1))
	 (warn (format #f "At ~a, Variable ~a only used one time"
		       (get-refstr n m) x))))
      ((x . l)
       (lp x) (lp l))
      (_ #t))))

(define (get-variables exp)
  (define vs (make-hash-table))
  (define (add v)
    (hash-set! vs v #t))
 
  (define (loop exp)
    (match (pp 'get-variables exp)
      (((_ . _) x y _ _)
       (loop x)
       (loop y))
      (((_ . _) x _ _)
       (loop x))
            
      ((#:lam-term (or (#:atom s . _) (and #f s)) l closed? _ _)
       (loop l))
      
      ((#:group x)
       (loop x))

      ((#:variable x . _) 
       (if (not (eq? x '_))
	   (add x)))
      ((#:termvar v _ l . _)
       (add v)
       (loop l))

      ((#:termstring v l . _)
       (loop l))

      ((#:fknfkn a b)
       (loop a)
       (loop b))

      ((#:term v l . _)
       (loop l))
      ((#:list l . _)
       (loop l))
      ((x)
       (loop x))
      (_ #t)))

  (map loop exp)
 
  (hash-fold (lambda (k v l) (cons k l)) '() vs))

(define (get-lhs stx)
  (lambda (x)
    (map (lambda (x) (pat-match stx x)) x)))
      
(define (get-rhs stx)
  (lambda (vl vs x) 
    (with-varstat
     (init-first-variables)
     (for-each first-variable! vl)
     (register-variables x)
     (let ((code  (goal stx x))
	   (loc   (let lp ((var vs))
		   (if (pair? var)
		       (if (local-variable? (car var))
			   (cons (car var) (lp (cdr var)))
			   (lp (cdr var)))
		       '())))
	   (var   (let lp ((var vs))
		    (if (pair? var)
			(if (not (local-variable? (car var)))
			    (cons (car var) (lp (cdr var)))
			    (lp (cdr var)))
			'()))))
       (list loc var code)))))


(define-syntax compile-prolog-string
  (lambda (x)
    (syntax-case x ()
      ((n str)
       (wrapu s
	 (with-syms
	  (compile x
		   (prolog-parse x (syntax->datum #'str)))))))))

(define (re-compile stx str nm closed?)
  (if (string? str)
      (let ((str (string-trim-right str)))
	(when (not (eq? (string-ref str
				    (- (string-length str) 1))
			#\.))
	  (set! str (string-append str ".")))    
	(compile stx
		 (prolog-parse stx str)
		 nm #t closed?))     
      (compile stx (match str
		     (((_ _ "|" . _) . _)
		      (list str))
		     (_ str))
	       nm #t closed?)))

(set! (@@ (logic guile-log prolog var ) compile-lambda) re-compile)
(set! (@@ (logic guile-log prolog goal) compile-lambda) re-compile)

(define-syntax compile-prolog-file
  (lambda (x)
    (syntax-case x ()
      ((n str . l)
       (with-input-from-file (syntax->datum #'str)
	 (lambda ()
           (with-fluids ((*prolog-file* (syntax->datum #'str)))
	     (wrapu s
	       (with-syms
		(compile x
			 (prolog-parse x)))))))))))

(eval-when (compile eval load)
 (define lamman (make-fluid)))

(define mod (current-module))
        
(define burp (make-fluid #f))

(define (read-prolog-term state stream module)
  (let ((stx (vector 'syntax-object 'a '((top)) 
		     (cons* 'hygiene (module-name module)))))
    (with-fluids ((lambdas '()))
      (begin
	(term-init-variables)
        (with-syms
	 (let* ((r  (pp 'parse (prolog-parse-read stream stx))))
	   (if (and (pair? r) (pair? (car r)))
	       (let* ((r   (pp 'term (term stx (reverse (car r)))))
		      (vl  (pp 'vl   (term-get-variables-list)))
		      (vl  (union vl (fluid-ref v-variables)))			    
		      (vs  (union
                            (fluid-ref v-variables)
                            (pp 'vs   (term-get-variables))))
		      (h   (make-hash-table))
		      (w   (map (lambda (x) 
				  (let ((r (gp-make-var))) 
				    (hash-set! h x r)
				    r))
				vs))
		      (s   (make-hash-table))
		      (wl  (let lp ((vl vl) (r '()))
			     (if (pair? vl)
				 (let ((x (car vl)))
				   (if (hash-ref s x)
				       (begin
					 (hash-set! s x 1)
					 (lp (cdr vl) r))
				       (begin
					 (hash-set! s x 0)
					 (lp (cdr vl) (cons x r)))))
				 (reverse! r))))
		      (ws  (let lp ((vs vs))                            
			     (if (pair? vs)
				 (let ((x (car vs)))
				   (if (hash-ref s x)
				       (cons (cons x  (hash-ref h x))
					     (lp (cdr vs)))
				       (cdr vs)))
				 '())))
		      (burp #f))
                
		 (add-non-defined (get-syms))

		 (pp 'lambdas (fluid-ref lambdas))
		 ;; Make sure to define closure parents in current module
		 (let lp ((lams (fluid-ref lambdas)) (done '()))
		   (match lams
		     (((nm vs lam) . lams)
                      (if (memq nm done)
                          (lp lams done)
                          (let ((fkn 
                                 (eval
                                  `(let-syntax 
                                       ((f
                                         (lambda (x)
                                           ,((@@ (logic guile-log prolog base) 
                                                 pp) 'lammit lam))))
                                     f)
                                  (current-module))))
                            (define-or-set-fkn! nm fkn)
                            (lp lams (cons nm done)))))
		     
		     ((#(nm lam) . lams)
                      (if (memq nm done)
                          (lp lams done)
                          (let ((fkn 
                                 (eval
                                  `(let-syntax 
                                       ((f
                                         (lambda (x)
                                           ,((@@ (logic guile-log prolog base) 
                                                 pp) 'lammit lam))))
                                     f)
                                  (current-module))))
                            ;(define-or-set-fkn! nm fkn)
                            (lp lams (cons nm done)))))
		     
		     (() #t)))
		   
                 (set! burp 
                   (pp 'burp
		       #`(lambda (s123 #,@(map 
					   (lambda (x)
					     (datum->syntax stx x))
					   vs))
			   `#,r)))

		 (values
		  (let ((r 
			 (apply
			  (eval 
			   `((lambda () (let-syntax ((fexpr (lambda (x) ,burp)))
					  fexpr)))
			   mod)
			  state w)))
		    r)
		  w wl ws))
	       end_of_file)))))))

(define (add-non-defined l)
  (when #t (eq? (get-flag 'auto_sym) 'on)
    (let lp ((l l))
      (match l
        ((x . l)
         (let ((mod (current-module)))
           (unless (module-defined? mod x)
             (let ((f (make-unbound-fkn x)))
               ;(format #t "Defined non defined variable ~a~%" x)
               (module-define! mod x f)
               (set-procedure-property! f 'module (module-name mod))
               (set-procedure-property! f 'shallow #t)
               (set-procedure-property! f 'name x))))
         (lp l))
        (() #t)))))

(define-syntax-rule (twist (sy a (def f . u) . v) ... cc)
  (let () (def f (sy a . u)) ... cc))

#|
For read term we need to know how to translate in a goody daddy way,
especially symbol functors need to be handled, the translation of
functor names to functor objects. In the end we might need to add special
syntax for symbol + module knowledge, this means that e,g, code that is defined
in one module get's it's value in another one and name object mapping might go 
out of order and be buggy. To note is that we might use some kind of directive 
to tell the parser how to translate
|#

