(define-module (logic guile-log prolog analyze)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log type)
  #:use-module (logic guile-log prolog names)
  #:use-module (logic guile-log guile-prolog copy-term)
  #:export (analyze))

(define #{,}# prolog-and)

(<define> (analyze-type in)
   (<<match>> (#:mode - #:name analyze-type) (in)
      ((x . l)
       (<and>
	(<values> (xx) (analyze-type x))
	(<values> (ll) (analyze-type l))
	(<cc> (cons xx ll))))

      (#(x)
       (<and>
	(<values> (xx) (analyze-type x))
	(<cc> (vector xx))))

      (#(x y)
       (<and>
	(<values> (xx) (analyze-type x))
	(<values> (yy) (analyze-type y))
	(<cc> (vector xx yy))))

      (x
       (<let> ((x (<lookup> x))) (<cc> x) #;
	 (<var> (v)
            (<if> (<get-attr> x Type v)
                  (<and>
                   (<values> (vv) (analyze-type v))
                   (<cc> (list Type x vv)))
                  (<cc> x)))))))

(<define> (analyze in goal)
  (<values> (in)            (analyze-type in))
  (<values> (in.goal extra) (duplicate-term-3 (cons in goal)))
  (<let>  ((in    (car in.goal))
	   (goal  (cdr in.goal)))
    (<cc> in (let lp ((l extra))
	       (if (pair? l)
		   (vector (list #{,}# (car l) (lp (cdr l))))
		   goal)))))
