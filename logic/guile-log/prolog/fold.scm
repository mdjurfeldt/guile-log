(define-module (logic guile-log prolog fold)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log tools)
  #:use-module (logic guile-log prolog goal-transformers)
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log prolog order)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (ice-9 match)
  #:export (findall bagof setof	    
	    foldof   sumof   prodof   countof   maxof   minof   lastof
	    foldall  sumall  prodall  countall  maxall  minall  lastall
	    foldstp  sumstp  prodstp  countstp  maxstp  minstp  laststp
	    foldalln sumalln prodalln countalln maxalln minalln lastalln
	    foldstpn sumstpn prodstpn countstpn maxstpn minstpn laststpn
	    foldofp  sumofp  prodofp  countofp  maxofp  minofp  lastofp
	    foldofs  sumofs  prodofs  countofs  maxofs  minofs  lastofs
	    foldofn  sumofn  prodofn  countofn  maxofn  minofn  lastofn
	    foldofpn sumofpn prodofpn countofpn maxofpn minofpn lastofpn
	    foldofsn sumofsn prodofsn countofsn maxofsn minofsn lastofsn
	    amaxof   aminof   bmaxof   bminof
	    amaxall  aminall  bmaxall  bminall
	    amaxstp  aminstp  bmaxstp  bminstp
	    amaxalln aminalln bmaxalln bminalln
	    amaxstpn aminstpn bmaxstpn bminstpn
	    amaxofp  aminofp  bmaxofp  bminofp
	    amaxofs  aminofs  bmaxofs  bminofs
	    amaxofn  aminofn  bmaxofn  bminofn
	    amaxofpn aminofpn bmaxofpn bminofpn
	    amaxofsn aminofsn bmaxofsn bminofsn))
	    
;;FINDALL

(<define> (findall template g l)
  (<code> (gp-var-set *call-expression* g S))
  (<var> (r)
    (<fold> cons '() (<lambda> () (goal-eval g)) template r)
    (<if>
       (<=> ,l ,(reverse (<lookup> r)))
       <cc>
       (<let> ((ll (<lookup> l)))
         (<if> (<not> (list/plist? ll))
               (type_error list l)
               <fail>)))))

(define (mk-all kons knil <fold>)
  (let ()
    (<define> (findall template g l)
       (<code> (gp-var-set *call-expression* g S))
       (<var> (t)
         (<let> ((f (vector (list is t template))))
	   (<fold> kons knil (<lambda> () (goal-eval g) (goal-eval f))
                 t l))))
    findall))

(define-syntax-rule (def-all x-all kons knil aggr)      
  (define x-all 
    (let ((f (mk-all kons knil aggr)))
      (set-procedure-property! f 'name 'x-all)
      f)))



(define (mk-alln kons knil <fold>)
  (let ()
    (<define> (findall n template g l)
       (<code> (gp-var-set *call-expression* g S))
       (<recur> lp ((n n) (strict? #f))
	(<<match>> (#:mode -) (n)
	 (#(("strict" n)) (lp n #t))
	 (_
          (<var> (t r)
	   (<let> ((f (vector (list is t template)))
		   (n (<lookup> n)))
	    (<fold> (lambda (x s) 
			  (cons (+ (car s) 1) (kons x (cdr s))))
			(cons 0 knil) 
			(<lambda> ()
			 (goal-eval g)
			 (goal-eval f))
			t r 
			(<lambda> (s) (when (< (car s) n))))
	    (if strict?
		(<=> (,n . ,l) ,r)
		(<=> (_ . ,l) ,r))))))))
    findall))

(define (mk-alli kons knil <fold>)
  (let ()
    (<define> (aminall template g l)
       (<code> (gp-var-set *call-expression* g S))
          (<var> (t r)
	   (<let> ((f (vector (list is t template))))
	    (<fold> (lambda (x s) 
                      (kons x s))
                    (cons 0 knil) 
                    (<lambda> ()
                      (goal-eval g)
                      (goal-eval f))
                    t r)                    
            (<=> (_ . ,l) ,r))))
    aminall))

(define (mk-allin kons knil <fold>)
  (let ()
    (<define> (findall n template g l)
       (<code> (gp-var-set *call-expression* g S))
       (<recur> lp ((n n) (strict? #f))
	(<<match>> (#:mode -) (n)
	 (#(("strict" n)) (lp n #t))
	 (_
          (<var> (t r)
	   (<let> ((f (vector (list is t template)))
		   (n (<lookup> n)))
	    (<fold> (lambda (x s) 
		      (kons x s))
                    (cons 0 knil) 
                    (<lambda> ()
                      (goal-eval g)
                      (goal-eval f))
                    t r 
                    (<lambda> (s) (when (< (car s) n))))
	    (if strict?
		(<=> (,n . ,l) ,r)
		(<=> (_ . ,l) ,r))))))))
    findall))

(define-syntax-rule (def-alli x-all kons knil aggr)      
  (define x-all 
    (let ((f (mk-alli kons knil aggr)))
      (set-procedure-property! f 'name 'x-all)
      f)))

(define-syntax-rule (def-allin x-all kons knil aggr)      
  (define x-all 
    (let ((f (mk-allin kons knil aggr)))
      (set-procedure-property! f 'name 'x-all)
      f)))

(define-syntax-rule (def-alln x-all kons knil aggr)      
  (define x-all 
    (let ((f (mk-alln kons knil aggr)))
      (set-procedure-property! f 'name 'x-all)
      f)))


(define-syntax-rule (mk-mkof mk def-of args mk-args
			     (template-x g gg fixed)
			     code ...)
  (begin
    (define (mk . mk-args)
      (<define> (x-of . args) 
       (<recur> lp ((gg g) (q '()) (i 0))
         (<let> ((gg (<lookup> gg)))
           (cond
             ((<var?> gg)
	      (instantiation_error))
	     (else <cc>))
	   (<or>
	    (<var> (X A)
	      (<=> gg ,(vector (list "^" X A)))
	      (<cut> (lp A (cons (<lookup> X) q) (+ i 1))))
	    (<let*> ((t      (<get-idfixed> template-x '()))
		     (fixed  (<get-idfixed> gg (append q))))
	      (<code> (gp-var-set *call-expression* gg S))
	      code ...)))))
      x-of)

    (define-syntax-rule (def-of x-of . mk-args)      
      (define x-of 
	(let ((f (mk . mk-args)))
	  (set-procedure-property! f 'name 'x-of)
	  f)))))

(mk-mkof mk-li def-li-of 
	 (template g l)
	 (kons knil <fix-fold>)
	 (template g gg fixed)
  (<var> (r)
     (<fix-fold> kons knil (<lambda> () (goal-eval gg))
		 template fixed r)
     (<if> (<=> ,l ,(reverse (<lookup> r)))
	   <cc>
	   (<let> ((ll (<lookup> l)))
	      (<if> (<not> (list/plist? ll))
		    (type_error list l)
		    <fail>)))))

(def-li-of bagof cons '() <fix-fold>)

(mk-mkof mk-val def-val-of 
	 (template g l)
	 (kons knil <fix-fold>)
	 (template g gg fixed)
  (<var> (t)
    (<let> ((f (vector (list is t template))))
     (<fix-fold> kons knil 
	 (<lambda> () 
	     (goal-eval gg)
	     (goal-eval f))
	 t fixed l))))

(mk-mkof mk-valn def-valn-of 
	 (n template g l)
	 (kons knil <fix-fold>)
	 (template g gg fixed)
   (<var> (t r)
     (<recur> lp ((n n) (strict? #f))
       (<<match>> (#:mode -) (n)
	 (#(("strict" n)) (lp n #t))
	 (_
	  (<let> ((f (vector (list is t template)))
		  (n (<lookup> n)))
	    (<fix-fold> (lambda (x s) 
			  (cons (+ (car s) 1) (kons x (cdr s))))
			(cons 0 knil) 
			(<lambda> ()
			 (goal-eval gg)
			 (goal-eval f))
			t fixed r 
			(<lambda> (s) (when (< (car s) n))))
	    (if strict?
		(<=> (,n . ,l) ,r)
		(<=> (_ . ,l) ,r))))))))

(mk-mkof mk-vali def-vali-of 
	 (template g l)
	 (kons knil <fix-fold>)
	 (template g gg fixed)
   (<var> (t r)
     (<let> ((f (vector (list is t template))))
      (<fix-fold> (lambda (x s) 
                    (kons x s))
                  (cons 0 knil) 
                  (<lambda> ()
                    (goal-eval gg)
                    (goal-eval f))
                  t fixed r)
      (<=> (_ . ,l) ,r))))

(mk-mkof mk-valin def-valin-of 
	 (n template g l)
	 (kons knil <fix-fold>)
	 (template g gg fixed)
   (<var> (t r)
     (<recur> lp ((n n) (strict? #f))
       (<<match>> (#:mode -) (n)
	 (#(("strict" n)) (lp n #t))
	 (_
	  (<let> ((f (vector (list is t template)))
		  (n (<lookup> n)))
	    (<fix-fold> (lambda (x s) 
			  (kons x s))
			(cons 0 knil) 
			(<lambda> ()
			 (goal-eval gg)
			 (goal-eval f))
			t fixed r 
			(<lambda> (s) (when (< (car s) n))))
	    (if strict?
		(<=> (,n . ,l) ,r)
		(<=> (_ . ,l) ,r))))))))


(define-syntax mku
  (lambda (x)
    (define (extend x l)
      (datum->syntax
       x (symbol-append
	  (syntax->datum x)
	  l)))

    (syntax-case x ()
      ((_ def-all def-alln def-val-of def-valn-of name kons knil)
       (with-syntax ((countall   (extend #'name 'all))
		     (countstp   (extend #'name 'stp))
		     (countalln  (extend #'name 'alln))
		     (countstpn  (extend #'name 'stpn))
		     (countof    (extend #'name 'of))
		     (countofp   (extend #'name 'ofp))
		     (countofs   (extend #'name 'ofs))
		     (countofn   (extend #'name 'ofn))
		     (countofpn  (extend #'name 'ofpn))
		     (countofsn  (extend #'name 'ofsn)))
	 #'(begin
	     (def-all     countall  kons  knil <fold>)
	     (def-all     countstp  kons  knil <fold-step>)
	     (def-alln    countalln kons  knil <fold>)
	     (def-alln    countstpn kons  knil <fold-step>)
	     (def-val-of  countof   kons  knil <fix-fold>)
	     (def-val-of  countofp  kons  knil <fix-fold-pre>)
	     (def-val-of  countofs  kons  knil <fix-fold-sorted>)
	     (def-valn-of countofn  kons  knil <fix-fold>)
	     (def-valn-of countofpn kons  knil <fix-fold-pre>)
	     (def-valn-of countofsn kons  knil <fix-fold-sorted>)))))))

(define (sum1 x s) (+ s 1))
(mku def-all def-alln def-val-of def-valn-of countof sum1 0)
(mku def-all def-alln def-val-of def-valn-of sum     +    0)
(mku def-all def-alln def-val-of def-valn-of prodof  *    1)
(mku def-all def-alln def-val-of def-valn-of maxof   max  (- (inf)))
(mku def-all def-alln def-val-of def-valn-of minof   min  (inf))
(define (amax x s)
  (if (> x (car (cdr s)))
      (cons (+ (car s) 1) (cons x (+ (car s) 1)))
      (cons (+ (car s) 1) (cdr s))))
(define (amin x s)
  (if (< x (car (cdr s)))
      (cons (+ (car s) 1) (cons x (+ (car s) 1)))
      (cons (+ (car s) 1) (cdr s))))

(mku def-alli def-allin def-vali-of def-valin-of 
     amax amax (cons (- (inf)) 0))

(mku def-alli def-allin def-vali-of def-valin-of 
     amin amin (cons (inf)     0))

(define (bmax x s)
  (if (>= x (car (cdr s)))
      (cons (+ (car s) 1) (cons x (+ 1 (car s))))
      (cons (+ (car s) 1) (cdr s))))
(define (bmin x s)
  (if (<= x (car (cdr s)))
      (cons (+ (car s) 1) (cons x (+ 1 (car s))))
      (cons (+ (car s) 1) (cdr s))))

(mku def-alli def-allin def-vali-of def-valin-of 
     bmax  bmax (cons (- (inf)) 0))

(mku def-alli def-allin def-vali-of def-valin-of 
     bmin bmin (cons (inf) 0))

(define (laster x y) x)
(mku def-all def-alln def-val-of def-valn-of lastof  laster #f)


(<define> (gen update F X XX X0)
   (<recur> lp ((Y X0))
      (<var> (YY)
         (<call> ((YY XX)) (<and> (<=> X Y) (F update)))
         (<cut>
	  (<or> (<=> X YY) (lp YY))))))

(mk-mkof mfold def-fold-of 
	 ( x xx x0 g lam)
	 (<fix-fold>)
	 ((cons x xx) g gg fixed)
    (<fix-fold> 
     (lambda (x s) x)
     x0 
     (<lambda> () (gen gg lam x xx x0))
     x
     fixed
     x))

(define-syntax-rule (mk-folder folder-of-fkn folderfn-fkn <fix-fold>)
  (begin
    (def-fold-of foldof-fkn0 <fix-fold>)
    (<define> (foldof-fkn x xx x0 update)
      (foldof-fkn0 x xx x0 update
        (<lambda> (update) (goal-eval update))))

    (<define> (foldofn-fkn m x xx x0 update)
       (if (= (<lookup> m) 0)
	   (<=> x x0)
	   (<var> (n nn)
	     (foldof-fkn0 (cons n x) (cons nn xx) (cons 0 x0)
			  update
			  (<lambda> (update)
			    (<let> ((n (<lookup> n))
				    (m (<lookup> m)))
			      (when (< n m))
			      (<=> nn ,(+ n 1)) 
			      (goal-eval update)))))))))

(mk-folder foldof  foldofn  <fix-fold>)
(mk-folder foldofs foldofns <fix-fold-sorted>)
(mk-folder foldofp foldofnp <fix-fold-pre>)

(define (mk-fold-all lam <fold>)
  (let ()
    (<define> (findall x xx x0 g l)
       (<code> (gp-var-set *call-expression* g S))
       (<fold> 
	(lambda (x s) x)
	x0 
	(<lambda> () (gen g lam x xx x0))
	x
	x))
    findall))

#;
(define-syntax-rule (def-all x-all lam aggr)      
  (define x-all 
    (let ((f (mk-fold-all lam aggr)))
      (set-procedure-property! f 'name 'x-all)
      f)))

(define-syntax-rule (mk-folder-all folder-all-fkn folder-alln-fkn <fold>)
  (begin
    (def-fold-all folder-all-fkn0 <fold>)
    (<define> (fold-all-fkn x xx x0 update)
      (fold-all-fkn0 x xx x0 update
        (<lambda> (update) (goal-eval update))))

    (<define> (fold-alln-fkn m x xx x0 update)
       (if (= (<lookup> m) 0)
	   (<=> x x0)	   
	   (<var> (n nn)
	     (fold-all-fkn0 (cons n x) (cons nn xx) (cons 0 x0)
			  update
			  (<lambda> (update)
			    (<let> ((n (<lookup> n))
				    (m (<lookup> m)))
			      (when (< n m))
			      (<=> nn ,(+ n 1)) 
			      (goal-eval update)))))))))

(mk-folder foldall  foldalln  <fold>)
(mk-folder foldstp  foldstpn  <fold-step>)


(define (filter s x)
  (match x
    ((x y . l)
     (if ((</.> (<==> x y)) s (lambda () #f) (lambda x #t))
         (filter s (cons y l))
         (cons x (filter s (cons y l)))))
    (x x)))

(mk-mkof mset def-set-of 
	 (template g l)
	 (kons knil <fix-fold>)
	 (template g gg fixed)
    (<var> (r)
       (<fix-fold> cons '() (<lambda> () (goal-eval gg))
		   template fixed r)
             (<if> (<=> ,l ,(filter S
                              (stable-sort (<lookup> r) 
                                 (lambda (x y)
                                   (term< S 
                                      (lambda () #f)
                                      (lambda x  #t) x y)))))
                   <cc>
                   (<let> ((ll (<lookup> l)))
                     (<if> (<not> (list/plist? ll))
                           (type_error list l)
                           <fail>)))))

(def-set-of setof cons '() <fix-fold>)
