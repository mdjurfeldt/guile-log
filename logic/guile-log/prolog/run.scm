(define-module (logic guile-log prolog run)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log iinterleave)
  #:use-module (logic guile-log prolog error)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:use-module ((logic guile-log umatch) 
		#:select (gp-newframe gp-unwind gp-unwind-tail *current-stack*))
  #:use-module (logic guile-log prompts)
  #:export (prolog-run prolog-run-* proog-run-0 var->code
		       prolog-run-rewind))

(define (var->code x)
  (define (get-fkn a)
    (cond
     ((string? a)
      (format #f "'~a'" a))
     ((symbol? a)
      (format #f "~a" a))
     ((procedure? a)
      (format #f "~a" (procedure-name a)))))
     
        
  (match x
    (#((f))
     (format #f "~a()" (get-fkn f)))

    (#((f a b ...))
     (format #f "~a(~a~{, ~a~})"
             (get-fkn f)
             (var->code a)
             (map var->code b)))

    (a 
     (if (procedure? a)
         (format #f "~a" (procedure-name a))
         (format #f "~a" a)))))

(set! (@@ (logic guile-log prolog parser) var->code) var->code)
(define-syntax-rule (prolog-run n v code ...)
  (scheme-wrapper
   (lambda ()
     (<run> n v 
	(<catch> 'prolog #f             
	   (<lambda> () (init-machines) code ...)
	     (<lambda> (tag next l)
	       (<format> #t "DYNAMIC ERROR:~%=> ~a~%~%" (var->code (<scm> l)))
	       (<ret> (<scm> l))))))))

(define-syntax-rule (prolog-run-rewind n v code ...)
  (let* ((fr1 ((@ (logic guile-log umatch) gp-newframe)
	       (fluid-ref (@ (logic guile-log umatch) *current-stack*))))
	 (fr2 ((@ (logic guile-log umatch) gp-newframe)
	       fr1)))
    (with-fluids ((*current-stack* fr2))
      (let ((ret
	     (scheme-wrapper
	      (lambda ()
		(<run> n v 
		  (<catch> 'prolog #f             
		    (<lambda> () (init-machines) code ...)
		    (<lambda> (tag next l)
		      (<format> #t "DYNAMIC ERROR:~%=> ~a~%~%" 
				(var->code (<scm> l)))
		      (<ret> (<scm> l)))))))))
	((@ (logic guile-log umatch) gp-unwind-tail) fr1)
	ret))))

(define-syntax-rule (prolog-run-* code ...)
  (let* ((fr1 ((@ (logic guile-log umatch) gp-newframe)
	       (fluid-ref (@ (logic guile-log umatch) *current-stack*))))
	 (fr2 ((@ (logic guile-log umatch) gp-newframe)
	       fr1)))
    (with-fluids ((*current-stack* fr2))
      (scheme-wrapper
       (lambda ()
	 (<run> 1 ()
	   (<catch> 'prolog #f
	      (<lambda> () (init-machines) code ...)
	      (<lambda> (tag next l)
		 (<format> #t "DYNAMIC ERROR:~%=> ~a~%~%" (var->code (<scm> l)))
		 (<ret> (<scm> l))))))))
    ((@ (logic guile-log umatch) gp-unwind-tail) fr1)))


(define (prolog-run-0 f . l)
  (let* ((fr1 ((@ (logic guile-log umatch) gp-newframe)
	       (fluid-ref (@ (logic guile-log umatch) *current-stack*))))
	 (fr2 ((@ (logic guile-log umatch) gp-newframe)
	       fr1)))
    (with-fluids ((*current-stack* fr2))
      (scheme-wrapper
       (lambda ()
	 (<run> 1 () 
	   (<catch> 'prolog #f             
	     (<lambda> () 
	       (init-machines)
	       (<apply> f l) 
	       <cc>
	       (<code> ((@ (logic guile-log umatch) gp-unwind-tail) fr1))) 
	     (<lambda> (tag next l)
		(<format> #t "DYNAMIC ERROR:~%=> ~a~%~%" (var->code (<scm> l)))
		(<ret> (<scm> l))))))))))
		            
