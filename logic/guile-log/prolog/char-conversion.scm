(define-module (logic guile-log prolog char-conversion)
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log prolog names)
  #:use-module (logic guile-log prolog parser)
  #:use-module (logic guile-log prolog directives)
  #:use-module (logic guile-log dynamic-features)
  #:use-module (logic guile-log vlist)
  #:use-module (ice-9 match)
  #:use-module (logic guile-log umatch)
  #:use-module ((logic guile-log)
		#:renamer (lambda (x)
                            (if (eq? x '_)
                                '__
                                x)))
  
  
  #:export
  (reset-char-conversion char-convert char-conversions->assq
                        save-char-conversion-table
                        assq->char-conversions
                        get_prolog_conversion_handle
                        current_char_conversion init-char-conversion))


(define *conversion* (make-fluid vlist-null))
(add-fluid-dynamics (fluid-ref *current-stack*)
                    (lambda x #f)
                    (lambda x #t)
                    *conversion*)
(<define> (get_prolog_conversion_handle x) (<=> x *conversion*))


(define (reset-char-conversion)
  (fluid-set! *conversion* vlist-null))

(define (char-convert ch)
  (if (do-character-convert)
      (let ((l (vhash-assq ch (fluid-ref *conversion*))))
        (if l
            (cdr l)
            ch))
      ch))

(define (char-conversions->assq)
  (vhash->assoc (fluid-ref *conversion*)))

(define (assq->char-conversions a)
  (let lp ((a a) (vhash (fluid-ref *conversion*)))
    (if (pair? a)
        (lp (cdr a) (vhash-consq (caar a) (cdar a) vhash))
        (fluid-set! *conversion* vhash))))

(define-syntax save-char-conversion-table
  (lambda (x)
    #`(assq->char-conversions 
       '#,(datum->syntax #'define (char-conversions->assq)))))
  

(define (add-char-conversion ch1 ch2)
  (let* ((vhash (fluid-ref *conversion*)))
    (if (eq? ch1 ch2)
        (let ((l (vhash-assq ch1 vhash)))
          (if l
              (if (cdr l)
                  (fluid-set! *conversion*
                              (vhash-consq ch1 #f vhash)))))
        (fluid-set!  *conversion*
                     (vhash-consq ch1 ch2 vhash)))))

(define (->ch x)
  (define (ch1? x) (= (string-length x) 1))
        
  (cond
   ((and (string? x) (ch1? x))
    (string-ref x 0))
   ((procedure? x)
    (->ch (procedure-name x)))
   ((symbol? x)
    (->ch (symbol->string x)))
   ((char? x) 
    x)
   (else
    #f)))

(define (<-ch x)
  (list->string (list x)))

(<define> (char_conversion ch1 ch2)
  (<let> ((ch1 (<lookup> ch1))
          (ch2 (<lookup> ch2)))
    (if (or (<var?> ch1) (<var?> ch2))
        (instantiation_error)
        (<let> ((ch1- (->ch ch1))
                (ch2- (->ch ch2)))
          (cond
           ((not ch1-)
            (representation_error character))
           ((not ch2-)
            (representation_error character))
           (else
            (<code> (add-char-conversion ch1- ch2-))))))))     

(set! (@ (logic guile-log prolog names) char_conversion)
      char_conversion)

(init-flags)

(<define> (current_char_conversion ch1 ch2)
  (<let*> ((ch1  (<lookup> ch1))
           (ch1- (->ch ch1))
           (ch2- (->ch (<lookup> ch2))))
    (cond
     ((<var?> ch1)
      (<let> ((ch2 (if (<var?> ch2) ch2 ch2-)))
        (cond
         ((not ch2-)
          (representation_error character))
         (else
          (<recur> lp ((x (char-conversions->assq)))
            (<match> (#:mode * #:name current_char_conversion) (x)
             (((ch1* . ch2*) . l)
              (if (<var?> ch2)
                  (<=> (,(<-ch ch1*) ,(<-ch ch2*)) (ch1 ch2))
                  (<=> (,(<-ch ch1*)  ch2*)        (ch1 ch2-))))
             ((_ . l)
              (<cut> 
               (lp l)))
             (() (<cut> <fail>))))))))

     ((not ch1-)
      (representation_error character))

     (else
      (<let> ((ch2* (char-convert ch1-)))
        (cond
         ((<var?> ch2)
          (<=> ch2 ,(<-ch ch2*)))
         ((not ch2-)
          (representation_error character))
         (else
          (<=> ch2- ch2*))))))))

(define (str x)
  (cond
   ((symbol? x)
    (symbol->string x))
   (else
    x)))

(define-parser-directive-onfkn char_conversion (char_conv_directive stx l N M)
(let lp ((l l))
  (match l
    ((l) (lp l))
    (((_ _ "," _) 
      (or (#:string a _ _) (#:atom a . _))
      (or (#:atom b . _) (#:string b _ _)) _ _)
     #`(char_conversion (fluid-ref *current-stack*)
                        (lambda x #f) (lambda x #t) #,(str a) #,(str b)))
    (_
     (format #t "COMPILE ERROR: Bad character format in char_conversion at ~a~%" (get-refstr N M))))))

(set! (@ (logic guile-log prolog names) char-convert)
      char-convert)
