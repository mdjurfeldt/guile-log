(define-module (logic guile-log prolog load)
  #:use-module (logic guile-log prolog modules)
  #:use-module (logic guile-log umatch)
  #:use-module (system base language)
  #:export (load-prolog ensure_loaded ensure_loaded_))

(define* (load-in-vicinity-q dir file-name #:optional reader)
  "Load source file FILE-NAME in vicinity of directory DIR.  Use a
pre-compiled version of FILE-NAME when available, and auto-compile one
when none is available, reading FILE-NAME with READER."

  ;; The auto-compilation code will residualize a .go file in the cache
  ;; dir: by default, $HOME/.cache/guile/2.0/ccache/PATH.go.  This
  ;; function determines the PATH to use as a key into the compilation
  ;; cache.
  (define (canonical->suffix canon)
    (cond
     ((and (not (string-null? canon))
           (file-name-separator? (string-ref canon 0)))
      canon)
     ((and (eq? (system-file-name-convention) 'windows)
           (absolute-file-name? canon))
      ;; An absolute file name that doesn't start with a separator
      ;; starts with a drive component.  Transform the drive component
      ;; to a file name element:  c:\foo -> \c\foo.
      (string-append file-name-separator-string
                     (substring canon 0 1)
                     (substring canon 2)))
     (else canon)))

  (define compiled-extension
    ;; File name extension of compiled files.
    (cond ((or (null? %load-compiled-extensions)
               (string-null? (car %load-compiled-extensions)))
           (warn "invalid %load-compiled-extensions"
                 %load-compiled-extensions)
           ".go")
          (else (car %load-compiled-extensions))))

  (define (more-recent? stat1 stat2)
    ;; Return #t when STAT1 has an mtime greater than that of STAT2.
    (or (> (stat:mtime stat1) (stat:mtime stat2))
        (and (= (stat:mtime stat1) (stat:mtime stat2))
             (>= (stat:mtimensec stat1)
                 (stat:mtimensec stat2)))))

  (define (fallback-file-name canon-file-name)
    ;; Return the in-cache compiled file name for source file
    ;; CANON-FILE-NAME.

    ;; FIXME: would probably be better just to append
    ;; SHA1(canon-file-name) to the %compile-fallback-path, to avoid
    ;; deep directory stats.
    (and %compile-fallback-path
         (string-append %compile-fallback-path
                        (canonical->suffix canon-file-name)
                        compiled-extension)))

  (define (compile file)
    ;; Compile source FILE, lazily loading the compiler.
    (with-fluids ((*current-language* (lookup-language 'scheme)))
        ((module-ref (resolve-interface '(system base compile))
		     'compile-file)
	 file
	 #:opts %auto-compilation-options
	 #:env (current-module))))

  ;; Returns the .go file corresponding to `name'.  Does not search load
  ;; paths, only the fallback path.  If the .go file is missing or out
  ;; of date, and auto-compilation is enabled, will try
  ;; auto-compilation, just as primitive-load-path does internally.
  ;; primitive-load is unaffected.  Returns #f if auto-compilation
  ;; failed or was disabled.
  ;;
  ;; NB: Unless we need to compile the file, this function should not
  ;; cause (system base compile) to be loaded up.  For that reason
  ;; compiled-file-name partially duplicates functionality from (system
  ;; base compile).

  (define (fresh-compiled-file-name name scmstat go-file-name)
    ;; Return GO-FILE-NAME after making sure that it contains a freshly
    ;; compiled version of source file NAME with stat SCMSTAT; return #f
    ;; on failure.
    (false-if-exception
     (let ((gostat (and (not %fresh-auto-compile)
                        (stat go-file-name #f))))
       (if (and gostat (more-recent? gostat scmstat))
           go-file-name
           (begin
             (if gostat
                 (format (current-warning-port)
                         ";;; note: source file ~a\n;;;       newer than compiled ~a\n"
                         name go-file-name))
             (cond
              (%load-should-auto-compile
               (%warn-auto-compilation-enabled)
               (format (current-warning-port) ";;; compiling ~a\n" name)
               (let ((cfn (compile name)))
                 (format (current-warning-port) ";;; compiled ~a\n" cfn)
                 cfn))
              (else #f)))))
     #:warning "WARNING: compilation of ~a failed:\n" name))

  (define (sans-extension file)
    (let ((dot (string-rindex file #\.)))
      (if dot
          (substring file 0 dot)
          file)))

  (define (load-absolute abs-file-name)
    ;; Load from ABS-FILE-NAME, using a compiled file or auto-compiling
    ;; if needed.
    (define scmstat
      (false-if-exception
       (stat abs-file-name)
       #:warning "Stat of ~a failed:\n" abs-file-name))

    (define (pre-compiled)
      (and=> (search-path %load-compiled-path (sans-extension file-name)
                          %load-compiled-extensions #t)
             (lambda (go-file-name)
               (let ((gostat (stat go-file-name #f)))
                 (and gostat (more-recent? gostat scmstat)
                      go-file-name)))))

    (define (fallback)
      (and=> (false-if-exception (canonicalize-path abs-file-name))
             (lambda (canon)
               (and=> (fallback-file-name canon)
                      (lambda (go-file-name)
                        (fresh-compiled-file-name abs-file-name
                                                  scmstat
                                                  go-file-name))))))

    (let ((compiled (and scmstat (or (pre-compiled) (fallback)))))
      (if compiled
          (begin
            (if %load-hook
                (%load-hook abs-file-name))
            (load-compiled compiled))
          (start-stack 'load-stack
                       (primitive-load abs-file-name)))))

  (save-module-excursion
   (lambda ()
     (with-fluids ((current-reader reader)
                   (%file-port-name-canonicalization 'relative))
       (cond
        ((absolute-file-name? file-name)
         (load-absolute file-name))
        ((absolute-file-name? dir)
         (load-absolute (in-vicinity dir file-name)))
        (else
         (load-from-path (in-vicinity dir file-name))))))))

(define-syntax load-q
  (make-variable-transformer
   (lambda (x)
     (let* ((src  (syntax-source x))
            (file (and src (assq-ref src 'filename)))
            (dir  (and (string? file) (dirname file))))
       (syntax-case x ()
         ((_ s arg ...)
          #`(let* ((fr1 (gp-newframe s))
		   (fr2 (gp-newframe fr1)))
	      (with-fluids ((*current-stack* fr2))
		(load-in-vicinity-q #,(or dir #'(getcwd)) arg ...))
	      (gp-unwind-tail fr1)))
         (id
          (identifier? #'id)
          #`(lambda args
              (apply load-in-vicinity-q #,(or dir #'(getcwd)) args))))))))

(define (process-use_module module-interface-args)
  (with-fluids ((*current-language* (lookup-language 'scheme)))
    (let ((interfaces (map (lambda (mif-args)
			     (or (apply resolve-interface mif-args)
				 (error "no such module" mif-args)))
			   module-interface-args)))
      (module-use-interfaces! (current-module) interfaces))))

(define-syntax-rule (load-prolog s str)
  (begin
    #;(with-fluids ((*current-language* (lookup-language 'scheme)))
    (load (load-prolog_ str)))
    (load-q s (load-prolog_ s str))))

(define (proj x)
  (cond
   ((procedure? x) 
    (proj (procedure-name x)))
   ((symbol? x)    
    (symbol->string x))
   (else x)))

(define (load-prolog_ s str)
  (let* ((str     (proj str))
	 (str     (if (> (length (string-split str #\.)) 1)
		      str
		      (string-append str ".pl")))
         (pl      str)
         (scm     (string-append str ".scm")))

    (define (action)
      (let ((r (pk 'mod? (is-module-file? pl))))
	(if r
	    (write-module-scratch r pl)
	    (with-output-to-file scm
	      (lambda ()
		(format #t "(compile-prolog-file ~s)~%" pl))))))

    (catch #t
	   (lambda ()
	     (let* ((mpl     (stat:mtime (stat pl)))
		    (mscm    (stat:mtime (stat scm))))
	       (when (< (+  mscm 10) mpl)
		     (action))))
	   (lambda x (action)))

					;(pk `(compiling and/or load of ,str))
    scm))

(define ensure_loaded #f)
