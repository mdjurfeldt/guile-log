(define-module (logic guile-log prolog char)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log prolog goal-transformers)
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log prolog names)
  #:use-module (logic guile-log prolog directives)
  #:use-module ((logic guile-log prolog util)
                #:select ((append . pr-append)))
  #:use-module (logic guile-log umatch)
  #:use-module (ice-9 match)
  #:export (atom_length atom_concat atom_chars atom_codes char_code 
                        number_chars number_codes sub_atom
			code_type char_type))
(define (procedure-name- x)
  (let ((f (object-property x 'prolog-functor-type)))
    (if (eq? f #:goal)
        (let ((f (procedure-property x 'prolog-operator)))
          (if f
              f
              (procedure-name x)))
        (procedure-name x))))

(<define> (atom_length atm n)
  (<let> ((atm (<lookup> atm))
          (n   (<lookup> n)))
    (cond
     ((<var?> atm)
      (instantiation_error))
     ((not (or (string? atm) (procedure? atm)))
      (type_error atom atm))
     ((not (or (<var?> n) (and (number? n) (integer? n))))
      (type_error integer n))
     ((and (not (<var?> n)) (< n 0))
      (domain_error not_less_than_zero n))
     (else
      (if (string? atm)
          (<=> n ,(string-length atm))
          (<=> n ,(string-length (symbol->string (procedure-name atm)))))))))

(define-syntax-rule (->chars x)
  (cond 
   ((<var?> x) (gp-var! S))
   ((null? x)  (list "[" "]"))
   ((procedure? x) (chr->str
                    (string->list (symbol->string (procedure-name x)))))
   ((string? x)    (chr->str (string->list x)))
   ((char?   x)    (list->string (list x)))))

(define-syntax-rule (->chaars x)
  (cond 
   ((<var?> x) (gp-var! S))
   ((null? x)  (list #\[ #\]))
   ((procedure? x) (string->list (symbol->string (procedure-name x))))
   ((string? x)    (string->list x))
   ((char?   x)    x)))

(define (str->chr x)
  (if (pair? x)
      (cons (string-ref (car x) 0) (str->chr (cdr x)))
      x))

(define (chr->str x)
  (if (pair? x)
      (cons (list->string (list (car x))) (chr->str (cdr x)))
      x))

(define-syntax-rule (chars-> x)
  (list->string (str->chr (<scm> x))))
  
(<define> (atom_concat a b c)
 (<let> ((a (<lookup> a))
         (b (<lookup> b))
         (c (<lookup> c)))
   (cond
    ((and (<var?> a) (<var?> c))
     (instantiation_error))
    ((and (<var?> b) (<var?> c))
     (instantiation_error))
    (else
     (<cond>
      ((<and> (<not> (-atom a)) (<not> (-var a)))
       (type_error atom a))
      ((<and> (<not> (-atom b)) (<not> (-var b)))
       (type_error atom b))
      ((<and> (<not> (-atom c)) (<not> (-var c)))
       (type_error atom c))
      (else
       (<let> ((a1 (->chars a))
               (b1 (->chars b))
               (c1 (->chars c)))
        (pr-append a1 b1 c1)
        (<let> ((a2 (chars-> a1))
                (b2 (chars-> b1))
                (c2 (chars-> c1)))
         (<=> (a b c) (a2 b2 c2))))))))))

(<define> (check-achars l L)
  (<let> ((l (<lookup> l)))
    (if (<var?> l)
        (instantiation_error)
        (<match> (#:mode - #:name check-achars) (l)
          ((x . l)
           (<cut>
            (<let> ((x (<lookup> x)))
             (cond
              ((<var?> x)
               (instantiation_error))
              ((not (or (char? x) 
                        (and (procedure? x) 
			     (or (eq? x unary-minus)
				 (eq? x binary-minus)
				 (eq? x binary-plus)))
                        (and (string? x) 
                             (= (string-length x) 1))
                        (and (procedure? x)
                             (= (string-length (symbol->string 
                                                (procedure-name x)))
                                1))))
               (type_error character x))
              (else
               (check-achars l L))))))
          (()
           (<cut> <cc>))
          (_
           (type_error list L))))))

(<define> (check-cchars l L)
  (<let> ((l (<lookup> l)))
    (if (<var?> l)
        (instantiation_error)
        (<match> (#:mode - #:name check-achars) (l)
          ((x . l)
           (<cut>
            (<let> ((x (<lookup> x)))
             (cond
              ((<var?> x)
               (instantiation_error))
              ((not (and (number? x) (integer? x)))
               (type_error integer x))
              (else
               (check-cchars l L))))))
          (()
           (<cut> <cc>))
          (_
           (type_error list L))))))

(define (plist->chars l)
  (cond
   ((pair? l)
    (let ((x (car l)))
      (cond
       ((string? x)
        (cons (string-ref x 0) (plist->chars (cdr l))))
       ((number? x)
        (cons (integer->char x) (plist->chars (cdr l))))
       ((procedure? x)        
        (cons (cond
	       ((eq? x unary-minus)
		#\-)
	       ((eq? x binary-minus)
		#\-)
	       ((eq? x binary-plus)
		#\+)
	       (else
		(string-ref (symbol->string (procedure-name x)) 0)))
              (plist->chars (cdr l))))
       (else
        (cons x (plist->chars (cdr l)))))))
   (else
    l)))


(<define> (atom_chars atm l)
  (<let> ((atm (<lookup> atm))
          (l   (<lookup> l)))
    (if (<var?> atm)
        (<and>
         (check-achars l l)
         (<=> atm ,(list->string (plist->chars (<scm> l)))))
        (if (or (procedure? atm) (string? atm) (char? atm) (null? atm))
            (<=> ,(->chars atm) ,(<scm> l))
            (type_error atom atm)))))


(<define> (atom_codes atm l)
  (<let> ((atm (<lookup> atm))
          (l   (<lookup> l)))
    (if (<var?> atm)
        (<and>
         (check-cchars l l)
         (<=> atm ,(list->string (map integer->char (<scm> l)))))
        (if (or (procedure? atm) (string? atm) (char? atm) (null? atm))
            (<=> ,(map char->integer (->chaars atm)) ,(<scm> l))
            (type_error atom atm)))))

(<define> (char_code ch code)
   (<let> ((ch   (<lookup> ch))
           (code (<lookup> code)))
     (cond
      ((and (<var?> ch) (<var?> code))
       (instantiation_error))
      ((not (or (<var?> ch) (and (string? ch) (= (string-length ch) 1))
                (char? ch) (and (procedure? ch) (= (string-length 
                                                    (symbol->string
                                                     (procedure-name ch)))
                                                   1))))
       (type_error character ch))
      ((not (or (<var?> code) (and (number? code) (integer? code))))
       (<match> (#:mode - #:name char_code) (code)
         (#((,unary-minus x))
          (representation_error character_code))
         (_
          (type_error integer code))))
      ((<var?> code)
       (<=> code ,(char->integer (list-ref (->chaars ch) 0))))
      ((< code 0)
       (representation_error character_code))
      ((<var?> ch)
       (<=> ch ,(list->string (list (integer->char code)))))
      (else
       (<=> code ,(char->integer (list-ref (->chaars ch) 0)))))))
                              
(define (have-var? l s)
  (if (pair? l)
      (if (gp-var? (car l) s)
          #t
          (have-var? (cdr l) s))
      (gp-var? l s)))
          
(define nums (string->list "0123456789+-"))
(define (get-num l)
  (match l
    ((#\0 #\x . l)
     (string->number (list->string l) 16))
    ((#\0 #\' x)
     (char->integer x))
    ((x . y)
     (if (member x  nums)
         (string->number (list->string l))
         (get-num y)))))

(<define> (number_chars atm l)
  (<let> ((atm (<lookup> atm))
          (l   (<lookup> l)))
    (if (<var?> atm)
        (<and>
         (check-achars l l)
         (<let> ((num (get-num (plist->chars (<scm> l)))))
           (if (number? num)
               (<=> atm num)
               (syntax_error atm))))
        (if (number? atm)
            (if (not (have-var? (<scm> l) S))
                (<let> ((num (get-num (plist->chars (<scm> l)))))
                  (if (number? num)
                      (when (= atm num))
                      (syntax_error atm)))
                (<=> ,(->chars (number->string atm))
                     ,(<scm> l)))
            (type_error number atm)))))

(define (atom? x)
  (or (procedure? x) (string? x) (char? x) (null? x)))

(<define> (number_codes atm l)
  (<let> ((atm (<lookup> atm))
          (l   (<lookup> l)))
    (if (<var?> atm)
        (<and>
         (check-cchars l l)
         (<let> ((num (get-num (plist->chars (<scm> l)))))
           (if (number? num)
               (<=> atm num)
               (representation_error character_code))))
        (if (number? atm)
            (if (not (have-var? (<scm> l) S))
                (<let> ((num (get-num (plist->chars (<scm> l)))))
                  (if (number? num)
                      (when (= atm num))
                      (representation_error character_code)))
                (<=> ,(map char->integer (->chaars (number->string atm)))
                     ,(<scm> l)))
            (type_error number atm)))))
       
  
(<define> (iter x n)
  (<let> ((x (<lookup> x)))
    (cond
     ((<var?> x)
      (<recur> lp ((i 0))
        (<or>
         (<=> x i)
         (when (< i n)
           (lp (+ i 1))))))
     ((<= x n)
      <cc>))))

(<define> (select* l bef len lout)
  (<recur> lp1 ((i 0) (l2 l))
    (if (< i bef)
        (<match> (#:name select1) (l2)
          ((x . ll)
           (<cut> (lp1 (+ i 1) ll))))
        (<recur> lp2 ((j 0) (l3 l2) (lout lout))
          (if (< j len)
              (<match> (#:name select2) (l3 lout)
                ((x . ll) (y . llout)
                 (<cut> 
                  (<and>
                   (<=> x y)
                   (lp2 (+ j 1) ll llout))))
                (_ _ (<cut> <fail>)))
              (<=> lout ()))))))

(<define> (sub_atom atm bef len aft sub)
  (<let> ((atm (<lookup> atm))
          (bef (<lookup> bef))
          (len (<lookup> len))
          (aft (<lookup> aft))
          (sub (<lookup> sub)))
    (cond
     ((<var?> atm)
      (instantiation_error))
     ((not (atom? atm))
      (type_error atom atm))
     ((not (or (atom? sub) (<var?> sub)))
      (type_error atom sub))
     ((not (or (and (number? bef) (integer? bef)) (<var?> bef)))
      (type_error integer bef))
     ((not (or (and (number? len) (integer? len)) (<var?> len)))
      (type_error integer len))
     ((not (or (and (number? aft) (integer? aft)) (<var?> aft)))
      (type_error integer aft))
     ((and (not (<var?> bef)) (< bef 0))
      (domain_error not_less_than_zero bef))
     ((and (not (<var?> len)) (< len 0))
      (domain_error not_less_than_zero len))
     ((and (not (<var?> aft)) (< aft 0))
      (domain_error not_less_than_zero aft))
     ((<var?> sub)      
      (<var> (subsub l)
         (atom_chars atm l)
         (<let> ((n (length (<scm> l))))
           (iter bef n)
           (iter len (- n (<scm> bef)))
           (<=> aft ,(- n (<scm> bef) (<scm> len)))
           (select* l (<scm> bef) (<scm> len) subsub)
           (<=> sub ,(list->string (plist->chars (<scm> subsub)))))))
     (else
       (<var> (subsub latm lsub)
         (atom_chars atm latm)
         (atom_chars sub lsub)
         (<let> ((natm (length (<scm> latm)))
                 (nsub (length (<scm> lsub))))
           (iter bef (- natm nsub))
           (<=> len nsub)
           (<=> aft ,(- natm nsub (<scm> bef)))
           (select* latm (<scm> bef) (<scm> nsub) lsub)))))))

(set! (@@ (logic guile-log prolog var) get-double-quote-flag-fkn)
      (lambda ()
      (lambda (str)
        (let ((flag-val (get-flag double_quotes)))
          (cond
           ((eq? flag-val atom)
            str)
           ((eq? flag-val codes)
            (map char->integer (string->list str)))
           ((eq? flag-val chars)
            (map (lambda (x) (list->string (list x)))
                 (string->list str))))))))
                 
(<define> (code_type code type)
  (<var> (ch)
    (char_code ch code)
    (char_type ch type)))
       
(<define> (char_type char0 type)
  (<let> ((char (->chaars (<lookup> char0))))
    (if (and (list? char) (= (length char) 1))
	(char_type_char type (car char))
	(type_error "character" char0))))

(define (char-punctuation? ch)
  (char-set-contains? char-set:punctuation ch))

(define (char-graphic? ch)
  (char-set-contains? char-set:graphic ch))

(define (char-blank? ch)
  (char-set-contains? char-set:blank ch))

(<define> (char_type_char type x)
  (<<match>> (#:mode -) (type)
    ("alnum"
     (when (or (char-alphabetic? x) (char-numeric? x))))
    ("alpha"
     (when (or (char-alphabetic? x))))
    ("csym"
     (when (or (char-alphabetic? x) (char-numeric? x) (eq? x #\_))))
    ("csymf"
     (when (or (char-alphabetic? x) (eq? x #\_))))
    ("ascii"
     (when (< (char->integer x) 128)))
    ("white"
     (when (char-whitespace? x)))
    ("cntrl"
     (when (< (char->integer x) 32)))
    ("digit"
     (<let> ((x (- (char->integer x) (char->integer #\0))))
       (when (and (>= x 0) (<= x 9)))))
    (#("digit" weight)
     (when (member x '(0 1 2 3 4 5 6 7 8 9))
	(<=> weight ,(- (char->integer x) (char->integer #\0)))))
    (#("xdigit" weight)
     (when (member x '(0 1 2 3 4 5 6 7 8 9 a b c d e f))
	(<=> weight ,(- (char->integer x) (char->integer #\0)))))
    ("graph"
     (when (char-graphic? x)))
    ("lower"
     (when (char-lower-case? x)))
    (#(("lower" up))
     (when (char-lower-case? x)
       (<=> up (list->string (list (char-upcase x))))))
    ("upper"
     (when (char-upper-case? x)))
    (#(("upper" low))
     (when (char-upper-case? x)
	   (<=> low (list->string (list (char-downcase x))))))
    (#(("to_upper" lower))
     (<=> lower ,(char-upcase x)))
    
    ("punct"
     (when (char-punctuation? x)))
    ("space"
     (when (char-blank? x)))
    ("end_of_file" 
     (when (eq? (char->integer x) 0)))
    ("end_of_line"
     (when (member (char->integer x)'(10 11 12 13))))
    ("newline"
     (when (eq? x #\newline)))
    ("period"
     (when (char-punctuation? x)))
    ("quote"
     (when (member x '(#\' #\" #\`))))
    (#(("paren" close))
     (cond
      ((eq? x #\()
       (<=> close #\)))
      ((eq? x #\[)
       (<=> close #\]))
      ((eq? x #\{)
       (<=> close #\}))))
    ("prolog_var_start"
     (when (or (char-upper-case? x) (eq? x #\_))))
    ("prolog_atom_start"
     (when (char-lower-case? x)))
    ("prolog_prolog_symbol"
     (when (not (or (char-alphabetic? x) (char-numeric? x) (eq? x #\_)))))
    (else
     (type_error "char_type" type))))





