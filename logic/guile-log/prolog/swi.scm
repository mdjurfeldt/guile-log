(define-module (logic guile-log prolog swi)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log vlist)
  #:use-module (ice-9 match)
  #:use-module (logic guile-log prolog error)
  #:use-module ((logic guile-log prolog names) #:select (callable))
  #:use-module (logic guile-log prolog util)
  #:use-module (logic guile-log prolog order)
  #:use-module (logic guile-log prolog io)
  #:use-module (logic guile-log prolog char)
  #:use-module (logic guile-log prolog base)
  #:use-module (logic guile-log prolog modules)
  #:use-module (logic guile-log prolog names)
  #:use-module (logic guile-log prolog dynamic)
  #:use-module (logic guile-log prolog goal-transformers)
  #:use-module (logic guile-log prolog goal-functors)
  #:replace (sort plus delete string reverse)
  #:export (memberchk keysort $skip_list is_list
		      term_variables 
		      setup_call_cleanup
		      compare ground assertion		
		      strip_module
		      succ between
		      must_be collation_key
		      atomic_list_concat
		      atomic_concat assert
		      string_codes $type_error
		      $set_source_module
		      print_message
		      msort
		      acyclic_term
		      cyclic_term
		      set_random
		      callable
		      pairs_values
		      pairs_keys
		      pairs_keys_values
		      same_length
		      same_term
		      group_pairs_by_key
		      transpose))

(define term_variables
  (<case-lambda>
   ((term l tail)
    (<let> ((seen (make-hash-table)))
      (<recur> lp ((term term) (l l) (tail tail))
	(<let> ((term (<lookup> term)))
 	  (if (attvar? term) 
	      (if (not (hashq-ref seen term #f))
		  (<and>
		   (<code> (hashq-set! seen term #t))
		   (<=> l ,(cons term tail)))
		  (<=> l tail))	
	      (<<match>> (#:mode -) (term)
	        ((x . y)
		 (<var> (ll)
		   (lp x ll tail)
		   (lp y l  ll)))
		(#(u)
		 (lp u l tail))
		(_
		 (<=> l tail))))))))
   ((term l)
    (term_variables term l '()))))

(<define> (setup_call_cleanup Setup Goal Cleanup)
  (once-f Setup)
  (<let> ((first #t))
    (<dynwind>
     (lambda () (error "setup_call_cleanup is not reentrable"))
     (lambda (x)
       (if first
	   (begin
	     (set! first #f)
	     (<wrap-s> goal-eval* S CUT Cleanup)))))
    (<var> (l)
     (findall-fkn Goal Goal l)
     (<code> (set! first #f)
	     (<wrap-s> goal-eval* S CUT Cleanup))
     (member Goal l))))
  

(<define> (memberchk x l)
  (<<match>> (#:mode -) (l)
    ((a . l)
     (<cut> 
      (<if> (<not> (<not> (<=> a x)))
	    <cc>
	    (memberchk x l))))
    (()
     (<cut> <fail>))
    (else
     (type_error list l))))

(<define> (keysort x l)
  (<let> ((a (<scm> x)))
     (if (list? a)
	 (<let> ((res (stable-sort a (lambda (x y)
				       (match x
					 (#((f keyx valx))
					  (match y
					   (#((f keyy valy))
					    (term< S 
						   (lambda () #f) 
						   (lambda x #t)
						   keyx keyy)))))))))
		(<=> l res))
	 (type_error list x))))

(<define> (msort x l)
  (<let> ((a (<scm> x)))
     (if (list? a)
	 (<let> ((res (stable-sort a (lambda (x y)
				       (term< S 
					      (lambda () #f) 
					      (lambda x #t)
					      x y)))))
	    (<=> l res))
	 (type_error list x))))

(<define> ($skip_list len x rest)
   (<recur> lp ((x x) (n 0))	  
     (<<match>> (#:mode -) (x)
       ((a . l)
	(lp l (+ n 1)))
       (x
	(<=> (x n) (rest len))))))

(<define> (is_list x)
   (<recur> lp ((x x))	  
     (<<match>> (#:mode -) (x)
       ((a . l)
	(lp l))
       (()
	<cc>)
       (x
	<fail>))))

	 
(<define> (compare op x y)
 (<let> ((op (<lookup> op)))
   (if (attvar? op)
       (<and>
	(<or> (<=> op "op2<") (<=> op "op2>") (<=> op "op2="))
	(compare op x y))
       (<<match>> (#:mode -) (op)
	 ("op2<"
	  (term< x y))
	 ("op2>"
	  (term< y x))
	 ("op2="
	  (<==> x y))
	 (_
	  (type_error "operators <,>,=" op))))))


(<define> (ground x)
  (<let> ((x (<lookup> x)))
    (if (attvar? x)
	<fail>
	(<<match>> (#:mode -) (x)
	  ((x . y)
	   (<and>
	     (ground x)
	     (ground y)))
	  (#((x . y))
	    (<and>
	     (ground x)
	     (ground y)))
	  (_ 
	   <cc>)))))

(<define> (assertion . x) <cc>)

(define (get-module-name)
  (define (last x) 
    (let ((ret (car (scm-reverse x))))
      (if (symbol? x)
	  (symbol->string x)
	  x)))

  (let ((modnm (module-name (current-module))))
    (let ((x (string-split (last modnm) #\.)))
      (module-ref (current-module)
		  (string->symbol
		   (if (equal? (car x) "")
		       (cadr x)
		       (car x)))))))

(<define> (strip_module term module name)
    (<match> (#:mode -) (term) 
      (#((#((,op2: mod n)) . l))
       (<cut>
	(<let> ((mod (<lookup> mod))
		(n   (<lookup> n)))
	  (<=> name  ,(module-ref (ref-module (procedure-name mod))
				   (procedure-name n)))
	  (<=> module mod))))
      (#((n . l))
       (<cut>
	(<=> name     n)
	(<=> module  ,(get-module-name))))
      (else
       (type_error "term" term))))
      
(<define> (plus X Y Z)
  (<let> ((X (<lookup> X))
	  (Y (<lookup> Y))
	  (Z (<lookup> Z)))
  (cond
   ((attvar? Z)
    (if (or (attvar? X) (attvar? Y))
	(instantiation_error)
	(<=> Z (+ X Y))))
   ((attvar? X)
    (if (or (attvar? Z) (attvar? Y))
	(instantiation_error)
	(<=> X (- Z Y))))
   ((attvar? Y)
    (if (or (attvar? Z) (attvar? X))
	(instantiation_error)
	(<=> Y ,(- Z X))))
   (else
    (<==> Z ,(+ X Y))))))

(<define> (succ X Y)
  (<let> ((X (<lookup> X))
	  (Y (<lookup> Y)))
  (cond
   ((attvar? X)
    (if (attvar? Y)
	(instantiation_error)
	(<=> X ,(- Y 1))))

   (else
    (<=> Y ,(+ X 1))))))

(<define> (between low high value)
  (<let> ((low   (<lookup> low))
	  (high  (<lookup> high))
	  (value (<lookup> value)))
   (if (<= low high)
       (if (attvar? value)
	   (<recur> lp ((i low))
	      (when (<= i high)
	        (<or>
		 (<=> value i)
		 (lp (+ i 1)))))
	   (if (number? value)
	       (when (and (<= low value) (>= high value)))
	       (type_error "number" value)))
       (type_error "x <= y" (list low high)))))

;; must_be is defined in error.scm
(define must_be (lambda x (error "use_module(library(error)) must be loaded for must_be to be available")))

(<define> (collation_key a x) (<=> a x))

#|
(<define> (delete Lin Xin Lout)
  (<match> (#:mode -) (Lin)
    ((X . Lin)
     (<cut> 
      (<if> (<not> (<not> (<=> Xin X)))
	    (delete Lin Xin Lout)
	    (<match> (#:mode +) (Lout)
	       ((,X . Lout)
		(<cut>
		 (delete Lin Xin Lout)))
	       (else
		(<cut> <fail>))))))
    (else
     (<cut> <fail>))))
|#

(<define> (atomic_concat x y z) <cc> #;
  (<if> (<and> (atom x) (atom y))
	(atom_concat x y z)
	(<if> (<and> (atomic x) (atomic y))
	      (<=> y ,(with-output-to-string 
			(lambda () (write S (lambda () #f) (lambda x #t) x))))
	      (type_error "atomic" (list x y)))))

#|
(<define> (atomic_list_concat l a)
  (<match> (#:mode -) (l)
    ((x)
     (<cut>
      (<=> a x)))
    ((x . l)
     (<cut>
      (<var> (y)
       (atomic_list_concat l y)
       (atomic_concat x y a))))
    (else
     (type_error "nonempty list" l))))
|#



(<define> (assert . x) (<apply> assertz x))
(<define> (string_codes . x) (<apply> atom_codes x))
(<define> ($type_error . x ) (<apply> type_error x))
(<define> ($set_source_module . x) <cc>)
(<define> (print_message . x) (write x))
(<define> (string x) (when (string? (<lookup> x))))

(<define> (acyclic_term x)
  (<recur> lp ((found vlist-null) (x x))
    (<<match>> (#:mode - #:name acyclic_term) (x)
      ((a . b)
       (if (vhashq-ref found x #f)
	   <fail>
	   (<let> ((new-found (vhash-consq x #t found)))
	     (lp new-found a)
	     (lp new-found b))))
      (#(l)
       (lp found l))
     
      (#(a ...)
       (if (vhashq-ref found x #f)
	   <fail>
	   (<let> ((new-found (vhash-consq x #t found)))
	     (lp new-found a))))

      (x <cc>))))

(<define> (cyclic_term x) (<not> (acyclic_term x)))

(<define> (set_random x)
 (<<match>> (#:mode - #:name set_random) (x)
   (#(("seed" x))
    (<let> ((x (<lookup> x)))
      (cond
       ((attvar? x)
	(instantiation_error))
       ((and (number? x) (integer? x))
	(<code> (set! *random-state*
		      (seed->random-state x))))
       (else
	(type_error "integer" x)))))
   (else
    (type_error "guile-log set_random does not implement state argumnet" x))))

(set! callable 
      (<lambda> (x)
	(<<match>> (#:mode - #:name 'callable) (x)
	  (#((f . a))
	   (<let> ((ff (<lookup> f)))
		  (when (or (procedure? ff) (string? ff)))))
	  (f
	   (<let> ((ff (<lookup> f)))
		  (when (or (procedure? ff) (string? ff))))))))
     

(<define> (pairs_values pairs values)
  (<recur> lp ((pairs pairs) (values values))
     (<<match>> (#:mode + #:name pairs-values) (pairs values)
        ((#((_ _ val)) . pairs) (+ + (,val . values))
	(lp pairs values))
       (()                          (+ + ())
	<cc>))))

(<define> (pairs_keys pairs values)
  (<recur> lp ((pairs pairs) (values values))
     (<<match>> (#:mode + #:name pairs-values) (pairs values)
       ((#((_ key _)) . pairs) (+ + (,key . values))
	(lp pairs values))
       (()                          (+ + ())
	<cc>))))

(<define> (pairs_keys_values pairs keys values)
  (<recur> lp ((pairs pairs) (keys keys) (values values))
     (<<match>> (#:mode + #:name 'pairs-key-values) (pairs keys values)
       ((#((,gop2- key val)) . pairs) (+ + (,key . keys)) (+ + (,val . values))
	(lp pairs keys values))
       (()                       (+ + ())            (+ + ())
	<cc>))))

(<define> (same_length l1 l2)
  (<recur> lp ((l1 l1) (l2 l2))
    (<<match>> (#:mode + #:name same_length) (l1 l2)
      ((? attvar?) (? attvar?) <cc>)
      ((_ . l1) (_ . l2) (lp l1 l2))
      ((_ . l1) v        (<and> (<=> v (_ . l2)) (lp l1 l2)))
      (v        (_ . l2) (<and> (<=> v (_ . l1)) (lp l1 l2)))
      (()       ()       <cc>)
      (()       x        (<=> x ()))
      (x        ()       (<=> x ())))))

(<define> (same_term x y) (if (eq? (<lookup> x) (<lookup> y)) <cc>))

(<define> (transpose x y)
  (<let*> ((xx (<scm> x))
	   (n  ((@ (guile) length) xx))
	   (m  ((@ (guile) length) (car xx))))
    (<recur> lp1 ((mm 0) (y y))
      (if (< mm m)
	  (<match> (#:mode + #:name transpose) (y)
             ((a . y)
	      (<recur> lp2 ((nn 0) (a a))
		 (if (< nn n)
		     (<match> (#:mode + #:name transpose2) (a)
                        ((,(list-ref (list-ref xx nn) mm) . a)
			 (lp2 (+ nn 1) a)))
		     (<and>
		      (<=> a ())
		      (lp1 (+ mm 1) y))))))
	  (<=> y ())))))

     
(<define> (group_pairs_by_key pairs groups)
  (<<match>> (#:mode -) (pairs)
     ((#(("op2-" k v)) . u)
      (<recur> lp ((k k) (p u) (dg (list v)) (g '()))
	(<<match>> (#:mode - #:name group_pairs_by_key) (p)
	   ((#(("op2-" ,k  v)) . u)
	    (lp k u (cons v dg) g))
	   ((#(("op2-"  kk v)) . u)
	    (lp kk u (list v) (cons (vector (list op2- k (scm-reverse dg))) g)))
	   (()
	    (<=> groups
		 ,(scm-reverse (cons  (vector (list op2- k (scm-reverse dg))) g)))))))
     (()
      (<=> groups ())))
)
(define scm-reverse (@@ (guile) reverse))
(<define> (reverse x y)
  (<let> ((x (<scm> x))
	  (y (<scm> y)))
    (<=> ,(scm-reverse x) y)))
