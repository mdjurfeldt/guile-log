(define-module (logic guile-log prolog goal-transformers)
  #:use-module (guile-user)
  #:use-module (logic guile-log prolog operators)
  #:use-module (logic guile-log prolog pre)
  #:use-module (logic guile-log prolog parser)
  #:use-module (logic guile-log tools)
  #:use-module (logic guile-log iinterleave)
  #:use-module (logic guile-log soft-cut)
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log prolog directives)
  #:use-module (logic guile-log prolog goal)
  #:use-module (logic guile-log prolog modules)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (logic guile-log prolog varstat)
  #:use-module ((logic guile-log prolog var)
		#:renamer (lambda (x)
			    (cond
			     ((eq? x 'arg)
			      '*arg*)
			     ((eq? x 'var)
			      '*var*)
			     (else x))))

  #:use-module (logic guile-log functional-database)
  #:use-module (logic guile-log prolog order)
  #:use-module (logic guile-log ck)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log prolog variant)
  #:use-module ((logic guile-log) 
		#:renamer  (lambda (x)
			     (cond
			      ((eq? x '<_>)
			       'GL:_)
			      ((eq? x 'umatch)
			       'GL:umatch)
			      (else x))))

  #:use-module (logic guile-log prolog names)
  #:use-module (logic guile-log prolog namespace)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:replace (catch throw)
  #:export (     unify_with_occurs_check copy_term functor arg
		 var atomic compound nonvar
		 directive
		 procedure_name
		 once once_i *once* once-f 
		 -var -atom 
		 halt
                  
		 uniq yield_at_change

		  ^ :- #{,}# #{,,}# -> #{\\+}# op2= == =@= | -i> *->
		  #{\\=}# #{\\==}# #{\\=@=}# @< @> @>= @=< is op2<=
		  op2+ op2- op1- op1+  #{\\}# op2* op2/ // rem mod div
		  ** << >> #{/\\}# #{\\/}# op2< op2> op2>= op2=< =:= #{=\\=}#
		  =.. --> ? $ ?- ← → =>

		  gop2+ gop2- gop1+ gop1-
		  #{g\\}# gop2* gop2/ g// gop2rem gop2mod
		  g** g^
		  g<< g>> #{g/\\}# #{g\\/}# gop2< gop2> gop2>= gop2=< g=:= 
		  #{g=\\=}#

		  #{;}#	#{;;}#
		    mk-scheme-biop
		    mk-scheme-unop
		    mk-prolog-biop-tr
		    mk-prolog-biop
		    s a
                  )
	 #:re-export(sin cos atan exp log sqrt))
#|
We could make all variable references through a stack frame e.g.
(m A),(m B)

(lambda (s p cc a b)
  ((ref a 1) s p 
   (lambda (ss pp)
     ((ref a 2) ss pp cc (ref a 3)))
   (ref a 4)))

(define (m code)
  (lambda (b)
    (lambda (s p c cc a)
      (exec a b c code))))

(define (, x y)
  (lambda (b)
    (let ((xx (x b))
	  (yy (y b)))
      (lambda (s p c cc a)
	(xx s p (lambda (ss pp) (yy ss pp cc a)) a)))))

(define (or x y)
  (lambda (b)
    (let ((xx (x b))
	  (yy (y b)))
      (lambda (s p c cc a)
	(let ((fr (gp-newframe-choice s)))
	  (xx s (lambda ()
		  (gp-unwind-tail fr)
		  (yy s p c cc a)) c cc a))))))

(define (if p x y)
  (lambda (b)
    (let ((pp (p b))
	  (xx (x b))
	  (yy (y b)))
      (lambda (s p c cc a)
	(let ((fr (gp-newframe-choice s)))
	  (pp s (lambda ()
		  (gp-unwind-tail fr)
		  (yy s p c cc a)) 
	      c 
	      (lambda (ss pp)
		(gp-prune-tail fr)
		(xx ss p c cc a))))))))
		
|#

(define (transfer-o-p from to)
  (set-object-properties! to (object-properties from)))

(define do-print #f)
(define pp
  (case-lambda
   ((s x)
    (when do-print
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when do-print
      (pretty-print (syntax->datum x)))
    x)))

(define-syntax-rule (define-syntax-rule-pk (f . a) c)
  (define-syntax f
    (lambda (x)
      (syntax-case x ()
	((_ . a) #'c)))))

(define-syntax-rule (define-quote-rule quote (f . x) code)
  (define-syntax f
    (lambda (y)
      ;(pk (syntax->datum y))
      (syntax-case y (quote)
        ((_ . x) #'code)))))
  

(<define-guile-log-rule> (<1> nm x) 
  (<and> (nm x)))

(<define-guile-log-rule> (<2> nm x y) 
  (<and> (nm x y)))

(<define-guile-log-rule> (<is2> nm x y) 
  (<and> (nm x ,y)))

(<define-guile-log-rule> (<not-2> nm x y) 
  (<not> (nm x y)))

(<define-guile-log-rule> (<when-2> nm x y)
  (<and> (when (nm (<lookup> x) (<lookup> y)))))

(<define-guile-log-rule> (<when-not-2> nm x y)
  (<and> (when (not (nm (<lookup> x) (<lookup> y))))))

(define-syntax -id-
  (syntax-rules (quote)
    ((_ s 'x)  (ck s 'x))))

(define-syntax -ia-
  (syntax-rules (quote)
    ((_ s 'x)  (ck s 'x))))

(define-syntax -goal-eval-
  (syntax-rules (quote)
    ((_ s 'x)  (ck s '(goal-eval x)))))

(define-syntax -scm-eval-
  (syntax-rules (quote)
    ((_ s 'x)  (ck s '(scm-eval x)))))

(define-syntax -fff-eval-
  (syntax-rules (quote)
    ((_ s 'x)  (ck s '(fff-eval x)))))

(define-syntax-rule (-2- nm x y) (nm x y))
(define-syntax-rule (-1- nm x)   (nm x))

(define-syntax delay-ck
  (syntax-rules (quote)
    ((_ s 'f 'w 'x ...)
     (ck s '(f w x ...)))))


(define-syntax-rule (mk-kind nm x1 x2)
  (define-guile-log nm
    (lambda (x)
      ;(pk (syntax->datum x))
      (syntax-case x (quote)
	((_ (a (... ...)) 'x)
	 #'(x2 (a (... ...)) 'x))
	((_ stx x)
	 #'(x1 stx x))
	((_ x)
	 #'(x2 x))))))

(define (-arg- stx x)
  #``#,(*arg* stx x))

(mk-kind g  goal   -goal-eval-)
(mk-kind v  *var*  -id-)
(mk-kind a  -arg-  -ia-)
(mk-kind s  scm    -scm-eval-)
(mk-kind ff fff    -fff-eval-)

(define-guile-log goal-ck
  (syntax-rules (quote)
    ((_ w 'f . l) (ck () (delay-ck 'f 'w . l)))))

(define-guile-log scm-ck
  (syntax-rules (quote)
    ((_ 'f . l) (ck () (delay-ck 'f . l)))))

(define-syntax-rule (GT x) `(@@ (logic guile-log prolog goal-transformers) x))

(define-syntax-rule (meta-mk-prolog-op mk-prolog stx nm-code 
				       (a ...) (tp ...) code)
  (define-syntax-rule (mk-prolog op-type op nm-tr nm-func nm-code tp ...)
    (begin      
      (define-goal-functor (nm-func a ...)
	(goal-ck 'code 'nm-code (tp 'a) ...))
      (set-object-property! nm-func 'goal-compile-stub 
                            (lambda (a ...)
                              `(,(GT code) ,(GT nm-code) 
				,a ...)))
      (set-object-property! nm-func 'goal-compile-types
                            '(tp ...))
      (define-goal-transformer nm-func (nm-tr stx n m a ...)
	#`(code nm-code #,(tp stx a) ...))
      (bind-operator-to-functor nm-func op op-type))))

(define-syntax-rule (meta-mk-prolog-op-tr mk-prolog stx nm-code 
				       (a ...) (tp ...) code)
  (define-syntax-rule (mk-prolog op-type op nm-tr nm-func nm-code (tp ...) tr)
    (begin
      (define-goal-functor (nm-func a ...)
	(goal-ck 'code 'nm-code (tp 'a) ...))
      (set-object-property! nm-func 'goal-compile-stub 
                            (lambda (a ...)
                              `(,(GT code) ,(GT nm-code) 
				,a ...)))
      (set-object-property! nm-func 'goal-compile-types
                            '(tp ...))
      (define-goal-transformer nm-func . tr)
      (bind-operator-to-functor nm-func op op-type))))

(meta-mk-prolog-op mk-prolog-biop stx nm-code
		   (x y) (tp1 tp2) <2>)

(meta-mk-prolog-op-tr mk-prolog-biop-tr stx nm-code
		   (x y) (tp1 tp2) <2>)

(meta-mk-prolog-op mk-prolog-unop stx nm-code
		   (x)   (tp1)     <1>)
(meta-mk-prolog-op mk-prolog-biop-not stx nm-code
		   (x y) (tp1 tp2) <not-2>)
(meta-mk-prolog-op mk-prolog-biop-when stx nm-code
		   (x y) (tp1 tp2) <when-2>)
(meta-mk-prolog-op mk-prolog-biop-when-not stx nm-code
		   (x y) (tp1 tp2) <when-not-2>)
; ---------------------
(define-syntax-rule (meta-mk-prolog-term
		     mk-prolog-term stx 
		     (a ...) (tp ...))
  (define-syntax-rule (mk-prolog-term nm-tr nm-func nm-code tp ...)
    (begin
      (define-goal-functor (nm-func a ...)
	(goal-ck 'nm-code (tp 'a) ...))
      (set-object-property! nm-func 'goal-compile-stub 
                            (lambda (a ...)
                              `(,(GT nm-code) ,a ...)))
      (set-object-property! nm-func 'goal-compile-types
                            '(tp ...))
      (define-goal-transformer nm-func (nm-tr stx n m a ...)
	#`(nm-code #,(tp stx a) ...)))))
        
(meta-mk-prolog-term mk-prolog-term-0 stx ()      ())
(meta-mk-prolog-term mk-prolog-term-1 stx (x)     (tp))
(meta-mk-prolog-term mk-prolog-term-2 stx (x y)   (tp-x tp-y))
(meta-mk-prolog-term mk-prolog-term-3 stx (x y z) (tp-x tp-y tp-z))
(meta-mk-prolog-term mk-prolog-term-4 stx (x y z w) 
		     (tp-x tp-y tp-z tp-w))
(meta-mk-prolog-term mk-prolog-term-5 stx (x y z w u) 
		     (tp-x tp-y tp-z tp-w tp-u))

; ------------------------
(define-syntax-rule (meta-mk-scheme-op mk-scheme stx nm-code 
				       (a ...) (tp ...) code)

  (define-syntax-rule (mk-scheme op-type op nm-tr nm-func nm-code tp ...)
    (begin
      (define-scm-functor (nm-func s a ...)
	(syntax-parameterize ((S (identifier-syntax s)))
	  (scm-ck 'code 'nm-code (tp 'a) ...)))
      (define-goal-transformer nm-func (nm-tr stx n m a ...)
	#`(code nm-code #,(tp stx a) ...))
      (bind-operator-to-functor nm-func op op-type))))

(meta-mk-scheme-op mk-scheme-biop stx nm-code
		   (x y) (tp1 tp2) -2-)
(meta-mk-scheme-op mk-scheme-unop stx nm-code
		   (x) (tp1) -1-)

(define-syntax-rule (meta-mk-scheme-op-dual mk-scheme stx nm-code 
				       (a1 a2) (tp1 tp2) code1 code2)

  (define-syntax-rule (mk-scheme op-type op nm-tr nm-func nm-code tp1 tp2)
    (begin
      (define-scm-functor (nm-func s . a)
	(apply (case-lambda 
		 ((a1 a2)		  
		  (syntax-parameterize ((S (identifier-syntax s)))
		     (scm-ck 'code2 'nm-code 
			     (tp1 'a1) (tp2 'a2))))
		 ((a1)
		  (syntax-parameterize ((S (identifier-syntax s)))
		     (scm-ck 'code1 'nm-code 
			     (tp1 'a1)))))
	       a))

      (define-goal-transformer nm-func (nm-tr stx n m . a)
	(apply (case-lambda
		 ((a1 a2)
		  #`(code2 nm-code #,(tp1 stx a1) #,(tp2 stx a2)))
		 ((a1)
		  #`(code1 nm-code #,(tp1 stx a1))))
	       a))

      (bind-operator-to-functor nm-func op op-type))))

(meta-mk-scheme-op-dual mk-scheme-dual stx nm-code
			(x y) (tp1 tp2) -1- -2-)

; ------------------------------
(define (tr-error op) 
  (lambda (stx n m . l)
    (warn (format #f "int ~a op ~a should not be translated directly"
		  (get-refstr n m) op))
    #'((fk-error 'op))))

(define (fk-error op)
  (let ((warn-message 
         (format #f "Operator ~a is not evaluable, will fail" op)))
                              
   (<lambda> (Cut SCut . X)
    (<let> ((e (get-flag unknown)))
      (cond
        ((eq? e error)
         (existence_error procedure op))

        ((eq? e warning)
         (<code> (warn warn-message))
         <fail>)

        ((eq? e fail)
         <fail>)

        (else
         (<code> (error "Bug in prolog flag 'unknown' implementation"))))))))

(<define> (<iss> x y)
  (<let> ((x (<lookup> x))
	  (y (<lookup> y)))
    (if (number? x)
        (if (number? y)
            (if (inexact? x)
                (if (inexact? y)
                    (when (my-equal? x y))
                    <fail>)
                (if (inexact? y)
                    <fail>
                    (when (eqv? x y))))
            (<r=> x ,(scm-eval y)))
	(if (<var?> x)
	    (<set> x (scm-eval y))
	    (<r=> x ,(scm-eval y))))))

(define-syntax-rule (mk-prolog-abstract tp op fk-name tr-name)
 (begin   
   (define-goal-functor      fk-name         (fk-error op))
   (define-goal-transformer  fk-name tr-name (tr-error op))
   (set-procedure-property!  fk-name 'name 'fk-name)
   (bind-operator-to-functor fk-name op)))

(mk-prolog-abstract 'xfx ":-"  :-     tr-fact)
(mk-prolog-abstract 'xfx "-->" -->    tr-dcg)
(mk-prolog-abstract 'fx "?"    ?      tr-?)
(mk-prolog-abstract 'fx "?-"   ?-     tr-?-)
(mk-prolog-abstract 'fx "$"    $      tr-$)

(<define> (<:> module l)
  (<let> ((module (ref-module (procedure-name (<lookup> module))))
	  (l      (<lookup> l)))
    (if (procedure? l) 
	(namespace-switch module (module-ref module 
					     (procedure-name l)))
	(namespace-switch module l))))

(define-syntax-rule (mk-meta --ifa -ifa)
  (<define-guile-log-rule> (--ifa x (... ...)) 
    (-ifa x (... ...))))

(define -if
  (<case-lambda>
   ((p a)   (p) <cut> (a))
   ((p a b) (<if> (p) (a) (b)))))

(mk-meta --if <if>)
			
(define --soft-if
  (<case-lambda>
   ((p a)   (p) (a))
   ((p a b) (<soft-if> (p) (a) (b)))))
(mk-meta --soft-if <soft-if>)


(define-guile-log -if-ii
  (lambda (x)
    (syntax-case x ()
      ((_ w p x)   #'(<and> w (<once-ii> p) x))
      ((_ w p x y) #'(<if> w  (<once-ii> p) x y)))))

(<define-guile-log-rule> (<if-ii> a b c)
     (<if> (<once-ii> a) b c))

(mk-meta --if-ii -if-ii)

(<define> (-and x y) (<and> (x) (y)))
(mk-meta --and <and>)

(<define> (-and-ii x y) (<and-ii> (x) (y)))
(mk-meta --and-ii <and-ii>)

(<define> (-or x y) (<or> (x) (y)))
(mk-meta --or <or>)

(<define> (-or-ii x y) (<or-ii> (x) (y)))
(mk-meta --or-ii <or-ii>)

(<define> (-not x) (<not> (x)))
(mk-meta --not <not>) 

(mk-prolog-biop     'xfy ":"    tr-ns:          op2:     <:>       a a)
(mk-prolog-biop     'xfy ","    tr-and          #{,}#    --and     g g)
(mk-prolog-biop     'xfy ",,"   tr-and-ii       #{,,}#   --and-ii  g g)
(mk-prolog-biop     'xfy "->"   tr-if-then      ->       --if      g g)
(mk-prolog-biop     'xfy "*->"  tr-if-then-soft *->      --soft-if g g)
(mk-prolog-biop     'xfy "-i>"  tr-if-then-ii   -i>      --if-ii   g g)
(mk-prolog-unop     'fy  "\\+"  tr-negation     #{\\+}#  --not     g  )

(set! (@ (logic guile-log prolog goal-functors) op2:) op2:)

(<define> (-<r=>  x y) (<r=> x y))		    
(<define> (-<=>   x y) (<=> x y))		    
(<define> (-<==>  x y) (<==> x y))

(mk-prolog-biop-tr  'xfx "="    unify-tr     op2=    <r=>  (v v)
  ((unify-tr stx n m x y)
   (match x
    ((#:variable v id nn mm)
     (if (and #f (first-variable? v id) v (variable-not-included? v y))
	 (begin
	   (local-variable! v)
	   #`(<code> 
	      (set! #,(datum->syntax stx v) `#,(*var* stx y))))
	 #`(-<r=> `#,(*var* stx x) `#,(*var* stx y))))
    (_
     (match y
       ((#:variable v id nn mm)
	(if (and #f (first-variable? v id) v 
		 (variable-not-included? v x))
	    (begin
	      (local-variable! v)
	      #`(<code> (set! #,(datum->syntax stx v) `#,(*var* stx y))))
	    #`(-<r=> `#,(*var* stx x) `#,(*var* stx y))))
       (_
	#`(-<r=> `#,(*var* stx x) `#,(*var* stx y))))))))

(mk-prolog-biop     'xfx "=="   ident-tr     ==       -<==>   a a)
(mk-prolog-biop-not 'xfx "\\==" not-ident-tr #{\\==}# -<==>   a a)
(mk-prolog-biop-not 'xfx "\\="  not-unify-tr #{\\=}#  -<r=>   a a)

(mk-prolog-biop     'xfx "=@="    variant-tr      =@=       is-variant?  a a)
(mk-prolog-biop-not 'xfx "\\=@="  not-variant-tr  #{\\=@=}# is-variant?  a a)


(mk-prolog-biop     'xfx "@>"   tr-ogt       @>           term> a a)
(mk-prolog-biop     'xfx "@<"   tr-olt       @<           term< a a)
(mk-prolog-biop-not 'xfx "@>="  tr-0ge       @>=          term< a a)
(mk-prolog-biop-not 'xfx "@=<"  tr-0le       @=<          term> a a)

(mk-prolog-biop-tr  'xfx "is"   tr-is        is           <iss> (a s)
 ((tr-is stx n m x y)
  (match x
   ((#:variable v id nn mm)
    (if (and #f (first-variable? v id))
	(begin
	  (local-variable! v)
	  #`(<code> (set! #,(datum->syntax stx v) #,(scm stx y))))
	#`(<iss> #,(-arg- stx x) #,(scm stx y))))
   (_
    #`(<iss> #,(-arg- stx x) #,(scm stx y))))))
(define (my-rem x y) (- x (* (quotient x y) y)))
(define consx
  (case-lambda 
    ((x y) (cons x y))
    ((x)   x)))

(define (mk-lam cc)
  (define (lam s p c* . x)
    (apply cc s p x))
  lam)
     
(<define> (<<=> data goal)
  (<<match>> (#:mode -) (data)
    (#((f . l))
      (<and>          
       (<values> x (let ((cc CC))
                     (<and>
                      (<=> f ,(mk-lam cc))
                      (goal-eval goal))))
       (<=> x l)))
    (_
     (<and>
      (<values> x (goal-eval goal))
      (<=> x data)))))

(<define> (<=>> data goal)
  (<<match>> (#:mode -) (data)
    (#((f . l))
      (<and>          
       (<values> x (let ((cc CC))
                     (<and>
                      (<=> f ,(mk-lam cc))
                      (goal-eval goal))))
       (<=> x l)))
    (_
     (<and>
      (<values> x (goal-eval goal))
      (<=> x data)))))

(mk-prolog-biop 'xfx "<="   <=-tr     op2<=       <<=> a a)
(mk-prolog-biop 'xfx "←"   ←-tr     ←           <<=> a a)
(mk-prolog-biop 'xfx "=>"   =>-tr     op2=>        <=>> a a)
(mk-prolog-biop 'xfx "←"   →-tr     →           <=>> a a)

(define-syntax-rule (shr x y) (ash x (- y)))
(mk-scheme-dual     'yfx "|"    tr-|         |             consx     s s)
(mk-scheme-dual     'yfx "+"    tr-+         op2+          .+        s s)
(mk-scheme-dual     'yfx "-"    tr--         op2-          .-        s s)
(mk-scheme-unop     'fy  "\\"   tr-bitnot    #{\\}#        lognot    s  )
(mk-scheme-biop     'yfx "*"    tr-*         op2*          .*        s s)
(mk-scheme-biop     'yfx "/"    tr-/         op2/          ./        s s)
(mk-scheme-biop     'yfx "//"   tr-i/        //            truncate/ s s)
(mk-scheme-biop     'yfx "rem"  tr-rem       rem           my-rem    s s)
(mk-scheme-biop     'yfx "mod"  tr-mod       mod           modulo    s s)
(mk-scheme-biop     'yfx "div"  tr-div       div           quotient  s s)
(mk-scheme-biop     'xfx "**"   tr-pow       **            myexpt    s s)
(mk-scheme-biop     'xfx "^"    tr-pow2      ^             myexpt    s s)
(mk-scheme-biop     'yfx "<<"   tr-shr       <<            .ash      s s)
(mk-scheme-biop     'yfx ">>"   tr-shr       >>            shr       s s)
(mk-scheme-biop     'yfx "/\\"  tr-bitand    #{/\\}#       logand    s s)
(mk-scheme-biop     'yfx "\\/"  tr-bitor     #{\\/}#       .logior   s s)

(define (name-as f g) (set-procedure-property! f 'name (procedure-name g)))


(mk-prolog-biop-when 'xfx "<"    tr-<         op2<           .<        s s)
(mk-prolog-biop-when 'xfx ">"    tr->         op2>           .>        s s)
(mk-prolog-biop-when 'xfx ">="   tr->=        op2>=          my->=     s s)
(mk-prolog-biop-when 'xfx "=<"   tr-=<        op2=<          my-<=     s s)
(mk-prolog-biop-when 'xfx "=:="  tr-equal     =:=            my-equal? s s)
(mk-prolog-biop-when-not 'xfx "=\\="  tr-not-equal  #{=\\=}# my-equal? s s)

(mk-scheme-dual     'yfx "+"    tr-+g         gop2+         +        s s)
(mk-scheme-dual     'yfx "-"    tr--g         gop2-         -        s s)
(mk-scheme-unop     'fy  "\\"   tr-bitnotg    #{g\\}#       lognot    s  )
(mk-scheme-biop     'yfx "*"    tr-*g         gop2*         *        s s)
(mk-scheme-biop     'yfx "/"    tr-/g         gop2/         /        s s)
(mk-scheme-biop     'yfx "//"   tr-i/g        g//           truncate/ s s)
(mk-scheme-biop     'xfx "**"   tr-powg       g**           expt     s s)
(mk-scheme-biop     'xfx "^"    tr-pow2g      g^            expt     s s)
(mk-scheme-biop     'yfx "<<"   tr-shrg       g<<           ash      s s)
(mk-scheme-biop     'yfx ">>"   tr-shrg       g>>           shr       s s)
(mk-scheme-biop     'yfx "/\\"  tr-bitandg    #{g/\\}#      logand    s s)
(mk-scheme-biop     'yfx "\\/"  tr-bitorg     #{g\\/}#      logior    s s)


(define-syntax-rule (mkbi f ff) 
  (define-syntax-rule (f x y) 
    (ff (<lookup> x) (<lookup> y))))

(mkbi bi-equal? equal?)
(mkbi bi<       <)
(mkbi bi>       >)
(mkbi bi<=      <=)
(mkbi bi>=      >=)

(mk-prolog-biop-when 'xfx "<"    tr-<g         gop2<           bi<       s s)
(mk-prolog-biop-when 'xfx ">"    tr->g         gop2>           bi>       s s)
(mk-prolog-biop-when 'xfx ">="   tr->=g        gop2>=          bi>=      s s)
(mk-prolog-biop-when 'xfx "=<"   tr-=<g        gop2=<          bi<=      s s)
(mk-prolog-biop-when 'xfx "=:="  tr-equalg     g=:=            bi-equal? s s)
(mk-prolog-biop-when-not 'xfx "=\\="  tr-not-equalg  #{g=\\=}# bi-equal? s s)

(define (.logior x y)
  (check-num (logior (is-a-num? x) (is-a-num? y))))

(define (myexpt x y) (exact->inexact (expt x y)))

(define (.< x y)
  (check-num (< (is-a-num? x) (is-a-num? y))))
(define (.> x y)
  (check-num (> (is-a-num? x) (is-a-num? y))))

(define .+ 
  (case-lambda 
   ((x y)
    (check-num (+ (is-a-num? x) (is-a-num? y))))
   ((x)
    (check-num (+ (is-a-num? x))))))

(define .- 
  (case-lambda 
   ((x y)
    (check-num (- (is-a-num? x) (is-a-num? y))))
   ((x)
    (check-num (- (is-a-num? x))))))

(define .-1 .-)

(define .+1 .+)

(define (.* x y)
  (check-num (* (is-a-num? x) (is-a-num? y))))
(define (./ x y)
  (exact->inexact (/ (is-a-num? x) (is-a-num? y))))
(define (.ash x n)
  (check-num (ash (is-a-num? x) (is-a-num? n))))


(define e1 1.000000000001)
(define e2 0.999999999999)
(define (my-equal? x y)
  (if (and (is-a-num? x) (is-a-num? y))
      (begin
	(if (< x 0) (set! x (- x)))
	(if (< y 0) (set! y (- y)))

	(if (inexact? x)
	    (and (<= y (* e1 x)) (>= y (* e2 x)))
	    (if (inexact? y)
		(and (<= x (* e1 y)) (>= x (* e2 y)))
		(= x y))))
      (= x y)))

(define (my-<= x y)
  (if (inexact? x)
      (> y (* e2 x))
      (if (inexact? y)
          (< x (* e1 y))
          (<= x y))))

(define (my->= x y)
  (if (inexact? x)
      (< y (* e1 x))
      (if (inexact? y)
          (> x (* e2 y))
          (>= x y))))

#| Further supported operators scm functions are
abs(x)                     (abs x)
sign(x)                    (if (= x 0) 0 (if (< x 0) -1 1))
float_integer_part(x)      (truncate/ x 1)
float_fractional_part(x)   (call-with-values (lambda () (truncate/ x 1)
                                                (lambda (x y) y)))
float(x)                   (exact->inexact x)
truncate(x)                (truncate x)
round(x)                   (round x)
ceiling(x)                 (ceiling x)
floor(x)                   (floor x)
|#

(define (project@ s x)
  (if (namespace? x)
      (project@ s (namespace-val (gp-lookup x s)))
      x))

(define (al s x)
  (umatch (#:mode - #:status s) (x)
    ((x . l)
     (cons x (al s l)))
    (()
     '())
    (x x)))

(<define> (sup=.. x y)
  (<let*> ((xx (<lookup> x))
	   (y  (al S y))
	   (x  (project@ S xx))
	   (x  (al S x)))
    (cond
     ((null? x)
      (<=> y (())))

     ((pair? x)
      (sup=.. (vector (list | (car x) (cdr x))) y))

     ((or (number? x) (null? x))
      (<cut> (<=> (x) y)))
          
     ((procedure? x)
      (<cut>
       (sup=.. (vector (list x)) y)))
     
     ((vector? x)
      (<cut>
        (<match> (#:mode - #:name =..1) (x)
          (#(l) 
           (<cut>
             (<match> (#:mode - #:name =..2) (y)
              ((f . u)
               (<cut>
                (<let> ((f (<lookup> f)))                   
                  (cond 
                   ((string? f)
                    (<let> ((g (module-ref (current-module) 
                                           (string->symbol f))))
                      (if (procedure? g)
                          (<=> l  ,(cons g u))
                          (existence_error 
                           procedure 
                           (vector `(,divide ,f
                                             ,(length (<scm> u))))))))
                   (else
                    (<=> l y))))))
              (y
               (<cut> 
                (<=> l y))))))
          (#(a b) (<cut> <fail>))
          (_    (type_error 'compound x)))))

     ((<var?> x)
      (<cut>
       (cond
        ((<var?> y)
         (instantiation_error))
       
        ((list? y)
	 (<<match>> (#:mode - #:name =..3) (y)
           ((,| a b)
	     (<=> x ,(cons a b)))

	   ((f . l)
             (<cut>
              (<let> ((f (<lookup> f)))
               (cond              
                ((or (procedure? f) (string? f))
                 (<=> xx ,(vector (cons f l))))

                ((and (number? f) (null? (<lookup> l)))
                 (<=> xx f))

		((or (pair? f) (vector? f))
		 (<=> xx ,(vector (cons f l))))

                ((number? f)
                 (type_error atom f))
              
                ((<var?> f)
                 (instantiation_error))
		
		((eq? f |)
		 (<=> x ,(apply cons* l)))

		((eq? f '()) 
		 (if (null? l)
		     (<=> x ())
		     (<=> x ,(vector y))))
		 
                (else
                 (type_error atom f))))))
            (()
             (<cut>
              (type_error list y)))))

        ((pair? y)
	 (<recur> lp ((z y))
	   (if (pair? z)
	       (lp (cdr z))
	       (if (<var?> z)
		   (instantiation_error)
		   (type_error list y)))))
       
        (else
         (type_error list y))))))))

(<define-guile-log-rule> (mac=.. a b) (sup=.. a b))
(mk-prolog-biop     'xfx "=.." tr=.. =.. mac=.. a a)

(define-goal-functor (#{;}# x y)
  (<var> (if then)
    (<if> (<=> x ,(vector `(,-> ,if ,then)))
	  (<if> (goal-eval if) (goal-eval then) (goal-eval y))
	  (<if> (<=> x ,(vector `(,*-> ,if ,then)))
		(<soft-if> (goal-eval if) (goal-eval then) (goal-eval y))
		(<if> (<=> x ,(vector `(,-i> ,if ,then)))
		      (<if-ii> (goal-eval if) (goal-eval then) (goal-eval y))
		      (<or> (goal-eval x) (goal-eval y)))))))

(set-object-property! #{;}# 'goal-compile-stub 
  (lambda (a b)
    `((@ (logic guile-log) <or>) ,a ,b)))
(set-object-property! #{;}# 'goal-compile-types
                      '(g g))

(define-goal-transformer #{;}# (tr-disjunction stx n m x y)
  (match x
    ((('xfy _ "->" _) i t n m)
     (let lp ((i i))
       (match i
	((x) (lp x))
	(_
	 #`(--if #,(goal stx i) #,(goal stx t) #,(goal stx y))))))

    ((('xfy _ "-i>" _) i t n m)
     (let lp ((i i))
       (match i
	((x) (lp x))
	(_
	 #`(--if-ii #,(goal stx i) #,(goal stx t) #,(goal stx y))))))
    
    ((('xfy _ "*->" _) i t n m)
     (let lp ((i i))
       (match i
	((x) (lp x))
	(_
	 #`(--soft-if #,(goal stx i) #,(goal stx t) #,(goal stx y))))))
    (_
     #`(--or #,(goal stx x) #,(goal stx y)))))

(bind-operator-to-functor #{;}# ";")

(define-goal-functor (#{;;}# x y)
  (<var> (if then)
    (<if> (<=> x ,(vector `(,-i> ,if ,then)))
	  (<if>   (<once-ii> (goal-eval if)) (goal-eval then) (goal-eval y))
          (<or-ii> (goal-eval x) (goal-eval y)))))


(set-object-property! #{;;}# 'goal-compile-stub 
  (lambda (a b)
    `((@ (logic guile-log iinterleave) <or-ii>) ,a ,b)))

(set-object-property! #{;;}# 'goal-compile-types
                      '(g g))
(define-goal-transformer #{;;}# (tr-disjunction-ii stx n m x y)
  (match x
    ((('xfy _ "-i>" _) i t n m)
     (let lp ((i i))
       (match i
	((x) (lp x))

	(((_ _ (or "<" ">" "=<" ">=" "==" "=:=" "=\\=") _) a b n2 m2)
	 #`(--if-ii #,(goal stx i) #,(goal stx t) #,(goal stx y)))
	(_
	 #`(--if-ii #,(goal stx i) #,(goal stx t) #,(goal stx y))))))
    (_
     #`(--or-ii #,(goal stx x) #,(goal stx y)))))

(bind-operator-to-functor #{;;}# ";;")


(<define-guile-log-rule> (<unify_with_occurs_check> x y) (<=> x y))
(mk-prolog-term-2 tr-uoc unify_with_occurs_check <unify_with_occurs_check> 
		  v a)

;; CALL
(define call
  (<case-lambda>
   ((g)
    (<code> (gp-var-set *call-expression* g S))
    (goal-eval g))
   ((g . l)
    (<<match>> (#:mode - #:name 'call) (g)
      (#(u)
       (<and>
	(<code> (gp-var-set *call-expression* g S))
	(goal-eval  (vector (append (<scm> u) l)))))
      (else
       (goal-eval (vector (cons g l))))))))

#|
(<define-guile-log-rule> (call-mac g) (call-fkn g))
(mk-prolog-term-1 tr-call call call-mac a)
|#

;; CATCH
(<define> (catch-fkn cut scut g c h)
  (<with-cut> cut scut
    (<code> (gp-var-set *call-expression* g S))
    (<catch> 'prolog #f 
      (<lambda> () (goal-eval g))
      (<lambda> (tag next x)
        (<or> 
         (<and> (<=> c x) <cut> (goal-eval h))
         (next))))))
        
(<define-guile-log-rule> (catch-mac g c h) (catch-fkn CUT SCUT g c h))
(mk-prolog-term-3 tr-catch catch catch-mac a a a)

;; THROW
(<define> (throw x) (throw-it (<scm> x)))

;; COPY_TERM
(<define-guile-log-rule> (<copy_term> x l)
   (<=> ,l ,(<cp> x))
   (do-attribute-constructors))

(mk-prolog-term-2 tr-cp copy_term <copy_term> a a)

(<define> (uniq X goal)
  (<uniq> (</.> (goal-eval goal)) X))

(<define> (yield_at_change test action)
   (<yield-at-change> test
     (<lambda> (start?) (goal-eval action))))

(define-syntax-rule (mk-test tr-nm fk-nm -nm x code)
  (begin
     (<define-guile-log-rule> (-nm x) 
       (<and> (if code <cc> <fail>)))
     (mk-prolog-term-1 tr-nm fk-nm -nm a)))

(mk-test tr-var      var      -var      x (attvar? x))
(mk-test tr-atom     atom     -atom     x (let ((y (<lookup> x)))
                                            (or (string? y)
                                                (procedure? y)
                                                (char? y)
                                                (null? y))))
(mk-test tr-integer  integer*  -integer  x (and
                                            (number? (<lookup> x))
                                            (integer? (<lookup> x))))
(mk-test tr-float    float*    -float    x (and (number? (<lookup> x))
                                                (inexact? (<lookup> x))))


(define-syntax-rule (mk-num float float*)
  (<define> (float x)
    (<match> (#:mode - #:name float) (x)
     (#((,op2- x))
      (<cut> (float* x)))
     (x
      (<cut> (float* x))))))

(mk-num integer -integer)
(mk-num float+  -float)

(define float
  (case-lambda
    ((x) (exact->inexact x))
    (x   (apply float+ x))))

     

(define (atomic? s x)    
  (let ((y (gp-lookup x s)))
    (or (symbol? y) (null? y)
        (number?  y) (string? y) (char? y) (procedure? y))))

(define (comp? s x)
  (let ((y (gp-lookup x s)))
    (or (vector? y)
        (gp-pair- y s)
        (pair? y))))

(mk-test tr-atomic   atomic    -atomic   x (atomic? S x))
(mk-test tr-compound compound* -compound x (comp? S x))
(mk-test tr-nonvar   nonvar    -nonvar   x (not (attvar? x)))
(mk-test tr-number   number*   -number   x (number? (<lookup> x)))

(mk-num number -number)

(<define> (compound x)
  (<not> (number x))
  (-compound x))

(define (gp->list x s)
  (if (gp-pair? x s)
      (cons (gp-car x s) (gp->list (gp-cdr x s) s))
      '()))

(<define> (functor comp nm arity)
  (<match> (#:mode - #:name functor) (comp)
    (#((f . l))
     (<cut>
      (<and>
       (<=> f nm)
       (<=> arity ,(length (gp->list l S))))))

    ((a b . l)
     (<cut>
      (<and>
       (<=> nm ",")
       (<=> arity 2))))

    ((a . l)
     (<cut>
      (<and>
       (<=> nm ".")
       (<=> arity 2))))

    (f
     (<cut>
      (<let> ((f (<lookup> f)))
       (cond
        ((or (procedure? f) (number? f) (string? f) (null? f) (char? f))
         (<and> (<=> f nm) (<=> 0 arity)))

        ((<var?> f)
         (<let> ((arity (<lookup> arity)))
           (cond
            ((<var?> arity)
             (instantiation_error))
            ((and (number? arity) (integer? arity))
             (<let> ((nm (<lookup> nm)))                    
               (cond 
                ((<var?> nm)
                (instantiation_error))
                ((or (procedure? nm) (number? nm) (char? nm) (string? nm)
                     (null? nm))
                 (if (>= arity 0)
                     (cond
                      ((= arity 0)
                       (<=> f nm))
                      ((> arity (get-flag max_arity))
                       (representation_error max_arity))
                      ((procedure? nm)
                       (<=> f ,(vector (cons nm 
                                             (let lp ((n arity))
                                               (if (= n 0)
                                                   '()
                                                   (cons (gp-var! S) 
                                                         (lp (- n 1)))))))))
                      (else
                       (type_error atom nm)))
                     (domain_error not_less_than_zero arity)))
                (else
                 (type_error atomic nm)))))
            (else
             (type_error integer arity)))))
               
        (else
         (type_error callable f))))))))
           
           


(<define> (arg n term a)
  (<let> ((n    (<lookup> n))
          (term (<lookup> term)))
    (<<match>> (#:mode - #:name arg) (n)
       (#((,op2- x))
        (if (integer? (<lookup> x))
            (domain_error not_less_than_zero n)
            (type_error integer n)))
       (_ (<cut> <cc>)))

    (cond
     ((procedure? term)
      <fail>)

     ((<var?> term)
      (instantiation_error))
	      
     ((<var?> n) <cc>)

     ((not (integer? n))
      (type_error integer n))
     
     ((< n 0)
      (domain_error not_less_than_zero n))

     (else <cc>))
    
    (cond
     ((<var?> n)
      (<let> ((lam (<lambda> (term)
		     (<recur> lp ((i 0) (term term))
		       (<match> (#:mode + #:name arg) (term)
			 ((,a . l)
			  (<=> n i))
			 ((_  . l) 
			  (lp (+ i 1) l))
			 (_ <fail>))))))

	  (cond 
	   ((and (vector? term) (= (vector-length term) 1))
	    (lam (vector-ref term 0)))
	   (else
	    (<<match>> (#:mode - #:name 'arg2) (term)
	      ((x . l) (lam (list cons x l)))
	       (_ (domain_error "term" term)))))))

       ((and (vector? term)  (= (vector-length term) 1))
	(<recur> lp ((l (vector-ref term 0)) (n n))
	  (<<match>> (#:mode - #:name arg-2) (l)
	    ((x . l)
	     (if (= n 0)
		 (<=> x a)
		 (lp l (- n 1))))
	    (()
	     <fail>))))
       (else
	(<<match>> (#:mode - #:name 'arg3) (term)
	  ((x . l)
	   (cond
	    ((= n 0) (<=> a cons))
	    ((= n 1) (<=> a x))
	    ((= n 2) (<=> a l))
	    (else
	     <fail>)))
	  (()
	   <fail>)
	  (_
	   (domain_error "term" term)))))))
  

(<define> (procedure_name f out)
   (<let> ((f (<lookup> f)))
      (if (procedure? f)
          (<=> out ,(procedure-name f))
          (if (<var?> f)
              (instantiation_error)
              (type_error procedure f)))))

(<define> (fail) <fail>)
(<define> (false) <fail>)
(<define> (true) <cc>)

#;
(define-goal-functor (!)
  (<with-fail> CUT <cc>))

(<define-guile-log-rule> (<!>)
  (<and> <cut>))

(mk-prolog-term-0 tr-! ! <!>)

(define cc  (lambda x #t))
(define p   (lambda x #f))
(define ss  (fluid-ref *current-stack*))

(define *once* (gp-make-var #t))

(<define> (once-f v)
 (<let> ((v (<lookup> v)))
   (if (<var?> v)
       (instantiation_error)
       (<let> ((p P))
	  (<code> (gp-var-set *call-expression* v S))
	  (goal-eval v)
	  (<with-fail> p <cc>)))))

(<define> (once_i v)
 (<let> ((v (<lookup> v)))
   (if (<var?> v)
       (instantiation_error)
       (<let> ((p P))
	  (<code> (gp-var-set *call-expression* v S))
	  (<once-ii> (goal-eval v))
	  (<with-fail> p <cc>)))))
 
(<define-guile-log-rule> (once-mac v) (once-f v))
(mk-prolog-term-1 tr-once once once-mac a)



(define halt
  (case-lambda 
    ((s p cc)    'halt)
    ((s p cc x)  
     ((<lambda> (x)
        (<let> ((x (<lookup> x)))
           (cond
            ((<var?> x)
             (instantiation_error))
            ((not (integer? x))
             (type_error integer x))
            (else
             (<ret> `(halt ,x))))))
      s p cc x))))


(name-as gop2- op2-)
(name-as gop2+ op2+)
(name-as #{g\\}# #{\\}#) 
(name-as gop2* op2*)
(name-as gop2/ op2/)
(name-as g//   //)
(name-as g**   **)
(name-as g^    ^)
(name-as g<<   <<)
(name-as g>>   >>)
(name-as #{g/\\}# #{/\\}#) 
(name-as #{g\\/}# #{\\/}#) 
(name-as gop2<    op2<)
(name-as gop2>    op2>)
(name-as gop2>=   op2>=)
(name-as gop2=<   op2=<)
(name-as g=:=     =:= )
(name-as #{g=\\=}# #{=\\=}#)

(define first? #t)
(if first?
    (begin
      (set! (@ (logic guile-log prolog names) float)   float)
      (set! (@ (logic guile-log prolog names) number)  number)  
      (set! (@ (logic guile-log prolog names) integer) integer)
      
      (set! (@ (logic guile-log prolog names) prolog-and) #{,}#)      
      (set! (@@ (logic guile-log prolog analyze) #{,}#) #{,}#)      
      (set! (@ (logic guile-log prolog names) prolog-or)  #{;}#)      
      (set! (@ (logic guile-log prolog names) prolog-not) #{\\+}#)
      (set! (@ (logic guile-log prolog names) prolog=..)  =..)
      (set! (@ (logic guile-log prolog names) divide) op2/)
      (set! (@ (logic guile-log prolog names) plus)   op2+)
      (set! (@ (logic guile-log prolog names) unary-minus)  op2-)
      (set! (@ (logic guile-log prolog names) binary-minus) op2-)
      (set! (@ (logic guile-log prolog names) binary-plus)  op2+)
      (set! (@ (logic guile-log prolog names) fact)   :-)
      (set! (@ (logic guile-log prolog names) true)   true)
      (set! (@ (logic guile-log prolog names) fail)   fail)
      (set! (@ (logic guile-log prolog names) false)   false)
      (set! (@ (logic guile-log prolog names) !)      !)
      (set! (@ (logic guile-log prolog names) atom)   atom)
      (set! (@ (logic guile-log prolog names) call)   call)
      (set! (@@ (logic guile-log prolog variant) op2=) op2=)
      (set! (@@ (logic guile-log prolog modules) *once*) *once*)
      (set! (@@ (logic guile-log prolog dynamic) once-f) once-f)
      (set! first? #f)))

(define op1+ op2+)
(define op1- op2-)
(define gop1+ gop2+)
(define gop1- gop2-)

