(define-module (logic guile-log prolog modules)
  #:use-module (logic guile-log prolog parser)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (logic guile-log prolog pre)
  #:use-module (logic guile-log functional-database)
  #:use-module (logic guile-log)
  #:use-module ((logic guile-log umatch) #:select (gp-unwind gp-unwind-tail))
  #:use-module ((logic guile-log umatch) #:select (*current-stack* gp-var-set
								   gp-newframe
								   gp-unwind))
  #:use-module (system base language)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 format)
  #:use-module (ice-9 pretty-print)
  #:export
  (is-module-file?
   write-module-scratch
   namespace-switch 
   set-module-optable-from-current
   module-optable-ref
   module-optable-set!
   module-mac
   use-module-mac
   use_module
   parse-out-module
   ref-module-name
   ref-module
   module_name
   current_module))

(define op #f)
(define *ops* (@@ (logic guile-log prolog parser) *prolog-ops*))

(define (source-file stx)
  (let ((r (syntax-source stx)))
    (if r
	(let ((r (assq 'filename r)))
	  (if r
	      (cdr r)
	      #f))
	#f)))

(define (str-it x)
  (cond
   ((string? x)
    x)
   ((symbol? x)
    (symbol->string x))
   ((procedure? x)
    (procedure-name x))))

(define (make-file-from-path path ext)
  (let lp ((l (reverse path)) (first? #t) (r ""))
    (if (pair? l)
	(let ((rr (str-it (car l))))
	  (if first? 
	      (set! r (string-append rr ext))
	      (set! r (string-append r "/" rr)))
	  (lp (cdr l) #f r))
	r)))

(define (hh x)
  (list->string
   (let lp ((l (string->list (format #f "~a" x))))
     (if (pair? l)
	 (let ((x (car l)))
	   (if (eq? x #\\)
	       (cons* #\\ (lp (cdr l)))
	       (cons* x   (lp (cdr l)))))
	 '()))))
	
(define relative-path (make-fluid #f)) 
(define* (write-module fpl fscm path #:key (boot? #t))
  (define (m p) p)

  (define module-data-opdata
    (with-input-from-file fpl
      (lambda ()
	(prolog-parse-module #f))))

  (define module-data (let lp ((l module-data-opdata))
			(if (pair? l)
			    (let ((x (car l))) 
			      (if (not (and (pair? x) 
					    (member (car x) 
						    '(#:op #:term #:goal))))
				  (cons x (lp (cdr l)))
				  (lp (cdr l))))
			    '())))

  (define module-opdata (let lp ((l module-data-opdata))
			  (if (pair? l)
			      (let ((x (car l))) 
				(if (and (pair? x) (eq? (car x) #:op))
				    (cons (cdr x) (lp (cdr l)))
				    (lp (cdr l))))
			      '())))

  (define module-termdata (let lp ((l module-data-opdata))
			    (if (pair? l)
				(let ((x (car l))) 
				  (if (and (pair? x) (eq? (car x) #:term))
				      (cons (cadr x) (lp (cdr l)))
				      (lp (cdr l))))
				'())))

  (define module-goaldata (let lp ((l module-data-opdata))
			    (if (pair? l)
				(let ((x (car l))) 
				  (if (and (pair? x) (eq? (car x) #:goal))
				      (cons (cadr x) (lp (cdr l)))
				      (lp (cdr l))))
				'())))

  (define module-dyndata (let lp ((l module-data-opdata))
			    (if (pair? l)
				(let ((x (car l))) 
				  (if (and (pair? x) (eq? (car x) #:dyn))
				      (cons (cadr x) (lp (cdr l)))
				      (lp (cdr l))))
				'())))

  (define (add.. n x)
    (if (> n 0)
	(add.. (- n 1) (string-append "../" x))
	x))
  
  (define (h2 x)
    (list->string (let lp ((l (string->list x)))
		    (if (pair? l)
			(let ((x (car l)))
			  (if (eq? x #\\)
			      (cons* #\\ #\\ #\\ #\\ (lp (cdr l)))
			      (cons  x       (lp (cdr l)))))
			'()))))
  (define (h x)
    (list->string (let lp ((l (string->list x)))
		    (if (pair? l)
			(let ((x (car l)))
			  (if (eq? x #\\)
			      (cons* #\\ #\\ (lp (cdr l)))
			      (cons  x       (lp (cdr l)))))
			'()))))

  
  (lambda ()
    (define rpl #f)
    (let* ((r    module-data)
	   (a.b  (let lp ((rpl (string-split fpl  #\/))
			  (rsc (string-split fscm #\/)))
		   (if (pair? rpl)
		       (if (equal? (car rpl) (car rsc))
			   (lp (cdr rpl) (cdr rsc))
			   (cons (add.. (length rsc) (string-join rpl "/"))
				 (string-join rsc "/")))
		       (error "bug in module code"))))
	   (rpl0  (if (fluid-ref relative-path) (car a.b) fpl))
	   (rsc   (cdr a.b)))
      (set! rpl rpl0)
      (if r
	  (format #t "
(define-module ~a
   #:use-module (logic guile-log iso-prolog)
   #:use-module (logic guile-log)
   #:use-module (logic guile-log prolog goal-expand)
   #:use-module (logic guile-log guile-prolog ops)
   #:use-module (logic guile-log guile-prolog attribute)
   #:use-module (logic guile-log guile-prolog coroutine)  
   #:use-module (logic guile-log guile-prolog gc-call)
   #:use-module (logic guile-log guile-prolog set)
   #:use-module (logic guile-log guile-prolog interpreter)
   #:use-module (logic guile-log guile-prolog dynamic-features)
   #:pure
   #:duplicates (last replace)
   #:replace ~a)
" (m path) (map hh (append (cdr r) (if (pair? module-opdata) 
                                       (map caddr module-opdata)
                                       '()))))))
    (format #t "(clear-directives)~%")
    (format #t "((@ (guile) eval-when) (compile load eval)
		  ((@ (guile) fluid-set!)
		   *prolog-ops* *swi-standard-operators*))~%")
    (format #t "((@ (guile) eval-when) (compile load eval)
		 ((@ (guile) fluid-set!)
		   (@ (logic guile-log prolog parser) *term-expansions*)
		   '()))~%")
    (format #t "((@ (guile) eval-when) (compile load eval)
		  ((@ (guile) fluid-set!)
		   (@ (logic guile-log prolog parser) *goal-expansions*)
		   '()))~%")
    (format #t "((@ (guile) eval-when) (compile load eval)
		 ((@ (guile) define)
		  *prolog-dynamic-modularation*
		  '()))~%")

(if (not boot?)
    (format #t
" 
(compile-prolog-string
\"
  :- use_module(boot(if)).
  :- use_module(boot(dcg)).
  :- use_module(sandbox).
  :- use_module(user).
  :- use_module(swi(term_macro)).
\")
"))

    (when (pair? module-opdata)
       (format #t "(compile-prolog-string \"~%")

       (map (lambda (x)
              (format #t ":- op(~a,~a,'~a').~%"
                   (car x) (cadr x) (h2 (symbol->string (caddr x)))))
         module-opdata)
        (format #t "~%\")~%")
        (format #t "((@ (guile) define) *public-module-operators* ((@ (guile) list) ~{'~a ~}))~%"
         (map hh module-opdata))
     )

    (format #t "(compile-prolog-file ~s)~%" fpl)


    (when (pair? module-termdata)
        (format #t "((@ (guile) define) *public-module-term-expansions* ((@ (guile) list) ~{~a ~}))~%"
         module-termdata)
     )

    (when (pair? module-goaldata)
        (format #t "((@ (guile) define) *public-module-goal-expansions* ((@ (guile) list) ~{~a ~}))~%"
         module-goaldata)
     )

    (when (pair? module-dyndata)
        (format #t "((@ (guile) define) *prolog-dynamic-modularation* 
		     ((@ (guile) list) ~{~a ~}))~%"
         module-dyndata)
     )

    (format #t "((@ (guile) define) *optable-save-first* #t)
                ((@ (guile) when) *optable-save-first*
                 ((@ (guile) set!) *optable-save-first* #f)		 
                  (module-optable-set! (save-operator-table)))~%")
    (format #t "((@ (guile) define) *prolog-scm-path* ~s)~%"     "./")
    (format #t "((@ (guile) define) *prolog-reverse-path* ~s)~%" rpl)))


(define reg    (make-regexp "\\W*\\:\\-\\W*module\\(([^,]*)"))
(define ws     (make-regexp "^\\w*$"))
(define reg2   (make-regexp "^\\((.*)\\)$"))
(define prefix (make-regexp "^(.*)\\((.*)\\)$"))

(define (is-module-file? f) 
  (with-input-from-file f
    (lambda ()
      (let lp ((l (read-line)))
	(if (eof-object? l)
	    #f
	    (if (regexp-exec ws l)
		(lp (read-line))
		(let ((ret (regexp-exec reg l)))
		  (if ret
		      (let lp ((s (match:substring ret 1)) (r '()))
			(let ((rr (regexp-exec prefix s)))
			  (if rr
			      (lp (match:substring rr 2)
				  (cons
				   (match:substring rr 1)
				   r))
			      (reverse (cons s r)))))
		      #f))))))))

(define default-scratch "guile-prolog-scratch")
(define home (cdr (string-split (getenv "HOME") #\/)))
(define scratch-directory (append home (list default-scratch)))
(define (mkdir* x)
  (if (not (file-exists? x))
      (mkdir x)))

(mkdir* (string-append (getenv "HOME") "/" default-scratch))
(mkdir* (string-append (getenv "HOME") "/" default-scratch "/language"))
(mkdir* (string-append (getenv "HOME") "/" default-scratch "/language/prolog"))
(mkdir* 
 (string-append (getenv "HOME") "/" default-scratch "/language/prolog/modules"))

(define (write-module-scratch path pl)
  (let* ((pth (string-join (append scratch-directory 
				   '(language prolog modules)
				   path)
			   "/"))
	 (scm? (is-scm-path? pth)))
    (if scm?
	(if (equal? (car scm?) pl)
	    (apply check #f #t pth)            ;; equal paths, check timestamps
	    (apply check #f #f (list pl pth))) ;; always clobber
	(apply check #f #f (list pl pth)))     ;; always clobber
    (map string->symbol (map str-it (append '(language prolog modules) path)))))

(define (lpath x) (string-split x #\/))
(define (find-module-path x)
  (define l (lpath x))
  (let ground ((lps %load-path))
    (if (pair? lps)
	(let loop ((l l) (lp (string-split (car lps) #\/)))
	  (if (pair? lp)
	      (if (pair? l)
		  (if (equal? (car l) (car lp))
		      (loop (cdr l) (cdr lp))
		      (ground (cdr lps)))
		  (ground (cdr lps)))
	      l))
	#f)))

(define (is-prolog-prefix? x)
  (match x
    (("language" "prolog" "modules" . l)
     l)
    (_
     #f)))

(define (module-path file symbol)
  (define module-path (find-module-path file))
  (if module-path
      (if (equal? (car (reverse module-path))
		  (symbol->string symbol))
	  (map string->symbol module-path)
	  (error "prolog module name does not match filename, this is not supported"))
      (error "prolog module file is not in a load path of guile, make sure to place it correctly or change guile's %load-path")))
  
(define (module-optable-ref module)
  (if (module-defined? module '*module-optable*)
      (module-ref module '*module-optable*)
      *standard-opmap*))

(define (module-termexp-ref module)
  (if (module-defined? module '*prolog-term-expansions*)
      (module-ref module '*prolog-term-expansions*)
      '()))

(define module-optable-set!
  (case-lambda 
   (()
    (module-define! (current-module) '*module-optable* 
		    (opdata-ref)))
   ((x)
    (module-define! (current-module) '*module-optable* x))
   ((mod x)
    (module-define! mod ' *module-optable* x))))
  
(define module-termexp-set!
  (case-lambda 
   (()
    (module-define! (current-module) '*prolog-term-expansions* 
		    '()))
   ((x)
    (module-define! (current-module) '*prolog-term-epansions* x))
   ((mod x)
    (module-define! mod ' *prolog-term-expansions* x))))

(define module-goalexp-set!
  (case-lambda 
   (()
    (module-define! (current-module) '*prolog-goal-expansions* 
		    '()))
   ((x)
    (module-define! (current-module) '*prolog-goal-expansions* x))
   ((mod x)
    (module-define! mod ' *prolog-goal-expansions* x))))

(define module-dyn-set!
  (case-lambda 
   (()
    (module-define! (current-module) '*prolog-dynamic-modularation* 
		    '()))
   ((x)
    (module-define! (current-module) '*prolog-dynamic-modularation* x))
   ((mod x)
    (module-define! mod ' *prolog-goal-expansions* x))))
  
(define (set-optable-from-module module)
  (fluid-set! *term-expansions* 
	      (if (module-defined? module '*prolog-term-expansions*)
		  (module-ref module '*prolog-term-expansions*)
		  '()))
  (fluid-set! *goal-expansions* 
	      (if (module-defined? module '*prolog-goal-expansions*)
		  (module-ref module '*prolog-goal-expansions*)
		  '()))
  (opdata-set! 
   (if (module-defined? module '*module-optable*)
       (module-ref module '*module-optable*)
       *swi-standard-operators*)))       

(define (set-module-optable-from-current)
  (if (module-defined? (current-module) '*module-optable*)
      (module-set!    (current-module) '*module-optable* (opdata-ref))
      (module-define! (current-module) '*module-optable* (opdata-ref)))
  (if (module-defined? (current-module) '*prolog-term-expansions*)
      (module-set!    (current-module) '*prolog-term-expansions* 
		      (fluid-ref *term-expansions*))
      (module-define! (current-module) '*prolog-term-expansions*
		      (fluid-ref *term-expansions*)))
  (if (module-defined? (current-module) '*prolog-goal-expansions*)
      (module-set!    (current-module) '*prolog-goal-expansions* 
		      (fluid-ref *goal-expansions*))
      (module-define! (current-module) '*prolog-goal-expansions*
		      (fluid-ref *goal-expansions*)))

  (if (module-defined? (current-module) '*prolog-dynamic-modularation*)
      (module-set!    (current-module)  '*prolog-dynamic-modularation* 
		      (map (lambda (x) (cons (car x)
					     (dynamic-env-ref (car x))))
			   (module-ref
			    (current-module)
			    '*prolog-dynamic-modularation*)))))

(define (regdyns)
  (if (module-defined? (current-module) '*prolog-dynamic-modularation*)
      (module-set!    (current-module)  '*prolog-dynamic-modularation* 
		      (map (lambda (x) (cons (car x)
					     (dynamic-env-ref (car x))))
			   (module-ref 
			    (current-module)
			    '*prolog-dynamic-modularation*)))))

(define (setup-dynamic-modularation)
  (if (module-defined? (current-module) '*prolog-dynamic-modularation*)
      (for-each
       (lambda (x) (dynamic-env-set! (car x) (cdr x)))
       (module-ref (current-module) '*prolog-dynamic-modularation*))))

(define-syntax-rule (with-dynamics code)
  (dynamic-wind
    (lambda () (regdyns))
    (lambda () (regdyns) code)
    (lambda () (setup-dynamic-modularation))))
      

(<define> (namespace-switch new-module code)
  (<let*> ((old-module  (current-module))
	   (fnew          (lambda x
			    (set-module-optable-from-current)
			    (set-optable-from-module new-module)
			    (set-current-module      new-module)
			    (setup-dynamic-modularation)))
	   (fold	 (lambda x
			   (set-module-optable-from-current)
			   (set-current-module      old-module)
			   (setup-dynamic-modularation)
			   (set-optable-from-module old-module))))

	  
     (<dynwind> fnew fold)
     (<code> (fnew))
     (code)
     (<dynwind> fold fnew)
     (<code> (fold))))

(<define> (module_name x)
  (<=> x 
       ,(if (module-defined? (current-module) '*prolog-module-name*)
	    (symbol->string (module-ref (current-module) 
					'*prolog-module-name*))
	    "nonmodule")))

(<define> (current_module x)
  (<=> x ,(current-module)))

(define (module-mac stx l n m)
  (let ((l    (get.. "," l)))
    (match l
      (((#:atom nm . _) . _)
       (module-define! (current-module) '*prolog-module-name* nm)
       #`(eval-when (eval load compile)
	   (define *prolog-current-module* '#,(datum->syntax stx nm)))))))

(define (check boot? chk? fpl fscm path)
   (when (or (not chk?)
	     (not (file-exists? fscm))
	     (let* ((mpl     (stat:mtime (stat fpl)))
		    (mscm    (stat:mtime (stat fscm))))
	       (< (+  mscm 10) mpl)))
      (pk `(write-scm ,fscm))
      (with-output-to-file fscm
	(write-module fpl fscm path #:boot? boot?))))

  (define (mk-file f ext)
    (let ((r (reverse f)))
      (reverse (cons (string-append
		      (str-it (car r))
		      ext)
		     (map str-it 
			  (cdr r))))))

(define (is-scm-path? str-scm)
  (if (file-exists? str-scm)
      (with-input-from-file str-scm
	(lambda ()
	  (let lp ((r (read)) (scm #f) (pl #f))
	    (match r
		   ((? eof-object?)
		    (if (and scm pl)
			(list pl scm)
			#f))
		   (('*prolog-scm-path* x)
		    (lp (read) x pl))
		   ((*prolog-reverse-path* x)
		    (lp (read) scm x))
		   (else
		    (lp (read) scm pl))))))
      #f))
	 
(define (find-path f)
  (let ((fpl  (mk-file f ".pl"))
	(fscm (mk-file f ".scm")))
    (let lp ((l %load-path))
      (if (pair? l)
	  (let ((str-scm (string-join
			  (append
			   (string-split (car l) #\/)
			   fscm) "/")))
	    (if (file-exists? str-scm)
		(is-scm-path? str-scm)
		(lp (cdr l))))
	  #f))))

(define (search-prolog-source f)
  (let ((fpl  (mk-file f ".pl"))
	(fscm (mk-file f ".scm")))
    (let lp ((l %load-path))
      (if (pair? l)
	  (let ((str-pl  (string-join
			  (append
			   (string-split (car l) #\/)
			   fpl) "/"))
		(str-scm (string-join
			  (append
			   (string-split (car l) #\/)
			   fscm) "/")))
	    (if (file-exists? str-pl)
		(list str-pl str-scm)
		(lp (cdr l))))	  
	  #f))))

(define* (pre-compile-prolog-file f #:key (boot? #t))
  (let ((pth (find-path f)))
    (if pth
	(apply check boot? #t (append pth (list f)))
	(let ((pth (search-prolog-source f)))
	  (if pth
	      (apply check boot? #t (append pth (list f)))
	      (if #f #f))))))

(define ref '*prolog-modules*)
(define (set-module! sym x)
  (let ((mod (current-module)))
    (when (not (module-defined? mod ref))
	  (module-define! mod ref (make-hash-table)))
    (let ((h (module-ref mod ref)))
      (hashq-set! h sym x))))

(define (proj x)
  (cond
   ((string? x)
    (string->symbol x))
   ((procedure? x)
    (procedure-name x))
   (else
    x)))

(define (ref-module sym)
  (let ((sym (proj sym))
	(mod (current-module)))
    (if (module-defined? mod ref)
	(hashq-ref (module-ref mod ref) sym #f)
	#f)))

(set! (@@ (logic guile-log prolog goal-functors) get-module) ref-module)

(define (ref-module-name sym)
  (let ((mod (current-module)))
    (if (module-defined? mod ref)
	(let ((r (hashq-ref (module-ref mod ref) sym #f)))
	  (if r
	      (module-name r)
	      #f))
	#f)))

(define-syntax-rule (wrap-* l)
  (let* ((fr1 (gp-newframe (fluid-ref *current-stack*)))
	 (fr2 (gp-newframe fr1)))
    (let ((res (with-fluids ((*current-stack* fr2)) l)))
      (gp-unwind-tail fr1)
      res)))

(define-syntax-rule (with-state code ...)
  (with-fluids ((*term-expansions* 
		 (fluid-ref *term-expansions*))
		(*goal-expansions* 
		 (fluid-ref *goal-expansions*))		       
		(*ops*       
		 (fluid-ref *ops*))
		(*current-language* (lookup-language 'scheme)))
    (with-dynamics
     (begin code ...))))
  
(define (use-module-mac stx l n m)
  (letrec ((lp (lambda (l)
  (define (atom c)
    (match c
	   ((#:atom n . l)
	    n)))

  (define (imp x)
    (match x
       ((#:list l . _)
	(get.. "," l))
       (x (list x))))

  (define (f c p)
    (define (mk x)
      (if p
	  x
	  x))
	  
    (match c
	   ((#:atom n . l)
	    `(,(mk n)))
	   ((#:term a l . _)
	    `(,@(f a #f) ,@(f l p)))))

  (define (-> x)    
    `(language prolog modules ,@(f l #t)))
       
  (define nm (car (reverse (f l #f))))

  (let ((l (get.. "," l)))
    (match l
      (((#:list l . _))
       (with-syntax (((f ...)
		      (map (lambda (x)
			     (datum->syntax stx (-> x)))
			   (get.. "," l))))
       #`(begin
	   #,@(map lp l))))

      ((l)
       (let ((pth (-> l))
	     (h   (append '(language prolog modules) 
			  (f l #f))))
	 (with-syntax ((f2   (datum->syntax stx pth))		      
		       (ff   (datum->syntax stx h)))
	   (with-state
	    (wrap-*
	     (begin
	       (set-module! nm (resolve-module pth))
	       (pre-compile-prolog-file h)
	       (process-use_module (list (list pth))))))

	   (use-pub-ops (resolve-module pth) (fluid-ref *current-stack*))
	   (use-pub-goal-expansions (resolve-module pth) 
				    (fluid-ref *current-stack*))
	   (use-pub-term-expansions (resolve-module pth) 
				    (fluid-ref *current-stack*))

	   #`(begin
	       (with-state
		(wrap-* (pre-compile-prolog-file 'ff))
		(set-module! '#,(datum->syntax stx nm)
			     (resolve-module 'f2)))

	       (with-state
		(wrap-* (process-use_module '((f2)))))

	       (use-pub-ops (resolve-module 'f2) 
			    (fluid-ref *current-stack*))
	       (use-pub-goal-expansions (resolve-module 'f2) 
					(fluid-ref *current-stack*))
	       (use-pub-term-expansions (resolve-module 'f2) 
					(fluid-ref *current-stack*))))))
						
      ((name imports)
       (let* ((g  (-> name))
	      (h  (append '(language prolog modules) (f name #f)))
	      (ff (datum->syntax stx h))
	      (f  (datum->syntax stx g)))
	 (define (ff x)
	   (match x
	      (((_ _ "as" _) m n)
	      `((,(atom m) . ,(atom n))))
	     (x (atom x))))
	  (begin
	    (wrap-*
	     (with-state
	      (pre-compile-prolog-file h)
	      (set-module! nm (resolve-module g))
	      (process-use_module (list (list g '(#:duplicates (last)))))))
  
	    (use-pub-ops (resolve-module g) (fluid-ref *current-stack*))
	    (use-pub-goal-expansions (resolve-module g) 
				     (fluid-ref *current-stack*))
	    (use-pub-term-expansions (resolve-module g) 
				     (fluid-ref *current-stack*)))
	 #`(begin
	     (with-state
	      (wrap-*
	       (pre-compile-prolog-file '#,ff))	      
	      (set-module! '#,(datum->syntax stx nm) (resolve-module 'f)))

	     (with-state
	      (wrap-*
	       (process-use_module 
		'((#,f
		   '(#:duplicates (last))
		   #:select 
		   #,(datum->syntax stx (map ff (imp l))))))))

	     (use-pub-term-expansions (resolve-module 'f2) 
				      (fluid-ref *current-stack*))
	     (use-pub-goal-expansions (resolve-module 'f2) 
				      (fluid-ref *current-stack*))
	     (use-pub-ops (resolve-module 'f2) 
			  (fluid-ref *current-stack*))))))))))
    (lp l)))

	
(<define> (get-mod x y)
  (<<match>> (#:mode - #:name get-mod) (x)
    (#((a b))
     (<cut>
      (<let> ((aa (<lookup> a)))
       (when (procedure? aa))
       (<var> (l)
	 (<=> y (,(procedure-name aa) . l))
	 (get-mod b l)))))
    (a
     (<cut>
      (<let> ((aa (<lookup> a)))
       (when (procedure? aa))
       (<=> y (,(procedure-name aa))))))))
      	

(define (mod-last l)
  (define (change x) x)
  (let ((rev (reverse l)))
    (reverse (cons (change (car rev)) (cdr rev)))))

(define (process-use_module module-interface-args)
  (with-fluids ((*current-language* (lookup-language 'scheme)))
    (let ((interfaces (map (lambda (mif-args)
			     (or (apply resolve-interface mif-args)
				 (error "no such module" mif-args)))
			   module-interface-args)))
      (module-use-interfaces! (current-module) interfaces)
      (let lp ((is interfaces))
	(if (pair? is)
	    (begin
	      (module-for-each 
	       (lambda (k v)
		 (if (module-locally-bound? (current-module) k)
		     (module-remove! (current-module) k)))
	       (car is))
	      (lp (cdr is))))))))
      

(define specials  '(*prolog-modules* 
		    *prolog-term-expansions* 
		    *prolog-goal-expansions* 
		    *public-module-operators*
		    *public-module-term-expansions*
		    *public-module-goal-expansions*))
    
(define (undef-symbols mod)
  (lambda (syms)
   (for-each
    (lambda (x)
      (if (and 
	   (not (member x specials))
	   (module-defined? (current-module) x)
	   (not (eq? (module-ref (current-module) x)
		     (module-ref mod              x))))
	  (module-remove! (current-module) x)))
    (let lp ((l syms))
      (if (pair? l)
	  (if (pair? (car l))
	      (cons (cdar l) (lp (cdr l)))
	      (cons (car  l) (lp (cdr l))))
	  '())))))

(define *once* #f)
(define (modspec x)
  (let* ((fx (cadr x))
	 (fx (if (symbol? fx)
		 (module-ref (resolve-module '(logic guile-log prolog names))
			     fx))))
    (list (car x) fx (caddr x))))

(define-syntax-rule (with-s s code ...)
  (let ((s (gp-newframe s)))
    code ...
    (gp-unwind-tail s)))
 
(define (use-pub-ops mod s)
  (with-s s
    (if (module-defined? mod '*public-module-operators*)
	(let lp ((l (module-ref 
		     mod
		     '*public-module-operators*)))
	  (if (pair? l)
	      (begin
		(apply op 
		       s
		       (lambda x x)
		       (lambda x x) 
		       (modspec (car l)))
		(lp (cdr l))))))))

(define (use-pub-term-expansions mod s)
  (with-s s                        
    (if (module-defined? mod '*public-module-term-expansions*)
	(let lp ((l (module-ref 
		     mod
		     '*public-module-term-expansions*)))
	  (if (pair? l)
	      (begin
		(fluid-set! *term-expansions*
			    (cons (car l)
				  (fluid-ref *term-expansions*)))
		(lp (cdr l))))))))
  
(define (use-pub-goal-expansions mod s)
  (with-s s
    (if (module-defined? mod '*public-module-goal-expansions*)
	(let lp ((l (module-ref 
		     mod
		     '*public-module-goal-expansions*)))
	  (if (pair? l)
	      (begin
		(fluid-set! *goal-expansions*
			    (cons (car l)
				  (fluid-ref *goal-expansions*)))
		(lp (cdr l))))))))
  
(<define> (use_module . l)
  (<let> ((p P))
    (<apply> use_module_ l)
    (<with-fail> p <cc>)))

(define use_module_
  (<case-lambda>
   ((mod syms)
    (<var> (mod2)
      (get-mod mod mod2)
      (<let> ((syms (<scm> syms)))
        (<values> (syms)		  
	  (cond
	   ((string? syms)
	    (<cc> (list (string->symbol syms))))
	   ((procedure? syms)
	    (<cc> (list (procedure-name syms))))
	   (else
	    (<recur> lp ((syms syms) (r '()))
	      (<<match>> (#:mode - #:name use_module_) (syms)
		((x . l)
		 (<cut>
		   (<values> (xx) (lp x '()))
		   (lp l (cons (car xx) r))))
		(#((f x y))
		 (<cut> 
                   (<values> (xx) (lp x '()))
		   (<values> (yy) (lp y '()))
		   (<cc> (list (cons (car xx) (car yy))))))
		(x
		 (<cut>
		  (cond
		   ((string? x)
		    (<cc> (list (symbol->string x))))
		   ((procedure? x)
		    (<cc> (list (procedure-name x))))))))))))
             
	(<let*> ((mname (car (reverse (<scm> mod2))))
		 (f1 (cons* 'language 'prolog 'modules (<scm> mod2)))
		 (f2 (cons* 'language 'prolog 'modules (mod-last 	
							(<scm> mod2)))) )
						
		(<code> 
		 (with-fluids ((*current-stack* S))
		   (wrap-* (pre-compile-prolog-file f1)))
		 ;(undef-symbols syms)
		 (let  ((mod (with-state
			      (resolve-module f2))))
		   (begin
		     (set-module! mname (resolve-module f2))
		     (with-state
		      (wrap-*
		       (process-use_module `((,f2 #:select syms)))))
		     (use-pub-ops             mod S)
		     (use-pub-goal-expansions mod S)
		     (use-pub-term-expansions mod S))))))))


   ((x)
    (<<match>> (#:mode - #:name use_module_2) (x)
      ((a)
       (<cut>
	(use_module_ a)))

      ((a . l) 
       (<cut>
	(<and>
	 (use_module_ a)
	 (use_module_ l))))
      
      (()
       (<cut> <cc>))
      
      (x
       (<cut>
        (<var> (y)
	 (get-mod x y)
	 (<let*> ((mname (car (reverse (<scm> y))))
		  (f1 (cons* 'language 'prolog 'modules (<scm> y)))
		  (f2 (cons* 'language 'prolog 'modules (mod-last (<scm> y)))))
	    (<code> 
	     (with-fluids ((*current-stack* S))
	       (wrap-* (pre-compile-prolog-file f1)))
	     (let ((mod (with-fluids 
			  ((*ops* (fluid-ref *ops*))
			   (*current-language* 
			    (lookup-language 'scheme))
			  (*term-expansions* 
			   (fluid-ref *term-expansions*))
			  (*goal-expansions* 
			   (fluid-ref *goal-expansions*)))

			  (resolve-module f2))))
	       (set-module! mname (resolve-module f2))
	       (module-for-each (lambda x 
				  ((undef-symbols mod)
				   (list (car x))))
				mod)
	       (with-fluids ((*ops* (fluid-ref *ops*))
			     (*term-expansions* 
			      (fluid-ref *term-expansions*))
			     (*goal-expansions* 
			      (fluid-ref *goal-expansions*))
			     (*current-stack* S))
		 (wrap-* (process-use_module `((,f2)))))
	       (use-pub-ops mod S)
	       (use-pub-goal-expansions mod S)
	       (use-pub-term-expansions mod S)))))))))))


(define (parse-out-module stx x)
  (match x
    ((#:atom nm . _)
     (datum->syntax stx nm))
    (_ (error "in parsing out module information in prolog namespacing"))))

(set! (@@ (logic guile-log prolog goal-functors) namespace-switch) namespace-switch)
