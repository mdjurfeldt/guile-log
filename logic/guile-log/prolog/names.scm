(define-module (logic guile-log prolog names)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log functional-database)
  #:use-module (logic guile-log prolog error)
  #:use-module (ice-9 match)
  #:replace (force)
  #:export (make-unbound-fkn mk-sym make-sym
			     call
                             ;;goal
                             character_code
                             not_less_than_zero
                             evaluable
                             callable
                             modify
                             static_procedure
                             private_procedure
                             access
                             end_of_file
                             character
                             predicate_indicator
                             source_sink

			     write           

                             ;; goal transformers
                             divide
                             plus
			     unary-minus
			     binary-minus
			     binary-plus
                             fact
                             true
                             !
                             fail
                             float
                             number
                             integer
                             atom
			     prolog-or
			     prolog-and
			     prolog-not
			     prolog=..

                             ;; directives
                             operator_specifier
                             fx
                             fy
                             xf
                             yf
                             xfx
                             yfx
                             xfy
                             operator_priority
                             flag_value
                             flag
                             prolog_flag
                             on
                             off
                             false
                             towards_zero
                             down
                             warning
                             chars
                             codes
                             bounded
                             auto_sym
                             max_integer
                             min_integer
                             integer_rounding_function
                             debug
                             max_arity
                             unknown
                             double_quotes
                             generate_debug_info
			     debug_term_position
			     clpfd_monotonic
			     clpfd_propagation
			     full
			     clpb_validation
			     clpfd_goal_expansion

                             get_flag
                             is-a-num?
                             check-num

                             ;;io
                             quoted
                             ignore_ops
                             numbervars
                             write_option
			     functional

                             read
                             input
                             output
                             stream_or_alias
                             stream
                             stream_option
                             io_mode
                             text
                             binary
                             type
                             eof_action
                             reposition
                             alias
;                            reset
                             eof_code
                             at
                             no
                             end_of_stream
                             position
                             variable
                             mode
                             file_name
                             binary_stream
                             text_stream
                             byte
                             variables
                             variable_names
                             singletons
                             read_option
                             ;;char-conversion
                             char_conversion
                             char-convert))
                             

(define first? #t)
(define get-flag #f)

(define cc  (lambda x #t))
(define p   (lambda x #f))
(define ss  (fluid-ref *current-stack*))


(define (make-unbound-fkn nm)
  (letrec ((warn-message  
	    (lambda (f) 
	      (format #f "fkn ~a is not evaluable, will fail" f)))
           (d #f)
           (f (lambda k
                (match k
                  (()
                   (if d
                       d
                       (begin
                         (mk-dyn nm (lambda (x) (set! d x)))
                         d)))
		  
		  ((a b c . l)
                   (if d (apply d k)
                   (apply 
                    (<lambda> l
                      (<let> ((e (get-flag unknown)))
                        (cond
                          ((eq? e error)
                           (existence_error 
                            procedure 
                            (vector `(,divide ,f
                                              ,(length l)))))
                          ((eq? e warning)
                           (<code> (warn (warn-message (procedure-name f))))
                           <fail>)

                          ((eq? e fail)
                           <fail>)

                          (else
                           (<code>
                            (error 
                            "Bug in prolog flag 'unknown' implementation"))))))
                    a b c l)))

                  (_ 
                   (type_error ss p cc evaluable
                               (vector `(,divide ,f
                                                 ,(- (length k) 0)))))))))
    (set-object-property! f 'prolog-symbol #t)
    f))

(define-syntax-rule (mk-sym a)
  (when (not (module-defined? (current-module) 'a))        
    (define! 'a (make-unbound-fkn 'a))
    (set-procedure-property! a 'module (module-name (current-module)))
    (set-procedure-property! a 'name 'a)))

(define (make-sym mod a)
  (if (not (module-defined? mod a))
    (let ((f (make-unbound-fkn a)))
      (module-define! mod a f)
      (set-procedure-property! f 'module (module-name mod))
      (set-procedure-property! f 'name a)      
      f)
    #f))

(mk-sym is-a-num?)
(mk-sym check-num)

(mk-sym divide)
(mk-sym prolog-and)
(mk-sym prolog-not)
(mk-sym prolog-or)
(mk-sym prolog=..)
(mk-sym unary-minus)
(mk-sym binary-minus)
(mk-sym binary-plus)
(mk-sym plus)
(mk-sym fact)
(mk-sym true)
(mk-sym fail)
(mk-sym !)
(mk-sym atom)

(mk-sym character_code)
(mk-sym not_less_than_zero)
(mk-sym evaluable)
(mk-sym callable)
(mk-sym modify)
(mk-sym static_procedure)
(mk-sym private_procedure)
(mk-sym access)
(mk-sym end_of_file)
(mk-sym character)
(mk-sym predicate_indicator)
(mk-sym source_sink)

(mk-sym clpfd_monotonic)
(mk-sym clpfd_goal_expansion)
(mk-sym clpfd_propagation)
(mk-sym full)
(mk-sym clpb_validation)
(mk-sym operator_specifier)
(mk-sym fx)
(mk-sym fy)
(mk-sym xf)
(mk-sym yf)
(mk-sym xfx)
(mk-sym yfx)
(mk-sym xfy)
(mk-sym operator_priority)
(mk-sym flag_value)
(mk-sym flag)
(mk-sym prolog_flag)
(mk-sym on)
(mk-sym off)
(mk-sym false)
(mk-sym towards_zero)
(mk-sym down)
(mk-sym warning)
(mk-sym chars)
(mk-sym codes)
(mk-sym bounded)
(mk-sym auto_sym)
(mk-sym max_integer)
(mk-sym min_integer)
(mk-sym integer_rounding_function)
(mk-sym debug)
(mk-sym max_arity)
(mk-sym unknown)
(mk-sym double_quotes)
(mk-sym generate_debug_info)
(mk-sym debug_term_position)
(mk-sym float)
(mk-sym number)
(mk-sym integer)

(mk-sym force)
(mk-sym input)
(mk-sym output)
(mk-sym stream_or_alias)
(mk-sym stream)
(mk-sym stream_option)
(mk-sym io_mode)
(mk-sym text)
(mk-sym binary)
(mk-sym type)
(mk-sym eof_action)
(mk-sym reposition)
(mk-sym alias)
(mk-sym eof_code)
(mk-sym at)
(mk-sym no)
(mk-sym end_of_stream)
(mk-sym position)
(mk-sym variable)
(mk-sym mode)
(mk-sym file_name)
(mk-sym binary_stream)
(mk-sym text_stream)
(mk-sym byte)

(mk-sym variables)
(mk-sym variable_names)
(mk-sym singletons)
(mk-sym read_option)

(mk-sym quoted)
(mk-sym ignore_ops)
(mk-sym numbervars)
(mk-sym write_option)
(mk-sym functional)

(mk-sym char_conversion)
(mk-sym char-convert)

(define first #t)
(if first
    (begin
      (set! first #f)
      (set! (@@ (logic guile-log prolog error) number)      number)
      (set! (@@ (logic guile-log prolog error) integer)     integer)
      (set! (@@ (logic guile-log prolog error) source_sink) source_sink)))

(set! first? #f)
(define call #f)
