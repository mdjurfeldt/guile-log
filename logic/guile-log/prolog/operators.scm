(define-module (logic guile-log prolog operators)
  #:use-module ((logic guile-log)
		#:select (procedure-name S CUT SCUT <define> <with-cut>))
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:use-module (logic guile-log prolog pre)
  #:export (f->stxfkn
	    define-goal-transformer
	    define-goal-functor
	    define-scm-functor 
	    bind-operator-to-functor
	    get-attached-module
	    attach-defined-module!
	    define-op
	    get-operator-from-f))

(define do-print #f)
(define pp
  (case-lambda
    ((s x)
     (when do-print
       (pretty-print `(,s ,(syntax->datum x))))
     x)
    ((x)
     (when do-print
       (pretty-print (syntax->datum x)))
     x)))

(define ppp
  (case-lambda
    ((s x)
     (when #t
       (pretty-print `(,s ,(syntax->datum x))))
     x)
    ((x)
     (when #t
       (pretty-print (syntax->datum x)))
     x)))

(define arg #f)
(define (get-functor-stx stx f)
  (let ((f1 (object-property f 'goal-functor))
        (f* (if (string? f)
                (datum->syntax stx (string->symbol f))
                (datum->syntax stx (procedure-name f)))))
    (if f1
        (let ((r (object-property f1 'prolog-functor-stx)))
          (if r
              r
              f*))
        f*)))

(define (get-functor-stx? f)
  (let ((f1 (object-property f 'goal-functor)))
    (if f1
        (let ((r (object-property f1 'prolog-functor-stx)))
          (if r
              r
              #f))
        #f)))

(define (wrap-ns-sym f)
  (define res
    (lambda (q mod s . l)
      (cond
       ((not mod)
	(apply res q (current-module) s l))
       ((eq? mod 1)
	(if (symbol? s) (symbol->string s) s))
       ((pair? mod)
	(apply res q (resolve-module mod) s l))
       ((string? s)
	(apply res q mod (string->symbol s) l))
       (else
	(apply f q mod s l)))))
  res)

(define get-op-fkn 
  (wrap-ns-sym
   (lambda (module s n)
     (let ((f (lambda (n)
		(string->symbol (string-append
				 "op"
				 (number->string n)
				 (symbol->string s))))))

       (let lp ((ss (if n (f n) (f 2)))
		(nn (if n n     2)))
	 (define (next)
	   (if n
	       (if nn
		   (lp s #f)
		   (values #f #f))
	       (cond		    
		((eq? nn #f)
		 (values #f #f))
		((= nn 2)
		 (lp (f 1) 1))
		((= nn 1)
		 (lp s #f)))))
		       
	 (if (module-defined? module ss)
	     (let ((f (module-ref module ss)))
	       (if (or (object-property f 'prolog-symbol)
		       (object-property f 'prolog-functor-type))
		   (values f (object-property f 'prolog-functor-type))
		   (next)))
	     (next)))))))

(define (modstx) 
  (vector 'syntax-object '+ `((top) ,(vector 'ribcage '() '() '())) 
	  `(hygiene ,@(module-name (current-module)))))

(define (get-binding stx x f)
  (match x
    (#f
     (datum->syntax (modstx) f))

    ((#:atom _ #f #f n m)
     (datum->syntax (modstx) f))

    ((#:atom _ amp (#:string a . _) n m)
     #`(#,(datum->syntax stx amp) 
	(#,(datum->syntax stx (string->symbol a))) 
        #,(datum->syntax (modstx) f)))

   #; ((#:atom _ amp (#:atom a . _) n m)
     #`(#,(datum->syntax stx amp) (#,(datum->syntax stx a)) 
        #,(datum->syntax (modstx) f)))

    ((#:atom _ amp (#:atom ff . _) n m)
     #`(#,(datum->syntax stx amp) 
        #,(datum->syntax stx `(language prolog prolog-modules ,ff))
        #,(datum->syntax stx f)))

    ((#:atom _ amp l n m)
     (let ((l (map
               (lambda (x)
                 (match x
		   ((#:atom a _ _ n m) a)                
                   ((#:string a n m)
                    (string->symbol a))
		   (l l)
                   (_ (error (format #f
				     "wrong @ argument (~a) in ~a" x 
				     (get-refstr n m))))))
               (get.. "," l))))
       #`(#,(datum->syntax stx amp) 
	  #,@(if (pair? (car l))
		 (datum->syntax stx l)
		 (list (datum->syntax stx l)))
          #,(datum->syntax stx f))))))

(define bindings (make-fluid '()))

(define f->stxfkn 
  (wrap-ns-sym
   (lambda (worker mod f local? atom arg goal? stx N n m l . u)
     (if (or (pair? f) (vector? f))
	 f
	 (let ()
     (define ll (map (lambda (x) (arg stx x)) l))
     (define uu (if (pair? u) (arg stx (car u)) '()))
     (define lll (if (pair? u) (append ll (list uu)) ll))
     (define (wrap f) (get-binding stx atom f))

     (define (get-funct-from-proc f)
       (pk (list 'goal? goal? 'f f))
       (let ((extr (if (eq? goal? #:goal) #'(CUT SCUT) #'(S))))
	 (if worker
	     #`(#,worker #,(wrap (procedure-name f)) #,@extr #,@lll)
	     #`(#,(wrap (procedure-name f)) #,@extr #,@lll))))

     (define (get-func-from-proc f)
       (let ((extr (if (eq? goal? #:scm) #'(S) #'())))
	 (if worker
	     #`(#,worker #,(wrap (procedure-name f)) #,@extr #,@lll)
	     #`(#,(wrap (procedure-name f)) #,@extr #,@lll))))

     (define (get-func-from-sym f)
       (if worker
	   #`(#,worker #,(wrap f) #,@lll)
	   #`(#,(wrap f) #,@lll)))

     (define (get-funct/stx-from-proc f)
       (if goal?
	   (let ((ff (object-property f 'goal-transformer)))
	     (if ff	     
		 (apply ff stx n m l)
		 (get-funct-from-proc f)))
	   (get-func-from-proc f)))

     (if (match atom ((_ _ #:current-module . _) #t) (_ #f))
	 (if (symbol? f) (symbol->string f) f)
	 (call-with-values (lambda () (get-op-fkn mod f N))
	   (lambda (ff functor?)
	     (if ff
		 (if functor?
		     (get-funct/stx-from-proc ff)
		     (get-func-from-proc      ff))
		 (get-func-from-sym f))))))))))
	     
			  
;; (transformer stx n m arg ...)
(define-syntax define-goal-transformer 
  (syntax-rules () 
    ((_ func (ftr . a) . code)
     (begin
       (define (ftr . a) . code)
       (set-object-property! func 'goal-transformer ftr)
       (set-object-property! ftr  'goal-functor     func)))

    ((_ func ftr code)
     (begin
       (define ftr  code)
       (set-object-property! func 'goal-transformer ftr)
       (set-object-property! ftr  'goal-functor     func)))))

;; (<define> (functor cut arg ...) code ...)
(define-syntax-rule (define-op op gop)
  (begin
    (define op gop)
    (attach-defined-module! op)))

(define-syntax define-goal-functor 
  (syntax-rules ()
    ((_ (nm . a) . code)
     (begin
       (<define> (nm cut scut . a)
	 (<with-cut> cut scut . code))
       (attach-defined-module! nm)
       (set-object-property! nm 'prolog-functor-type #:goal)
       (set-object-property! nm 'prolog-functor-stx  #'nm)))

    ((_ nm code)
     (begin
       (define nm code)
       (attach-defined-module! nm)
       (set-object-property! nm 'prolog-functor-type #:goal)
       (set-object-property! nm 'prolog-functor-stx  #'nm)))))

(define-syntax define-scm-functor 
  (syntax-rules ()
    ((_ (nm s . a) . code)
     (begin
       (define (nm s . a)
         (syntax-parameterize ((S (identifier-syntax s))) . code))
       (attach-defined-module! nm)
       (set-object-property! nm 'prolog-functor-type #:scm)
       (set-object-property! nm 'prolog-functor-stx  #'nm)))

    ((_ nm code)
     (begin
       (define nm code)
       (attach-defined-module! nm)
       (set-object-property! nm 'prolog-functor-type #:scm)
       (set-object-property! nm 'prolog-functor-stx  #'nm)))))

(define* (attach-defined-module! f #:optional (mod (current-module)))
  (set-procedure-property! f 'module (module-name mod)))

(define*  (get-attached-module f #:optional (not-pretty? #t))
  (define (st l) (map symbol->string l))
  (define mod (procedure-property f 'module))

  (if (and (not not-pretty?) 
           (symbol? (procedure-name f))
	   (if mod
	       (eq? (resolve-module mod) (current-module))
	       (module-defined?
		(current-module) (procedure-name f))))
      '()
      (let ((x (procedure-property f 'module)))
        (if x
            (if not-pretty?
                (st x)
                (let ((r (module-name (current-module))))
                  (if (equal? r x)
                      '()
                      (st x))))

            (if not-pretty?
                (st (module-name (current-module)))
                '())))))

(define bind-operator-to-functor
  (case-lambda
    ((f op) (bind-operator-to-functor f op 'xfy))
    ((f op type)
     (set-procedure-property! f 'prolog-operator op)
     (set-procedure-property! f 'prolog-operator-type type))))

(define-inlinable (get-operator-from-f f)
  (procedure-property f 'prolog-operator))
