(define-module (logic guile-log prolog persist)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log persistance)
  #:export (new_persister persist_ref persist_set
			  load_persists save_persists
			  persist-state
			  persist-restate))


(<define*> (new_persister ret #:key (file "persist.scm"))
   (<let> ((p (make-persister #:file file)))
     (<code> (set-procedure-property! p 'name 'persister))
     (<=> ret p)))

(<define> (persist_ref p i out)
  (<let> ((i (<lookup> i)))
    (if (procedure? i)
	(<code> (set! i (procedure-name i)))
	<cc>)
    (<=> ,(persist-ref (<lookup> p) i) out)))

(<define> (persist_set p i in)
  (<let> ((i (<lookup> i)))
    (if (procedure? i)
	(<code> (set! i (procedure-name i)))
	<cc>)
    (<code> (persist-set! (<lookup> p) (<lookup> i) in))))

(<define> (load_persists p)
  (<code> (load-persists (<lookup> p))))

(<define> (save_persists p)
  (<code> (save-persists (<lookup> p))))

(define (persist-state per key)
  (let ((state (<state-ref>)))
    (<clear>)
    (persist-set! per key state)
    (<state-set!> state)))

(define (persist-restate per key)
  (<clear>)
  (<state-set!> (persist-ref per key)))
