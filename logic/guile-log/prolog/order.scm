(define-module (logic guile-log prolog order)
  #:use-module (logic guile-log) 
  #:use-module (logic guile-log umatch) 
  #:export (term< term> atom?))

(define-syntax-rule (variable< x y)
  (<when> (< (gp-address (<lookup> x) S) (gp-address (<lookup> y) S))))

(define-syntax-rule (<var?> x)
  (gp-var? x S))

(define symbol< (lambda (x y) (string<? (symbol->string x) (symbol->string y))))

(define (functor? x)
  (and (procedure? x) (or (object-property x 'prolog-functor)
                          (procedure-name x))))

(define (get-nm x)
  (let ((nm (object-property x 'prolog-functor)))
    (if nm
        nm
        (procedure-name x))))

(define (functor< x y)  
  (let ((nm-x (get-nm x))
	(nm-y (get-nm y)))
    (if (eq? nm-x nm-y)
	(< (object-address x) (object-address y))
	(symbol< nm-x nm-y))))

(define (else? x) #f)
(define (else< x y) (< (object-address x) (object-address y)))

(define orders
  `((,symbol?    .  ,symbol<)
    (,string?    .  ,string<?)
    (,char?      .  ,char<?)
    (,functor?   .  ,functor<)
    (,procedure? .  ,else<)
    (,else?      .  ,else<)))

(define atom= equal?)

(define (atom< x-in y-in)
  (let lp ((l orders) (r '()))
    (if (pair? l)
        (let ()
          (define x  (car l))
          (define x? (car x))
          (define x< (cdr x))
          (define or-f (lambda (f) (f y-in)))
          (if (x? x-in)
              (cond
               ((x? y-in)
                (x< x-in y-in))
               ((or-map or-f r)
                #f)
               (else
                #t))
              (lp (cdr l) (cons x? r))))
        (error "Wrong atom< logic in order.scm"))))
        

(define (atom? x)
  (or-map (lambda (pred) ((car pred) x)) orders))

(define tag (gensym "order-tag"))
(define (tag? x) (and (pair? x) (eq? (car x) tag)))

(define (term0< s p cc x y)
  (define i 0)
  (define fail-all p)
  (<define> (compound? y)
     (<match> (#:mode - #:name compound?) (y)
      (#((_ . _)) (<cut>  <cc>))
      ((_ . _)    (<cut>  <cc>))
      (_          (<cut> <fail>))))


  (<define> (get-arity0 x n)
    (<match> (#:mode - #:name get-arity0) (x)
      ((_ . l) (<cut> (get-arity0 l (+ n 1))))
      (()      (<cut> (<cc> n)))
      (x       (<cut> (<cc> (+ n 1))))))

  (<def-> (get-arity x)
    (#((f . l)) (get-arity0 l 0)))

  (<define> (compound< x y)
    (<match> (#:mode - #:name compound<) (x y)
      ((_ . _) (_ . _)
       (<cut> (args< x y)))
      ((_ . _) _
       (<cut> (<cc> 1)))
      (_ (_ . _)
        (<cut> (<fail> fail-all)))
      (_ _
       (<cut>
        (<and>
        (<values> (arity-x) (get-arity x))
        (<values> (arity-y) (get-arity y)) 
        (cond
         ((< arity-x arity-y) 
          (<cc> 1))
         ((= arity-x arity-y)
          (args< x y))
         (else (<fail> fail-all))))))))


  (<define> (args< x y)
    (<match> (#:mode - #:name args<) (x y)
      (#(lx) #(ly)
       (<cut> (args< lx ly)))

      (#(_)  _
       (<cut> (<cc> 1)))

      (_    #(_)
       (<cut> (<fail> fail-all)))

      ((x . lx) (y . ly)
       (<cut>
        (<and>
         (<values> (i) (term00< x y))
         (if (= i 0)
             (args< lx ly)
             (<cc> 1)))))

      (() (_ . _)
       (<cut> (<cc> 1)))

      (_ _
       (<cut> <fail>))))

  (<define> (term00< x y)
    (<let> ((x (<lookup> x)) (y (<lookup> y)))
      (cond 
       ((tag? x)
        (if (tag? y)
            (< (cdr x) (cdr y))
            (<cc> 1)))
       ((tag? y)
        (<fail> fail-all))

       ((attvar? x)
        (if (attvar? y)
            (if (eq? x y)
		(<and>
		 (<set> x (cons tag i))
		 (<code> (set! i (+ i 1)))
		 (<cc> 0))
		(<or>
		 (<and>
		  (<set> x y)
		  (<set> y (cons tag i))
		  (<code> (set! i (+ i 1)))
		  (<cc> 0))
		 (if (< (object-address x)
			(object-address y))
		     (<cc> 1)
		     (<fail> fail-all))))
            (<cc> 1)))

       ((and (number? x) (inexact? x))
        (cond 
         ((attvar? y) 
          (<fail> 'fail-all))
         ((and (number? y) (inexact? y))
          (cond
           ((< x y) 
            (<cc> 1))
           ((= x y)
            (<cc> 0))
           (else
            (<fail> fail-all))))
         (else
          (<cc> 1))))

       ((and (number? x) (integer? x))
        (cond 
         ((or (attvar? y) (and (number? y) (inexact? y)))
          (<fail> fail-all))
         ((integer? y)
          (cond
           ((< x y)
            (<cc> 1))
           ((= x y)
            (<cc> 0))
           (else
            (<fail> fail-all))))
         (else 
          (<cc> 1))))

       ((atom? x) 
        (cond
         ((or (attvar? y) (number? y))
          (<fail> fail-all))
         ((atom? y)
          (cond
           ((atom= x y)
            (<cc> 0))
           ((atom< x y)
            (<cc> 1))
           (else
            (<fail> fail-all))))
         (else
          (<cc> 1))))

       (else
        (<if> (compound? x)
              (<if> (compound? y)
                    (compound< x y)
                    (<fail> fail-all))
              (<code> (error "BUG could not find a comparison for ~a ~a"
                             x y)))))))
  ((</.> (<values> (i) (term00< x y))
         (if (= i 0)
             <fail>
             <cc>))
   s p cc))
        

(<define> (term< x y)
  (<let> ((x (<lookup> x)) (y (<lookup> y)))
    (if (and (number? x) (number? y))
	(when (< x y))
	(if (let* ((fr (gp-newframe S))
		   (ret (term0< S (lambda () #f) (lambda x #t) x y)))
	      (gp-unwind-tail fr)
	      ret)
	    <cc>
	    <fail>))))

(<define-guile-log-rule> (term> x y) (term< y x))
