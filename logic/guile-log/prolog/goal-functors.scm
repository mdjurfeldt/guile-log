(define-module (logic guile-log prolog goal-functors)
  #:use-module ((logic guile-log) #:select
		(CUT SCUT <if> <define> <match> <<match>> <lambda>
		     <let> <apply> <cut> <fail> S <values> <scm>
		     <and> <with-cut> <cc> <pp> <lookup> <var?> <pp> <cut>
		     <code> </.> procedure-name
                     define-guile-log))
  #:use-module (logic guile-log guile-prolog closure)
  #:use-module (logic guile-log umatch) 
  #:use-module (ice-9 pretty-print)
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log prolog names)
  #:export (goal-eval goal-eval* scm-eval if-then-else fff-eval
		      goal-fkn? get-goal-sym  op2:))

(define do-print #f)
(define pp
  (case-lambda
   ((s x)
    (when do-print
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when do-print
      (pretty-print (syntax->datum x)))
    x)))
(define ppp
  (case-lambda
   ((s x)
    (when #t
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when #t
      (pretty-print (syntax->datum x)))
    x)))

(define (get-goal-sym f)
  (syntax->datum (object-property f 'prolog-functor-stx)))

(define *expr* (make-fluid #f))

(define-guile-log goal-eval
  (syntax-rules ()
    ((_ (cut a b c) x)
     (goal-eval* a b c cut SCUT x))))

(define (goal-fkn? f)
  (object-property f 'prolog-functor-type))

(define op2: #f)
(define get-module #f)
(define namespace-switch #f)

(<define> (get-vars a)
  (<<match>> (#:mode -) (a)
    ((x . y)
     (<and>
      (<values> (xx) (get-vars x))
      (<values> (yy) (get-vars y))
      (<cc> (cons xx yy))))
    
    (#(("," x y))
     (<cc> (cons x y)))
    
    (z (<cc> z))))
     

(<define> (subst code vars)
  (<<match>> (#:mode -) (code)
    (#(("op2*" (and a #((f . l))) code))
     (<and>
      (<values> (vs) (get-vars (<scm> l)))
      (let ((va (map car vs))
            (vb (map cdr vs)))
        (<values> (c)
                  (subst code
                         (append (cons*
                                  (cons f #f)
                                  (map (lambda (x) (cons (car x) #f)) vs))
                                 vars)))
        (<values> (l) (subst vb vars))
        (<cc> (lambda (x) (vector (list "op2*"
                                        (map (lambda (v w) (cons v (w x)))
                                             va vb)
                                        (c x))))))))
          
    (#(a)
     (<and>
      (<values> (aa) (subst a vars))
      (<cc> (lambda (x) (vector (aa x))))))
   
    (#(a b)
     (<and>
      (<values> (aa) (subst a vars))
      (<values> (bb) (subst b vars))
      (<cc> (lambda (x) (vector (aa x) (bb x))))))
   
    ((a . b)
     (<and>
      (<values> (aa) (subst a vars))
      (<values> (bb) (subst b vars))
      (<cc> (lambda (x) (cons (aa x) (bb x))))))
   
    (a
     (<cc>
      (lambda (x)
        (let ((r (assq a vars)))
          (if r
              (let ((r (cdr r)))
                (if r
                    (if (number? r)
                        (list-ref x r)
                        r)
                    a))
              a)))))))

           
(<define> (goal-eval* cut scut x)
   (<<match>> (#:mode - #:name goal-eval) ((pp  'goal-eval x))
    (#((,op2: mod #((n . l))))
      (let ((mod (get-module (procedure-name (<lookup> mod))))
            (n   (<lookup> n)))
	(namespace-switch mod
	  (</.>
	   (goal-eval* cut scut
		       (vector (cons (module-ref mod 
						 (procedure-name n))
				     l)))))))

    (#(("op2*" #((f . val)) code))
     (<and>
      (<values> (vars.vals)  (get-vars (<scm> val)))
      (let* ((vars  (map car vars.vals))
             (vals  (map cdr vars.vals))
             (vvars (let lp ((i 0) (vv vars))
                      (if (pair? vv)
                          (cons (cons (car vv) i)
                                (lp (+ i 1) (cdr vv)))
                          '())))
             (code2 #f)
             (lp    (<lambda> x
                      (goal-eval* cut scut (code2 x)))))
        (<values> (code3) (subst code (cons (cons (<scm> f) lp) vvars)))
        (<code> (set! code2 code3))
        (<apply> lp vals))))
         
    (#((f . l))
     (<let> ((f (<lookup> f)))
       (<let> ((x (object-property f 'prolog-functor-type)))
	 (case x
	   ((#:goal)	   
	    (<apply> f cut scut l))
	   ((#f)
	    (if (and (struct? f) (prolog-closure? f))
		(<apply> (prolog-closure-closure f) l)
		(<apply> f l)))))))

    ((x) 
     (goal-eval* cut scut x))

    (x
     (<let> ((f (<lookup> x)))         
       (cond 
        ((<var?> f)
         (instantiation_error))

        ((procedure? f)
         (<cut>
          (<let> ((x (object-property f 'prolog-functor-type)))
	    (case x
	      ((#:goal)
	       (f cut scut))
	      (else
	       (f))))))

        ((and (struct? f) (prolog-closure? f))
         ((prolog-closure-closure f)))

        ((eq? f '!)
         (<with-cut> cut scut (<and> <cut>)))

        (else
         (type_error callable (gp-var-ref *call-expression*))))))))


(define-syntax-rule (define-exp-functor (nm s a) . code)
  (begin
    (define (nm s a)
    (set-object-propery nm 'prolog-functor-type #:exp))))

(define (scm-brace-eval s x) x)

(define-syntax-rule (scm-eval x) (scm-eval* S x))
(define (scm-eval* s x)
  (umatch (#:mode - #:status s #:name scm-eval*) ((pp 'scm-eval x))
    (#((f . l))
     (let* ((f (gp-lookup f s))
            (v (object-property f 'prolog-functor-type)))
       (cond 
        ((eq? f float)
         (apply exact->inexact (map (lambda (a) (scm-eval* s a)) l)))      
        ((eq? v #:scm)
         (apply f s (map (lambda (a) (scm-eval* s a)) l)))
        (else         
         (apply f (map (lambda (a) (scm-eval* s a)) l))))))
    (#(#:brace _)
     (scm-brace-eval s x))
    (x
     (let ((x (gp-lookup x s)))
       (cond
        ((or (string? x) (procedure? x) (char? x))
         (type_error s (lambda x 1) (lambda x 2)
                     evaluable (vector (list divide  x 0))))
        
        ((gp-var? x s)
         x #;(instantiation_error s (lambda x 1) (lambda x 2)))
        
        (else
         x))))))

(define (fff-eval x) x)
  



    



