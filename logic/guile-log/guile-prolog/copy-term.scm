(define-module (logic guile-log guile-prolog copy-term)
  #:use-module ((logic guile-log umatch) 
		#:select (gp-var? gp-attvar-raw? gp-make-var
				  gp-att-data
				  gp-make-attribute-from-data))		

  #:use-module (logic guile-log macros)
  #:use-module (logic guile-log umatch)
  #:replace (copy_term)
  #:export (duplicate_term 
	    copy-term-2 copy-term-3
	    duplicate-term-2 duplicate-term-3))

(define fail (lambda (x) x))

(define-guile-log check
  (syntax-rules ()
    ((check w (h x y) code ...)
     (let ((r (hashq-ref h x #f)))
       (cond
	((eq? r #t)
	 (let ((r (make-variable 12))
	       (y 13))
	   (hashq-set! h x r)
	   (<with-guile-log> w (<cc> r))))
	((not r)
	 (<with-guile-log> w
           (if (gp-var? x S)
	       (<let> ((r (gp-make-var)))
		   (<code> (hashq-set! h x r))
		   (<cc> r))
	       (<let> ((y #f))
		 (<code> (hashq-set! h x #t))
		 (<and> code ...)
		 (<let> ((e (hashq-ref h x #f)))
		   (if (variable? e)
		       (<and>
			(<code> (variable-set! e y))
			(<cc> e))
		       (<and>
			(<code> (hashq-set! h x #f))
			(<cc> y))))))))

	(else
	 (<with-guile-log> w (<cc> r))))))

    ((check w (h x y z) code ...)
     (let ((r (hashq-ref h x #f)))
       (cond
	((eq? r #t)
	 (let ((r (make-variable 14)))
	   (hashq-set! h x r)
	   (<with-guile-log> w (<cc> r '()))))
	((not r)
	 (<with-guile-log> w
           (if (gp-var? x S)
	       (<let> ((r (gp-make-var)))
		   (<code> (hashq-set! h x r))
		   (<cc> r '()))
	       (<let> ((y #f)
		       (z #f))
		 (<code> (hashq-set! h x #t))
		 (<and> code ...)
		 (<let> ((e (hashq-ref h x #f)))
		   (if (variable? e)
		       (<and>
			(<code> (variable-set! e y))
			(<cc> e z))
		       (if (gp-attvar? x S)
			   (<and>
			    (<code> (hashq-set! h x y))
			    (<cc> y z))
			   (<and>
			    (<code> (hashq-set! h x #f))
			    (<cc> y z)))))))))

	(else
	 (<with-guile-log> w (<cc> r '()))))))))

(define (mk s f)
  (define (lam xx)
    (f s (lambda () #f) (lambda (s p x) x) xx))
  lam)

(define (mk3 s f)
  (define (lam xx)
    (f s (lambda () #f) (lambda (s p x l) (cons x l)) xx))
  lam)

(define-syntax-rule (mk-copy-term-2 copy-term-2 reuse?)
(<define> (copy-term-2 x)
  (<let> ((h (make-hash-table)))
    (<recur> lp ((x x))
      (<let*> ((x (<lookup> x)))
        (cond
          ((gp-attvar-raw? x S)
	   (check (h x y)
              (let* ((w (gp-make-var)))
                (let lp2 ((data (gp-att-data x S)))
                  (if (pair? data)
                      (let ((id (caar data)))
                        (if (not (attribute-cstor-ref id))
                            (<or>
                             (<and>
                              (<values> (e) (id (cdar data)
                                                (mk S lp) "cp"))
                              <cut>
                              (<code> (gp-put-attr! w id e S))
                              (lp2 (cdr data)))
                             (<and>
                              (<values> (e) (lp (cdar data)))
                              (<code> (gp-put-attr! w id (<lookup> e) S))			   
                              (lp2 (cdr data))))
                            (lp2 (cdr data))))                    
                      (<code> (set! y w)))))))
          
	  ((gp-var? x S)
	   (check (h x y)))

	  (else
	   (<<match>> (#:mode - #:name copy-term-2) (x)
	    ((a . b)
	     (check (h x y)
	       (<values> (aa) (lp a))
	       (<values> (bb) (lp b))
	       (if (and reuse? (pair? x) (eqv? a aa) (eqv? b bb))
		   (<code> (set! y x))
		   (<let> ((ret (cons aa bb)))
                     (<code> (set! y ret))))))
	    
	    (#(a)
	     (check (h x y)
	       (<values> (aa) (lp a))    
	       (if (and reuse? (eqv? a aa))
		    (<code> (set! y x))
		    (<let> ((ret (vector aa)))
		      (<code> (set! y ret))))))

	    (#(a b)
	     (check (h x y)
	       (<values> (aa) (lp a))
	       (<values> (bb) (lp b))
	       (if (and reuse? (eqv? a aa) (eqv? b bb))
		   (<code> (set! y x))
		   (<let> ((ret (vector aa bb)))
		     (<code> (set! y ret))))))

	    (x (<cc> x))))))))))

(mk-copy-term-2 copy-term-2      #t)
(mk-copy-term-2 duplicate-term-2 #f)

(define the-tag (cons 'the 'tag))

(define-syntax-rule (mk-copy-term-3 copy-term-3- copy-term-3 reuse?)
(begin
  (<define> (copy-term-3- h x)
   (<let> ()
    (<recur> lp ((x x))
      (<let*> ((x (<lookup> x)))
        (cond
          ((gp-attvar-raw? x S)
	   (check (h x y z)
	     (<var> (repr)
	       (build_attribut_representation repr '() x)
	       (<let> ((ret (gp-make-var)))
		 (<code> (set! y ret))
		 (<code> (set! z (<scm> repr)))))))

          ((gp-attvar-raw? x S)
	   (check (h x y z)
              (let* ((w (gp-make-var)))
                (let lp2 ((data (gp-att-data x S)) (l '()))
                  (if (pair? data)
                      (let ((id (caar data)))
                        (if (not (attribute-cstor-ref id))
                            (<or>
                             (<and>
                              (<values> (e ll) (id (cdar data)
                                                (mk3 S lp) "cp3"))
                              <cut>
                              (<code> (gp-put-attr! w id e S))
                              (lp2 (cdr data) (append ll l)))
                             (<and>
                              (<values> (e ll) (lp (car data)))
                              (<code> (gp-put-attr! w id e S))
                              (lp2 (cdr data) (append ll l))))
                            (lp2 (cdr data) l)))
                      (<and>
                       (<code> (set! y w))
                       (<code> (set! z l))))))))

	  ((gp-var? x S)
	   (check (h x y z)))

	  (else
	   (<<match>> (#:mode - #:name copy-term-2) (x)
	    ((a . b)
	     (check (h x y z)
	       (<values> (aa la) (lp a))
	       (<values> (bb lb) (lp b))
	       (if (and reuse? (eqv? a aa) (eqv? b bb))
		   (<and>
		    (<code> (set! y x))
		    (<code> (set! z '())))
		   (<let> ((ret (cons aa bb)))
                     (<code> (set! y ret))
		     (<code> (set! z (append la lb)))))))
                        
	    (#(a)
	     (check (h x y z)
	       (<values> (aa la) (lp a))    
	       (if (and reuse? (eqv? a aa))
		   (<and>
		    (<code> (set! y x))
		    (<code> (set! z '())))
		   (<let> ((ret (vector aa)))
		     (<code> (set! y ret))
		     (<code> (set! z la))))))
	    
	    (#(a b)
	     (check (h x y z)
	       (<values> (aa la) (lp a))
	       (<values> (bb lb) (lp b))
	       (if (and reuse? (eqv? a aa) (eqv? b bb))
		   (<and>
		    (<code> (set! y x))
		    (<code> (set! z '())))
		   (<let> ((ret (vector aa bb)))
		     (<code> (set! y ret))
		     (<code> (set! z (append la lb)))))))
            
	    (x (<cc> x '())))))))))

  (<define> (copy-term-3 p)
    (<let> ((h (make-hash-table)))
      (<values> (pp lq) (copy-term-3- h p))
      (<recur> lp ((p lq) (l '()))
	(<values> (q lq) (copy-term-3- h p))
	(<let> ((lq (<lookup> lq))
		(l  (<lookup> l )))
	  (if (null? lq)
	      (<cc> pp (append q l))
	      (lp lq (append q l)))))))))

(mk-copy-term-3 copy-term-3-      copy-term-3 #t)
(mk-copy-term-3 duplicate-term-3- duplicate-term-3 #f)

(define copy_term	     
  (<case-lambda>
   ((x y)
    (<values> (yy) (copy-term-2 x))
    (<=> y yy))
   
   ((x y z) 
    (<let> ((s  S)
	    (fr (<newframe>)))
	(<values> (yy zz) (copy-term-3 x)) 
	(<values> (ww)    (copy-term-2 (cons yy zz)))
	(<code> (<unwind-tail> fr))
	(<with-s> s
	  <cut>
	  (<=> (y . z) ww))))))


(define duplicate_term	     
  (<case-lambda>
   ((x y)   (<values> (yy) (duplicate-term-2 x)) (<=> y yy))
   ((x y z) 
    (<let> ((s  S)
	    (fr (<newframe>)))
	(<values> (yy zz) (duplicate-term-3 x)) 
	(<values> (ww)    (copy-term-2 (cons yy zz)))
	(<code> (<unwind-tail> fr))
	(<with-s> s
	   <cut>
	   (<=> (y . z) ww))))))


(define (cp x s) (copy-term-2 s (lambda () #f) (lambda (s p x) x) x))
