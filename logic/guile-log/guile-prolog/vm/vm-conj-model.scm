#|
In guile prolog we take an interesting short-cut in
  a,b,c,fail  

We could deside it is all a fail, but a may print out info and there is a 
side effect. But still it would be a dramatic simplification to kill the whole
expression. We actually do that the case
  a,b,c,1=2.
would simplify totally the idea is that typically this situation is what
you get after compiling a macro and there the default should be simplify
as much as possible. We don't loose anything because you can always insert
a direct fail. But the gain can be dramatic and it's nice to have the compiler
do the heavy lifting.
|#

(compile-prolog-string
"
collect_conj(X,[X|LL],LL) :- var(X),!.
collect_conj((X,Y),L,LL) :- !,
   collect_conj(X,L ,L1),
   collect_conj(Y,L1,LL).
collect_conj(X,[X|LL],LL).
  
-extended.
compile_conj0([],Tail,V,L)  :- throw(#t).

compile_conj0([X],Tail,V,L) :- !,
     compile_goal(X,Tail,V,L).

compile_conj0([X|Gs],Tail,V,L) :- var(X),!,
  compile_conj0([call(X)|Gs],Tail,V,L).

compile_conj0([!|Gs],Tail,V,[L,LL]) :- !,
     get_C(V,[[_,#t]|_]),
     ifc(compile_conj0(Gs,Tail,V,[L1,LL]),E,
        (
          tfc(E),
          (
            E==#t -> 
              (Tail==#t -> L=[[cut],[cc]|LL] ; L=[[cut]|LL]);
            throw(c)
          )
        ),
        (
           L=[[cut]|L1]
        )).

compile_conj0([softie(A)|Gs],Tail,V,[L,LL]) :- !,
     pop_Q(3,V,Q),
     ifc(compile_conj0(Gs,Tail,V,[L1,LL]),E,
        (
          tfc(E),
          (
            E==#t -> 
              (
                push_Q(3,V,Q),
                (
                  Tail==#t ->               
                     L=[[softie,A],[cc]|LL] ;
                   L=[[softie,A]|LL]
                )
              );
            E==c  ->
              throw(c);
            throw(softie(A))
          )
        ),
        (
          L=[[softie,A]|L1],
          push_Q(3.2,V,Q)
        )).

compile_conj0([(fail;false)|Gs],Tail,V,[L,LL]) :- !,
   L=[[fail]|LL].
 
compile_conj0([G|Gs],Tail,V,L) :- !,
  link_l(L,L1,L2),
  ifc(compile_goal(G,#f,V,L1),E,
  (
     tfc(E),
     (
       E==#t -> 
         compile_conj0(Gs,Tail,V,L) ;
         throw(E)
     )
  ),
  (
     catch(compile_conj0(Gs,Tail,V,L2),E2,
       (
          tfc(E2),
          (
            E2==#t -> 
              (Tail==#t -> L2=[[[cc]|U],U] ; L2=[U,U]);
            E2==c  ->
              L2=[[[cut],[fail]|U],U] ;
            E2=softie(A) ->
              L2=[[[softie,A],[fail]|U],U] ;
            throw(E2)
          )
       ))
  )).
  
compile_conj(Gs,Tail,V,L) :-
   compile_conj0(Gs,Tail,V,L).
")
