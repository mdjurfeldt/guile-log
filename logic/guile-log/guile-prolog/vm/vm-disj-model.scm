(<define> (mksym x) (<=> x (gensym "tag")))

(compile-prolog-string "
cat(F,G) :-
  catch(F,Er,
    (
      tfc(Er),
      (
         Er==#t -> throw(bug_should_not_throw_true) ;
         Er==#f -> throw(bug_should_not_throw_cut)  ; 
         G
      )
    )).

collect_disj([],U,U).

collect_disj(['*->'(A,B)|L],U,UU) :-
 U=['soft-if-f'(A,B,';'(|L))|UU].

collect_disj(['-i>'(A,B)|L],U,UU) :-
 U=['interleaving-if-f'(A,B,';'(|L))|UU].

collect_disj([X|L],U,UU) :-
  collect_disjunction(X,U,U1),
  collect_disj(L,U1,UU).

collect_disjunction(X,[X|UU],UU) :- var(X),!.
collect_disjunction(';'(|L),U,UU) :- !,
   collect_disj(L,U,UU).

collect_disjunction(X,[X|UU],UU).

%head_at_true(First,Last,A,C,Lab,Lab2,L1,LLX)
head_at_true(Q,#t,_,A,C,Lab,Lab2,L1,LLX) :- L1=LLX, Lab2=Lab.

head_at_true(Q,#f,#f,A,C,Lab,Lab2,L1,LLX) :-
   (var(Q) -> tr('unwind-light',Unwind) ; tr(unwind,Unwind)),
   (var(Lab) -> true ; throw(error_head)),
   L1=[[label,Lab,U],[Unwind,A,Lab2]|LLX].

head_at_true(Q,#f,#t,A,C,Lab,Lab2,L1,LLX) :-
   (var(Q) -> tr('unwind-light-tail',Unwind) ; tr('unwind-tail',Unwind)),
   (var(Lab) -> true ; throw(error_head)),
   L1=[[label,Lab,U],[Unwind,A]|LLX].

compile_disjunction0
    ([X],First,Aq,Ae,Out,Lab,A,Tail,S0,[U],B2,V,[L,LL]) :- !,   
   (First=#t -> compile_goal(X,First,V,[L,LL]) ;
   catch(( 
     read_Q(0,V,Qit),
     pop_Q(0,V,_),
     get_CES(V,[C|_],E,S),
     set_AB2ES(V,Aq,B2,0,S0),
     ( 
       ((nonvar(X),X=(G1->G2)) -> XX=(once(G1),G2) ; XX=X),
       compile_goal(XX,Tail,V,[LX,LL]),
       get_ACES(V,Aq1,[C1|_],E1,S1),
       (C == C1 -> true ; throw(bug_c_state_error)),
       (
         eql_a(Ae,Aq1) -> true ; 
         throw(all_disjuction_goals_needs_the_same_begin_level)
       ),
       SS is max(S,S1),
       EE is E \\/ E1,
       set_AES(V,Aq,EE,SS),
       U = [E1,_],
       head_at_true(Qit,First,#t,A,C,Lab,Lab2,L,LX)
     )),Er,
     (
       tfc(Er),
       (
         Er == #t -> 
           (
             First==#t -> 
                throw(#t) ;
                (
                  head_at_true(Qit,#f,#t,A,C,Lab,Lab2,L,LL),
                  U=[0,_]
                )
           );
         (
           Er == c -> 
             throw(c) ;
             throw(#f)
         )
       )
     ))).

goal(X,Y,First,Lab,Tail,Out,A,Ae,Aq,S0,[U|UU],V,L,LL,LG,B2,LLX) :-    
     read_Q(1,V,Qit),
     get_CES(V,[C|_],E,S),
     set_AB2ES(V,Aq,B2,0,S0),
     ((nonvar(X),X=(G1->G2)) -> 
        XX=(call(G1),softie(A),collect_F(FF),G2) ; 
       (XX=(X,newtag_F(FF)))),
     compile_goal(XX,Tail,V,[LX,LG]),
     (var(FF) -> true ; set_F(V,FF)),
     get_ACES(V,A1q,[C1|_],E1,S1),
     (C == C1 -> true ; throw(bug_c_state_error)),
     S2 is max(S,S1),
     E2 is E \\/ E1,
     U  = [E1,_],
     set_ES(V,E2,S2),
     (
       First == #t ->
       (
          A1q=Ae
       );
       (
          mksym(Sym),
          set_F(V,Sym),
          (Ae == A1q -> true ;
             throw(all_disjuction_goals_needs_the_same_begin_level))
       )
     ),            
     ifc(compile_disjunction0(Y,#f,Aq,Ae,Out,Lab2,A,Tail,S0,UU,B2,
                               V,[LLX,LL]),Err,
        (
          (
            Err==#t -> 
              throw(bug_true_not_be_send_in_non_first_disjunction);
              (
                 pop_Q(1,V,_),
                 head_at_true(Qit,First,#f,A,C,Lab,Lab2,L,LX),
                 UU=[[0,_]],
                 Err==c ->
                   LLX=[[cut],[fail]|LL];
                   LLX=LL
              )
          )
        ),             
         head_at_true(Qit,First,#f,A,C,Lab,Lab2,L,LX)
        ).

compile_disjunction0([X|Y],First,Aq,Ae,Out, Lab,A,Tail,S0,U,B2,V,[L,LL]) :- !,
   tr('goto-inst',Goto),
   read_Q(2,V,Qit),
   (Tail==#t -> LG=LLX ; LG = [[Goto,Out]|LLX]),
   catch(goal(X,Y,First,Lab,Tail,Out,A,Ae,Aq,S0,U,V,L,LL,LG,B2,LLX), Er,
   (
    tfc(Er),
    (Tail==#t -> LLG = [[cc]|LG] ; LLG=LG),
    (       
       Er==c ->
         throw(c) ;
       Er==#t -> 
       (
         U  = [[0,_]|UU],
         ifc(compile_disjunction0
             (Y,#f,Aq,Aq,Out,Lab2,A,Tail,S0,UU,B2,V,[LLX,LL]),Er2,
           (
             tfc(Er2),
             (
                Er2==#f ->
                (
                   First = #t -> 
                      throw(#t);
                      (
                        UU=[[0,_]], 
                        head_at_true(Qit,#f,#t,A,C,Lab,Lab2,L,LLG),
                        LLX=LL
                      )
                );                
                throw(bug_should_not_throw_true_or_cut)
             )
           ),
           (
            (First==#t -> true ;
              (Aq==Ae -> true ;
                 throw(syntax_error_begin_en_missmatch))),
             head_at_true(Qit,First,#f,A,C,Lab,Lab2,L,LG)
           )));
       compile_disjunction0(Y,First,Aq,Ae,Out,Lab,A,Tail,S0,U,B2,V,[L,LL])
    )
   )).

compile_disjunction(Y,First,Aq,Ae,Out,Lab,A,Tail,S0,U,B2,V,[L,LL]) :-
  catch(compile_disjunction0(Y,First,Aq,Ae,Out,Lab,A,Tail,S0,U,B2,V,[L,LL]),Er,
    (
        tfc(Er),
        (
          Er=softie(_) -> throw(#f) ; throw(Er)
        )
    )).
")
