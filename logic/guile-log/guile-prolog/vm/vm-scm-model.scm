(compile-prolog-string 
"
-extended.
compile_scm(X,V,L,LL) :-
  var_p(X)    -> 
   ( !,
     add_var(X,V,Tag),tr('push-variable-scm',Push),
     push_v(1,V),
     L=[[Push,Tag]|LL]
   );
  constant(X)    -> (!,tr('push-constant',Atom),      L=[[Atom  ,XX]|LL], 
                     E=X,regconst(X,XX), push_v(1,V));
  instruction(X) -> (!,tr('push-instruction',Atomic), L=[[Atomic,X]|LL], 
                     E=X,push_v(1,V)).

compile_scm((Op, ';'(max,min,+,-,*,/,<<,>>,\\/,/\\,mod))
                  (X,Y),V,L,LL) :- !,
  ifc(compile_scm(X,V,L,LX),EX,
      (
        (number(EX) -> true ; throw(EY)),
        ifc(compile_scm(Y,V,L,LY),EY,
            (
              (number(EY) -> true ; throw(EY)),
              call(E is Op(EX,EY)),
              throw(E)
            ),
            (
              binop1L(Op,O),
              LY=[[O,EX]|LL]
            ))
      ),
      ifc(compile_scm(Y,V,LX,LY),EY,
      (
         (number(EY) -> true ; throw(EY)),
         binop1R(Op,O),
         LX=[[O,EY]|LL]
      ),
      (
         push_v(-1,V),
         tr(Op,O),
         LY=[[Op]|LL]
      ))).

compile_scm((Op,(+ ; - ; \\))(X),V,L,LL) :- !,
  ifc(compile_scm(X,V,L,LX),EX,
     (
         (number(EX) -> true ; throw(EX)),
         call(E is Op(EX)),
         throw(E)
     ),
     (
         Op=='+' -> LX=LL ;
         (
           (
             Op=='-' -> unop('op1_-',O) ;
             unop(Op,O)
           ),
          LX=[[O]|LL]
        )
    )).
")
