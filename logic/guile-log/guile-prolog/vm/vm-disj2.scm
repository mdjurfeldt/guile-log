(define-module (logic guile-log guile-prolog vm vm-disj2)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log guile-prolog hash)
  #:use-module (logic guile-log guile-prolog ops)
  #:use-module (logic guile-log prolog swi)
  #:use-module (compat racket misc)
  #:use-module (system vm assembler)
  #:use-module (logic guile-log soft-cut)
  #:use-module (logic guile-log guile-prolog macros)
  #:use-module ((logic guile-log guile-prolog vm-compiler)
		#:select ())
  #:use-module (logic guile-log guile-prolog vm vm-pre)
  #:use-module (logic guile-log guile-prolog vm vm-var2)
  #:export (compile_disj compile_disjunction collect_disj))

#;
(eval-when (compile)
(prolog-run-rewind 1 (x) 
		   (dyntrace (@@ (logic guile-log guile-prolog vm vm-goal) 
				 compile_goal))))

(eval-when (compile)
  (set! (@@ (logic guile-log prolog compile) include-meta) #f))

(compile-prolog-string
"
- eval_when(compile).
the_tr2(X,[X]).
:- add_term_expansion_temp(the_tr2).
")

(include-from-path "logic/guile-log/guile-prolog/vm/vm-disj-model.scm")

(eval-when (compile)
  (set! (@@ (logic guile-log prolog compile) include-meta) #t))
