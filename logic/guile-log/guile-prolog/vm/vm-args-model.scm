(compile-prolog-string "'with-cut'. 'without-cut'.")

#|
By setting a procedure as 'with-cut we can pass under the radar
|#
(<define> (argkind f k)
  (let ((f (<lookup> f)))
    (if (procedure? f)
	(case (procedure-property (<lookup> f) 'argkind)
	  ((with-cut)
	   (<=> k with_cut))
	  ((without-cut)
	   (<=> k without_cut))
	  (else
	   (<=> k #f)))
	(<=> k #f))))

(<define> (gen_f x) (<=> x ,(gensym "F")))
(compile-prolog-string "
narg(X,N,N) :- var_p(X),!.
narg([X|L],I,N) :-
  II is I + 1,
  narg(L,II,N).
narg(_,I,I).

push_code_with_cut(X,Label,V,L,LL) :-
  L=[[label,Label],['clean-sp']|LX],
  compile_goal(X,#t,V,[LX,LL]).

push_code_without_cut(X,Label,V,L,LL) :-
  L=[[label,Label],['clean-sp']|LX],
  compile_goal(call(X),#t,V,[LX,LL]).

push_args_args(K,X,V,L,LL,_,_) :- var_p(X),!,K==#f,
  push_args(X,V,L,LL).

push_args_args(#f,[X|Y],V,L,LL,U,U) :- !,
  push_args(X,V,L,L1),
  push_args_args(#f,Y,V,L1,LL,U,U).

push_args_args(with_cut,[X|Y],V,L,LL,LW,LLW) :- !,
  L=[['push-closure',Label]|L1],
  push_code_with_cut(X,Label,V,LW,LLW),
  push_args_args(with_cut,Y,V,L1,LL).

push_args_args(without_cut,[X|Y],V,L,LL,LW,LLW) :- !,
  L=[['push-closure',Label]|L1],
  push_code_without_cut(X,Label,V,L,L1),
  push_args_args(without_cut,Y,V,L1,LL).

push_args_args(_,[],V,L,L,LW,LW) :- !.

push_args_args2(X,V,L,LL) :- var_p(X),!,
  push_args(X,V,L,LL).

push_args_args2([X|Y],V,L,LL) :- !,
  push_args(X,V,L,L1),
  push_args_args2(Y,V,L1,LL).

push_args_args2([],V,L,LL) :-
  push_args([],V,L,LL).

push_args(X,V,L,LL) :- var_p(X),!,
  add_var(X,V,Tag),
  push_v(1,V),
  tr('push-variable',Push),
  L=[[Push,Tag]|LL].

push_args([X|Y],V,L,LL) :- !,
  tr('mk-cons',Cons),
  push_args(X,V,L,L1),
  push_args(Y,V,L1,L2),
  push_v(-1,V),
  L2=[[Cons]|LL].

push_args(X(|Y),V,L,LL) :- !,
  tr('mk-fkn',Fkn),
  narg(Y,0,NN),N is NN + 1,
  push_args_args2([X|Y],V,L,L1),
  M is -N, push_v(M,V),
  L1=[[Fkn,N]|LL].

push_args({X},V,L,LL) :- !,
  tr('mk-curly',MK),
  push_args(X,V,L,L1),
  L1=[[MK]|LL].

push_args(X,V,L,LL) :-
  push_v(1,V),
  (
    constant(X) -> 
     (tr('push-constant',Push),regconst(X,XX),L=[[Push,XX]|LL]) ; 
    (tr('push-instruction',Push),L=[[Push,X]|LL])
  ).

get_post(S,C,Cplx,Tail,X,XX) :-
  Tail==#t -> X=XX ;
  (
    (S==0,C==0) ->
       (
         Cplx==#f ->
           (
             X=[[pop,3]|XX]
           ) ;
          X=XX
       );
    Cplx==#f ->
       (
          tr('post-call',Post),
          X=[[Post,C,#t]|XX]
       );
    (
       tr('post-call',Post),
       X=[[Post,C,#f]|XX]
    )
  ).



caller(cc,Args,label(G,N),V,[L,LL]) :- !,
  touch_Q(1,V),
  narg(Args,0,MM),
  M is MM + 3,
  (M==N -> true ; throw(cc_does_not_match_caller)),
  tr('clear-sp' , Clear),
  L=[[Clear]|L2],
  get_S(V,S),
  set_S(V,0),
  push_v(2,V),
  L2=[[seek,3]|L4],
  push_args_args(#f,Args,V,L4,LL2,_,_),
  tr('goto-inst', Goto),
  LL2 = [[Goto,G]|LL].

caller(cc,Args,Tail,V,[L,LL]) :- !,
  touch_Q(2,V),
  (Tail=#f -> throw(cc_not_in_tail_context) ; true),
  tr('clear-sp' , Clear),
  L=[[Clear]|L2],
  get_S(V,S),
  set_S(V,0),
  push_args(F,V,L2,L3),
  push_v(2,V),
  L3=[[seek,2]|L4],
  push_args_args(#f,Args,V,L4,LL2,_,_),
  set_FS(V,F,S),
  tr('tail-cc', Call),
  LL2 = [[Call]|LW].

caller(F,Args,Tail,V,[L,LL]) :-
  touch_Q(3,V),
  (get_recur(F,A,N) -> rec(F,A,N,Args,Tail,V,[L,LL]) ;
  (   
  tr('clear-sp' , Clear),
  L=[[Clear]|L2],
  get_CS(V,[C|_],S),
  set_S(V,0),
  push_args(F,V,L2,L3),
  push_v(3,V),
  L3=[[seek,3]|L4],
  argkind(F,K),
  push_args_args(K,Args,V,L4,LL2,LW,LL),
  touch_A(V),
  gen_f(Fsym),
  set_FS(V,Fsym,S),
  (Tail == #t -> 
    (  
      tr('tail-call', Call), 
      LL2 = [[Call]|LW]
    );
   Tail = label(G,N) ->
    (
       tr(goto-inst,Goto),
       tr('call-n',Call),
       LL2 = [[Call,N],[Goto,G]|LW]
    );
   (
      tr('call', Call),
      LL2=[[Call]|LLL], 
      get_post(S,C,#f,Tail,LLL,LW)
   )
  ))).

rec(F,A,N,Args,Tail,V,[L,LL]) :-
  (narg(Args,0,N) -> true ; throw(recur_call_wrong_number_of_arguments(F))),
  tr('clear-sp' , Clear),
  L=[[Clear]|L2],
  get_CS(V,[C|_],S),
  set_S(V,0),
  push_args_args(#f,Args,V,L2,LL2,_,_),
  touch_A(V),
  %set_FS(V,F,S),
  set_S(V,S),
  (
    tr('goto-inst',Goto),
    LL2 = [[Goto,A]|LL]
  ).
")


