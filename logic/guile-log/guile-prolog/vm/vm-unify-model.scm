(compile-prolog-string "
compile_unify(X,Y,V,[L,LL],M) :-
   X==Y   -> (!,throw(#t));
   var_p(X) -> 
   ( 
     !,
     (
        constant(Y)   -> 
          (
            tr('unify-constant-2',Unify),
            regconst(Y,YY),
            add_var(X,V,Tag1),
            (isFirst(Tag1) -> true ; touch_A(V)),
            L = [[Unify,Tag1,YY,M]|LL]
          );
        instruction(Y) ->
          (
            tr('unify-instruction-2',Unify),
            add_var(X,V,Tag1),
            (isFirst(Tag1) -> true ; touch_A(V)),
            L = [[Unify,Tag1,Y,M]|LL]
          );
 
        var_p(Y)    -> 
          ( 
            get_H(V,H),
            tr('unify-2',Unify),
            add_var(X,V,Tag1),
            add_var(Y,V,Tag2),
            ((isFirst(Tag1) ; isFirst(Tag2)) -> true ; touch_A(V)),
            ((isFirst(Tag1),isFirst(Tag2),M=#f) -> throw(#f) ; true),
            L = [[Unify,Tag1,Tag2,M]|LL]
          );
        (
          tr('push-variable',Push),
          L=[[Push,Tag1]|L1],
          add_var(X,V,Tag1),
          (isFirst(Tag1) -> true ; touch_A(V)),
          push_v(1,V),
          compile_imprint(Y,V,L1,LL,M)
        )
      )
   ) ;
   var_p(Y) -> (!,compile_unify(Y,X,V,[L,LL],M)).

compile_unify([X|LX],[Y|LY],V,L,M) :- !,
  link_l(L,L1,L2),
  catch(compile_unify(X,Y,V,L1,M),E,
     (
        tt(E),L1=[Q,Q]
     )),
  compile_unify(LX,LY,V,L2,M).

compile_unify(X(|LX),Y(|LY),V,L,M) :- !,
  link_l(L,L1,L2),
  catch(compile_unify(X,Y,V,L1,M),E,
     (
        tt(E),L1=[Q,Q]
     )),
  compile_unify(LX,LY,V,L2,M).

compile_unify({X},{Y},V,L,M) :- !,
  compile_unify(X,Y,V,L,M).

compile_unify(X,Y,V,[[[False]|LL],LL],M) :-
  throw(#f).
")
