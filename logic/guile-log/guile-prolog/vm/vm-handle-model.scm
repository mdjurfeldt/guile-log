(compile-prolog-string "
%newvars needs to be variables

newv([]).

newv([[newvar,[[V,S],N,F]]|L]) :- !,
   (N==1 -> true ; S=#t).

newv([_|L]) :- newv(L).

not_first([_,_,F|_]) :- F == #t -> false ; true.

handle_all(L,Lout) :-
  newv(L),
  handle_all(L,0,II,Lout,[]).

handle_all([],I,I,L,L).

handle_all([['push-variable-scm',A],
            ['push-variable-scm',B],
            [CMP]|Y]
               ,I,II,L,LL) :- 
  binss(CMP,_),
  !,
  handle([comp,CMP,A,B],I,I1,L,L1),
  handle_all(Y,I1,II,L1,LL).

handle_all([['push-variable-scm',A],
            ['push-variable-scm',B],
            [Op],
            [unify,C,#t]|Y]
               ,I,II,L,LL) :- 
  binss2(Op,_),!,
  handle([bin,Op,A,B,C],I,I1,L,L1),
  handle_all(Y,I1,II,L1,LL).

handle_all([['push-instruction',A],
            ['push-variable-scm',B],
            [Op],
            [unify,C,#t]|Y]
               ,I,II,L,LL) :- !,
  binis2(Op,_),!,
  handle([ibin,Op,A,B,C],I,I1,L,L1),
  handle_all(Y,I1,II,L1,LL).

handle_all([['push-variable-scm',B],
            ['push-instruction',A],
            [Op],
            [unify,C,#t]|Y]
               ,I,II,L,LL) :- 
  binsi2(Op,_),
  !,
  handle([bini,Op,A,B,C],I,I1,L,L1),
  handle_all(Y,I1,II,L1,LL).

handle_all([['push-variable',A],
            ['push-variable',B],
            ['push-variable',C]|Y], I,II,L,LL) :-
  not_first(A),
  not_first(B),
  not_first(C),
  !,
  handle(['push-3variables',A,B,C],I,I1,L,L1),
  handle_all(Y,I1,II,L1,LL).

handle_all([['push-variable',A],
            ['push-variable',B]|Y], I,II,L,LL) :-
  not_first(A),
  not_first(B),
  !,
  handle(['push-2variables',A,B],I,I1,L,L1),
  handle_all(Y,I1,II,L1,LL).

handle_all([X|Y],I,II,L,LL) :- !,
  handle(X,I,I1,L,L1),!,
  handle_all(Y,I1,II,L1,LL).


chech_push(F) :-
     F==#t ->  
       throw(first_variable_in_scheme_context); 
     true.
 
-extended.
handle([label,N],I,II,L,LL) :- !,
 N=I,I=II,LL=L.

handle([softie,A],I,II,L,LL) :- !,
   (
     (var(A) ; number(A)) ->
       L=[[softie,A]|LL] ;
     (
       A=[[S,V,Q],N,F|_],
       new_var(V,Q,S),
       V=[VC|_],
       L=[['softie-light',VC]|LL]
     )
   ),
   II is I + 2.

handle(['newframe-light',[[S,V,Q],N,F|_],A],I,II,L,LL) :-
   new_var(V,Q,S),
   V=[VC|_],
   L=[['newframe-light',VC,A]|LL], 
   II is I + 3.

handle(['unwind-light-tail',[[S,V,Q],N,F|_]],I,II,L,LL) :-
   new_var(V,Q,S),
   V=[VC|_],
   L=[['unwind-light-tail',VC]|LL], 
   II is I + 2.

handle(['unwind-light',[[S,V,Q],N,F|_],B],I,II,L,LL) :-
   new_var(V,Q,S),
   V=[VC|_],
   L=[['unwind-light',B]|LL], 
   II is I + 2.

handle((X,[('goto-inst';'store-state'   ;'unwind-tail';
            'post-q'   ;'newframe-light'),N]),
         I,II,L,LL) :- !,
 II is I + 2,
 L=[X|LL].


handle((X,[(newframe ; 'newframe-negation' ; 'post-negation'
                     ; 'unwind'            ; 'unwind-negation'
                     ; 'post-s'),A,B]),
        I,II,L,LL) :- !,
 II is I + 3,
 L=[X|LL].

handle(['newframe-ps',A0,[[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_]],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 new_var(V2,Q2,S2),
 (V1=[V1C|_] -> E1=0 ; (V1=V1C,E1=1)),
 (V2=[V2C|_] -> E2=0 ; (V2=V2C,E2=1)),
 U is E1 + E2<<1,
 Code is V1C + V2C << 16 + U << 32
 L = [['newframe-ps',A0,Code]|LL],
 II is I + 2.

handle(['newframe-pst',A0,[[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_],
                          [[S2,V2,Q2],N2,F2|_]],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 new_var(V2,Q2,S2),
 new_var(V3,Q3,S3),
 (V1=[V1C|_] -> E1=0 ; (V1=V1C,E1=1)),
 (V2=[V2C|_] -> E2=0 ; (V2=V2C,E2=1)),
 (V3=[V3C|_] -> E3=0 ; (V3=V3C,E3=1)),
 U is E1 + E2<<1 + E3 << 2,
 Code is V1C + V2C << 16 + V3C<<32 + U << 48
 L = [['newframe-pst',A0,Code]|LL],
 II is I + 3.

handle(['unwind-ps',[[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_]],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 new_var(V2,Q2,S2),
 (V1=[V1C|_] -> E1=0 ; (V1=V1C,E1=1)),
 (V2=[V2C|_] -> E2=0 ; (V2=V2C,E2=1)),
 U is E1 + E2<<1,
 Code is V1C + V2C << 16 + U << 32
 L = [['unwind-ps', Code]|LL],
 II is I + 2.

handle(['unwind-psc',[[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_],C],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 new_var(V2,Q2,S2),
 (V1=[V1C|_] -> E1=0 ; (V1=V1C,E1=1)),
 (V2=[V2C|_] -> E2=0 ; (V2=V2C,E2=1)),
 U is E1 + E2<<1,
 Code is C + V1C<<16 + V2C << 32 + U << 48
 L = [['unwind-psc', Code]|LL],
 II is I + 2.

handle(['unwind-psct',A0,[[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_],
                         [[S2,V2,Q2],N2,F2|_], C],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 new_var(V2,Q2,S2),
 new_var(V3,Q3,S3),
 (V1=[V1C|_] -> E1=0 ; (V1=V1C,E1=1)),
 (V2=[V2C|_] -> E2=0 ; (V2=V2C,E2=1)),
 (V3=[V3C|_] -> E3=0 ; (V3=V3C,E3=1)),
 U is E1 + E2<<1 + E3 << 2,
 Code is V1C + V2C << 16 + V3C<<32 + U << 48
 L = [['unwind-psct',C,Code]|LL],
 II is I + 3.

handle(['unwind-pst',A0,[[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_],
                         [[S2,V2,Q2],N2,F2|_]],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 new_var(V2,Q2,S2),
 new_var(V3,Q3,S3),
 (V1=[V1C|_] -> E1=0 ; (V1=V1C,E1=1)),
 (V2=[V2C|_] -> E2=0 ; (V2=V2C,E2=1)),
 (V3=[V3C|_] -> E3=0 ; (V3=V3C,E3=1)),
 U is E1 + E2<<1 + E3 << 2,
 Code is V1C + V2C << 16 + V3C<<32 + U << 48
 L = [['unwind-pst',Code]|LL],
 II is I + 2.


handle(['store-ps',A0,[[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_]],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 new_var(V2,Q2,S2),
 (V1=[V1C|_] -> E1=0 ; (V1=V1C,E1=1)),
 (V2=[V2C|_] -> E2=0 ; (V2=V2C,E2=1)),
 U is E1 + E2<<1,
 Code is V1C + V2C << 16 + U << 32
 L = [['store-ps',A0,Code]|LL],
 II is I + 2.

handle(['store-p',A0,[[S1,V1,Q1],N1,F1|_]],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 (V1=[V1C|_] -> E1=0 ; (V1=V1C,E1=1)),
 Code is V1C + E1 << 16
 L = [['store-p',A0,Code]|LL],
 II is I + 2.

handle(['restore-c',C,],I,II,L,LL) :-
 !,
 L = [['store-p',C]|LL],
 II is I + 2.

handle(['fail-psc',[[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_],C],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 new_var(V2,Q2,S2),
 (V1=[V1C|_] -> E1=0 ; (V1=V1C,E1=1)),
 (V2=[V2C|_] -> E2=0 ; (V2=V2C,E2=1)),
 U is E1 + E2<<1,
 Code is C + V1C<<16 + V2C<<32 + U << 48,
 L = [['fail-psc',Code]|LL],
 II is I + 2.

handle(['fail-pc',[[S1,V1,Q1],N1,F1|_],C],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 (V1=[V1C|_] -> E1=0 ; (V1=V1C,E1=1)),
 Code is C + V1C<<16 + E1<<32,
 L = [['fail-psc',Code]|LL],
 II is I + 2.

handle(['softie-psc',[[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_],C],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 new_var(V2,Q2,S2),
 (V1=[V1C|_] -> E1=0 ; (V1=V1C,E1=1)),
 (V2=[V2C|_] -> E2=0 ; (V2=V2C,E2=1)),
 U is E1 + E2<<1,
 Code is C + V1C<<16 + +V2C<<32 + U << 48
 L = [['softie-psc',Code]|LL],
 II is I + 2.

handle(['softie-ps',[[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_]],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 new_var(V2,Q2,S2),
 (V1=[V1C|_] -> E1=0 ; (V1=V1C,E1=1)),
 (V2=[V2C|_] -> E2=0 ; (V2=V2C,E2=1)),
 U is E1 + E2<<1,
 Code is V1C + +V2C<<16 + U << 32
 L = [['softie-ps',Code]|LL],
 II is I + 2.

handle(['softie-pc',[[S1,V1,Q1],N1,F1|_],C],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 (V1=[V1C|_] -> E1=0 ; (V1=V1C,E1=1)),
 Code is C + V1C<<16 + E1 << 32,
 L = [['softie-pc',Code]|LL],
 II is I + 2.

handle(['post-sc',[[S1,V1,Q1],N1,F1|_],C],I,II,L,LL) :-
 !,
 new_var(V1,Q1,S1),
 (V1=[V1C|_] -> E1=0 ; (V1=V1C,E1=1)),
 Code is C + V1C<<16 + E1 << 32
 L = [['post-sc',Code]|LL],
 II is I + 2.


handle(['post-c',C],I,II,L,LL) :-
 !,
 L = [['post-c',C]|LL],
 II is I + 2.

handle([label,N,[_,Tags]],I,II,L,LL) :- !,
  N=I,
  addvs(Tags,I,II,L,LL).

handle((X,['post-call',A,P]),I,II,L,LL) :- !,
  II is I + 2, L=[X|LL].

handle((X,['post-unicall',A,P]),I,II,L,LL) :- !,
  get_nsvars(P,N),
  set(P,N),
  II is I + 3, L=[X|LL].
  
handle(['newvar', _],I,I,L,L) :- !.

handle([cutter,[[S1,V1,Q1],N1,F1|_],[[S2,V2,Q2],N2,F2|_]],I,II,L,LL) :- 
 !,
 (
   new_var(V1,Q1,S1),
   new_var(V2,Q2,S2),
   (V1=[V1C|_] -> E1=0 ; (V1=V1C,E1=1)),
   (V2=[V2C|_] -> E2=0 ; (V2=V2C,E2=1)),
   E  is E1  + E2 << 1,
   VC is V1C + V2C << 16 + E << 32,
   L=[[cutter,VC]|LL],
   II is I + 2
 ).

handle(['pre-unify',At,Vx],I,II,L,LL) :- !,
 Vx=[[S,V,Q],N,F|_],
 (  
   N==1 -> throw(begin_with_no_end) ;
   isCplx(At) -> 
      (
        new_var(V,Q,S),
        L=[['pre-unify',V]|LL],
        II is I + 2
      ) ;
      (
        L=LL,
        I=II
      )
 ).

handle(['post-unify-tail',[[S,V,Q],N,F|_]],I,II,L,LL) :- 
 !,
 (
   F==#t -> throw(end_with_no_begin) ;
   (
     new_var(V,Q,S),
     L=[['post-unify-tail',V]|LL],
     II is I + 2
   )
 ).

handle(['post-unify',[[S,V,Q],N,F|_]],I,II,L,LL) :- 
 !,
 (
   F==#t -> throw(end_with_no_begin) ;
   (
     (get_nsvars(Q,M) -> true ; M=0),
     new_var(V,Q,S),
     L=[['post-unify',V,M]|LL],
     II is I + 3
   )
 ).

handle([set,(W,[[S,V,Q],N,F|_])],I,II,L,LL) :- !,
  (
    (F=#t,N=1) -> (L=LL, I=II)                                ;
     S=#t      -> (new_var(V,Q,S),L=[[set,V]|LL],II is I + 2) ;
                  handle([unify,W,#t],I,II,L,LL)
  ).

handle(['push-variable',[[S,V,Q],N,F|_]],I,II,L,LL) :- !,
  (
    (F==#t,N==1) -> (L=[[pushv,#f]|LL]                     , II is I + 2);
    F==#t        -> (new_var(V,Q,S), L=[[pushv,V]|LL]          , II is I + 2);
    (
      new_var(V,Q,S),
      (V=[VC|_] -> P='push-variable-s' ; (P='push-variable-v',VC=V)),
      L=[[P,VC]|LL], 
      II is I + 2
    )
  ).

handle(['push-2variables',[[S1,V1,Q1],N1,F1|_],
                          [[S2,V2,Q2],N2,F2|_]],I,II,L,LL) :- !,
   new_var(V1,Q1,S1), 
   new_var(V2,Q2,S2),
   code([push2,V1,V2],Code,Push2),
   L=[[Push2,Code]|LL], 
   II is I + 2.

handle(['push-3variables',[[S1,V1,Q1],N1,F1|_],
                          [[S2,V2,Q2],N2,F2|_],
                          [[S3,V3,Q3],N3,F3|_]],I,II,L,LL) :- !,
   new_var(V1,Q1,S1), 
   new_var(V2,Q2,S2),
   new_var(V3,Q3,S3),
   code([push3,V1,V2,V3],Code,Push3),
   L=[[Push3,Code]|LL], 
   II is I + 2.
 


handle(['push-variable-scm',[[S,V,Q],N,F|_]],I,II,L,LL) :- !,
  (
     F==#t        ->  
       throw(first_variable_in_scheme_context);
       (L=[['push-variable-scm',V]|LL], II is I + 2)
  ).

handle([comp,CMP,[[S1,V1,Q1],N1,F1|_],
                 [[S2,V2,Q2],N2,F2|_]],I,II,L,LL) :- !,
    chech_push(F1),
    chech_push(F2),
    new_var(V1,Q1,S1),
    new_var(V2,Q2,S2),
    code([comp,CMP,V1,V2],Code,Comp),
    L=[[Comp,Code]|LL],
    II is I + 2.    

handle([bin,Op,[[S1,V1,Q1],N1,F1|_],
               [[S2,V2,Q2],N2,F2|_],
               [[S3,V3,Q3],N3,F3|_]],I,II,L,LL) :- !,
    chech_push(F1),
    chech_push(F2),
    new_var(V1,Q1,S1),
    new_var(V2,Q2,S2),    
    (
      (F3==#t,N3==1) ->
        (
          (L=[[true]|LL] , II is I + 1)
        );
      (F3==#t,S3==#t) ->
        (
          new_var(V3,Q3,S3),
          code([bin,Op,V1,V2,V3,3],K,Add),
          L=[[Add,K]|LL],
          II is I + 2
        );
      F3==#t ->
        (
           new_var(V3,Q3,S3),
           code([bin,Op,V1,V2,V3,0],K,Add),
           L=[[Add,K]|LL],
           II is I + 2          
        );
      N3==1 ->
      (
        new_var(V3,Q3,S3),
        code([bin,Op,V1,V2,V3,1],5,Add),
        L=[[Add,K]|LL],
        II is I + 2
      ) ;
      (
        new_var(V3,Q3,S3),
        code([bin,Op,V1,V2,V3,1],K,Unify),
        L=[[Unify,K]|LL],
        II is I + 2      
      )
    ).

handle([(Kind,(ibin;bini)),Op,X,
            [[S2,V2,Q2],N2,F2|_],
            [[S3,V3,Q3],N3,F3|_]],I,II,L,LL) :- !,
    chech_push(F2),
    new_var(V2,Q2,S2),
    (
      (F3==#t,N3==1) ->
        (
          (L=[[true]|LL] , II is I + 1)
        );
      (F3==#t,S3==#t) ->
        (
          new_var(V3,Q3,S3),
          code([Kind,Op,V2,V3,3],K,Add),
          L=[[Add,X,K]|LL],
          II is I + 3
        );
      F3==#t ->
        (
           new_var(V3,Q3,S3),
           code([Kind,Op,V2,V3,0],K,Add),
           L=[[Add,X,K]|LL],
           II is I + 3
        );
      N3==1 ->
      (
        new_var(V3,Q3,S3),
        code([Kind,Op,V2,V3,5],K,Add),
        L=[[Add,X,K]|LL],
        II is I + 3
      ) ;
      (
        new_var(V3,Q3,S3),
        code([Kind,Op,V2,V3,1],K,Unify),
        L=[[Unify,X,K]|LL],
        II is I + 3      
      )
    ).


handle([unify,[[S,V,Q],N,F|_],M],I,II,L,LL) :- !,
    (
      (F==#t,N==1) ->
        (
          (M==#t ; M==0) -> (L=[[pop,1]|LL],II is I + 2) ;
          (L=[[false]|LL] , II is I + 1)
        );
      (F==#t,S==#t) ->
        (
          M=#f -> (L=[[false]|LL], II is I + 1) ;
          (
            new_var(V,Q,S),
            code([unify,M,V,#t],K,Unify),
            L=[[Unify,K]|LL],
            II is I + 2
          )
        );
      (F==#t) ->
        (
          M=#f -> (L=[[false]|LL], II is I + 1) ;
          (
            new_var(V,Q,S),
            code([unify,M,V,#f],K,Unify),
            L=[[Unify,K]|LL],
            II is I + 2
          )
        );
      (N==1) ->
      (
        code([unify,M,V,1],K,Unify),
        L=[[Unify,K]|LL],
        II is I + 2
      ) ;
      (
        code([unify,M,V,0],K,Unify),
        L=[[Unify,K]|LL],
        II is I + 2      
      )
   ).

handle([U,[[S,V,Q],N,F|_],A,M],I,II,L,LL) :- !,
 ((U='unify-constant-2' ; U='unify-instruction-2') ->
  ( 
    regconst(A,XX),
    (
     (F==#t,N==1)  -> (L=LL,II is I) ;
     (F==#t,S==#t) ->
      (
        new_var(V,Q,S),
        L=[[U,M,XX,V,#t]|LL],
        II is I + 5
      );
      F==#t -> 
      (
        new_var(V,Q,S),
        L=[[U,M,XX,V,#f]|LL],
        II is I + 5
      ) ;
      N==1 ->
      (
       L=[[U,M,XX,V,1]|LL],
       II is I + 5
      ) ;        
      (
       L=[[U,M,XX,V,0]|LL],
       II is I + 5
      )
     )        
  );
 (U='unify-2') ->
   (
     A=[[S2,V2,Q2],N2,F2|_],
     (
       ((F==#t,N==1) ; (F2==#t,N2==1)) -> 
          (
            LL=L, 
            II=I
          ) ;
       (F==#t,S==#t,F2==#t,S2==#t) ->
          (
            M=#f -> (L=[[false]|LL],II is I + 1) ;
            (
              new_var(V,Q,S),new_var(V2,Q2,S2),
              code([U,M,V,#t,V2,#t],K),
              L=[[U,K]|LL],
              II is I + 2
            )
          );
       (F==#t,S==#t,F2==#t) ->
          (
            M=#f -> (L=[[false]|LL], II is I + 1) ;
            (
              new_var(V,Q,S), new_var(V2,Q2,S2),
              code([U,M,V,#t,V2,#f],K),
              L=[[U,K]|LL],
              II is I + 2
            )
          );
       (F==#t,F2==#t,S2==#t) ->
          (
            M=#f -> (L=[[false]|LL], II is I + 1) ;
            (
              new_var(V,Q,S), 
              new_var(V2,Q2,S2),
              code([U,M,V,#f,V2,#t],K),
              L=[[U,K]|LL],
              II is I + 2
            )
          );
       (F==#t,F2==#t) ->
          (
            M=#f -> (L=[[false]|LL], II is I + 1) ;
            (
              new_var(V,Q,S), 
              new_var(V2,Q2,S2),
              code([U,M,V,#f,V2,#t],K),
              L=[[U,K]|LL],
              II is I + 2
            )
          );
       F==#t ->
         (
            M=#f -> (L=[[false]|LL], II is I + 1) ;
            (
              new_var(V,Q,S),
              new_var(V2,Q2,S2),
              (N2 == 1 -> K=1 ; K=0),
              code([U,M,V,#f,V2,K],Code),
              L=[[U,Code]|LL],
              II is I + 2
            )
         );
       F2==#t ->
         (
            M=#f -> (L=[[false]|LL], II is I + 1) ;
            (
              new_var(V2,Q2,S2),
              (N == 1 -> K=1 ; K=0),
              code([U,M,V,K,V2,#f],Code),
              L=[[U,Code]|LL],
              II is I + 2
            )
         );
       (
         (N  == 1 -> K1=1 ; K1=0),
         (N2 == 1 -> K2=1 ; K2=0),
         code([U,M,V,K1,V2,K2],Code),
         L=[[U,Code]|LL],
         II is I + 2
       )          
     )
   )).


handle(X,I,II,L,LL) :- !,
   (
    X=[A]   -> (U=[], II is I + 1) ;
    X=[A,N] -> 
      (regconst(N,J),U=[J],II is I+2);
    X=[A,N,M] ->
      (regconst(N,J1),regconst(M,J2),U=[J1,J2],II is I+3);
    X=[A,N,M,O] ->
      (regconst(N,J1),regconst(M,J2),regconst(O,J3),U=[J1,J2,J3],II is I+4);
    X=[A,N,M,O,P] ->
      (
        regconst(N,J1),regconst(M,J2),regconst(O,J3),regconst(P,J4),
        U=[J1,J2,J3,J4],II is I+5
      )
   ), 
   tr(A,AA),!,
   XX=[A|U],
   L=[XX|LL].

code([unify,M,V,K],Code,Action) :- !,
  (
  (M\\=#f,K=#f) ->
  (
    V=[U|_] -> 
       (Action = 'sp-move-s', Code = U);
       (Action = 'sp-move'  , Code = V)
  ) ;
  (
   Action=unify,
  (
    M = #f -> MC=2 ;
    M = #t -> MC=3 ;
    MC=M
  ),
  (V=[VC|_] -> A=1 ; (V=VC, A=0)),
  (
    K = #f -> KC=2 ;
    K = #t -> KC=3 ;
    KC=K
  ),
  U    is A + KC << 1 + VC << 3,
  Code is MC + U << 2
  )).

code(['unify-2',M,V1,K1,V2,K2],Code) :- !,
  (
    M = #f -> MC=2 ;
    M = #t -> MC=3 ;
    MC=M
  ),
  (V1=[V1C|_] -> A1=1 ; (V1=V1C, A1=0)),
  (V2=[V2C|_] -> A2=1 ; (V2=V2C, A2=0)),
  (
    K1  = #f -> K1C=2 ;
    K1  = #t -> K1C=3 ;
    K1C = K1
  ),
  (
    K2  = #f -> K2C=2 ;
    K2  = #t -> K2C=3 ;
    K2C = K2
  ),
  U1   is A1 + K1C << 1 + V1C << 3,
  U2   is A2 + K2C << 1 + V2C << 3,
  Code is  MC + U1 << 2 + U2 << (2 + 24).

code([bin,Op,V1,V2,V3,K3],Code,Action) :-
 (
  V1=[V1C|_] ->
    (
       V2=[V2C|_] ->
         (
            V3 = [V3C|_] ->
               (
                 K3 = #f ->
                   (
                     !,
                     code3(V1C,V2C,V3C,Code),
                     binss2(Op,Action)
                   ) 
               ) 
         )
    )
 ).

code([bin,Op,V1,V2,V3,K3],Code,Action) :-
  (V1=[V1C|_] -> A1=1 ; (V1=V1C, A1=0)),
  (V2=[V2C|_] -> A2=1 ; (V2=V2C, A2=0)),
  (V3=[V3C|_] -> A3=1 ; (V3=V3C, A3=0)),
  KC = K3,
  A    is A1 + A2 << 1 + A3 << 2 + KC << 3,
  Code is V1C + V2C << 16 + V3C << 32 + A << 48,
  binxx2(Op,Action).

code([bini,OP,V2,V3,K3],Code,Action) :-
 (
   V2=[V2C|_] ->
   (
     V3 = [V3C|_] ->
     (
       K3 == 0 ->
         (
           !,
           code2(V2C,V3C,Code),
           binsi2(OP,Action)
         ) 
      ) 
   )
 ).

code([bini,OP,V1,V2,K],Code,Action) :-
  (V1=[V1C|_] -> A1=1 ; (V1=V1C, A1=0)),
  (V2=[V2C|_] -> A2=1 ; (V2=V2C, A2=0)),
  A is A1 + A2 << 1 +  K << 2,
  Code is V1C + V2C << 16 + A << 32,
  binxi2(OP,Action).

code([ibin,OP,V2,V3,K3],Code,Action) :-
 (
   V2=[V2C|_] ->
   (
     V3 = [V3C|_] ->
     (
       K3 == #f ->
         (
           !,
           code2(V2C,V3C,Code),
           binis2(OP,Action)
         ) 
      ) 
   )
 ).

code([push2,V2,V3],Code,Action) :-
 (
   V2=[V2C|_] ->
   (
     V3 = [V3C|_] ->
     (
        !,
        code2(V2C,V3C,Code),
        Action = 'push-2variables-s'
     ) 
   )
 ).

code([push2,V1,V2],Code,Action) :-
  (V1=[V1C|_] -> A1=1 ; (V1=V1C, A1=0)),
  (V2=[V2C|_] -> A2=1 ; (V2=V2C, A2=0)),
  A is A1 + A2 << 1,
  Code is V1C + V2C << 16 + A << 32,
  Action = 'push-2variables-x'.

code([comp,CMP,V2,V3],Code,Action) :-
 (
   V2=[V2C|_] ->
   (
     V3 = [V3C|_] ->
     (
        !,
        code2(V2C,V3C,Code),
        binss(CMP,Action)
     ) 
   )
 ).

code([comp,CMP,V2,V3],Code,Action) :-
  (V1=[V1C|_] -> A1=1 ; (V1=V1C, A1=0)),
  (V2=[V2C|_] -> A2=1 ; (V2=V2C, A2=0)),
  A is A1 + A2 << 1,
  Code is V1C + V2C << 16 + A << 32,
  binxx(CMP,Action).

code([push3,V1,V2,V3],Code,Action) :-
 V1=[V1C|_] ->
 (
   V2=[V2C|_] ->
   (
     V3 = [V3C|_] ->
     (
        !,
        code3(V1C,V2C,V3C,Code),
        Action = 'push-3variables-s'
     ) 
   )
 ).

code([push3,V1,V2,V3],Code,Action) :-
  (V1=[V1C|_] -> A1=1 ; (V1=V1C, A1=0)),
  (V2=[V2C|_] -> A2=1 ; (V2=V2C, A2=0)),
  (V3=[V3C|_] -> A3=1 ; (V3=V3C, A3=0)),
  A is A1 + A2 << 1 + A3 << 2,
  Code is V1C + V2C << 16 + V3C << 32 + A << 48,
  Action = 'push-3variables-x'.

code3(V1,V2,V3,C) :- C is V1 + V2 << 16 + V3 << 32.
code2(V1,V2,C) :- C is V1 + V2 << 16.


")
