(<define> (set_extended x)
  (<code> (set! unify_operators (combine_ops (<scm> x)))))

(<define> (gen x) (<=> x ,(gensym "disj")))
(<define> (gen-rec x) (<=> x ,(gensym "Rec")))

(<define> (get_consts x) (<=> x ,(get-consts)))

(compile-prolog-string "

reverse_op(<,>).
reverse_op(>,<).
reverse_op(=<,>=).
reverse_op(>=,=<).
reverse_op(@<,@>).
reverse_op(@>,@<).
reverse_op(@=<,@>=).
reverse_op(@>=,@=<).
reverse_op(=:=,=:=).
reverse_op(=\\=,=\\=).



zero(V)    :- get_A(V,A),A=[[0|_]].

print([]).
print([X|L]) :- write(X),nl,print(L).

wrap(Code,[L,LL]) :-
  catch(Code,E,
     (  
        tfc(E),
        (
           E==#t -> L=[[[cc]|LL],LL];
           E==c  -> L=[[[cut],[fail]|LL],LL];
           L=[[[fail]|LL],LL]
        )
     )).

-extended(',',m_and,;,m_or,\\+,m_not).
compile_goal(Code,Iout):- !,
  compile_goal(Code,Iout,StackSize,Narg,Consts,#t).

compile_goal(Code,Iout,StackSize,Narg,Constants,Pretty) :- !,
  (
    Code = (F(|A) :- Goal) -> length(A,Narg) ;
    Code = (F     :- Goal) -> Narg = 0       ;
    throw(compiles_only_clauses(Code))
  ),
  make_vhash(HV),
  make_vhash(HC),
  init_vars,init_e,
  (var(Constants) -> init_const ; true),
  b_setval(pretty,#t),
  make_state(0,[[0,_,_]],[],[[0,_]],0,0,0,0,0,[HC,HV],[],V),
  wrap(compile_goal(Code,#t,V,[L,[]]),[L,[]]),!,
  %print(L),nl,!,
  get_M(V,StackSize),
  handle_all(L,LL),
  
  (var(Constants)-> get_consts(Constants);true),
  print(LL),nl,!,
  (Pretty==#t -> Iout=LL ; (b_setval(pretty,#f),mmtr(LL,Iout))).

compile_goal(X,Tail,V,L) :- var_p(X),!,
  compile_goal(call(X),Tail,V,L).


compile_goal(pr(X),Tail,V,[L,L]) :- !,
  write(pr(X)),nl.

compile_goal(!,Tail,V,[L,LL]) :- !,
  check_tail(Tail),
  (Tail==#t -> L=[[cut],[cc]|LL] ; L=[[cut]|LL]).

compile_goal(softie(A),Tail,V,[LL,L]) :- !,
  check_tail(Tail),
  (Tail==#t -> L=[[softie,A],[cc]|LL] ; L=[[softie,A]|LL]).

compile_goal(begin_att,Tail,V,[L,LL]) :- !,
   check_tail(Tail),
   tail(Tail,LL,LLL),
   (
     Tail==#t -> L=LLL ;
     (
       get_A(V,A),
       A=[[Ai  | _] | _],
       set_A(V,[[AAi,AAx,AAt] | A]),
       AAi is Ai + 1,
       var_p(AAx),
       add_var(AAx,V,Tagx),
       tr('pre-unify',Pre),
       L = [[Pre,AAt,Tagx]|LL]
     )
   ).

compile_goal(end_att,Tail,V,[L,LL]) :- !,
   check_tail(Tail),
   get_AF(V, A, F),
   [[Ai,Ax,At]|AA]=A,
   set_A(V, AA),
   (Ai==0  -> throw(missmatching_begin_end_pair) ; true),
   var_p(Ax),
   add_var(Ax,V,Tag),
   (Tail==#t -> tr('post-unify-tail',Post) ; tr('post-unify',Post)),      
   (
     Tail==#t ->  LLL=LL ; 
     (
       At==#t ->
         (
           tr('post-unicall',PostCall),
           get_C(V,[C|_]),
           LLL=[[PostCall,C,F]|LL]
         ) ;
         LLL=LL
     )
   ),
   (
      At   == #t  ->  L = [[Post,Tag]|LLL] ; 
      Tail == #t  ->  L = [[cc]|LLL]       ; 
                      L = LLL
   ).  

compile_goal(pop(N),Tail,V,[L,LL]) :- !,
  check_tail(Tail),
  M is -N,
  push_v(M,V),
  tail(Tail,LL,LLL),
  L=[[pop,N]|LLL].

compile_goal(with_cut(C,CS),Tail,V,[L,LL]) :- !,
  var_p(C),
  var_p(CS),
  add_var(C ,V,TagC ),
  add_var(CS,V,TagCS),
  L=[[cutter,TagC,TagCS]|LL].

compile_goal((Args <= Goal),Tail,V,L) :- !,
  (listp(Args) -> true ; throw(not_proper_head(Args <= goal))),
  reverse(Args,AArgs),
  mg(cc(|Args),AArgs,Impr,0,N),
  NN is N + 3,
  link_l(L,L1,L2,L3),
  compile_goal(Goal,label(G,NN),V,L1),
  L2=[[[label,G]|U],U],
  set_S(V,0),
  push_v(NN,V),
  compile_goal((begin_att,Impr,pop(3),end_att),Tail,V,L3).

compile_goal((F(|Args) :- Goal),Tail,V,L) :- !,
  (listp(Args) -> true ; throw(not_proper_head(F(|Args)))),
  reverse(Args,AArgs),
  mg(F(|Args),AArgs,Impr,0,N),
  NN is N + 4,
  push_v(NN,V),
  get_A(V,[[0|_]|_]) ->
     wrap(compile_goal((begin_att,Impr,pop(4),end_att,Goal),Tail,V,L),L);
     wrap(compile_goal((begin_att,Impr,pop(4),Goal),Tail,V,L),L).

compile_goal((F :- Goal),Tail,V,L) :- !,
  push_v(4,V),
  wrap(compile_goal((pop(4),Goal),Tail,V,L),L).

compile_goal(newtag_F(F),Tail,V,[L,L]) :-
  gen(F).

compile_goal(extended_off,Tail,V,[L,L]) :- !,
    set_extended(#f).

compile_goal(extended_on(|L),Tail,V,[L,L]) :- !,
  set_extended(L).

compile_goal(extended_on,Tail,V,[L,L]) :- !,
  set_extended([]).
compile_goal(collect_F(F),Tail,V,[L,L]) :-
  get_F(V,F).

compile_goal(Op(\"recur\",Args),Tail,V,[L,LL]) :-    
   Args = F(|U),
   Op=\"op2*\",!,
   get_line(U,X,Xin,N),
   reverse(X,XX),
   mg(recur(|U),XX,Impr,0,N),
   add_recur(F,A,N),
   push_args_args(#f,Xin,V,L,L1,_,_),
   touch_A(V),
   touch_Q(10,V),
   %gen_rec(Sym)
   %set_F(V,Sym)
   L1=[[label,A]|L2],
   compile_goal((begin_att,Impr,end_att),Tail,V,[L2,LL]).


compile_goal((X,Y),Tail,V,L) :- !,
  collect_conj((X,Y),Gs,[]),
  compile_conj(Gs,Tail,V,L).


compile_goal(m_or(fail,false),#t,V,[[[fail],[cc] |L], L]).
compile_goal(m_or(fail,false),#f,V,[[[fail]      |L], L]).
compile_goal(true,_,_,_) :- throw(#t).
compile_goal(';'(|X),Tail,V,[L,LL]) :- !,
   collect_disj(X,XX,[]),
   (
     XX=[]  -> L=[[false]|LL]                ;
     XX=[Z] -> compile_goal(Z,Tail,V,[L,LL]) ; 
     (
       get_AB2ESM(V,Aq,B2,E,S,M),
       get_F(V,F), 
       label(Lab),label(Out),
       push_Q(0,V,Q),
       compile_disjunction(XX,#t,Aq,Ae,Out,Lab,LabA,Tail,S,U,B2,V,LM),!,
       (zero(V) -> Tp is 0 ; Tp is 1),
       (
         var(Q) ->
            (
              var_p(Var),
              add_var_f(Var,V,F,LabA), 
              L   = [['newframe-light',LabA,Lab] | LX]
            ); 
            (
              Lab = LabA,
              L   = [[newframe        ,LabA,Tp] | LX]   
            )
       ),
       LM = [LX,  [[label,Out] | LL]],
       get_EBH(V,Ed,B,H),
       add_missing_variables(H,U,Ed,Ed,EEd),!,
       BB2 is B2 \\/ EEd,
       EE is  E  \\/ EEd,
       set_B2E(V,BB2,EE)
     )
  ).

compile_goal(verbatim_call(X),Tail,V,[L,LL]) :- !,
   ( \\+var_p(X),
     ( 
       F(|Args)=X -> 
        (
          (var_p(F) ; isApply(Args)) -> fail ; true
        ) ; 
       true
     )
   ),
   !, 
   compile_goal(X,Tail,V,[L,LL]).

compile_goal(call(X),Tail,V,L) :- !,
   new_var(VS,V,TagS1),
   new_var(VP,V,TagP1),
   compile_goal(call_(X,A,Al,[C0|_],Pre,Post,LP),Tail,V,L),
   new_var(VS,V,TagS2),
   new_var(VP,V,TagP2),
   Ad=[A0,Cut_p],
   (
    A == #t ->
    ( 
     Cut_p == #t ->
     (
        Pre  = ['store-ps'   , A0, TagP1, TagS1    ],
        Post = [['goto-inst' , Out                 ],
                [label       , A0                  ],
                ['fail-psc'  , TagP1, TagS1, C0    ],
                [label       , Out                 ],
                ['post-sc'   , TagS1, C0           ]|LP]
     ) ; 
     (
       Pre  = [true],
       Post = LP
     )
    ) ;
    (
     Cut_p == #t ->
     (
        Pre  = ['store-p', A0, TagP1],
        Post = [['goto-inst' , Out          ],
                [label       , A0           ],
                ['fail-pc  ' ,TagP1, C0     ],
                [label       , Out          ],
                ['post-c'    , C0    ]|LP]
     ) ; 
     (
       Pre  = [true],
       Post = LP
     )
    )
  ).

compile_goal(call_(X,A0,Al,C0,Pre,Post,LP),Tail,V,[L,LL]) :- !,
   L=[Pre|LX],
   get_C(V,C),
   get_AA(V,A),
   AA = [A0|A],
   set_AA(V,AA),
   C  = [C0|_],
   CC = [Al|C],
   set_C(V,CC),
   compile_goal(X,#f,V,[LX,LLX]),
   set_AA(V,A),
   LLX = Post,
   set_C(V,C),
   (Tail=#t -> LP=[[cc]|LL] ; LP=LL).

compile_goal((X =.. Y),Tail,V, L, LL) :- !,
  (var_p(X);constant(X)) ->
  (
     compile_arg(X,Branch,HC,HV,L,LLX),
     compile_imprint(Y,Branch,HC,HV,LLX,LLY),
     tr(=..,Op),
     LLY=[[Op]|LL]
  ).
  

compile_goal(set(V,X),Tail,V,[L,LL]) :- !,
  touch_Q(11,V),
  tr(set,Set),
  (var_p(X) -> true ; throw(no_var_in_set)),
  add_var(X,V,Tag),
  (isFirst(Tag) -> true ; force_variable(Tag)),
  push_args(X,V,L,LX),
  push_v(-1,V),
  (
    Tail=#t ->
       LX=[[Set,Tag],[cc]|LL];
       LX=[[Set,Tag]     |LL]  
  ).

compile_goal(once(G),Tail,V,[L,LL]) :- !,
  var_p(VP),
  var_p(VS),
  new_var(VP,V,TagP1),
  new_var(VS,V,TagS1),
  compile_goal(call_(G,A,Al,[C0|_],Pre,Post,LP),#f,V,[LX,LLX]),
  new_var(VP,V,TagP2),
  new_var(VS,V,TagS2),
  get_C(V,C),    
  Al=[A0,Cut_p],
  (
    A==#t ->
    (
      Cut_p==#t ->
      (
        Pre  = ['store-ps'  , A0,TagP1,TagS1   ],
        Post = [['goto-inst' , Out              ],
                [label       , A0               ],
                [fail-psc    , TagP2,TagS2,C0   ],
                [label,Out                      ],
                ['softie-psc', TagP2,TagS2,C0   ]|LP]
      ) ;
      (
        Pre  = ['store-ps',TagP1,TagS1],
        Post = [['softie-ps',TagP2,TagS2]|LP]
      )
   ) ;
   (
     Cut_p==#t ->
      (
        Pre  = ['store-p',A0,TagP1],
        Post = [['goto-inst',Out],
                [label      ,A0],
                [fail-pc    ,TagP2,C0],
                [label,Out],
                ['softie-pc',TagP2,C0]|LP]
      ) ;
      (
        Pre  = [true],
        Post = LP
      )
   )
  ).
  
compile_goal((X->Y),Tail,V,L) :- !,
  compile_goal((once(X),Y),Tail,V,L).

compile_goal(\\+\\+\\+X,Tail,V,[L,LL]) :- !,
   compile_goal(\\+X,Tail,V,[L,LL]).

compile_goal(\\+\\+X,Tail,V,[L,LL]) :- !,
   L=[['newframe-negation',Al,0]|LX],
   get_QAESBB2(V,Q,AA,E,S,B,B2),
   set_QAE(V,[],[[0|_]],0),
   new_var(VP,V,TagP1),
   new_var(VS,V,TagS1),
   new_var(VT,V,TagT1),
   compile_goal(call_(X,A,Ad,[C0|_],Pre,Post,LP),#f,V,[LX,LLX]),
   new_var(VP,V,TagP2),
   new_var(VS,V,TagS2),
   new_var(VT,V,TagT2),
   set_QAESBB2(V,Q,AA,E,S,B,B2),
   (A=[[0|_]|_] -> Tp is 0 ; Tp is 1),
   Ad=[A0,Cut_p],
   ( 
    A==#t ->
    (
     Cut_p == #t ->
     (
       Tp == 0 ->
       (
         Pre  = ['newframe-ps', A0, TagP1, TagS1],
         Post = [['goto-inst' , Out],
                 [label       , A0],
                 ['fail-psc'  , TagP2, TagS2, C0],
                 [label       , Out],
                 ['unwind-psc', TagP2, TagS2, C0]|LP]
       ) ;
       (
         Pre  = ['newframe-pst', A0, TagP1, TagS1, TagT1],
         Post = [['goto-inst'  , Out],
                 [label        , A0],
                 ['fail-psc'   , TagP2, TagS2, C0],
                 [label        , Out],
                 ['unwind-psct', TagP2, TagS2, TagT2, C0]|LP]
       )
     ) ;
     (
       Tp == 0 ->
       (
         Pre =  ['newframe-ps', TagP1, TagS1],
         Post = [['unwind-ps' , TagP2, TagS2]|LP]
       ) ;
       (
         Pre =  ['newframe-pst', TagP1, TagS1, TagT1],
         Post = [['unwind-pst' , TagP2, TagS2, TagT2]|LP]
       )
     )
    );
    (
     Cut_p == #t ->
     (
         Pre  = ['store-p'  , A0, TagP1],
         Post = [['goto-inst', Out      ],
                 [label      , A0       ],
                 ['fail-pc'  , TagP2, C ],
                 [label      , Out      ],
                 ['restore-c', C0       ]|LP]
     ) ;
     (
         Pre =  [true],
         Post = LP
     )
    )
 ).

compile_goal(\\+X,Tail,V,L) :- !,
  check_tail(Tail),
  new_var(VS,V,TagS1),
  new_var(VP,V,TagP1),
  new_var(VT,V,TagT1),
  get_QAESB(V,Q,A,E,S,B),
  
  ifc(
  (
     set_QAE(V,[],[[0|_]],0),
     compile_goal(call_(X,A,Ad,[C0|_],Pre,Post,LP),#f,V,L)
  ),Er,
  (   
      tfc(Er),
      (Er==#t -> throw(#f) ; throw(#t))
  ),
  (
    new_var(VS,V,TagS2),
    new_var(VP,V,TagP2),
    new_var(VT,V,TagT2),
    get_A(V,A1),
    set_QAESB(V,Q,A,E,S,B),
    (A=[[0|_]|_] -> Tp is 0 ; Tp is 1),
    Ad=[A0,Cut_p],
    
    ( 
     A==#t ->
     (
       Cut_p == #t ->
       (
         Tp == 0 ->
           (
              Pre  = ['newframe-psc',Al,TagP1,TagS1], 
              Post = [['fail-psc',TagP2,TagS2,C0],
                      [label,Al],
                      ['unwind-psc',TagP2,TagS2,C0]|LP]
          );
          (
              Pre  = ['newframe-psct',Al,TagP1,TagS1, TagT1], 
              Post = [['fail-psc',TagP2,TagS2,C0],
                      [label,Al],
                      ['unwind-psct',TagP2,TagS2,TagT2,C0]|LP]
          )
      ) ;
      (
        Tp == 0 ->
          (
            Pre  = ['newframe-ps',TagP1,TagS1],
            Post = [['fail-p',TagP2],
                    [label,Al],
                    ['unwind-ps',TagP2,TagS2]|LP]
          );
          (
            Pre  = ['newframe-pst',TagP1,TagS1,TagT1],
            Post = [['fail-p',TagP2],
                    [label,Al],
                    ['unwind-pst',TagP2,TagS2,TagT2]|LP]
          )
      )
     );
     (
       Cut_p == #t ->
       (
          Pre  = ['store-p',Al,TagP1], 
          Post = [['fail-pc',TagP2,C0],
                  [label,Al],
                  ['restore-pc',TagP2,C0]|LP]      
      ) ;
      (
         Pre  = ['store-p',Al,TagP1],
         Post = [['fail-p',TagP2],
                 [label,Al],
                 ['restore-p',TagP2]|LP]
      )
     )
    )
  )).



compile_goal((m_and(Op,m_or(<,>,=<,>=,=:=,=\\=,@<,@=<,@>,@>=)))(X,Y),
              Tail,V,[L,LL]) :- !,
  check_tail(Tail),
  tail(Tail,LL,LLL),
  ifc(compile_scm(X,V,L,LX),EX,
     (
       ifc(compile_scm(Y,V,L,LY),EY,
          (
             call(Op(EX,EY)) -> throw(#t) ; throw(#f)
          ),
          (
              push_v(-1,V),
              reverse_op(Op,Or),
              binop1L(Or,O),

              tr(O,OO),
              LY=[[OO,EX]|LLL]
         ))
     ),
     ifc(compile_scm(Y,V,LX,LY),EY,
       (
         push_v(-1,V),
         binop1L(Op,O),
         tr(O,OO),
         LX=[[OO,EY]|LLL]
       ),
       (
         push_v(-2,V),
         tr(Op,OOp),
         binop(OOp,O),
         LY=[[Op]|LLL]
       ))).

compile_goal(X is Y,Tail,V,L) :- !,
    (zero(V) -> 
      compile_goal((begin_att,iss(X,Y),end_att),Tail,V,L);
      compile_goal(iss(X,Y),Tail,V,L)).

compile_goal(iss(X,Y),Tail,V,[L,LL]) :- !,
  check_tail(Tail),
  tail(Tail,LL,LLL),
  (
    instruction(Y) ->
    (
      instruction(X) ->
      (
        X==Y -> throw(#t) ; throw(#f)
      );
      (
        tr('unify-instruction-2',Unify),
        var_p(X),
        add_var(X,V,Tag),
        L=[[Unify,Tag,Y,#t]|LLL]
      )
    );
    ifc(compile_scm(Y,V,L,LX),EY,compile_goal(X is EY, Tail, V, [L,LL]),
    (
      var_p(X)   ->   
       ( 
         add_var(X,V,Tag),
         (isFirst(Tag) -> true ; touch_Q(12,V)),
         push_v(-1,V),
         tr(unify,Unify),
         LX=[[unify,Tag,#t]|LLL]
       );
       (
         (
          constant(X) ->
             tr('equal-constant',Equal) ; 
          tr('equal-instruction',Equal)
         ),
         push_v(-1,V),
         LX=[[Equal,X]|LLL]
       )
    ))).
   

compile_goal(unify_with_occurs_check(X,Y),Tail,V,L) :- !,
  touch_Q(13,V),
  (
    X==Y    -> throw(#t) ;
    zero(V) -> 
      compile_goal((begin_att,uni_0(X,Y),end_att),Tail,V,L);
      compile_goal(uni_0(X,Y),Tail,V,L)
  ).

compile_goal(uni_0(X,Y),Tail,V,[L,LL]) :- !,
  check_tail(Tail),
  tail(Tail,LL,LLL),
  compile_unify(X,Y,V,[L,LLL],0).


compile_goal(X = Y,Tail,V,L) :- !,
  touch_Q(14,V),
  (
    X==Y    -> throw(#t) ;
    zero(V) -> 
      compile_goal((begin_att,uni_x(X,Y),end_att),Tail,V,L);
      compile_goal(uni_x(X,Y),Tail,V,L)
  ).

compile_goal(uni_x(X,Y),Tail,V,[L,LL]) :- !,
  check_tail(Tail),
  tail(Tail,LL,LLL),
  compile_unify(X,Y,V,[L,LLL],#t).

compile_goal(X == Y,Tail,V,L) :- !,
  (
    X==Y    -> throw(#t) ;
    zero(V) -> 
      compile_goal((begin_att,uni(X,Y),end_att),Tail,V,L);
      compile_goal(uni(X,Y),Tail,V,L)
  ).

compile_goal(uni(X,Y),Tail,V,[L,LL]) :- !,
  check_tail(Tail),
  tail(Tail,LL,LLL),
  compile_unify(X,Y,V,[L,LLL],#t).

compile_goal(imprint(X,M),Tail,V,[L,LL]) :- !,
  check_tail(Tail),
  tail(Tail,LL,LLL),
  compile_imprint(X,V,L,LLL,M).

compile_goal(m_and(X,F(|Args)),Tail,V,L) :- !,
   (
    (var_p(F) ; isApply(Args)) -> compile_goal(call(X),Tail,V,L) ;
    caller(F,Args,Tail,V,L)
   ).

compile_goal(F,Tail,V,L) :-
  constant(F) -> (!,caller(F,[],Tail,V,L)).

compile_goal(X,_,_,_) :-
  throw(failed_compile_goal(X)).
      
isApply(X) :- var_p(X),!.
isApply([X|L]) :- isApply(L).

ncons(X,N) :-
  ncons(X,0,N).

")


(set! (@@ (logic guile-log guile-prolog vm vm-var) compile_goal)
  compile_goal)
