
(<define> (make_state  f a aa c e m s b b2 h q x)
  (<var> (F A AA C E M S B B2 H Q)
    (<=> (F A AA C E M S B B2 H Q) 
	 (f a aa c e m s b b2 h q))
    (<=> x ,(vector F A AA C E M S B B2 H Q))))

(define nf  0)
(define na  1)
(define naa 2)
(define nc  3)
(define ne  4)
(define nm  5)
(define ns  6)
(define nb  7)
(define nb2 8)
(define nh  9)
(define nq  10)

(<define> (push_Q e v u)
  ;(<pp> `(push ,e))
  (let ((v (<lookup> v)))
    (<set> (vector-ref v nq) (cons u (<lookup> (vector-ref v nq))))))

(<define> (pop_Q e v x)
 ;(<pp> `(pop ,e))
 (let ((v (<lookup> v)))
   (<=> x ,(car (<lookup> (vector-ref v nq))))
   (<set> (vector-ref v nq) (cdr (<lookup> (vector-ref v nq))))))

(<define> (touch_Q e v)
   ;(<pp> `(touch ,e))
   (<recur> lp ((l (<lookup> (vector-ref (<lookup> v) nq))))
     (if (pair? l)
	 (<and>
	  (<=> #t ,(car l))
	  (lp (cdr l)))
	 <cc>)))

(<define> (read_Q e v q)
  ;(<pp> `(read ,e))
  (let* ((v (<lookup> v)))
    (<=> q ,(car (<lookup> (vector-ref v nq))))))

(<define> (copy_state v1 v2)
  (<var> (L F A AA C E M S B H Q)
    (<=> v2 ,(vector L F A AA C E M S B H Q))
    (<=> v2 v1)))

(<define> (get_EB v e b)
  (<let> ((v (<lookup> v)))
  (<=> e ,(<lookup> (vector-ref v ne)))
  (<=> b ,(<lookup> (vector-ref v nb)))))

(<define> (get_EBH v e b h)
  (<let> ((v (<lookup> v)))
  (<=> e ,(<lookup> (vector-ref v ne)))
  (<=> b ,(<lookup> (vector-ref v nb)))
  (<=> h ,(<lookup> (vector-ref v nh)))))

(<define> (get_SM v s m)
  (<let> ((v (<lookup> v)))
  (<=> s ,(<lookup> (vector-ref v ns)))
  (<=> m ,(<lookup> (vector-ref v nm)))))

(<define> (get_ESM v e s m)
  (<let> ((v (<lookup> v)))
  (<=> e ,(<lookup> (vector-ref v ne)))
  (<=> s ,(<lookup> (vector-ref v ns)))
  (<=> m ,(<lookup> (vector-ref v nm)))))

(<define> (get_AESM v a e s m)
  (<let> ((v (<lookup> v)))
  (<=> a ,(<lookup> (vector-ref v na)))
  (<=> e ,(<lookup> (vector-ref v ne)))
  (<=> s ,(<lookup> (vector-ref v ns)))
  (<=> m ,(<lookup> (vector-ref v nm)))))

(<define> (get_AB2ESM v a b2 e s m)
  (<let> ((v (<lookup> v)))
  (<=> a  ,(<lookup> (vector-ref v na)))
  (<=> b2 ,(<lookup> (vector-ref v nb2)))
  (<=> e  ,(<lookup> (vector-ref v ne)))
  (<=> s  ,(<lookup> (vector-ref v ns)))
  (<=> m  ,(<lookup> (vector-ref v nm)))))

(<define> (get_AES v a e s)
  (<let> ((v (<lookup> v)))
  (<=> s ,(<lookup> (vector-ref v ns)))
  (<=> e ,(<lookup> (vector-ref v ne)))
  (<=> a ,(<lookup> (vector-ref v na)))))

(<define> (get_CES v c e s)
  (<let> ((v (<lookup> v)))
  (<=> c ,(<lookup> (vector-ref v nc)))
  (<=> e ,(<lookup> (vector-ref v ne)))
  (<=> s ,(<lookup> (vector-ref v ns)))))

(<define> (get_ACES v a c e s)
  (<let> ((v (<lookup> v)))
  (<=> s ,(<lookup> (vector-ref v ns)))
  (<=> e ,(<lookup> (vector-ref v ne)))
  (<=> c ,(<lookup> (vector-ref v nc)))
  (<=> a ,(<lookup> (vector-ref v na)))))

(<define> (get_FEBH v f e b h)
  (<let> ((v (<lookup> v)))
  (<=> f ,(<lookup> (vector-ref v nf)))
  (<=> e ,(<lookup> (vector-ref v ne)))
  (<=> b ,(<lookup> (vector-ref v nb)))
  (<=> h ,(<lookup> (vector-ref v nh)))))

(<define> (get_FEBB2H v f e b b2 h)
  (<let> ((v (<lookup> v)))
  (<=> f  ,(<lookup> (vector-ref v nf)))
  (<=> e  ,(<lookup> (vector-ref v ne)))
  (<=> b  ,(<lookup> (vector-ref v nb)))
  (<=> b2 ,(<lookup> (vector-ref v nb2)))
  (<=> h  ,(<lookup> (vector-ref v nh)))))

(<define> (get_ACESB v a c e s b)
  (<let> ((v (<lookup> v)))
  (<=> s ,(<lookup> (vector-ref v ns)))
  (<=> e ,(<lookup> (vector-ref v ne)))
  (<=> c ,(<lookup> (vector-ref v nc)))
  (<=> a ,(<lookup> (vector-ref v na)))
  (<=> b ,(<lookup> (vector-ref v nb)))))

(<define> (get_QACESB v q a c e s b)
  (<let> ((v (<lookup> v)))
  (<=> q ,(<lookup> (vector-ref v nq)))
  (<=> s ,(<lookup> (vector-ref v ns)))
  (<=> e ,(<lookup> (vector-ref v ne)))
  (<=> c ,(<lookup> (vector-ref v nc)))
  (<=> a ,(<lookup> (vector-ref v na)))
  (<=> b ,(<lookup> (vector-ref v nb)))))

(<define> (get_QAESBB2 v q a e s b b2)
  (<let> ((v (<lookup> v)))
  (<=> q  ,(<lookup> (vector-ref v nq )))
  (<=> s  ,(<lookup> (vector-ref v ns )))
  (<=> e  ,(<lookup> (vector-ref v ne )))
  (<=> a  ,(<lookup> (vector-ref v na )))
  (<=> b  ,(<lookup> (vector-ref v nb )))
  (<=> b2 ,(<lookup> (vector-ref v nb2)))))

(<define> (get_H v h)
  (<let> ((v (<lookup> v)))
  (<=> h ,(<lookup> (vector-ref v nh)))))

(<define> (get_C v c)
  (<let> ((v (<lookup> v)))
  (<=> c ,(<lookup> (vector-ref v nc)))))

(<define> (get_CS v c s)
  (<let> ((v (<lookup> v)))
    (<=> c ,(<lookup> (vector-ref v nc)))
    (<=> s ,(<lookup> (vector-ref v ns)))))

(<define> (get_A v a)
  (<let> ((v (<lookup> v)))
  (<=> a ,(<lookup> (vector-ref v na)))))

(<define> (get_AA v aa)
  (<let> ((v (<lookup> v)))
  (<=> aa ,(<lookup> (vector-ref v naa)))))

(<define> (get_E v e)
  (<let> ((v (<lookup> v)))
  (<=> e ,(<lookup> (vector-ref v ne)))))

(<define> (get_F v f)
  (<let> ((v (<lookup> v)))
  (<=> f ,(<lookup> (vector-ref v nf)))))

(<define> (get_M v m)
  (<let> ((v (<lookup> v)))
  (<=> m ,(<lookup> (vector-ref v nm)))))

(<define> (get_AF v a f)
  (<let> ((v (<lookup> v)))
    (<=> a ,(<lookup> (vector-ref v na)))
    (<=> f ,(<lookup> (vector-ref v nf)))))

(<define> (set_EB v e b)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v ne) (<lookup> e))
    (<set> (vector-ref v nb) (<lookup> b))))

(<define> (set_EBB2 v e b b2)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v ne ) (<lookup> e))
    (<set> (vector-ref v nb ) (<lookup> b))
    (<set> (vector-ref v nb2) (<lookup> b2))))

(<define> (set_SM v s m)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v ns) (<lookup> s))
    (<set> (vector-ref v nm) (<lookup> m))))

(<define> (set_AES v a e s)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v na) (<lookup> a))
    (<set> (vector-ref v ne) (<lookup> e))
    (<set> (vector-ref v ns) (<lookup> s))))

(<define> (set_AB2ES v a b2 e s)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v na)  (<lookup> a))
    (<set> (vector-ref v nb2) (<lookup> b2))
    (<set> (vector-ref v ne)  (<lookup> e))
    (<set> (vector-ref v ns)  (<lookup> s))))

(<define> (set_ESM v e s m)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v nm) (<lookup> m))
    (<set> (vector-ref v ne) (<lookup> e))
    (<set> (vector-ref v ns) (<lookup> s))))

(<define> (set_ACES v a c e s)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v na) (<lookup> a))
    (<set> (vector-ref v ne) (<lookup> e))
    (<set> (vector-ref v nc) (<lookup> c))
    (<set> (vector-ref v ne) (<lookup> s))))

(<define> (set_ACE v a c e)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v na) (<lookup> a))
    (<set> (vector-ref v ne) (<lookup> e))
    (<set> (vector-ref v nc) (<lookup> c))))

(<define> (set_QAE v q a e)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v nq) (<lookup> q))
    (<set> (vector-ref v na) (<lookup> a))
    (<set> (vector-ref v ne) (<lookup> e))))

(<define> (set_ACESB v a c e s b)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v na) (<lookup> a))
    (<set> (vector-ref v ne) (<lookup> e))
    (<set> (vector-ref v nc) (<lookup> c))
    (<set> (vector-ref v ne) (<lookup> s))
    (<set> (vector-ref v nb) (<lookup> b))))

(<define> (set_QAESBB2 v q a e s b b2)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v nq ) (<lookup> q ))
    (<set> (vector-ref v na ) (<lookup> a ))
    (<set> (vector-ref v ne ) (<lookup> e ))
    (<set> (vector-ref v ne ) (<lookup> s ))
    (<set> (vector-ref v nb ) (<lookup> b ))
    (<set> (vector-ref v nb2) (<lookup> b2))))

(<define> (set_ES v e s)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v ne) (<lookup> e))
    (<set> (vector-ref v ns) (<lookup> s))))

(<define> (set_CE v c e)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v nc) (<lookup> c))
    (<set> (vector-ref v ne) (<lookup> e))))

(<define> (set_C v c)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v nc) (<lookup> c))))

(<define> (set_A v a)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v na) (<lookup> a))))

(<define> (set_E v e)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v ne) (<lookup> e))))

(<define> (set_AA v aa)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v naa) (<lookup> aa))))

(<define> (set_B2E v b2 e)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v nb2) (<lookup> b2))
    (<set> (vector-ref v ne)  (<lookup> e))))

(<define> (set_F v f)
  (let ((v (<lookup> v)))
    (<set0> (vector-ref v nf) (<lookup> f))))

(<define> (set_FS v f s)
  (let ((v (<lookup> v)))
    (<set0> (vector-ref v nf) (<lookup> f))
    (<set> (vector-ref v ns) (<lookup> s))))

(<define> (set_S v s)
  (let ((v (<lookup> v)))
    (<set> (vector-ref v ns) (<lookup> s))))

(compile-prolog-string
"
touch_A0([[0|_]]).
touch_A0([[_,_,#t]|L]) :- touch_A0(L).
touch_AA([]).
touch_AA([#t|L]) :- touch_AA(L).

touch_A(V) :-
  get_A(V,A),
  touch_A0(A),
  get_AA(V,AA),
  touch_AA(AA).

isCplx(X) :- X==#t.

ifc(G,E,X,Y) :-
 catch(G,E,X),
 (var(E) -> Y ; true).
 
tf(E) :- (E==#t;E==#f) -> true ; throw(E).
tfc(E) :- (E==#t;E==#f;E==c;E=softie(_)) -> true ; throw(E).
tt(E) :- E==#t -> true ; throw(E).
ff(E) :- 
  (E == #t -> throw(bug_should_not_throw_true) ; throw(E)).

check_tail(Tail) :- 
   (Tail=#t ; Tail = #f ; Tail=label(G,0)) -> true ; 
   throw(a_cc_tail_with_args_not_at_cc_call). 

reference([HC,HV],[_,V,_,X,E]) :-
  var(V) -> set(V,1) ; 
  (
     VV is V + 1, 
     set(V,VV),
     vhashq_ref(HV,X,[_,_,Etags|_]),
     Enew is Etags /\\ \\E,
     set(Etags,Enew)
  ).   

eql_a(X,X).

first  ([_,_,#t|_]).
isFirst([_,_,X |_]) :- X==#t.

force_variable([[#t|_]|_]).

new_tag2([HC,HV],X,XX,Tag,Etag) :-
  new_e(Etag),
  Tag=[XX,_,_,X,Etag],
  vhashq_cons  (HV,Tag,Etag),
  vhashql_cons (HC,Etag,Tag).

get_e_tag(X,[HC,HV],F,Ex,Tag,Etags) :-
  (
    vhashq_ref(HV,X,[XX,E,Etags]) -> 
       (XX=[_,_,FF],(FF == #f -> true ; (F=FF -> true ; set(FF,#f)))) ;
    (
      Etags=0,
      new_e(E),
      XX=[_,_,F],
      vhashq_cons(HV,X,[XX,E,Etags]),
      vhashql_cons(HC,E,X)
    )
  ),
  new_tag2([HC,HV],X,XX,Tag,Etag),
  EE is Etags \\/ Etag,
  Ex is E     \\/ Etag,
  set(Etags,EE).

-extended.
get_tags_from_bits(H,0,[]) :- !.
get_tags_from_bits((H,[HC,HV]),N,LL) :-
   maskoff(N,E,NN),
   (vhash_ref(HC,E,X) -> 
      (LL=[X|L],get_tags_from_bits(H,NN,L)) ;
    get_tags_from_bits(H,NN,LL)).

-extended.
m((Q,F(H)),[X|L]) :- F(H,X),m(Q,L).
m(_,[]).


add_var_f(X,V,F,Tag) :-
  add_var_f(X,0,V,F,Tag).

add_var_f(X,S,V,FF,Tag) :-
   get_FEBB2H(V,F,E,B,B2,H),
   get_e_tag(X,H,FF,Ex,Tag,Etags),
   EE  is E  \\/ Ex,
   BB  is B  \\/ Ex,
   BB2 is B2 \\/ Ex,
   set_EBB2(V,EE,BB,BB2),
   
   reference(H,Tag),
   (0 =:= Ex /\\ S  -> true       ; force_variable(Tag)),    
   (0 =:= Ex /\\ B2 -> first(Tag) ; true),
   Edeps is Etags /\\ B,
   get_tags_from_bits(H,Edeps,Deps),
   m(reference(H),Deps).

add_var(X,V,Tag) :-
  add_var(X,0,V,Tag).

add_var(X,S,V,Tag) :-
   get_FEBB2H(V,F,E,B,B2,H),
   get_e_tag(X,H,F,Ex,Tag,Etags),
   EE  is E  \\/ Ex,
   BB  is B  \\/ Ex, 
   BB2 is B2 \\/ Ex, 
   set_EBB2(V,EE,BB,BB2),
   reference(H,Tag),
   (0 =:= Ex /\\ S  -> true       ; force_variable(Tag)),    
   (0 =:= Ex /\\ B2 -> first(Tag) ; true),
   Edeps is Etags /\\ B,
   get_tags_from_bits(H,Edeps,Deps),
   m(reference(H),Deps).

find_all_vars(H,0 ,[]).
find_all_vars(H,Es,L) :-
   H=[HC,HV],
   maskoff(Es,E,EEs),
   vhashql_ref(HC,E,V),
   (
     var(V)    -> (L=[V|LL] , find_all_vars(H,EEs,LL));
     find_all_vars(H,EEs,L)
   ).

add_miss(_,[],[],E,E).
add_miss(H,[V|Xs],Ys,E,EE) :-
   new_tag2(H,V,_,Tag,Etag),
   first(Tag),
   reference(H,Tag),
   E1 is E \\/ Etag,
   Ys=[Tag|YYs],
   add_miss(H,Xs,YYs,E1,EE).

add_missing_variables(_,[],_,E,E).
add_missing_variables(H,[[E,V]|Es],EE,Ein,Eout) :-
   Ex is EE /\\ \\E,
   find_all_vars(H,Ex,Vars),
   add_miss(H,Vars,V,Ein,EE2),
   add_missing_variables(H,Es,EE,EE2,Eout).

addvs([],I,I,L,L).
addvs([[[S,V,Q],N,F]|Tags],I,II,L,LL) :-
  N==1 -> addvs(Tags,I,II,L,LL) ;
  (
    new_var(V,Q,S),
    tr(newvar,Newvar),
    L=[[newvar,V]|L2],
    I2 is I + 2,
    addvs(Tags,I2,II,L2,LL)
  ).

mmtr([],[]).
mmtr([[X|Y]|LA],[[XX|Y]|LB]) :-
   (tr(X,XX);binop(X,XX);unop(X,XX)),
   mmtr(LA,LB).

mg(E,X,_,_,_) :- 
  var(X) -> throw(predicate_must_have_proper_tail(E)).
mg(E,[],true,N,N).
mg(E,[X],imprint(X,0),I,N) :- !, N is I + 1.
mg(E,[X|L],(imprint(X,0),U),I,N) :-
  II is I + 1,
  mg(E,L,U,II,N).

listp(X) :- var(X) -> (!, fail).
listp([X|Y]) :- listp(Y).
listp([]).

tail(Tail,LL,LLL) :-
  Tail = #t -> 
   (tr(cc,CC),LLL=[[CC]|LL]) ; 
  LLL=LL. 

push_v(N,V) :-
   get_SM(V,S,M),
   SS is S + N,
   MM is max(M,SS),
   set_SM(V,SS,MM).

get_line(U,X,Xin,N) :-
   get_line(U,X,Xin,0,N).

get_line([(A,B)|U],[A|X],[B|Xin],I,N) :- !,
  II is I + 1,
  get_line(U,X,Xin,II,N).

get_line([A|U],[A|X],[_|Xin],I,N) :- !,
  II is I + 1,
  get_line(U,X,Xin,II,N).

get_line(_,[],[],I,I).


%:- dynamic([compile_goal/2]).
compile_goal.

collect_F.
newtag_F.

")

(all-defined-out)
