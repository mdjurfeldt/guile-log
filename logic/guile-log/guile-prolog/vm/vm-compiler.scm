(define instr (@@ (logic guile-log guile-prolog vm vm-pre) instr))

#|
The question is if we can design a system for prolog programs
f -> X=1,f.    => this system works quite nicely
f -> X=1,f,X=2 => the second f needs to be a new function

A=[V3,V2,V1]
F1->X,F2,V,X

A=[V4,V3,V2,V1]
F2->X,F3,V,X

In tail call context we will create variale set records, that can garabage 
collect, now the A vector will grow but that's fine, this means that the cc
code needs a field to check the A link and restore the stack in that case
for this we need the V3 identity which is carried over as a closure variable
(A,Code,Constants) can be imbeded in the actual function

(let ((A,Code,Constant))
  (lambda () (ins A Code Constant)))

The cc object has cc=(V,i,f)
The p  object has p =(P,i,f)

This is quite effective in that if 

If we store a state then all function frames that are active will need 
to be freezed. What we can do is to simply link frames in the stack and 
traverse them backwards at state storage. This means that when we set a
a variable we simply check if the frame is looked or not. One option is
then to set all variables at function creation. But the first version
will be a create all at startup variant for now.
|#

#|
TODO:
1.  Trace failure thunk, so that we know if we can remove a variable, DONE
2.  Cut, DONE
3.  true
4.  false/fail
5.  Expression evaluator.
6.  Creation of variables will be done if not already present
7.  Implement all the unification idioms
8.  Add attribute logic to the unification
9.  Add a clause compiler
10. Add a clauses compiler
11. Debug or, also make sure that -> and \+ works as it should
12. Add macro logic, not numbers in op's.
13. handle negative unary operator

What about storing the control stack in a linked assoc
newfram: link on, id
unwind:  link of, id

plus,   link on, id
sucess, link off, id

onece
link on id
link off id

We also want to transfer the cut between frames, unwinds have a cut in them
via the instruction this means that we need to store both the failure thunk
as well as the cc when we enter a function.

Another optimization is to check at cc if failure in is failure out. if so
we now that we can change the p thunk to the actual memory location and skip
a rounderm to the guile vm, this will speed upp execution quite alot.

what need to be stored per session: 
  p,cc variables
what need to be transfered at a call:
  ctrl-stack, stack, next-instruction, narg
what is constant 
  instructions,constants,dimensions,nvar

( #(narg  ctrl-stack stack)
   variables
   .
   (nvar instructions constants))

variables is the most difficult part to maintain
|#

(define vm-bytevector
  (let ((bv (make-u32vector 1 0)))
    (u32vector-set! bv 0 175)
    bv))

;; Setting up the vm engine meta information
(define vm-model 
  ((@@ (logic guile-log code-load) gp-make-vm-model) vm-bytevector))

((@@ (logic guile-log code-load) gp-setup-prolog-vm-env)
 (@@ (logic guile-log macros   ) dls)
 (@@ (logic guile-log code-load) *delayers*)
 (@@ (logic guile-log umatch   ) *unwind-hooks*)
 (@@ (logic guile-log umatch   ) *unwind-parameters*)
 (lambda x  #t)
 (lambda () #f)
 (@@ (logic guile-log macros   ) gp-not-n) 
 (@@ (logic guile-log macros   ) gp-is-delayed?)
 vm-model)

(define (make-vm-function a b)
  ((@@ (logic guile-log code-load) gp-custom-fkn)
   vm-model a b))

(define (list->vector l)
  (let ((v (make-vector ((@ (guile) length) l) #f)))
    (let lp ((l l) (i 0))
      (if (pair? l)
	  (begin
	    (vector-set! v i (car l))
	    (lp (cdr l) (+ i 1)))))
    v))

(define (mk-instructions l)
  (let ((v (make-vector ((@ (guile) length) l) #f)))
    (let lp ((l l) (i 0))
      (if (pair? l)
	  (begin
	    (vector-set! v i (car l))
	    (lp (cdr l) (+ i 1)))))
    v))

(define (pp x y)
  ;(format #t "~a ~a~%" x y)
  y)


(define (get-extended)
  (let lp ((l (combine_ops *extended*)) (r '()))
    (if (pair? l)
	(lp (cdr l) (cons* (cadr (car l)) (car (car l)) r))
	r)))

(define (parse-extended)
  (set! unify_operators (combine_ops *extended*)))

(define pack-start (@@ (logic guile-log code-load) pack-start))
(define (name-it x) (set-procedure-property! x 'name 'anonymous) x)
(<define> (compile_to_fkn code f)
  (<code> (parse-extended))
  (<var> (stackSize constants l nvar nsvar tvar narg)
    (compile_goal code l stackSize narg constants #f)
    (get_varn nvar)
    (get_vart tvar)
    (max_svarns nsvar)
    (<recur> lp ((l (<scm> l)) (o '()))
	(<<match>> (#:mode -) (l)
	 ((x . l) (lp l ((@ (guile) append) ((@ (guile) reverse) x) o)))
	 (()    
	  (let ((instructions (pp 'instructions: ((@ (guile) reverse) o)))
		(nvar         (pp 'nvar:      (<lookup> nvar)))
		(tvar         (pp 'tvar:      (<lookup> tvar)))
		(nsvar        (pp 'nsvar:     (<lookup> nsvar)))
		(narg         (pp 'narg:      (<lookup> narg)))
		(stackSize    (pp 'size:      (<lookup> stackSize)))
		(constants    (pp 'constants: (map car (<scm> constants)))))
            <cut>
	    (<=> f
		 ,(name-it
		   (make-vm-function 
		    (cons (+ narg 4) (+ stackSize nsvar))
		    (pack-start nvar
				stackSize
				(mk-instructions instructions)
				(list->vector constants)
				tvar))))))))))

(define (get-mod c)
  (let ((a (procedure-property c 'module)))
    (if a
	a
	(module-name (current-module)))))

(<define> (compile_to_meta stx code meta)
  (<code> (parse-extended))
  (<var> (stackSize constants l nvar nsvar tvar narg)
    (compile_goal code l stackSize narg constants #f)
    (get_varn nvar)
    (get_vart tvar)
    (max_svarns nsvar)
    (<recur> lp ((l (<scm> l)) (o '()))
	(<<match>> (#:mode -) (l)
	 ((x . l) (lp l ((@ (guile) append) ((@ (guile) reverse) x) o)))
	 (()    
	  (let* ((instructions (pp 'instructions: ((@ (guile) reverse) o)))
		 (nvar         (pp 'nvar:      (<lookup> nvar)))
		 (tvar         (pp 'tvar:      (<lookup> tvar)))
		 (nsvar        (pp 'nsvar:     (<lookup> nsvar)))
		 (narg         (pp 'narg:      (<lookup> narg)))
		 (stackSize    (pp 'size:      (<lookup> stackSize)))
		 (constants    (pp 'constants: (map car (<scm> constants))))
		 (constants-r  (map (lambda (c)
				      (let ((c (<lookup> c)))
					#`(@@ 
					   #,(map
					      (lambda (x)
						(datum->syntax stx x))
					      (get-mod c))
					   #,(datum->syntax 
					      stx
					      (procedure-name c)))))
				    constants)))
            <cut>
	    (<=> meta
		 ,#`(make-vm-function 
		     '(#,(+ narg 4) . #,(+ stackSize nsvar))
		     `#,(pack-start nvar
				    stackSize
				    (mk-instructions instructions)
				    #`,(vector #,@constants-r)
				    tvar)))))))))

(<define> (eval_to_meta stx code meta)
  (<var> (stackSize constants l nvar nsvar tvar narg)
    (compile_goal code l stackSize narg constants #f)
    (get_varn nvar)
    (get_vart tvar)
    (max_svarns nsvar)
    (<recur> lp ((l (<scm> l)) (o '()))
	(<<match>> (#:mode -) (l)
	 ((x . l) (lp l ((@ (guile) append) ((@ (guile) reverse) x) o)))
	 (()    
	  (let* ((instructions (pp 'instructions: ((@ (guile) reverse) o)))
		 (nvar         (pp 'nvar:      (<lookup> nvar)))
		 (tvar         (pp 'tvar:      (<lookup> tvar)))
		 (nsvar        (pp 'nsvar:     (<lookup> nsvar)))
		 (narg         (pp 'narg:      (<lookup> narg)))
		 (stackSize    (pp 'size:      (<lookup> stackSize)))
		 (constants    (pp 'constants: (map car (<scm> constants)))))
	    <cut>
	    (<=> meta
		 ,(make-vm-function 
		   `(,(+ narg 4) . ,(+ stackSize nsvar))
		   (pack-start nvar
			       stackSize
			       (mk-instructions instructions)
			       constants
			       tvar)))))))))

(define readline_term_str (@@ (logic guile-log guile-prolog interpreter)
			      readline_term_str))

(compile-prolog-string "
generate_lambda(X,F) :-
   readline_term_str(X,T,[variables(V),variable_names(N)]),
   compile_to_fkn(T,F).
")


(compile-prolog-string "
generate_stx(STX,X,F) :-
   catch(
     (
       readline_term_str(X,T,[variables(V),variable_names(N)]),
       compile_to_meta(STX,T,F)
     ),E,
     (
       write(error(E)),nl,F=1
     )).  
")

(define-syntax-rule (define-prolog-fkn n code-string)
  (define n (letrec ((n 
		      (let ((g (prolog-run 1 (f) 
				  (generate_lambda code-string f))))
			(if (null? g)
			    (error "failed compile")
			    (car g)))))
	      n)))

(define (cur x)
  (map (lambda (s) (datum->syntax x s))
       (module-name (current-module))))

(define-syntax define-prolog 
  (lambda (x)
    (syntax-case x ()
      ((_ n code-string)
       #`(define n (let ((g (lambda ()
			      #,(let ((g (prolog-run-rewind 1 (meta) 
					  (generate_stx x 
							(syntax->datum 
							 #'code-string)
							meta))))
				  (if (null? g)				   
				      (begin
					(warn "compilation failed of " 
					      (syntax->datum #'n))
					#'(lambda x 
					    (error 
					     (format #f "not implemented ~a"
						     'n))))
				      (if (not (eq? (car g) 1))
					  (car g)
					  #'(lambda x 
					      (error
					       (format #f "not implemented ~a"
						       'n)))))))))
		     (letrec ((n (lambda x
				   (let ((gg (g)))
				     (module-set! (resolve-module '#,(cur x))
						  'n gg)
				     (apply gg x)))))
		       n)))))))

(define (mk-lam f)
  (lambda ()
    (lambda (s p cc cut scut x)
      (apply f s p cc cut scut x))))
    
(define-syntax-rule (with a code)
  code
  #;
  (let ((n (vector-ref (times) 0)))
    (let ((res code))
      (pk `(,a ,(/ (- (vector-ref (times) 0) n) 1e9)))
      res)))

(define (comma x y) (vector (list #{,}# x y)))
(define (mockalambda source? s pat code)
  (let* ((Cut  (gp-var! s))
	 (SCut (gp-var! s))
	 (rhs  (if *extended-body* 
		   (comma extended_off
			  (comma (vector (list with_cut Cut SCut)) 
				 code))
		   (comma (vector (list with_cut Cut SCut)) code)))
	 (lhs  (vector (cons* mockalambda Cut SCut pat)))
	 (oth  (with 'cccc (compile-prolog s pat code source?
					   (cons* #t #t
						  (get-extended)))))
	 (all  (vector (list :- lhs rhs))))
    ;(<pp> (s (lambda () #f) (lambda () #f) (lambda x x)) all)
    (if source?
	#`(let ((o #,oth))
	    (list 
	     (car o)
	     #,(let ((comp
		      (with-fluids ((*current-stack* s))
		        (with '____
			      (prolog-run-rewind 
			       1 (meta) 
			       (compile_to_meta source? all meta))))))
		 ((@ (guile) catch) #t
		   (lambda ()
		     (if (pair? comp)
			 #`(mk-lam #,(car comp))
			 (begin
			   (warn "failed compiling")
			   #'(lambda () (error "misscompiled")))))
		   (lambda a
		     (pk a)
		     (warn "failed compiling")
		     #'(lambda () (error "misscompiled")))))
	     (cadr o)))
	(let ((comp
	       (prolog-run-rewind 
		1 (meta) 
		(eval_to_meta all meta))))
	  (if (pair? comp)
	      (let ((f (car comp)))
		(lambda ()
		  (lambda (s p cc cut scut x)
		    (apply f s p cc cut scut x))))
	      (begin
		(warn "failed compiling")
		#f))))))
      
	  
(set! (@@ (logic guile-log match) mockalambda) mockalambda)
	  
      
