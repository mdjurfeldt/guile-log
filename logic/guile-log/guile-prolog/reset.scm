(define-module (logic guile-log guile-prolog reset)
  #:use-module (logic guile-log macros)
  #:use-module (logic guile-log prompts)
  #:use-module (logic guile-log prolog names)
  #:export (reset shift))

(define (continuation? x)
  (and
   (procedure? x)
   (object-property x 'cont)))

(define (touch x)
  (set-object-property! x 'cont #t))

(define reset-tag (list 'reset-tag))
(define (mk-goal goal ball)
  (<lambda> ()
    (let* ((g.b (<cp> (cons goal ball)))
           (g   (car g.b))
           (b   (cdr g.b)))
    (call g)
    (<abort> reset-tag #t b (<lambda> () <fail>)))))

(<define> (reset ball)
 (<abort> reset-tag #f ball
  (<lambda> (fail?) (when (not fail?)))))

(<define> (reset goal ball cont)
 (if (continuation? (<lookup> goal))
     (goal ball cont)
     (<prompt> reset-tag #f (mk-goal goal ball)
      (letrec ((h (lambda (ball cont)
                   (<lambda> (t next kk final? ball-from)
                    (if final?
                        (<or>
                         (<=> ball ball-from)
                         (<re-prompt> reset-tag kk (h ball cont) '()))
                        (<if> (<=> ball ball-from)
                              (let ((k
                                     (touch
                                      (<lambda> (ball cont)
                                       (<re-prompt> reset-tag kk 
                                                    (h ball cont) '(#f))))))
                                (<with-fail>
                                 (lambda ()
                                   (<re-prompt> S P CC reset-tag kk
                                                (h ball cont) '(#t)))
                                 (<=> cont k)))
                              (next)))))))
        ( ball cont)))))
