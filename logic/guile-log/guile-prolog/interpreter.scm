(define-module (logic guile-log guile-prolog interpreter)
  #:use-module ((logic guile-log) #:select 
                (<clear> <define> <let> <let*> <=> <lookup> <match> <fail>
                         <cut> <wrap> <state-ref> <state-set!> <continue>
                         <code> <scm> <stall> <case-lambda> <cc> <set>
			 <<match>> <recur>
                         <newframe> <=> <and> <lambda> <apply> <pp> S P))
  #:use-module (logic guile-log guile-prolog hash)
  #:use-module (logic guile-log guile-prolog fluid)
  #:use-module (logic guile-log vlist)
  #:use-module (logic guile-log persistance)
  #:use-module (logic guile-log prolog persist)
  #:use-module (logic guile-log iinterleave)
  #:use-module (ice-9 match)
  #:use-module (ice-9 readline)
  #:use-module (ice-9 rdelim)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log memoize)
  #:use-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log prolog names)
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log prolog parser)
  #:use-module (logic guile-log prolog namespace)
  #:use-module (logic guile-log prolog closed)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (logic guile-log dynamic-features)
  #:use-module (logic guile-log guile-prolog attribute)
  #:use-module (logic guile-log guile-prolog project)
  #:use-module (logic guile-log guile-prolog dynamic-features)
  #:use-module (logic guile-log guile-prolog memoize)
  #:use-module (logic guile-log prolog global)
  #:use-module (logic guile-log guile-prolog copy-term)

  #:export (prolog-shell conversation leave read-prolog user_ref user_set 
                         stall thin_stall))

(define-named-object -all- (make-fluid false))
(define *cc* (@@ (logic guile-log run) *cc*))
(<wrap> add-fluid-dynamics *cc*)
(<wrap> add-fluid-dynamics -all-)
(define-named-object *once*     (gp-make-var #f))
(define-named-object -nsol-     (make-fluid #f))
(define-named-object -mute?-    (make-fluid #f))
(define-named-object -rec?-     (make-fluid #f))
(define-named-object -nonrec?-  (make-fluid #f))
(<wrap> add-fluid-dynamics -mute?-)

(define-named-object *user-data* (make-fluid vlist-null))
(<wrap> add-vhash-dynamics *user-data*)

(<define> (user_set a v)
  (<code> (fluid-set! *user-data* (vhash-cons (<lookup> a)
                                              (<scm> v)
                                              (fluid-ref *user-data*)))))
(<define> (user_ret a v)
  (<=> (_ . v) ,(vhash-assoc (<lookup> a) (fluid-ref *user-data*))))

(define (usr-ref x) 
  (cdr (vhash-assoc x (fluid-ref *user-data*))))
(define (usr-set! x v) 
  (fluid-set! *user-data* (vhash-cons x v (fluid-ref *user-data*))))

;; Silence the prolog compiler
(define conversation1   #t)
(define conversation2   #t)
(define loop            #f)
(define finish          #f)
(define leave           #f)
(define solve           #t)
(define output_and_more #t)
(define consult         #t)
(define conversation    #t)
(define conversation_   #t)
(define conversation__  #t)
(define more            #t)
(define write_out       #t)
(define empty           #t)
(define hash_new        #t)
(define vtosym          #t)
(define vtosym_         #t)
(define vtosym4         #t)
(mk-sym finish)

(define (prolog-shell)
  ((@ (guile) catch) #t
   (lambda () 
     (<clear>)
     (prolog-run 1 () (loop))
     (format #t "leaving prolog~%"))
   (lambda x
     (format #t "System error~%~a~%RESTARTING~%" x)
     (prolog-shell))))
  
(define readline_term* (@ (logic guile-log guile-prolog readline)
			  readline_term))
(define readline       (@ (logic guile-log guile-prolog readline)
			  readline))
(define -n-            (@ (logic guile-log guile-prolog readline)
		 	  -n-))
(define lold #f)
(define-named-object *usr-state* (make-fluid #f))

(define (tosym x)
  (cond
   ((string? x) (string->symbol x))
   ((procedure? x) (procedure-name x))
   (else x)))

(define stall
  (<case-lambda>
   (()
    (<code> 
     (usr-set! 'stall-ret '())
     (fluid-set! *usr-state* S)
     (fluid-set! (@ (logic guile-log umatch) *current-stack*) S)
     (set! lold (<state-ref>)))
    (<stall>))

   (l
    (<let> ((n.x (let lp ((l l) (n '()) (r '()))
		   (if (pair? l)
		       (lp (cddr l) 
			   (cons (car l) n)
			   (cons (cadr l) r))
		       (cons n r)))))
      (add_env (car n.x) (cdr n.x))
      (<code> 
       (fluid-set! *usr-state* S)
       (fluid-set! (@ (logic guile-log umatch) *current-stack*) S)
       (set! lold (<state-ref>)))
      (<stall>)))))

(<define> (thin_stall)
  (<stall>))

(define-named-object env (make-fluid '()))
(<define> (add_env n x)
  (fluid-guard-dynamic-object env)
  (<code> (let lp ((r (fluid-ref env)) (n (<scm> n)) (x (<scm> x)))
	    (if (pair? n)
		(lp (cons (cons (tosym (car n))
				(car x)) r) (cdr n) (cdr x))
		(fluid-set! env r)))))
	    
(<define> (unify_env n x)
  (<recur> lp ((n n) (x x))
    (<<match>> (#:mode - #:name unify_env) (n x)
       ((n . ns) (x . xs)
	(<let*> ((r (assoc (tosym (<lookup> n)) (fluid-ref env))))
	   (if r
	       (<=> x ,(cdr r))
	       <cc>)
	   (lp ns xs)))

       (_ _ <cc>))))
	       

(define *states* (make-hash-table))
(define *persister* (make-persister))

(define (read-prolog port env)
  (define nn?   #f)
  (define all?  #f)
  (define fail? #f)
  (define mute? #f)
  (define help? #f)
  (define save  #f)
  (define load  #f)
  (define cont  #f)
  (define ref   #f)
  (define set   #f)
  (define old   #f)
  (define clear #f)
  (define endl  #f)  
  (define profile #f)
  (define newp  #f)
  (let* 
      ((l (with-input-from-port port
	    (lambda ()
      	      (let lp ((first? #t) (ch (peek-char)) (r '()) (dot-cont? #f)
                       (i 0))
		(when (eof-object? ch)
		      (set! ch #\.))
		(match ch
                  (#\{
                   (read-char)
                   (lp #f (peek-char) (cons ch r) #f (+ i 1)))
                  
                  (#\}
                   (read-char)
                   (lp #f (peek-char) (cons ch r) #f (- i 1)))
                  
		  (#\space 
		   (read-char)
		   (if first? 
		       (lp first? (peek-char) r           #f i)
		       (lp first? (peek-char) (cons ch r) #f i)))
                  
                  (#\.
                   (read-char)
		   (if first?
                       (let ((action ((@ (guile) read))))
                         (cond 
                          ((integer? action)
                           (set! nn? action))
                          ((pair? action)
                           action)
                          (else
                           (case action
			     ((profile pr)
			      (set! profile #t))
			     ((newp)
			      (set! *persister* (make-persister))
			      (set! old #t))
			     ((setp)
			      (persist-state *persister* ((@ (guile) read)))
			      (set! old #t))
			     ((savep)
			      (save-persists *persister*)
			      (set! old #t))
			     ((loadp)
			      (load-persists *persister*)
			      (set! old #t))
			     ((refp)
			      (persist-restate *persister* 
					       ((@ (guile) read)))
			      (set! old #t))
			     ((rec)       (begin
					    (fluid-set! -rec?-    #t)
					    (fluid-set! -nonrec?- #f)))
			     ((nonrec)    (begin
					    (fluid-set! -rec?-    #f)
					    (fluid-set! -nonrec?- #t)))
                             ((mute m)    (fluid-set! -mute?- #t))
                             ((unmute um) (fluid-set! -mute?- #f))
                             ((all *)     (set! all?  #t))
                             ((once)      (set! nn? 1))
                             ((h help)    (set! help? #t))
                             ((s save)    (set! save  ((@ (guile) read))))
                             ((l load)    (set! load  ((@ (guile) read))))
                             ((c cont)    (set! cont  #t))
                             ((ref)       (set! ref ((@ (guile) read))))
                             ((set)    (set! set (list ((@ (guile) read))
                                                       ((@ (guile) read)))))
                             ((clear)  (set! clear #t) (set! endl #\.))
                             ((lo lold) 
                              (set! old #t)
                              (if lold (<state-set!> lold)))
                             (else 
                              (set! fail? #t)))))

			 (cond
			  (endl
			   (if #f #f))
			  ((or fail? help?)
			   #f)
			  ((pair? action)
			   action)
			  ((or load save cont ref set old)
			   #t)
			  (else
			   (lp #t (peek-char) '() #f i))))
                       (if (not (= i 0))
                           (lp #f (peek-char) (cons ch r) #f i) 
                           (let ((ch (peek-char)))
                             (if dot-cont?
                                 (lp #f ch (cons #\. r) #f i)
                                 (if (not (char-whitespace? ch))
                                     (if (eq? ch #\.)
                                         (lp #f ch (cons #\. r) #t i)
                                         (lp #f ch (cons #\. r) #f i))
                                     (list->string ((@ (guile) reverse)
                                                    (cons #\. r)))))))))

		  (#\,
		   (read-char)
		   (if first?
		       (cons ch (string->list (read-line)))
                       (lp #f (peek-char) (cons ch r) #f i)))
		     
		  (_
		   (read-char)
		   (lp #f (peek-char) (cons ch r) #f i))))))))

    (cond
     (clear
      `((@ (logic guile-log) begin)
	((@ (logic guile-log) <clear>))
	((@ (guile) if) #f #f)))
     (old
      '((@ (guile) if)  #f #f))
     (ref
      `((@@ (logic guile-log guile-prolog interpreter) usr-ref) ,ref))

     (set
      `((@ (guile) begin)
        ((@@ (logic guile-log guile-prolog interpreter) usr-set!) ,@set)
        ((@ (guile) if)  #f #f)))

     (load
      `((@ (guile) let*)
         ((x          
           ((@ (guile) hash-ref)
            (@@ (logic guile-log guile-prolog interpreter)
                *states*)
            ',load))
          (state ((@ (guile) car) x))
          (s     ((@ (guile) cdr) x)))
         ((@ (logic guile-log) <state-set!>) state)
         ((@ (guile) fluid-set!)
          (@ (logic guile-log umatch) *current-stack*)
          s)
         ((@ (guile) if)  #f #f)))

     (save
      `((@ (guile) begin)
         ((@ (guile) hash-set!) 
          (@@ (logic guile-log guile-prolog interpreter) *states*)
          ',save
          ((@ (guile) cons)
           ((@ (logic guile-log) <state-ref>))
           ((@ (guile) fluid-ref)
            (@@ (logic guile-log guile-prolog interpreter) *usr-state*))))
         ((@ (guile) if)  #f #f)))

     (cont
      `((@ (guile) begin)
	((@ (guile) fluid-set!)
	 (@@ (logic guile-log guile-prolog interpreter) -nsol-)
	  ,(cond
	    (all? '(@ (logic guile-log iso-prolog) true))
	    (nn? nn?)
	    (else
	     '(@ (logic guile-log iso-prolog) false))))
	((@ (logic guile-log prolog error) redo-wrapper)
	 ((@ (guile) lambda) () ((@ (logic guile-log) <continue>))))
	((@ (guile) if) #f #f)))

     (fail? 
      '((@ (guile) begin)
         ((@ (guile) format) #t "wrong-input of '.' action ~%")
         ((@ (guile) if)  #f #f)))

     (help?
      (format #t "
HELP FOR PROLOG COMMANDS
---------------------------------------------------------------------
(.n           )             try to find n solutions
(.all    | .* )             try to find all solutions
(.once   | .1 )             try to find one solution
(.mute   | .m )             no value output is written.
(.unmute | .um)             output values is written.
---------------------------------------------------------------------
(.save   | .s ) <ref>       associate current state with name ref
(.load   | .l ) <ref>       restore associate state with name ref
(.cont   | .c )             continue the execution from last stall point
(.lold   | .lo)             restore the last state at a stall
(.clear       )             clear the prolog stack and state
(.rec         )             enable rational tree handling
(.unrec       )             disable rational tree handling
---------------------------------------------------------------------
(.ref         ) <ref>       get value of reference user variable ref
(.set         ) <ref> <val> set user variable ref to value val
---------------------------------------------------------------------
(.setp        ) <key>       associate current state to key
(.refp        ) <key>       instate state referenced by key
(.savep       )             save all referenced states to disk
(.loadp       )             load new referenced states from disk
")
      '((@ (guile) if) #f #f))
        
      
     ((string? l)
      (let ((str l))
        (when (eq? (string-ref str 0) #\,)
          (string-set! str 0 #\space)
          (set! str (string-append str " "))
          (with-input-from-string (string-trim str)
            (lambda ()
              ((@@ (system repl command) meta-command) repl)))
          (set! str "do[#f]"))
       (let ((lam (lambda (x) 
                    (if profile 
                       `((@ (statprof) statprof)
                           ((@@ (guile) lambda) () ,x))
                        x))))

        (lam `((@ (guile) begin)
            ((@@ (logic guile-log prolog run) prolog-run-0)
             (@@ (logic guile-log guile-prolog interpreter) 
                 conversation1)                 
             ,str 
             ,(cond
               (all? '(@ (logic guile-log iso-prolog) true))
               (nn? nn?)
               (else
                '(@ (logic guile-log iso-prolog) false))))
           ((@ (guile) if) #f #f))))))
     (else
        `((@ (guile) with-fluids)
            (((@ (system base language) *current-language*)
              ((@ (system base language) lookup-language) 'scheme)))
                ,l)))))

(<define> (wrap_frame) (<let> ((fr (<newframe>))) <cc>))

(<define> (readline_term T O)
  (<let*> ((n  (fluid-ref -n-))
	   (pr (if (= n 1) "-? " (format #f "(~a)? " n)))
	   (cr (let lp ((n (string-length pr)))
		 (if (= n 1)
		     " "
		     (string-append "." (lp (- n 1)))))))
     (readline_term* pr cr T O)))

(define (readline_term_str s p cc Str T O)
  (with-input-from-string Str
    (lambda ()
      (let ((port (current-input-port)))
        (read_term s p cc port T O)))))
	  
(<define> (ftof X Y I H)
   (<match> (#:mode + #:name 'ftof) (X Y)
     (#(XL) #(YL) (<cut> (vtosym4 XL YL I H)))
     (_     _     (<cut> <fail>))))

(<wrap> add-fluid-dynamics -nsol-)
(<wrap> add-fluid-dynamics env)

(<define> (wrap_namespace x y yy)
  (<let> ((x (<lookup> x)))
    (<code> (gp-set! y (make-namespace 
                        yy
                        (namespace-ns      x)
                        (namespace-local? x)
                        (namespace-lexical? x))
		     S))))

(<define> (set_once) (<set> *once* P))
(<define> (if_once Y Z)
   (if (eq? (<lookup> *once*) P)
       (goal-eval Y)
       (goal-eval Z)))

(compile-prolog-string
"
leave :- throw(leave).

loop :- catch(conversation,X,(write(X),nl,loop)).

conversation        :-
  fluid_guard_dynamic_object(scm[-n-]),
   (      
      conversation__
   ).

conversation__ :- 
  do[(fluid-set! -n- (+ (fluid-ref -n-) 1))],
  conversation_.

conversation_       :- 
   (
    fluid_guard_dynamic_object(scm[-all-]),
    do[ (fluid-set! -all- false) ],
    nl,readline_term(T,[variables(V),variable_names(N)]),
    consult(T,V,N,false,false)     
   ) ; conversation_.

conversation1(X,All) :-  
  backtrack_dynamic_object(scm[*globals-map*]),
  fluid_guard_dynamic_object(
      scm[-n-],
      scm[-nsol-], 
      scm[-all-],
      scm[*globals-map*],
      scm[*var-attributator*],
      scm[env],
      scm[*cc*],
      scm[*user-data*],
      scm[-mute?-]),
  
  state_guard_dynamic_object( 
      scm[-n-], 
      scm[-nsol-], 
      scm[-all-], 
      scm[*globals-map*],
      scm[*var-attributator*],
      scm[env],
      scm[*cc*],
      scm[*user-data*],
      scm[-mute?-]),
  
  wrap_frame,  
  conversation2(X,All).

tree :- when[(fluid-ref -rec?-)]    
            ->  (do[(fluid-set! -rec?- #f)],rational_trees);
        when[(fluid-ref -nonrec?-)] 
            ->  (do[(fluid-set! -rec?- #f)],non_rational_trees);
        true.

conversation2(X,All) :- 
   do[(fluid-set! -n- (+ (fluid-ref -n-) 1))],   
   readline_term_str(X,T,[variables(V),variable_names(N)]),
   unify_env(N,V),
   add_env(N,V),
   tree,
   consult(T,V,N,All).

consult(X,V,N,All)     :-
   do[(fluid-set! -nsol- (<lookup> All))],
   catch((solve(V,N,X) ; (nl,write(no),nl,fail)),finish, fail).

add_N(H,[],[]).
add_N(H,[V|Vs],[N|Ns]) :-
   (var(V) -> vhashq_cons(H,V,N) ; true),
   add_N(H,Vs,Ns).

vtosym(X,Y,N,L,LL) :- 
 make_vhash(H),add_N(H,X,N),
 make_fluid(0,I),
 rec_analyze(X),vtosym4(X,Y,H,I),
 rec_analyze(L),vtosym4(L,LL,H,I).

%vtosym_(X,Y,_,_) :- write([1,X,Y]),nl,fail.

vtosym_(X,Y,H,I) :-
  var(X)         -> (!, (vhashq_ref(H,X,Y)->true ; hash_new(X,Y,H,I)));
  namespace_p(X) -> (!, namespace_val(X,XX),
                        vtosym4(XX,YY,H,I),
                        wrap_namespace(X,Y,YY)) ; fail.

vtosym_({X},{Y},H,I) :- !,vtosym_(X,Y,H,I).

vtosym_([X|XL],[U|UL],H,I) :- 
  !,vtosym4(X,U,H,I), vtosym4(XL,UL,H,I).

vtosym_([],[],_,_) :- !.
vtosym_(X,Y,_,_)   :- atomic(X) -> (!,X=Y) ; fail.
vtosym_(X,Y,H,I) :-
  X =.. [F|A], !,
  vtosym_(F,G,H,I),vtosym_(A,B,H,I), Y =.. [G|B].
 

vtosym_(F,G,H,I) :- ftof(F,G,H,I).

vtosym_(X,X,_,_) :- !.

hash_new(X,Y,H,I) :-
  Y = scm[(string->symbol (format #f \"X~a\" (fluid-ref (<lookup> I))))],
  fluid_set(I,scm[(+ 1 (fluid-ref (<lookup> I)))]),
  vhashq_cons(H,X,Y).

output_and_more(V,N,More) :-
      (when[(eq? (fluid-ref -mute?-) #t)] -> more ;
        (
         (V==[] -> (write(\"yes\"),nl) ; (once((copy_term(V,U,L),
                                                vtosym(U,VV,N,L,LL))),
                                            setenv,write_out(VV,N,LL))),
          (More=#t -> more ; throw(finish))
        )
      ).

%write_out(X,Y) :- write(writeout(X,Y)),nl,fail.
write_out(VV,N,L) :- write_out0(VV,N,Empty), write_out_trail(L,Empty).
write_out0([],[],_).
write_out0([V|Vs],[N|Ns],Empty) :-
  V == N -> write_out0(Vs,Ns,Empty) ;
  Empty=1,nl,write(\"   \"),write(N),write(\" = \"),write(V),
  write_out2(Vs,Ns,Empty).

write_out2([],[],_).
write_out2([V|Vs],[N|Ns],Empty) :-
  V == N -> write_out2(Vs,Ns,Empty) ;
  Empty=1,write(','),nl,write(\"   \"),write(N),write(\" = \"),write(V),
  write_out2(Vs,Ns,Empty).

write_out_trail([A|B], Empty) :- 
  (Empty==1 -> write(',') ; true), nl, 
  write(\"   \"),write(A), write_out_trail(B,1).

write_out_trail([],Empty) :- 
  Empty==1 -> write('.'), nl ; true.

wstall :- stall,tree.

more :- 
  scm[(fluid-ref -all-)]  == true -> fail           ; 
  (
    N=scm[(fluid-ref -nsol-)], 
    (
      N == true   -> fail ;
      integer(N)  -> (N > 1 -> (do[(fluid-set! -nsol- (- (<lookup> N) 1))], 
                                fail)
                             ; wstall ,fail)               ;
      readline(\"more (y/n/a/s) > \",Ans),
      (
        Ans == \"y\" -> fail                               ;
        Ans == \"n\" -> throw(finish)                      ;
        Ans == \"a\" -> scm[(fluid-set! -all- true)]==1    ;
        Ans == \"s\" -> stall, fail                        ;
        write(\" wrong input\"),nl,more
      )
    )
  ).

empty :- peek_char(X),char_code(X,Code),Code==10->get_char(_);true.

h([(?- X)|L],(X,LL)) :- h(L,LL).
h([],true).

solve(V,N,X) :- 
   'new-machine',
   set_once,
   (expand_term_0((?- X),Y) -> (h(Y,YY),YY) ; X),
   once(project_the_attributes(V)),
   if_once(output_and_more(V,N,#f),output_and_more(V,N,#t)).
.
")

(define recurs-map (make-fluid '()))
(<define> (rec_analyze x) 
  (<code> (fluid-set! recurs-map (make-hash-table)))
  ((with-atomic-frec
    (<lambda> (x)
      (rec-action
       (<lambda> (x)
         (<code> (hashq-set! (fluid-ref recurs-map) 
			     (<lookup> x) #t)))
       x)))
   x))

(<define> (vtosym-guard x s)
  (<let> ((a (car x)))
    (if (or (gp-pair? a s) (pair? a) (vector? a) (struct? a))
	(<let> ((b (hashq-ref (fluid-ref recurs-map) a #f)))
	  (if (eq? b #t)
	      (<code> (hashq-set! (fluid-ref recurs-map) a (cadr x)))
	      <cc>)
	  (<cc> (list a)))
	(<and>
	 (<cc> #f)))))

(<define> (doit-at-rec x)
 (<apply> (<lambda> (x y a b)
	     (<let> ((r (hashq-ref (fluid-ref recurs-map) (<lookup> x) #f)))
	       (if r
		   (rec= y  r)
		   (<=> y '*))))
	  x))

(define vtosym4 
  (with-atomic-rec
   (rec-00 vtosym_ vtosym-guard doit-at-rec)))

(<define> (setenv)
  (<code> (fluid-set! (@@ (logic guile-log vset) sfluid) S))) 


(<define> (attr_sym lam x y h i)
  ((<lookup> lam) x y h i vtosym_))






