(define-module (logic guile-log guile-prolog interleave)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (logic guile-log)
  #:export (or_i and_i or_union))

(define-syntax-rule (mk-i or_i f)
  (<define> (or_i . x)
    (<let> ((x (map (lambda (x) (<lambda> () (goal-eval x))) x)))
      ((@@ (logic guile-log interleave) f) x))))

(mk-i  or_i      f-interleave)
(mk-i  and_i     and-interleave)
(mk-i  or_union  f-interleave-union)
