(define-module (logic guile-log guile-prolog fiber)
  #:use-module (logic guile-log guile-prolog engine)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (logic guile-log fiber)
  #:use-module (logic guile-log)
  #:export (with_fibers
            spawn_fiber
            sleep
            make_channel
            get_message
            put_message))

(define with_fibers
  (<case-lambda>
   ((goal ret)
    (<with-fibers> (<lambda> () (goal-eval goal)) ret))
   ((goal)
    (<var> (v) (with_fibers goal v)))))

(define spawn_fiber
  (<case-lambda>
   ((goal fiber)
    (<spawn-fiber> fiber
      (<lambda> ()
        (<var> (t engine v)
           (engine_create t goal engine)
           (engine_next engine v)
           <fail>))))
   ((goal)
    (<var> (v)
           (spawn_fiber goal v)))))


(<define> (sleep x) (<sleep> x))

(<define> (make_channel . l) (<apply> <make-channel> l))

(<define> (put_message ch x) (<put-message> ch x))
(<define> (get_message ch x) (<get-message> ch x))
