(define-module (logic guile-log guile-prolog set)
  #:use-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log prolog swi)
  #:use-module (logic guile-log dynamic-features)
  #:use-module (logic guile-log prolog names)
  #:use-module (logic guile-log prolog variant)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (logic guile-log guile-prolog attribute)
  #:use-module (logic guile-log attributed)
  #:use-module (logic guile-log)
  #:use-module ((logic guile-log umatch)
                #:select (gp-att-data gp-attvar-raw? gp-lookup gp-get-attr
                                      set-attribute-cstor!))
  #:use-module (ice-9 set complement)
  #:use-module ((logic guile-log umatch) #:select (gp-lookup gp->scm))
  #:use-module (logic guile-log vset)
  #:use-module (ice-9 set set)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (logic guile-log prolog goal)
  #:use-module ((logic guile-log prolog goal-transformers) 
		#:select (mk-prolog-biop-tr 
			  mk-scheme-biop 
			  mk-scheme-unop
			  mk-prolog-biop 
			  ->			  
			  s a))
  #:export 
  (∩ ∪ ∖ ∈ ⊕ ⊂ ⊆ ≡ ⊃ ⊇ ⊈ ⊄ ⊉ ⊅ ᶜ ∈ ∉ ∋ ∅ Ω ⊔ ∖∖
    set_to_list set_to_kvlist to_set
    list_to_set
    mapset
    complex_set_p is_set
    advanced-set-printer set_unify set-lookup
    use_complex_map use_simple_map ppp
    n u o m c subset subseteq mk mem
    subset-scm subseteq-scm))

(define ∅     vsetx-empty)
(define mk∅   #f)
(define Ω     #f)
(define empty vsetx-empty)

(define (set-lookup s x)
  (set-lookup-
   s
   (lambda () x)
   (lambda (ss p y) y)
   x))


(fluid-set! (@@ (ice-9 set complement) *equal?*)
            (lambda (x y)
              (vsetx-equal? #f x y)))

(<define> (is_set x)
  (if (cset? (<lookup> x))
      <cc>
      <fail>))

(<define> (ele0 x)
  (<<match>> (#:mode -) ((<lookup> x))
   (#(("op2-" k v))
    (<and>
      (<values> (kk) (ele k))
      (<values> (vv) (ele v))
      (<cc> (vosetx-union S (mk-kvx (<scm> kk) (<lookup> vv)))
            vsetx-empty)))
   (#(("-" k v))
    (<and>
      (<values> (kk) (ele k))
      (<values> (vv) (ele v))
      (<cc> (vosetx-union S (mk-kvx (<scm> kk) (<lookup> vv)))
            vsetx-empty)))
    (x
     (<and>
      (<values> (xx) (ele x))
      (if (equal? xx #:null)
          (<cc> vsetx-empty 
                vsetx-empty )
          (<cc> vsetx-empty 
                (vosetx-union S (mk-kx (<scm> xx)))))))))

(<define> (ele1 x)
  (<<match>> (#:mode -) ((<lookup> x))
    (#((,prolog-and x y))
     (<and>
      (<values> (x1 y1) (ele0 x))
      (<values> (x2 y2) (ele1 y))
      (<cc> (vosetx-union S x1 x2) (vosetx-union S y1 y2))))

    (x (ele0 x))))
      


(<define> (ele x)
  (<<match>> (#:mode -) ((<lookup> x))
    (#(#:brace v)
     (<and>
      (<values> (a c) (ele1 v))
      (<cc> (make-set- a vsetx-empty (vosetx-difference S c a) #f #f))))

    ((a . b)
     (<and>
      (<values> (aa) (ele a))
      (<values> (bb) (ele a))
      (<cc> (cons aa bb))))

    (#(l)
     (<and>
      (<values> (a) (ele l))
      (<cc> (vector a))))

    (x (<cc> x))))


(<define> (make-2 f x y)
  (<values> (x) (ele x))
  (<values> (y) (ele y))
  (<ret> (f S x y)))


(<define> (make-1 f x)
  (<values> (x) (ele x))
  (<ret> (f S x)))

(define (fail) #f)
(define (ccx s p x) x)
(eval-when (compile eval load)
  (begin
    (set! (@@ (logic guile-log prolog goal-functors) scm-brace-eval)
      (lambda (s x)
        (ele s 
             (lambda () #f)
             (lambda (s p x) x)
             x)))

    (set! (@@ (logic guile-log prolog goal) brace-stx-scm)
      (lambda (stx x)
        #`(ele S fail ccx               
               `#,((@@ (logic guile-log prolog var) arg) stx x))))))

(define (make2 s f x y)
  (make-2 s (lambda () #f) (lambda x #t) f x y))

(define (make1 s f x)
  (make-1 s (lambda () #f) (lambda x #t) f x))

(define-syntax-rule (mk2 nm1 nm2) 
  (define-syntax-rule (nm1 x y) 
    (make2 S nm2 
           (gp-lookup x S)
           (gp-lookup y S))))

(define-syntax-rule (mk1 nm1 nm2) 
  (define-syntax nm1
    (lambda (x)
      (syntax-case x ()      
	((_ x) 
	 #'(nm2 S (gp-lookup x S)))
	((_ s x)
	 #'(nm2 s (gp-lookup x s)))
	((_ . _)
	 (error "wrong number of arguments to complement"))
	(_ #'nm2)))))

(define complex_set_p (make-fluid #t))
(<wrap> add-fluid-dynamics complex_set_p)

(<define> (use_complex_map) (<code> (fluid-set! complex_set_p #t)))
(<define> (use_simple_map)  (<code> (fluid-set! complex_set_p #f)))

#;
(define (wrap name f fo)
  (let ((ff
         (lambda (s . x)
           (apply (if (fluid-ref complex_set_p)
                      fo
                      f)
                 s (map (lambda (x) (set-lookup s x)) x)))))
    (set-procedure-property! ff 'name name)
    (set-object-property! ff 'prolog-functor-type #:scm)
    ff))

(define-syntax-rule (wrap2 fn f fo)
  (define-syntax-rule (fn x y)
    ((if (fluid-ref complex_set_p)
         fo
         f)
     S
     (set-lookup S x)
     (set-lookup S y))))

#;(define (wrap1 name f fo)
  (let ((ff
         (lambda (s . x)
           (apply (if (fluid-ref complex_set_p)
                      fo
                      f)
                 s (map (lambda (x) (set-lookup s x)) x)))))
    (set-procedure-property! ff 'name name)
    (set-object-property! ff 'prolog-functor-type #:scm)
    ff))

(define-syntax-rule (wrap1 fn f fo)
  (define-syntax-rule (fn x)
    ((if (fluid-ref complex_set_p)
         fo
         f)
     S
     (set-lookup S x))))


(define-syntax-rule (wrap f fo x y)
  ((if (fluid-ref complex_set_p)
       fo
       f)
   S
   (set-lookup S x)
   (set-lookup S y)))

;;(mk2 set_union_        (wrap '∪ vsetx-union        vosetx-union        ))
;;(mk2 set_union2_       (wrap '⊔ vsetx-union        vosetx-union        ))
;;(mk2 set_addition_     (wrap '⊕ vsetx-addition     vosetx-addition     ))
;;(mk2 set_difference_   (wrap '∖ vsetx-difference   vosetx-difference   ))
;;(mk2 set_intersection_ (wrap '∩ vsetx-intersection vosetx-intersection ))

(wrap2 set_union_        vsetx-union        vosetx-union        )
(wrap2 set_union2_       vsetx-union        vosetx-union        )
(wrap2 set_addition_     vsetx-addition     vosetx-addition     )
(wrap2 set_difference_   vsetx-difference   vosetx-difference   )
(wrap2 set_intersection_ vsetx-intersection vosetx-intersection )


(eval-when (compile load eval)
  (mk-scheme-biop 'yfx "⊔" tr-⊔ ⊔ set_union2_        s s)
  (mk-scheme-biop 'yfx "∪" tr-∪ ∪ set_union_         s s)
  (mk-scheme-biop 'yfx "⊕" tr-⊕ ⊕ set_addition_      s s)
  (mk-scheme-biop 'yfx "∖" tr-∖ ∖ set_difference_     s s)
  (mk-scheme-biop 'yfx "∖∖" tr-∖∖ ∖∖ set_difference_   s s)
  (mk-scheme-biop 'yfx "∩" tr-∩ ∩ set_intersection_  s s))

;;(mk1 set_complement (wrap 'ᶜ vsetx-complement vosetx-complement))
(wrap1 set_complement vsetx-complement vosetx-complement)

(mk-scheme-unop 'xf "ᶜ" tr-ᶜ ᶜ set_complement s)
;(define ᶜ (wrap 'ᶜ vsetx-complement vosetx-complement))

(<define> (memb x y)
  (when (vsetx-in (<lookup> x) (set-lookup S y))))
(<define> (equiv x y)
  (when (vsetx-equal? (set-lookup S x) (set-lookup S y))))
(<define> (subset-scm x y)
  (when (wrap vsetx-subset<  vosetx-subset< x y)))
(<define> (subseteq-scm x y)
  (when (wrap  vsetx-subset<=  vosetx-subset<= x y)))

(<define> (supset x y)   (subset y x))
(<define> (supseteq x y) (subseteq y x))

(define subseteq #f)
(define subset   #f)

(<define> (nsubseteq x y) (<not> (subseteq x y)))
(<define> (nsubset   x y) (<not> (subset   x y)))
(<define> (nsupseteq x y) (<not> (supseteq x y)))
(<define> (nsupset   x y) (<not> (supset   x y)))

(eval-when (compile load eval)
  (mk-prolog-biop 'xfx "⊆" tr-⊆ ⊆ subseteq  a a)
  (mk-prolog-biop 'xfx "⊂" tr-⊂ ⊂ subset    a a)
  (mk-prolog-biop 'xfx "⊇" tr-⊇ ⊇ supseteq  a a)
  (mk-prolog-biop 'xfx "⊃" tr-⊃ ⊃ supset    a a)
  (mk-prolog-biop 'xfx "⊈" tr-⊈ ⊈ nsubseteq a a)
  (mk-prolog-biop 'xfx "⊄" tr-⊄ ⊄ nsubset   a a)
  (mk-prolog-biop 'xfx "⊉" tr-⊉ ⊉ nsupseteq a a)
  (mk-prolog-biop 'xfx "⊅" tr-⊅ ⊅ nsupset   a a)
  (mk-prolog-biop 'xfx "≡" tr-≡ ≡ equiv     a a)
  (mk-prolog-biop 'xfx "∈" tr-∈ ∈ mem       a a)
  (mk-prolog-biop 'xfx "∉" tr-∉ ∉ nmem      a a)
  (mk-prolog-biop 'xfx "∋" tr-∋ ∋ memrev    a a))

(define (uu x)
  (if (equal? x '(nonvalue))
      #f
      x))

(define (mq x)

  (if (pair? x)
      (cons (car x) (uu (cdr x)))
      (if x
          (cons (get-k x) (uu (get-v x)))
          x)))
          
(<define> (nmem x y) (<not> (mem x y)))
          
(<define> (mem x y)
  (<match> (#:mode -) ((<lookup> x))
    (#(("op2-" key val))
     (<cut>
      (<let> ((key (<lookup> key)))
        (if (<var?> key)
            (<recur> lp ((l (vsetx->assoc S (set-lookup S y))))
               (if (pair? l)
                   (<or>
                    (<=> (key . val) ,(mq (car l)))
                    (lp (cdr l)))
                   <fail>))
            (<=> (_ . val) ,(mq (vsetx-member (mk-kx key)
                                              (set-lookup S y))))))))
    (key
     (if (<var?> key)
         (<recur> lp ((l (vsetx->assoc S (set-lookup S y))))
           (if (pair? l)
               (<or>
                (<=> (key . _) ,(mq (car l)))
                (lp (cdr l)))
               <fail>))       
         (if (vsetx-member (mk-kx key) (set-lookup S y))
             <cc>
             <fail>)))))

(<define> (memrev x y) (mem y x))


(<define> (set_to_list   x y) (<=> y ,(make1 S vsetx->list (set-lookup S x))))
(<define> (set_to_kvlist xx y)
 (<var> (x)
   (build xx x)        
   (let ((x (set-lookup S x)))
     (if (and (cset? x)
              (eq? (struct-ref x 0) ∅)
              (eq? (struct-ref x 2) ∅))
         (<=> y ())
         (<=> y ,(make1 S
                        (lambda (s x)
                          ((@ (guile) map)
                           (lambda (x)
                             (vector (list op2- (get-k x) (uu (get-v x)))))
                           (vsetx->kvlist s x)))
                        x))))))


(<define> (to_set x y) (<=> y ,(make1 S ->vsetx (set-lookup S x))))

(<define> (rev l ll) (<=> ll ,((@ (guile) reverse) (<scm> l))))

(<define> (complement_as_set x y pred)
  (let ((s.p (call-with-values (lambda () (complement-as-set (set-lookup S x) empty)) cons)))
    (<=> y    ,(cdr s.p))
    (<=> pred ,(car s.p))))

(<define> (set_as_complement x y pred)
  (<=> y ,(set-as-complement (set-lookup S x) empty (<lookup> pred))))

(<define> (as_set x y pred)
  (let ((p.s (call-with-values
                 (lambda () (as-finite-set (set-lookup S x) empty))
               cons)))
    (<=> y    ,(cdr p.s))
    (<=> pred ,(car p.s))))

(<define> (set_as x y pred)
  (<=> y ,(finite-as-set (set-lookup S x) empty
                         (<lookup> pred))))
  



(compile-prolog-string 
"
build(X,Y) :- 
   Y is (X ∪scm[∅]).

f(X,Y) :- Y is X + X.

list_to_set(L,S) :-
  rev(L,LL),
  list_to_set(LL,∅,S).

list_to_set([],S,S).

list_to_set([X|L],S,SS) :-
  (
    (\\+var(X), X = K-V) ->
      S2 is {K-V} ∪ S;
      S2 is {X}   ∪ S
  ),
  list_to_set(L,S2,SS).

mapset(S,Map,SS) :- 
    findall(V, (_-V ∈ (Map ∩ S), V \\= #f),L), 
    list_to_set(L,SS).

")

(compile-prolog-string
"
  get_set(X,Y) :-
     get_attr(X,set_unify,Y) ->
        true;
     is_set(X) ->
        Y=X;
     fail.


  varx(X) :- var(X),\\+(attvar(X),test_attr(X,nonvar)).

  fall2([],_,_).
  fall2([F(K,A)|L],Y,Op) :- mem(K-B,Y),!,call(Op,A,B),fall2(L,Y,Op).  

  fall2g([],_,_,_).
  fall2g([F(K,A)|L],Y,Op,U) :- mem(K-B,Y),!, call(Op,A,B|U), fall2g(L,Y,Op,U).  

  fall2h([],_,_,L,_,_) :- cc(L).

  fall2h([F(K,A)|U],Y,Op,L,Pred,H) :-
    mem(K-B,Y),!,
    [LL] <= Op(A,B,L,Pred,H),
    fall2h(U,Y,Op,LL,Pred,H).

  fall1([],_).
  fall1([F(K,A)|L],Op) :- call(Op,A),fall1(L,Op).

  fall1v([],L,L,_).
  fall1v([F(K,A)|U],L,LL,Op) :- 
     Op(A,L,L1),
     Op(K,L1,L2),
     fall1v(U,L2,LL,Op).

  forall2(X,Y,Op) :-
    set_to_kvlist(X,L),
    fall2(L,Y,Op).

   forall2g(X,Y,Op,U) :-
     set_to_kvlist(X,L),
     fall2g(L,Y,Op,U).

   forall2h(X,Y,Op,L,Pred,H) :-
      set_to_kvlist(X,U),
      fall2h(U,Y,Op,L,Pred,H).

  forall1(X,Op) :-
    set_to_kvlist(X,L),
    fall1(L,Op).

  forall1v(X,L,LL,Op) :-
    set_to_kvlist(X,U),
    fall1v(U,L,LL,Op).

  set_unify(V,N,\"xhash\") :- !,
    cc(scm[(xhash V N)]).

  set_unify(V,Op) :-
    (get_attr(V,set_unify,X) -> true; X=V),         
    as_set(X,X1,_),         
    forall1(X1,Op),
    complement_as_set(X,XX,_),
    forall1(XX,Op).  

  unify((A ⊔ B),U) :- !,
     varx(U) ->
        U=A⊔B;
     fail.

  unify(X,W) :- varx(W),!,
     get_set(X,Y),
     W ⊆ Y.

  unify(X,W) :- 
     get_set(X,Y),
     X==Y.

  unify(X,A∖∖B) :- !,
   (
     (varx(A),varx(B)) ->
        (
          A = (_ ∖∖ B) ⊔ X,
          B = _ ∖∖ X
        );
     varx(A) ->
        (
           get_set(B,BB) ->
              (
                 scm[mk∅] = X ∩ BB,                
                 A =  X
              );
           (
              scm[mk∅] = X ∩ B,
              C = (_ ∖∖ B) ⊔ X,
              unify(C,A)
           )
        );
     varx(B) ->
        (
           get_set(A,AA) ->
             (
               X ⊆ A,
               D is A ∖ X,
               B = (_ ∖∖ A) ⊔ D
             );
           (
             C = (D ∖∖ X) ⊔ X,
             B = (D ∖∖ X) ⊔ (E ∖∖ X),
             unify(C,A)
           )
        );
     (
        C = (D ∖∖ X) ⊔ X,
        F = (D ∖∖ X) ⊔ (E ∖∖ X),           
        unify(C,A),
        unify(F,B)
     )
   ).

  unify(X,A∖B) :- !,
    (
     (varx(A),varx(B)) ->
        (
          A = (_ ∖∖ B) ⊔ X,
          B = _ ∖∖ X
        );
     varx(A) ->
        (
           get_set(B,BB) ->
              (
                 scm[∅] is X ∩ BB,                
                 A = (_ ∖∖ BB) ⊔ X
              );
           (
              scm[∅] is X ∩ B,
              C = (_ ∖∖ B) ⊔ X,
              unify(C,A)
           )
        );
     varx(B) ->
        (
           get_set(A,AA) ->
             (
               X ⊆ A,
               D is A ∖ X,
               B = (_ ∖∖ A) ⊔ D
             );
           (
             C = (D ∖∖ X) ⊔ X,
             B = (D ∖∖ X) ⊔ (E ∖∖ X),
             unify(C,A)
           )
        );
     (
        C = (D ∖∖ X) ⊔ X,
        F = (D ∖∖ X) ⊔ (E ∖∖ X),           
        unify(C,A),
        unify(F,B)
     )
   ).

    
  unify(X,A∪W) :- !,
    (
     (varx(A),varx(W)) ->
        (
          A ⊆ X,
          unify(X,A∪W)
        );
     varx(A) ->
        (
           get_set(W,Y) ->
              (
                 Q is X ∖ Y,                
                 D ⊆ Y,
                 A is D ∪ Q
              );
           (
              A ⊆ X,
              unify(X,A∪W)
           )
        );
     varx(W) ->
        unify(X,W∪A);
     (
        S ⊆ X,
        unify(X,A∪S),
        unify(S,W)
     )
    ).

  unify(X,A ⊔ W) :- !,
    (
     (varx(A),varx(W)) ->
        (
           A⊆X,
           W is X ∖ A
        );
     varx(A) ->
        (
           get_set(W,Y) ->
              (
                 A is X ∖ Y
              );
           (
              A ⊆ X,
              Q is X ∖ A,
              unify(Q,W)
           )
        );
     varx(W) ->
        unify(X,W⊔A);
     (
        S ⊆ X,
        T is X ∖ S,
        unify(S,A),
        unify(T,W)
     )
    ).

  unify(X,A∩W) :- !;
    (
     (varx(A),varx(W)) ->
       (
          A = (B ∖∖ X) ⊔ X,
          W = (C ∖∖ A) ⊔ X
       );
     varx(A) ->
       (
          get_set(W,WW) ->
             (
               X ⊆ WW,
               A=(_ ∖∖ WW) ⊔ X
             );
          (
             A = (B ∖∖ X) ⊔ X,
             Q = (C ∖∖ A) ⊔ X,
             unify(Q,W)
          )
        );
     varx(W) ->
        unify(X,W∩A);
     (
       S = (B ∖∖ X) ⊔ X,
       T = (C ∖∖ S) ⊔ X,
       unify(S,A),
       unify(T,W)
     )
    ).


  set_unify_(V,W,Op) :- !,
    (get_attr(V,set_unify,X)->true ; X=V),
    (
       get_attr(W,set_unify,Y) ->
         (
           as_set(X,X1,_),         
           as_set(Y,Y1,_),      
           X1==Y1,
           forall2(X1,Y1,Op),
           complement_as_set(Y,YY,_),
           complement_as_set(X,XX,_),
           XX==YY,
           forall2(XX,YY,Op)
          );
       varx(W) -> 
         (Op==(=) -> put_attr(W,set_unify,X) ; fail);
       Op=(=) ->
         unify(X,W);
       fail
     ).

  tr0([],[],[],_).  

  tr0([K-V|U],M,S,CopyIt) :- V==#f,!,
      tr0(U,M,SS,CopyIt),
      ( SS=[] -> S=K ; S=(K,SS)).

  tr0([K-V|U],M,S,CopyIt) :-
      tr0(U,MM,S,CopyIt),
      ( CopyIt -> copy_term(V,VV) ; VV=V),
      (MM=[] -> M=(K-VV); M=((K-VV),MM)).

 
  tr(X,M,S,CopyIt) :-
      set_to_kvlist(X,U),
      tr0(U,MM,SS,CopyIt),
      (MM = [] -> M={} ; M={MM}),
      (SS = [] -> S={} ; S={SS}).

  combine({},{},{}) :- !.
  combine({},B ,B ) :- !.
  combine(B ,{},B ) :- !.
  combine({A},{B},{C}) :- combine0(A,B,C).

  combine0((A,B),C,(A,U)) :- !,
     combine(B,C,U).

  combine0(A,C,(A,C)).

%properties
  set_unify(\"nonvar\").

expand(Var,V,YY,Lam) :-
     as_set(V,X1,P1),
     (X1 is {} -> (M1={},S1={}); tr(X1,M1,S1,Lam)),
     complement_as_set(V,X2,P2),
     (X2 is {} -> (M2={},S2={}); tr(X2,M2,S2,Lam)),
     (
       P1 = #t ->
         (
            combine(M2,S2,MM),
            (
              M1 = {} ->
                (
                   MM = {} ->
                      Y = Ω;
                   Y = MMᶜ
                );
              (
                MM = {} ->
                   Y = (M1 ∪ Ω);
                Y = (M1 ∪ MMᶜ)
              )
            )
         );
       (
          M1 = {} ->
             (
                M2={} ->
                  Y=S1;
                Y = ((∅ ∖ M2) ∪ S1)
             );
          M2 = {} ->
             (
                S1 = {} ->
                  Y = M1;
                Y = (M1 ∪ S1)
             );
           (
              S1 = {} ->
                Y = (M1 ∖ M2);
              Y = ((M1 ∖ M2) ∪ S1)
           )            
      )
    ), 
    YY=mk(Y,Var).




%write, used to generate a representation that's printable
  set_unify(Var,V,YY,write) :-
     expand(Var,V,YY,false).

%cp
  set_unify(D,Lam,cp) :-
     cpx(D,Lam).
  set_unify(D,Lam,cp3) :-
     cpx3(D,Lam).

  set_unify(X,Y,#t) :- !,set_unify_(X,Y,=).
  set_unify(X,Y,#f) :- !,set_unify_(X,Y,==).
  set_unify(X,Y,Op) :- !,set_unify_(X,Y,Op).

  set_unify(X,L,LL,Op) :-
     Op=\"vars\",
     as_set(X,X1,_),
     forall1v(X1,L,L1,Op),
     complement_as_set(X,X2,_),
     forall1v(X2,L1,LL,Op).

  mk_set0([F(K,V)],F(K,VV),H,I,Op) :- !,
    Op(V,VV,H,I).

  mk_set0([],scm[#:null],H,I,Op).

  mk_set0([X|L],A,H,I,Op) :-
    Op(X,F(K,VV),H,I),
    mk_set(L,B,H,I,Op),
    A=(F(K,VV),B).

  mk_set(X,{A},H,I,Op) :-
    set_to_kvlist(X,L),    
    mk_set0(L,A,H,I,Op).


  gen([],L,L,_,_).
  gen([K-V|U],L,LL,Tok,Op) :-
    Op(V,L,L1,Tok),
    gen(U,L1,LL,Tok,Op).

%get_var from co-routines
  set_unify(V,L,LL,Tok,Op) :-
     Op=\"get_var\",!,
     get_attr(V,set_unify,X),
     as_set(X,X1,_),
     set_to_kvlist(X1,U1),
     gen(U1,L,L1,Tok,Op),
     complement_as_set(X,X2,_), 
     set_to_kvlist(X2,U2),
     gen(U2,L1,LL,Tok,Op).

%vtosym_/4 from interpreter
  set_unify(X,Y,H,I,Op) :-
    as_set(X,X1,P),
    set_to_kvlist(X,L),Op(L,Y,H,I).
%    mk_set(X1,X2,H,I,Op),
%    X3 is X2,
%    set_as(X3,A,P),
%    complement_as_set(X,XX,P),
%    mk_set(XX,A2,H,I,Op),
%    A3 is A2,
%    set_as_complement(A3,AA,P),
%    write(aa(X1,X2,X3,A,XX,A2,A3,AA)),nl,
%    Y is A ∪ AA.

%unifiable/3 :u from variant needed by when in coroutines
  set_unify(V,W,L,Pred,H,Op) :-
    get_attr(V,set_unify,X),
    (
      get_attr(W,set_unify,Y) ->
        (         
          as_set(X,X1,_),         
          as_set(Y,Y1,_),         
          X1==Y1,
          [L1] <= forall2h(X1,Y1,Op,L,Pred,H),
          complement_as_set(X,XX,_),
          complement_as_set(Y,YY,_),
          XX==YY,
          [L2] <= forall2h(XX,YY,Op,L1,Pred,H),
          !,
          cc(L2)
         );
      varx(W) ->
        cc([[V=W]|L]);
      fail
     ).

%=@= from variant
  set_unify(V,W,IX,IY,HX,HY,SX,SY,Op) :-
    (get_attr(V,set_unify,X) -> true; X=V),         
    (get_attr(W,set_unify,Y) -> true; Y=W),
    (
      var(Y) -> 
         fail;
      is_set(Y) ->
        (
          as_set(X,X1,_),         
          as_set(Y,Y1,_),         
          X1==Y1,
          forall2g(X1,Y1,Op,[IX,IY,HX,HY,SX,SY]),
          complement_as_set(X,X2,_),
          complement_as_set(Y,Y2,_),
          X2==Y2,
          forall2g(X2,Y2,Op,[IX,IY,HX,HY,SX,SY])
        );
      fail
    ).

  map_unify_wrap(X,V)   :- put_attr(V,set_unify,X).  
  map_unify_unwrap(V,X) :- raw_attvar(V,X).

  n(X,Y,Z) :- W is X ∩ Y, put_attr(Q,set_unify,W),Q=Z.
  u(X,Y,Z) :- W is X ∪ Y, put_attr(Q,set_unify,W),Q=Z.
  o(X,Y,Z) :- W is X ⊕ Y, put_attr(Q,set_unify,W),Q=Z.
  m(X,Y,Z) :- W is X ∖ Y , put_attr(Q,set_unify,W),Q=Z.
  c(X,Z)   :- W is Xᶜ    , put_attr(Q,set_unify,W),Q=Z.
  mk(X,Y)  :- call(W is scm[∅] ∪ X), put_attr(Z,set_unify,W),Y=Z.

  subgen([],S,S).

  subgen([(K-#f)|L],S,SS) :- !,
    (
       S1 is {K} ∪ S;
       S1 is S
    ),
    subgen(L,S1,SS).

  subgen([(K-V)|L],S,SS) :- !,
    (
       S1 is {K-V} ∪ S;
       S1 is S
    ),
    subgen(L,S1,SS).


  subseteq1(XX,SS) :-
     get_set(SS,S) ->
       (
         get_set(XX,X) ->
           'subseteq-scm'(X,S);
         varx(XX) ->
            (!,
             set_to_kvlist(S,L),!,
             reverse(L,LL),
             S0 is {},
             subgen(LL,S0,XX)
            );
         fail
       );
    (!,fail).
 

  subseteq1(A∖∖B,S) :-
      get_set(B,BB) ->
        (
          !,
          SS is S ∖ BB,
          subseteq(A,SS)
        ),
      fail.

  subseteq1(A⊔B,S) :-
     get_set(B,BB) ->
       (  
          !,
          subseteq(BN,S),
          SS is S ∖ BB,
          subseteq(A,SS)
       );
     fail.

  subseteq1(Q,S) :-
      subseteq1(U,S),
      mk(U,UU),
      UU = Q.

  subset1(X,S) :-
     get_set(X,Y) ->
        'subset-scm'(Y,S);
     (
       subseteq1(X,S),
       notequal(X,S)
     ).

  fc :-
     X is {1}, 
     Y is {2}, 
     Z is X ∩ Y, 
     W is Zᶜ,
     mk(∅,Q),
     do[(set! ∅   (gp-lookup Z S))],
     do[(set! mk∅ (gp-lookup Q S))],
     do[(set! Ω   (gp-lookup W S))].
")

(define subset   subset1)
(define subseteq subseteq1)

(<define> (notequal x y) (when (not (equal? x y))))


(<define> (set-lookup- x)
  (let lp ((x (gp-lookup x S)))
    (if (cset? x)
        (<cc> x)
        (cond
         ((gp-attvar-raw? x S)
          (let ((r (gp-get-attr x set_unify S)))
	    (if r
		(lp (gp-lookup r S))
		(<cc> x))))
	 
         ((vector? x)          
          (<var> (y)
            (<if> (call (vector (list is y x)))
                  (<cc> (<scm> y))
                  (<cc> x))))
         
         (else
          (<cc> x))))))

(fc #f (lambda () #f) (lambda x #t))

(<define> (ppp x) (<code> (pk (<lookup> x))))

(set! subseteq subseteq1)
(set! subset   subset1  )

(define (cp0 s x lam)
  (let* ((l (vsetx->assoc s x)))
    (let lp ((l ((@ (guile) reverse) l)) (u ∅))
      (if (pair? l)
          (lp (cdr l) (vosetx-union s (mk-kvx (car (car l))
                                            (lam (cdr (car l))))
                                    u))
          (if (cset? u)
              (struct-ref u 0)
              u)))))

(<define> (cp30 s x lam)
  (let* ((l (vsetx->assoc s x)))
    (let lp ((l ((@ (guile) reverse) l)) (u ∅) (q '()))
      (if (pair? l)
          (let ((v.l (lam (cdr (car l)))))
            (lp (cdr l)
                (vosetx-union s (mk-kvx (car (car l)) (car v.l)) u)
                ((@ (guile) append) (cdr v.l) q)))
          (<cc> (if (cset? u) (struct-ref u 0) u)
                q)))))

(define make-set (@@ (ice-9 set complement) make-set-))

(<define> (cpx d lam)
  (let ((d (<lookup> d)))
    (cond
     ((cset? d)
      (let ((a0 (struct-ref d 0))
            (a1 (struct-ref d 1))
            (a2 (struct-ref d 2))
            (p  (struct-ref d 3))
            (m  (struct-ref d 4)))
        (<cc> (make-set (cp0 S a0 lam) (cp0 S a1 lam) a2 p m))))
     ((set? d)
      (<cc> (cp0 S d lam))))))
        
(<define> (cpx3 d lam)
  (let ((d (<lookup> d)))
    (cond
     ((cset? d)
      (let ((a0 (struct-ref d 0))
            (a1 (struct-ref d 1))
            (a2 (struct-ref d 2))
            (p  (struct-ref d 3))
            (m  (struct-ref d 4)))
        (<values> (a0 l0) (cp30 S a0 lam))
        (<values> (a1 l1) (cp30 S a1 lam))
        (<cc> (make-set a0 a1 a2 p m) ((@ (guile) append) l0 l1))))
     ((set? d)
      (<cc> (cp0 S d lam) '())))))
        
((@@ (logic guile-log code-load) set-set-tag!) set_unify)
(set! (@@ (logic guile-log functional-database) set-unify) set_unify)
