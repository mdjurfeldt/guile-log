(define-module (logic guile-log guile-prolog zip)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log prolog names)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (logic guile-log prolog goal-transformers)
  #:export (zip usr_zip lane update))

(define-syntax-rule (mk-sym a)
  (begin
    (define a (make-unbound-fkn 'a))
    (set-procedure-property! a 'name 'a)))

(mk-sym lane)

(<define> (zip_ xs codes)
  (<match> (#:mode - #:name zip_) (xs codes)
    (()    ()   
     (<cut> <cc>))

    ((x)   (c)  
     (<cut> (goal-eval c)))

    ((x1 x2) (c1 c2)
     (<cut>
      (<zip> (x1 (goal-eval c1)) 
             (x2 (goal-eval c2)))))

    ((x1 x2 x3) (c1 c2 c3)
     (<cut> 
      (<zip> (x1 (goal-eval c1)) 
             (x2 (goal-eval c2)) 
             (x3 (goal-eval c3)))))

    ((x1 x2 x3 x4) (c1 c2 c3 c4)
     (<cut>
      (<zip> (x1 (goal-eval c1)) 
             (x2 (goal-eval c2)) 
             (x3 (goal-eval c3))
             (x4 (goal-eval c4)))))

    ((x1 x2 x3 x4 . xl) (c1 c2 c3 c4 . cl)
     (<cut>
      (<zip> (x1 (goal-eval c1)) 
             (x2 (goal-eval c2)) 
             (x3 (goal-eval c3))
             (x4 (goal-eval c4))
             (xl (zip_ xl cl)))))))

(<define> (zip . l)
  (<recur> lp ((l l) (a '()) (b '()))
     (<match> (#:mode - #:name zip) (l)
       ((#((,lane x code)) . u)
        (<cut> (lp u (cons x a) (cons code b))))
       (()
        (<let> ((a (reverse! a))
                (b (reverse! b)))
           (<cut> 
            (zip_ a b))))
       (_ (type_error lane l)))))

(define-syntax-rule (mk f)
  (<lambda> (n)
    (case n
      ((1) (<update>     (f)))
      ((2) (<update-val> (f))))))

(<define> (usr_zip_ fs xs cs guard)
  (<let> ((n (length fs)))
    (case n
      ((0) <cc>)
      ((1)
       (<match> (#:mode - #:name usr_zip_1) (fs xs cs)
         ((f) (x)   (c)       
          (<cut>
           (<//> ((df11 ((y x)) (goal-eval c))) 
              (<=> x y) (<=> f ,(mk df11))
              (goal-eval guard))))))
      ((2)    
       (<match> (#:mode - #:name usr_zip_2) (fs xs cs)
         ((f1 f2) (x1 x2) (c1 c2)
          (<cut>
           (<//> ((df21 ((y1 x1)) (goal-eval c1))
    	          (df22 ((y2 x2)) (goal-eval c2))) 
	      (<=> (x1 x2) (y1 y2)) 
              (<=> (f1 f2) (,(mk df21) ,(mk df22)))
              (goal-eval guard))))))

      ((3)
       (<match> (#:mode - #:name usr_zip_3) (fs xs cs)
          ((f1 f2 f3) (x1 x2 x3) (c1 c2 c3)
           (<cut> 
            (<//> ((df31 ((y1 x1)) (goal-eval c1))
                   (df32 ((y2 x2)) (goal-eval c2))
                   (df33 ((y3 x3)) (goal-eval c3))) 
              (<=> (x1 x2 x3) (y1  y2  y3)) 
              (<=> (f1 f2 f3) (,(mk df31) ,(mk df32) ,(mk df33)))
              (goal-eval guard))))))
      ((4)
       (<match> (#:mode - #:name usr_zip_4) (fs xs cs)
         ((f1 f2 f3 f4) (x1 x2 x3 x4) (c1 c2 c3 c4)
          (<cut>
           (<//> ((df41 ((y1 x1)) (goal-eval c1))
                  (df42 ((y2 x2)) (goal-eval c2))
                  (df43 ((y3 x3)) (goal-eval c3))
                  (df44 ((y4 x4)) (goal-eval c4))) 
             (<=> (x1 x2 x3 x4) (y1  y2  y3  y4)) 
             (<=> (f1 f2 f3 f4) (,(mk df41) ,(mk df42) ,(mk df43) ,(mk df44)))
             (goal-eval guard))))))
      (else
       (syntax_error "unsuported usr_zip number of lanes")))))


(<define> (usr_zip . l)
  (<recur> lp ((l l) (a '()) (b '()) (c '()))
     (<match> (#:mode - #:name usr_zip) (l)
       ((#((,lane f x cs)) . u)
        (<cut> 
         (lp u (cons f a) (cons x b) (cons cs c))))
       ((guard)
        (<cut>
         (usr_zip_ (reverse! a) (reverse! b) (reverse! c) guard)))
       (_
        (type_error lane l)))))
 

(define update
  (<case-lambda>
   ((x)
    (<let> ((y (<lookup> x)))
     (y 1)))

   ((x . l) 
    (<let> ((y (<lookup> x)))
     (y 2)
     (<apply> update  l)))))
