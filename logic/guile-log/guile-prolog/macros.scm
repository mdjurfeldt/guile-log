(define-module (logic guile-log guile-prolog macros)
  #:use-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log)
  #:replace (extended)
  #:export (*extended* 
	    *extended-body* 
	    extended_macro 
	    extended_body 
	    all_extended 
	    all_extended_body))

(<define> (extended . l)
  (<code> (error "extended is no good function")))

(set-procedure-property! extended 'dynamic-directive #t)

(define *extended*      #f)
(define *extended-body* #f)

(<define> (set_extended L B)
  (<code> (set! *extended*      
	    (let lp ((l (<scm> L)) (r '()))
	      (if (pair? l)
		  (lp (cddr l) (cons (list (car l) (cadr l)) r))
		  r))))
  (<code> (set! *extended-body* (<scm> B))))


(compile-prolog-string "
   :- dynamic([register/2,register_f/2]).
 
   on(L,Body) :-
     register(X,L,Body) -> X=on.
 
   add_f(F) :-
      (   
      (
        on(L,Body) ->
          ( 
            asserta(register(off,L,Body)),
            asserta(register_f(F,L,Body))
          ) ;
         register_f(F,L,Body)
      ) ->
        set_extended(L,Body) ;
        set_extended(#f,#f)
      ).

   extended_macro(\"op2-\"(extended(|L)),[]) :-
     write(extended(|L)),nl,
     asserta(register(on,L,#f)).

   extended_macro(\"op2-\"(extended_body(|L)),[]) :-
     write(extended_body(|L)),nl,
     asserta(register(on,L,#t)).

   extended_macro(\"op2-\"(extended),[]) :-
     write(extended([])),nl,
     asserta(register(on,[],#f)).

   extended_macro(\"op2-\"(extended_body),[]) :-
     write(extended_body([])),nl,
     asserta(register(on,[],#t)).

   extended_macro(\"op2-\"(all_extended(|L)),[]) :-
     write(all_extended(|L)),nl,
     asserta(register_F(_,L,#f)).

   extended_macro(\"op2-\"(all_extended_body(|L)),[]) :-
     write(all_extended_body(|L)),nl,
     asserta(register_f(_,L,#t)).

   extended_macro(\"op2-\"(all_extended),[]) :-
     write(all_extended([])),nl,
     asserta(register_f(_,[],#f)).

   extended_macro(\"op2-\"(all_extended_body),[]) :-
     write(all_extended_body([])),nl,
     asserta(register_f(_,[],#t)).


  
   extended_macro((F(|L) :-  Code), _) :- !,
     add_f(F),fail.
   extended_macro((F(|L) --> Code), _) :- !,
     add_f(F),fail.
   extended_macro(F(|L)           , _) :- !,
     add_f(F),fail.       

")
