(define-module (logic guile-log guile-prolog continuations)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log prompts)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (logic guile-log iso-prolog)
  #:export (abort_to_prompt with_prompt generator next yield translate 
			    re_prompt call_k))

(define eat  #f)
(define feed #f)

(define prompt-tag (list 'prolog-prompt))
(<define> (abort_to_prompt tag data feed)
   (<abort> prompt-tag (<lambda> (x) (<=> x feed) <cc>) tag data))


(<define> (with_prompt tag code handler-data handler)
  (<prompt> prompt-tag '()
    (<lambda> () (goal-eval code))
    (<lambda> (t next k tt data)
       (<if> (<=> tt tag)
	     (<and>
	      (<=> handler-data ,(list tt next k data))
	      (goal-eval handler))
	     (next)))))


(define re_prompt
  (<case-lambda>
    ((tag k hdata handle data)
     (<re-prompt> (<lookup> k) 
		  (<lambda> (t next k tt data)
		     (<if> (<=> tt tag)
			   (<and>
			    (<=> hdata ,(list tt next k data)) 
			    (goal-eval handle))
			   (next)))
		  (list data)))
    ((k hdata handle data)
     (<re-prompt> (<lookup> k)
		  (<lambda> (t next k tt data)
		    (<=> hdata ,(list tt next k data))
		    (goal-eval handle))
		  (list data)))))

(<define> (call_k K D) (((<lookup> K)) D))

(compile-prolog-string "yield(X) :- abort_to_prompt(generator,X,_).")
(compile-prolog-string "eat(X)   :- abort_to_prompt(generator,_,X).")
(compile-prolog-string 
"generator(Goal,F) :- 
   with_prompt(generator, Goal,[generator,_,K,X],F=[K,X]).")

(<define> (pref)
 (<pp> (gp-handlers-ref)))

(compile-prolog-string 
"
next([K,X],X,F)        :- re_prompt(K,[generator,_,K2,XX],F=[K2,XX],_).
feed([K,_],Y,F)        :- re_prompt(K,[generator,_,K2,_ ],F=[K2,_ ],Y).
translate([K,X],X,Y,F) :- re_prompt(K,[generator,_,K2,XX],F=[K2,XX],Y).
")


	