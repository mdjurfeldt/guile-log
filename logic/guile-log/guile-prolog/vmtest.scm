(use-modules (logic guile-log iso-prolog))
(use-modules (logic guile-log guile-prolog ops))
(use-modules (logic guile-log guile-prolog vm-compiler))

#;
(eval-when (compile)
(pk (prolog-run-rewind 1 (x) 
		   (dyntrace (@@ (logic guile-log guile-prolog vm vm-goal2) 
				 compile_goal)))))
#;
(compile-prolog-string
"
- eval_when(compile).
the_tr2(X,[X]).
:- add_term_expansion_temp(the_tr2).
")

(compile-prolog-string
"
compiles_only_clauses.
")


(define-prolog q "
q(Code,Narg) :-
  (
    Code = (F(|A) :- Goal) -> length(A,Narg) ;
    Code = (F     :- Goal) -> Narg = 0       ;
    throw(compiles_only_clauses(Code))
  ).
")

(define-prolog b "
  b(X,Y) :- Y is X + 1.
")

(define-prolog a "
  a(X,Z) :- b(X,Y),b(Y,Z)
")

#;
(define-prolog f1 "
   f1(N,I,J,S) :- 
      I < N -> 
        (
          II is I + 1, 
          JJ is J + I, 
          f1(N,II,JJ,S)
        ) ; 
      S=J
  ")

#;
(define-prolog f0 "
   f1(N,I,J,S) :- 
      I > N -> S=J ;
        (
          II is I + 1, 
          JJ is J + I, 
          f1(N,II,JJ,S)
        ).
  ")

#;
(define-prolog f2 "
   f2(N,S) :-
     recur * lp((I,0),(J,0)),
       (
         I < N ->
           (
             II is I + 1,
             JJ is J + I,
             lp(II,JJ)
           ) ;
         S=J
       ).
")

#;
(define-prolog memb "
   memb(X,L) :-
     recur * lp((LL,L)),
        LL=[A|B],
        (
          A=X;
          lp(B)
        ).
")




