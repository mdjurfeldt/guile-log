(define-module (logic guile-log guile-prolog fluid)
  #:use-module (logic guile-log)
  #:export(make_fluid fluid_ref fluid_set))

(<define> (make_fluid Init Out) (<=> Out ,(make-fluid (<lookup> Init))))
(<define> (fluid_ref Fluid Out) (<=> Out ,(fluid-ref  (<lookup> Fluid))))
(<define> (fluid_set Fluid Val) (<code> (fluid-set! (<lookup> Fluid)
                                                    (<lookup> Val))))

