(define-module (logic guile-log guile-prolog hash)
  #:use-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log canonacalize)
  #:use-module (logic guile-log hash)
  #:use-module (logic guile-log vlist)
  #:use-module (logic guile-log umatch)
  #:use-module (logic guile-log prolog names)
  #:use-module (logic guile-log prolog util)
  #:use-module (logic guile-log prolog error)
  #:use-module (logic guile-log dynamic-features)
  #:use-module (logic guile-log prolog goal-transformers)
  #:use-module (logic guile-log)
  #:export (make_vhash vhash vhashp vhash_ref vhashq_ref vhashql_ref 
		       vhash_cons vhashq_cons vhashql_cons
                       peek_vhash vhash_to_assoc))

(mk-sym vhash)

;;  TODO: debug and stabilize the C-vlist code
;;  TODO: These hashes get's bloated if we do a lot of hash-cons in which case

;;  onw might wand hash_set_x!! in stead of vhash-cons
;;  We need to support vectors or f(X) like objects

;;  TODO: vhash must be intelligent with respect to truncate
;;  TODO: EQ, hashes EQV hashes?
;;  TODO: hash fold
;;  TODO: region guarding.

#|

vhashes works like an association list where seting new values is done by
consing a new pair to the top of the list and therefore shading the old pair
it is possible to set it and remove th eold value but that is an expensive
operation, it is not uncommon to have this restriction. Another nice feature 
we have with the vhash is that it can backtrack very effectively and is a well
optimized datastructure for doing this. But it is backtracking with 
intelligence! If a value is stored in e.g. an interleaving operation it will
handle that and make act to chain a frech new datastructure else it will reuse
it's old datastructure.

|#

(<define> (make_vhash H)
  (<let> ((h (<make-vhash>)))
    (add-vhash-dynamics h)
    (<=> H h)))

(<define> (vhashp x) (when (<vhash?> (<lookup> x))))

(<define> (vhash_ref h k ret)
  (<let> ((h   (<lookup> h))
	  (k   (canon-it k S)))
    (cond
     ((<var?> h)
      (instantiation_error))
     ((not (<vhash?> h))
      (type_error vhash h))
     (else
      (<let> ((val (vhash-assoc (<scm> k) (fluid-ref h))))
	(when val
	  (<=> ,(un-canon-it val) (k . ret))))))))

(<define> (vhashq_ref h k ret)
  (<let> ((h   (<lookup> h))
	  (k   (<lookup> k)))
    (cond
     ((<var?> h)
      (instantiation_error))
     ((not (<vhash?> h))
      (type_error vhash h))
     (else
      (<let> ((val (vhash-assq k (fluid-ref h))))
	(when val
	  (<=> ,val (_ . ret))))))))

(<define> (vhashql_ref h k ret)
  (<let> ((h   (<lookup> h))
	  (k   (<lookup> k)))
    (cond
     ((<var?> h)
      (instantiation_error))
     ((not (<vhash?> h))
      (type_error vhash h))
     (else
      (<let> ((val (vhash-assoc k (fluid-ref h))))
	(when val
	  (<=> ,val (_ . ret))))))))

(<define> (vhash_cons h k v)
  (<let*> ((h    (<lookup> h))
	   (k.v  (canon-it (cons k v) S))
	   (k    (car k.v))
	   (v    (cdr k.v)))
    (cond
     ((<var?> h)
      (instantiation_error))
     ((not (<vhash?> h))
      (type_error vhash h))
     (else
      (<code> (fluid-set! h (vhash-cons k v (fluid-ref h))))))))


(<define> (vhashq_cons h k v)
  (<let*> ((h    (<lookup> h))
	   (k    (<lookup> k)))
    (cond
     ((<var?> h)
      (instantiation_error))
     ((not (<vhash?> h))
      (type_error vhash h))
     (else
      (<code> (fluid-set! h (vhash-consq k
                                         (<lookup> v) (fluid-ref h))))))))

(<define> (vhashql_cons h k v)
  (<let*> ((h    (<lookup> h))
	   (k    (<lookup> k)))
    (cond
     ((<var?> h)
      (instantiation_error))
     ((not (<vhash?> h))
      (type_error vhash h))
     (else
      (<code> (fluid-set! h (vhash-cons k
					(<lookup> v) (fluid-ref h))))))))

(<define> (peek_vhash h)
 (<code> (analyze (fluid-ref (<lookup> h)))))

(define (analyze x)
  (if (vlist? x)
      (let ((a (struct-ref x 0))
            (b (struct-ref x 1)))
        (format #t "<vhash> offset = ~a, " b)
        (let ((block (vector-ref a 0))
              (off   (vector-ref a 2))
              (size  (vector-ref a 3))
              (free  (vector-ref a 4)))
          (format #t " size ~a, free ~a~%" size free)
          (let lp ((i b))
            (if (>= i 0)
                (let* ((next (number->string
                              (logand #xffffffff 
                                      (vector-ref block (+ (* size 3) i)))
                              16))
                       (back (ash
                              (vector-ref block (+ (* size 3) i))
                              -32))
                       (hash (vector-ref block (+ (* size 2) back)))
                       (v    (object-address (vector-ref block i)))) 
                    
                  (format #t "~a: next ~a, back ~a hashv ~a key ~a~%" 
                          i next back 
                          hash (number->string v 16))
                  (lp (- i 1)))))))
      (format #t "<assoc>~%")))

(<define> (vhash_to_assoc h l)
   (<let> ((h (<lookup> h)))
    (cond
     ((<var?> h)
      (instantiation_error))
     ((not (<vhash?> h))
      (type_error vhash h))
     (else 
       (<=> l ,(vhash->assoc (fluid-ref h)))))))
