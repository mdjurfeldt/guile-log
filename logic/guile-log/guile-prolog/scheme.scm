#|
interact with scheme expressions e.g.
scheme([+,X,Y],Out)
|#

(<define> (scheme L Out)
 (<=> Out ,(eval (<scm> L) (current-module))))

