(define-module (logic guile-log memoize)
  #:use-module (logic guile-log vlist)
  #:use-module ((ice-9 match) #:select ((match . ice9:match)))
  #:use-module (srfi srfi-9)
  #:use-module (logic guile-log vlist-macros)
  #:use-module (logic guile-log iinterleave)
  #:use-module (logic guile-log hash)
  #:use-module (logic guile-log postpone)
  #:use-module (logic guile-log prompts)
  #:use-module (logic guile-log prolog order)
  #:use-module ((logic guile-log prolog names) #:select
		(call))
  #:use-module ((logic guile-log umatch) #:select
		(gp-unifier gp-raw-unifier gp-m-unifier gp? gp-pair?
			    gp-attvar-raw? gp-att-raw-var gp-att-data
			    gp-newframe gp-unwind gp-unwind-tail gp-lookup
			    gp-store-state gp-restore-wind
			    attribute-cstor-ref gp->scm))
  #:use-module (logic guile-log canonacalize)
  #:use-module (logic guile-log dynamic-features)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log prolog closed)
  #:use-module (logic guile-log prolog namespace)
  #:use-module (logic guile-log guile-prolog closure)
  #:export (memo rec rec-00 tabling memo-rec memo-ref rec-ref table-ref 
		 rec-once memos recs rec= rec== rec-action with-rec-unifyer
                 rec-unifyer rec-0
		 rec-once-0 rec-lam rec-action00
		 rec-lam-once rec-lam-0 rec-lam-once-0
		 gp-cp-rec gp->scm-rec canon-it-rec
		 with-nonrec-unifyer nonrec-unifyer table table_i table_rec
                 table_simple
		 recursive with-canon with-atomic-rec with-atomic-frec))


(define-record-type %cont%
  (make-cont cont)
  cont?
  (cont cont-ref cont-set!))

#|
Memoizing can speed up function evaluation by memoizing the input and output.
Also it is possible to solve inifinite recursion.
|#
(define memo-lookup vhash-assoc)
(define add-memo    vhash-cons)
(define (make-memo) vlist-null)
(define memos (make-weak-key-hash-table))
(define memo-ref0 
  (case-lambda 
   ((f tag) (memo-ref0 f tag #f))
   ((f tag e)
    (let ((m (hashq-ref memos f #f)))
      (if m
	  (hash-ref m tag e)
	  e)))))

(define (add-memo-* F)
  (letrec ((h  (lambda (s f tag x)
		 (let ((m (hashq-ref memos f #f)))
		   (if m
		       (let ((p (hash-ref m tag #f)))
			 (if (not p)
			     (hash-set! m tag (F s x))))
		       (begin
			 (hashq-set! memos f (make-hash-table))
			 (h s f tag x)))))))
    h))

(define add-memo-p
  (add-memo-*
   (lambda (s x)
     (<lambda> (x) <fail>))))

(define add-memo-cc
  (add-memo-*
   (lambda (s x)
     (let ((tag (canon-it-rec x s)))
       (<lambda> (x) 
	 (<=> x ,(un-canon-it tag)))))))

(define add-table-p  
  (letrec 
      ((h 
	(lambda (f tag x s)
	  (let ((m (hashq-ref memos f #f)))
	    (if m
		(let lp ((l (hash-ref m tag #f)))
		  (if l
                      (hash-set! m tag (vhash-cons tag #t l))
		      (lp vlist-null)))
		(begin
		  (hashq-set! memos f (make-hash-table))
		  (h f tag x s)))))))
    h))

(define add-table-cc 
  (letrec 
      ((h 
	(lambda (f tag x s update gr? tr ig cont)
	  (let ((m (hashq-ref memos f #f)))
	    (if m
		(let lp ((l (hash-ref m tag #f)))
		  (if l
		      (let ((c (canon-it-rec x s)))
			(if gr?			    
			    (let* ((c2 (tr c))
				   (xx (vhash-ref l c2 #f)))
			      (if (not xx)
				  (let ((xx (make-variable (ig c))))
				    (hash-set! m tag (vhash-cons c2 xx l))
				    s)
				  (begin
				    (variable-set!
                                     xx (update s (ig c) (variable-ref xx)))
				    #f)))
			    (begin
			      (if (not (vhash-ref l c #f))
                                  (begin
                                    (hash-set!
                                     m tag (vhash-cons c c l))
                                    s)
                                  #f))))
		      (lp (vhash-cons 'cont cont vlist-null))))
		(begin
		  (hashq-set! memos f (make-hash-table))
		  (h f tag x s update gr? tr ig cont)))))))
    h))

						
(define recs (<make-vhash>))
(define (rec-ref0 f tag)
  (let ((m (vhashq-ref (fluid-ref recs) f #f)))
    (if m
	(vhasha-ref (fluid-ref m) tag #f)
	#f)))
(define (rec-ref00 f)
  (let ((m (vhashq-ref (fluid-ref recs) f #f)))
    (fluid-ref m)))

(define (rec-set0! f h)
  (let ((m (vhashq-ref (fluid-ref recs) f #f)))
    (if m
	(fluid-set! m h)
	#f)))

(define (table-ref0 f tag)
  (let ((m (vhashq-ref (fluid-ref tables) f #f)))
    (if m
	(vhash-ref (fluid-ref m) tag #f)
	#f)))

(define mark (list (cons 1 2)))

(<define> (add-rec f tag x D F)
  (<let> ((m (vhashq-ref (fluid-ref recs) f #f)))
    (if m
	(<recur> lp ((first #t))
	  (if first
	      (if (eq? (fluid-ref m) vlist-null)
		  (D m
		     (<lambda> ()
			 (lp #f)))
		  (lp #f))
	      (<let> ((q (vhasha-ref (fluid-ref m) tag #f)))
		 (if (not q)
		     (<and> 
		      (<code> (fluid-set! m (vhash-consa tag 
							 #t (fluid-ref m))))
		      (F))
		     <cc>))))

	(<let> ((m (<make-vhash>)))
	   (add-vhash-dynamics m)
	   (<code> (fluid-set! recs (vhash-consq f m (fluid-ref recs))))
	   (D m
                (<lambda> ()
		   (<code>
		    (fluid-set! m (vhash-consa mark #t (fluid-ref m))))
		   (add-rec f tag x D F)))))))

(define tables (<make-vhash>))
(<define> (init-memo-rec f tag xin D F)
  (<let> ((m (vhashq-ref (fluid-ref tables) f #f)))
    (if m
	(<let> ((x (vhash-ref (fluid-ref m) tag #f)))
           (if (not x)
	       (<code> (fluid-set! m (vhash-cons tag xin (fluid-ref m))))
	       <cc>)
	   (F))

	(<let> ((m (<make-vhash>)))
           (add-vhash-dynamics m)
	   (<code> (fluid-set! tables (vhash-consq f m (fluid-ref recs))))
	   (D m
                (<lambda> ()
		   (init-memo-rec f tag xin D F)))))))

(<define> (init-table f tag xin D F)
  (<let> ((m (vhashq-ref (fluid-ref tables) f #f)))
    (if m
	(<recur> lp ((first #t))
	  (if first
	      (if (eq? (fluid-ref m) vlist-null)
		  (D m (<lambda> () (lp #f)))
		  (lp #f))
	      (<let> ((x (vhash-ref (fluid-ref m) tag #f)))
		     (if (not x)
			 (<and>
			  (<code> (fluid-set! m 
					      (vhash-cons tag xin 
							  (fluid-ref m)))))
			 <cc>)
		     (F))))

	(<let> ((m (<make-vhash>)))
           (add-vhash-dynamics m)
	   (<code> (fluid-set! tables (vhash-consq f m (fluid-ref tables))))

	   (D m
	      (<lambda> ()
                 (<code> (fluid-set! m (vhash-cons 1 1 (fluid-ref m))))
		 (init-table f tag xin D F)))))))
	       						      
(<define> (memo-ref f x)  (<=> x ,(hashq-ref memo f #f)))
(<define> (rec-ref  f x)  (<=> x ,(vhashq-ref (fluid-ref recs) f #f)))
(<define> (table-ref f x) (<=> x ,(vhashq-ref (fluid-ref tables) f #f)))
(<define> (my-postpone . x)
  (<let> ((n (<lookup> (@@ (logic guile-log) gp-not-n))))
    (if (> n 0)
	(<set> (@@ (logic guile-log) gp-is-delayed?) #t)
	<cc>)
    (<apply> postpone x)))

(<define> (do-tabling . l)
  (<values> (p?) (<apply> do-tabling0 l))
  (when (eq? p? 1)))

(<define> (do-tabling0 f tag x ig gr?)
 (let ((t (gensym "x")))         
 (<recur> lp ((P vlist-null) (cont #f))
  (<let> ((L0 (memo-ref0 f tag vlist-null)))
   (<vhash-fold> P ((seed #f) (cont cont)) (key val) next L0
    (if (not (and gr? seed))
        (<and>
         (<recur> lp1 ((first? #t))
          (<let> ((L1 (memo-ref0 f tag vlist-null)))
           (if (eq? L0 L1)
               (let ((c (cont-ref cont)))
                 (if (and first? c)
                     (c)
                     <fail>)
                 (lp1 #f))
               (lp L0 cont)))))
        <fail>)

    (cond
     ((eq? key 'cont)
      (next seed val))
           
     ((eq? val #t)
      <fail>)
     
     (else
      (let ((val (if (variable? val) (variable-ref val) val)))
        (<or>
         (<and> (rec= (un-canon-it val) (ig x)) (<cc> 1))
         (next #t cont))))))))))

(<define> (do-tabling-rec . l)
  (<values> (p?) (<apply> do-tabling-rec0 l))
  (when (eq? p? 1)))

(<define> (do-tabling-rec0 f tag x ig gr? first rec?)          
  (if rec?
      (<or-ii>
       (<and> (rec= (ig x) (ig first)) (<cc> 1))
       (do-tabling0 f tag x ig gr?)
       (<and> (<apply> f x) (<cc> 1)))
      (do-tabling0 f tag x ig gr?)))

(define (memo f)
  (<lambda> x
     (<let*> ((tag (canon-it x S))
	      (me  (memo-ref0 f tag))
	      (p   P)
	      (cc  CC))
	(if me
	    (me x)
	    (<with-fail> (lambda ()
			   (add-memo-p #f f tag x) (p))
	      (<with-cc> (lambda (s p0 . y)
			   (add-memo-cc s f tag x)
			   (apply cc s p y))
	        (<apply> f x)))))))


(<define> (memo-rec f . x)
  (<let*> ((tag    (canon-it x S))
	   (me     (memo-ref0 f tag))
	   (first  (table-ref0 f tag))
	   (p      P)
	   (cc     CC))
     (cond
      (me (me x))
      
      (first
       (rec= first x))
      
      (else
       (init-memo-rec f tag x (Q with-backtrack-dynamic-object)
         (<lambda> ()
           (<with-fail> (lambda () 
			  (add-memo-p f tag x) (p))
			
	       (<with-cc> (lambda (s p . u)
			    ((<lambda> ()
				(add-memo-cc s f tag x)
				(<with-cc> cc <cc>))
			     s p cc))
			 (<apply> f x)))))))))

(define (getkind x)
  (<wrap>
   (<lambda> ()
    (let ((x (<lookup> x)))
      (<match> (#:mode -) (x)
        (#((a b))
	 (<ret> (list (getkind a) (<lookup> b))))
	(a
	 (let ((x (<lookup> x)))
	   (if (procedure? x)
	       (<ret> (symbol->string (procedure-name x)))
	       (if (<var?> x)
		   (<ret> "op2+")
		   (<ret>
                    (error (format #f "wrong spec in table (~a)" x))))))))))))

(define (get-param u)
  (define-values (gr? tr gr ig)
    (if (pair? u)
	(let lp ((u u) (f '()))
	  (ice9:match u
	    ((x . u)
	     (lp u
		 (cons
		  (ice9:match (getkind x)
                    ("op2+"   (let ((ground
                                     (<lambda> (x)
                                       (if (<ground?> x)
                                           <cc>
                                           <fail>))))
				ground))
                    ("ignore" 2)          
		    ("all"    (let ((all (<lambda> (x) <cc>)))
				all))
		    (("ground" f) f)		    
		    (_        #f))
		  f)))
	    (()
	     (let ((f (reverse f)))
	       (values
		#t
		(lambda (x)
		  (let lp ((x x) (f f))
		    (if (pair? x)
			(if (eq? (car f) 2)
                            (lp (cdr x) (cdr f))
                            (cons
                             (if (not (car f))
                                 #t
                                 (car x))
                             (lp (cdr x) (cdr f))))
			'())))
		
		(lambda (s x)
		  (let lp ((xs x) (fs f))
		    (if (pair? xs)
			(let ((f (car fs))
			      (x (car xs)))
			  (if (not (or (number? f) (boolean? f)))
			      (if (<wrap-s>
				   (<lambda> ()
				     (<if> (f x)
					   (<ret> #t)
					   (<ret> #f))) s)
				  (lp (cdr xs) (cdr fs))
				  #f)
			      (lp (cdr xs) (cdr fs))))
			#t)))

                (lambda (x)
		  (let lp ((x x) (f f))
		    (if (pair? x)
                        (if (eq? 2 (car f))
                            (lp (cdr x) (cdr f))
                            (cons (car x) (lp (cdr x) (cdr f))))
			'()))))))))

	(values
	 #f
	 (lambda (x)    x)
	 (lambda (s x) #f)
         (lambda (x)   x))))
  
  (define-values (first? update)
    (if (pair? u)
	(let lp ((u u) (f '()) (first? #t))
	  (ice9:match u
             ((x . u)
              (define (a)    (lp u f first?))
              (define (b1 x)  (lp u (cons x f) #f))
              (define (b2 x)  (lp u (cons x f) first?))
              (ice9:match (getkind x)                  
                ("op2+"
                 (b2 (<lambda> (x xold) (<cc> x))))
		
                ("ignore" (a))
		
                ("all"
                 (b2 (<lambda> (x xold) (<cc> x))))
			 
                ("index"
                 (b2 (<lambda> (x xold) (<cc> x))))

		(("ground" f)
		 (b2 (<lambda> (x xold) (<cc> x))))
		 
                (("lattice" f)
                 (b1 (<lambda> (x xold)
                              (<var> (r)
                                     (call f xold x r)
                                     (<cc> (<scm> r))))))
		     
                (("po" f)
                 (b1 (<lambda> (x xold)
                              (<if> (call f xold x)
                                    (<cc> x)
                                    (<cc> xold)))))
		    
                ("first"
                 (b2 (<lambda> (x xold) (<cc> xold))))
		    
                ("last"
                 (b1 (<lambda> (x xold) (<cc> x))))
		    
                ("min"
                 (b1 (<lambda> (x xold)
                              (<if> (term< x xold)
                                    (<cc> x)
                                    (<cc> xold)))))
		    
                ("max"
                 (b1 (<lambda> (x xold)
                              (<if> (term> x xold)
                                    (<cc> x)
                                    (<cc> xold)))))

                ("sum"
                 (b1 (<lambda> (x xold)
                              (<cc> (+ (<lookup> x) (<lookup> xold))))))))
             
	     (()
	      (values
	       first?
	       (let ((f (reverse f)))
		 (lambda (s c x)
		   (<wrap-s> 
		     (<lambda> ()
		      (<recur> lp ((c c) (x x) (f f) (xx '()))
		        (if (pair? x)
			    (<or>
			     (<and>
			      (<values> (r) ((car f) (car c) (car x)))
			      <cut>
			      (lp (cdr c) (cdr x) (cdr f) (cons r xx)))
			     (<ret> (error "bug in latice finction fot tabling")))
			    (<ret> (reverse xx)))))
		     s)))))))
	(values
	 #f
	 (lambda (s c x)
	   c))))

  (values gr? tr gr ig first? update))

(define (table fkn . u)
  (define-values (gr? tr gr ig first? update) (get-param u))
  (<lambda> x (<apply> tabling_adv #f fkn update first? gr? gr tr ig x)))

(define (table_rec fkn . u)
  (define-values (gr? tr gr ig first? update) (get-param u))
  (<lambda> x (tabling_adv_rec #f fkn update first? gr? gr tr ig x #t)))

(define (table_i fkn . u)
  (define-values (gr? tr gr ig first? update) (get-param u))
  (<lambda> x (tabling_adv_rec #f fkn update first? gr? gr tr ig x #f)))

(define (table_simple fkn . u)
  (define-values (gr? tr gr ig first? update) (get-param u))
  (<lambda> x (<apply> tabling_adv #t fkn update first? gr? gr tr ig x)))

(<define> (tabling f . x) (<apply> tabling_adv #f f (lambda (s x) #f) #f #f x))

(<define> (make-catcher x f tag data)
   (let ((p P) (cc CC))
    (init-table f tag x (Q (<lambda> (p c) (c)))
     (lambda (s p cc)
      (ice9:match data
       ((f tag update do? tr ig gr? fi? cont)
        (<and> (p s p cc)
         (<with-fail> (lambda () 
                        (add-table-p f tag x #f) 
                        (p))
			     
        (<with-cc> (lambda (s p2 . u)
                     (let ((s (add-table-cc f tag x s update
                                            do? tr ig cont)))
                       
                       (if (and gr? fi?)
                           (if s
                               (apply cc s p u)
                               (p2))
                           (if s
                               (apply cc s p2 u)
                               (p2)))))
        (<apply> f x))))))))))

(<define> (tabling_adv simple? f update fi? do? gr tr ig . x)
 (let* ((tag    (canon-it-rec (ig x) S))
        (me     (memo-ref0 f tag))
        (first  (table-ref0 f tag))
        (f?     #t)
        (gr?    (gr S x)))
   (cond
    (first
     (do-tabling f tag x ig gr?))
       
    (else
     (let* ((cont (make-cont #f))
            (data (list f tag update do? tr ig gr? fi? cont)))
       (pwrap (or gr? simple?) (cons f tag) f tag x ig gr? cont
        (<lambda> (x) 
         (make-catcher x f tag data))))))))

(<define> (tabling_adv_rec simple? f update fi? do? gr tr ig x rec?)
  (let* ((tag    (canon-it-rec (ig x) S))
         (me     (memo-ref0 f tag))
         (first  (table-ref0 f tag))
         (f?     #t)
         (gr?    (gr S x)))
    (cond     
     (first
      (do-tabling-rec f tag x ig gr? first rec?))

     (else
      (let* ((cont (make-cont #f))
             (data (list f tag update do? tr ig gr? fi? cont)))
        (pwrap (or gr? simple?) (cons f tag) f tag x ig gr? cont
          (<lambda> (x)
            (with-new-machine0 old :
             (<lambda> ()
              (make-catcher x f tag data))))))))))

(define (get) (cdr (get-machine-wind)))

(<define> (return c-outer)
  (<code> (gp-restore-wind (car c-outer) #t))
  (<ret> ((list-ref c-outer 1) (list-ref c-outer 2) (list-ref c-outer 3))))

(define (muck cc s p state)
  (<lambda> ()
   (let ((c-outer (list (gp-store-state S) CC S P)))
     (<code> (gp-restore-wind state #t))
     (<ret> (cc s p c-outer)))))

(<define> (store cont)
 (let* ((cc      CC)
        (s       S)
        (p       P)
        (state   (gp-store-state S))
        (c-inner (muck cc s p state)))
     (<code> (cont-set! cont c-inner))
     (<cc> #f)))

(<define> (pwrap gr? fkn fu tag x ig ggr? cont f)
  (if gr?
      (f x)
      (let* ((ret    #f)
             (p1     (lambda () (if ret (return #f #f #f ret) (P))))
             (first? #t))
        (<or>
         (let ((p2   P)
               (t   (gensym "y")))
           (<with-fail> p1 (f (<cp> x)))
           (let ((p3 P))
             (<with-fail> p2
                (<and>
                 (<values> (r) (store cont))
                 (if r
                     (<and>
                      (<code> (set! ret r))
                      (<with-fail> p3 <fail>))
                    
                     (if first?
                         (<and>
                          (<code> (set! first? #f))
                          (<with-fail> p2 <fail>))
                         (return ret)))))))
         (do-tabling fu tag x ig ggr?)))))


(define (Q D) (<lambda> (h code)
		(with-fluid-guard-dynamic-object h 
		   (<lambda> ()
		     (with-state-guard-dynamic-object h 
		       (<lambda> () (D h code)))))))

(define-syntax-rule (get-tag s x)     (map (lambda (x) (<lookup> x)) x))

(define-syntax-rule (mk-rec-lam rec with get-tag-1)
  (define (rec f guard doit)
    (<lambda> (lam . x)
       (<let*> ((tag (get-tag-1 S x))
		(x   (get-tag   S x)))
	 (<values> (tag) (guard tag S))
	 (cond 
	  ((eq? tag #f)
	       <cc>)
	  (tag
	   (<let> ((hit (rec-ref0 f tag)))
		  (if hit
		      (<and>
		       (doit x)
		       (<apply> lam x))
		      (add-rec f tag x with
			       (<lambda> () (<apply> f lam x))))))
	  (else
	   (<apply> f lam x)))))))

(define-syntax-rule (mk-rec rec with get-tag-1)
  (define (rec f guard doit)
    (<lambda> x
       (<let*> ((tag (get-tag-1 S x))
		(x   (get-tag   S x)))
	 (<values> (tag) (guard tag S))         
	 (cond 
	  ((eq? tag #t)
	   <cc>)

	 (tag
	  (<let> ((hit (rec-ref0 f tag)))
	    (if hit
		(doit x)
		(add-rec f tag x with
			 (<lambda> () (<apply> f x))))))
	 (else
	  (<apply> f x)))))))


(<define> (id h x) (x))
(<define> (id00 x code) (code))
(mk-rec rec        (Q with-backtrack-dynamic-object)       get-tag)
(mk-rec rec-once   (Q with-backtrack-dynamic-object-once)  get-tag)
(mk-rec rec-0      with-backtrack-dynamic-object           get-tag)
(mk-rec rec-00     id00                                    get-tag)
(mk-rec rec-once-0 with-backtrack-dynamic-object-once      get-tag)

(mk-rec rec-00 id00                                    get-tag)

(mk-rec-lam rec-lam        (Q with-backtrack-dynamic-object)      get-tag)
(mk-rec-lam rec-lam-once   (Q with-backtrack-dynamic-object-once) get-tag)
(mk-rec-lam rec-lam-00     id00                                   get-tag)
(mk-rec-lam rec-lam-0      with-backtrack-dynamic-object          get-tag)
(mk-rec-lam rec-lam-once-0 with-backtrack-dynamic-object-once     get-tag)

;; Example of a recursive unifyer does not work for closures and namespaces yet
(define-syntax-rule (mk <<define>> rec=* rec= == bang? mode)
(<<define>> #:mode mode (rec=* x y)
  ((? <var?> x) y
   (<let> ((x (<lookup> x))
	   (y (<lookup> y)))
     (if bang?
	 (if (not (eq? x y))
	     (<let> ((s ((@@ (logic guile-log umatch) gp-set!) x y S)))
		    (when s (<with-s> s <cc>)))
	     <cc>)
	 (when (eq? x y)))))

  (y (? <var?> x)
   (when bang?
     (<let> ((x (<lookup> x))
	     (y (<lookup> y)))
       (<let> ((s ((@@ (logic guile-log umatch) gp-set!) x y S)))
	  (when s (<with-s> s <cc>))))))
  
  (#(x)    #(y)
   (<and>
    (rec= x y)))
   
  (#(a (... ...))     #(x (... ...)) 
   (<and> 
    (rec= a x)))
  
  ((a . b) (x . y)
   (<and>
    (rec= a x)
    (rec= b y)))
   
  (x y   
   (<let> ((x (<lookup> x))
	   (y (<lookup> y)))
     (cond
      ((variable? x)
       (rec= (variable-ref x) y))
      ((variable? y)
       (rec= x (variable-ref y)))

      ((prolog-closure? x)
       (when (prolog-closure? y)
	 (<let> ((lx (prolog-closure-state x))
		 (ly (prolog-closure-state y))
		 (fx (prolog-closure-parent x))
		 (fy (prolog-closure-parent x)))
	    (if (or (prolog-closure-closed? x) (prolog-closure-closed? y))
		(if (eq? fx fy)
		    (rec= lx ly)
		    (when (fluid-ref error-when-closed?) (closed-err x y)))
		(<and> (rec= fx fy) (rec= lx ly))))))
      ((namespace? x)
       (<let> ((s  (ns-unify S x y bang?)))
	 (when s (<with-s> s <cc>))))

      ((namespace? y)
       (<let> ((s  (ns-unify S y x bang?)))
	 (when s (<with-s> s <cc>))))

      (else	             
       (<let> ((s (== x y S)))
	 (when s (<with-s> s <cc>)))))))))


(mk <<define>>  rec==* rec== (@@ (logic guile-log umatch) gp-m-unify!-)   
    #f +r)
(mk <<define>>  rec=*  rec=  (@@ (logic guile-log umatch) gp-unify-raw!-) 
    #t +r)
    
(<define> (unify-guard x s)
  (if (and-map (lambda (x) (or (gp-pair? x s)
			       (pair? x) (vector? x)  (struct? x)
			       #;(gp-attvar-raw? x s)))
	      x)
      (<cc> x)
      (<cc> #f)))

(<define> (doit-id x) <cc>)

#|
(define i 0)
(<define> (rec= x y)
  (<let> ((j i))
    (<code> (set! i (+ i 1)))
    ((@ (logic guile-log iso-prolog) write) (list j x y))
    ((@ (logic guile-log iso-prolog) nl))
    (rec=_ x y)
    ((@ (logic guile-log iso-prolog) write) j)
    ((@ (logic guile-log iso-prolog) nl))))
|#  
  
(define-syntax-rule (with-atomic-rec code)
  (let ((f code))
    (<lambda> x
      (<let> ((h (fluid-ref recs))
	      (p P))
	(<with-fail> (lambda () (fluid-set! recs h) (p))
  	  (<apply> f x)
	  (<code> 
	   (vhash-truncate! h)
	   (fluid-set! recs h)))))))

(define-syntax-rule (with-atomic-frec f)
    (<lambda> x
      (<let> ((h (fluid-ref recs))
	      (p P))
	(<with-fail> (lambda () (fluid-set! recs h) (p))
  	  (<apply> f x)
	  (<code> 
	   (vhash-truncate! h)
	   (fluid-set! recs h))))))

(define rec=  (with-atomic-rec (rec-00 rec=*  unify-guard doit-id)))
(define rec== (with-atomic-rec (rec-00 rec==* unify-guard doit-id)))

(<<define>> #:mode -r (rec-action* lam x)
  (lam #(x)
   (rec-action* lam x))

  (lam #(x ...)
   (<and>
    (rec-action* lam x)))

  (lam (a . b)          
   (<and>
   (<let> ((a (<lookup> a))
	   (b (<lookup> b))
	   (h (rec-ref00 rec-action*)))
      (rec-action lam a)
      (<code>
       (vhash-truncate! h)
       (rec-set0! rec-action* h))
      (rec-action lam b))))

  (lam x
   (<let> ((x (<lookup> x)))
     (<and>
     (cond
      ((gp-attvar-raw? x S)
       (<let> ((v (gp-att-raw-var x S))
	       (l (gp-att-data x S)))
	 (rec-action lam x)
	 (<recur> lp ((l l))
	   (<match> (#:mode -) (l)
	      (((and x (id . _)) . l)
	       (if (attribute-cstor-ref id)
		   (lp l)
		   (<and>
		    (rec-action lam x)
		    (lp l))))))))
	  
      ((variable? x)
       (rec-action lam (variable-ref x)))
      ((prolog-closure? x)
       (<let> ((lx (prolog-closure-state x)))
	 (rec-action lam lx)))      
      ((namespace? x)
       (rec-action lam (namespace-val x)))
      (else	      
       <cc>))))))

(define rec-action   (rec-lam-00 rec-action* unify-guard doit-id))
(define rec-action00 rec-action)

(define (gp-rec=  x y s) 
  (rec=  s (lambda x #f) (lambda (s . u) s) x y))
(define (gp-rec== x y s) 
  (rec== s (lambda x #f) (lambda (s . u) s) x y))

(define gp-cp++ (@@ (logic guile-log umatch) gp-cp++))
(define *gp-cp* (@@ (logic guile-log umatch) *gp-cp*))
(define *gp->scm* (@@ (logic guile-log umatch) *gp->scm*))
(define *canon-it* (@@ (logic guile-log canonacalize) *canon-it*))
(define canon-it++ (@@ (logic guile-log canonacalize) canon-it++))
(define mpf (@@ (logic guile-log umatch) recurs-map))
(define gp-cp-rec
  (case-lambda
   ((x s)
    (gp-cp-rec x '() s))
   ((x l s)
    (define mp  (make-hash-table))
    (mpf mp)
    (<wrap-s> rec-action00 s 
	      (with-atomic-rec
	       (<lambda> (y)
			 (<code> (hashq-set! mp (<lookup> y) #t))))
	      x)
    (rec-set0! rec-action* vlist-null)
    (gp-cp++ #f x l s))))

(define (gp->scm-rec x s)
  (define mp  (make-hash-table))
  (mpf mp)
  (<wrap-s> rec-action00 s 
    (with-atomic-rec
      (<lambda> (y)     
	(<code> (hashq-set! mp (<lookup> y) #t))))
    x)
  (rec-set0! rec-action* vlist-null)
  (let ((r (gp-cp++ #t x s)))
    r))


(define (pp . x) (car (reverse x)))

(define canon-id (<lambda> (x) (<cc> x)))
(define canon (make-fluid canon-id))
(make-fluid-dynamics canon) 

(define (canon-it-rec x s)
  (define (id a b x s) x)
  (define (analyze mp x s)    
      (<wrap-s> rec-action00 s 
       (with-atomic-rec
        (<lambda> (y)
	  (<code> (hashq-set! mp (<lookup> y) #t))))
       x)
      (rec-set0! rec-action* vlist-null))
  (let ((s0 s)
	(fr (gp-newframe s)))
    (let ((sy ((fluid-ref canon) s (lambda x #f) (lambda (s p y) (cons s y))
	       x)))
      (let ((ret
	     (canon-it++ gp->scm-rec analyze  (cdr sy) (car sy))))
	(gp-unwind-tail fr)
	ret))))


(make-fluid-dynamics gp-unifier) 
(make-fluid-dynamics gp-raw-unifier)
(make-fluid-dynamics gp-m-unifier)
(make-fluid-dynamics *canon-it*)
(make-fluid-dynamics *gp-cp*)
(make-fluid-dynamics *gp->scm*)

(define with 
  (<case-lambda> 
   ((code x . l)
    (with-state-guard-dynamic-object x
       (<lambda> ()
	  (with-fluid-guard-dynamic-object x (<lambda> ()
						 (<apply> with code l))))))
   ((code) (code))))

(define on-status
  (<case-lambda> 
   ((code x . l)
    (state-guard-dynamic-object x)
    (fluid-guard-dynamic-object x)
    (<apply> on-status code l))
   ((code) (code))))
				     
(<define> (with-rec-unifyer code)
  (with (<lambda> () 
	  (<code> (fluid-set! gp-unifier     gp-rec=)
		  (fluid-set! gp-m-unifier   gp-rec==)
		  (fluid-set! gp-raw-unifier gp-rec=)
		  (fluid-set! *canon-it*     canon-it-rec)
		  (fluid-set! *gp-cp*        gp-cp-rec)
		  (fluid-set! *gp->scm*      gp->scm-rec))
	  (code))
	gp-unifier gp-raw-unifier gp-m-unifier
	*canon-it* *gp->scm* *gp-cp*))

(<define> (rec-unifyer)
  (on-status 
      (<lambda> () 
	  (<code> (fluid-set! gp-unifier     gp-rec=)
		  (fluid-set! gp-m-unifier   gp-rec==)
		  (fluid-set! gp-raw-unifier gp-rec=)
		  (fluid-set! *canon-it*     canon-it-rec)
		  (fluid-set! *gp-cp*        gp-cp-rec)
		  (fluid-set! *gp->scm*      gp->scm-rec)))
	gp-unifier gp-raw-unifier gp-m-unifier
	*canon-it* *gp->scm* *gp-cp*))


(<define> (with-nonrec-unifyer code)
  (with (<lambda> () 
	  (<code> (fluid-set! gp-unifier     (@@ (logic guile-log code-load)
						 gp-unify!-))
		  (fluid-set! gp-m-unifier   (@@ (logic guile-log code-load)
						 gp-m-unify!-))
		  (fluid-set! gp-raw-unifier (@@ (logic guile-log code-load)
						 gp-unify-raw!-))
		  (fluid-set! *canon-it*     (@@ (logic guile-log canonacalize)
						 canon-it+))
		  (fluid-set! *gp-cp*        (@@ (logic guile-log umatch)
						 gp-cp+))
		  (fluid-set! *gp->scm*      (@@ (logic guile-log code-load)
						 gp->scm-))
		  (code)))
  gp-unifier gp-raw-unifier gp-m-unifier
  *canon-it* *gp->scm* *gp-cp*))

(<define> (nonrec-unifyer)
  (on-status 
      (<lambda> () 
	  (<code> (fluid-set! gp-unifier     (@@ (logic guile-log code-load)
						 gp-unify!-))
		  (fluid-set! gp-m-unifier   (@@ (logic guile-log code-load)
						 gp-m-unify!-))
		  (fluid-set! gp-raw-unifier (@@ (logic guile-log code-load)
						 gp-unify-raw!-))
		  (fluid-set! *canon-it*     (@@ (logic guile-log canonacalize)
						 canon-it+))
		  (fluid-set! *gp-cp*        (@@ (logic guile-log umatch)
						 gp-cp+))
		  (fluid-set! *gp->scm*      (@@ (logic guile-log code-load)
						 gp->scm-))))
	 
  gp-unifier gp-raw-unifier gp-m-unifier
  *canon-it* *gp->scm* *gp-cp*))


(<define> (recursive f . x)
  (with-rec-unifyer  (<lambda> () (<apply> f x))))

(<define> (with-canon- code f)
  (with (<lambda> () 
	  (<code> (fluid-set! canon f))
	  (code))
	canon))
			      
(define with-canon 
  (lambda (g)
    (<lambda> (f . x)
      (with-canon-  (<lambda> () (<apply> f x)) g)))) 

