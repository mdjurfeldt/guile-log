(define-module (logic guile-log iso-prolog)
  #:use-module (logic guile-log prolog persist)
  #:use-module (logic guile-log prolog fold)
  #:use-module (logic guile-log scmspace)
  #:use-module (logic guile-log tools)
  #:use-module (logic guile-log prolog symbols)
  #:use-module (logic guile-log hash-dynamic)
  #:use-module (logic guile-log prolog base)
  #:use-module (logic guile-log prolog parser)
  #:use-module (logic guile-log prolog goal-transformers)
  #:use-module (logic guile-log prolog goal-functors)
  #:use-module (logic guile-log prolog directives)
  #:use-module (logic guile-log prolog dynamic)
  #:use-module (logic guile-log prolog io)
  #:use-module (logic guile-log prolog util)
  #:use-module (logic guile-log prolog run)
  #:use-module (logic guile-log prolog load)
  #:use-module (logic guile-log prolog modules) 
  #:use-module (logic guile-log prolog error) 
  #:use-module (logic guile-log prolog char)
  #:use-module (logic guile-log prolog char-conversion)
  #:use-module (logic guile-log prolog conversion)
  #:use-module ((logic guile-log prolog names)
		#:renamer 
		(lambda (x)
		  (if (eq? x 'callable)
		      'uno-callable
		      x)))
  #:use-module (logic guile-log prolog closed)
  #:use-module (logic guile-log prolog namespace)
  #:use-module (logic guile-log prolog variant)
  #:use-module (logic guile-log prolog swi)
  #:use-module (logic guile-log prolog global)
  #:use-module (logic guile-log functional-database)
  #:use-module (logic guile-log guile-prolog reset)
  #:use-module (system base language)
  #:use-module (logic guile-log)
  #:use-module ((logic guile-log code-load) #:select (multibute))
  #:use-module ((guile) #:select (@ @@ quote unquote random eval-when))
  #:use-module ((logic guile-log umatch) #:select (gp-var!)) 
  #:use-module ((logic guile-log functional-database) 
		#:select (extended interleaved extended_interleaved))
  #:use-module (logic guile-log guile-prolog copy-term)
  #:export    (reset-flags reset-prolog set cc)
  #:replace   (sort load)

  #:re-export (;;guile stuff
	       ;; profile
	       lambda define
	       use-modules
	       new_persister
	       persist_ref 
	       persist_set
	       load_persists 
	       save_persists
	       accessify_predicate
               
               unifiable
               
	       foldall  sumall  prodall  countall  maxall  minall  lastall
	       foldalln sumalln prodalln countalln maxalln minalln
	       foldstp  sumstp  prodstp  countstp  maxstp  minstp 
	       foldof   sumof   prodof   countof   maxof   minof   lastof
	       foldofp  sumofp  prodofp  countofp  maxofp  minofp  lastofp
	       foldofs  sumofs  prodofs  countofs  maxofs  minofs  lastofs
	       foldofn  sumofn  prodofn  countofn  maxofn  minofn  lastofn
	       foldofpn sumofpn prodofpn countofpn maxofpn minofpn lastofpn
	       foldofsn sumofsn prodofsn countofsn maxofsn minofsn lastofsn
	       amaxof   aminof   bmaxof   bminof
	       amaxall  aminall  bmaxall  bminall
	       amaxstp  aminstp  bmaxstp  bminstp
	       amaxofp  aminofp  bmaxofp  bminofp
	       amaxofs  aminofs  bmaxofs  bminofs
	       amaxofn  aminofn  bmaxofn  bminofn
	       amaxofpn aminofpn bmaxofpn bminofpn
	       amaxofsn aminofsn bmaxofsn bminofsn

               shift reset
               
	       functor	  
	       uniq yield_at_change        

	       ;;swi stuff 
	       meta_predicate public
	       memberchk $skip_list is_list
	       keysort compare ground assertion
	       strip_module must_be
	       succ plus between
	       collation_key
	       atomic_list_concat
	       atomic_concat
	       subsumes_term
	       predicate_options
	       code_type char_type
	       volatile noprofile assert
	       string_codes $type_error
	       $set_source_module  print_message
	       create_prolog_flag
	       thread_local
	       setup_call_cleanup
	       debug_term_position	       
	       term_variables 
	       string
	       acyclic_term
	       cyclic_term
	       msort
	       random set_random
	       clpfd_monotonic
	       clpfd_goal_expansion
	       clpfd_propagation
	       full
	       clpb_validation
	       add_term_expansion
	       add_term_expansion_temp
	       add_goal_expansion
	       add_goal_expansion_temp
	       group_pairs_by_key
	       gcd


	       expand_term
	       expand_goal
	       callable 
	       pairs_values
	       pairs_keys
	       pairs_keys_values
	       same_length
	       same_term
	       transpose

	       duplicate_term

	       ;; guile-log
	       multibute extended interleaved extended_interleaved
	       add_attribute_cstr
	       add_attribute_projector
	       module_name
	       current_module
	       make_dynamic
	       make_hash_dynamic
	       make_generic_hash_dynamic
	       make_dynamic
	       make_generic_dynamic
	       add_sym once_i

	       ;;Swi global variables
	       b_setval b_getval nb_setval nb_getval nb_current
	       setarg nb_setarg

	       ;;guile stuff that is needed
	       @ @@ quote unquote

	       ;;module
	       module
	       use_module
	       module-optable-set!
	       module-optable-ref
	       set-module-optable-from-current
	       
	       ;;unknown cludge
	       gp-var!

	       ;; Functor management
	       Trace Level trace-level
	       functorize adaptable_vars trace
	       
	       ;; Scheme functions
               compile-prolog-string compile-prolog-file
               save-operator-table prolog-run
	       prolog-run-rewind
               load-prolog clear-directives 
               save-char-conversion-table
               reset-char-conversion
               reset-operator-map
               
               ;; Math
               sin cos atan exp log sqrt

               ;; Standard ideoms
               functor arg
               catch throw call unify_with_occurs_check copy_term findall bagof
	       setof  var atom integer float atomic compound nonvar number 
	       dynamic multifile discontiguous
	       assertaf assertzf
	       asserta assertz clause retract abolish current_predicate
               op current_op set_prolog_flag 
               repeat once
               nl               
               fail true !
               current_prolog_flag

               ;; module
               ensure_loaded 

               ;; io
	       functional
               current_input current_output standard_input standard_output
               set_input set_output
               current_char_conversion char_conversion 
               put_char put_code get_char get_code peek_char peek_code
               get_byte put_byte peek_byte
               stream_property force binary_stream text_stream byte

               input output stream_or_alias stream stream_option io_mode text 
               binary type eof_action reposition alias reset eof_code at no 
               end_of_stream position variable mode file_name
               flush_output

               write_canonical writeq write write_term
               read read_term variables variable_names singletons read_option
               quoted ignore_ops numbervars write_option

               ;; replacings
               append length open close member

               ;; Error functions
               error type_error instantiation_error domain_error               
               permission_error existence_error representation_error
	       system_error
               character_code syntax_error
               source_sink evaluation_error

               ;; Non standard ideoms
               procedure_name pp

               ;; symbols
               not_less_than_zero evaluable callable modify 
               static_procedure access end_of_file
               predicate_indicator private_procedure
	       procedure

               ;; characters
               atom_length atom_concat atom_chars atom_codes char_code
               number_chars number_codes sub_atom
               
               ;; conversion
               round floor ceiling truncate
               
               ;; modules
        

               ;; symbols directive
               include
               operator_specifier
               fx fy xf yf xfx yfx xfy
               operator_priority
               flag_value flag
               on off false
               towards_zero down
               warning chars codes bounded auto_sym max_integer
               min_integer integer_rounding_function character
               debug max_arity unknown double_quotes
               prolog_flag
	       generate_debug_info
               initialization local_initialization
               
               halt

	       ;; Standard operator functors, these symbols need to be in the
	       ;; current module and those are maped en evaluation.
	       ^ :- | #{,}# #{,,}# -> #{\\+}# op2= op2<= == =.. -i> *->
	       #{\\=}# #{\\==}# @< @> @>= @=< is op2: =@= #{\\=@=}#
	       op2+ op2- op1+ op1- #{\\}# op2* op2/ // rem mod div
	       ** << >> #{/\\}# #{\\/}# op2< op2> op2>= op2=< =:= #{=\\=}#
	       #{;}# #{;;}# --> ? $ ?- ← → =>
      
	       eval_when
	       *prolog-ops*
	       *term-expansions*
	       *goal-expansions*
	       *swi-standard-operators*
	       min max abs
	      )

  #:export (make-unbound-term 
	    default_module
	    re-export-iso-operators
	    ;SWI Stuff
	    set set_x #;gcd
	    $member $append pp_dyn
            dyntrace untr
	    ))


(define reset-flags init-flags)
(define (reset-prolog)
  (reset-flags)
  (reset-char-conversion)
  (reset-operator-map))

(define-syntax-rule (make-unbound-term a)
  (begin    
    (define a (make-unbound-fkn 'a))
    (set-procedure-property! a 'name 'a)))

(define $member member)
(define $append append)

(<define> (default_module)
  (<code> (set-current-module	   
           ((language-make-default-environment (lookup-language 'prolog))))))

(<define> (set x y)   (<set> x y))
(<define> (set_x x y) (<set!> x y))

(define-syntax-rule (re-export-iso-operators)
  (re-export ^ op1:- :- #{,}# -> #{\\+}# op2= == =..
	     #{\\=}# #{\\==}# @< @> @>= @=< is
	     op2+ op2- op1- #{\\\\}# op2* op2/ // op2rem op2mod
	     ** << >> #{/\\}# #{/\\}# op2< op2> op2>= op2=< =:= #{=\\=}#
	     #{;}#))



(compile-prolog-string 
"
unique([X|L],LL) :- member(X,L) -> unique(L,LL)
                                 ; ([X|LLL] = LL, unique(L,LLL)).
unique([],[]).

sort(X,L) :- msort(X,LL),unique(LL,L).
")

(<define> (pp_dyn x y) (<pp-dyn> x y))
(define-guile-log load
  (lambda (x) 
    (syntax-case x ()
      ((load w x)
       #'(parse<> w (<code> (load-prolog S (<lookup> x)))))
      (load
       #'(let ((load (lambda (s p cc x) 
		       (load-prolog s (gp-lookup x s))
		       (cc s p))))
	   load)))))


(define original (make-hash-table))
(define-syntax-rule (aif (r) p a b) (let ((r p)) (if r a b)))
(define i 0)
(<define> (wr)
  (<recur> lp ((i i) (n 0))
    (if (> i 20)
	(lp (- i 20) (+ n 20))
	(<and>
	 (write n)
	 (<recur> lp ((i i))
	   (if (> i 0)
	       (<and>
		(write ".")
		(lp (- i 1)))
	       <cc>))))))

(<define*> (dyntrace f #:optional (simple? #f))
  (<code>
   (aif (r) (hashq-ref original f #f)
	#t
	(aif (r) (procedure-property f 'debug-fkn)
	     (let* ((ff  (r))
		    (n   (procedure-name f))
		    (v   (lambda (x) (if simple? n (vector (cons f x)))))
		    (new (<lambda> x
			   (<dyn>
			    (<with-log-in-code>
			     (<code> (set! i (+ i 1)))
			     (wr)
			     (write `(enter +  ,(v x)))
			     (nl))
			    (<with-log-in-code>
			     (wr)
			     (write `(enter - ,(v x)))
			     (nl)
			     (<code> (set! i (- i 1)))))
			   (<code> (set! i (+ i 1)))
			   (wr)
			   (write `(enter + ,(v x))) (nl)
			   (<apply> ff x)
			   (<dyn>
			    (<with-log-in-code>
			     (wr)
			     (write `(leave +  ,(v x)))
			     (nl)
			     (<code> (set! i (- i 1))))
			    (<with-log-in-code>
			     (<code> (set! i (+ i 1)))
			     (wr)
			     (write `(leave - ,(v x)))
			     (nl)))
			   (wr)
			   (write `(leave + ,(v x)))
			   (<code> (set! i (- i 1)))
			   (nl))))
	       (hashq-set! original f ff)
	       (r new))
	     #t))))
	  
(<define> (untr f)
  (<code>
   (aif (r) (hashq-ref original f #f)
	(aif (s) (procedure-property f 'debug-fkn)
	     (begin
	       (s r)
	       (hashq-set! original f #f))
	     #t)
	#t)))
	     
(set-procedure-property! list 'name 'list)
(export-scm)

(set! (@@ (logic guile-log prolog error) ecp) 
  (@@ (logic guile-log guile-prolog copy-term) cp))
(set! (@@ (logic guile-log prompts) cp)
  (@@ (logic guile-log guile-prolog copy-term) cp))


(define (cc s p c . l) (apply c s p l))
