(define-module (logic guile-log dynlist)
  #:use-module (ice-9 match)
  #:export (make-dynlist dynlist-add fold-dynlist-lr fold-dynlist-rl
			 walk-dynlist-lr walk-dynlist-rl
			 dynlist-remove
			 replace-dynlist-lr))
#|
This will functionally build up a tree keeping it balanced and allow for quite
efficient lookup

tree format
tree:
0 : (#f #f -1)
1 : (d1 x1  0)
2 : ((#f #f -1)  2 d12   . (x2 . x1)
3 : ((d3 x3 0)   2 d12   . (x2 . x1)
4 : ((#f #f -1)  4 d1234 . ((d34 x4 . x3) . (d12 x2 . x1)))
5 : ((d5 x5  0)  4 d1234 . ((d43 x4 . x3) . (d12 x2 . x1)))   
6 : (((#f #f -1) 2 . (d65 x6 . x5)) 4 d1234 . ((d43 x4 . x3) . (d12 x2 . x1)))
7 : (((d7 x7  0) 2 . (d65 x6 . x5)) 4 d1234 . ((d43 x4 . x3) . (d12 x2 . x1)))
8 : ((#f #f -1) 8 d12345678 
       (d5678 (d78 x8 . x7) . (d56 x6 . x5))
       .
       (d1234 (d34 x4 . x3) . (d12 x2 . x1)))

xi = #(val x)
|#

(define default (case-lambda (() #f) ((x) (values #f x)) ((x y) #f)))
(define-inlinable (val-ref x) 
  (cond 
   ((vector? x) (vector-ref x 0))
   ((pair?   x) (car x))
   (else        #f)))
(define k `(#f #f -1 #f))
(define (make-dynlist) k)
(define* (dynlist-add tree x #:optional (comp default))
  (define val 0)
  (define (lp tree x)
    (match tree
      ((l data-all . (and rest (nr data-r . r)))
       (if (>= nr 1)
	   (call-with-values (lambda () (lp l x))
	     (lambda (l nl data-l fix?)	      
	       (if (= nl nr)
		   (let ((nnr (+ nl nr))
			 (dr  (comp data-r data-l)))
		     (values `(,(cons data-l l) ,data-r . ,r) nnr dr #t))
		   (let ((c (comp data-l data-r)))
		     (if fix?		       
			 (values `((,k ,data-l ,nl ,data-l . ,l) ,c . ,rest) 
				 0 c #f)
			 (values `(,l ,c . ,rest) 0  c #f))))))
	   (call-with-values (lambda () (comp x)) 
	     (lambda (val leaf)
	       (case nr	
		 ((-1) 
		  (values `(,leaf ,val 0 ,val) 0 val #f))
		 ((0)
		  (let ((vval (comp val (val-ref l))))
		    (values 
		     `(,leaf . ,l) 2 vval #t))))))))))
  
  (call-with-values (lambda () (lp tree x))
    (lambda (tree n d fix?)
      (if fix?
          `(,k ,d ,n ,d . ,tree)
          tree))))

(define succeed (lambda (x) #t))
(define* (fold-dynlist-rl f tree seed #:optional (dive? succeed))
  (define (fold-dynlist-rl-r r seed)
    (match r
      ((l) 
       (f l seed))
      ((d l . r)
       (if (dive? d)
	   (fold-dynlist-rl-r l (fold-dynlist-rl-r r seed))
	   seed))
      (l
       (if (dive? (val-ref l))
	   (f l seed)
	   seed))))

  (let lp ((tree tree) (seed seed))
    (match tree
      ((_ _ -1 _) 
       seed)
      ((l _ 0 d)
       (if (dive? d)
	   (f l seed)
	   seed))
      ((l d n d)
       (if (dive? d)
	   (lp l seed)
	   seed))
      ((l d n . r)
       (if (dive? d)
	   (lp l (fold-dynlist-rl-r r seed))
	   seed)))))

(define* (fold-dynlist-lr f tree seed  #:optional (dive? succeed))
  (define (fold-dynlist-lr-r r seed)
    (match r
      ((l) 
       (f l seed))
      ((d l . r)
       (if (dive? d)
	   (fold-dynlist-lr-r r (fold-dynlist-lr-r l seed))
	   seed))
      (l 
       (if (dive? (val-ref l))
	   (f l seed)
	   seed))))

  (let lp ((tree tree) (seed seed))
    (match tree
      ((_ _ -1 _)
       seed)
      ((l d 0 _)
       (if (dive? d)
	   (f l seed)
	   seed))
      ((l d n _)
       (if (dive? d)
	   (lp l seed)
	   seed))
      ((l d n . r)
       (if (dive? d)
	   (fold-dynlist-lr-r r (lp l seed))
	   seed))

      (l      
       (if (dive? (val-ref l))
	   (f l seed)
	   seed)))))

(define (rebuild tree)
  (fold-dynlist-rl 
   (lambda (x seed)
     (if x (dynlist-add seed x) seed))
   tree
   (make-dynlist)))
		      
(define* (remove-raw pred tree one? #:optional (co default))
  (define never (lambda (x) #f))
  (define (do-one x v)
    (if x
	(if (pred x)
	    (begin 
              (if one? (set! pred never)) 
              (values #f #t 0 1 (co)))
	    (values v  #f 1 1 (co v)))
	(values #f #t 0 1 (co))))

  (define-syntax-rule (comb-l x y n d)
    (if x
	(if y
	    `(,x ,d ,n . ,y)
	    `(,x 1))
	(if y
	    `(,k ,d ,n . ,y)
	    #f)))

  (define-syntax-rule (comb-r x y n d)
    (if x
	(if y
	    `(,d ,x . ,y)
	    x)
	(if y
	    y
	    #f)))

  (define-syntax-rule (sum tree comb x y N)
    (call-with-values (lambda () x)
      (lambda (val-x touch-x k-x n-x d-x)
	(call-with-values (lambda () y)
	  (lambda (val-y touch-y k-y n-y d-y)
	    (let ((k     (+ k-x k-y))
		  (d     (co d-x d-y))
		  (touch (or touch-x touch-y)))
	      (if (not touch)
		  (values tree #f k N)
		  (if (or val-x val-y)
		      (values (comb val-x val-y N d) #t k N d)
		      (values #f                     #t 0 1 (co))))))))))
		      
  (define (remove-r tree)
    (match tree
      ((_ l . r)
       (sum tree comb-r (remove-r l) (remove-r r) 1))
      (l 
       (do-one l l))))
  
  (define (remove-l tree)
    (match tree
      ((l _ n . r)
       (if (>= n 1)
	   (sum tree comb-l (remove-raw pred l one?) (remove-r r) n)
	   (case n
	     ((-1) (values tree #f 0 0))
	     ((0)  (do-one l tree)))))))


  (remove-l tree))

(define (dynlist-remove pred tree one?)
  (call-with-values (lambda () (remove-raw pred tree one?))
    (lambda (val touch k n)
      (if touch	  
	  (if val 
	      (if (< (+ k k) n)
		  (values val #t)
		  (values val #f))
	      (values (make-dynlist) #f))
	  (values tree #f)))))


(define* (walk-dynlist-lr p f tree  #:optional (dive? succeed))
  (define (lp-r p tree)
    (match tree	   
      ((d l . r)   
       (if (dive? d)
	   (lp-r (lambda () (lp-r p r)) l)
	   (p)))
      (#f 
       (p))
      (x
       (if (dive? (val-ref x))
	   (f p x)
	   (p)))))
       
  (define (lp p tree)
    (match tree
      ((_ _ -1 _)
       (p))

      ((x d 0 _)
       (if (dive? d)
	   (f p x)
	   (p)))

      ((l d n . r)
       (if (dive? d)
	   (lp (lambda () (lp-r p r)) l)
	   (p)))))
		 
  (lp p tree))

(define* (walk-dynlist-rl p f tree  #:optional (dive? succeed))
  (define (lp-r p tree)
    (match tree	   
      ((d l . r)
       (if (dive? d)
	   (lp-r (lambda () (lp-r p l)) r)
	   (p)))
      (#f 
       (p))
      (x 
       (if (dive? (val-ref x))
	   (f p x)
	   (p)))))
       
  (define (lp p tree)
    (match tree
      ((_ _ -1 _) 
       (p))
      ((x d 0 _)
       (if (dive? d)
	   (f p x)
	   (p)))
      ((l d n . r)
       (if (dive? d)
	   (lp-r (lambda () (lp p l)) r)
	   (p)))))		 
  (lp p tree))

(define* (replace-dynlist-lr tree replace 
			     #:optional (dive? succeed) (comb default))
   (define (lp-r tree)
     (match tree	   
       ((d l . r)   
	(if (dive? d)
 	    (call-with-values (lambda () (lp-r l))
	      (lambda (val-l dl)
		(if (eq? val-l l)
		    (call-with-values (lambda () (lp-r r))
		      (lambda (val-r dr)
			(if (and (eq? val-l l) (eq? val-r r))
			    (values tree d)
			    (let ((d (comb dl dr)))
			      (values (cons* d val-l val-r) d)))))
		    (let ((d (comb dl (val-ref r))))
		      (values (cons* d val-l r) d)))))
	    (values tree d)))

       (#f 
	(values #f (comb)))

       (x
	(if (dive? (val-ref x))
	    (let ((val (replace x)))
	      (values x (val-ref x)))
	    (values x (val-ref x))))))
       
  (define (lp tree)
    (match tree
      ((_ _ -1 _)
       (values tree (comb)))

      ((x d 0 _)
       (if (dive? d)
	   (let ((x (replace x)))
	     (values x (val-ref x)))
	   (values x (val-ref x))))

      ((l d n . r)
	(if (dive? d)
 	    (call-with-values (lambda () (lp l))
	      (lambda (val-l dl)
		(if (eq? val-l l)
		    (call-with-values (lambda () (lp-r r))
		      (lambda (val-r dr)
			(if (and (eq? val-l l) (eq? val-r r))
			    (values tree d)
			    (let ((d (comb dl dr)))
			      (values (cons* val-l d n val-r) d)))))
		    (let ((d (comb dl (val-ref r))))
		      (values (cons* val-l d n r) d)))))
	    (values tree d)))))
		 
  (lp tree))


