(define-module (logic guile-log type)
  #:use-module (logic guile-log)
  #:use-module ((logic guile-log umatch)
		#:select (gp-attvar-raw? set-attribute-cstor! gp-lookup
					 gp-make-var gp-get-attr gp-var?
                                         gp-make-var
					 gp-attvar?))
  
  #:use-module (logic guile-log inheritance)
  #:replace (type)
  #:export (type? Type mk-type))

(define-syntax-rule (aif it p a b) (let ((it p)) (if it a b)))

(define cons--
  (case-lambda
    (() '())
    ((a . b)
     (cons a (apply cons-- b)))))

(define cons-
  (case-lambda
    ((x) (apply cons-- x))
    (()  '())
    ((x y . l)
     (cons x (apply cons- y l)))))

(define Type
  (<case-lambda>
   ((V)
    (<=> V "nonvar"))
   
   ((Var V YY Write)    
    (<=> Write "write")
    (let ((V (<lookup> V)))
      ;;(<=> YY ,(vector (cons- type Var (reverse-lookup (car V)) (cdr V))))))
      (<=> YY ,(vector (cons- type Var
                              (aif it (reverse-lookup (car V)) it (car V))
                              (cdr V))))))
   
   ((me val pred?)
    (let ((val (<lookup> val))
	  (pred? (<lookup> pred?)))
      (cond
       ((eq? pred? #t)
        (if (gp-attvar-raw? (<lookup> val) S)
            (<var> (v)
	      (<if> (<get-attr> val Type v)
                    (<<match>> (#:mode -) (me v)
                      ((f . l) (ff . ll)
                       (let ((f  (<lookup> f))
                             (ff (<lookup> ff)))
                         (if (set< ff f)
                             (<=> l ll))))
                             
                      (f ff
                         (<and>
                          (when (set< f ff)))))))
            (when (<var?> val)
	      (<put-attr> val Type (<lookup> me)))))
	       
       ((eq? pred? #f)
	(if (gp-attvar? (<lookup> val) S)
	    (<var> (v)
	      (<if> (<get-attr> val Type v)
		    (<<match>> (#:mode -) (me v)
		      ((f . l) (ff . ll)
                       (let ((f  (<lookup> f))
                             (ff (<lookup> ff)))
                         (if (set< ff f)
                             (<=> l ll)
                             (if (set< ff f)
                                 (<and>
                                  (<put-attr> val Type (cons f l))
                                  (<==> l ll))))))
                       (f ff
                          (when (set< f ff))))))))
       (else
        <fail>))))
   
   (x <fail>)))

(<define> (pisType a tail x)
  (<var> (r)
    (<get-attr> x Type r)
    (<=> a (,(vector
              (apply list type x
                     (let ((r (<lookup> r)))
                       (if (pair? r)
                           r
                           (list r)))))
            . tail))))

;(set-attribute-cstor! Type pisType)

(<define> (type x f . l)
  (let ((x (<lookup> x))
	(f (aif it (lookup (<lookup> f)) it (<lookup> f))))
    (<var> (v)
      (<if> (<get-attr> x Type v)
	    (let* ((v  (<lookup> v))
		   (ff (car v))
		   (ll (cdr v)))
	      (if (set< f ff)
		  (<=> l ll)))
	    (<put-attr> x Type (cons f l))))))


(define (type? s)
  (lambda (x)
    (and (gp-attvar-raw? x s)
	 (gp-get-attr x Type s))))

(<define> (mk-type x y)
 (let ((v (gp-make-var)))
   (<put-attr!> v Type (list (<scm> x)))
   (<set> y v)))
            
          

((@@ (logic guile-log code-load) set-type-attribute!) Type)
