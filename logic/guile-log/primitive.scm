(define-module (logic guile-log primitive)
  #:use-module (system vm program)
  #:use-module (logic guile-log code-load)
  #:export (get-primitive primitive?))

(define m (make-hash-table))

(define (primitive? x)
  (and (procedure? x)
       (program? x)
       (primitive-code? (program-code x))))

(define f
  (lambda (k v)
    (if (and (variable? v) (variable-bound? v) (primitive? (variable-ref v)))
	(hash-set! m k (variable-ref v)))))
  
(module-for-each f (resolve-module '(guile)))
(module-for-each f (resolve-module '(logic guile-log code-load)))
      

(define (get-primitive x)
  (define nm (if (procedure? x) (procedure-name x) x))
  (hash-ref m nm #f))
      
