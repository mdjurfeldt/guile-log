(define-module (logic guile-log parser)
  #:use-module (logic guile-log parsing scanner)
  #:use-module (logic guile-log)
  #:use-module (logic guile-log vlist)
  #:use-module (logic guile-log fstream)
  #:use-module (logic guile-log umatch)
  #:use-module ((ice-9 match) #:renamer (symbol-prefix-proc 'ice:))
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 pretty-print)
  #:export (pmatch plambda X XL N M <p-define> <p-lambda> .. xx <p-cc>
             s-scan-item
             f-test f-test!  f-char f-read
             f-char!
             clear-tokens mk-token tok-ws* tok-ws* Ds
             s* s+ fn f< f> f-pk f-do-nl
             mk-simple-token p-freeze
             make-file-reader file-next-line file-skip
             pr-char pr-test pr-reg
             s-tag s-tag! pr-tag s-clear-body pr-not
             s-rpl f-rpl s-tr f-tr
             f-and f-and! f-and!! 
             f-or  f-or! f-not* f-not f-not!
             f-seq f-seq! f-seq!!
             f* f+ f? f-tag f-tag! f-tag-pr	   
             f-reg f-reg! f-reg-pr
             f-false f-true f-id
             f-eof f-nl f-nl! f-nl-pr
	     f-line f-cons f-list f-append f-cons* ff? ff* ff+ f-out
             parse parse-no-clear
             *current-file-parsing*
             *translator* *whitespace*
	     setup-parser
	     f-clear-body
	     f-ichar))
 
(define *translator*           (make-fluid (lambda (x) x)))
(define *current-file-parsing* (make-fluid 'repl))
(define *whitespace* (make-fluid (lambda (s p cc . x) (apply cc s p x))))

(define do-print #f)
(define pp
  (case-lambda
   ((s x)
    (when do-print
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when do-print
      (pretty-print (syntax->datum x)))
    x)))

(define ppp
  (case-lambda
   ((s x)
    (when #t
      (pretty-print `(,s ,(syntax->datum x))))
    x)
   ((x)
    (when #t
      (pretty-print (syntax->datum x)))
    x)))

(define-syntax-rule (Ds f) (lambda x (apply f x)))

(define *freeze-map*   #f)

(define clear-tokens
  (let ((fl (make-fluid vlist-null)))
    (set! *freeze-map* fl)
    (lambda ()
      (let ((ht vlist-null))
	(fluid-set! fl ht)))))

(clear-tokens)

(define make-file-reader 
  (case-lambda
   ((stream)
    (if (port? stream)
	(rw-fstream-from-port stream)
	(let ((s (fluid-ref stream)))
	  (cond
	   ((fstream-rw? s)
	    s)
	   ((fstream-r? s)
	    (r->rw-fstream s))))))
   (()
    (make-file-reader (current-input-port)))))

(define (print-last-line n xl)
  (if (and do-print (not (feof? xl)))
      (if (and (fluid? xl) (fstream-rw? (fluid-ref xl)))
	  (format #t "endline (~a) : ~a~%" n
		  (list->string (reverse (freadline-l xl)))))))

(define file-next-line  
  (case-lambda 
   ((data i)
    (error "not supported file-next-line verison"))
   ((data)   
    (freadline data))))

(define (file-skip n data) ((caddr data) (+ n (car data))))

(define-guile-log-parser-tool (<p-lambda> (X XL N M)) <p-define> .. xx <p-cc>)

(make-guile-log-scanner-tools <p-lambda> <fail> <p-cc> <succeds> .. 
			      (X XL N M)
			      (c) (d)
			      s-false s-true s-mk-seq s-mk-and s-mk-or)


(define wrap-n
  (lambda (f)
    (lambda (s p cc x xl n m . u)
      (let ((c  (cdr (reverse u)))
	    (cc (lambda (s p x xl n m c) (cc s p x xl n m 
					      (reverse 
					       (cons c 
						     (cdr (reverse u))))))))
      (f s p cc x xl n m c)))))

(define f-nl
  (lambda (s p cc x xl n m . u)
    (ice:match x
      ('()
       (call-with-values (lambda () (freadline xl))
	 (lambda (xl x)
	   (if (not (and (null? x) (feof? xl)))
	       (apply cc s p x xl 0 (+ m 1) u)
	       (p)))))
      (_ (p)))))


(define f-nl!
  (wrap-n
  (lambda (s p cc x xl n m c)
    (ice:match x
      ('()
       (call-with-values (lambda () (freadline xl))
	 (lambda (xl x)
	   (if (not (and (null? x) (feof? xl)))
	       (let* ((v (gp-var! s))
		      (s (gp-unify! c (cons #\newline v) s)))
		 (if s
		     (cc s p x xl 0 (+ m 1) v)
		     (p)))
	       (p)))))
      (_ (p))))))


(define f-nl-pr
  (lambda (s p cc x xl n m . u)
    (ice:match x
      ('()
       (call-with-values (lambda () (freadline xl))
	 (lambda (xl x)
	   (if (not (and (null? x) (feof? xl)))
	       (begin
		 (format #t "~%")
		 (apply cc s p x xl 0 (+ m 1) u))
	       (p)))))
      (_ (p)))))


(define f-do-nl
  (lambda (s p cc x xl n m . u)
    (call-with-values (lambda () (freadline xl))
      (lambda (xl x)
	(if (not (feof? xl))
	    (apply cc s p x xl 0 (+ m 1) u)
	    (p))))))

(define (s-scan-item nn k)
  (let ((i k))
    (lambda (f)
       (lambda (s p cc x xl n m . u)
	 (if (= i 0)
	     (begin
	       (set! i k)
	       (let ((xl-save xl))
		 (apply f s p (lambda (s p x xl n m . u)
			  (file-skip nn xl-save)
			  (apply cc s p x xl n m u))
		    x xl n m u)))
	     (begin
	       (set! i (- i 1))
	       (apply f s p cc x xl n m u)))))))


(define-syntax-rule (setup-parser-0
		     <p-define> <p-lambda> <fail> <p-cc> <succeds> .. xx
		     X XL ((N NI) (M MI) (XX Init) ...)
		     s-false s-true s-mk-seq s-mk-and s-mk-or
                     f-read f-test f-test! pr-test f-tag f-tag! pr-tag
                     chtr f-id f-pk s-tag s-tag! spr-tag
		     s-seq s-and s-and! s-and!! s-and-i s-or s-or-i
                     f-or f-or! f-and f-and! f-seq f? f+ f* ff? ff+ ff*
                     f-line f-cons f-list f-cons* f-append mk-token p-freeze
		     parse 
                     f-out
                     f-true f-false f-nl ss f< f> fn f-eof
                     <s-match> <s-lambda> mk-simple-token f-clear-body
                     f-char f-char! pr-char f-reg f-reg! pr-reg
                     f-ws f-ws* pr-ws+ tok-ws* tok-ws+ parse-no-clear
                     s-rpl f-rpl s-tr f-tr f-1-char f-1-char! pr-1-char
                     f-not f-not! f-not* pr-not f-seq! f-seq!! f-deb
		     pp do-print f-wrap f-tag-pr f-ichar)
(begin
(define f-read
 (<p-lambda> (c)
   (when (pair? X)
     (<let> ((a (chtr (car X)))
	     (l (cdr X))
	     (n (+ N 1)))
	(<syntax-parameterize> ((X (lambda x #'l))
				(N (lambda x #'n)))
	  (<p-cc> a))))))


			  
(define (f-test! f)
 (<p-lambda> (c)
   (when (pair? X)
     (<let> ((a (chtr (car X)))
	     (l (cdr X))
	     (n (+ N 1)))
	(<match> (#:mode + #:name f-test!) (X c)    
	   (((= chtr (? f ch)) . l) (ch . cc)
	    (<syntax-parameterize> ((X (lambda x #'l))
				    (N (lambda x #'n)))
			 
	      (<p-cc> cc)))
	   (_  _ (<cut> <fail>)))))))

(define (f-test f)
 (<p-lambda> (c)
   (when (pair? X)
     (<let> ((a (chtr (car X)))
	     (l (cdr X))
	     (n (+ N 1)))
	(<match> (#:mode + #:name f-test) (X)    
	   (((= chtr (? f ch)) . l)
	    (<syntax-parameterize> ((X (lambda x #'l))
				    (N (lambda x #'n)))
			 
	      (<p-cc> c)))
	   (_ (<cut> <fail>)))))))

(define (pr-test f)
 (<p-lambda> (c)
   (when (pair? X)
     (<let> ((a (chtr (car X)))
	     (l (cdr X))
	     (n (+ N 1)))
	(<match> (#:mode + #:name pr-test!) (X)    
	   (((= chtr (? f ch)) . l)
	    (<syntax-parameterize> ((X (lambda x #'l))
				    (N (lambda x #'n)))
	      (<format> #t "~a" ch)
	      (<p-cc> c)))
	   (_ (<cut> <fail>)))))))


(define (chtr x) ((fluid-ref *translator*) x))

(<p-define> (f-id c) (<p-cc> c))

(s-mk-seq s-seq   <and>)

(s-mk-and s-and   <and>)
(s-mk-and s-and!  <and!>)
(s-mk-and s-and!! <and!!>)
(s-mk-and s-and-i <and-i>)

(s-mk-or  s-or    <or>)
(s-mk-or  s-or-i  <or-i>)

(define (head-n n x)
  (let lp ((i n) (x x) (r '()))
    (if (= i 0)
        (reverse r)
        (if (pair? x)
            (lp (- i 1) (cdr x) (cons (car x) r))
            (reverse r)))))

(define (f-pk nm)
  (<p-lambda> (c)
    (<pp-dyn> `(,nm ,(head-n 10 X) ,c) `(leaving ,nm))
    (<p-cc> c)))

(define (f-deb nm)
  (<p-lambda> (c)
    (if do-print
        (<pp-dyn> `(,nm ,X ,c) `(leaving ,nm))
        <cc>)
    (<p-cc> c)))

(define (f-wrap s f)
  (case-lambda
    ((x . l)
     (if (symbol? x)
         (s-seq (f-deb `(,x ,s)) (apply f l) (f-deb `(success ,x ,s)))
         (apply f x l)))
    (()
     (f))))

(define (ss x)
  (cond
   ((string? x)
    (let ((ws (fluid-ref *whitespace*)))
      (f-seq ws (f-tag x) ws)))
   ((procedure?  x)
    x)
   (else
    (f-out x))))

(define f-and
  (f-wrap 'f-and
    (case-lambda 
      ((f)    (ss f))
      ((f g)  (s-and (ss f) (ss g)))
      ((f g . l) (s-and (ss f) (ss g) (apply f-and l)))
      (()     s-true))))

(define f-and!
  (f-wrap 'f-and!
    (lambda l
      (s-and! (apply f-and l)))))

(define f-or
  (f-wrap 'f-or
   (case-lambda 
     ((f)       (ss f))
     ((f g)     (s-or (ss f) (ss g)))
     ((f g . l) (s-or (ss f) (ss g) (apply f-or l)))
     (()         s-false))))

(define f-or!
  (f-wrap 'f-or!
   (lambda x
     (f-and! (apply f-or  x)))))

(define (f-out tag)
  (lambda (s p cc . x)
    (apply cc s p (reverse (cons tag (cdr (reverse x)))))))
    
(define (f> f n)    (letrec ((ret (lambda (n)
				    (if (= n 0)
					s-true
					(s-seq f (ret (- n 1)))))))
		      (ret n)))
(define (f< f n)    (letrec ((ret (lambda (n)
				    (if (= n 0)
					s-true
					(f-or! (s-seq f (ret (- n 1)))
					       s-true)))))
		      (ret n)))
(define (fn f n m) (s-and! (s-seq (f> f n) (f< f (- m n)))))

(define-guile-log  <s-match>
  (syntax-rules ()
    ((<s-match>  w (x xl n m c) (p1 p2 code (... ...)) (... ...))
     (<match> w (#:mode + #:name '<s-match>) (x c)
       ((* * p1) p2 (<cut> (<and> code (... ...))))
       (... ...)
       (_ _   (<cut> <fail>))))))

(define-syntax-rule (<s-lambda> (x xl n m c) . l)
  (<lambda> (x xl n m c)
    (<s-match> (x xl n m c) . l)))

(define (mk-simple-token nm f)
 (<p-lambda> (x)
   (<var> (ret cin)
     (.. (cout) (f cin))
     (<=> cout '())
     (<=> ret (nm ,(list->string (<scm> cin))))
     (<p-cc> ret))))

(<p-define> (f-eof cin)
   (if (and (null? X)
	    (call-with-values (lambda () (freadline XL))
	      (lambda (xl x) (feof? xl))))
       (<p-cc> cin)
       <fail>))

(define (f-clear-body f)
  (<p-lambda> (c)
    (<let> ((op P)
	    (os S)
	    (p  (<newframe>)))
      (<with-s> p
        (.. (c) (f c))
	(<code> (<unwind-tail> p))
	(<with-fail> op
	(<with-s>    os
	  (<p-cc> c)))))))

(define (f-char!  ch) (f-test! (lambda (x) (eq? x ch))))   
(define (f-char   ch) (f-test  (lambda (x) (eq? x ch))))   
(define (pr-char  ch) (pr-test (lambda (x) (eq? x ch))))   


(define (char->string x) (list->string (list x)))
(define (f-reg pat-str)
  (let ((reg (make-regexp pat-str)))
    (f-test (lambda (x) 
	      (let ((x (char->string x)))
		(regexp-exec reg x))))))

(define (f-reg! pat-str)
  (let ((reg (make-regexp pat-str)))
    (f-test! (lambda (x) 
	       (let ((x (char->string x)))
		 (regexp-exec reg x))))))

(define (pr-reg pat-str)
  (let ((reg (make-regexp pat-str)))
    (pr-test (lambda (x) 
	       (let ((x (char->string x)))
		 (regexp-exec reg x))))))

;; The idea of this function is to perform a tokenizing activity
;; utilizing this means that we loose the ability to redo and undo
;; inside the scanner part.
;; This is not ass effective as a regexp tokenizer but should be a
;; much faster then doing a full parser of everything.

(define (mk-token f)
 (<p-lambda> (c)
    (<var> (cnew)
       (.. (cc) (f cnew))	       
       (<=> cc '())
       (<let*> ((x   (<scm> cnew))
		(ret (if (gp-var? x S) "" (list->string x))))
	 (<p-cc> ret)))))


(define (p-freeze tok f mk)
  (<p-lambda> (c)
     (<and!>
      (<let> ((val 
               (vhash-ref (fluid-ref *freeze-map*)
                          (cons* N M tok) #f))
	      (op  P)
	      (os  S)
	      (fr  (<newframe>)))
	 (if (not val)
	     (<let> ((n N) (m M))
	       (<or>
                (<and>
                 (.. (cc) (f c))
                 (<let> ((val2 (mk S c cc)))		   
                   (<code>
                    (<unwind-tail> fr))
		   (<with-fail> op
		   (<with-s>    os
                    (<code>
                     (fluid-set!
                      *freeze-map*                
                      (vhash-cons 
                       (cons* n m tok) 
                       (list X XL N M XX ... val2)
                       (fluid-ref *freeze-map*))))
                    (<p-cc> val2)))))
                (<let> ((val2 'fail))
                  (<code>
                   (fluid-set!
                    *freeze-map*                
                    (vhash-cons 		   
                     (cons* n m tok) val2
                     (fluid-ref *freeze-map*))))
                  <fail>)))
	     (if (pair? val)
		 (<and>
		  (<code> (gp-unwind-tail fr))
		  (<with-fail> op
		  (<with-s>    os
		   (<apply> f-true val))))
		 <fail>))))))

(define (f* f) (letrec ((ret (f-or (s-seq f ret) s-true))) (s-and! ret)))
(define (f+ f) (s-seq f (f* f)))
(define f-ws  (f-or! (f-char #\space) f-nl (f-char #\tab)))
(define f-ws* (f* f-ws))
(define f-ws+ (f+ f-ws))

(define tok-ws* (f-or!
		 (p-freeze 'ws* (mk-token (s-seq f-ws f-ws f-ws*))
			   (lambda (s cin cout) cin))
		 f-ws*))

(define tok-ws+ (f-or!
		 (p-freeze 'ws+ (mk-token (s-seq f-ws f-ws f-ws*))
			   (lambda (s cin cout) cin))
		 f-ws+))

(define parse*
  (case-lambda
   ((str matcher)
    (define f
      (lambda (stream)
        (with-fluids ((*freeze-map* (fluid-ref *freeze-map*)))
          (clear-tokens)
          ((<lambda> ()
	    (<values> (x xl n . l)
			(matcher '() (make-file-reader stream) 
				 NI MI Init ... <_>))
	      (<code> (print-last-line n (if xl xl '())))
	      (<cc> (car (reverse l))))
	   (fluid-ref *current-stack*)
	   (lambda () #f)
	   (lambda (s p cc) cc)))))
    (if (string? str)
	(f (make-fluid (rw-fstream-from-string str)))
	(f str)))

   ((matcher)
    (with-fluids ((*freeze-map* (fluid-ref *freeze-map*)))
      (clear-tokens)
      ((<lambda> ()
	(<and>
	 (<values> (x xl n . l)
		   (matcher '() (make-file-reader) NI MI Init ... <_>))
	 (<code> (print-last-line n (if xl xl '())))
	 (<cc> (car (reverse l)))))
       (fluid-ref *current-stack*)
       (lambda () #f)
       (lambda (s p cc) cc))))))

(define parse
  (case-lambda
    ((str matcher)
     (parse* str (f-seq f-nl matcher)))
    ((matcher)
     (parse* matcher))))

(define (equalize stream x xl nstart)
  (define n (- (rw-fstream-n xl) (length x)))
  (if (port? stream)
      (seek stream (+ nstart n) SEEK_SET)
      (let ((s (fluid-ref stream)))
	(fluid-set! stream (fmove s n)))))

(define (get-pos stream)
  (cond
   ((port? stream)
    (file-position stream))
   ((fluid? stream)
    (let ((s (fluid-ref stream)))
      (if (fstream? s)
	  (fposition s)
	  0)))
   (else
    0)))

(define parse-no-clear
  (case-lambda
   ((stream str matcher)
    (define N (get-pos stream))
    (define f
      (lambda ()
	(with-fluids ((*freeze-map* (fluid-ref *freeze-map*)))
	  (clear-tokens)
	  (<run> 1 (cout) 
		 (<values> (x xl n m out)
			   (matcher '() (make-file-reader stream) 0 0 <_>))
		 (<code> 
		  (equalize stream x xl N)
		  (print-last-line n xl))
		 (<=> cout out)))))

    (if (string? str)
	(with-input-from-string str f)
	(with-input-from-port   str f)))

   ((stream matcher)
    (define N (get-pos stream))
    (with-fluids ((*freeze-map* (fluid-ref *freeze-map*)))
      (clear-tokens)
      (<run> 1 (cout) 
	   (<values> (x xl n m out)
		     (matcher '() (make-file-reader stream) 0 0 <_>))
           (<code>
	    (equalize stream x xl N) 
	    (print-last-line n xl))
	   (<=> cout out))))))


(define (s-rpl m y)
  (<p-lambda> (c)
    (.. (c) (m c))
    (<format> #t "~a" y)
    (<p-cc> c)))

(define (f-rpl m f)
  (<p-lambda> (c1)
    (.. (c2) (m c1))
    (<code> (f c2))
    (<p-cc> c2)))


(define-syntax-rule (s-tr x y) (s-rpl (s-tag x) y))
(define-syntax-rule (f-tr x y) (f-rpl (mk-token (s-tag! x)) y))
	    
(define f-1-char  (f-reg  "."))
(define f-1-char! (f-reg! "."))
(define pr-1-char (pr-reg "."))

(define (f-not f)
  (<p-lambda> (c)
    (<not> (<and> (.. (q) (f c))))
    (.. (c) (f-1-char c))
    (<p-cc> c)))

(define (f-not* f)
  (<p-lambda> (c)
    (<not> (<and> (.. (q) (f c))))
    (<p-cc> c)))

(define (f-not! f)
  (<p-lambda> (c)
    (<not> (<and> (.. (q) (f c))))
    (.. (c) (f-1-char! c))
    (<p-cc> c)))

(define (f-ichar ch)
  (let ((ch (if (string? ch) 
		(car (string->list ch))
		ch)))
    (<p-lambda> (c)
      (<var> (v)
	(<=> (ch . v) c)
	(<p-cc> v)))))

(define (pr-not f)
  (<p-lambda> (c)
    (<not> (<and> (.. (q) (f c))))
    (.. (c) (pr-1-char c))
    (<p-cc> c)))

(define f-seq
  (f-wrap 'f-seq
    (case-lambda 
      ((f)       (ss f))
      ((f g)     (s-seq (ss f) (ss g)))
      ((f g . l) (s-seq (ss f) (ss g) (apply f-seq l)))
      (()         s-true))))

(define f-line
  (<p-lambda> (c)
    (<p-cc> (cons M N))))

(define f-cons
  (f-wrap 'f-cons
    (lambda (f g)
    (<p-lambda> (c)
      (.. (c1) ((ss f) c))
      (.. (c2) ((ss g) c1))
      (if (eq? c1 c)
	  (<p-cc> c2)
	  (<p-cc> (cons c1 c2)))))))

(define f-cons*
  (f-wrap 'f-cons*
    (case-lambda
      ((x)    (ss x))
      ((x y)  (f-cons x y))
      ((x y . l)
       (f-cons x (apply f-cons* y l))))))

(define f-list
  (f-wrap 'f-list
    (case-lambda
      ((x)    (f-cons x (f-out '())))
      ((x y)  (f-cons x (f-cons y (f-out '()))))
      ((x y . l)
       (f-cons x (apply f-list y l))))))

(define f-append
  (f-wrap 'f-list
    (case-lambda
      ((x)    x)
      ((f g)  
       (<p-lambda> (c)
         (.. (c1) ((ss f) c))
         (.. (c2) ((ss g) c1))
         (<p-cc> (append c1 c2))))
       
      ((x y . l)
       (f-append x (apply f-append y l))))))


(define f*
  (f-wrap 'f*
    (lambda (x)
      (f-or! (s-seq (s-and! (ss x)) (Ds (f* x))) s-true))))

(define ff*
  (f-wrap 'ff*
   (case-lambda 
     ((x)
      (f-or! (f-cons (f-and! (ss x)) (Ds (ff* x))) (f-out '())))
     ((x d)
      (f-or! (f-cons (f-and! (ss x)) (Ds (ff* x d))) (f-out d))))))

(define f?
  (f-wrap 'f?
    (lambda (x)
      (f-or! (ss x) s-true))))

(define ff? 
  (f-wrap 'ff?
    (case-lambda
      ((x)
       (f-or! (ss x) (f-out #f)))
      ((x default)
       (f-or! (ss x) (f-out default))))))
   

(define f+ 
  (f-wrap 'f+
    (lambda (x)
      (s-seq (ss x) (f* x)))))

(define ff+
  (f-wrap 'ff+
    (lambda (x . l)
      (f-cons (ss x) (apply ff* x l)))))

(define-syntax s-tag
  (lambda (x)
    (syntax-case x ()
      ((_ x)
       (let* ((w (syntax->datum #'x))
	      (w (cond
		  ((symbol? w) (string->list (symbol->string w)))
		  ((string? w) (string->list w))
		  (else #f))))
	 (if w		   		 
	     (with-syntax (((ch (... ...)) w))
               #'(f-seq (f-char ch) (... ...)))
	     (error "argument to s-tag is either string or symbol")))))))

(define-syntax s-tag!
  (lambda (x)
    (syntax-case x ()
      ((_ x)
       (let* ((w (syntax->datum #'x))
	      (w (cond
		  ((symbol? w) (string->list (symbol->string w)))
		  ((string? w) (string->list w))
		  (else #f))))
	 (if w		   		 
	     (with-syntax (((ch (... ...)) w))
		#'(f-seq (f-char! ch) (... ...)))
	     (error "argument to s-tag is either string or symbol")))))))

(define-syntax spr-tag
  (lambda (x)
    (syntax-case x ()
      ((_ x)
       (let* ((w (syntax->datum #'x))
	      (w (cond
		  ((symbol? w) (string->list (symbol->string w)))
		  ((string? w) (string->list w))
		  (else #f))))
	 (if w		   		 
	     (with-syntax (((ch (... ...)) w))
		#'(f-seq (pr-char ch) (... ...)))
	     (error "argument to s-tag is either string or symbol")))))))

(define (f-tag x)
  (let ((l ((@ (guile) map) (lambda (x) (f-char x))
	    (string->list (format #f "~a" x)))))
    (apply f-seq l)))
(define (f-tag! x)
  (let ((l ((@ (guile) map) (lambda (x) (f-char! x))
	    (string->list (format #f "~a" x)))))
    (apply f-seq l)))
(define (f-tag-pr x)
  (let ((l ((@ (guile) map) (lambda (x) (pr-char x))
	    (string->list (format #f "~a" x)))))
    (apply f-seq l)))

(define (f-seq!  . x) (f-and! (apply f-seq x)))
(define (f-seq!! . x) (apply f-seq (map (lambda (x) (f-and! x)) x)))
(define (f-and!! . x) (apply f-and (map (lambda (x) (f-and! x)) x)))

(define f-true s-true)
(define f-false s-false)
))

(eval-when (compile load eval)
  (define names '(f-read f-test f-test! pr-test f-tag f-tag! pr-tag
                         chtr f-id f-pk s-tag s-tag! spr-tag
                         s-seq s-and s-and! s-and!! s-and-i s-or s-or-i
                         f-or f-or! f-and f-and! f-seq f? f+ f* ff? ff+ ff*
                         f-line f-cons f-list f-cons* f-append
                         mk-token p-freeze parse f-out
                         f-true f-false f-nl ss f< f> fn f-eof
                         <s-match> <s-lambda> mk-simple-token f-clear-body
                         f-char f-char! pr-char f-reg f-reg! pr-reg
                         f-ws f-ws* pr-ws+ tok-ws* tok-ws+ parse-no-clear
                         s-rpl f-rpl s-tr f-tr f-1-char f-1-char! pr-1-char
                         f-not f-not! f-not*  pr-not f-seq! f-seq!! f-deb
                         pp do-print f-wrap f-tag-pr
			 f-ichar)))

(define-syntax setup-parser
  (lambda (x)
    (syntax-case x ()
      ((w <p-define> <p-lambda> <fail> <p-cc> <succeds> .. xx 
          X XL ((N NI) ...)
          s-false s-true s-mk-seq s-mk-and s-mk-or)
       
       (with-syntax (((nm ...) (map (lambda (x) 
                                      (datum->syntax #'w x))
                                    names)))
         #'(setup-parser-0
             <p-define> <p-lambda> <fail> <p-cc> <succeds> .. xx 
             X XL ((N NI) ...)
             s-false s-true s-mk-seq s-mk-and s-mk-or
             nm ...))))))

;; Creating the standard parser
(setup-parser  <p-define> <p-lambda> <fail> <p-cc> <succeds> .. xx 
               X XL ((N 0) (M 0))
               s-false s-true s-mk-seq s-mk-and s-mk-or)
