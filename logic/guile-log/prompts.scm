(define-module (logic guile-log prompts)
  #:use-module (logic guile-log macros)
  #:use-module (logic guile-log interleave)
  #:use-module (logic guile-log umatch)
  #:export (<abort> <prompt> <re-prompt> <catch>))
#|
s b [J[#f] x y z]

|#

(define cp gp-cp)

(define-inlinable (fw wind) (gp-rebased-level-ref wind))

(define-inlinable (guard s val wind)	    
  (gp-undo-safe-variable-guard val (fw wind) s))

(define-inlinable (make-env) (make-vector 5 '()))
(define-inlinable (get-env h) (variable-ref h))
(define-inlinable (level-set! env b)
  (vector-set! env 0 b))
(define-inlinable (tag-set! env b)
  (vector-set! env 1 b))
(define-inlinable (h-set! env b)
  (vector-set! env 2 b))
(define-inlinable (next-set! env b)
  (vector-set! env 3 b))
(define-inlinable (old-set! env o)
  (vector-set! env 4 o))

(define-inlinable (level-ref env)
  (vector-ref env 0))
(define-inlinable (tag-ref env)
  (vector-ref env 1))
(define-inlinable (h-ref env)
  (vector-ref env 2))
(define-inlinable (next-ref env)
  (vector-ref env 3))
(define-inlinable (old-ref env)
  (vector-ref env 4))


(define (eval-wind h x)
  (let lp ((h h) (k 0) (as '()))
    (define ref #f)
    (define (cont env as)
      (lp (next-ref env) (+ k (level-ref env)) as))
    (define (cont2 h)
      (let* ((env (get-env h))
	     (old (old-ref h)))
	(if (or (not old) (null? old) ref)
	    (cont env as)
	    (cont env (cons (cons env old) as)))))

    (if (or (eq? h x) (null? h))
	k
	(if (not (null? as))
	    (let ((r (assq h as)))
	      (if r
		  (let ((x (cdr r)))
		    (if (null? x)
			k
			(let ((env (car x)))
			  (set-cdr! r (cdr x))
			  (set! ref #t)
			  (cont env as))))
		  (cont2 h)))
	    (cont2 h)))))
		    
		    
	
(define-syntax-rule (<ddlambda> H ((a v) ...) (x ...) code ...)
  (letrec ((H (lambda (a ...)
		(<lambda> (x ...)
                   code ...))))
    (H v ...)))

(define (<guard> s p cc f args fin fout)
  (apply f s 
	 (lambda () 
	   (fout)
	   (p))
	 (lambda (s p . l) 
	   (fin) 
	   (apply cc s  l))
	 args))

(define-syntax-rule (mk-handle-lam tag wind-start cdr-start
				   h env p-pr cc-pr cc-init
				   handler X)
  (<ddlambda> H ((handler handler) (first #f)) (t next cont-lam h-args)
     (<let*> ((cc-handle CC)
	     (p-handle   P)
	     (s-handle   S)
	     (state (gp-store-state S))
	     (kkk         
	      (lambda (s-ab p-ab cc-ab)
		(letrec 
		    ((f
		      (case-lambda 
		       (()    
			(f tag #f))
		       ((h-x)
			(f tag h-x))
		       ((tag-x h-x)
		  (<lambda> k-args
		    (<let*> ((env-i     (get-env h))
			     (o-i       (old-ref env-i))
			     (o-x       (if o-i
					    (cons env-i o-i)
					    '()))
			     (env-x     (make-env))
			     (sin       S)
			     (ccin      CC)
			     (pin       P)
			     (cdr-i     (next-ref env-i))
			     (cdr-x     (gp-handlers-ref))
			     (cc-pr-i   (variable-ref cc-pr))
			     (p-pr-i    (variable-ref p-pr))
			     (bwind-i   (level-ref env))
			     (wind      (gp-windlevel-ref sin))
			     (bwind-x   (+ (- wind wind-start)
					  (eval-wind cdr-i cdr-start)))
			     (h-out-fkn #f)
			     (state-in  #f)
			     (h-x       (<lambda> x
					   (<code> (h-out-fkn))
					   (<apply> h-x x)))
			     (k-args-cp (cp k-args S)))
		      (<code>
		       (level-set!   env-x bwind-x)
		       (next-set!    env-x (if first
					       cdr-x
					       (next-ref env-i)))
		       (tag-set!     env-x tag-x)
		       (h-set!       env-x (if h-x (H h-x #f) #f))
		       (old-set!     env-x o-x))
		      (<letrec>
			    ((cc-pr-x
			      (lambda (s-re p-re . l)
				(let ((k-args-cp2 (gp-cp k-args-cp s-re))
				      (X-cp       (gp-cp X         s-re))
				      (state-out  (gp-store-state s-re)))

				  (gp-restore-wind state-in (fw wind-start))
				  (f-out)
				  (let* ((s (gp-unify! k-args 
						       k-args-cp2
						       sin))
					 (p (lambda ()
					      (gp-restore-wind
					       state-out (fw wind))
					      (f-in)
					      (p-re))))
				    (if s
					(let ((s (gp-unify! X X-cp s)))
					  (if s
					      (apply ccin s p l)
					      (p)))
					(p))))))
			     (p-pr-x
			      (lambda ()
				(gp-restore-wind state-in (fw wind-start))
				(f-out)
				(pin)))

			     (f-out
			      (lambda () 
				(variable-set! cc-pr  cc-pr-i)
				(variable-set! p-pr   p-pr-i)
				(variable-set! h      env-i)))
			    
			     (f-in
			      (lambda ()
				(variable-set! cc-pr  cc-pr-x)
				(variable-set! p-pr   p-pr-x)
				(variable-set! h      env-x)))

			     (re-handler
			      (<lambda> (t next . l) (next))))
		           
		    (<code>
		     (set! h-out-fkn f-out)
		     (set! state-in (gp-store-state sin))
		     (gp-restore-wind state wind)
		     (f-in))
		    
		     (<with-s>     s-ab
		     (<with-fail>  p-ab
		     (<with-cc>    cc-ab
		       (<apply> cont-lam k-args-cp)))))))))))
		  f))))
	     
	(<code> 
	 (gp-handlers-set! cdr-start))
	(handler t next kkk h-args))))

(define-syntax-rule (mk-handle-lam2 tag wind-start cdr-start
				   h env p-pr cc-pr cc-init
				   handler X)
  (<ddlambda> H ((handler handler) (first #f)) (t next cont-lam h-args)
     (<let*> ((cc-handle CC)
	     (p-handle   P)
	     (s-handle   S)
	     (state (gp-store-state S))
	     (kkk         
	      (lambda (s-ab p-ab cc-ab)
		(letrec 
		    ((f
		      (case-lambda 
		       (()    
			(f tag #f))
		       ((h-x)
			(f tag h-x))
		       ((tag-x h-x)
		  (<lambda> k-args
		    (<let*> ((env-i     (get-env h))
			     (o-i       (old-ref env-i))
			     (o-x       (if o-i
					    (cons env-i o-i)
					    '()))
			     (env-x     (make-env))
			     (sin       S)
			     (ccin      CC)
			     (pin       P)
			     (cdr-i     (next-ref env-i))
			     (cdr-x     (gp-handlers-ref))
			     (cc-pr-i   (variable-ref cc-pr))
			     (p-pr-i    (variable-ref p-pr))
			     (bwind-i   (level-ref env))
			     (wind      (gp-windlevel-ref sin))
			     (bwind-x   (+ (- wind wind-start)
					  (eval-wind cdr-i cdr-start)))
			     (h-out-fkn #f)
			     (state-in  #f)
			     (h-x       (<lambda> x
					   (<code> (h-out-fkn))
					   (<apply> h-x x)))
			     (k-args-cp (cp k-args S)))
		      (<code>
		       (level-set!   env-x bwind-x)
		       (next-set!    env-x (if first
					       cdr-x
					       (next-ref env-i)))
		       (tag-set!     env-x tag-x)
		       (h-set!       env-x (if h-x (H h-x #f) #f))
		       (old-set!     env-x o-x))
		      (<letrec>
			    ((cc-pr-x
			      (lambda (s-re p-re . l)
				(let ((k-args-cp2 (gp-cp k-args-cp s-re))
				      (X-cp       (gp-cp X         s-re))
				      (state-out  (gp-store-state s-re)))

				  (gp-restore-wind state-in (fw wind-start))
				  (f-out)
				  (let* ((s (gp-unify! k-args 
						       k-args-cp2
						       sin))
					 (p (lambda ()
					      (gp-restore-wind
					       state-out (fw wind))
					      (f-in)
					      (p-re))))
				    (if s
					(let ((s (gp-unify! X X-cp s)))
					  (if s
					      (apply ccin s p l)
					      (p)))
					(p))))))
			     (p-pr-x
			      (lambda ()
				(gp-restore-wind state-in (fw wind-start))
				(f-out)
				(pin)))

			     (f-out
			      (lambda () 
				(variable-set! cc-pr  cc-pr-i)
				(variable-set! p-pr   p-pr-i)
				(variable-set! h      env-i)))
			    
			     (f-in
			      (lambda ()
				(variable-set! cc-pr  cc-pr-x)
				(variable-set! p-pr   p-pr-x)
				(variable-set! h      env-x)))

			     (re-handler
			      (<lambda> (t next . l) (next))))
		           
		    (<code>
		     (set! h-out-fkn f-out)
		     (set! state-in (gp-store-state sin))
		     (gp-restore-wind state wind)
		     (f-in))
		    
		     (<with-s>     s-ab
		     (<with-fail>  p-ab
		     (<with-cc>    cc-ab
		       (<apply> cont-lam k-args-cp)))))))))))
		  f))))
	     
	(<code> 
	 (gp-handlers-set! cdr-start))
	(handler t next kkk h-args))))

(define-syntax-rule (mk-handle-lam-catch tag cdr-start
				   h env
				   handler X)
  (<lambda> (t next cont-lam h-args)
     (<let*> ((cc-handle CC)
	      (p-handle  P)
	      (s-handle  S))
      (<code> 
       (gp-handlers-set! cdr-start))
      (handler t next h-args))))

(define (<prompt-raw> s p cc tag X action handler)
  (let* ((p-pr      (make-variable p))
	 (cc-pr     (make-variable cc))
	 (env       (make-env))
	 (h         (make-variable env))
	 (hs-init   (gp-handlers-ref))
         (p-init    (lambda () ((variable-ref p-pr))))
	 (cc-init   (lambda l 
                      #;(gp-handlers-set! hs-init)
                      (apply (variable-ref cc-pr) l))))
    (next-set!  env hs-init)
    (tag-set!   env tag)
    (level-set! env 0)
    (old-set!   env #f)
    (call-with-values
        (lambda () (gp-new-wind-level s))
      (lambda (s wind-init)
        (h-set! env
                (mk-handle-lam tag wind-init hs-init
                               h env p-pr cc-pr cc-init
                               handler X))
        (let ((wind (gp-rebased-level-ref (- wind-init 1))))
          (guard s h      wind)
          (guard s p-pr   wind)
          (guard s cc-pr  wind)
          (gp-handlers-set! h)
          (action s p-init cc-init))))))

(define (<catch-raw> s p cc tag X action handler)
  (let* ((env       (make-env))
         (h         (make-variable env))
         (hs-init   (gp-handlers-ref)))
    (next-set!  env hs-init)
    (tag-set!   env tag)
    (level-set! env 0)
    (old-set!   env #f)
    (h-set! env
            (mk-handle-lam-catch tag hs-init
                                 h env
                                 handler X))
    (gp-handlers-set! h)
    (action s p 
            (lambda x
              (gp-handlers-set! hs-init)
              (apply cc x)))))

(define find-handler 
  (case-lambda
   ((hs as tag)
    (let lp ((hs hs) (as as))      
      (define ref #f)
      (if (not (null? hs))
          (let ((env 
                 (let ((h hs))
                   (if (null? as)
                       (get-env h)
                       (let ((r (assq h as)))
                         (if r
                             (let ((x (cdr r)))
                               (if (null? x)
                                   (error (format #f "Unhandled guile-log prompt or BUG! tag(~a)" tag) 123)
                                   (begin
                                     (set! ref #t)
                                     (set-cdr! r (cdr x))
                                     (get-env (car x)))))
                             (get-env h)))))))
	    (let* ((o   (old-ref env))
		   (Tag (tag-ref env))
		   (hit (h-ref   env)))
	      (define (next-hs)
		(if (or (not o) (null? o) ref)
		    as
		    (cons (cons hs o) as)))
	      (if (and hit (or (eq? Tag #t) (equal? tag Tag)))
		  (values hit (next-ref env) (next-hs))
		  (lp (next-ref env) (next-hs)))))
	  (error (format #f "Unhandled guile-log prompt or BUG! tag(~a)" tag) 123))))
   ((tag)
    (find-handler (gp-handlers-ref) '() tag))))

(define (<abort> s p cc tag lam . args)
  (call-with-values (lambda() (find-handler tag))
    (lambda (handler next as)
      (handler s p cc tag (<lambda> () (<re-abort> next as tag lam args)) 
	       lam args))))

  
(define (<re-abort> s p cc re as tag lam args)
  (call-with-values (lambda() (find-handler re as tag))
    (lambda (handler next as)
      (handler s p cc tag (<lambda> () (<re-abort> next as tag lam args)) 
	       lam args))))

;; Higher level routines
(<define> (<prompt> tag l lam handler)
  (<let> ((sin   S)
	  (state (gp-store-state S))
	  (wind  (+ (gp-windlevel-ref sin) 1))
	  (pin   P)	  
	  (cci   CC))
     (<prompt-raw> tag l lam
       (<ddlambda> H ((handler handler) (first #t)) (t next kkk args)
	  (<let*> ((kl (kkk S P CC))
		   (kk (case-lambda 
			((   ) (kl        ))
			((h  ) (kl   (H h #f)))
			((t h) (kl t (H h #f))))))
	      (if first
		  (<and>
		   (<code> (gp-restore-wind state (fw wind)))
		   (<with-s>    sin
		   (<with-fail> pin
		   (<with-cc>   cci
		      (<apply> handler t next kk args)))))
		  (<apply> handler t next kk args)))))))

(define <re-prompt> 
  (<case-lambda>
   ((tag k handler data)
    (<let> ((hs-init  (gp-handlers-ref)))
      (<let> ((sin      S)
      	      (state    (gp-store-state S))
	      (wind     (+ (gp-windlevel-ref sin) 1))
	      (pin      P)	  
	      (cci      CC))
         (<apply> (k tag
		     (<lambda> (t next kk . args)
		       (<let> ((args (cp args S)))
		         (<code> (gp-restore-wind state (fw wind)))
		         (<with-s>    sin
		         (<with-fail> pin
		         (<with-cc>   cci
			   (<apply> handler t next kk args)))))))
	   data))))

   ((k handler data)
    (<let> ((hs-init (gp-handlers-ref)))
      (<let> ((sin   S)
	      (state (gp-store-state S))
	      (wind  (+ (gp-windlevel-ref sin) 1))
	      (pin   P)
	      (cci   CC))
       (<apply> (k (<lambda> (t next kk . args)
			 (<let> ((args (cp args S)))
			    (<code> (gp-restore-wind state (fw wind)))
			    (<with-s>    sin
			    (<with-fail> pin
			    (<with-cc>   cci
			    (<apply> handler t next kk args)))))))
			   
	  data))))))
    

(<define> (<catch> tag l lam handler)
  (<let> ((sin   S)
	  (state (gp-store-state S))
	  (wind  (+ (gp-windlevel-ref sin) 1))
	  (pin   P)	  
	  (cci   CC))
     (<catch-raw> tag l lam
       (<lambda> (t next args)
	 (<code> (gp-restore-wind state (fw wind)))
	   (<with-s>    sin
	   (<with-fail> pin
	   (<with-cc>   cci
	     (<apply> handler t next args))))))))
