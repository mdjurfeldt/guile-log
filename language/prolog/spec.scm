(define-module (language prolog spec)
  #:use-module (system base compile)
  #:use-module (system base language)
  #:use-module (logic guile-log guile-prolog readline)
  #:use-module (language scheme compile-tree-il)
  #:use-module (logic guile-log guile-prolog interpreter)
  #:export (prolog))

;;;
;;; Language definition
;;;

(define (beautify-prolog-user-module! module)
  (let ((interface (module-public-interface module)))
    (if (or (not interface)
            (eq? interface module))
        (let ((interface (make-module 31)))
          (set-module-name! interface (module-name module))
          (set-module-version! interface (module-version module))
          (set-module-kind! interface 'interface)
          (set-module-public-interface! module interface))))
  (if (and (not (memq the-scm-module (module-uses module)))
           (not (eq? module the-root-module)))
      ;; Import the default set of bindings (from the SCM module) in MODULE.

      (save-module-excursion
       (lambda ()
	 (set-current-module module)
	 (use-modules ((guile) #:renamer renamer))))

      #;(module-for-each
       (lambda (k v)
	 (if (or #t (member k '(quote unquote)))
	     (module-define! module k v)
	     (module-define! module (symbol-append ':scm k) v)))      
       the-scm-module)      
      #;(for-each (lambda (x) (module-use! module x)) 
		(module-uses! the-scm-module))
      #;(module-use! module the-scm-module)))

(define (make-fresh-prolog-user-module)
  (let ((m (make-module)))
    ;(set-module-name! m '(prolog-user))
    (beautify-prolog-user-module! m)
    m))

(define (copy-from-guile-user)
  (let ((m   (current-module))
	(g   (resolve-module '(guile-user))))
    (process-use_module '(((guile))))
    (module-for-each
     (lambda (k v)
       (module-define! m k v))
     g)))

(define (process-use_module module-interface-args)

    (let ((interfaces   
	   (with-fluids 
	    ((*current-language* (lookup-language 'scheme)))
	    (map (lambda (mif-args)
		   (or (apply resolve-interface mif-args)
		       (error "no such module" mif-args)))
		 module-interface-args))))
      (module-use-interfaces! (current-module) interfaces)))

(define-language prolog
  #:title	"Prolog"
  #:reader      read-prolog
  #:compilers   `((tree-il . ,compile-tree-il))
  #:evaluator	(lambda (x module) (pk x) (primitive-eval x))
  #:printer	write
  #:make-default-environment
  (lambda ()
    (with-fluids ((*current-language* (lookup-language 'scheme)))
      (resolve-module '(prolog-user)))

    #;(let ((m (make-fresh-prolog-user-module)))
      ;(set-module-name! m '(prolog-user))
      
      (module-use! m (module-public-interface 
		      (resolve-module '(logic guile-log iso-prolog))))      
      m)))
      
(define (ask str ok?)
  (let lp ()
    (format #t "~%~a" str)
    (let ((ans ((@@ (logic guile-log guile-prolog readline) readline_))))
      (if (ok? (with-input-from-string ans (lambda () (read))))
	  #t
	  (begin
	    (format #t "wrong input!")
	    (lp))))))

(define-syntax-rule (patched-use-modules . l)
  (let ((mod   (current-module))
	(patch (make-module)))
    (save-module-excursion
     (lambda ()
       (set-current-module patch)
       (use-modules . l)))
    (let ((yall? #f)
	  (nall? #f))
      (for-each-module 
       (lambda (k v)	
	 (when (module-defined? old k)
	       (if yall? (module-define! k v))
	       (if nall?
		   #t
		   (ask (format 
		     #f "'~a' already defined, overwrite? (y/n/yall/nall)> " k)
			(lambda (x)
			  (cond
			   ((equal? x 'yall')
			    (set! yall? #t)
			    (set! x     'y))
			   ((equal? x 'nall)
			    (set! nall? #t)
			    (set! x     'y)))
			  (cond
			   ((equal? x 'y)
			    (module-define! k v)
			    #t)
			   ((equal? x 'n)
			    #t)
			   (else
			    #f)))))))))))
