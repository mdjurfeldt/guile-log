:- module(if, 
	  [fast_compile/1,
           eval/1,
	   term(compile),
           if/1,
           else/0,
	   elseif/1,
	   endif/0,
	   term(ifmacro)
	 ]).

%%
:- dynamic(fast_compile/1).

nocolon(:) :- !, fail.
nocolon(_).

compile((:- eval(X)        ), []) :- X.
compile((:- fast_compile(X)), []) :- 
   X=="true"  ->
      asserta((fast_compile(true) :- !));
   X=="false" ->
      asserta((fast_compile(true) :- !, fail));
   asserta((fast_compile(X) :- !)).

compile((:- X), _) :- !,fail.
compile((?  X), _) :- !,fail.
compile((-  X), _) :- !,fail.
compile((X :- Y),[X :- Y]) :- !,
    X =.. [F|_], nocolon(F), (fast_compile(true) ; fast_compile(F)),!,write(did1(F)),nl.
compile(X,[X]) :-
    X =.. [F|_], nocolon(F), (fast_compile(true) ; fast_compile(F)),!,write(did2(F)),nl.
  

%% This is the dynamic.
:- dynamic(doif/0).
doif(1).

ifmacro((:- if(Code)),[]) :- !,
    Code -> asserta(doif(1)) ; asserta(doif(0)).

ifmacro((:- endif), []) :- !,
    asserta(doif(1)).

ifmacro((:- elseif(Code)), []) :- !,
    Code -> asserta(doif(1)) ; asserta(doif(0)).

ifmacro((:- else), []) :- !,
    (doif(X),!,X==1) -> asserta(doif(0)) ; asserta(doif(1)).

ifmacro(X,[Y]) :-
    (doif(Z),!,Z==1) -> fail ; Y=[].
