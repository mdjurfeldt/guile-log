:- module(cluster,[set_arrow/2,set_parent/1,point_to/2]).

/*
The idea with clustering in dependency tracking is to take advantage that
the cluster has a lookup function, but globaly one needs to use a backtracking
algorithm using a interface relation. The idea is that the interfaces are small 
in number between the clusters and hence enables great speeds.

Each cluster has a map of it's own. guile-log's databases can only be 10000 
elements this means at worst only 100 elements in a cluster. Else it shoule 
scale. note that a cluster usually have ant least
*/
:- use_module(library(forward_chaining)).

:- dynamic(2,arrow/2).
:- dynamic(5,parent/5).
:- dynamic(3,pparent/3).
:- dynamic(4,interface/4).
:- set_trigger(t).


exec(X) :- expand((?- X),(?- Z)), Z.

arrow(X,Y) =f>
   {parent(X,XX,TX,PX,_,P)},
   {parent(Y,YY,TY,PY,_)},
      {XX==YY} ->   {fire(TX,PX(X,Y))} ;
      ({PAX==PAY} -> {fire(TXX,PAX(XX,YY))} ; arrow(XX,YY)),
      interface(XX,YY,X,Y).

% A small efficiency hack here.
makeTriggers(T,P,PP) :-
   (var(P)  -> 
	(
	    make_generic_dynamic(p ,P ),
	    make_generic_dynamic(pp,PP),
            make_generic_dynamic(trigger,T),
	    set_trigger(T),
	    exec((P (X,Y)          =f> PP(X,Y))),
	    exec((P (X,Y), PP(Y,X) =f> PP(X,Z))),
	    exec((PP(X,Y), P (Y,X) =f> PP(X,Z)))
	) ; true).

:- dynamic(db_pdata/3).

pdata(Pa,T,P,PP) :-
    db_pdata(Pa,T,P,PP) -> true ; 
    makeTriggers(T,P,PP),
    asserta(db_padata(Pa,P,PP)).

set_arrow(X,Y) :- fire(trig,arrow(X,Y)).
set_parent(X,Pa) :- 
    pdata(Pa,T,P,PP),
    fire(trig,parent(X,Pa,T,P,PP)).

point_to(X,Y) :-
    X==Y -> true ;
    (
	parent(X,XX,_,_,PP),parent(Y,YY,_,_,_),  
	XX == YY -> PP(W,Y) ; j(X,Y,XX,YY)
    ).

:- dynamic(h/2).
new(X,Y,XX,YY) :- h(X,Y) -> fail ; asserta(h(X,Y)).


j(X,Y,XX,YY) :- 
    (h -> true ; add_dynamic_feature(h)),
    with_fluid_guard_dynamic_object_once(h,i(X,Y,XX,YY)).

i(X,Y,XX,YY) :-
    new(X,Y,XX,YY),
    db_parent(XX,PAX),
    db_parent(YY,PAY),
    (XX==YY -> PAX(XX,YY) ; 
      (parent(XX,XXX,_,_,_),parent(YY,YYY,_,_,_),i(XX,YY,XXX,YYY))),
    db_pdata(XX,_,_,PPX),
    db_pdata(YY,_,_,PPY),
    (
	(interface(XX,YY,Z,W),PPX(X,Z),PPY(W,Y),BinHere2)  ; 
	interface(XX,ZZ,Z,W),PPX(X,Z),i(W,Y,ZZ,YY)
    ).

