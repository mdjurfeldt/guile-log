:- module(term_macro, 
	  [term(attr_macro),
	   term(term_macro),
	   term(goal_macro),
	   term(directives),
	   goal(goalex)
	 ]).

/*
To get swi prolog attribute functionality we can use the following
term macro,

atom_concat produces a string, when unifying with @@ a variable 
will name space it to the current module, e.g. the module under compilation.
Unification with strings are ignorant of any namespace issues e.g. we do not
need to export and import those symbols.

Also term_macro and goal_macro framework is added
*/

doall((:- X),_)     :- !, fail.
doall((?- X),_)     :- !, fail.
doall(( - X),_)     :- !, fail.
doall((_ : _) :- _) :- !, fail.
doall(_ : _)        :- !, fail.
doall((A :- B),[A :- B]) :- !, \+ (A =.. [(_ : _) | _]), \+ (A=(_ : _)).
doall(A,[A])        :- \+ A =..[(_ : _) | _], \+ A=(_ : _).


directives((:- "fast_compile"),[]) :-
    do[((@ (guile) set!) (@@ (logic guile-log prolog base) *eval-only*) #t)].

mname(X) :- module_name(N), X@@! = N.


attr_macro(X, Z) :-
    (X = (XX :- Code) ; X = (XX --> Code)),!,
    X = OP(_,_),
    (XX = (Name : Head) -> true ; (Head=XX, mname(Name))),
    Head =.. [F|A],
    (
	F == "attr_unify_hook" -> 
	(
	    A   =   [Var,Val],
	    AA1 =   [Var,Val,#t],
	    G1  =.. [Name|AA1], 
	    GG1 =.. [OP,G1,Code], 
	    AA2 =   [Var2,Val2,#f],
	    G2  =.. [Name|AA2], 
	    GG2 =.. [OP,G2,eq(Var2,Val2)], 
	    Z   =   [GG1,GG2]
	);
	F == "attribute_goals" -> 
        (
	    atom_concat(Name,"_attribute_goals",Name22),	    
	    Name2@@! = Name22,
	    add_sym(Name2),
            G  =.. [Name2|A], 
            GG =.. [OP,G,Code],
            Z  =   [GG, (:- add_attribute_cstor(Name,Name2))]
        );

	F == "project_attributes" -> 
        (
            atom_concat(Name,"_project_attributes",Name22),
	    Name2@@! = Name22,
	    add_sym(Name2),
            G  =.. [Name2|A], 
            GG =.. [OP,G,Code], 
            Z  =   [GG, (:- add_attribute_projector(Name,Name2))]
        )
    ).
attr_macro(X, Z) :-
    (X = (Name : FF) -> true ; (mname(Name),FF=X)),
    FF =.. [F,Var,Val], 
    (
	F == "attr_unify_hook" -> 
	    (
		A=[Var,Val],
		AA1=[Var,Val,#t],
		G1 =.. [Name|AA1],
		GG1 = (G1 :- true),
		AA2=[Var2,Val2,#f],
		G2 =.. [Name|AA2], 
		GG2 = (G2 :- eq(Var2,Val2)), 
		Z=[GG1,GG2]
	    )
    ).

:-dynamic(termexp/4).

term_macro(X,Z) :-
    goal_expand(X,XX) -> term_macro0(XX,Z) ; term_macro0(X,Z).

term_macro0((F :- Z),_) :-
    F=..[U,X,Y], 
    U=="term_expansion",
    current_module(Name),
    assertz(termexp(Name,X,Y,Z)),fail.

term_exp(X,Y) :-    
    current_module(Name),termexp(Name,X,Y,Z),Z.

:- scm[
  ((@ (guile) set!) (@@ (logic guile-log prolog parser) term_exp) term_exp)
].


:-dynamic(goalexp/4).

goal_macro(X,Z) :- goal_macro0(X,Z).

goal_macro0((F :- Z),Res) :- !,
    (
	F=(M : FF) ->
	(
	    FF=..[U,X,Y], 
	    U=="goal_expansion",
	    atom_concat(M,"_goal_expansion",Name2),
	    Name@@! = Name2,
	    add_sym(Name),
	    FFF=..[Name,X,Y],
	    Res = [(FFF :- Z)]
	) ;
        (
	    F=..[U,X,Y],
	    U=="goal_expansion",
	    current_module(Name),   
	    assertz(goalexp(Name,X,Y,Z)),fail
	)
    ).

goal_macro0(F,Res) :-
    F=(M : FF) ->
	(
	    FF=..[U,X,Y], 
	    U=="goal_expansion",
	    atom_concat(M,"_goal_expansion",Name2),
	    Name@@! = Name2,
	    add_sym(Name),
	    FFF=..[Name,X,Y],
	    Res = [FFF]
	) ;
    (
	F=..[U,X,Y], 
	U=="goal_expansion",
	current_module(Name),
	assertz(goalexp(Name,X,Y,true)),fail
    ).

goal_exp(X,Y) :-
    current_module(Name),
    goalexp(Name,X,Y,Z),Z.

goalex((M : F),Z) :-
    F=..["goal_expansion",X,Y],
    atom_concat(M,"_goal_expansion",Name2),
    Name@@! = Name2,
    add_sym(Name),
    Z=Name(X,Y).

:- scm[
  ((@ (guile) set!) (@@ (logic guile-log prolog parser) goal_exp) goal_exp)
].


% This makes sure to setup a database of all defined function in case
% it is needed by the macros

dyn([X,L],[X,U]) :- dyn0(L,U).
dyn([X,L/N],[X,[L]]).
dyn([L],[U])     :- dyn0(L,U).
dyn([L/N],[[L]]).

dyn0([],[]).
dyn0([F/N|L],[F|U]) :- dyn0(L,U).

mkdyn(I,[[X]],make_generic_dynamic(I,X)).  
mkdyn(I,[[X|L]],(make_generic_dynamic(I,X),U)) :- mkdyn(I,L,U).
mkdyn([[X]],make_dynamic(X)).  
mkdyn([[X|L]],(make_dynamic(X),U)) :- mkdyn(L,U).

all((X : Y) :- Z) :- !,fail.
all((H :- X)) :- !,
 assertzf(H :- call(X)),
 fail.


all((:- Head)) :- !,
    Head =..[F|L],
    (
     F==use_module -> Head
%     F=="dynamic"  -> (dyn(L,LL),mkdyn(LL,G),G)
    ),
    fail.

all((?- Head)) :- !, fail.
all((-  Head)) :- !, fail. 
all(F : X)     :- !, fail.
all(Head) :-
    (
     Head =.. [F|L] -> 
     (
         assertzf(Head)
     ) ;
     atom(Head) -> assertz(Head)
    ),
    fail.


% all is the last ideom that are executed as a prolog macro it always fails
% it is used to log the database of the module functions for usage during macro
% expansion
:- scm[
((@ (guile) set!) (@@ (logic guile-log prolog parser) all) all)
].


:- dynamic(modders/1).

reg(F) :-
   (F=(M:U) -> (U=..[G|_], FF=M:G) ; F=..[FF|_]), 
   (modders(FF) -> true ; assertz(modders(FF))).

register([]).
register([X|L]) :-
    (
	X = (A :- B)  ->  reg(A)  ;
	X = (:- A)    ->  true    ;
	X = (?- A)    ->  true    ;
	reg(X),!                  ; 
	true
    ), register(L).

u((M : X),F) :- !, X =.. [FF|_], F=M:FF.
u(X      ,F) :- X =.. [F|_].

laster(X,Y,Touched) :-
    Touched == #f ->
	(
           X =   [:- _]   -> fail;
           X =   [?- _]   -> fail;
	   X =   [A :- B] -> u(A,F),modders(F),Y=X;
           u(X,F)         -> modders(F), Y=X
	) ;
    register(X),X=Y.

:- scm[
((@ (guile) set!) (@@ (logic guile-log prolog parser) laster) laster)
].
