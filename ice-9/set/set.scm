(define-module (ice-9 set set)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:use-module (oop goops)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:export (<set> set? set-hash set-n
		  make-set-from-assoc make-set-from-assoc-mac set-size
                  set-hash))

#|
This takes an assoc like library and transforms it to an ordered/undordered
set and setmap. There is a small change to the setup from scheme's assoc e.g.
the key value pair is not explicit in stead we assume the form (acons kv a). 

So consider every set constructed from a union if singletons, if an element is 
ordered, then they are guarrantead to be reproduced as they appear in the 
ordered list of union operations with a first come first defined principle. 
in a difference the oredered list remains but with the removed elements 
sieved out. This is as well for intersection. elements kan be key value pairs 
or just a key part, generally there is a poperty of beeing a value, a key 
value pair may be considered as having the value property but may be 
configured to not be a value. a value will be used in an assoc manner the value
 used is the first value that appears in the sequence of operations: 
unification or intersection, one can for example take an ordered set of non 
key-values and intersect on a key value set, then the resulting set/map will 
have the same order of the elements as in the non-value set, but have the 
values in the key-value set. We construct both a order perserving operations 
and non order preserving sets in order to be able to make a more effective set 
operation.

It is a higher order library, it takes a set of functions and construct set opaerations ∪,∩,∖,≡,⊕ ordered and unordered.

Input:
(make-set-from-assoc-mac ...) the macro version of the below
(make-set-from-assoc 
 null assoc acons delete hash mk-kv size value? order? equal?)

null    =  the empty assoc/set
assoc   =  (assoc x set/map),  maps to either a key value pair or x itself if 
                               x is a member of set, else #f
acons   =  (acons x set/map),  funcitonally add x to set/map
delete  =  (delete x set/map), functionally delete x from set/map
size    =  (size set/map)    , >= the number of elements, if deleted elements
                               conses a delete operation to the assoc then this
                               is greater then the number of elements in the set
element operations

mk-kv   = (mk-kv x), makes an element from a normal scheme value  
hash    = (hash x hashsize), creates a hash value from the element
value?  = if this is a key-value pair and the order of the kv pairs in set 
          operations are important
order?  = if the element's construction order is reflected by the set and 
          maintaind through the ordering of the set
equal?  = (equal? x y) the equality predicate used for the elements.

Output: set operations and iteratables


(values #:=   ≡  #:u   u  #:n   n  #:-  s- #:+  s+ #:<   ⊂ #:<=   ⊆
	#:o=  o≡ #:ou  ou #:on  on #:o- o- #:o+ o+ #:o< o⊂ #:o<= o⊆
	#:fold fold #:map map #:for-each for-each #:empty ∅
	#:set->list set->list)

|#

(define-record-type <set>
  (make-set list assoc n hash meta)
  set?
  (list  set-list)
  (assoc set-assoc)
  (n     set-n)
  (hash  set-hash)
  (meta  set-meta))
	 

(define set-size set-n)

(set-record-type-printer! <set>
  (lambda (vl port) 
    (define (m x)
      (if (pair? x)
          (list (car x) (cdr x))
          (list x '())))
    (let ((n (set-n vl)))
      (if (= n 0)
	  (format port "∅")
	  (apply format port "{~a~{,~a~}}" (m ((car (set-meta vl)) vl)))))))

(set-object-property! <set> 'prolog-printer
  (lambda (lp vl avanced) 
    (define (m lp x) 
      (list (lp (car x)) (map lp (cdr x))))
    (let ((n (set-n vl)))
      (if (= n 0)
	  (format #f "∅")          
	  (apply format #f "{~a~{,~a~}}" (m lp ((car (set-meta vl)) vl)))))))


(define-record-type <append>
  (make-append- x y)
  append?
  (x append-x)
  (y append-y))

(define-record-type <append-rev>
  (make-append-rev- x y)
  append-rev?
  (x append-rev-x)
  (y append-rev-y))

(define (make-append x y)
  (cond
   ((null? x) y)
   ((null? y) x)
   (else
    (make-append- x y))))

(define (make-append-rev x y)
  (cond
   ((null? x) y)
   ((null? y) x)
   (else
    (make-append-rev- x y))))

(define-syntax-rule (define-tool 
		      make-setfkns 
		      make-setfkns-mac
		      (args ...) code ...)
  (begin
    (define             (make-setfkns     args ...) code ...)
    (define-syntax-rule (make-setfkns-mac args ...) (begin code ...))))

(define-tool make-set-from-assoc make-set-from-assoc-mac
  (null assoc acons delete hash mk-kv kv? kv-key kv-val sizefkn 
   value? order? equal? ar->l l->ar)


  (define ∅ (make-set '() null 0 0 (list set->kvlist)))  

  (define size (@@ (ice-9 set vhashx) *size*))
  (define (l-serie app)
    (define mt null)
    (let lp ((app app))
      (match app
	 (($ <append-rev> x y)
	  (let ((yy (lp (ar->l y)))
		(xx (lp (ar->l x))))
	    (append xx yy)))
         (($ <append> x y)
	  (let ((xx (lp (ar->l x)))
		(yy (lp (ar->l y))))
	    (append xx  yy)))
	 ((x . l)
	  (if (assoc x mt)
	      (lp l)
	      (begin
		(set! mt (acons x mt))
		(cons x (lp l)))))
	 (x x))))

  (define (make-one x)
    (if (set? x)
	x
	(let ((kv (mk-kv x)))
          (hash kv size)
	  (make-set (l->ar (list kv)) (acons kv null) 1 (hash kv size) 
		    (list set->kvlist)))))

  (define (set->* repr s)
    (match (make-one s)
      (($ <set> ll mm n h)
       (if (= n 0)
           '()
           (let lp ((l '()) (ll (l-serie ll)))
             (if (pair? ll)
                 (let ((kv (car ll)))
                   (if kv
                       (let ((kv (assoc kv mm)))
                         (if kv
                             (lp (cons (repr kv) l)      ;; li
                                 (cdr ll))               ;; itarator
                             (lp l (cdr ll))))
                       (lp l (cdr ll))))
                 (reverse l)))))))
  
  (define (reprl x)  (value? x) x (kv-key x))
  (define (repra x)  (cons (kv-key x) (kv-val x)))
  (define (reprid x) x)

  (define set->list    (lambda (x) (set->* reprl x)))
  (define set->assoc   (lambda (x) (set->* repra x)))
  (define set->kvlist  (lambda (x) (set->* reprid x)))

 
  ;; Takes a sequence ll associated to assoc mm at length nn and 
  ;; produce a new Set which has all slack removed
  (define (do-truncate ll mm nn)
    (when (> (sizefkn mm) nn)
      (let lp ((m null) (i 0) (l '()) (ll (l-serie ll)) (h 0))
	(if (pair? ll)
	    (let ((kv (car ll)))
	      (if kv		  
		  (let ((kv (assoc kv mm)))
		    (if (and kv (not (assoc kv m)))
			(lp (acons kv m)            ;; hash
			    (+ i 1)                 ;; len
			    (cons kv l)             ;; li
			    (cdr ll)                ;; itarator
			    (logxor h (hash kv size))) ;; h
			(lp m i l (cdr ll) h)))
		  (lp m i l (cdr ll) h)))
	    (make-set (l->ar (reverse l)) m i h (list set->kvlist))))))
  


  (define (maybe-truncate l m n h)
    (let ((nn (sizefkn m)))
      (if (> nn (* 2 n))
	  (do-truncate l m n)
	  (make-set    l m n h (list set->kvlist)))))

  (define (≡ x y)
    (match (list (make-one x) (make-one y))
      ((($ <set> lx mx nx hx) ($ <set> ly my ny hy))
       (if (= hx hy)
	   (if (= nx ny)
	       (let lp ((lx (l-serie lx)))
		 (if (pair? lx)
		     (let ((kv (car lx)))
		       (if kv
			   (let ((kvx (assoc kv mx)))
			     (if kvx
				 (let ((kvy (assoc kv my)))
				   (if kvy
				       (if (equal? kvx kvy)
					   (lp (cdr lx))
					   #f)
				       #f))
				 (lp (cdr lx))))
			   (lp (cdr lx))))
		     #t))
	       #f)
	   #f))))

  (define (≡4 x1 x2 y1 y2)
    (match (list (make-one x1) 
                 (make-one x2) 
                 (make-one y1)
                 (make-one y2))
      ((($ <set> lx1 mx1 nx1 hx1) 
        ($ <set> lx2 mx2 nx2 hx2) 
        ($ <set> ly1 my1 ny1 hy1)
        ($ <set> ly2 my2 ny2 hy2))
       (if (= (logxor hx1 hx2) (logxor hy1 hy2))
	   (if (= (+ nx1 nx2) (+ ny1 ny2))
               (let ()
                 (define (try lx1 mx1)
                   (let lp ((lx1 (l-serie lx1)))
                     (if (pair? lx1)
                         (let ((kv (car lx1)))
                           (if kv
                               (let ((kvx (assoc kv mx1)))
                                 (if kvx
                                     (let ((kvy1 (assoc kv my1)))
                                       (if kvy1
                                           (if (equal? kvx kvy1)
                                               (lp (cdr lx1))
                                               #f)
                                           (let ((kvy2 (assoc kv my2)))
                                             (if kvy2
                                                 (if (equal? kvx kvy2)
                                                     (lp (cdr lx1))
                                                     #f)
                                                 #f))))
                                     (lp (cdr lx1))))
                               (lp (cdr lx1))))
                         #t)))
                 (and (try lx1 mx1) 
                      (try lx2 mx2)))
               #f)
	   #f))))

  (define (o≡ x y)
    (match (list (make-one x) (make-one y))
      ((($ <set> lx mx nx hx) ($ <set> ly my ny hy))
       (let ()
       (define (do-the-iteration)
	 (let lp ((lx (l-serie lx)) (ly (l-serie ly)))
	   (if (pair? lx)
	       (let ((kvx (car lx)))
		 (if kvx
		     (begin
		      (let ((kvxx (assoc kvx mx)))
			(if kvxx
			    (if (order? kvxx)
				(set! kvx kvxx)
				(let ((kvy (assoc kvxx my)))
				  (if kvy
				      (if (order? kvy)
					  (set! kvx kvxx)
					  (lp (cdr lx) ly))
				      #f)))
			    (lp (cdr lx) ly)))

		      (let lp2 ((ly ly))
			(if (pair? ly)
			    (let ((kvy (car ly)))
			      (begin
				(let ((kvyy (assoc kvy my)))
				  (if kvyy
				      (if (order? kvyy)
					  (set! kvy kvyy)
					  (let ((kvxx 
						 (assoc kvyy mx)))
					    (if kvxx
						(if (equal? kvxx 
							    kvx)
						    (lp (cdr lx) (cdr ly))
						    (if (order? kvxx)
							#f
							(lp2 (cdr ly))))
					
						#f)))
				      (lp2 (cdr ly))))
			     
				(if (equal? kvx kvy)
				    (lp (cdr lx) (cdr ly))
				    #f)))
			    #f)))
		     (lp (cdr lx) ly)))
	       (let lp2 ((ly ly))
		 (if (pair? ly)
		     (let ((ky (assoc (car ly) my)))
		       (if ky
			   #f
			   (lp2 (cdr ly))))
		     #t)))))
       
       (if (= hx hy)
	   (if (= nx ny)
	       (do-the-iteration)
	       #f)
	   #f)))))

  (define (o≡4 x1 x2 y1 y2)
    (match (list (make-one x1) (make-one x2) (make-one y1) (make-one y2))
      ((($ <set> lx1 mx1 nx1 hx1) 
        ($ <set> lx2 mx2 nx2 hx2)
        ($ <set> ly1 my1 ny1 hy1)
        ($ <set> ly2 my2 ny2 hy2))
       (let ()
       (define (do-the-iteration)
         (define (assocx kv)
           (let ((kvx (assoc kv mx1)))
             (if kvx
                 kvx
                 (assoc kv mx2))))

         (define (assocy kv)
           (let ((kvx (assoc kv my1)))
             (if kvx
                 kvx
                 (assoc kv my2))))

	 (let lp ((lx (l-serie (make-append lx1 lx2))) 
                  (ly (l-serie (make-append ly1 ly2))))
	   (if (pair? lx)
	       (let ((kvx (car lx)))
		 (if kvx
		     (begin
		      (let ((kvxx (assocx kvx)))
			(if kvxx
			    (if (order? kvxx)
				(set! kvx kvxx)
				(let ((kvy (assocy kvxx)))
				  (if kvy
				      (if (order? kvy)
					  (set! kvx kvxx)
					  (lp (cdr lx) ly))
				      #f)))
			    (lp (cdr lx) ly)))

		      (let lp2 ((ly ly))
			(if (pair? ly)
			    (let ((kvy (car ly)))
			      (begin
				(let ((kvyy (assocy kvy)))
				  (if kvyy
				      (if (order? kvyy)
					  (set! kvy kvyy)
					  (let ((kvxx 
						 (assocx kvyy)))
					    (if kvxx
						(if (equal? kvxx 
							    kvx)
						    (lp (cdr lx) (cdr ly))
						    (if (order? kvxx)
							#f
							(lp2 (cdr ly))))
					
						#f)))
				      (lp2 (cdr ly))))
			     
				(if (equal? kvx kvy)
				    (lp (cdr lx) (cdr ly))
				    #f)))
			    #f)))
		     (lp (cdr lx) ly)))
	       (let lp2 ((ly ly))
		 (if (pair? ly)
		     (let ((ky (assocy (car ly))))
		       (if ky
			   #f
			   (lp2 (cdr ly))))
		     #t)))))
       
       (if (= (logxor hx1 hx2) (logxor hy1 hy2))
	   (if (= (+ nx1 nx2) (+ ny1 ny2))
	       (do-the-iteration)
	       #f)
	   #f)))))


  (define (next-mute lp ll l m n h)
    (lp (cdr ll) l m n h))

  (define (next-skip lp ll l m n h kv)
    (lp (cdr ll) l m n h))

  (define (next-cons-kv-on-list lp ll l m n h kv kv*)
    (lp (cdr ll) (cons kv l) m n h))
  
  (define (next-add-kv lp ll l m n h kv kv*)
    (lp (cdr ll) (cons kv* l) (acons kv* m)        
	(+ n 1) (begin  (hash kv* size)
                        (logxor h (hash kv* size)))))

  (define (next-add-l  lp ll l m n h kv*)
    (lp (cdr ll) (cons kv* l) m n h))

  (define (next-swap-value lp ll l m n h kv kv* kv**)
    (lp (cdr ll) l (acons kv* m) n
	(logxor h 
		(logxor (hash kv*  size)
			(hash kv** size)))))

  (define (next-delete lp ll l m n h kv kv*)
    (lp (cdr ll) l
	(delete kv* m) 
	(- n 1)
	(logxor h (hash kv* size))))

  ;;TODO MAKE USE OF APPEND CONSTRUCT MAKE USE OF VLISTS? MAYBE?
  (define-syntax-rule (mku ou is-ordered-set?)
  (define ou 
    (case-lambda
     (()   ∅)
     ((x)  (make-one x))
     ((x y)
      (let* ((x     (make-one x))
	     (y     (make-one y))
	     (nx    (set-n x))
	     (ny    (set-n y))
	     (mx    (set-assoc x))
	     (my    (set-assoc y))
	     (lx    (set-list  x))
	     (ly    (set-list  y)))
	(cond
	 ((= nx 0)
	  (if (= ny 0)
	      ∅
	      y))
	 ((= ny 0)
	  x)
	 ((or (< nx 10) (< nx ny))
	  (let lp ((ll (l-serie lx))
		   (l  '()) (m my) (n ny) (h (set-hash y)))
	      
	    (define-syntax-rule (next f . lll)
	      (f lp ll l m n h . lll))

	    (if (pair? ll)
		(let ((kv (car ll)))
		  (let ((kvx (assoc kv mx)))
		    (if kvx
			(let ((kvy (assoc kv my)))
			  (if (value? kvx)
			      (if kvy
				  (next next-swap-value kv kvx kvy)
				  (next next-add-kv kv kvx))
			      (if kvy
				  (if (order? kvx)
				      (next next-cons-kv-on-list kv kvx)
				      (next next-skip kv))
				  (next next-add-kv kv kvx))))
			(next next-mute))))
		(maybe-truncate (make-append (l->ar (reverse l)) ly)
				m n h))))
	  
	 (else
	  (let lp ((ll      (l-serie ly))
		   (l  '()) (m mx) (n nx) (h (set-hash x)))
	      
	    (define-syntax-rule (next f . lll)
	      (f lp ll l m n h . lll))
	      
	    (if (pair? ll)
		(let ((kv (car ll)))
		  (let ((kvx (assoc kv mx)))
		    (if kvx
			(let ((kvy (assoc kv my)))
			  (if kvy		  
			      (if (and (value? kvy) (not (value? kvx)))
				  (next next-swap-value kv kvy kvx)
				  (next next-skip kv))
			      (next next-mute)))
			(let ((kvy (assoc kv my)))
			  (if kvy
			      (next next-add-kv kv kvy)
			      (next next-mute))))))
		(maybe-truncate 
		 (make-append-rev lx (l->ar (reverse l))) m n h)))))))
     ((x y . l)
      (ou x (apply ou y l))))))

  (mku ou #t)
  (mku u  #f)

  (define-syntax-rule (mkn on is-ordered-set?)
  (define on 
    (case-lambda
     ((x)  x)
     ((x y)
      (let* ((x     (make-one x))
	     (y     (make-one y))
	     (nx    (set-n x))
	     (ny    (set-n y))
	     (mx    (set-assoc x))
	     (my    (set-assoc y))
	     (lx    (set-list  x))
	     (ly    (set-list  y)))

	(cond
	 ((or (= nx 0) (= ny 0))
	  ∅)

	 ((or (< nx 10) (< nx (* 2 ny)))
	  (let lp ((ll (l-serie lx)) (l '())
		   (m mx) (n nx) (h (set-hash x)))

	      (define-syntax-rule (next f . lll) (f lp ll l m n h . lll))
	      (if (pair? ll)
		  (let ((kv (car ll)))
		    (let ((kvx (assoc kv mx)))
		      (if kvx
			  (let ((kvy (assoc kv my)))
			    (if kvy
				(if (and (value? kvy) 
					 (not (value? kvx)))
				    (next next-swap-value kv kvy kvx)
				    (next next-skip kv))
				(next next-delete kv kvx)))
			  (next next-skip kv))))
		  (maybe-truncate lx m n h))))
	 (else
	    (let lp ((ll (l-serie ly))
		     (l  ly)
		     (m my) (n ny) (h (set-hash y)))
	      
	      (define-syntax-rule (next f . lll) (f lp ll l m n h . lll))
	      (if (pair? ll)
		  (let ((kv (car ll)))
		    (let ((kvy (assoc kv my)))
		      (if kvy
			  (let ((kvx (assoc kv mx)))
			    (if kvx
				(if (value? kvx)
				    (if (order? kvx)
					(next next-swap-value kv kvx kvy)
					(next next-skip kv))
				    (next next-skip kv))
				(next next-delete kv kvy)))
			  (next next-mute))))
		  (if (is-ordered-set?)
		      (maybe-truncate lx m n h)		   
		      (maybe-truncate ly m n h))))))))

     ((x y . l)
      (on x (apply on y l))))))

  (mkn on #t)
  (mkn n  #f)

  (define tripple 
    (lambda (x y z)
      (let* ((x     (make-one x))
	     (y     (make-one y))
	     (z     (make-one z))
	     (nx    (set-n x))
	     (ny    (set-n y))
	     (nz    (set-n z))
	     (mx    (set-assoc x))
	     (my    (set-assoc y))
	     (mz    (set-assoc z))
	     (lx    (set-list  x))
	     (ly    (set-list  y)))

	(cond
	 ((= nx 0)
	  ∅)
	 ((= nz 0)
	  (if (= ny 0)
	      ∅  
	      (on x y)))
	 ((= ny 0)
	  (o- x z))
	 (else
	  (let lp ((ll (l-serie lx)) (l '())
		   (m mx) (n nx) (h (set-hash x)))

	    (define-syntax-rule (next f . lll) (f lp ll l m n h . lll))
	    (if (pair? ll)
		(let ((kv (car ll)))
		  (let ((kvx (assoc kv mx)))
		    (if kvx
			(let ((kvy (assoc kv my)))
			  (if kvy
			      (if (and (value? kvy) 
				       (not (value? kvx)))
				  (next next-swap-value kv kvy kvx)
				  (next next-skip kv))
			      (if (not (assoc kv mz))
				  (next next-skip kv)
				  (next next-delete kv kvx))))
			(next next-skip kv))))
		(maybe-truncate lx m n h))))))))

 (define-syntax-rule (mk- o- is-ordered-set?)
  (define o- 
    (case-lambda
     ((x)  x)
     ((x y)
      (let* ((x     (make-one  x))
	     (y     (make-one  y))
	     (nx    (set-n     x))
	     (ny    (set-n     y))
	     (mx    (set-assoc x))
	     (my    (set-assoc y))
	     (lx    (set-list  x))
	     (ly    (set-list  y)))

	(cond
	 ((= nx 0)
	  ∅)
	 ((= ny 0)
	  x)
	 (else
	  (let lp ((ll (l-serie lx)) (l '())
		   (m mx) (n nx) (h (set-hash x)))
	      
	    (define-syntax-rule (next f . lll) (f lp ll l m n h . lll))
	    
	    (if (pair? ll)
		(let ((kv (car ll)))
		  (let ((kvx (assoc kv mx)))
		    (if kvx
			(if (assoc kv my)
			    (next next-delete kv kvx)
			    (next next-skip kv))			  
			(next next-mute))))
		(maybe-truncate lx m n h)))))))
		
     ((x y . l)
      (apply o- (o- x y) l)))))

  (mk- o- #t)
  (mk- s- #f)

  (define o+ 
    (case-lambda
     (()  ∅)
     ((x) x)
     ((x y)
      (ou (o- x y) (o- y x)))
     ((x y . l)
      (o+ x (apply o+ y l)))))

  (define s+ 
    (case-lambda
     (()  ∅)
     ((x) x)
     ((x y)
      (u (s- x y) (s- y x)))
     ((x y . l)
      (s+ x (apply s+ y l)))))

  (define (fold f seed set)
    (let lp ((l  (l-serie (set-list set)))
	     (m  (set-assoc set)))	     
      (let lp ((l l) (seed seed))
	(if (pair? l)
	    (let ((kv (car l)))
	      (let ((kv (assoc kv m)))
		(if kv
		    (lp (cdr l) (f kv seed))
		    (lp (cdr l) seed))))
	    seed))))

  (define (in x s)
    (match (make-one s)
     (($ <set> l m n h)
      (assoc (mk-kv x) m))))

  (define (⊆ x y)
    (≡ (n x y) x))


  (define (o⊆ x y)
    (o≡ (on y x) x))

  (define (⊂ x y)
    (and (not (≡ x y)) (⊆ x y)))

  (define (o⊂ x y)
    (and (not (≡ x y)) (⊆ x y)))

  (define (map f set) 
    (reverse (fold (lambda (k seed) (cons (f k) seed)) '() set)))
  (define (for-each f set) 
    (reverse (fold (lambda (k seed) (f k) seed) (if #f #f) set)))

  (define (member x s)
    (let ((r (assoc (mk-kv x) (set-assoc s))))
      (if r
          (if (kv? r)
              (cons (kv-key r) (kv-val r))
              r)
          r)))

  (values #:member member
          #:=   ≡ #:=4   ≡4  
          #:u   u  #:n   n  #:-  s- #:+  s+ #:<   ⊂ #:<=   ⊆
	  #:o=  o≡ #:o=4  o≡4
          #:ou  ou #:on  on #:o- o- #:o+ o+ #:o< o⊂ #:o<= o⊆
 	  #:n- tripple
	  #:in in #:fold fold #:map map #:for-each for-each #:empty ∅
	  #:set->list set->list #:set->assoc set->assoc 
	  #:set->kvlist set->kvlist #:make-one make-one))


