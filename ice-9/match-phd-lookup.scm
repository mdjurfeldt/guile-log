;; 2010/05/20 - record matching for guile (Stefan Israelsson Tampe)
;; Modifying upstream version (match.upstream.scm) by Alex Shinn
;; 2010/08/29 - match abstractions and unquote added

(define-module (ice-9 match-phd-lookup)
  #:use-module (logic guile-log code-load)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-11)
  #:export     (match-define match-let* match-let match-letrec match-lambda*
			     match-lambda match make-phd-matcher))

(define (id x . l) x)

(define (gp-newframe s) #f)
(define (gp-unwind   s) #f)

(define-syntax match
  (syntax-rules (-abs -phd)
    ((match s)
     (match-syntax-error "missing match expression"))
    ((match s atom)
     (match-syntax-error "no match clauses"))

    ((match s -abs abs -phd p . l)
     (match* s (abs p) . l))
    ((match s phd p -abs abs . l)
     (match* s (abs p) . l))

    ((match s -abs abs . l)
     (match* s (abs ((car cdr pair? null? equal? id) ())) . l))

    ((match s -phd p . l)
     (match* s (() p) . l))

    ((match s x . l)
     (match* s (() ((car cdr pair? null? equal? id) ())) x . l))))

(define-syntax match*
  (lambda (x)
    (syntax-case x ()
      ((q s . l) 
       ;(pk `(match* ,(syntax->datum (syntax l))))
       (syntax (match** s . l))))))

(define-syntax match+
  (lambda (x)
    (syntax-case x ()
      ((_ s abs (arg ...) . l)
       (with-syntax (((v ...) 
                      (map (lambda (x) (datum->syntax x (gensym "v")))
                           #'(arg ...))))         

         #'(let ((v arg) ...)
             (match-next abs s (v ...) (#f #f) . l)))))))

(define-syntax match**
  (syntax-rules ()
    ((match* s abs (#:args arg ...) . l) 
     (match+ s abs (arg ...)  . l))


    ((match* s abs (app ...) (pat . body) ...)
     (let ((v (app ...)))
       (match-next abs s v ((app ...) (set! (app ...))) (pat . body) ...)))
    ((match* s abs #(vec ...) (pat . body) ...)
     (let ((v #(vec ...)))
       (match-next abs s v (v (set! v)) (pat . body) ...)))
    ((match* s abs atom (pat . body) ...)
     (let ((v atom))
       (match-next abs s v (atom (set! atom)) (pat . body) ...)))
    ))


(define-syntax match-next
  (syntax-rules (=> ->)
    ;; no more clauses, the match failed
    ((match-next abs s v g+s)
     (error 'match "no matching pattern"))

    ;; named failure continuation
    ((match-next abs s v g+s (pat (=> failure) . body) . rest)
     (let ((failure (lambda () (match-next abs s v g+s . rest))))
       ;; match-one analyzes the pattern for us
       (match-one abs s v pat g+s (match-drop-ids (begin . body)) 
                  (match-drop-ids (failure)) ())))

    ((match-next abs s v g+s (pat (-> failure (f a ...)) . body) . rest)
     (let ((failure (f a ... (match-next abs s v g+s . rest))))
       ;; match-one analyzes the pattern for us
       (match-one abs s v pat g+s (match-drop-ids (begin . body)) 
                  (match-drop-ids (failure)) ())))

    ((match-next abs s v g+s (pat (=> failure qq) . body) . rest)
     (let ((failure (lambda () qq (match-next abs s v g+s . rest))))
       ;; match-one analyzes the pattern for us
       (match-one abs s v pat g+s (match-drop-ids (begin . body)) 
                  (match-drop-ids (failure)) ())))

    ;; anonymous failure continuation, give it a dummy name
    ((match-next abs s v g+s (pat . body) . rest)
     (match-next abs s v g+s (pat (=> failure) . body) . rest))))

(define-syntax match-one
  (lambda (x)
    (syntax-case x ()
      ((q . l) 
       ;(pk `(match-one ,(syntax->datum (syntax l))))
       (syntax (match-one* . l))))))

(define-syntax match-one+
  (lambda (x)
    (syntax-case x ()
      ((match-one+ (abs ((car cdr pair? null? equal? id . u_) pp)) s v  . l)
       ;(pk `(match-one ,(syntax->datum (syntax l))))

       (syntax (let ((w (id v s)))
		 (match-one* (abs ((car cdr pair? null? equal? id . u_) pp)) s 
			     w . l)))))))



(define-syntax abs-drop
  (lambda (x)
    (syntax-case x ()      
      ((_ a s k        ) #;(pk 'abs (syntax->datum #'k))         #'k)
      ((_ a s (k ...) v) #;(pk 'abs (syntax->datum #'(k ... v))) #'(k ... v)))))

(define-syntax match-one*
  (syntax-rules ()
    ;; If it's a list of two or more values, check to see if the
    ;; second one is an ellipse and handle accordingly, otherwise go
    ;; to MATCH-TWO.
    ((match-one* abs s v (p q . r) g+s sk fk i)
     (match-check-ellipse
      q
      (match-extract-vars abs s p 
                          (abs-drop (match-gen-ellipses 
                                     abs s v p r  g+s sk fk i)) i ())
      (match-two abs s v (p q . r) g+s sk fk i)))
    ;; Go directly to MATCH-TWO.
    ((match-one* . x)
     (match-two . x))))

(define-syntax insert-abs
  (lambda (x)
    (syntax-case x ()
      ((q . l) 
       ;(pk `(insert-abs ,(syntax->datum (syntax l))))
       (syntax (insert-abs* . l))))))


(define-syntax insert-abs*
  (syntax-rules (begin)
    ((insert-abs abs s (begin . l)) (begin . l))
    ((insert-abs abs s (x))         (x))
    ((insert-abs abs s (n nn ...))  (n abs s nn ...))))

    
(define-syntax match-two
  (lambda (x)
    (syntax-case x ()
      ((q . l) 
       ;(pk `(match-two ,(syntax->datum (syntax l))))
       (syntax (match-two* . l))))))
  
(define-syntax match-two*
  (syntax-rules (_ ___ *** <> arguments cond unquote unquote-splicing 
                   quote quasiquote ? $ = and or not set! get!)

    ((match-two (abs ((car cdr pair? null? equal? id . u_) pp)) s v () 
                g+s (sk ...) fk i)
     (let* ((s (null? v s)))
       (if s
           (insert-abs (abs ((car cdr pair? null? equal? id . u_) pp)) s
		       (sk ... i))
           (insert-abs (abs ((car cdr pair? null? equal? id . u_) pp)) s fk))))

    ((match-two (abs ((car cdr pair? null? equal? id . u_) pp)) s v (quote p) 
                g+s (sk ...) fk i)
     (let ((s (equal? v 'p s)))
       (if s
           (insert-abs (abs ((car cdr pair? null? equal? id . u_) pp)) s
		       (sk ... i))
           (insert-abs (abs ((car cdr pair? null? equal? id . u_) pp)) s fk))))
    
    ;;Stis unquote logic
    ((match-two (abs ((car cdr pair? null? equal? id . u_) pp)) s v (unquote p)  
                g+s (sk ...) fk i)
     (let ((s (equal? v p s)))
       (if s
           (insert-abs (abs ((car cdr pair? null? equal? id . u_) pp)) s
		       (sk ... i))
           (insert-abs (abs ((car cdr pair? null? equal? id . u_) pp)) s fk))))

    ((match-two (abs ((ccar ccdr ppair? null? equal? id . u_) rr)) s v 
                ((unquote-splicing p) . ps)  g+s sk fk i)
     (let loop ((vv (id v s))
                (pp p))       
       (if (pair? pp)
           (let ((s (ppair? vv s)))
             (if s
                 (let ((s (equal? (ccar vv s) (car pp) s)))
                   (if s
                       (loop (id (ccdr vv s) s) (cdr pp))
                       (insert-abs (abs 
				    ((ccar ccdr ppair? null? equal? id . u_) 
				     rr)) s fk)))
                 (insert-abs (abs ((ccar ccdr ppair? null? equal? id . u_) 
                                   rr)) s fk)))
             
           (match-one (abs ((ccar ccdr ppair? null? equal? id . u_) rr)) 
                      s vv ps g+s sk fk i))))

    ((match-two abs s () (arguments) g+s (sk ...) fk i)
     (insert-abs abs s (sk ... i)))

    ((match-two abs s (a as ...) (arguments p ps ...) g+s sk fk i)
     (let ((v a))
       (match-two abs s v p g+s (match-one (as ...) (arguments ps ...) 
                                           g+s sk fk) fk i)))

    ((match-two abs s v (quasiquote p) . x)
     (match-quasiquote abs s v p . x))    
    ((match-two abs s v (and) g+s (sk ...) fk i) (insert-abs abs s (sk ... i)))
    ((match-two abs s v (and p q ...) g+s sk fk i)
     (match-one abs s v p g+s (match-one v (and q ...) g+s sk fk) fk i))
    ((match-two abs s v (or) g+s sk fk i) (insert-abs abs s fk))
    ((match-two abs s v (or p) . x)
     (match-one abs s v p . x))
    ((match-two abs s v (or p ...) g+s sk fk i)
     (match-extract-vars abs s (or p ...) 
                         (abs-drop (match-gen-or abs s v (p ...) 
                                                 g+s sk fk i)) i ()))

    ((match-two abs s v (cond) g+s sk fk i) (insert-abs abs s fk))
    ((match-two abs s v (cond p) . x)
     (match-one abs s v p . x))
    ((match-two abs s v (cond p ps ...) g+s sk fk i)
     (match-one abs s v p g+s sk (abs-drop (match-one abs s v (cond ps ...) 
                                                      g+s sk fk i)) i))

    ((match-two abs s v (not p) g+s (sk ...) (fk fkk ...) i)
     (match-one abs s v p g+s (match-drop-ids (fk abs s fkk ...)) (sk ... i) i))
    ((match-two (abs ((car cdr pair? null? equal? id . u_) pp)) ss v 
		(get! getter) ((g ...) s) (sk ...) fk i)
     (let ((getter (lambda (sin) (id (g ... sin) sin))))
       (insert-abs (abs ((car cdr pair? null? equal? id . u_) pp)) ss
		   (sk ... i))))
    ((match-two abs s v (set! setter) (g (se ...)) (sk ...) fk i)
     (let ((setter (lambda (ss p cc x) (let ((sss (se ... ss x))) (cc sss p)))))
       (insert-abs abs s (sk ... i))))
    ((match-two abs s v (? pred . p) g+s sk fk i)
     (if (pred (id v s)) 
         (match-one abs s v (and . p) g+s sk fk i) 
	 (insert-abs abs s fk)))    
    ((match-two (abs ((car cdr pair? null? equal? id . u_) pp))
		s v ($ n) g-s (sk ...) fk i)
     (let ((vv (id v s)))
       (if (n vv) 
	   (insert-abs (abs ((car cdr pair? null? equal? id . u_) pp)) s 
		       (sk ... i))
	   (insert-abs (abs ((car cdr pair? null? equal? id . u_) pp)) s fk))))
    
    ((match-two (abs ((car cdr pair? null? equal? id . u_) pp))
		s v ($ nn p ...) g+s sk fk i)
     (let ((vv (id v s)))
       (if (nn vv)
	   (match-$ (abs ((car cdr pair? null? equal? id . u_) pp))
		    (and) 0 (p ...) s vv sk fk i)
	   (insert-abs (abs ((car cdr pair? null? equal? id . u_) pp)) s fk))))
     
    ;; stis, added the possibility to use set! and get to records everything is
    ;; done through boxing here    
    ((match-two (abs ((car cdr pair? null? equal? id (set-car! set-cdr! sset!) 
			   . u_) 
		      pp)) s v (= 0 m p) g+s sk fk i)
     (let ((w  (struct-ref v m)))
       (match-one (abs ((car cdr pair? null? equal? id 
			     (set-car! set-cdr! sset!) . u_) 
			pp)) s w p (w (set! w)) sk fk i)))

    ((match-two (abs ((car cdr pair? null? equal? id (set-car! set-cdr! sset!)
			   . u_)
		      pp)) ss v (= g s p) 
		g+s sk fk i)
     (let ((w (g v))) 
       (match-one (abs ((car cdr pair? null? equal? id 
			     (set-car! set-cdr! sset!) . u_)
			pp)) ss w p (w (s w)) sk fk i)))

    ((match-two 
      (abs ((car cdr pair? null? equal? id (set-car! set-cdr! sset!) . u_) pp))
      s v (= proc p) g+s . x)
     (let ((w (proc v))) 
       (match-one (abs ((car cdr pair? null? equal? id 
			     (set-car! set-cdr! sset!) . u_)
			pp)) s w p (w (set! w)) . x)))

    ((match-two abs s v ((<> f p) . l) g+s sk fk i)
     (call-with-values (lambda () (f v))
       (lambda (s res)
	 (if res
	     (match-one abs s (car res) p g+s 
                      (match-one (cdr res) l g+s sk fk)
                      fk i)
	     (insert-abs abs s fk)))))

    ((match-two abs s v (p ___ . r) g+s sk fk i)
     (match-extract-vars abs s p (abs-drop 
				(match-gen-ellipses abs s v p r 
						    g+s sk fk i) i ())))
    ((match-two (abs phd) s v p       g+s sk fk i)
     (match-abstract () abs phd s v p g+s sk fk i))))

;; This does not transport s correctly
(define-syntax match-gen-or
  (lambda (x)
    (syntax-case x ()
      ((nm a . l)
       #;(pk (syntax->datum #'(nm . l)))
       #'(match-gen-or_ a . l)))))

(define-syntax match-gen-or_
  (syntax-rules ()
    ((_ abs s v p g+s (sk ...) fk (i ...) ((id id-ls) ...))
     (let ((fr  (gp-newframe s))
	   (sk2 (lambda (id ...) (insert-abs abs s (sk ... (i ... id ...))))))
       (match-gen-or-step abs fr s v p g+s (match-drop-ids (sk2 id ...)) 
			  fk (i ...))))))

(define-syntax match-gen-or-step
  (lambda (x)
    (syntax-case x ()
      ((nm a . l)
       #;(pk (syntax->datum #'(nm . l)))
       #'(match-gen-or-step_ a . l)))))

(define-syntax match-gen-or-step_
  (syntax-rules ()
    ((_ abs fr s v () g+s sk fk . x)
     ;; no OR clauses, call the failure continuation
     (insert-abs abs s fk))
    ((_ abs fr s v (p) . x)
     ;; last (or only) OR clause, just expand normally
     (match-one abs s v p . x))
    ((_ abs fr s v (p . q) g+s sk fk i)
     ;; match one and try the remaining on failure
     (let ((failure (lambda () 
		      (match-gen-or-step__ abs fr s v q g+s sk fk i))))
       (match-one abs s v p g+s sk (match-drop-ids (failure)) i)))
    ))

(define-syntax match-gen-or-step__
  (syntax-rules ()
    ((m__ abs fr . l)
     (begin
       (gp-unwind fr)
       (match-gen-or-step abs fr . l)))))

(define-syntax match-three
  (lambda (x)
    ;(pk (syntax->datum x))
    (syntax-case x ()
      ((q abs s w p g+s sk fk i)
       (check-sym (syntax->datum (syntax p))) 
       (syntax (match-three* abs s w p g+s sk fk i))))))

(define-syntax match-three*
  (syntax-rules (_ ___ *** quote quasiquote ? $ = and or not set! get!)
    ((match-two (abs ((car cdr pair? null? id (set-car! set-cdr! sset!) . u_)
		      rr)) 
		s v (p) g+s sk fk i)
     (let-values (((w cd s) (pair? v s)))
       (if s
           (let ((s (null? cd s)))
             (if s
                 (match-one (abs ((car cdr pair? null? id (set-car! set-cdr! 
				       sset!) . u_)  rr)) s w p 
                            ((car w) 
                             (set-car! w)) sk fk i)
                 (insert-abs (abs ((car cdr pair? null? equal? id (set-car! 
					set-cdr! sset!) . u_)  pp)) s fk)))
           (insert-abs (abs ((car cdr pair? null? equal? id (set-car! set-cdr! 
				  sset!) . u_) pp)) s fk))))

    ((match-two abs s v (p *** q) g+s sk fk i)
     (match-extract-vars abs s p (match-gen-search s v p q g+s sk fk i) i ()))

    ((match-two abs s v (p *** . q) g+s sk fk i)
     (match-syntax-error "invalid use of ***" (p *** . q)))

    ((match-two (abs ((car cdr pair? null? equal? id 
			   (set-car! set-cdr! sset!) . u_) 
		      pp)) s v (p . q) 
                g+s sk fk i)
     (let-values (((w x s) (pair? v s)))
       (if s
           (match-one (abs ((car cdr pair? null? equal? id (set-car! set-cdr! 
				 sset!) . u_) pp)) 
		      s w p ((car v) (set-car! v))
		      (match-one x q ((cdr v) (set-cdr! v)) sk fk)
                      fk
                      i)
           (insert-abs (abs ((car cdr pair? null? equal? id (set-car! set-cdr! 
				  sset!) . u_) pp)) s fk))))
    
    ((match-two (abs ((car *cdr *pair? null? equal? iid . u_) pp)) s v #(p ...) 
		g+s sk fk i)
     (let* ((w   (iid v s))
	    (lam (lambda (ww) (match-vectorie 
			     (abs ((car *cdr *pair? null? equal? iid . u_) pp)) 
			     s ww 0 
			     (p ...) g+s sk fk i))))
       (if (vector? w)	  
	   (lam w)
	   (let* ((ii  (let lp ((iii 0) (vvv '(p ...)))
                         (if (pair? vvv)
                             (lp (+ iii 1) (cdr vvv))
                             iii)))
                  (vec (make-vector ii))
		  (s   (equal? w vec s)))
	     (if s
		 (begin 
                   (let lp ((iii (- ii 1)))
                     (when (>= iii 0)
                       (vector-set! vec iii (gp-var! s))
                       (lp (- iii 1))))
                   (lam vec))
		 (insert-abs (abs ((car *cdr *pair? null? equal? iid . u_) pp)) 
			     s fk))))))
	   
    ((match-two abs s v _ g+s (sk ...) fk i) (insert-abs abs s (sk ... i)))

    ;; Not a pair or vector or special literal, test to see if it's a
    ;; new symbol, in which case we just bind it, or if it's an
    ;; already bound symbol or some other literal, in which case we
    ;; compare it with EQUAL?.
    ((match-two (abs ((car cdr pair? null? equal? iid . u_) pp)) s v x 
                g+s (sk ...) fk (id ...))
     (let-syntax
         ((new-sym?
           (syntax-rules (id ...)
             ((new-sym? x sk2 fk2) sk2)
             ((new-sym? y sk2 fk2) fk2))))
       (new-sym? random-sym-to-match
                 (let ((x (iid v))) 
                   (insert-abs (abs ((car cdr pair? null? equal? iid . u_) pp)) 
                               s (sk ... (id ... x))))
                 (let ((s (equal? v x s)))
                   (if s
                       (insert-abs (abs ((car cdr pair? null? equal? iid . u_) 
					 pp)) s
                                   (sk ... (id ...)))
                       (insert-abs (abs ((car cdr pair? null? equal? iid . u_) 
					 pp)) 
                                   s fk))))))))
     
(define-syntax match-vectorie
  (lambda (x)
    (syntax-case x ()
      ((_ abs s w n (p ...) g+s sk fk i)
       (with-syntax ((N (length #'(p ...))))
	 #'(if (= (vector-length w) N)
	       (match-vector* abs s w n (p ...) g+s sk fk i)
	       (insert-abs abs s fk)))))))
    

(define-syntax match-vector*
  (syntax-rules ()
    ((_ (abs ((car cdr pair? null? equal? id (set-car! set-cdr! sset!) . u_)
	      pp)) 
	s w n (p ps ...) g+s sk fk i)
     (let ((nnew (+ n 1))
	   (wnew (vector-ref w n)))
       (match-one (abs ((car cdr pair? null? equal? id 
			     (set-car! set-cdr! sset!) . u_)
			pp))
		  s wnew p (wnew (sset! wnew))
		  (match-vector*  w nnew 
				   (ps ...) g+s sk fk)
		  fk i)))
    
    ((_ (abs ((car cdr pair? null? equal? id . u_) pp)) s w n () s+g (sk ...) 
	fk i)
     (insert-abs (abs ((car cdr pair? null? equal? id . u_) pp)) s 
		 (sk ... i)))))


;;warn agains miss spelled abstractions
(define (check-sym x)
  (let ((f (lambda (x)
             (let ((l (string->list (symbol->string x))))
               (if (eq? (car l) #\<)
                   (if (not (and (pair? (cdr l)) 
                                 (eq? #\> (cadr l)) (null? (cddr l))))
                       (let loop ((l l))
                         (if (pair? l)
                             (if (null? (cdr l))
                                 (if (eq? (car l) #\>)
                                     #t #;(warn (format #f
		       "<> like variable that is not an abstraction e.g. ~a"
                                                   x)))
                                 (loop (cdr l)))
			     #t))))))))
    (if (symbol? x)
        (f x)
        (if (and (pair? x) (symbol? (car x)))
            (f (car x))))))
            

         

        
    
(define-syntax match-abstract
  (lambda (x)
    (syntax-case x ()
      ((q . l) 
       ;(pk `(match-abstract ,(syntax->datum (syntax l))))
       (syntax (match-abstract* . l))))))

(define-syntax match-abstract*
  (lambda (x)
    (syntax-case x ()
      ((q x () phd         s  y p               . l)
       (syntax (match-phd () phd x s y p . l)))
               
      ((q (x ...) ((a) us ...) phd s y ((b bs ...) . ps) g+s sk fk i)
       (if (eq? (syntax->datum (syntax a)) (syntax->datum (syntax b)))
           (syntax (let ((ret ((a bs ...) y)))
                     (if ret
                         (match-one  (((a) us ... x ...) phd) s (cdr ret) ps 
				     g+s sk fk i)
                         (insert-abs (((a) us ... x ...) phd) s fk))))
           (syntax (match-abstract ((a) x ...) (us ...) phd s y 
				   ((b bs ...) . ps) g+s sk fk i))))

      ((q (x ...) ((a aa as ...) us ...) phd s y ((b  bs ...) . ps) g+s sk fk i)
       (if (eq? (syntax->datum (syntax a)) (syntax->datum (syntax b)))
           (syntax (let ((ret ((a bs ...) y)))
                     (if ret
                         (let ((aa (car ret)))
                           (match-one  (((a as ...) us ... x ...) phd) s 
				       (cdr ret) ps g+s sk fk (aa . i)))
                         (insert-abs (((a as ...) us ... x ...) phd) s fk))))
           (syntax (match-abstract ((a aa as ...) x ...) (us ...) phd s y 
				   ((b bs ...) . ps) g+s sk fk i))))



      ((q (x ...) ((a) us ...) phd s y (b . ps) g+s sk fk i)
       (if (eq? (syntax->datum (syntax a)) (syntax->datum (syntax b)))
           (syntax (let ((ret (a y)))
                     (if ret
                         (match-one  (((a) us ... x ...) phd) s (cdr ret) ps
				     g+s sk fk i)
                         (insert-abs (((a) us ... x ...) phd) s fk))))
           (syntax (match-abstract ((a) x ...) (us ...) phd s y (b . ps) 
				   g+s sk fk i))))

      ((q (x ...) ((a aa as ...) us ...) phd s y (b . ps) g+s sk fk i)
       (if (eq? (syntax->datum (syntax a)) (syntax->datum (syntax b)))
           (syntax (let ((ret (a y)))
                     (if ret
                         (let ((aa  (car ret)))
                           (match-one  (((a as ...) us ... x ...) phd) s 
				       (cdr ret) ps g+s sk fk (aa . i)))
                         (insert-abs (((a as ...) us ... x ...) phd) s fk))))
           (syntax (match-abstract ((a aa as ...) x ...) (us ...) phd s y 
				   (b . ps) g+s sk fk i))))
      ((q () abs phd s y p g+s sk fk i)
       (syntax (match-phd () phd abs s y p g+s sk fk i))))))

(define-syntax match-phd
  (lambda (x)
    (syntax-case x ()
      ((_ phd (c (            )) abs . l) 
       (syntax (match-three (abs (c phd)) . l)))
      ((_ (phd ...) (c ((h a) hh ...)) abs s v (h2 h3 x) g+s sk fk i)
       (if (eq? (syntax->datum (syntax h)) (syntax->datum (syntax h2)))
           (if (eq? (syntax->datum (syntax h)) 
		    (syntax->datum (syntax h3)))                     
               (syntax (match-one (abs (a ((h a) hh ... phd ...))) s v x g+s 
                                  (set-phd-sk c sk) (set-phd-fk c fk) i))
               (syntax (match-one (abs (a ((h a) hh ... phd ...))) s v 
				  (h3 x) g+s 
                                  (set-phd-sk c sk) (set-phd-fk c fk) i)))
           (syntax (match-phd ((h a) phd ...) (c (hh ...)) abs s v (h2 h3 x)
			      g+s sk fk i))))

      ((_ (phd ...) (c ((h a) hh ...)) abs s v (h2 . l) g+s sk fk i)
       (if (eq? (syntax->datum (syntax h)) (syntax->datum (syntax h2)))
           (syntax (match-one (abs (a ((h a) hh ... phd ...))) s v l g+s 
			      (set-phd-sk c sk) (set-phd-fk c fk) i))
           (syntax (match-phd ((h a) phd ...) (c (hh ...)) abs s v (h2 . l)
			      g+s sk fk i))))
      ((_ () phd abs . l)
       (syntax (match-three (abs phd) . l))))))

(define-syntax set-phd-fk
  (syntax-rules (begin)
    ((_ abs          s cc (begin . l))  (begin . l))
    ((_ abs          s cc (fk))         (fk))
    ((_ (abs (c pp)) s cc  (fk fkk ...)) (fk (abs (cc pp)) s fkk ...))))

(define-syntax set-phd-sk
  (syntax-rules (begin)
    ((_ abs          s cc (begin . l)  i ...)  (begin . l))
    ((_ abs          s cc (fk)         i ...)  (fk))
    ((_ (abs (c pp)) s cc (fk fkk ...) i ...)  (fk (abs (cc pp)) s 
						   fkk ... i ...))))

(define-syntax match-$
  (lambda (x)
    (syntax-case x ()
      ((q abs (a ...) m (p1 p2 ...) . v)
       (with-syntax ((m+1 (datum->syntax (syntax q) 
					 (+ (syntax->datum (syntax m)) 1))))
          (syntax (match-$ abs (a ... (= 0 m p1)) m+1 (p2 ...) . v))))
      ((_ abs newpat  m ()            s v kt ke i)
       (syntax (match-one abs  s v newpat () kt ke i))))))


(define-syntax match-gen-ellipses
  (lambda (x)
    (syntax-case x ()
      ((q . l) 
       ;(pk `(match-gen-ellipses ,@(syntax->datum (syntax l))))
       (syntax (match-gen-ellipses* . l))))))

;; list? is not correctly transported hear, need a new format of abs
;; to support ellipses
(define-syntax match-gen-ellipses*
  (syntax-rules ()
    ((_ abs s v p () g+s (sk ...) fk i ((id id-ls) ...))
     (match-check-identifier p
       ;; simplest case equivalent to (p ...), just bind the list
       (let ((p v))
         (if (list? p)
             (insert-abs abs s (sk ... i))
             (insert-abs abs s fk)))
       ;; simple case, match all elements of the list
       (let loop ((ss s) (ls v) (id-ls '()) ...)
         (cond
           ((null? ls)
            (let ((id (reverse id-ls)) ...) (insert-abs abs ss (sk ... i))))
           ((pair? ls)
            (let ((w (car ls)))
              (match-one abs ss w p ((car ls) (set-car! ls))
                         (match-drop-ids (loop ss (cdr ls) (cons id id-ls) ...))
                         fk i)))
           (else
            (insert-abs abs s fk))))))

    ((_ abs s v p r g+s (sk ...) fk i ((id id-ls) ...))
     ;; general case, trailing patterns to match, keep track of the
     ;; remaining list length so we don't need any backtracking
     (match-verify-no-ellipses
      r
      (let* ((tail-len (length 'r))
             (ls v)
             (len (length ls)))
        (if (< len tail-len)
            fk
            (let loop ((ls ls) (n len) (id-ls '()) ...)
              (cond
                ((= n tail-len)
                 (let ((id (reverse id-ls)) ...)
                   (match-one abs s ls r (#f #f) (sk ...) fk i)))
                ((pair? ls)
                 (let ((w (car ls)))
                   (match-one abs s w p ((car ls) (set-car! ls))
                              (match-drop-ids
                               (loop (cdr ls) (- n 1) (cons id id-ls) ...))
                              fk
                              i)))
                (else
                 fk)))))))))



(define-syntax match-drop-ids
  (syntax-rules ()
    ((_ expr              ) expr)
    ((_ abs s expr ids ...) expr)))

(define-syntax match-gen-search
  (syntax-rules ()
    ((match-gen-search abs s v p q g+s sk fk i ((id id-ls) ...))
     (letrec ((try (lambda (w fail id-ls ...)
                     (match-one abs s w q g+s
                                (match-drop-ids
                                 (let ((id (reverse id-ls)) ...)
                                   sk))
                                (match-drop-ids (next w fail id-ls ...)) i)))
              (next (lambda (w fail id-ls ...)
                      (if (not (pair? w))
                          (fail)
                          (let ((u (car w)))
                            (match-one
                             abs s u p ((car w) (set-car! w))
                             (match-drop-ids
                              ;; accumulate the head variables from
                              ;; the p pattern, and loop over the tail
                              (let ((id-ls (cons id id-ls)) ...)
                                (let lp ((ls (cdr w)))
                                  (if (pair? ls)
                                      (try (car ls)
                                           (lambda () (lp (cdr ls)))
                                           id-ls ...)
                                      (fail)))))
                             (fail) i))))))
       ;; the initial id-ls binding here is a dummy to get the right
       ;; number of '()s
       (let ((id-ls '()) ...)
         (try v (lambda () (insert-abs abs s fk)) id-ls ...))))))

(define-syntax match-quasiquote
  (syntax-rules (unquote unquote-splicing quasiquote)
    ((_ abs s v (unquote p) g+s sk fk i)
     (match-one abs s v p g+s sk fk i))
    ((_ abs s v ((unquote-splicing p) . rest) g+s sk fk i)
     (if (pair? v)
       (match-one abs s v
                  (p . tmp)
                  (match-quasiquote s tmp rest g+s sk fk)
                  fk
                  i)
       (insert-abs abs s fk)))
    ((_ abs s v (quasiquote p) g+s sk fk i . depth)
     (match-quasiquote abs s v p g+s sk fk i #f . depth))
    ((_ abs s v (unquote p) g+s sk fk i x . depth)
     (match-quasiquote abs s v p g+s sk fk i . depth))
    ((_ abs s v (unquote-splicing p) g+s sk fk i x . depth)
     (match-quasiquote abs s v p g+s sk fk i . depth))
    ((_ abs s v (p . q) g+s sk fk i . depth)
     (if (pair? v)
       (let ((w (car v)) (x (cdr v)))
         (match-quasiquote
          abs s w p g+s
          (match-quasiquote-step s x q g+s sk fk depth)
          fk i . depth))
       (insert-abs abs s  fk)))
    ((_ abs s v #(elt ...) g+s sk fk i . depth)
     (if (vector? v)
       (let ((ls (vector->list v)))
         (match-quasiquote abs s ls (elt ...) g+s sk fk i . depth))
       (insert-abs abs s fk)))
    ((_ abs s v x g+s sk fk i . depth)
     (match-one abs s v 'x g+s sk fk i))))

(define-syntax match-quasiquote-step
  (syntax-rules ()
    ((match-quasiquote-step abs s x q g+s sk fk depth i)
     (match-quasiquote abs s x q g+s sk fk i . depth))))

(define-syntax match-extract-vars
  (lambda (x)
    (syntax-case x ()
      ((q a . l) 
       #;(pk `(match-extract-vars ,(syntax->datum (syntax l))))
       (syntax (match-extract-vars* a . l))))))


;;We must be able to extract vars in the new constructs!!
(define-syntax match-extract-vars*
  (syntax-rules (_ ___ *** ? $ <> = quote quasiquote unquote unquote-splicing 
		   and or not get! set!)
    ((match-extract-vars abs s (? pred . p) . x)
     (match-extract-vars abs s p . x))
    ((match-extract-vars abs s ($ rec . p) . x)
     (match-extract-vars abs s p . x))
    ((match-extract-vars abs s (= proc p) . x)
     (match-extract-vars abs s p . x))
    ((match-extract-vars abs s (= u m p) . x)
     (match-extract-vars abs s p . x))
    ((match-extract-vars abs s (quote x) (k kk ...) i v)
     (k abs s kk ... v))
    ((match-extract-vars abs s (unquote x) (k kk ...) i v)
     (k abs s kk ... v))
    ((match-extract-vars abs s (unquote-splicing x) (k kk ...) i v)
     (k abs s kk ... v))
    ((match-extract-vars abs s (quasiquote x) k i v)
     (match-extract-quasiquote-vars abs x k i v (#t)))
    ((match-extract-vars abs s (and . p) . x)
     (match-extract-vars abs s p . x))
    ((match-extract-vars abs s (or . p) . x)
     (match-extract-vars abs s p . x))
    ((match-extract-vars abs s (not . p) . x)
     (match-extract-vars abs s p . x))
    ;; A non-keyword pair, expand the CAR with a continuation to
    ;; expand the CDR.
    ((match-extract-vars abs s (<> f p) k i v)
     (match-extract-vars abs s p k i v))
    ((match-extract-vars abs s p k i v)
     (match-extract-vars2 abs s p k i v))))

(define-syntax match-extract-vars2
  (syntax-rules (_ ___ *** ? $ = quote quasiquote and or not get! set!)
    ((match-extract-vars abs s (p q . r) k i v)
     (match-check-ellipse
      q
      (match-extract-vars abs s (p . r) k i v)
      (match-extract-vars abs s p (match-extract-vars-step (q . r) k i v) i ())))
    ((match-extract-vars abs s (p . q) k i v)
     (match-extract-vars abs s p (match-extract-vars-step q k i v) i ()))
    ((match-extract-vars abs s #(p ...) . x)
     (match-extract-vars abs s (p ...) . x))
    ((match-extract-vars abs s _ (k kk ...) i v)    (k abs s kk ... v))
    ((match-extract-vars abs s ___ (k kk ...) i v)  (k abs s kk ... v))
    ((match-extract-vars abs s *** (k kk ...) i v)  (k abs s kk ... v))
    ;; This is the main part, the only place where we might add a new
    ;; var if it's an unbound symbol.
    ((match-extract-vars abs s p (k kk ...) (i ...) v)
     (let-syntax
         ((new-sym?
	   (lambda (y)
	     (syntax-case y (i ...)
	       ((new-sym? p sk fk) #;(pk 'new-sk) #'sk)
	       ((new-sym? x sk fk) #;(pk 'new-fk) #'fk)))))
       (new-sym? random-sym-to-match
                 (k abs s kk ... ((p p-ls) . v))
                 (k abs s kk ... v))))
    ))

(define-syntax abs-extract-vars
  (lambda (x)
    (syntax-case x ()
      ((q . l) 
       ;(pk `(abs-extract-vars ,@(syntax->datum (syntax l))))
       (syntax (abs-extract-vars* . l))))))

(define-syntax abs-extract-vars*
  (lambda (x)
    (syntax-case x ()
      ((q abs () phd p . l) (syntax (match-extract-phd () phd abs p . l)))
      ((q (abs ...) ((a x . xs) us ...) phd ((b bs ...) w ...) k i v)
       (if (eq? (syntax->datum (syntax a)) (syntax->datum (syntax b)))
           (syntax (match-extract-vars 
                    (((a . xs) us ... abs ...) phd) (w ...) k i ((x x-ls) . v)))
           (syntax (abs-extract-vars   
                    ((a x . xs) abs ...) (us ...) phd ((b bs ...) w ...)
		    k i v))))

      ((q (abs ...) ((a) us ...) phd ((b bs ...) w ...) k i v)
       (if (eq? (syntax->datum (syntax a)) (syntax->datum (syntax b)))
           (syntax (match-extract-vars 
                    (((a) us ... abs ...) phd) (w ...) k i v)))
           (syntax (abs-extract-vars   
                    ((a) abs ...) (us ...) phd ((b bs ...) w ...) k i v)))

      ((q (abs ...) ((a x . xs) us ...) phd (b w ...) k i v)
       (if (eq? (syntax->datum (syntax a)) (syntax->datum (syntax b)))
           (syntax (match-extract-vars 
                    (((a . xs) us ... abs ...) phd) (w ...) k i ((x x-ls) . v)))
           (syntax (abs-extract-vars   
                    ((a x . xs) abs ...) (us ...) phd (b w ...) k i v))))

      ((q (abs ...) ((a) us ...) phd (b w ...) k i v)
       (if (eq? (syntax->datum (syntax a)) (syntax->datum (syntax b)))
           (syntax (match-extract-vars 
                    (((a) us ... abs ...) phd) (w ...) k i v))
           (syntax (abs-extract-vars   
                    ((a) abs ...) (us ...) phd (b w ...) k i v))))
      ((q () a phd p k i v)
       (syntax (match-extract-phd () phd a p k i v))))))

(define-syntax match-extract-phd
  (lambda (x)
    (syntax-case x ()
      ((_ _ phd abs . l) 
       #;(pk 'match-extract-phd (syntax->datum #'l))
       #'(match-extract-vars2 (abs phd) . l)))))

(define-syntax match-extract-vars-step
  (syntax-rules ()
    ((_ abs s p k i v ((v2 v2-ls) ...))
     (match-extract-vars abs s p k (v2 ... . i) ((v2 v2-ls) ... . v)))
    ))

(define-syntax match-extract-quasiquote-vars
  (syntax-rules (quasiquote unquote unquote-splicing)
    ((match-extract-quasiquote-vars abs (quasiquote x) k i v d)
     (match-extract-quasiquote-vars abs x k i v (#t . d)))
    ((match-extract-quasiquote-vars abs (unquote-splicing x) k i v d)
     (match-extract-quasiquote-vars abs (unquote x) k i v d))
    ((match-extract-quasiquote-vars abs (unquote x) k i v (#t))
     (match-extract-vars abs x k i v))
    ((match-extract-quasiquote-vars abs (unquote x) k i v (#t . d))
     (match-extract-quasiquote-vars abs x k i v d))
    ((match-extract-quasiquote-vars abs (x . y) k i v (#t . d))
     (match-extract-quasiquote-vars abs
      x
      (match-extract-quasiquote-vars-step y k i v d) i ()))
    ((match-extract-quasiquote-vars abs #(x ...) k i v (#t . d))
     (match-extract-quasiquote-vars abs (x ...) k i v d))
    ((match-extract-quasiquote-vars abs x (k kk ...) i v (#t . d))
     (k abs kk ... v))
    ))

(define-syntax match-extract-quasiquote-vars-step
  (syntax-rules ()
    ((_ abs x k i v d ((v2 v2-ls) ...))
     (match-extract-quasiquote-vars abs x k (v2 ... . i) ((v2 v2-ls) ... . v) 
				    d))
    ))


(define-syntax match-define
  (syntax-rules (abstractions)
    ((q abstractions abs arg code)
     (match-extract-vars abs arg 
			 (sieve (match-define-helper0 arg code) ()) () ()))
    ((q arg code)
     (match-extract-vars ()  arg 
			 (sieve (match-define-helper0 arg code) ()) () ()))))

(define-syntax sieve
  (syntax-rules ()
    ((_ cc (w ...) ((v q) v2 ...))
     (sieve cc (v w ...) (v2 ...)))
    ((_ cc (w ...) (v v2 ...))
     (sieve cc (v w ...) (v2 ...)))
    ((_ (cc ...) w ())
     (cc ... w))))
  
(define-syntax match-define-helper0
  (lambda (x)
    (syntax-case x ()
      ((q arg code v)
       (with-syntax ((vtemp (map (lambda (x)
				   (datum->syntax
				    (syntax q) (gensym "temp")))
				 (syntax->datum (syntax v)))))
	  (syntax (match-define-helper v vtemp arg code)))))))

(define-syntax match-define-helper
  (syntax-rules ()
    ((_ (v ...) (vt ...) arg code) 
     (begin 
       (begin (define v 0) 
	      ...)
       (let ((vt 0) ...)
	 (match  code 
		 (arg (begin (set! vt v) 
			     ...)))
	 (begin (set! v vt) 
		...))))))


;;;Reading the rest from upstream

;;Utility
(define-syntax include-from-path/filtered
  (lambda (x)
    (define (hit? sexp reject-list)
      (if (null? reject-list)
	  #f
	  (let ((h (car reject-list))
		(l (cdr reject-list)))
	    (if (and (pair? sexp)
		     (eq? 'define-syntax (car sexp))
		     (pair? (cdr sexp))
		     (eq? h (cadr sexp)))
		#t
		(hit? sexp l)))))

    (define (read-filtered reject-list file)
      (with-input-from-file (%search-load-path file)
        (lambda ()
          (let loop ((sexp (read)) (out '()))
            (cond
             ((eof-object? sexp) (reverse out))
             ((hit? sexp reject-list)
              (loop (read) out))
             (else
              (loop (read) (cons sexp out))))))))

    (syntax-case x ()
      ((_ reject-list file)
       (with-syntax (((exp ...) (datum->syntax
                                 x 
                                 (read-filtered
                                  (syntax->datum #'reject-list)
                                  (syntax->datum #'file)))))
		    #'(begin exp ...))))))

(include-from-path/filtered
 (match-extract-vars  match-one       match-gen-or      match-gen-or-step 
                      match-two       match match-next  match-gen-ellipses
                      match-drop-ids  match-gen-search  match-quasiquote
                      match-quasiquote-step  match-extract-vars-step
                      match-extract-quasiquote-vars  
		      match-extract-quasiquote-vars-step)
 "ice-9/match.upstream.scm")



(define-syntax make-phd-matcher
  (syntax-rules ()
    ((_ name pd)
     (begin
       (define-syntax name 
         (lambda (x)
           (syntax-case x ()
             ((_ s . l)
              (syntax (match s -phd pd . l))))))))))
  

