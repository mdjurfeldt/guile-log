SCM scm_equal_p852271;

SCM scm_leq_p848299;

SCM scm_geq_p844327;

SCM scm_less_p840162;

SCM scm_gr_p836383;

SCM brace161892;

SCM scm_num721462;

SCM scm_string836001;

SCM scm_string808963;

SCM scm_string804418;

SCM scm_string799446;

SCM scm_string795488;

SCM scm_string780398;

SCM scm_string761574;

SCM scm_string742443;

SCM scm_string740972;


void * _soperations_s[1024];

SCM _sdls_s = SCM_BOOL_F;

SCM _sdelayers_s = SCM_BOOL_F;

SCM _sunwind__hooks_s = SCM_BOOL_F;

SCM _sunwind__parameters_s = SCM_BOOL_F;

SCM _strue_s = SCM_BOOL_F;

SCM _sfalse_s = SCM_BOOL_F;

SCM _sgp__not__n_s = SCM_BOOL_F;

SCM _sgp__is__delayed_p_s = SCM_BOOL_F;

SCM pack__start (SCM nvar, SCM nstack, SCM instructions, SCM contants, SCM tvars);

SCM pack__start (SCM nvar, SCM nstack, SCM instructions, SCM contants, SCM tvars) {
     {
        SCM ret901721;
         {
            
             {
                SCM a901722;
                SCM a901723;
                a901722 = scm_cons(scm_num721462, SCM_EOL);
                 {
                    SCM a901724;
                    SCM a901725;
                    a901724 = SCM_EOL;
                     {
                        SCM a901726;
                        SCM a901727;
                        a901726 = SCM_BOOL_F;
                         {
                            SCM vv901719;
                            vv901719 = scm_c_make_vector(5, SCM_BOOL_F);
                             {
                                SCM * vp901720;
                                vp901720 = SCM_I_VECTOR_WELTS(vv901719);
                                 {
                                    
                                    
                                    
                                    AREF(vp901720, 0) = nvar;
                                    
                                    AREF(vp901720, 1) = nstack;
                                    
                                    AREF(vp901720, 2) = contants;
                                    
                                    AREF(vp901720, 3) = instructions;
                                    
                                    AREF(vp901720, 4) = tvars;
                                    
                                    a901727 = vv901719;
                                }
                            }
                        }
                        a901725 = scm_cons(a901726, a901727);
                    }
                    a901723 = scm_cons(a901724, a901725);
                }
                ret901721 = scm_cons(a901722, a901723);
            }
        }
        return ret901721;
    }
}

SCM gp_c_vector_x (SCM x, int n, SCM s);

SCM gp_c_vector_x (SCM x, int n, SCM s) {
     {
        SCM ret901729;
         {
            
            if (SCM_I_IS_VECTOR(x) && n == SCM_I_VECTOR_LENGTH(x)) {
                ret901729 = s;
            } else {
                if (scm_is_true(gp_varp(x, s))) {
                     {
                        SCM v901728;
                        v901728 = scm_c_make_vector(n, SCM_BOOL_F);
                        
                        
                        
                         {
                            int i901730;
                            i901730 = n;
                             {
                                
                              lp901731:
                                 {
                                    
                                    if (i901730 == 0) {
                                        ret901729 = gp_gp_unify(x, v901728, s);
                                    } else {
                                         {
                                            int ii901732;
                                            ii901732 = i901730 - 1;
                                            
                                            
                                            
                                            scm_c_vector_set_x(v901728,
                                                               ii901732,
                                                               gp_mkvar(s));
                                            
                                             {
                                                int next901735;
                                                next901735 = ii901732;
                                                i901730 = next901735;
                                                goto lp901731;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    ret901729 = SCM_BOOL_F;
                }
            }
        }
        return ret901729;
    }
}

SCM gp_c_vector (SCM x, int n, SCM s);

SCM gp_c_vector (SCM x, int n, SCM s) {
     {
        SCM ret901737;
         {
            
             {
                SCM x901736;
                x901736 = gp_gp_lookup(x, s);
                
                
                
                if (SCM_I_IS_VECTOR(x901736)
                      && n == SCM_I_VECTOR_LENGTH(x901736)) {
                    ret901737 = s;
                } else {
                    ret901737 = 0;
                }
            }
        }
        return ret901737;
    }
}

SCM _smodel__lambda_s = SCM_BOOL_F;

SCM init__vm__data (SCM dls, SCM delayers, SCM unwind__hooks, SCM unwind__parameters, SCM true, SCM false, SCM gp__not__n, SCM gp__is__delayed_p, SCM model__lambda);

SCM init__vm__data (SCM dls, SCM delayers, SCM unwind__hooks, SCM unwind__parameters, SCM true, SCM false, SCM gp__not__n, SCM gp__is__delayed_p, SCM model__lambda) {
     {
        SCM ret901738;
         {
            
            _sdls_s = dls;
            
            _sdelayers_s = delayers;
            
            _sunwind__hooks_s = unwind__hooks;
            
            _sunwind__parameters_s = unwind__parameters;
            
            _strue_s = true;
            
            _sfalse_s = false;
            
            _sgp__not__n_s = gp__not__n;
            
            _sgp__is__delayed_p_s = gp__is__delayed_p;
            
            _smodel__lambda_s = model__lambda;
            
            ret901738 = SCM_BOOL_F;
        }
        return ret901738;
    }
}

SCM * vm__raw (scm_i_thread * thread, vp_t * vp, SCM * fp, SCM * sp, SCM * free, scm_t_uint32 * recieve_p, int register_p);

SCM * vm__raw (scm_i_thread * thread, vp_t * vp, SCM * fp, SCM * sp, SCM * free, scm_t_uint32 * recieve_p, int register_p) {
     {
        SCM * ret901768;
         {
            
            --sp;
            
            if (register_p) {
                 {
                    
                    AREF(_soperations_s, 97) = &&cutter;
                    
                    AREF(_soperations_s, 0) = &&store__state;
                    
                    AREF(_soperations_s, 7) = &&softie;
                    
                    AREF(_soperations_s, 8) = &&softie__light;
                    
                    AREF(_soperations_s, 1) = &&newframe;
                    
                    AREF(_soperations_s, 2) = &&newframe__light;
                    
                    AREF(_soperations_s, 3) = &&unwind;
                    
                    AREF(_soperations_s, 5) = &&unwind__tail;
                    
                    AREF(_soperations_s, 4) = &&unwind__light;
                    
                    AREF(_soperations_s, 6) = &&unwind__light__tail;
                    
                    AREF(_soperations_s, 11) = &&newframe__negation;
                    
                    AREF(_soperations_s, 12) = &&unwind__negation;
                    
                    AREF(_soperations_s, 13) = &&post__negation;
                    
                    AREF(_soperations_s, 30) = &&pre__unify;
                    
                    AREF(_soperations_s, 31) = &&post__unify;
                    
                    AREF(_soperations_s, 32) = &&post__unify__tail;
                    
                    AREF(_soperations_s, 9) = &&post__s;
                    
                    AREF(_soperations_s, 10) = &&post__q;
                    
                    AREF(_soperations_s, 33) = &&clear__sp;
                    
                    AREF(_soperations_s, 34) = &&false;
                    
                    AREF(_soperations_s, 14) = &&set;
                    
                    AREF(_soperations_s, 25) = &&sp__move;
                    
                    AREF(_soperations_s, 26) = &&sp__move__s;
                    
                    AREF(_soperations_s, 24) = &&unify;
                    
                    AREF(_soperations_s, 23) = &&unify__2;
                    
                    AREF(_soperations_s, 16) = &&unify__constant;
                    
                    AREF(_soperations_s, 17) = &&unify__instruction;
                    
                    AREF(_soperations_s, 18) = &&unify__constant__2;
                    
                    AREF(_soperations_s, 19) = &&unify__instruction__2;
                    
                    AREF(_soperations_s, 95) = &&equal__constant;
                    
                    AREF(_soperations_s, 96) = &&equal__instruction;
                    
                    AREF(_soperations_s, 20) = &&icurly;
                    
                    AREF(_soperations_s, 21) = &&ifkn;
                    
                    AREF(_soperations_s, 22) = &&icons;
                    
                    AREF(_soperations_s, 27) = &&icurly_e;
                    
                    AREF(_soperations_s, 28) = &&ifkn_e;
                    
                    AREF(_soperations_s, 29) = &&icons_e;
                    
                    AREF(_soperations_s, 35) = &&fail;
                    
                    AREF(_soperations_s, 36) = &&cc;
                    
                    AREF(_soperations_s, 37) = &&tail__cc;
                    
                    AREF(_soperations_s, 38) = &&call;
                    
                    AREF(_soperations_s, 39) = &&call__n;
                    
                    AREF(_soperations_s, 40) = &&tail__call;
                    
                    AREF(_soperations_s, 41) = &&goto__inst;
                    
                    AREF(_soperations_s, 42) = &&cut;
                    
                    AREF(_soperations_s, 43) = &&post__call;
                    
                    AREF(_soperations_s, 44) = &&post__unicall;
                    
                    AREF(_soperations_s, 56) = &&pushv;
                    
                    AREF(_soperations_s, 45) = &&push__constant;
                    
                    AREF(_soperations_s, 46) = &&push__instruction;
                    
                    AREF(_soperations_s, 47) = &&push__variable__s;
                    
                    AREF(_soperations_s, 48) = &&push__variable__v;
                    
                    AREF(_soperations_s, 49) = &&push__2variables__s;
                    
                    AREF(_soperations_s, 50) = &&push__2variables__x;
                    
                    AREF(_soperations_s, 51) = &&push__3variables__s;
                    
                    AREF(_soperations_s, 52) = &&push__3variables__x;
                    
                    AREF(_soperations_s, 54) = &&push__variable__scm;
                    
                    AREF(_soperations_s, 55) = &&pop__variable;
                    
                    AREF(_soperations_s, 62) = &&dup;
                    
                    AREF(_soperations_s, 61) = &&pop;
                    
                    AREF(_soperations_s, 60) = &&seek;
                    
                    AREF(_soperations_s, 57) = &&mk__cons;
                    
                    AREF(_soperations_s, 58) = &&mk__fkn;
                    
                    AREF(_soperations_s, 59) = &&mk__curly;
                    
                    AREF(_soperations_s, 143) = &&lognot;
                    
                    AREF(_soperations_s, 141) = &&uminus;
                    
                    AREF(_soperations_s, 103) = &&plus;
                    
                    AREF(_soperations_s, 105) = &&minus;
                    
                    AREF(_soperations_s, 107) = &&mul;
                    
                    AREF(_soperations_s, 109) = &&divide;
                    
                    AREF(_soperations_s, 117) = &&shift_l;
                    
                    AREF(_soperations_s, 120) = &&shift_r;
                    
                    AREF(_soperations_s, 133) = &&xor;
                    
                    AREF(_soperations_s, 104) = &&plus2_1;
                    
                    AREF(_soperations_s, 106) = &&minus2_1;
                    
                    AREF(_soperations_s, 108) = &&mul2_1;
                    
                    AREF(_soperations_s, 110) = &&div2_1;
                    
                    AREF(_soperations_s, 130) = &&bitand;
                    
                    AREF(_soperations_s, 132) = &&bitor;
                    
                    AREF(_soperations_s, 134) = &&xor1;
                    
                    AREF(_soperations_s, 118) = &&shiftLL;
                    
                    AREF(_soperations_s, 119) = &&shiftLR;
                    
                    AREF(_soperations_s, 121) = &&shiftRL;
                    
                    AREF(_soperations_s, 122) = &&shiftRR;
                    
                    AREF(_soperations_s, 118) = &&shiftLL;
                    
                    AREF(_soperations_s, 124) = &&modL;
                    
                    AREF(_soperations_s, 125) = &&modR;
                    
                    AREF(_soperations_s, 123) = &&modulo;
                    
                    AREF(_soperations_s, 129) = &&band;
                    
                    AREF(_soperations_s, 131) = &&bor;
                    
                    AREF(_soperations_s, 76) = &&gt;
                    
                    AREF(_soperations_s, 79) = &&ls;
                    
                    AREF(_soperations_s, 82) = &&ge;
                    
                    AREF(_soperations_s, 85) = &&le;
                    
                    AREF(_soperations_s, 77) = &&gtL;
                    
                    AREF(_soperations_s, 80) = &&ltL;
                    
                    AREF(_soperations_s, 83) = &&geL;
                    
                    AREF(_soperations_s, 86) = &&leL;
                    
                    AREF(_soperations_s, 88) = &&eq;
                    
                    AREF(_soperations_s, 89) = &&neq;
                    
                    ret901768 = sp;
                }
            } else {
                 {
                    SCM pinned_p901739;
                    int call_p901740;
                    int narg901741;
                    int nlocals901742;
                    SCM always901743;
                    SCM middle901744;
                    SCM session901745;
                    SCM cnst901746;
                    SCM variables__scm901747;
                    SCM * variables901748;
                    SCM constants__scm901749;
                    SCM * constants901750;
                    SCM instructions__scm901751;
                    SCM * instructions901752;
                    SCM tvars__scm901753;
                    SCM * tvars901754;
                    int ninst901755;
                    int nstack901756;
                    int nvar901757;
                    SCM ctrl__stack901758;
                    SCM sp__stack901759;
                    SCM * inst__pt901760;
                    int p_p901761;
                    SCM pp901762;
                    SCM s901763;
                    SCM p901764;
                    SCM cut901765;
                    SCM scut901766;
                    int iter901767;
                    pinned_p901739 = SCM_BOOL_F;
                    call_p901740 = 1;
                    narg901741 = 0;
                    nlocals901742 = 0;
                    always901743 = SCM_BOOL_F;
                    middle901744 = SCM_BOOL_F;
                    session901745 = SCM_BOOL_F;
                    cnst901746 = SCM_BOOL_F;
                    variables__scm901747 = SCM_BOOL_F;
                    variables901748 = 0;
                    constants__scm901749 = SCM_BOOL_F;
                    constants901750 = 0;
                    instructions__scm901751 = SCM_BOOL_F;
                    instructions901752 = 0;
                    tvars__scm901753 = SCM_BOOL_F;
                    tvars901754 = 0;
                    ninst901755 = 0;
                    nstack901756 = 0;
                    nvar901757 = 2;
                    ctrl__stack901758 = SCM_EOL;
                    sp__stack901759 = SCM_EOL;
                    inst__pt901760 = 0;
                    p_p901761 = 0;
                    pp901762 = SCM_BOOL_F;
                    s901763 = SCM_BOOL_F;
                    p901764 = scm_num721462;
                    cut901765 = scm_num721462;
                    scut901766 = scm_num721462;
                    iter901767 = 0;
                    
                    
                    
                     {
                        SCM x901769;
                        x901769 = AREF(free, 1);
                        
                        
                        
                        narg901741 = scm2int(SCM_CAR(x901769));
                        
                        nlocals901742 = scm2int(SCM_CDR(x901769));
                    }
                    
                     {
                        
                        always901743 = AREF(free, 2);
                        
                        middle901744 = SCM_CDR(always901743);
                        
                        session901745 = SCM_CDR(middle901744);
                        
                        cnst901746 = SCM_CDR(session901745);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        int pred902031;
                         {
                            
                            int x902032;
                            
                            int y902033;
                            
                            if (fp > sp) {
                                x902032 = fp - sp;
                            } else {
                                 {
                                    
                                    scm_misc_error("prolog-vm",
                                                   "negative stack pointer",
                                                   SCM_EOL);
                                    
                                    x902032 = 1;
                                }
                            }
                            
                            y902033 = 1;
                            pred902031 = x902032 > y902033;
                        }
                        if (pred902031) {
                             {
                                
                                p_p901761 = 0;
                                
                                s901763 = AREF(fp, -1);
                                
                                p901764 = AREF(fp, -2);
                            }
                        } else {
                            p_p901761 = 1;
                        }
                    }
                    
                    if (p_p901761) {
                        sp = fp;
                    }
                    
                     {
                        SCM * r901770;
                        r901770 = SCM_I_VECTOR_WELTS(cnst901746);
                        
                        
                        
                        nvar901757 = scm2int(AREF(r901770, 0));
                        
                        nstack901756 = scm2int(AREF(r901770, 1));
                        
                        constants__scm901749 = AREF(r901770, 2);
                        
                        instructions__scm901751 = AREF(r901770, 3);
                        
                        tvars__scm901753 = AREF(r901770, 4);
                    }
                    
                    instructions901752
                      = SCM_I_VECTOR_WELTS(instructions__scm901751);
                    
                    constants901750 = SCM_I_VECTOR_WELTS(constants__scm901749);
                    
                    tvars901754 = SCM_I_VECTOR_WELTS(tvars__scm901753);
                    
                    variables__scm901747 = SCM_CAR(session901745);
                    
                     {
                        
                        printf("get-variables\n");
                        
                        if (scm_is_true(variables__scm901747)) {
                             {
                                SCM * vv901771;
                                vv901771
                                  = SCM_I_VECTOR_WELTS(variables__scm901747);
                                
                                
                                
                                pinned_p901739
                                  = scm_variable_ref(AREF(vv901771, 3));
                                
                                variables901748 = vv901771;
                            }
                        } else {
                             {
                                int n901772;
                                n901772 = nvar901757;
                                 {
                                    SCM v901773;
                                    v901773
                                      = scm_c_make_vector(n901772, SCM_BOOL_F);
                                     {
                                        SCM * vv901774;
                                        vv901774 = SCM_I_VECTOR_WELTS(v901773);
                                         {
                                            
                                            
                                            
                                            session901745
                                              = scm_cons(v901773, cnst901746);
                                            
                                            middle901744
                                              = scm_cons(SCM_EOL,
                                                         session901745);
                                            
                                             {
                                                int i902036;
                                                i902036 = 0;
                                                 {
                                                    
                                                  lp902037:
                                                     {
                                                        
                                                        if (i902036 < n901772) {
                                                             {
                                                                int var_p902038;
                                                                var_p902038
                                                                  = scm2int(AREF(tvars901754,
                                                                                 i902036));
                                                                
                                                                
                                                                
                                                                if (var_p902038) {
                                                                    AREF(vv901774,
                                                                         i902036)
                                                                      = gp_mkvar(s901763);
                                                                }
                                                                
                                                                 {
                                                                    int next902039;
                                                                    next902039
                                                                      = i902036
                                                                          + 1;
                                                                    i902036
                                                                      = next902039;
                                                                    goto lp902037;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            AREF(vv901774, 3)
                                              = gp_get_state_token();
                                            
                                            variables901748 = vv901774;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        SCM x1901775;
                        x1901775 = SCM_CAR(always901743);
                         {
                            SCM x2901776;
                            x2901776 = SCM_CDR(x1901775);
                             {
                                
                                
                                
                                ninst901755 = scm2int(SCM_CAR(x1901775));
                                
                                if (p_p901761) {
                                     {
                                        
                                        ctrl__stack901758 = x2901776;
                                        
                                        pp901762 = SCM_BOOL_F;
                                    }
                                } else {
                                    if (ninst901755 == 0) {
                                         {
                                            
                                            ctrl__stack901758 = SCM_EOL;
                                            
                                            pp901762 = SCM_BOOL_F;
                                        }
                                    } else {
                                         {
                                            
                                            ctrl__stack901758
                                              = SCM_CAR(x2901776);
                                            
                                            pp901762 = SCM_CDR(x2901776);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if (p_p901761 || ninst901755 == 0) {
                        call_p901740 = 0;
                    }
                    
                    if (SCM_CONSP(pp901762)
                          && scm_is_eq(SCM_CDR(pp901762), p901764)) {
                        p901764 = SCM_CAR(pp901762);
                    }
                    
                    if (ninst901755 == 0) {
                         {
                            
                            AREF(variables901748, 0) = AREF(fp, -3);
                            
                            AREF(variables901748, 1) = AREF(fp, -2);
                            
                            AREF(variables901748, 2) = AREF(fp, -2);
                        }
                    }
                    
                    sp__stack901759 = SCM_CAR(middle901744);
                    
                    inst__pt901760 = instructions901752 + ninst901755;
                    
                    INTERUPT();
                    
                    gp_gc();
                    
                    cut901765 = p901764;
                    
                    scut901766 = s901763;
                    
                     {
                        void * jmp901777;
                        jmp901777
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901777;
                    }
                    
                     {
                        
                        
                      pre__unify:
                        
                        printf("%s : %d\n", "pre-unify", 30);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM n901778;
                        n901778 = *inst__pt901760;
                        
                        
                        
                        call_p901740 = 0;
                        
                        inst__pt901760 = inst__pt901760 + 1;
                        
                        if (SCM_CONSP(n901778)) {
                             {
                                int i2902044;
                                i2902044 = scm2int(SCM_CAR(n901778));
                                
                                
                                
                                AREF(fp, -(nstack901756 + i2902044))
                                  = scm_fluid_ref(_sdelayers_s);
                            }
                        } else {
                             {
                                int i2902045;
                                i2902045 = scm2int(n901778);
                                
                                
                                
                                if (1) {
                                    if (scm_is_true(pinned_p901739)) {
                                         {
                                            
                                            variables__scm901747
                                              = gp_copy_vector(&(variables901748),
                                                               nvar901757);
                                            
                                            session901745
                                              = scm_cons(variables__scm901747,
                                                         cnst901746);
                                            
                                            middle901744
                                              = scm_cons(SCM_EOL,
                                                         session901745);
                                            
                                            pinned_p901739 = SCM_BOOL_F;
                                        }
                                    }
                                }
                                
                                AREF(variables901748, i2902045)
                                  = scm_fluid_ref(_sdelayers_s);
                            }
                        }
                    }
                    
                     {
                        void * jmp901779;
                        jmp901779
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901779;
                    }
                    
                     {
                        
                        
                      post__unify:
                        
                        printf("%s : %d\n", "post-unify", 31);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM n901780;
                        n901780 = *inst__pt901760;
                         {
                            int nsloc901781;
                            nsloc901781 = scm2int(AREF(inst__pt901760, 1));
                             {
                                SCM old901784;
                                if (SCM_CONSP(n901780)) {
                                     {
                                        int i2901782;
                                        i2901782 = scm2int(SCM_CAR(n901780));
                                        
                                        
                                        
                                        old901784
                                          = AREF(fp,
                                                 -(nstack901756 + i2901782));
                                    }
                                } else {
                                     {
                                        int i2901783;
                                        i2901783 = scm2int(n901780);
                                        
                                        
                                        
                                        if (0) {
                                            if (scm_is_true(pinned_p901739)) {
                                                 {
                                                    
                                                    variables__scm901747
                                                      = gp_copy_vector(&(variables901748),
                                                                       nvar901757);
                                                    
                                                    session901745
                                                      = scm_cons(variables__scm901747,
                                                                 cnst901746);
                                                    
                                                    middle901744
                                                      = scm_cons(SCM_EOL,
                                                                 session901745);
                                                    
                                                    pinned_p901739
                                                      = SCM_BOOL_F;
                                                }
                                            }
                                        }
                                        
                                        old901784
                                          = AREF(variables901748, i2901783);
                                    }
                                }
                                 {
                                    
                                    
                                    
                                    if (SCM_CONSP(n901780)) {
                                         {
                                            int i2902049;
                                            i2902049
                                              = scm2int(SCM_CAR(n901780));
                                            
                                            
                                            
                                            AREF(fp,
                                                 -(nstack901756 + i2902049))
                                              = SCM_BOOL_F;
                                        }
                                    } else {
                                         {
                                            int i2902050;
                                            i2902050 = scm2int(n901780);
                                            
                                            
                                            
                                            if (1) {
                                                if (scm_is_true(pinned_p901739)) {
                                                     {
                                                        
                                                        variables__scm901747
                                                          = gp_copy_vector(&(variables901748),
                                                                           nvar901757);
                                                        
                                                        session901745
                                                          = scm_cons(variables__scm901747,
                                                                     cnst901746);
                                                        
                                                        middle901744
                                                          = scm_cons(SCM_EOL,
                                                                     session901745);
                                                        
                                                        pinned_p901739
                                                          = SCM_BOOL_F;
                                                    }
                                                }
                                            }
                                            
                                            AREF(variables901748, i2902050)
                                              = SCM_BOOL_F;
                                        }
                                    }
                                    
                                    inst__pt901760 = inst__pt901760 + 2;
                                    
                                    if (!scm_is_eq(scm_fluid_ref(_sdelayers_s),
                                                   old901784)) {
                                         {
                                            
                                             {
                                                SCM l902051;
                                                l902051 = SCM_EOL;
                                                 {
                                                    
                                                  lp902059:
                                                     {
                                                        
                                                        if (fp == sp) {
                                                            sp__stack901759
                                                              = l902051;
                                                        } else {
                                                             {
                                                                
                                                                sp = sp + 1;
                                                                
                                                                 {
                                                                    SCM r902060;
                                                                    r902060
                                                                      = *sp;
                                                                    
                                                                    
                                                                    
                                                                    *sp
                                                                      = SCM_BOOL_F;
                                                                    
                                                                     {
                                                                        SCM next902063;
                                                                        next902063
                                                                          = scm_cons(r902060,
                                                                                     l902051);
                                                                        l902051
                                                                          = next902063;
                                                                        goto lp902059;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            if (nsloc901781 > 0) {
                                                 {
                                                    SCM * vp902052;
                                                    vp902052
                                                      = fp
                                                          + -nstack901756
                                                               + nsloc901781;
                                                    
                                                    
                                                    
                                                     {
                                                        int i902064;
                                                        i902064 = nsloc901781;
                                                         {
                                                            
                                                          lp902069:
                                                             {
                                                                
                                                                if (i902064
                                                                      > 0) {
                                                                     {
                                                                        
                                                                        sp__stack901759
                                                                          = scm_cons(AREF(vp902052,
                                                                                          1),
                                                                                     sp__stack901759);
                                                                        
                                                                        --vp902052;
                                                                        
                                                                         {
                                                                            int next902070;
                                                                            next902070
                                                                              = i902064
                                                                                  - 1;
                                                                            i902064
                                                                              = next902070;
                                                                            goto lp902069;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            AREF(fp, 0) = _sdls_s;
                                            
                                            AREF(fp, -4) = old901784;
                                            
                                            sp = fp + -5;
                                            
                                            goto call;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp901785;
                        jmp901785
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901785;
                    }
                    
                     {
                        
                        
                      post__unify__tail:
                        
                        printf("%s : %d\n", "post-unify-tail", 32);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM n901786;
                        n901786 = *inst__pt901760;
                         {
                            SCM old901789;
                            if (SCM_CONSP(n901786)) {
                                 {
                                    int i2901787;
                                    i2901787 = scm2int(SCM_CAR(n901786));
                                    
                                    
                                    
                                    old901789
                                      = AREF(fp, -(nstack901756 + i2901787));
                                }
                            } else {
                                 {
                                    int i2901788;
                                    i2901788 = scm2int(n901786);
                                    
                                    
                                    
                                    if (0) {
                                        if (scm_is_true(pinned_p901739)) {
                                             {
                                                
                                                variables__scm901747
                                                  = gp_copy_vector(&(variables901748),
                                                                   nvar901757);
                                                
                                                session901745
                                                  = scm_cons(variables__scm901747,
                                                             cnst901746);
                                                
                                                middle901744
                                                  = scm_cons(SCM_EOL,
                                                             session901745);
                                                
                                                pinned_p901739 = SCM_BOOL_F;
                                            }
                                        }
                                    }
                                    
                                    old901789
                                      = AREF(variables901748, i2901788);
                                }
                            }
                             {
                                
                                
                                
                                if (SCM_CONSP(n901786)) {
                                     {
                                        int i2902075;
                                        i2902075 = scm2int(SCM_CAR(n901786));
                                        
                                        
                                        
                                        AREF(fp, -(nstack901756 + i2902075))
                                          = SCM_BOOL_F;
                                    }
                                } else {
                                     {
                                        int i2902076;
                                        i2902076 = scm2int(n901786);
                                        
                                        
                                        
                                        if (1) {
                                            if (scm_is_true(pinned_p901739)) {
                                                 {
                                                    
                                                    variables__scm901747
                                                      = gp_copy_vector(&(variables901748),
                                                                       nvar901757);
                                                    
                                                    session901745
                                                      = scm_cons(variables__scm901747,
                                                                 cnst901746);
                                                    
                                                    middle901744
                                                      = scm_cons(SCM_EOL,
                                                                 session901745);
                                                    
                                                    pinned_p901739
                                                      = SCM_BOOL_F;
                                                }
                                            }
                                        }
                                        
                                        AREF(variables901748, i2902076)
                                          = SCM_BOOL_F;
                                    }
                                }
                                
                                inst__pt901760 = inst__pt901760 + 1;
                                
                                if (!scm_is_eq(scm_fluid_ref(_sdelayers_s),
                                               old901789)) {
                                     {
                                        
                                         {
                                            SCM l902077;
                                            l902077 = SCM_EOL;
                                             {
                                                
                                              lp902084:
                                                 {
                                                    
                                                    if (fp == sp) {
                                                        sp__stack901759
                                                          = l902077;
                                                    } else {
                                                         {
                                                            
                                                            sp = sp + 1;
                                                            
                                                             {
                                                                SCM r902085;
                                                                r902085 = *sp;
                                                                
                                                                
                                                                
                                                                *sp
                                                                  = SCM_BOOL_F;
                                                                
                                                                 {
                                                                    SCM next902088;
                                                                    next902088
                                                                      = scm_cons(r902085,
                                                                                 l902077);
                                                                    l902077
                                                                      = next902088;
                                                                    goto lp902084;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                        AREF(fp, 0) = _sdls_s;
                                        
                                        AREF(fp, -4) = old901789;
                                        
                                        sp = fp + -5;
                                        
                                        goto tail__call;
                                    }
                                } else {
                                    goto cc;
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      clear__sp:
                        
                        printf("%s : %d\n", "clear-sp", 33);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM l901790;
                        l901790 = SCM_EOL;
                         {
                            
                          lp902091:
                             {
                                
                                if (fp == sp) {
                                    sp__stack901759 = l901790;
                                } else {
                                     {
                                        
                                        sp = sp + 1;
                                        
                                         {
                                            SCM r902092;
                                            r902092 = *sp;
                                            
                                            
                                            
                                            *sp = SCM_BOOL_F;
                                            
                                             {
                                                SCM next902095;
                                                next902095
                                                  = scm_cons(r902092, l901790);
                                                l901790 = next902095;
                                                goto lp902091;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                      lp902096:
                         {
                            
                            if (sp > fp) {
                                 {
                                    
                                    printf("wrong stack state sp - xp = %d, will equalize\n",
                                           (fp - sp));
                                    
                                    sp = fp;
                                }
                            } else {
                                if (sp < fp) {
                                     {
                                        
                                        sp = sp + 1;
                                        
                                        *sp = SCM_BOOL_F;
                                        
                                        goto lp902096;
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp901791;
                        jmp901791
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901791;
                    }
                    
                     {
                        
                        
                      cc:
                        
                        printf("%s : %d\n", "cc", 36);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM st901793;
                         {
                            SCM l901792;
                            l901792 = SCM_EOL;
                             {
                                
                              lp902099:
                                 {
                                    
                                    if (fp == sp) {
                                        st901793 = l901792;
                                    } else {
                                         {
                                            
                                            sp = sp + 1;
                                            
                                             {
                                                SCM r902100;
                                                r902100 = *sp;
                                                
                                                
                                                
                                                *sp = SCM_BOOL_F;
                                                
                                                 {
                                                    SCM next902103;
                                                    next902103
                                                      = scm_cons(r902100,
                                                                 l901792);
                                                    l901792 = next902103;
                                                    goto lp902099;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                         {
                            SCM p901794;
                             {
                                
                                scm_simple_format(SCM_BOOL_T,
                                                  scm_string740972,
                                                  scm_list_2(p901764, pp901762));
                                
                                if (SCM_I_INUMP(p901764)) {
                                    if (SCM_CONSP(pp901762)
                                          && scm_is_eq(SCM_CAR(pp901762),
                                                       p901764)) {
                                        p901794 = SCM_CDR(pp901762);
                                    } else {
                                         {
                                            
                                            printf("MAKEP-P-\n");
                                            
                                            p901794
                                              = gp_custom_fkn(_smodel__lambda_s,
                                                              scm_cons(scm_from_int(1),
                                                                       scm_from_int(nlocals901742)),
                                                              scm_cons(scm_cons(p901764,
                                                                                ctrl__stack901758),
                                                                       scm_is_eq(sp__stack901759,
                                                                                 SCM_EOL) ? middle901744 : scm_cons(sp__stack901759,
                                                                                                                    session901745)));
                                        }
                                    }
                                } else {
                                    p901794 = p901764;
                                }
                            }
                             {
                                SCM cc901795;
                                cc901795 = AREF(variables901748, 0);
                                 {
                                    
                                    
                                    
                                    scm_simple_format(SCM_BOOL_T,
                                                      scm_string742443,
                                                      scm_list_1(st901793));
                                    
                                     {
                                        
                                      lp902104:
                                         {
                                            
                                            if (sp > fp) {
                                                 {
                                                    
                                                    printf("wrong stack state sp - xp = %d, will equalize\n",
                                                           (fp - sp));
                                                    
                                                    sp = fp;
                                                }
                                            } else {
                                                if (sp < fp) {
                                                     {
                                                        
                                                        sp = sp + 1;
                                                        
                                                        *sp = SCM_BOOL_F;
                                                        
                                                        goto lp902104;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                    AREF(fp, 0) = cc901795;
                                    
                                    AREF(fp, -1) = s901763;
                                    
                                    AREF(fp, -2) = p901794;
                                    
                                    sp = fp + -3;
                                    
                                    goto ret;
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      cutter:
                        
                        printf("%s : %d\n", "cutter", 97);
                    }
                    
                     {
                        ulong v901796;
                        v901796 = scm2ulong(*(inst__pt901760));
                         {
                            ulong vc901797;
                            vc901797 = v901796 & 65535;
                             {
                                ulong vcs901798;
                                vcs901798 = v901796 >> 16 & 65535;
                                 {
                                    ulong q901799;
                                    q901799 = v901796 >> 32;
                                     {
                                        SCM xs901800;
                                        if (q901799 & 1) {
                                            xs901800
                                              = AREF(variables901748, vc901797);
                                        } else {
                                            xs901800
                                              = AREF(fp,
                                                     -(nstack901756 + vc901797));
                                        }
                                         {
                                            SCM xcs901801;
                                            if (q901799 & 2) {
                                                xcs901801
                                                  = AREF(variables901748,
                                                         vcs901798);
                                            } else {
                                                xcs901801
                                                  = AREF(fp,
                                                         -(nstack901756
                                                             + vcs901798));
                                            }
                                             {
                                                
                                                
                                                
                                                inst__pt901760
                                                  = inst__pt901760 + 1;
                                                
                                                cut901765 = xs901800;
                                                
                                                scut901766 = xcs901801;
                                                
                                                AREF(variables901748, 2)
                                                  = xs901800;
                                                
                                                 {
                                                    void * jmp902109;
                                                    jmp902109
                                                      = AREF(_soperations_s,
                                                             scm2int(*(inst__pt901760)));
                                                    
                                                    
                                                    
                                                    ++inst__pt901760;
                                                    
                                                    goto *jmp902109;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      call:
                        
                        printf("%s : %d\n", "call", 38);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM p0901802;
                        p0901802 = p901764;
                         {
                            SCM p901803;
                             {
                                
                                scm_simple_format(SCM_BOOL_T,
                                                  scm_string740972,
                                                  scm_list_2(p901764, pp901762));
                                
                                if (SCM_I_INUMP(p901764)) {
                                    if (SCM_CONSP(pp901762)
                                          && scm_is_eq(SCM_CAR(pp901762),
                                                       p901764)) {
                                        p901803 = SCM_CDR(pp901762);
                                    } else {
                                         {
                                            
                                            printf("MAKE-P");
                                            
                                            p901803
                                              = gp_custom_fkn(_smodel__lambda_s,
                                                              scm_cons(scm_from_int(1),
                                                                       scm_from_int(nlocals901742)),
                                                              scm_cons(scm_cons(scm_from_int((inst__pt901760
                                                                                                - instructions901752)),
                                                                                ctrl__stack901758),
                                                                       scm_is_eq(sp__stack901759,
                                                                                 SCM_EOL) ? middle901744 : scm_cons(sp__stack901759,
                                                                                                                    session901745)));
                                        }
                                    }
                                } else {
                                    p901803 = p901764;
                                }
                            }
                             {
                                SCM cc901804;
                                cc901804
                                  = gp_custom_fkn(_smodel__lambda_s,
                                                  scm_cons(scm_from_int(3),
                                                           scm_from_int(nlocals901742)),
                                                  scm_cons(scm_cons(scm_from_int((inst__pt901760
                                                                                    - instructions901752)),
                                                                    scm_cons(ctrl__stack901758,
                                                                             (SCM_CONSP(pp901762)
                                                                                && scm_is_eq(SCM_CAR(pp901762),
                                                                                             p0901802)) ? pp901762 : scm_cons(p0901802,
                                                                                                                              p901803))),
                                                           scm_is_eq(sp__stack901759,
                                                                     SCM_EOL) ? middle901744 : scm_cons(sp__stack901759,
                                                                                                        session901745)));
                                 {
                                    
                                    
                                    
                                    AREF(fp, -1) = s901763;
                                    
                                    AREF(fp, -2) = p901803;
                                    
                                    AREF(fp, -3) = cc901804;
                                    
                                    goto ret;
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      call__n:
                        
                        printf("%s : %d\n", "call-n", 39);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM na901805;
                        na901805 = *inst__pt901760;
                         {
                            SCM p0901806;
                            p0901806 = p901764;
                             {
                                SCM p901807;
                                 {
                                    
                                    scm_simple_format(SCM_BOOL_T,
                                                      scm_string740972,
                                                      scm_list_2(p901764,
                                                                 pp901762));
                                    
                                    if (SCM_I_INUMP(p901764)) {
                                        if (SCM_CONSP(pp901762)
                                              && scm_is_eq(SCM_CAR(pp901762),
                                                           p901764)) {
                                            p901807 = SCM_CDR(pp901762);
                                        } else {
                                             {
                                                
                                                printf("MAKE-P");
                                                
                                                p901807
                                                  = gp_custom_fkn(_smodel__lambda_s,
                                                                  scm_cons(scm_from_int(1),
                                                                           scm_from_int(nlocals901742)),
                                                                  scm_cons(scm_cons(scm_from_int((inst__pt901760
                                                                                                    - instructions901752)),
                                                                                    ctrl__stack901758),
                                                                           scm_is_eq(sp__stack901759,
                                                                                     SCM_EOL) ? middle901744 : scm_cons(sp__stack901759,
                                                                                                                        session901745)));
                                            }
                                        }
                                    } else {
                                        p901807 = p901764;
                                    }
                                }
                                 {
                                    SCM cc901808;
                                    cc901808
                                      = gp_custom_fkn(_smodel__lambda_s,
                                                      scm_cons(na901805,
                                                               scm_from_int(nlocals901742)),
                                                      scm_cons(scm_cons(scm_from_int((inst__pt901760
                                                                                        + 1
                                                                                        - instructions901752)),
                                                                        scm_cons(ctrl__stack901758,
                                                                                 (SCM_CONSP(pp901762)
                                                                                    && scm_is_eq(SCM_CAR(pp901762),
                                                                                                 p0901806)) ? pp901762 : scm_cons(p0901806,
                                                                                                                                  p901807))),
                                                               scm_is_eq(sp__stack901759,
                                                                         SCM_EOL) ? middle901744 : scm_cons(sp__stack901759,
                                                                                                            session901745)));
                                     {
                                        
                                        
                                        
                                        AREF(fp, -1) = s901763;
                                        
                                        AREF(fp, -2) = p901807;
                                        
                                        AREF(fp, -3) = cc901808;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      tail__call:
                        
                        printf("%s : %d\n", "tail-call", 40);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM p901809;
                        SCM cc901810;
                         {
                            
                            scm_simple_format(SCM_BOOL_T,
                                              scm_string740972,
                                              scm_list_2(p901764, pp901762));
                            
                            if (SCM_I_INUMP(p901764)) {
                                if (SCM_CONSP(pp901762)
                                      && scm_is_eq(SCM_CAR(pp901762), p901764)) {
                                    p901809 = SCM_CDR(pp901762);
                                } else {
                                     {
                                        
                                        printf("MAKEP-P-\n");
                                        
                                        p901809
                                          = gp_custom_fkn(_smodel__lambda_s,
                                                          scm_cons(scm_from_int(1),
                                                                   scm_from_int(nlocals901742)),
                                                          scm_cons(scm_cons(p901764,
                                                                            ctrl__stack901758),
                                                                   scm_is_eq(sp__stack901759,
                                                                             SCM_EOL) ? middle901744 : scm_cons(sp__stack901759,
                                                                                                                session901745)));
                                    }
                                }
                            } else {
                                p901809 = p901764;
                            }
                        }
                        cc901810 = AREF(variables901748, 0);
                        
                        
                        
                        AREF(fp, -1) = s901763;
                        
                        AREF(fp, -2) = p901809;
                        
                        AREF(fp, -3) = cc901810;
                        
                        goto ret;
                    }
                    
                     {
                        
                        
                      tail__cc:
                        
                        printf("%s : %d\n", "tail-cc", 37);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM p901811;
                        SCM cc901812;
                         {
                            
                            scm_simple_format(SCM_BOOL_T,
                                              scm_string740972,
                                              scm_list_2(p901764, pp901762));
                            
                            if (SCM_I_INUMP(p901764)) {
                                if (SCM_CONSP(pp901762)
                                      && scm_is_eq(SCM_CAR(pp901762), p901764)) {
                                    p901811 = SCM_CDR(pp901762);
                                } else {
                                     {
                                        
                                        printf("MAKE-P");
                                        
                                        p901811
                                          = gp_custom_fkn(_smodel__lambda_s,
                                                          scm_cons(scm_from_int(1),
                                                                   scm_from_int(nlocals901742)),
                                                          scm_cons(scm_cons(scm_from_int((inst__pt901760
                                                                                            - instructions901752)),
                                                                            ctrl__stack901758),
                                                                   scm_is_eq(sp__stack901759,
                                                                             SCM_EOL) ? middle901744 : scm_cons(sp__stack901759,
                                                                                                                session901745)));
                                    }
                                }
                            } else {
                                p901811 = p901764;
                            }
                        }
                        cc901812 = AREF(variables901748, 0);
                        
                        
                        
                        AREF(fp, 0) = cc901812;
                        
                        AREF(fp, -1) = s901763;
                        
                        AREF(fp, -2) = p901811;
                        
                        goto ret;
                    }
                    
                     {
                        
                        
                      store__state:
                        
                        printf("%s : %d\n", "store-state", 0);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM tag901813;
                        SCM np901814;
                        tag901813 = AREF(inst__pt901760, 0);
                        np901814 = AREF(inst__pt901760, 1);
                        
                        
                        
                        inst__pt901760 = inst__pt901760 + 2;
                        
                         {
                            int v902120;
                            v902120 = scm2int(tag901813);
                            
                            
                            
                            AREF(fp, -(nstack901756 + v902120)) = p901764;
                        }
                        
                        INTERUPT();
                        
                         {
                            void * jmp902121;
                            jmp902121
                              = AREF(_soperations_s,
                                     scm2int(*(inst__pt901760)));
                            
                            
                            
                            ++inst__pt901760;
                            
                            goto *jmp902121;
                        }
                    }
                    
                     {
                        
                        
                      newframe__light:
                        
                        printf("%s : %d\n", "newframe-light", 2);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM tag901815;
                        SCM np901816;
                        tag901815 = AREF(inst__pt901760, 0);
                        np901816 = AREF(inst__pt901760, 1);
                        
                        
                        
                        inst__pt901760 = inst__pt901760 + 2;
                        
                         {
                            int v902125;
                            v902125 = scm2int(tag901815);
                            
                            
                            
                            AREF(fp, -(nstack901756 + v902125)) = p901764;
                        }
                        
                        p901764 = np901816;
                        
                        INTERUPT();
                        
                         {
                            void * jmp902126;
                            jmp902126
                              = AREF(_soperations_s,
                                     scm2int(*(inst__pt901760)));
                            
                            
                            
                            ++inst__pt901760;
                            
                            goto *jmp902126;
                        }
                    }
                    
                     {
                        
                        
                      newframe:
                        
                        printf("%s : %d\n", "newframe", 1);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM np901817;
                        int tp901818;
                        SCM fr901820;
                        np901817 = AREF(inst__pt901760, 0);
                        tp901818 = scm2int(AREF(inst__pt901760, 1));
                         {
                            
                             {
                                SCM l901819;
                                l901819
                                  = scm_fluid_ref(_sunwind__parameters_s);
                                 {
                                    
                                  lp902132:
                                     {
                                        
                                        if (SCM_CONSP(l901819)) {
                                             {
                                                
                                                 {
                                                    SCM a902136;
                                                    SCM a902137;
                                                    a902136
                                                      = _sunwind__parameters_s;
                                                     {
                                                        SCM x902133;
                                                        x902133
                                                          = SCM_CAR(l901819);
                                                        
                                                        
                                                        
                                                         {
                                                            SCM a902142;
                                                            SCM a902143;
                                                            a902142 = x902133;
                                                             {
                                                                SCM a902146;
                                                                 {
                                                                    SCM l902138;
                                                                    l902138
                                                                      = x902133;
                                                                     {
                                                                        SCM r902139;
                                                                        r902139
                                                                          = SCM_EOL;
                                                                         {
                                                                            
                                                                          lp902147:
                                                                             {
                                                                                
                                                                                if (SCM_CONSP(l902138)) {
                                                                                     {
                                                                                        SCM next902149;
                                                                                        SCM next902150;
                                                                                        next902149
                                                                                          = SCM_CDR(l902138);
                                                                                         {
                                                                                            SCM f902148;
                                                                                            f902148
                                                                                              = SCM_CAR(l902138);
                                                                                            
                                                                                            
                                                                                            
                                                                                            next902150
                                                                                              = scm_cons(scm_cons(f902148,
                                                                                                                  scm_call_0(f902148)),
                                                                                                         r902139);
                                                                                        }
                                                                                        l902138
                                                                                          = next902149;
                                                                                        r902139
                                                                                          = next902150;
                                                                                        goto lp902147;
                                                                                    }
                                                                                } else {
                                                                                    a902146
                                                                                      = r902139;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                a902143
                                                                  = scm_reverse(a902146);
                                                            }
                                                            a902137
                                                              = scm_cons(a902142,
                                                                         a902143);
                                                        }
                                                    }
                                                    gp_with_fluid(a902136,
                                                                  a902137);
                                                }
                                                
                                                 {
                                                    SCM next902151;
                                                    next902151
                                                      = SCM_CDR(l901819);
                                                    l901819 = next902151;
                                                    goto lp902132;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                            fr901820 = gp_gp_newframe(s901763);
                        }
                        
                        
                        
                        inst__pt901760 = inst__pt901760 + 2;
                        
                         {
                            SCM a902154;
                            SCM a902155;
                            SCM a902156;
                             {
                                SCM a902157;
                                SCM a902158;
                                SCM a902159;
                                a902157 = np901817;
                                 {
                                    SCM a902160;
                                    SCM a902161;
                                    SCM a902162;
                                    a902160 = p901764;
                                     {
                                        SCM sfr902130;
                                        sfr902130
                                          = gp_cons_simple(s901763,
                                                           fr901820,
                                                           s901763);
                                        
                                        
                                        
                                        if (tp901818) {
                                            a902161
                                              = gp_cons_simple(sfr902130,
                                                               scm_fluid_ref(_sdelayers_s),
                                                               s901763);
                                        } else {
                                            a902161
                                              = gp_cons_simple(sfr902130,
                                                               SCM_BOOL_F,
                                                               s901763);
                                        }
                                    }
                                    a902162 = s901763;
                                    a902158
                                      = gp_cons_simple(a902160,
                                                       a902161,
                                                       a902162);
                                }
                                a902159 = s901763;
                                a902154
                                  = gp_cons_simple(a902157, a902158, a902159);
                            }
                            a902155 = ctrl__stack901758;
                            a902156 = s901763;
                            ctrl__stack901758
                              = gp_cons_simple(a902154, a902155, a902156);
                        }
                        
                        s901763 = fr901820;
                        
                        p901764 = np901817;
                        
                        INTERUPT();
                        
                         {
                            void * jmp902131;
                            jmp902131
                              = AREF(_soperations_s,
                                     scm2int(*(inst__pt901760)));
                            
                            
                            
                            ++inst__pt901760;
                            
                            goto *jmp902131;
                        }
                    }
                    
                     {
                        
                        
                      newframe__negation:
                        
                        printf("%s : %d\n", "newframe-negation", 11);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM np901821;
                        int tp901822;
                        SCM so901823;
                        SCM ss901825;
                        np901821 = AREF(inst__pt901760, 0);
                        tp901822 = scm2int(AREF(inst__pt901760, 1));
                        so901823 = s901763;
                         {
                            
                             {
                                SCM l901824;
                                l901824
                                  = scm_fluid_ref(_sunwind__parameters_s);
                                 {
                                    
                                  lp902165:
                                     {
                                        
                                        if (SCM_CONSP(l901824)) {
                                             {
                                                
                                                 {
                                                    SCM a902169;
                                                    SCM a902170;
                                                    a902169
                                                      = _sunwind__parameters_s;
                                                     {
                                                        SCM x902166;
                                                        x902166
                                                          = SCM_CAR(l901824);
                                                        
                                                        
                                                        
                                                         {
                                                            SCM a902175;
                                                            SCM a902176;
                                                            a902175 = x902166;
                                                             {
                                                                SCM a902179;
                                                                 {
                                                                    SCM l902171;
                                                                    l902171
                                                                      = x902166;
                                                                     {
                                                                        SCM r902172;
                                                                        r902172
                                                                          = SCM_EOL;
                                                                         {
                                                                            
                                                                          lp902180:
                                                                             {
                                                                                
                                                                                if (SCM_CONSP(l902171)) {
                                                                                     {
                                                                                        SCM next902182;
                                                                                        SCM next902183;
                                                                                        next902182
                                                                                          = SCM_CDR(l902171);
                                                                                         {
                                                                                            SCM f902181;
                                                                                            f902181
                                                                                              = SCM_CAR(l902171);
                                                                                            
                                                                                            
                                                                                            
                                                                                            next902183
                                                                                              = scm_cons(scm_cons(f902181,
                                                                                                                  scm_call_0(f902181)),
                                                                                                         r902172);
                                                                                        }
                                                                                        l902171
                                                                                          = next902182;
                                                                                        r902172
                                                                                          = next902183;
                                                                                        goto lp902180;
                                                                                    }
                                                                                } else {
                                                                                    a902179
                                                                                      = r902172;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                a902176
                                                                  = scm_reverse(a902179);
                                                            }
                                                            a902170
                                                              = scm_cons(a902175,
                                                                         a902176);
                                                        }
                                                    }
                                                    gp_with_fluid(a902169,
                                                                  a902170);
                                                }
                                                
                                                 {
                                                    SCM next902184;
                                                    next902184
                                                      = SCM_CDR(l901824);
                                                    l901824 = next902184;
                                                    goto lp902165;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                            ss901825 = gp_gp_newframe(s901763);
                        }
                        
                        
                        
                        inst__pt901760 = inst__pt901760 + 2;
                        
                        s901763 = ss901825;
                        
                        s901763
                          = gp_set(_sgp__not__n_s,
                                   SCM_I_MAKINUM((1
                                                    + SCM_I_INUM(gp_gp_lookup(_sgp__not__n_s,
                                                                              s901763)))),
                                   s901763);
                        
                        s901763
                          = gp_set(_sgp__is__delayed_p_s, SCM_BOOL_F, s901763);
                        
                         {
                            SCM a902187;
                            SCM a902188;
                            SCM a902189;
                             {
                                SCM a902190;
                                SCM a902191;
                                SCM a902192;
                                a902190 = np901821;
                                 {
                                    SCM a902193;
                                    SCM a902194;
                                    SCM a902195;
                                    a902193 = p901764;
                                     {
                                        SCM sfr902163;
                                        sfr902163
                                          = gp_cons_simple(so901823,
                                                           gp_cons_simple(ss901825,
                                                                          scut901766,
                                                                          so901823),
                                                           so901823);
                                        
                                        
                                        
                                        if (tp901822) {
                                            a902194
                                              = gp_cons_simple(sfr902163,
                                                               scm_fluid_ref(_sdelayers_s),
                                                               so901823);
                                        } else {
                                            a902194
                                              = gp_cons_simple(sfr902163,
                                                               SCM_BOOL_F,
                                                               so901823);
                                        }
                                    }
                                    a902195 = so901823;
                                    a902191
                                      = gp_cons_simple(a902193,
                                                       a902194,
                                                       a902195);
                                }
                                a902192 = so901823;
                                a902187
                                  = gp_cons_simple(a902190, a902191, a902192);
                            }
                            a902188 = ctrl__stack901758;
                            a902189 = so901823;
                            ctrl__stack901758
                              = gp_cons_simple(a902187, a902188, a902189);
                        }
                        
                        p901764 = np901821;
                        
                        cut901765 = np901821;
                        
                        INTERUPT();
                        
                         {
                            void * jmp902164;
                            jmp902164
                              = AREF(_soperations_s,
                                     scm2int(*(inst__pt901760)));
                            
                            
                            
                            ++inst__pt901760;
                            
                            goto *jmp902164;
                        }
                    }
                    
                     {
                        
                        
                      post__negation:
                        
                        printf("%s : %d\n", "post-negation", 13);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM np901826;
                        np901826 = AREF(inst__pt901760, 0);
                        
                        
                        
                        cut901765 = AREF(inst__pt901760, 1);
                        
                        inst__pt901760 = inst__pt901760 + 2;
                        
                         {
                            SCM fr902196;
                            int n902197;
                            SCM d902198;
                             {
                                
                              lp902205:
                                 {
                                    
                                     {
                                        SCM x902206;
                                        x902206 = car(ctrl__stack901758);
                                        
                                        
                                        
                                        if (scm_is_eq(car(x902206), np901826)) {
                                             {
                                                SCM x1902207;
                                                x1902207 = cdr(x902206);
                                                 {
                                                    SCM x2902208;
                                                    x2902208 = cdr(x1902207);
                                                     {
                                                        SCM x3902209;
                                                        x3902209
                                                          = cdr(x2902208);
                                                         {
                                                            SCM s0902210;
                                                            s0902210
                                                              = car(x2902208);
                                                             {
                                                                SCM x4902211;
                                                                x4902211
                                                                  = cdr(x2902208);
                                                                 {
                                                                    SCM fr902212;
                                                                    fr902212
                                                                      = car(x4902211);
                                                                     {
                                                                        SCM sc902213;
                                                                        sc902213
                                                                          = cdr(x4902211);
                                                                         {
                                                                            
                                                                            
                                                                            
                                                                            p901764
                                                                              = car(x1902207);
                                                                            
                                                                            s901763
                                                                              = s0902210;
                                                                            
                                                                            scut901766
                                                                              = sc902213;
                                                                            
                                                                            ctrl__stack901758
                                                                              = cdr(ctrl__stack901758);
                                                                            
                                                                            if (scm_is_true(x3902209)) {
                                                                                scm_fluid_set_x(_sdelayers_s,
                                                                                                x3902209);
                                                                            }
                                                                            
                                                                            fr902196
                                                                              = fr902212;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                             {
                                                
                                                ctrl__stack901758
                                                  = cdr(ctrl__stack901758);
                                                
                                                goto lp902205;
                                            }
                                        }
                                    }
                                }
                            }
                            n902197
                              = scm2int(gp_gp_lookup(_sgp__not__n_s, s901763));
                            d902198
                              = gp_gp_lookup(_sgp__is__delayed_p_s, s901763);
                            
                            
                            
                             {
                                
                                scm_fluid_set_x(_sunwind__hooks_s, SCM_EOL);
                                
                                 {
                                    SCM l902201;
                                    l902201
                                      = SCM_CDR(scm_fluid_ref(_sunwind__parameters_s));
                                     {
                                        
                                      lp902214:
                                         {
                                            
                                            if (SCM_CONSP(l902201)) {
                                                 {
                                                    SCM f902215;
                                                    f902215 = SCM_CAR(l902201);
                                                    
                                                    
                                                    
                                                    scm_call_1(SCM_CAR(f902215),
                                                               SCM_CDR(f902215));
                                                    
                                                     {
                                                        SCM next902216;
                                                        next902216
                                                          = SCM_CDR(l902201);
                                                        l902201 = next902216;
                                                        goto lp902214;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                 {
                                    SCM l902202;
                                    l902202
                                      = scm_reverse(scm_fluid_ref(_sunwind__hooks_s));
                                    
                                    
                                    
                                    if (SCM_CONSP(l902202)) {
                                         {
                                            
                                            gp_gp_unwind(fr902196);
                                            
                                             {
                                                SCM l902217;
                                                l902217 = l902202;
                                                 {
                                                    
                                                  lp902218:
                                                     {
                                                        
                                                        if (SCM_CONSP(l902217)) {
                                                             {
                                                                
                                                                scm_call_3(SCM_CAR(l902217),
                                                                           fr902196,
                                                                           _sfalse_s,
                                                                           _strue_s);
                                                                
                                                                gp_gp_unwind(fr902196);
                                                                
                                                                 {
                                                                    SCM next902219;
                                                                    next902219
                                                                      = SCM_CDR(l902217);
                                                                    l902217
                                                                      = next902219;
                                                                    goto lp902218;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            gp_gp_unwind_tail(fr902196);
                                        }
                                    } else {
                                        gp_gp_unwind_tail(fr902196);
                                    }
                                }
                            }
                            
                            if (n902197 > 1 && scm_is_true(d902198)) {
                                 {
                                    
                                    gp_fluid_force_bang(_sgp__is__delayed_p_s,
                                                        SCM_BOOL_T,
                                                        s901763);
                                    
                                    if (SCM_I_INUMP(p901764)) {
                                         {
                                            
                                            inst__pt901760
                                              = instructions901752
                                                  + scm2int(p901764);
                                            
                                             {
                                                void * jmp902203;
                                                jmp902203
                                                  = AREF(_soperations_s,
                                                         scm2int(*(inst__pt901760)));
                                                
                                                
                                                
                                                ++inst__pt901760;
                                                
                                                goto *jmp902203;
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            AREF(fp, 0) = p901764;
                                            
                                            sp = fp + -1;
                                            
                                            goto ret;
                                        }
                                    }
                                }
                            } else {
                                 {
                                    void * jmp902204;
                                    jmp902204
                                      = AREF(_soperations_s,
                                             scm2int(*(inst__pt901760)));
                                    
                                    
                                    
                                    ++inst__pt901760;
                                    
                                    goto *jmp902204;
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      post__s:
                        
                        printf("%s : %d\n", "post-s", 9);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM np901827;
                        np901827 = AREF(inst__pt901760, 0);
                        
                        
                        
                        cut901765 = AREF(inst__pt901760, 1);
                        
                        inst__pt901760 = inst__pt901760 + 2;
                        
                         {
                            
                          lp902229:
                             {
                                
                                 {
                                    SCM x902230;
                                    x902230 = car(ctrl__stack901758);
                                    
                                    
                                    
                                    if (scm_is_eq(car(x902230), np901827)) {
                                         {
                                            SCM x1902231;
                                            x1902231 = cdr(x902230);
                                             {
                                                SCM x2902232;
                                                x2902232 = cdr(x1902231);
                                                 {
                                                    SCM x3902233;
                                                    x3902233 = cdr(x2902232);
                                                     {
                                                        
                                                        
                                                        
                                                        p901764
                                                          = car(x1902231);
                                                        
                                                        ctrl__stack901758
                                                          = cdr(ctrl__stack901758);
                                                        
                                                         {
                                                            SCM sx902234;
                                                            sx902234
                                                              = car(x2902232);
                                                             {
                                                                SCM fr902235;
                                                                fr902235
                                                                  = cdr(sx902234);
                                                                 {
                                                                    SCM ss902236;
                                                                    ss902236
                                                                      = car(sx902234);
                                                                     {
                                                                        
                                                                        
                                                                        
                                                                        if (scm_is_true(x3902233)) {
                                                                            scm_fluid_set_x(_sdelayers_s,
                                                                                            x3902233);
                                                                        }
                                                                        
                                                                        s901763
                                                                          = ss902236;
                                                                        
                                                                        
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            ctrl__stack901758
                                              = cdr(ctrl__stack901758);
                                            
                                            goto lp902229;
                                        }
                                    }
                                }
                            }
                        }
                        
                         {
                            
                            scm_fluid_set_x(_sunwind__hooks_s, SCM_EOL);
                            
                             {
                                SCM l902224;
                                l902224
                                  = SCM_CDR(scm_fluid_ref(_sunwind__parameters_s));
                                 {
                                    
                                  lp902237:
                                     {
                                        
                                        if (SCM_CONSP(l902224)) {
                                             {
                                                SCM f902238;
                                                f902238 = SCM_CAR(l902224);
                                                
                                                
                                                
                                                scm_call_1(SCM_CAR(f902238),
                                                           SCM_CDR(f902238));
                                                
                                                 {
                                                    SCM next902239;
                                                    next902239
                                                      = SCM_CDR(l902224);
                                                    l902224 = next902239;
                                                    goto lp902237;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                             {
                                SCM l902225;
                                l902225
                                  = scm_reverse(scm_fluid_ref(_sunwind__hooks_s));
                                
                                
                                
                                if (SCM_CONSP(l902225)) {
                                     {
                                        
                                        gp_gp_unwind(s901763);
                                        
                                         {
                                            SCM l902240;
                                            l902240 = l902225;
                                             {
                                                
                                              lp902241:
                                                 {
                                                    
                                                    if (SCM_CONSP(l902240)) {
                                                         {
                                                            
                                                            scm_call_3(SCM_CAR(l902240),
                                                                       s901763,
                                                                       _sfalse_s,
                                                                       _strue_s);
                                                            
                                                            gp_gp_unwind(s901763);
                                                            
                                                             {
                                                                SCM next902242;
                                                                next902242
                                                                  = SCM_CDR(l902240);
                                                                l902240
                                                                  = next902242;
                                                                goto lp902241;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                        gp_gp_unwind_tail(s901763);
                                    }
                                } else {
                                    gp_gp_unwind_tail(s901763);
                                }
                            }
                        }
                        
                         {
                            void * jmp902226;
                            jmp902226
                              = AREF(_soperations_s,
                                     scm2int(*(inst__pt901760)));
                            
                            
                            
                            ++inst__pt901760;
                            
                            goto *jmp902226;
                        }
                    }
                    
                     {
                        
                        
                      post__q:
                        
                        printf("%s : %d\n", "post-q", 10);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                    cut901765 = AREF(inst__pt901760, 0);
                    
                    inst__pt901760 = inst__pt901760 + 1;
                    
                     {
                        void * jmp901828;
                        jmp901828
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901828;
                    }
                    
                     {
                        
                        
                      unwind__tail:
                        
                        printf("%s : %d\n", "unwind-tail", 5);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM tag901829;
                        tag901829 = *inst__pt901760;
                        
                        
                        
                        inst__pt901760 = inst__pt901760 + 1;
                        
                         {
                            SCM ss902245;
                             {
                                
                              lp902251:
                                 {
                                    
                                     {
                                        SCM x902252;
                                        x902252 = car(ctrl__stack901758);
                                        
                                        
                                        
                                        if (scm_is_eq(car(x902252), tag901829)) {
                                             {
                                                SCM x1902253;
                                                x1902253 = cdr(x902252);
                                                 {
                                                    SCM x2902254;
                                                    x2902254 = cdr(x1902253);
                                                     {
                                                        SCM x3902255;
                                                        x3902255
                                                          = cdr(x2902254);
                                                         {
                                                            
                                                            
                                                            
                                                            p901764
                                                              = car(x1902253);
                                                            
                                                            ctrl__stack901758
                                                              = cdr(ctrl__stack901758);
                                                            
                                                             {
                                                                SCM sx902256;
                                                                sx902256
                                                                  = car(x2902254);
                                                                 {
                                                                    SCM fr902257;
                                                                    fr902257
                                                                      = cdr(sx902256);
                                                                     {
                                                                        SCM ss902258;
                                                                        ss902258
                                                                          = car(sx902256);
                                                                         {
                                                                            
                                                                            
                                                                            
                                                                            if (scm_is_true(x3902255)) {
                                                                                scm_fluid_set_x(_sdelayers_s,
                                                                                                x3902255);
                                                                            }
                                                                            
                                                                            s901763
                                                                              = ss902258;
                                                                            
                                                                            ss902245
                                                                              = fr902257;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                             {
                                                
                                                ctrl__stack901758
                                                  = cdr(ctrl__stack901758);
                                                
                                                goto lp902251;
                                            }
                                        }
                                    }
                                }
                            }
                            
                            
                            
                             {
                                
                                scm_fluid_set_x(_sunwind__hooks_s, SCM_EOL);
                                
                                 {
                                    SCM l902248;
                                    l902248
                                      = SCM_CDR(scm_fluid_ref(_sunwind__parameters_s));
                                     {
                                        
                                      lp902259:
                                         {
                                            
                                            if (SCM_CONSP(l902248)) {
                                                 {
                                                    SCM f902260;
                                                    f902260 = SCM_CAR(l902248);
                                                    
                                                    
                                                    
                                                    scm_call_1(SCM_CAR(f902260),
                                                               SCM_CDR(f902260));
                                                    
                                                     {
                                                        SCM next902261;
                                                        next902261
                                                          = SCM_CDR(l902248);
                                                        l902248 = next902261;
                                                        goto lp902259;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                 {
                                    SCM l902249;
                                    l902249
                                      = scm_reverse(scm_fluid_ref(_sunwind__hooks_s));
                                    
                                    
                                    
                                    if (SCM_CONSP(l902249)) {
                                         {
                                            
                                            gp_gp_unwind(ss902245);
                                            
                                             {
                                                SCM l902262;
                                                l902262 = l902249;
                                                 {
                                                    
                                                  lp902263:
                                                     {
                                                        
                                                        if (SCM_CONSP(l902262)) {
                                                             {
                                                                
                                                                scm_call_3(SCM_CAR(l902262),
                                                                           ss902245,
                                                                           _sfalse_s,
                                                                           _strue_s);
                                                                
                                                                gp_gp_unwind(ss902245);
                                                                
                                                                 {
                                                                    SCM next902264;
                                                                    next902264
                                                                      = SCM_CDR(l902262);
                                                                    l902262
                                                                      = next902264;
                                                                    goto lp902263;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            gp_gp_unwind_tail(ss902245);
                                        }
                                    } else {
                                        gp_gp_unwind_tail(ss902245);
                                    }
                                }
                            }
                            
                             {
                                void * jmp902250;
                                jmp902250
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902250;
                            }
                        }
                    }
                    
                     {
                        
                        
                      unwind__light__tail:
                        
                        printf("%s : %d\n", "unwind-light-tail", 6);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM tag901830;
                        tag901830 = *inst__pt901760;
                        
                        
                        
                        inst__pt901760 = inst__pt901760 + 1;
                        
                         {
                            int v902265;
                            v902265 = scm2int(tag901830);
                            
                            
                            
                            p901764 = AREF(fp, -(nstack901756 + v902265));
                        }
                        
                         {
                            void * jmp902266;
                            jmp902266
                              = AREF(_soperations_s,
                                     scm2int(*(inst__pt901760)));
                            
                            
                            
                            ++inst__pt901760;
                            
                            goto *jmp902266;
                        }
                    }
                    
                     {
                        
                        
                      softie:
                        
                        printf("%s : %d\n", "softie", 7);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM tag901831;
                        tag901831 = *inst__pt901760;
                        
                        
                        
                        inst__pt901760 = inst__pt901760 + 1;
                        
                         {
                            
                          lp902273:
                             {
                                
                                 {
                                    SCM x902274;
                                    x902274 = car(ctrl__stack901758);
                                    
                                    
                                    
                                    scm_simple_format(SCM_BOOL_T,
                                                      scm_string761574,
                                                      scm_list_2(x902274,
                                                                 tag901831));
                                    
                                    if (scm_is_eq(car(x902274), tag901831)) {
                                         {
                                            SCM x1902275;
                                            x1902275 = cdr(x902274);
                                             {
                                                
                                                
                                                
                                                p901764 = car(x1902275);
                                                
                                                ctrl__stack901758
                                                  = cdr(ctrl__stack901758);
                                                
                                                gp_gp_prune_tail(cdr(car(cdr(x1902275))));
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            ctrl__stack901758
                                              = cdr(ctrl__stack901758);
                                            
                                            goto lp902273;
                                        }
                                    }
                                }
                            }
                        }
                        
                         {
                            void * jmp902270;
                            jmp902270
                              = AREF(_soperations_s,
                                     scm2int(*(inst__pt901760)));
                            
                            
                            
                            ++inst__pt901760;
                            
                            goto *jmp902270;
                        }
                    }
                    
                     {
                        
                        
                      softie__light:
                        
                        printf("%s : %d\n", "softie-light", 8);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM tag901832;
                        tag901832 = *inst__pt901760;
                        
                        
                        
                        inst__pt901760 = inst__pt901760 + 1;
                        
                         {
                            int v902276;
                            v902276 = scm2int(tag901832);
                            
                            
                            
                            p901764 = AREF(fp, -(nstack901756 + v902276));
                        }
                        
                         {
                            void * jmp902277;
                            jmp902277
                              = AREF(_soperations_s,
                                     scm2int(*(inst__pt901760)));
                            
                            
                            
                            ++inst__pt901760;
                            
                            goto *jmp902277;
                        }
                    }
                    
                     {
                        
                        
                      unwind:
                        
                        printf("%s : %d\n", "unwind", 3);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM tag901833;
                        tag901833 = AREF(inst__pt901760, 0);
                        
                        
                        
                        p901764 = AREF(inst__pt901760, 1);
                        
                        inst__pt901760 = inst__pt901760 + 2;
                        
                         {
                            int ncons902281;
                             {
                                
                              lp902287:
                                 {
                                    
                                     {
                                        SCM x902288;
                                        x902288 = car(ctrl__stack901758);
                                        
                                        
                                        
                                        if (scm_is_eq(car(x902288), tag901833)) {
                                             {
                                                SCM x1902289;
                                                x1902289 = cdr(x902288);
                                                 {
                                                    SCM x2902290;
                                                    x2902290 = cdr(x1902289);
                                                     {
                                                        SCM x3902291;
                                                        x3902291
                                                          = cdr(x2902290);
                                                         {
                                                            SCM ss902292;
                                                            ss902292
                                                              = cdr(car(x2902290));
                                                             {
                                                                
                                                                
                                                                
                                                                if (scm_is_true(x3902291)) {
                                                                     {
                                                                        
                                                                        scm_fluid_set_x(_sdelayers_s,
                                                                                        x3902291);
                                                                        
                                                                        s901763
                                                                          = ss902292;
                                                                        
                                                                        ncons902281
                                                                          = 5;
                                                                    }
                                                                } else {
                                                                     {
                                                                        
                                                                        s901763
                                                                          = ss902292;
                                                                        
                                                                        ncons902281
                                                                          = 5;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                             {
                                                
                                                ctrl__stack901758
                                                  = lookup(gp_gp_cdr(ctrl__stack901758,
                                                                     s901763),
                                                           s901763);
                                                
                                                goto lp902287;
                                            }
                                        }
                                    }
                                }
                            }
                            
                            
                            
                             {
                                
                                scm_fluid_set_x(_sunwind__hooks_s, SCM_EOL);
                                
                                gp_gp_unwind_ncons(s901763, ncons902281);
                                
                                 {
                                    SCM l902284;
                                    l902284
                                      = SCM_CDR(scm_fluid_ref(_sunwind__parameters_s));
                                     {
                                        
                                      lp902293:
                                         {
                                            
                                            if (SCM_CONSP(l902284)) {
                                                 {
                                                    SCM f902294;
                                                    f902294 = SCM_CAR(l902284);
                                                    
                                                    
                                                    
                                                    scm_call_1(SCM_CAR(f902294),
                                                               SCM_CDR(f902294));
                                                    
                                                     {
                                                        SCM next902295;
                                                        next902295
                                                          = SCM_CDR(l902284);
                                                        l902284 = next902295;
                                                        goto lp902293;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                 {
                                    SCM l902285;
                                    l902285
                                      = scm_reverse(scm_fluid_ref(_sunwind__hooks_s));
                                     {
                                        
                                      lp902296:
                                         {
                                            
                                            if (SCM_CONSP(l902285)) {
                                                 {
                                                    
                                                    scm_call_3(SCM_CAR(l902285),
                                                               s901763,
                                                               _sfalse_s,
                                                               _strue_s);
                                                    
                                                    gp_gp_unwind_ncons(s901763,
                                                                       ncons902281);
                                                    
                                                     {
                                                        SCM next902297;
                                                        next902297
                                                          = SCM_CDR(l902285);
                                                        l902285 = next902297;
                                                        goto lp902296;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                             {
                                void * jmp902286;
                                jmp902286
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902286;
                            }
                        }
                    }
                    
                     {
                        
                        
                      unwind__light:
                        
                        printf("%s : %d\n", "unwind-light", 4);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                    p901764 = AREF(inst__pt901760, 1);
                    
                    inst__pt901760 = inst__pt901760 + 1;
                    
                     {
                        void * jmp901834;
                        jmp901834
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901834;
                    }
                    
                     {
                        
                        
                      unwind__negation:
                        
                        printf("%s : %d\n", "unwind-negation", 12);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM n901835;
                        n901835 = *inst__pt901760;
                        
                        
                        
                        cut901765 = AREF(inst__pt901760, 1);
                        
                         {
                            
                          lp902301:
                             {
                                
                                 {
                                    SCM x902302;
                                    x902302 = car(ctrl__stack901758);
                                    
                                    
                                    
                                    if (scm_is_eq(car(x902302), n901835)) {
                                         {
                                            SCM x1902303;
                                            x1902303 = cdr(x902302);
                                             {
                                                SCM x2902304;
                                                x2902304 = cdr(x1902303);
                                                 {
                                                    SCM x4902305;
                                                    x4902305 = cdr(x2902304);
                                                     {
                                                        SCM sc902306;
                                                        sc902306
                                                          = cdr(x4902305);
                                                         {
                                                            
                                                            
                                                            
                                                            p901764
                                                              = car(x1902303);
                                                            
                                                            scut901766
                                                              = sc902306;
                                                            
                                                            ctrl__stack901758
                                                              = cdr(ctrl__stack901758);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            ctrl__stack901758
                                              = cdr(ctrl__stack901758);
                                            
                                            goto lp902301;
                                        }
                                    }
                                }
                            }
                        }
                        
                        gp_fluid_force_bang(_sgp__is__delayed_p_s,
                                            SCM_BOOL_F,
                                            s901763);
                        
                        if (SCM_I_INUMP(p901764)) {
                             {
                                
                                inst__pt901760
                                  = instructions901752 + scm2int(p901764);
                                
                                 {
                                    void * jmp902300;
                                    jmp902300
                                      = AREF(_soperations_s,
                                             scm2int(*(inst__pt901760)));
                                    
                                    
                                    
                                    ++inst__pt901760;
                                    
                                    goto *jmp902300;
                                }
                            }
                        } else {
                             {
                                
                                AREF(fp, 0) = p901764;
                                
                                sp = fp + -1;
                                
                                goto ret;
                            }
                        }
                    }
                    
                     {
                        
                        
                      false:
                        
                        printf("%s : %d\n", "false", 34);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                    if (SCM_I_INUMP(p901764)) {
                         {
                            
                            inst__pt901760
                              = instructions901752 + scm2int(p901764);
                            
                             {
                                void * jmp901836;
                                jmp901836
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp901836;
                            }
                        }
                    } else {
                         {
                            
                            AREF(fp, 0) = p901764;
                            
                            sp = fp + -1;
                            
                            goto ret;
                        }
                    }
                    
                     {
                        
                        
                      goto__inst:
                        
                        printf("%s : %d\n", "goto-inst", 41);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                    INTERUPT();
                    
                     {
                        
                        iter901767 = (iter901767 + 1) % 20;
                        
                        if (iter901767 == 0) {
                            gp_gc();
                        }
                    }
                    
                     {
                        int ni901837;
                        ni901837 = scm2int(*(inst__pt901760));
                        
                        
                        
                        inst__pt901760 = instructions901752 + ni901837;
                        
                         {
                            void * jmp902317;
                            jmp902317
                              = AREF(_soperations_s,
                                     scm2int(*(inst__pt901760)));
                            
                            
                            
                            ++inst__pt901760;
                            
                            goto *jmp902317;
                        }
                    }
                    
                     {
                        
                        
                      post__call:
                        
                        printf("%s : %d\n", "post-call", 43);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM c901838;
                        SCM pop_p901839;
                        c901838 = AREF(inst__pt901760, 0);
                        pop_p901839 = AREF(inst__pt901760, 1);
                        
                        
                        
                        inst__pt901760 = inst__pt901760 + 2;
                        
                        if (call_p901740) {
                             {
                                
                                call_p901740 = 0;
                                
                                if (scm_is_true(pop_p901839)) {
                                    sp = sp + 3;
                                }
                                
                                cut901765 = c901838;
                                
                                if (SCM_CONSP(sp__stack901759)) {
                                     {
                                        SCM st902320;
                                        st902320 = sp__stack901759;
                                        
                                        
                                        
                                         {
                                            SCM * vp902326;
                                            vp902326 = fp + -nstack901756;
                                            
                                            
                                            
                                             {
                                                int i902330;
                                                i902330 = 0;
                                                 {
                                                    
                                                  lp902335:
                                                     {
                                                        
                                                        if (i902330 > 0) {
                                                            if (SCM_CONSP(st902320)) {
                                                                 {
                                                                    
                                                                    AREF(vp902326,
                                                                         0)
                                                                      = SCM_CAR(st902320);
                                                                    
                                                                    st902320
                                                                      = SCM_CDR(st902320);
                                                                    
                                                                    vp902326
                                                                      = vp902326
                                                                          - 1;
                                                                    
                                                                     {
                                                                        int next902338;
                                                                        next902338
                                                                          = i902330
                                                                              - 1;
                                                                        i902330
                                                                          = next902338;
                                                                        goto lp902335;
                                                                    }
                                                                }
                                                            } else {
                                                                scm_misc_error("prolog-vm",
                                                                               "Stack link on length missmatch",
                                                                               SCM_EOL);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                         {
                                            int n902329;
                                             {
                                                SCM s902327;
                                                s902327 = st902320;
                                                 {
                                                    int i902328;
                                                    i902328 = 0;
                                                     {
                                                        
                                                      lp902343:
                                                         {
                                                            
                                                             {
                                                                SCM next902344;
                                                                int next902345;
                                                                next902344
                                                                  = SCM_CDR(s902327);
                                                                next902345
                                                                  = i902328
                                                                      + 1;
                                                                s902327
                                                                  = next902344;
                                                                i902328
                                                                  = next902345;
                                                                goto lp902343;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            
                                            
                                             {
                                                int n902341;
                                                if (fp > sp) {
                                                    n902341 = fp - sp;
                                                } else {
                                                     {
                                                        
                                                        scm_misc_error("prolog-vm",
                                                                       "negative stack pointer",
                                                                       SCM_EOL);
                                                        
                                                        n902341 = 1;
                                                    }
                                                }
                                                
                                                
                                                
                                                if (n902341) {
                                                     {
                                                        int i902348;
                                                        i902348
                                                          = nstack901756
                                                              + n902341;
                                                         {
                                                            
                                                          lp902351:
                                                             {
                                                                
                                                                if (!(fp == sp)) {
                                                                     {
                                                                        
                                                                        AREF(fp,
                                                                             -(i902348))
                                                                          = AREF(sp,
                                                                                 1);
                                                                        
                                                                        --sp;
                                                                        
                                                                         {
                                                                            int next902357;
                                                                            next902357
                                                                              = i902348
                                                                                  - 1;
                                                                            i902348
                                                                              = next902357;
                                                                            goto lp902351;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                             {
                                                SCM * vp902342;
                                                vp902342 = fp + -0;
                                                
                                                
                                                
                                                 {
                                                    int i902360;
                                                    i902360 = n902329;
                                                     {
                                                        
                                                      lp902365:
                                                         {
                                                            
                                                            if (i902360 > 0) {
                                                                if (SCM_CONSP(st902320)) {
                                                                     {
                                                                        
                                                                        AREF(vp902342,
                                                                             0)
                                                                          = SCM_CAR(st902320);
                                                                        
                                                                        st902320
                                                                          = SCM_CDR(st902320);
                                                                        
                                                                        vp902342
                                                                          = vp902342
                                                                              - 1;
                                                                        
                                                                         {
                                                                            int next902368;
                                                                            next902368
                                                                              = i902360
                                                                                  - 1;
                                                                            i902360
                                                                              = next902368;
                                                                            goto lp902365;
                                                                        }
                                                                    }
                                                                } else {
                                                                    scm_misc_error("prolog-vm",
                                                                                   "Stack link on length missmatch",
                                                                                   SCM_EOL);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            sp = sp + nstack901756;
                                        }
                                    }
                                }
                                
                                 {
                                    void * jmp902321;
                                    jmp902321
                                      = AREF(_soperations_s,
                                             scm2int(*(inst__pt901760)));
                                    
                                    
                                    
                                    ++inst__pt901760;
                                    
                                    goto *jmp902321;
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      post__unicall:
                        
                        printf("%s : %d\n", "post-unicall", 44);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM c901840;
                        int nsloc901841;
                        c901840 = AREF(inst__pt901760, 0);
                        nsloc901841 = scm2int(AREF(inst__pt901760, 1));
                        
                        
                        
                        inst__pt901760 = inst__pt901760 + 2;
                        
                        if (call_p901740) {
                             {
                                
                                call_p901740 = 0;
                                
                                sp = sp + 3;
                                
                                cut901765 = c901840;
                                
                                if (SCM_CONSP(sp__stack901759)) {
                                     {
                                        SCM st902373;
                                        st902373 = sp__stack901759;
                                        
                                        
                                        
                                         {
                                            SCM * vp902379;
                                            vp902379 = fp + -nstack901756;
                                            
                                            
                                            
                                             {
                                                int i902383;
                                                i902383 = nsloc901841;
                                                 {
                                                    
                                                  lp902388:
                                                     {
                                                        
                                                        if (i902383 > 0) {
                                                            if (SCM_CONSP(st902373)) {
                                                                 {
                                                                    
                                                                    AREF(vp902379,
                                                                         0)
                                                                      = SCM_CAR(st902373);
                                                                    
                                                                    st902373
                                                                      = SCM_CDR(st902373);
                                                                    
                                                                    vp902379
                                                                      = vp902379
                                                                          - 1;
                                                                    
                                                                     {
                                                                        int next902391;
                                                                        next902391
                                                                          = i902383
                                                                              - 1;
                                                                        i902383
                                                                          = next902391;
                                                                        goto lp902388;
                                                                    }
                                                                }
                                                            } else {
                                                                scm_misc_error("prolog-vm",
                                                                               "Stack link on length missmatch",
                                                                               SCM_EOL);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                         {
                                            int n902382;
                                             {
                                                SCM s902380;
                                                s902380 = st902373;
                                                 {
                                                    int i902381;
                                                    i902381 = 0;
                                                     {
                                                        
                                                      lp902396:
                                                         {
                                                            
                                                             {
                                                                SCM next902397;
                                                                int next902398;
                                                                next902397
                                                                  = SCM_CDR(s902380);
                                                                next902398
                                                                  = i902381
                                                                      + 1;
                                                                s902380
                                                                  = next902397;
                                                                i902381
                                                                  = next902398;
                                                                goto lp902396;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            
                                            
                                             {
                                                int n902394;
                                                if (fp > sp) {
                                                    n902394 = fp - sp;
                                                } else {
                                                     {
                                                        
                                                        scm_misc_error("prolog-vm",
                                                                       "negative stack pointer",
                                                                       SCM_EOL);
                                                        
                                                        n902394 = 1;
                                                    }
                                                }
                                                
                                                
                                                
                                                if (n902394) {
                                                     {
                                                        int i902401;
                                                        i902401
                                                          = nstack901756
                                                              + n902394;
                                                         {
                                                            
                                                          lp902404:
                                                             {
                                                                
                                                                if (!(fp == sp)) {
                                                                     {
                                                                        
                                                                        AREF(fp,
                                                                             -(i902401))
                                                                          = AREF(sp,
                                                                                 1);
                                                                        
                                                                        --sp;
                                                                        
                                                                         {
                                                                            int next902410;
                                                                            next902410
                                                                              = i902401
                                                                                  - 1;
                                                                            i902401
                                                                              = next902410;
                                                                            goto lp902404;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                             {
                                                SCM * vp902395;
                                                vp902395 = fp + -0;
                                                
                                                
                                                
                                                 {
                                                    int i902413;
                                                    i902413 = n902382;
                                                     {
                                                        
                                                      lp902418:
                                                         {
                                                            
                                                            if (i902413 > 0) {
                                                                if (SCM_CONSP(st902373)) {
                                                                     {
                                                                        
                                                                        AREF(vp902395,
                                                                             0)
                                                                          = SCM_CAR(st902373);
                                                                        
                                                                        st902373
                                                                          = SCM_CDR(st902373);
                                                                        
                                                                        vp902395
                                                                          = vp902395
                                                                              - 1;
                                                                        
                                                                         {
                                                                            int next902421;
                                                                            next902421
                                                                              = i902413
                                                                                  - 1;
                                                                            i902413
                                                                              = next902421;
                                                                            goto lp902418;
                                                                        }
                                                                    }
                                                                } else {
                                                                    scm_misc_error("prolog-vm",
                                                                                   "Stack link on length missmatch",
                                                                                   SCM_EOL);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            sp = sp + nstack901756;
                                        }
                                    }
                                }
                            }
                        }
                        
                         {
                            void * jmp902374;
                            jmp902374
                              = AREF(_soperations_s,
                                     scm2int(*(inst__pt901760)));
                            
                            
                            
                            ++inst__pt901760;
                            
                            goto *jmp902374;
                        }
                    }
                    
                     {
                        
                        
                      fail:
                        
                        printf("%s : %d\n", "fail", 35);
                    }
                    
                    if (scm_is_eq(p901764, scm_num721462)) {
                        p901764 = AREF(variables901748, 1);
                    }
                    
                    if (SCM_I_INUMP(p901764)) {
                         {
                            
                            inst__pt901760
                              = instructions901752 + scm2int(p901764);
                            
                             {
                                void * jmp901842;
                                jmp901842
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp901842;
                            }
                        }
                    } else {
                         {
                            
                            AREF(fp, 0) = p901764;
                            
                            sp = fp + -1;
                            
                            goto ret;
                        }
                    }
                    
                     {
                        
                        
                      cut:
                        
                        printf("%s : %d\n", "cut", 42);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                    if (scm_is_eq(cut901765, scm_num721462)) {
                        p901764 = AREF(variables901748, 2);
                    } else {
                        p901764 = cut901765;
                    }
                    
                    gp_gp_prune(scut901766);
                    
                     {
                        void * jmp901843;
                        jmp901843
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901843;
                    }
                    
                     {
                        
                        
                      set:
                        
                        printf("%s : %d\n", "set", 14);
                    }
                    
                     {
                        SCM n901844;
                        n901844 = AREF(inst__pt901760, 0);
                         {
                            SCM ss901847;
                             {
                                SCM a902431;
                                SCM a902432;
                                SCM a902433;
                                if (SCM_CONSP(n901844)) {
                                     {
                                        int i901845;
                                        i901845 = scm2int(SCM_CAR(n901844));
                                        
                                        
                                        
                                        a902431
                                          = AREF(fp, -(nstack901756 + i901845));
                                    }
                                } else {
                                     {
                                        int i901846;
                                        i901846 = scm2int(n901844);
                                        
                                        
                                        
                                        if (0) {
                                            if (scm_is_true(pinned_p901739)) {
                                                 {
                                                    
                                                    variables__scm901747
                                                      = gp_copy_vector(&(variables901748),
                                                                       nvar901757);
                                                    
                                                    session901745
                                                      = scm_cons(variables__scm901747,
                                                                 cnst901746);
                                                    
                                                    middle901744
                                                      = scm_cons(SCM_EOL,
                                                                 session901745);
                                                    
                                                    pinned_p901739
                                                      = SCM_BOOL_F;
                                                }
                                            }
                                        }
                                        
                                        a902431
                                          = AREF(variables901748, i901846);
                                    }
                                }
                                a902432 = AREF(sp, 1);
                                a902433 = s901763;
                                ss901847 = gp_set(a902431, a902432, a902433);
                            }
                             {
                                
                                
                                
                                inst__pt901760 = inst__pt901760 + 1;
                                
                                sp = sp + 1;
                                
                                s901763 = ss901847;
                                
                                 {
                                    void * jmp902430;
                                    jmp902430
                                      = AREF(_soperations_s,
                                             scm2int(*(inst__pt901760)));
                                    
                                    
                                    
                                    ++inst__pt901760;
                                    
                                    goto *jmp902430;
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          sp__move:
                            
                            printf("%s : %d\n", "sp-move", 25);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            ulong v901848;
                            v901848 = scm2ulong(AREF(inst__pt901760, 0));
                             {
                                
                                
                                
                                inst__pt901760 = inst__pt901760 + 1;
                                
                                if (0) {
                                    AREF(fp, -(nstack901756 + v901848))
                                      = AREF(sp, 1);
                                } else {
                                     {
                                        
                                        if (1) {
                                            if (scm_is_true(pinned_p901739)) {
                                                 {
                                                    
                                                    variables__scm901747
                                                      = gp_copy_vector(&(variables901748),
                                                                       nvar901757);
                                                    
                                                    session901745
                                                      = scm_cons(variables__scm901747,
                                                                 cnst901746);
                                                    
                                                    middle901744
                                                      = scm_cons(SCM_EOL,
                                                                 session901745);
                                                    
                                                    pinned_p901739
                                                      = SCM_BOOL_F;
                                                }
                                            }
                                        }
                                        
                                        AREF(variables901748, v901848)
                                          = AREF(sp, 1);
                                    }
                                }
                                
                                 {
                                    
                                    AREF(sp, 1) = SCM_BOOL_F;
                                    
                                    sp = sp + 1;
                                }
                                
                                 {
                                    void * jmp902439;
                                    jmp902439
                                      = AREF(_soperations_s,
                                             scm2int(*(inst__pt901760)));
                                    
                                    
                                    
                                    ++inst__pt901760;
                                    
                                    goto *jmp902439;
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          sp__move__s:
                            
                            printf("%s : %d\n", "sp-move-s", 26);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            ulong v901849;
                            v901849 = scm2ulong(AREF(inst__pt901760, 0));
                             {
                                
                                
                                
                                inst__pt901760 = inst__pt901760 + 1;
                                
                                if (1) {
                                    AREF(fp, -(nstack901756 + v901849))
                                      = AREF(sp, 1);
                                } else {
                                     {
                                        
                                        if (1) {
                                            if (scm_is_true(pinned_p901739)) {
                                                 {
                                                    
                                                    variables__scm901747
                                                      = gp_copy_vector(&(variables901748),
                                                                       nvar901757);
                                                    
                                                    session901745
                                                      = scm_cons(variables__scm901747,
                                                                 cnst901746);
                                                    
                                                    middle901744
                                                      = scm_cons(SCM_EOL,
                                                                 session901745);
                                                    
                                                    pinned_p901739
                                                      = SCM_BOOL_F;
                                                }
                                            }
                                        }
                                        
                                        AREF(variables901748, v901849)
                                          = AREF(sp, 1);
                                    }
                                }
                                
                                 {
                                    
                                    AREF(sp, 1) = SCM_BOOL_F;
                                    
                                    sp = sp + 1;
                                }
                                
                                 {
                                    void * jmp902445;
                                    jmp902445
                                      = AREF(_soperations_s,
                                             scm2int(*(inst__pt901760)));
                                    
                                    
                                    
                                    ++inst__pt901760;
                                    
                                    goto *jmp902445;
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          unify:
                            
                            printf("%s : %d\n", "unify", 24);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            ulong code901850;
                            code901850 = scm2ulong(AREF(inst__pt901760, 0));
                             {
                                int m901851;
                                m901851 = code901850 & 3;
                                 {
                                    int a901852;
                                    a901852 = (code901850 & 4) >> 2;
                                     {
                                        int k901853;
                                        k901853 = (code901850 & 24) >> 3;
                                         {
                                            int v901854;
                                            v901854 = code901850 >> 5;
                                             {
                                                
                                                
                                                
                                                inst__pt901760
                                                  = inst__pt901760 + 1;
                                                
                                                 {
                                                    SCM x902451;
                                                    if (a901852) {
                                                        x902451
                                                          = AREF(fp,
                                                                 -(nstack901756
                                                                     + v901854));
                                                    } else {
                                                         {
                                                            
                                                            if (0) {
                                                                if (scm_is_true(pinned_p901739)) {
                                                                     {
                                                                        
                                                                        variables__scm901747
                                                                          = gp_copy_vector(&(variables901748),
                                                                                           nvar901757);
                                                                        
                                                                        session901745
                                                                          = scm_cons(variables__scm901747,
                                                                                     cnst901746);
                                                                        
                                                                        middle901744
                                                                          = scm_cons(SCM_EOL,
                                                                                     session901745);
                                                                        
                                                                        pinned_p901739
                                                                          = SCM_BOOL_F;
                                                                    }
                                                                }
                                                            }
                                                            
                                                            x902451
                                                              = AREF(variables901748,
                                                                     v901854);
                                                        }
                                                    }
                                                     {
                                                        SCM y902452;
                                                        y902452 = AREF(sp, 1);
                                                         {
                                                            SCM ss902453;
                                                            if (m901851 == 2) {
                                                                ss902453
                                                                  = gp_m_unify(x902451,
                                                                               y902452,
                                                                               s901763);
                                                            } else {
                                                                if (m901851
                                                                      == 3
                                                                      || k901853
                                                                           == 3) {
                                                                    ss902453
                                                                      = gp_gp_unify_raw(x902451,
                                                                                        y902452,
                                                                                        s901763);
                                                                } else {
                                                                    ss902453
                                                                      = gp_gp_unify(x902451,
                                                                                    y902452,
                                                                                    s901763);
                                                                }
                                                            }
                                                             {
                                                                
                                                                
                                                                
                                                                 {
                                                                    
                                                                    AREF(sp, 1)
                                                                      = SCM_BOOL_F;
                                                                    
                                                                    sp
                                                                      = sp + 1;
                                                                }
                                                                
                                                                if (scm_is_true(ss902453)) {
                                                                     {
                                                                        
                                                                        s901763
                                                                          = ss902453;
                                                                        
                                                                         {
                                                                            void * jmp902464;
                                                                            jmp902464
                                                                              = AREF(_soperations_s,
                                                                                     scm2int(*(inst__pt901760)));
                                                                            
                                                                            
                                                                            
                                                                            ++inst__pt901760;
                                                                            
                                                                            goto *jmp902464;
                                                                        }
                                                                    }
                                                                } else {
                                                                    if (SCM_I_INUMP(p901764)) {
                                                                         {
                                                                            
                                                                            inst__pt901760
                                                                              = instructions901752
                                                                                  + scm2int(p901764);
                                                                            
                                                                             {
                                                                                void * jmp902465;
                                                                                jmp902465
                                                                                  = AREF(_soperations_s,
                                                                                         scm2int(*(inst__pt901760)));
                                                                                
                                                                                
                                                                                
                                                                                ++inst__pt901760;
                                                                                
                                                                                goto *jmp902465;
                                                                            }
                                                                        }
                                                                    } else {
                                                                         {
                                                                            
                                                                            AREF(fp,
                                                                                 0)
                                                                              = p901764;
                                                                            
                                                                            sp
                                                                              = fp
                                                                                  + -1;
                                                                            
                                                                            goto ret;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          unify__2:
                            
                            printf("%s : %d\n", "unify-2", 23);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            ulong code1901855;
                            code1901855 = scm2ulong(AREF(inst__pt901760, 0));
                             {
                                int m901856;
                                m901856 = code1901855 & 3;
                                 {
                                    ulong code2901857;
                                    code2901857 = code1901855 >> 2;
                                     {
                                        int a1901858;
                                        a1901858 = code2901857 & 1;
                                         {
                                            int k1901859;
                                            k1901859 = (code2901857 & 6) >> 1;
                                             {
                                                int v1901860;
                                                v1901860
                                                  = (code2901857 & 16777208)
                                                      >> 3;
                                                 {
                                                    ulong code3901861;
                                                    code3901861
                                                      = code2901857 >> 24;
                                                     {
                                                        int a2901862;
                                                        a2901862
                                                          = code3901861 & 1;
                                                         {
                                                            int k2901863;
                                                            k2901863
                                                              = (code3901861
                                                                   & 6)
                                                                  >> 1;
                                                             {
                                                                int v2901864;
                                                                v2901864
                                                                  = (code3901861
                                                                       & 16777208)
                                                                      >> 3;
                                                                 {
                                                                    
                                                                    
                                                                    
                                                                    inst__pt901760
                                                                      = inst__pt901760
                                                                          + 1;
                                                                    
                                                                    if (k1901859
                                                                          == 2) {
                                                                         {
                                                                            
                                                                            if (k2901863
                                                                                  == 2) {
                                                                                if (a2901862) {
                                                                                    AREF(fp,
                                                                                         -(nstack901756
                                                                                             + v2901864))
                                                                                      = gp_mkvar(s901763);
                                                                                } else {
                                                                                     {
                                                                                        
                                                                                        if (1) {
                                                                                            if (scm_is_true(pinned_p901739)) {
                                                                                                 {
                                                                                                    
                                                                                                    variables__scm901747
                                                                                                      = gp_copy_vector(&(variables901748),
                                                                                                                       nvar901757);
                                                                                                    
                                                                                                    session901745
                                                                                                      = scm_cons(variables__scm901747,
                                                                                                                 cnst901746);
                                                                                                    
                                                                                                    middle901744
                                                                                                      = scm_cons(SCM_EOL,
                                                                                                                 session901745);
                                                                                                    
                                                                                                    pinned_p901739
                                                                                                      = SCM_BOOL_F;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        
                                                                                        AREF(variables901748,
                                                                                             v2901864)
                                                                                          = gp_mkvar(s901763);
                                                                                    }
                                                                                }
                                                                            }
                                                                            
                                                                             {
                                                                                SCM rhs902473;
                                                                                if (a2901862) {
                                                                                    rhs902473
                                                                                      = AREF(fp,
                                                                                             -(nstack901756
                                                                                                 + v2901864));
                                                                                } else {
                                                                                     {
                                                                                        
                                                                                        if (0) {
                                                                                            if (scm_is_true(pinned_p901739)) {
                                                                                                 {
                                                                                                    
                                                                                                    variables__scm901747
                                                                                                      = gp_copy_vector(&(variables901748),
                                                                                                                       nvar901757);
                                                                                                    
                                                                                                    session901745
                                                                                                      = scm_cons(variables__scm901747,
                                                                                                                 cnst901746);
                                                                                                    
                                                                                                    middle901744
                                                                                                      = scm_cons(SCM_EOL,
                                                                                                                 session901745);
                                                                                                    
                                                                                                    pinned_p901739
                                                                                                      = SCM_BOOL_F;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        
                                                                                        rhs902473
                                                                                          = AREF(variables901748,
                                                                                                 v2901864);
                                                                                    }
                                                                                }
                                                                                
                                                                                
                                                                                
                                                                                if (a1901858) {
                                                                                    AREF(fp,
                                                                                         -(nstack901756
                                                                                             + v1901860))
                                                                                      = rhs902473;
                                                                                } else {
                                                                                     {
                                                                                        
                                                                                        if (1) {
                                                                                            if (scm_is_true(pinned_p901739)) {
                                                                                                 {
                                                                                                    
                                                                                                    variables__scm901747
                                                                                                      = gp_copy_vector(&(variables901748),
                                                                                                                       nvar901757);
                                                                                                    
                                                                                                    session901745
                                                                                                      = scm_cons(variables__scm901747,
                                                                                                                 cnst901746);
                                                                                                    
                                                                                                    middle901744
                                                                                                      = scm_cons(SCM_EOL,
                                                                                                                 session901745);
                                                                                                    
                                                                                                    pinned_p901739
                                                                                                      = SCM_BOOL_F;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        
                                                                                        AREF(variables901748,
                                                                                             v1901860)
                                                                                          = rhs902473;
                                                                                    }
                                                                                }
                                                                            }
                                                                            
                                                                             {
                                                                                void * jmp902474;
                                                                                jmp902474
                                                                                  = AREF(_soperations_s,
                                                                                         scm2int(*(inst__pt901760)));
                                                                                
                                                                                
                                                                                
                                                                                ++inst__pt901760;
                                                                                
                                                                                goto *jmp902474;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        if (k2901863
                                                                              == 2) {
                                                                             {
                                                                                SCM rhs902475;
                                                                                if (a2901862) {
                                                                                    rhs902475
                                                                                      = AREF(fp,
                                                                                             -(nstack901756
                                                                                                 + v2901864));
                                                                                } else {
                                                                                     {
                                                                                        
                                                                                        if (0) {
                                                                                            if (scm_is_true(pinned_p901739)) {
                                                                                                 {
                                                                                                    
                                                                                                    variables__scm901747
                                                                                                      = gp_copy_vector(&(variables901748),
                                                                                                                       nvar901757);
                                                                                                    
                                                                                                    session901745
                                                                                                      = scm_cons(variables__scm901747,
                                                                                                                 cnst901746);
                                                                                                    
                                                                                                    middle901744
                                                                                                      = scm_cons(SCM_EOL,
                                                                                                                 session901745);
                                                                                                    
                                                                                                    pinned_p901739
                                                                                                      = SCM_BOOL_F;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        
                                                                                        rhs902475
                                                                                          = AREF(variables901748,
                                                                                                 v2901864);
                                                                                    }
                                                                                }
                                                                                
                                                                                
                                                                                
                                                                                if (a1901858) {
                                                                                    AREF(fp,
                                                                                         -(nstack901756
                                                                                             + v1901860))
                                                                                      = rhs902475;
                                                                                } else {
                                                                                     {
                                                                                        
                                                                                        if (1) {
                                                                                            if (scm_is_true(pinned_p901739)) {
                                                                                                 {
                                                                                                    
                                                                                                    variables__scm901747
                                                                                                      = gp_copy_vector(&(variables901748),
                                                                                                                       nvar901757);
                                                                                                    
                                                                                                    session901745
                                                                                                      = scm_cons(variables__scm901747,
                                                                                                                 cnst901746);
                                                                                                    
                                                                                                    middle901744
                                                                                                      = scm_cons(SCM_EOL,
                                                                                                                 session901745);
                                                                                                    
                                                                                                    pinned_p901739
                                                                                                      = SCM_BOOL_F;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        
                                                                                        AREF(variables901748,
                                                                                             v1901860)
                                                                                          = rhs902475;
                                                                                    }
                                                                                }
                                                                                
                                                                                 {
                                                                                    void * jmp902502;
                                                                                    jmp902502
                                                                                      = AREF(_soperations_s,
                                                                                             scm2int(*(inst__pt901760)));
                                                                                    
                                                                                    
                                                                                    
                                                                                    ++inst__pt901760;
                                                                                    
                                                                                    goto *jmp902502;
                                                                                }
                                                                            }
                                                                        } else {
                                                                             {
                                                                                SCM x902476;
                                                                                if (a1901858) {
                                                                                    x902476
                                                                                      = AREF(fp,
                                                                                             -(nstack901756
                                                                                                 + v1901860));
                                                                                } else {
                                                                                     {
                                                                                        
                                                                                        if (0) {
                                                                                            if (scm_is_true(pinned_p901739)) {
                                                                                                 {
                                                                                                    
                                                                                                    variables__scm901747
                                                                                                      = gp_copy_vector(&(variables901748),
                                                                                                                       nvar901757);
                                                                                                    
                                                                                                    session901745
                                                                                                      = scm_cons(variables__scm901747,
                                                                                                                 cnst901746);
                                                                                                    
                                                                                                    middle901744
                                                                                                      = scm_cons(SCM_EOL,
                                                                                                                 session901745);
                                                                                                    
                                                                                                    pinned_p901739
                                                                                                      = SCM_BOOL_F;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        
                                                                                        x902476
                                                                                          = AREF(variables901748,
                                                                                                 v1901860);
                                                                                    }
                                                                                }
                                                                                 {
                                                                                    SCM y902477;
                                                                                    if (a2901862) {
                                                                                        y902477
                                                                                          = AREF(fp,
                                                                                                 -(nstack901756
                                                                                                     + v2901864));
                                                                                    } else {
                                                                                         {
                                                                                            
                                                                                            if (0) {
                                                                                                if (scm_is_true(pinned_p901739)) {
                                                                                                     {
                                                                                                        
                                                                                                        variables__scm901747
                                                                                                          = gp_copy_vector(&(variables901748),
                                                                                                                           nvar901757);
                                                                                                        
                                                                                                        session901745
                                                                                                          = scm_cons(variables__scm901747,
                                                                                                                     cnst901746);
                                                                                                        
                                                                                                        middle901744
                                                                                                          = scm_cons(SCM_EOL,
                                                                                                                     session901745);
                                                                                                        
                                                                                                        pinned_p901739
                                                                                                          = SCM_BOOL_F;
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            
                                                                                            y902477
                                                                                              = AREF(variables901748,
                                                                                                     v2901864);
                                                                                        }
                                                                                    }
                                                                                     {
                                                                                        SCM ss902478;
                                                                                         {
                                                                                            
                                                                                            scm_simple_format(SCM_BOOL_T,
                                                                                                              scm_string780398,
                                                                                                              scm_list_2(x902476,
                                                                                                                         y902477));
                                                                                            
                                                                                            if (m901856
                                                                                                  == 2) {
                                                                                                ss902478
                                                                                                  = gp_m_unify(x902476,
                                                                                                               y902477,
                                                                                                               s901763);
                                                                                            } else {
                                                                                                if (k1901859
                                                                                                      == 3
                                                                                                      || k2901863
                                                                                                           == 3) {
                                                                                                    ss902478
                                                                                                      = gp_gp_unify_raw(x902476,
                                                                                                                        y902477,
                                                                                                                        s901763);
                                                                                                } else {
                                                                                                    ss902478
                                                                                                      = gp_gp_unify(x902476,
                                                                                                                    y902477,
                                                                                                                    s901763);
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                         {
                                                                                            
                                                                                            
                                                                                            
                                                                                            if (scm_is_true(ss902478)) {
                                                                                                 {
                                                                                                    
                                                                                                    s901763
                                                                                                      = ss902478;
                                                                                                    
                                                                                                     {
                                                                                                        void * jmp902505;
                                                                                                        jmp902505
                                                                                                          = AREF(_soperations_s,
                                                                                                                 scm2int(*(inst__pt901760)));
                                                                                                        
                                                                                                        
                                                                                                        
                                                                                                        ++inst__pt901760;
                                                                                                        
                                                                                                        goto *jmp902505;
                                                                                                    }
                                                                                                }
                                                                                            } else {
                                                                                                if (SCM_I_INUMP(p901764)) {
                                                                                                     {
                                                                                                        
                                                                                                        inst__pt901760
                                                                                                          = instructions901752
                                                                                                              + scm2int(p901764);
                                                                                                        
                                                                                                         {
                                                                                                            void * jmp902506;
                                                                                                            jmp902506
                                                                                                              = AREF(_soperations_s,
                                                                                                                     scm2int(*(inst__pt901760)));
                                                                                                            
                                                                                                            
                                                                                                            
                                                                                                            ++inst__pt901760;
                                                                                                            
                                                                                                            goto *jmp902506;
                                                                                                        }
                                                                                                    }
                                                                                                } else {
                                                                                                     {
                                                                                                        
                                                                                                        AREF(fp,
                                                                                                             0)
                                                                                                          = p901764;
                                                                                                        
                                                                                                        sp
                                                                                                          = fp
                                                                                                              + -1;
                                                                                                        
                                                                                                        goto ret;
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          unify__instruction__2:
                            
                            printf("%s : %d\n", "unify-instruction-2", 19);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM n901865;
                            n901865 = AREF(inst__pt901760, 2);
                             {
                                SCM m901866;
                                m901866 = AREF(inst__pt901760, 0);
                                 {
                                    SCM i901867;
                                    i901867 = AREF(inst__pt901760, 1);
                                     {
                                        SCM k901868;
                                        k901868 = AREF(inst__pt901760, 3);
                                         {
                                            
                                            
                                            
                                            inst__pt901760
                                              = inst__pt901760 + 4;
                                            
                                            if (k901868 == SCM_BOOL_F) {
                                                 {
                                                    
                                                    if (SCM_CONSP(n901865)) {
                                                         {
                                                            int ii902513;
                                                            ii902513
                                                              = scm2int(SCM_CAR(n901865));
                                                            
                                                            
                                                            
                                                            AREF(fp,
                                                                 -(nstack901756
                                                                     + ii902513))
                                                              = i901867;
                                                        }
                                                    } else {
                                                         {
                                                            int ii902514;
                                                            ii902514
                                                              = scm2int(n901865);
                                                            
                                                            
                                                            
                                                            if (1) {
                                                                if (scm_is_true(pinned_p901739)) {
                                                                     {
                                                                        
                                                                        variables__scm901747
                                                                          = gp_copy_vector(&(variables901748),
                                                                                           nvar901757);
                                                                        
                                                                        session901745
                                                                          = scm_cons(variables__scm901747,
                                                                                     cnst901746);
                                                                        
                                                                        middle901744
                                                                          = scm_cons(SCM_EOL,
                                                                                     session901745);
                                                                        
                                                                        pinned_p901739
                                                                          = SCM_BOOL_F;
                                                                    }
                                                                }
                                                            }
                                                            
                                                            AREF(variables901748,
                                                                 ii902514)
                                                              = i901867;
                                                        }
                                                    }
                                                    
                                                     {
                                                        void * jmp902515;
                                                        jmp902515
                                                          = AREF(_soperations_s,
                                                                 scm2int(*(inst__pt901760)));
                                                        
                                                        
                                                        
                                                        ++inst__pt901760;
                                                        
                                                        goto *jmp902515;
                                                    }
                                                }
                                            } else {
                                                 {
                                                    SCM x902518;
                                                    if (SCM_CONSP(n901865)) {
                                                         {
                                                            int ii902516;
                                                            ii902516
                                                              = scm2int(SCM_CAR(n901865));
                                                            
                                                            
                                                            
                                                            x902518
                                                              = AREF(fp,
                                                                     -(nstack901756
                                                                         + ii902516));
                                                        }
                                                    } else {
                                                         {
                                                            int ii902517;
                                                            ii902517
                                                              = scm2int(n901865);
                                                            
                                                            
                                                            
                                                            if (0) {
                                                                if (scm_is_true(pinned_p901739)) {
                                                                     {
                                                                        
                                                                        variables__scm901747
                                                                          = gp_copy_vector(&(variables901748),
                                                                                           nvar901757);
                                                                        
                                                                        session901745
                                                                          = scm_cons(variables__scm901747,
                                                                                     cnst901746);
                                                                        
                                                                        middle901744
                                                                          = scm_cons(SCM_EOL,
                                                                                     session901745);
                                                                        
                                                                        pinned_p901739
                                                                          = SCM_BOOL_F;
                                                                    }
                                                                }
                                                            }
                                                            
                                                            x902518
                                                              = AREF(variables901748,
                                                                     ii902517);
                                                        }
                                                    }
                                                     {
                                                        SCM y902519;
                                                        y902519 = i901867;
                                                         {
                                                            SCM ss902520;
                                                            if (m901866
                                                                  == SCM_BOOL_F) {
                                                                ss902520
                                                                  = gp_m_unify(x902518,
                                                                               y902519,
                                                                               s901763);
                                                            } else {
                                                                if (m901866
                                                                      == SCM_BOOL_T
                                                                      || k901868
                                                                           == SCM_BOOL_T) {
                                                                    ss902520
                                                                      = gp_gp_unify_raw(x902518,
                                                                                        y902519,
                                                                                        s901763);
                                                                } else {
                                                                    ss902520
                                                                      = gp_gp_unify(x902518,
                                                                                    y902519,
                                                                                    s901763);
                                                                }
                                                            }
                                                             {
                                                                
                                                                
                                                                
                                                                if (scm_is_true(ss902520)) {
                                                                     {
                                                                        
                                                                        s901763
                                                                          = ss902520;
                                                                        
                                                                         {
                                                                            void * jmp902524;
                                                                            jmp902524
                                                                              = AREF(_soperations_s,
                                                                                     scm2int(*(inst__pt901760)));
                                                                            
                                                                            
                                                                            
                                                                            ++inst__pt901760;
                                                                            
                                                                            goto *jmp902524;
                                                                        }
                                                                    }
                                                                } else {
                                                                    if (SCM_I_INUMP(p901764)) {
                                                                         {
                                                                            
                                                                            inst__pt901760
                                                                              = instructions901752
                                                                                  + scm2int(p901764);
                                                                            
                                                                             {
                                                                                void * jmp902525;
                                                                                jmp902525
                                                                                  = AREF(_soperations_s,
                                                                                         scm2int(*(inst__pt901760)));
                                                                                
                                                                                
                                                                                
                                                                                ++inst__pt901760;
                                                                                
                                                                                goto *jmp902525;
                                                                            }
                                                                        }
                                                                    } else {
                                                                         {
                                                                            
                                                                            AREF(fp,
                                                                                 0)
                                                                              = p901764;
                                                                            
                                                                            sp
                                                                              = fp
                                                                                  + -1;
                                                                            
                                                                            goto ret;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          unify__constant__2:
                            
                            printf("%s : %d\n", "unify-constant-2", 18);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM n901869;
                            n901869 = AREF(inst__pt901760, 2);
                             {
                                SCM m901870;
                                m901870 = AREF(inst__pt901760, 0);
                                 {
                                    SCM i901871;
                                    i901871 = AREF(inst__pt901760, 1);
                                     {
                                        SCM k901872;
                                        k901872 = AREF(inst__pt901760, 3);
                                         {
                                            
                                            
                                            
                                            inst__pt901760
                                              = inst__pt901760 + 4;
                                            
                                            if (k901872 == SCM_BOOL_F) {
                                                 {
                                                    
                                                    if (SCM_CONSP(n901869)) {
                                                         {
                                                            int ii902531;
                                                            ii902531
                                                              = scm2int(SCM_CAR(n901869));
                                                            
                                                            
                                                            
                                                            AREF(fp,
                                                                 -(nstack901756
                                                                     + ii902531))
                                                              = AREF(constants901750,
                                                                     scm2int(i901871));
                                                        }
                                                    } else {
                                                         {
                                                            int ii902532;
                                                            ii902532
                                                              = scm2int(n901869);
                                                            
                                                            
                                                            
                                                            if (1) {
                                                                if (scm_is_true(pinned_p901739)) {
                                                                     {
                                                                        
                                                                        variables__scm901747
                                                                          = gp_copy_vector(&(variables901748),
                                                                                           nvar901757);
                                                                        
                                                                        session901745
                                                                          = scm_cons(variables__scm901747,
                                                                                     cnst901746);
                                                                        
                                                                        middle901744
                                                                          = scm_cons(SCM_EOL,
                                                                                     session901745);
                                                                        
                                                                        pinned_p901739
                                                                          = SCM_BOOL_F;
                                                                    }
                                                                }
                                                            }
                                                            
                                                            AREF(variables901748,
                                                                 ii902532)
                                                              = AREF(constants901750,
                                                                     scm2int(i901871));
                                                        }
                                                    }
                                                    
                                                     {
                                                        void * jmp902533;
                                                        jmp902533
                                                          = AREF(_soperations_s,
                                                                 scm2int(*(inst__pt901760)));
                                                        
                                                        
                                                        
                                                        ++inst__pt901760;
                                                        
                                                        goto *jmp902533;
                                                    }
                                                }
                                            } else {
                                                 {
                                                    SCM x902536;
                                                    if (SCM_CONSP(n901869)) {
                                                         {
                                                            int ii902534;
                                                            ii902534
                                                              = scm2int(SCM_CAR(n901869));
                                                            
                                                            
                                                            
                                                            x902536
                                                              = AREF(fp,
                                                                     -(nstack901756
                                                                         + ii902534));
                                                        }
                                                    } else {
                                                         {
                                                            int ii902535;
                                                            ii902535
                                                              = scm2int(n901869);
                                                            
                                                            
                                                            
                                                            if (0) {
                                                                if (scm_is_true(pinned_p901739)) {
                                                                     {
                                                                        
                                                                        variables__scm901747
                                                                          = gp_copy_vector(&(variables901748),
                                                                                           nvar901757);
                                                                        
                                                                        session901745
                                                                          = scm_cons(variables__scm901747,
                                                                                     cnst901746);
                                                                        
                                                                        middle901744
                                                                          = scm_cons(SCM_EOL,
                                                                                     session901745);
                                                                        
                                                                        pinned_p901739
                                                                          = SCM_BOOL_F;
                                                                    }
                                                                }
                                                            }
                                                            
                                                            x902536
                                                              = AREF(variables901748,
                                                                     ii902535);
                                                        }
                                                    }
                                                     {
                                                        SCM y902537;
                                                        y902537
                                                          = AREF(constants901750,
                                                                 scm2int(i901871));
                                                         {
                                                            SCM ss902538;
                                                            if (m901870
                                                                  == SCM_BOOL_F) {
                                                                ss902538
                                                                  = gp_m_unify(x902536,
                                                                               y902537,
                                                                               s901763);
                                                            } else {
                                                                if (m901870
                                                                      == SCM_BOOL_T
                                                                      || k901872
                                                                           == SCM_BOOL_T) {
                                                                    ss902538
                                                                      = gp_gp_unify_raw(x902536,
                                                                                        y902537,
                                                                                        s901763);
                                                                } else {
                                                                    ss902538
                                                                      = gp_gp_unify(x902536,
                                                                                    y902537,
                                                                                    s901763);
                                                                }
                                                            }
                                                             {
                                                                
                                                                
                                                                
                                                                if (scm_is_true(ss902538)) {
                                                                     {
                                                                        
                                                                        s901763
                                                                          = ss902538;
                                                                        
                                                                         {
                                                                            void * jmp902542;
                                                                            jmp902542
                                                                              = AREF(_soperations_s,
                                                                                     scm2int(*(inst__pt901760)));
                                                                            
                                                                            
                                                                            
                                                                            ++inst__pt901760;
                                                                            
                                                                            goto *jmp902542;
                                                                        }
                                                                    }
                                                                } else {
                                                                    if (SCM_I_INUMP(p901764)) {
                                                                         {
                                                                            
                                                                            inst__pt901760
                                                                              = instructions901752
                                                                                  + scm2int(p901764);
                                                                            
                                                                             {
                                                                                void * jmp902543;
                                                                                jmp902543
                                                                                  = AREF(_soperations_s,
                                                                                         scm2int(*(inst__pt901760)));
                                                                                
                                                                                
                                                                                
                                                                                ++inst__pt901760;
                                                                                
                                                                                goto *jmp902543;
                                                                            }
                                                                        }
                                                                    } else {
                                                                         {
                                                                            
                                                                            AREF(fp,
                                                                                 0)
                                                                              = p901764;
                                                                            
                                                                            sp
                                                                              = fp
                                                                                  + -1;
                                                                            
                                                                            goto ret;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          unify__instruction:
                            
                            printf("%s : %d\n", "unify-instruction", 17);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM i901873;
                            i901873 = AREF(inst__pt901760, 0);
                             {
                                SCM m901874;
                                m901874 = AREF(inst__pt901760, 1);
                                 {
                                    
                                    
                                    
                                    inst__pt901760 = inst__pt901760 + 2;
                                    
                                     {
                                        SCM x902549;
                                        x902549 = AREF(sp, 1);
                                         {
                                            SCM y902550;
                                            y902550 = i901873;
                                             {
                                                SCM ss902551;
                                                if (m901874 == SCM_BOOL_F) {
                                                    ss902551
                                                      = gp_m_unify(x902549,
                                                                   y902550,
                                                                   s901763);
                                                } else {
                                                    ss902551
                                                      = gp_gp_unify_raw(x902549,
                                                                        y902550,
                                                                        s901763);
                                                }
                                                 {
                                                    
                                                    
                                                    
                                                     {
                                                        
                                                        AREF(sp, 1)
                                                          = SCM_BOOL_F;
                                                        
                                                        sp = sp + 1;
                                                    }
                                                    
                                                    if (scm_is_true(ss902551)) {
                                                         {
                                                            
                                                            s901763 = ss902551;
                                                            
                                                             {
                                                                void * jmp902554;
                                                                jmp902554
                                                                  = AREF(_soperations_s,
                                                                         scm2int(*(inst__pt901760)));
                                                                
                                                                
                                                                
                                                                ++inst__pt901760;
                                                                
                                                                goto *jmp902554;
                                                            }
                                                        }
                                                    } else {
                                                        if (SCM_I_INUMP(p901764)) {
                                                             {
                                                                
                                                                inst__pt901760
                                                                  = instructions901752
                                                                      + scm2int(p901764);
                                                                
                                                                 {
                                                                    void * jmp902555;
                                                                    jmp902555
                                                                      = AREF(_soperations_s,
                                                                             scm2int(*(inst__pt901760)));
                                                                    
                                                                    
                                                                    
                                                                    ++inst__pt901760;
                                                                    
                                                                    goto *jmp902555;
                                                                }
                                                            }
                                                        } else {
                                                             {
                                                                
                                                                AREF(fp, 0)
                                                                  = p901764;
                                                                
                                                                sp = fp + -1;
                                                                
                                                                goto ret;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          unify__constant:
                            
                            printf("%s : %d\n", "unify-constant", 16);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM i901875;
                            i901875 = AREF(inst__pt901760, 0);
                             {
                                SCM m901876;
                                m901876 = AREF(inst__pt901760, 1);
                                 {
                                    
                                    
                                    
                                    inst__pt901760 = inst__pt901760 + 2;
                                    
                                     {
                                        SCM x902562;
                                        x902562 = AREF(sp, 1);
                                         {
                                            SCM y902563;
                                            y902563
                                              = AREF(constants901750,
                                                     scm2int(i901875));
                                             {
                                                SCM ss902564;
                                                if (m901876 == SCM_BOOL_F) {
                                                    ss902564
                                                      = gp_m_unify(x902562,
                                                                   y902563,
                                                                   s901763);
                                                } else {
                                                    ss902564
                                                      = gp_gp_unify_raw(x902562,
                                                                        y902563,
                                                                        s901763);
                                                }
                                                 {
                                                    
                                                    
                                                    
                                                     {
                                                        
                                                        AREF(sp, 1)
                                                          = SCM_BOOL_F;
                                                        
                                                        sp = sp + 1;
                                                    }
                                                    
                                                    if (scm_is_true(ss902564)) {
                                                         {
                                                            
                                                            s901763 = ss902564;
                                                            
                                                             {
                                                                void * jmp902567;
                                                                jmp902567
                                                                  = AREF(_soperations_s,
                                                                         scm2int(*(inst__pt901760)));
                                                                
                                                                
                                                                
                                                                ++inst__pt901760;
                                                                
                                                                goto *jmp902567;
                                                            }
                                                        }
                                                    } else {
                                                        if (SCM_I_INUMP(p901764)) {
                                                             {
                                                                
                                                                inst__pt901760
                                                                  = instructions901752
                                                                      + scm2int(p901764);
                                                                
                                                                 {
                                                                    void * jmp902568;
                                                                    jmp902568
                                                                      = AREF(_soperations_s,
                                                                             scm2int(*(inst__pt901760)));
                                                                    
                                                                    
                                                                    
                                                                    ++inst__pt901760;
                                                                    
                                                                    goto *jmp902568;
                                                                }
                                                            }
                                                        } else {
                                                             {
                                                                
                                                                AREF(fp, 0)
                                                                  = p901764;
                                                                
                                                                sp = fp + -1;
                                                                
                                                                goto ret;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          equal__instruction:
                            
                            printf("%s : %d\n", "equal-instruction", 96);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM i901877;
                            i901877 = AREF(inst__pt901760, 0);
                             {
                                SCM x901878;
                                x901878 = i901877;
                                 {
                                    SCM y901879;
                                    y901879 = AREF(sp, 1);
                                     {
                                        
                                        
                                        
                                         {
                                            
                                            AREF(sp, 1) = SCM_BOOL_F;
                                            
                                            sp = sp + 1;
                                        }
                                        
                                        if (scm_is_true(scm_equal_p(x901878,
                                                                    y901879))) {
                                             {
                                                void * jmp902575;
                                                jmp902575
                                                  = AREF(_soperations_s,
                                                         scm2int(*(inst__pt901760)));
                                                
                                                
                                                
                                                ++inst__pt901760;
                                                
                                                goto *jmp902575;
                                            }
                                        } else {
                                            if (SCM_I_INUMP(p901764)) {
                                                 {
                                                    
                                                    inst__pt901760
                                                      = instructions901752
                                                          + scm2int(p901764);
                                                    
                                                     {
                                                        void * jmp902576;
                                                        jmp902576
                                                          = AREF(_soperations_s,
                                                                 scm2int(*(inst__pt901760)));
                                                        
                                                        
                                                        
                                                        ++inst__pt901760;
                                                        
                                                        goto *jmp902576;
                                                    }
                                                }
                                            } else {
                                                 {
                                                    
                                                    AREF(fp, 0) = p901764;
                                                    
                                                    sp = fp + -1;
                                                    
                                                    goto ret;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          equal__constant:
                            
                            printf("%s : %d\n", "equal-constant", 95);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM i901880;
                            i901880 = AREF(inst__pt901760, 0);
                             {
                                SCM x901881;
                                x901881
                                  = AREF(constants901750, scm2int(i901880));
                                 {
                                    SCM y901882;
                                    y901882 = AREF(sp, 1);
                                     {
                                        
                                        
                                        
                                         {
                                            
                                            AREF(sp, 1) = SCM_BOOL_F;
                                            
                                            sp = sp + 1;
                                        }
                                        
                                        if (scm_is_true(scm_equal_p(x901881,
                                                                    y901882))) {
                                             {
                                                void * jmp902583;
                                                jmp902583
                                                  = AREF(_soperations_s,
                                                         scm2int(*(inst__pt901760)));
                                                
                                                
                                                
                                                ++inst__pt901760;
                                                
                                                goto *jmp902583;
                                            }
                                        } else {
                                            if (SCM_I_INUMP(p901764)) {
                                                 {
                                                    
                                                    inst__pt901760
                                                      = instructions901752
                                                          + scm2int(p901764);
                                                    
                                                     {
                                                        void * jmp902584;
                                                        jmp902584
                                                          = AREF(_soperations_s,
                                                                 scm2int(*(inst__pt901760)));
                                                        
                                                        
                                                        
                                                        ++inst__pt901760;
                                                        
                                                        goto *jmp902584;
                                                    }
                                                }
                                            } else {
                                                 {
                                                    
                                                    AREF(fp, 0) = p901764;
                                                    
                                                    sp = fp + -1;
                                                    
                                                    goto ret;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      icons_e:
                        
                        printf("%s : %d\n", "icons!", 29);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x901883;
                        x901883 = AREF(sp, 1);
                         {
                            SCM ss901884;
                            ss901884 = gp_pair_bang(x901883, s901763);
                             {
                                
                                
                                
                                if (scm_is_true(ss901884)) {
                                     {
                                        
                                        sp = sp - 1;
                                        
                                        s901763 = ss901884;
                                        
                                        AREF(sp, 2)
                                          = gp_gp_cdr(x901883, s901763);
                                        
                                        AREF(sp, 1) = gp_car(x901883, s901763);
                                        
                                         {
                                            void * jmp902591;
                                            jmp902591
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp902591;
                                        }
                                    }
                                } else {
                                    if (SCM_I_INUMP(p901764)) {
                                         {
                                            
                                            inst__pt901760
                                              = instructions901752
                                                  + scm2int(p901764);
                                            
                                             {
                                                void * jmp902592;
                                                jmp902592
                                                  = AREF(_soperations_s,
                                                         scm2int(*(inst__pt901760)));
                                                
                                                
                                                
                                                ++inst__pt901760;
                                                
                                                goto *jmp902592;
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            AREF(fp, 0) = p901764;
                                            
                                            sp = fp + -1;
                                            
                                            goto ret;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      ifkn_e:
                        
                        printf("%s : %d\n", "ifkn!", 28);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x901885;
                        x901885 = lookup(AREF(sp, 1), s901763);
                         {
                            SCM ss901886;
                            ss901886 = gp_c_vector_x(x901885, 1, s901763);
                             {
                                
                                
                                
                                if (scm_is_true(ss901886)) {
                                     {
                                        SCM xb902599;
                                        xb902599
                                          = scm_c_vector_ref(x901885, 0);
                                         {
                                            
                                            
                                            
                                            s901763 = ss901886;
                                            
                                            AREF(sp, 1) = xb902599;
                                            
                                             {
                                                void * jmp902601;
                                                jmp902601
                                                  = AREF(_soperations_s,
                                                         scm2int(*(inst__pt901760)));
                                                
                                                
                                                
                                                ++inst__pt901760;
                                                
                                                goto *jmp902601;
                                            }
                                        }
                                    }
                                } else {
                                    if (SCM_I_INUMP(p901764)) {
                                         {
                                            
                                            inst__pt901760
                                              = instructions901752
                                                  + scm2int(p901764);
                                            
                                             {
                                                void * jmp902600;
                                                jmp902600
                                                  = AREF(_soperations_s,
                                                         scm2int(*(inst__pt901760)));
                                                
                                                
                                                
                                                ++inst__pt901760;
                                                
                                                goto *jmp902600;
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            AREF(fp, 0) = p901764;
                                            
                                            sp = fp + -1;
                                            
                                            goto ret;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      icurly_e:
                        
                        printf("%s : %d\n", "icurly!", 27);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x901887;
                        x901887 = lookup(AREF(sp, 1), s901763);
                         {
                            SCM q901888;
                            q901888 = gp_c_vector_x(x901887, 2, s901763);
                             {
                                
                                
                                
                                if (scm_is_true(q901888)) {
                                     {
                                        SCM q902606;
                                        q902606
                                          = gp_gp_unify(brace161892,
                                                        scm_c_vector_ref(x901887,
                                                                         0),
                                                        q901888);
                                         {
                                            
                                            
                                            
                                            if (q902606) {
                                                 {
                                                    
                                                    s901763 = q902606;
                                                    
                                                    AREF(sp, 1)
                                                      = scm_c_vector_ref(x901887,
                                                                         1);
                                                    
                                                     {
                                                        void * jmp902608;
                                                        jmp902608
                                                          = AREF(_soperations_s,
                                                                 scm2int(*(inst__pt901760)));
                                                        
                                                        
                                                        
                                                        ++inst__pt901760;
                                                        
                                                        goto *jmp902608;
                                                    }
                                                }
                                            } else {
                                                if (SCM_I_INUMP(p901764)) {
                                                     {
                                                        
                                                        inst__pt901760
                                                          = instructions901752
                                                              + scm2int(p901764);
                                                        
                                                         {
                                                            void * jmp902609;
                                                            jmp902609
                                                              = AREF(_soperations_s,
                                                                     scm2int(*(inst__pt901760)));
                                                            
                                                            
                                                            
                                                            ++inst__pt901760;
                                                            
                                                            goto *jmp902609;
                                                        }
                                                    }
                                                } else {
                                                     {
                                                        
                                                        AREF(fp, 0) = p901764;
                                                        
                                                        sp = fp + -1;
                                                        
                                                        goto ret;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (SCM_I_INUMP(p901764)) {
                                         {
                                            
                                            inst__pt901760
                                              = instructions901752
                                                  + scm2int(p901764);
                                            
                                             {
                                                void * jmp902607;
                                                jmp902607
                                                  = AREF(_soperations_s,
                                                         scm2int(*(inst__pt901760)));
                                                
                                                
                                                
                                                ++inst__pt901760;
                                                
                                                goto *jmp902607;
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            AREF(fp, 0) = p901764;
                                            
                                            sp = fp + -1;
                                            
                                            goto ret;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      push__instruction:
                        
                        printf("%s : %d\n", "push-instruction", 46);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x901889;
                        x901889 = *inst__pt901760;
                        
                        
                        
                        ++inst__pt901760;
                        
                        *sp = x901889;
                        
                        sp = sp - 1;
                    }
                    
                     {
                        void * jmp901890;
                        jmp901890
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901890;
                    }
                    
                     {
                        
                        
                      pushv:
                        
                        printf("%s : %d\n", "pushv", 56);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x901891;
                        x901891 = *inst__pt901760;
                        
                        
                        
                        ++inst__pt901760;
                        
                        if (scm_is_false(x901891)) {
                            *sp = gp_mkvar(s901763);
                        } else {
                            if (SCM_CONSP(x901891)) {
                                 {
                                    int i902620;
                                    i902620 = scm2int(SCM_CAR(x901891));
                                    
                                    
                                    
                                    *sp = AREF(fp, -(nstack901756 + i902620));
                                }
                            } else {
                                 {
                                    int i902621;
                                    i902621 = scm2int(x901891);
                                    
                                    
                                    
                                    if (0) {
                                        if (scm_is_true(pinned_p901739)) {
                                             {
                                                
                                                variables__scm901747
                                                  = gp_copy_vector(&(variables901748),
                                                                   nvar901757);
                                                
                                                session901745
                                                  = scm_cons(variables__scm901747,
                                                             cnst901746);
                                                
                                                middle901744
                                                  = scm_cons(SCM_EOL,
                                                             session901745);
                                                
                                                pinned_p901739 = SCM_BOOL_F;
                                            }
                                        }
                                    }
                                    
                                    *sp = AREF(variables901748, i902621);
                                }
                            }
                        }
                        
                        sp = sp - 1;
                        
                         {
                            void * jmp902622;
                            jmp902622
                              = AREF(_soperations_s,
                                     scm2int(*(inst__pt901760)));
                            
                            
                            
                            ++inst__pt901760;
                            
                            goto *jmp902622;
                        }
                    }
                    
                     {
                        
                        
                      push__constant:
                        
                        printf("%s : %d\n", "push-constant", 45);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        int n901892;
                        n901892 = scm2int(*(inst__pt901760));
                        
                        
                        
                        ++inst__pt901760;
                        
                        *sp = AREF(constants901750, n901892);
                        
                        sp = sp - 1;
                    }
                    
                     {
                        void * jmp901893;
                        jmp901893
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901893;
                    }
                    
                     {
                        
                        
                      push__variable__s:
                        
                        printf("%s : %d\n", "push-variable-s", 47);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        int i901894;
                        i901894 = scm2int(*(inst__pt901760));
                         {
                            SCM v901895;
                            if (1) {
                                v901895 = AREF(fp, -(nstack901756 + i901894));
                            } else {
                                 {
                                    
                                    if (0) {
                                        if (scm_is_true(pinned_p901739)) {
                                             {
                                                
                                                variables__scm901747
                                                  = gp_copy_vector(&(variables901748),
                                                                   nvar901757);
                                                
                                                session901745
                                                  = scm_cons(variables__scm901747,
                                                             cnst901746);
                                                
                                                middle901744
                                                  = scm_cons(SCM_EOL,
                                                             session901745);
                                                
                                                pinned_p901739 = SCM_BOOL_F;
                                            }
                                        }
                                    }
                                    
                                    v901895 = AREF(variables901748, i901894);
                                }
                            }
                             {
                                
                                
                                
                                inst__pt901760 = inst__pt901760 + 1;
                                
                                scm_simple_format(SCM_BOOL_T,
                                                  scm_string795488,
                                                  scm_list_1(v901895));
                                
                                AREF(sp, 0) = v901895;
                                
                                sp = sp - 1;
                            }
                        }
                    }
                    
                     {
                        void * jmp901896;
                        jmp901896
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901896;
                    }
                    
                     {
                        
                        
                      push__variable__v:
                        
                        printf("%s : %d\n", "push-variable-v", 48);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        int i901897;
                        i901897 = scm2int(*(inst__pt901760));
                         {
                            SCM v901898;
                            if (0) {
                                v901898 = AREF(fp, -(nstack901756 + i901897));
                            } else {
                                 {
                                    
                                    if (0) {
                                        if (scm_is_true(pinned_p901739)) {
                                             {
                                                
                                                variables__scm901747
                                                  = gp_copy_vector(&(variables901748),
                                                                   nvar901757);
                                                
                                                session901745
                                                  = scm_cons(variables__scm901747,
                                                             cnst901746);
                                                
                                                middle901744
                                                  = scm_cons(SCM_EOL,
                                                             session901745);
                                                
                                                pinned_p901739 = SCM_BOOL_F;
                                            }
                                        }
                                    }
                                    
                                    v901898 = AREF(variables901748, i901897);
                                }
                            }
                             {
                                
                                
                                
                                inst__pt901760 = inst__pt901760 + 1;
                                
                                scm_simple_format(SCM_BOOL_T,
                                                  scm_string795488,
                                                  scm_list_1(v901898));
                                
                                AREF(sp, 0) = v901898;
                                
                                sp = sp - 1;
                            }
                        }
                    }
                    
                     {
                        void * jmp901899;
                        jmp901899
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901899;
                    }
                    
                     {
                        
                        
                      push__2variables__s:
                        
                        printf("%s : %d\n", "push-2variables-s", 49);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        ulong v901900;
                        v901900 = scm2ulong(*(inst__pt901760));
                         {
                            int v1901901;
                            v1901901 = v901900 & 65535;
                             {
                                int v2901902;
                                v2901902 = v901900 >> 16;
                                 {
                                    SCM x1901903;
                                    if (1) {
                                        x1901903
                                          = AREF(fp,
                                                 -(nstack901756 + v1901901));
                                    } else {
                                         {
                                            
                                            if (0) {
                                                if (scm_is_true(pinned_p901739)) {
                                                     {
                                                        
                                                        variables__scm901747
                                                          = gp_copy_vector(&(variables901748),
                                                                           nvar901757);
                                                        
                                                        session901745
                                                          = scm_cons(variables__scm901747,
                                                                     cnst901746);
                                                        
                                                        middle901744
                                                          = scm_cons(SCM_EOL,
                                                                     session901745);
                                                        
                                                        pinned_p901739
                                                          = SCM_BOOL_F;
                                                    }
                                                }
                                            }
                                            
                                            x1901903
                                              = AREF(variables901748, v1901901);
                                        }
                                    }
                                     {
                                        SCM x2901904;
                                        if (1) {
                                            x2901904
                                              = AREF(fp,
                                                     -(nstack901756 + v2901902));
                                        } else {
                                             {
                                                
                                                if (0) {
                                                    if (scm_is_true(pinned_p901739)) {
                                                         {
                                                            
                                                            variables__scm901747
                                                              = gp_copy_vector(&(variables901748),
                                                                               nvar901757);
                                                            
                                                            session901745
                                                              = scm_cons(variables__scm901747,
                                                                         cnst901746);
                                                            
                                                            middle901744
                                                              = scm_cons(SCM_EOL,
                                                                         session901745);
                                                            
                                                            pinned_p901739
                                                              = SCM_BOOL_F;
                                                        }
                                                    }
                                                }
                                                
                                                x2901904
                                                  = AREF(variables901748,
                                                         v2901902);
                                            }
                                        }
                                         {
                                            
                                            
                                            
                                            inst__pt901760
                                              = inst__pt901760 + 1;
                                            
                                            scm_simple_format(SCM_BOOL_T,
                                                              scm_string799446,
                                                              scm_list_2(x1901903,
                                                                         x2901904));
                                            
                                            AREF(sp, 0) = x1901903;
                                            
                                            AREF(sp, -1) = x2901904;
                                            
                                            sp = sp - 2;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp901905;
                        jmp901905
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901905;
                    }
                    
                     {
                        
                        
                      push__2variables__x:
                        
                        printf("%s : %d\n", "push-2variables-x", 50);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        ulong v901906;
                        v901906 = scm2ulong(*(inst__pt901760));
                         {
                            int v1901907;
                            v1901907 = v901906 & 65535;
                             {
                                int v2901908;
                                v2901908 = (v901906 & 4294901760) >> 16;
                                 {
                                    int a901909;
                                    a901909 = v901906 >> 32;
                                     {
                                        SCM x1901910;
                                        if (a901909 & 1) {
                                            x1901910
                                              = AREF(fp,
                                                     -(nstack901756 + v1901907));
                                        } else {
                                             {
                                                
                                                if (0) {
                                                    if (scm_is_true(pinned_p901739)) {
                                                         {
                                                            
                                                            variables__scm901747
                                                              = gp_copy_vector(&(variables901748),
                                                                               nvar901757);
                                                            
                                                            session901745
                                                              = scm_cons(variables__scm901747,
                                                                         cnst901746);
                                                            
                                                            middle901744
                                                              = scm_cons(SCM_EOL,
                                                                         session901745);
                                                            
                                                            pinned_p901739
                                                              = SCM_BOOL_F;
                                                        }
                                                    }
                                                }
                                                
                                                x1901910
                                                  = AREF(variables901748,
                                                         v1901907);
                                            }
                                        }
                                         {
                                            SCM x2901911;
                                            if (a901909 & 2) {
                                                x2901911
                                                  = AREF(fp,
                                                         -(nstack901756
                                                             + v2901908));
                                            } else {
                                                 {
                                                    
                                                    if (0) {
                                                        if (scm_is_true(pinned_p901739)) {
                                                             {
                                                                
                                                                variables__scm901747
                                                                  = gp_copy_vector(&(variables901748),
                                                                                   nvar901757);
                                                                
                                                                session901745
                                                                  = scm_cons(variables__scm901747,
                                                                             cnst901746);
                                                                
                                                                middle901744
                                                                  = scm_cons(SCM_EOL,
                                                                             session901745);
                                                                
                                                                pinned_p901739
                                                                  = SCM_BOOL_F;
                                                            }
                                                        }
                                                    }
                                                    
                                                    x2901911
                                                      = AREF(variables901748,
                                                             v2901908);
                                                }
                                            }
                                             {
                                                
                                                
                                                
                                                inst__pt901760
                                                  = inst__pt901760 + 1;
                                                
                                                scm_simple_format(SCM_BOOL_T,
                                                                  scm_string799446,
                                                                  scm_list_2(x1901910,
                                                                             x2901911));
                                                
                                                AREF(sp, 0) = x1901910;
                                                
                                                AREF(sp, -1) = x2901911;
                                                
                                                sp = sp - 2;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp901912;
                        jmp901912
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901912;
                    }
                    
                     {
                        
                        
                      push__3variables__s:
                        
                        printf("%s : %d\n", "push-3variables-s", 51);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        ulong v901913;
                        v901913 = scm2ulong(*(inst__pt901760));
                         {
                            int v1901914;
                            v1901914 = v901913 & 65535;
                             {
                                int v2901915;
                                v2901915 = (v901913 & 4294901760) >> 16;
                                 {
                                    int v3901916;
                                    v3901916 = v901913 >> 32;
                                     {
                                        SCM x1901917;
                                        if (1) {
                                            x1901917
                                              = AREF(fp,
                                                     -(nstack901756 + v1901914));
                                        } else {
                                             {
                                                
                                                if (0) {
                                                    if (scm_is_true(pinned_p901739)) {
                                                         {
                                                            
                                                            variables__scm901747
                                                              = gp_copy_vector(&(variables901748),
                                                                               nvar901757);
                                                            
                                                            session901745
                                                              = scm_cons(variables__scm901747,
                                                                         cnst901746);
                                                            
                                                            middle901744
                                                              = scm_cons(SCM_EOL,
                                                                         session901745);
                                                            
                                                            pinned_p901739
                                                              = SCM_BOOL_F;
                                                        }
                                                    }
                                                }
                                                
                                                x1901917
                                                  = AREF(variables901748,
                                                         v1901914);
                                            }
                                        }
                                         {
                                            SCM x2901918;
                                            if (1) {
                                                x2901918
                                                  = AREF(fp,
                                                         -(nstack901756
                                                             + v2901915));
                                            } else {
                                                 {
                                                    
                                                    if (0) {
                                                        if (scm_is_true(pinned_p901739)) {
                                                             {
                                                                
                                                                variables__scm901747
                                                                  = gp_copy_vector(&(variables901748),
                                                                                   nvar901757);
                                                                
                                                                session901745
                                                                  = scm_cons(variables__scm901747,
                                                                             cnst901746);
                                                                
                                                                middle901744
                                                                  = scm_cons(SCM_EOL,
                                                                             session901745);
                                                                
                                                                pinned_p901739
                                                                  = SCM_BOOL_F;
                                                            }
                                                        }
                                                    }
                                                    
                                                    x2901918
                                                      = AREF(variables901748,
                                                             v2901915);
                                                }
                                            }
                                             {
                                                SCM x3901919;
                                                if (1) {
                                                    x3901919
                                                      = AREF(fp,
                                                             -(nstack901756
                                                                 + v3901916));
                                                } else {
                                                     {
                                                        
                                                        if (0) {
                                                            if (scm_is_true(pinned_p901739)) {
                                                                 {
                                                                    
                                                                    variables__scm901747
                                                                      = gp_copy_vector(&(variables901748),
                                                                                       nvar901757);
                                                                    
                                                                    session901745
                                                                      = scm_cons(variables__scm901747,
                                                                                 cnst901746);
                                                                    
                                                                    middle901744
                                                                      = scm_cons(SCM_EOL,
                                                                                 session901745);
                                                                    
                                                                    pinned_p901739
                                                                      = SCM_BOOL_F;
                                                                }
                                                            }
                                                        }
                                                        
                                                        x3901919
                                                          = AREF(variables901748,
                                                                 v3901916);
                                                    }
                                                }
                                                 {
                                                    
                                                    
                                                    
                                                    inst__pt901760
                                                      = inst__pt901760 + 1;
                                                    
                                                    scm_simple_format(SCM_BOOL_T,
                                                                      scm_string804418,
                                                                      scm_list_3(x1901917,
                                                                                 x2901918,
                                                                                 x3901919));
                                                    
                                                    AREF(sp, 0) = x1901917;
                                                    
                                                    AREF(sp, -1) = x2901918;
                                                    
                                                    AREF(sp, -2) = x3901919;
                                                    
                                                    sp = sp - 3;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp901920;
                        jmp901920
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901920;
                    }
                    
                     {
                        
                        
                      push__3variables__x:
                        
                        printf("%s : %d\n", "push-3variables-x", 52);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        ulong v901921;
                        v901921 = scm2ulong(*(inst__pt901760));
                         {
                            int v1901922;
                            v1901922 = v901921 & 65535;
                             {
                                int v2901923;
                                v2901923 = (v901921 & 4294901760) >> 16;
                                 {
                                    int v3901924;
                                    v3901924
                                      = (v901921 & 281470681743360) >> 32;
                                     {
                                        int a901925;
                                        a901925 = v901921 >> 48;
                                         {
                                            SCM x1901926;
                                            if (a901925 & 1) {
                                                x1901926
                                                  = AREF(fp,
                                                         -(nstack901756
                                                             + v1901922));
                                            } else {
                                                 {
                                                    
                                                    if (0) {
                                                        if (scm_is_true(pinned_p901739)) {
                                                             {
                                                                
                                                                variables__scm901747
                                                                  = gp_copy_vector(&(variables901748),
                                                                                   nvar901757);
                                                                
                                                                session901745
                                                                  = scm_cons(variables__scm901747,
                                                                             cnst901746);
                                                                
                                                                middle901744
                                                                  = scm_cons(SCM_EOL,
                                                                             session901745);
                                                                
                                                                pinned_p901739
                                                                  = SCM_BOOL_F;
                                                            }
                                                        }
                                                    }
                                                    
                                                    x1901926
                                                      = AREF(variables901748,
                                                             v1901922);
                                                }
                                            }
                                             {
                                                SCM x2901927;
                                                if (a901925 & 2) {
                                                    x2901927
                                                      = AREF(fp,
                                                             -(nstack901756
                                                                 + v2901923));
                                                } else {
                                                     {
                                                        
                                                        if (0) {
                                                            if (scm_is_true(pinned_p901739)) {
                                                                 {
                                                                    
                                                                    variables__scm901747
                                                                      = gp_copy_vector(&(variables901748),
                                                                                       nvar901757);
                                                                    
                                                                    session901745
                                                                      = scm_cons(variables__scm901747,
                                                                                 cnst901746);
                                                                    
                                                                    middle901744
                                                                      = scm_cons(SCM_EOL,
                                                                                 session901745);
                                                                    
                                                                    pinned_p901739
                                                                      = SCM_BOOL_F;
                                                                }
                                                            }
                                                        }
                                                        
                                                        x2901927
                                                          = AREF(variables901748,
                                                                 v2901923);
                                                    }
                                                }
                                                 {
                                                    SCM x3901928;
                                                    if (a901925 & 4) {
                                                        x3901928
                                                          = AREF(fp,
                                                                 -(nstack901756
                                                                     + v3901924));
                                                    } else {
                                                         {
                                                            
                                                            if (0) {
                                                                if (scm_is_true(pinned_p901739)) {
                                                                     {
                                                                        
                                                                        variables__scm901747
                                                                          = gp_copy_vector(&(variables901748),
                                                                                           nvar901757);
                                                                        
                                                                        session901745
                                                                          = scm_cons(variables__scm901747,
                                                                                     cnst901746);
                                                                        
                                                                        middle901744
                                                                          = scm_cons(SCM_EOL,
                                                                                     session901745);
                                                                        
                                                                        pinned_p901739
                                                                          = SCM_BOOL_F;
                                                                    }
                                                                }
                                                            }
                                                            
                                                            x3901928
                                                              = AREF(variables901748,
                                                                     v3901924);
                                                        }
                                                    }
                                                     {
                                                        
                                                        
                                                        
                                                        inst__pt901760
                                                          = inst__pt901760 + 1;
                                                        
                                                        scm_simple_format(SCM_BOOL_T,
                                                                          scm_string804418,
                                                                          scm_list_3(x1901926,
                                                                                     x2901927,
                                                                                     x3901928));
                                                        
                                                        AREF(sp, 0) = x1901926;
                                                        
                                                        AREF(sp, -1)
                                                          = x2901927;
                                                        
                                                        AREF(sp, -2)
                                                          = x3901928;
                                                        
                                                        sp = sp - 3;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp901929;
                        jmp901929
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901929;
                    }
                    
                     {
                        
                        
                      push__variable__scm:
                        
                        printf("%s : %d\n", "push-variable-scm", 54);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x901930;
                        x901930 = *inst__pt901760;
                         {
                            SCM v901933;
                            if (SCM_CONSP(x901930)) {
                                 {
                                    int i901931;
                                    i901931 = scm2int(SCM_CAR(x901930));
                                    
                                    
                                    
                                    v901933
                                      = AREF(fp, -(nstack901756 + i901931));
                                }
                            } else {
                                 {
                                    int i901932;
                                    i901932 = scm2int(x901930);
                                    
                                    
                                    
                                    if (0) {
                                        if (scm_is_true(pinned_p901739)) {
                                             {
                                                
                                                variables__scm901747
                                                  = gp_copy_vector(&(variables901748),
                                                                   nvar901757);
                                                
                                                session901745
                                                  = scm_cons(variables__scm901747,
                                                             cnst901746);
                                                
                                                middle901744
                                                  = scm_cons(SCM_EOL,
                                                             session901745);
                                                
                                                pinned_p901739 = SCM_BOOL_F;
                                            }
                                        }
                                    }
                                    
                                    v901933 = AREF(variables901748, i901932);
                                }
                            }
                             {
                                
                                
                                
                                scm_simple_format(SCM_BOOL_T,
                                                  scm_string808963,
                                                  scm_list_2(x901930, v901933));
                                
                                inst__pt901760 = inst__pt901760 + 1;
                                
                                *sp = v901933;
                                
                                sp = sp - 1;
                            }
                        }
                    }
                    
                     {
                        void * jmp901934;
                        jmp901934
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901934;
                    }
                    
                     {
                        
                        
                      pop__variable:
                        
                        printf("%s : %d\n", "pop-variable", 55);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        int n901935;
                        n901935 = scm2int(*(inst__pt901760));
                        
                        
                        
                        ++inst__pt901760;
                        
                        AREF(variables901748, n901935) = AREF(sp, 1);
                        
                        sp = sp + 1;
                    }
                    
                     {
                        void * jmp901936;
                        jmp901936
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901936;
                    }
                    
                     {
                        
                        
                      pop:
                        
                        printf("%s : %d\n", "pop", 61);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        int n901937;
                        n901937 = scm2int(*(inst__pt901760));
                        
                        
                        
                         {
                            int m902695;
                            m902695 = n901937;
                             {
                                
                              lp902697:
                                 {
                                    
                                    if (!(m902695 == 0)) {
                                         {
                                            
                                            sp = sp + 1;
                                            
                                            AREF(sp, 0) = SCM_BOOL_F;
                                            
                                             {
                                                int next902702;
                                                next902702 = m902695 - 1;
                                                m902695 = next902702;
                                                goto lp902697;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        ++inst__pt901760;
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            void * jmp902696;
                            jmp902696
                              = AREF(_soperations_s,
                                     scm2int(*(inst__pt901760)));
                            
                            
                            
                            ++inst__pt901760;
                            
                            goto *jmp902696;
                        }
                    }
                    
                     {
                        
                        
                      seek:
                        
                        printf("%s : %d\n", "seek", 60);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        int n901938;
                        n901938 = scm2int(*(inst__pt901760));
                        
                        
                        
                        ++inst__pt901760;
                        
                        sp = sp - n901938;
                        
                         {
                            void * jmp902705;
                            jmp902705
                              = AREF(_soperations_s,
                                     scm2int(*(inst__pt901760)));
                            
                            
                            
                            ++inst__pt901760;
                            
                            goto *jmp902705;
                        }
                    }
                    
                     {
                        
                        
                      dup:
                        
                        printf("%s : %d\n", "dup", 62);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                    AREF(sp, 0) = AREF(sp, 1);
                    
                    sp = sp - 1;
                    
                     {
                        void * jmp901939;
                        jmp901939
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901939;
                    }
                    
                     {
                        
                        
                      mk__cons:
                        
                        printf("%s : %d\n", "mk-cons", 57);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM c901940;
                        c901940
                          = gp_cons_bang(AREF(sp, 2), AREF(sp, 1), s901763);
                        
                        
                        
                        AREF(sp, 2) = c901940;
                        
                         {
                            
                            AREF(sp, 1) = SCM_BOOL_F;
                            
                            sp = sp + 1;
                        }
                    }
                    
                     {
                        void * jmp901941;
                        jmp901941
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901941;
                    }
                    
                     {
                        
                        
                      mk__fkn:
                        
                        printf("%s : %d\n", "mk-fkn", 58);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        int n901942;
                        n901942 = scm2int(*(inst__pt901760));
                         {
                            
                            
                            
                            ++inst__pt901760;
                            
                             {
                                int i902712;
                                i902712 = n901942;
                                 {
                                    
                                  lp902713:
                                     {
                                        
                                        if (i902712 <= 0) {
                                             {
                                                SCM v902714;
                                                v902714
                                                  = scm_c_make_vector(1,
                                                                      SCM_BOOL_F);
                                                
                                                
                                                
                                                scm_c_vector_set_x(v902714,
                                                                   0,
                                                                   AREF(sp, 1));
                                                
                                                AREF(sp, 1) = v902714;
                                            }
                                        } else {
                                             {
                                                
                                                 {
                                                    SCM c902715;
                                                    c902715
                                                      = gp_cons_bang(AREF(sp,
                                                                          2),
                                                                     AREF(sp,
                                                                          1),
                                                                     s901763);
                                                    
                                                    
                                                    
                                                    AREF(sp, 2) = c902715;
                                                    
                                                     {
                                                        
                                                        AREF(sp, 1)
                                                          = SCM_BOOL_F;
                                                        
                                                        sp = sp + 1;
                                                    }
                                                }
                                                
                                                 {
                                                    int next902718;
                                                    next902718 = i902712 - 1;
                                                    i902712 = next902718;
                                                    goto lp902713;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp901943;
                        jmp901943
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901943;
                    }
                    
                     {
                        
                        
                      mk__curly:
                        
                        printf("%s : %d\n", "mk-curly", 59);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM v901944;
                        v901944 = scm_c_make_vector(2, SCM_BOOL_F);
                        
                        
                        
                        scm_c_vector_set_x(v901944, 0, brace161892);
                        
                        scm_c_vector_set_x(v901944, 1, AREF(sp, 1));
                        
                        AREF(sp, 1) = v901944;
                    }
                    
                     {
                        void * jmp901945;
                        jmp901945
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp901945;
                    }
                    
                     {
                        
                        
                      icons:
                        
                        printf("%s : %d\n", "icons", 22);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x901946;
                        x901946 = AREF(sp, 1);
                         {
                            SCM ss901947;
                            ss901947 = gp_pair(x901946, s901763);
                             {
                                
                                
                                
                                if (scm_is_true(ss901947)) {
                                     {
                                        
                                        sp = sp - 1;
                                        
                                        AREF(sp, 2)
                                          = gp_gp_cdr(x901946, s901763);
                                        
                                        AREF(sp, 1) = gp_car(x901946, s901763);
                                        
                                         {
                                            void * jmp902721;
                                            jmp902721
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp902721;
                                        }
                                    }
                                } else {
                                    if (SCM_I_INUMP(p901764)) {
                                         {
                                            
                                            inst__pt901760
                                              = instructions901752
                                                  + scm2int(p901764);
                                            
                                             {
                                                void * jmp902722;
                                                jmp902722
                                                  = AREF(_soperations_s,
                                                         scm2int(*(inst__pt901760)));
                                                
                                                
                                                
                                                ++inst__pt901760;
                                                
                                                goto *jmp902722;
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            AREF(fp, 0) = p901764;
                                            
                                            sp = fp + -1;
                                            
                                            goto ret;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      ifkn:
                        
                        printf("%s : %d\n", "ifkn", 21);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x901948;
                        x901948 = gp_gp_lookup(AREF(sp, 1), s901763);
                         {
                            SCM ss901949;
                            ss901949 = gp_c_vector(x901948, 1, s901763);
                             {
                                
                                
                                
                                if (ss901949) {
                                     {
                                        SCM xa902729;
                                        xa902729
                                          = scm_c_vector_ref(x901948, 0);
                                        
                                        
                                        
                                        AREF(sp, 1) = xa902729;
                                        
                                         {
                                            void * jmp902731;
                                            jmp902731
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp902731;
                                        }
                                    }
                                } else {
                                    if (SCM_I_INUMP(p901764)) {
                                         {
                                            
                                            inst__pt901760
                                              = instructions901752
                                                  + scm2int(p901764);
                                            
                                             {
                                                void * jmp902730;
                                                jmp902730
                                                  = AREF(_soperations_s,
                                                         scm2int(*(inst__pt901760)));
                                                
                                                
                                                
                                                ++inst__pt901760;
                                                
                                                goto *jmp902730;
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            AREF(fp, 0) = p901764;
                                            
                                            sp = fp + -1;
                                            
                                            goto ret;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                        
                      icurly:
                        
                        printf("%s : %d\n", "icurly", 20);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x901950;
                        x901950 = gp_gp_lookup(AREF(sp, 1), s901763);
                         {
                            SCM ss901951;
                            ss901951 = gp_c_vector(x901950, 2, s901763);
                             {
                                
                                
                                
                                if (ss901951) {
                                    if (brace161892
                                          == gp_gp_lookup(scm_c_vector_ref(x901950,
                                                                           0),
                                                          s901763)) {
                                         {
                                            
                                            AREF(sp, 1)
                                              = scm_c_vector_ref(x901950, 1);
                                            
                                             {
                                                void * jmp902736;
                                                jmp902736
                                                  = AREF(_soperations_s,
                                                         scm2int(*(inst__pt901760)));
                                                
                                                
                                                
                                                ++inst__pt901760;
                                                
                                                goto *jmp902736;
                                            }
                                        }
                                    } else {
                                        if (SCM_I_INUMP(p901764)) {
                                             {
                                                
                                                inst__pt901760
                                                  = instructions901752
                                                      + scm2int(p901764);
                                                
                                                 {
                                                    void * jmp902737;
                                                    jmp902737
                                                      = AREF(_soperations_s,
                                                             scm2int(*(inst__pt901760)));
                                                    
                                                    
                                                    
                                                    ++inst__pt901760;
                                                    
                                                    goto *jmp902737;
                                                }
                                            }
                                        } else {
                                             {
                                                
                                                AREF(fp, 0) = p901764;
                                                
                                                sp = fp + -1;
                                                
                                                goto ret;
                                            }
                                        }
                                    }
                                } else {
                                    if (SCM_I_INUMP(p901764)) {
                                         {
                                            
                                            inst__pt901760
                                              = instructions901752
                                                  + scm2int(p901764);
                                            
                                             {
                                                void * jmp902738;
                                                jmp902738
                                                  = AREF(_soperations_s,
                                                         scm2int(*(inst__pt901760)));
                                                
                                                
                                                
                                                ++inst__pt901760;
                                                
                                                goto *jmp902738;
                                            }
                                        }
                                    } else {
                                         {
                                            
                                            AREF(fp, 0) = p901764;
                                            
                                            sp = fp + -1;
                                            
                                            goto ret;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          plus:
                            
                            printf("%s : %d\n", "plus", 103);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901952;
                            SCM y901953;
                            x901952 = gp_gp_lookup(AREF(sp, 2), s901763);
                            y901953 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            if (SCM_I_INUMP(x901952) && SCM_I_INUMP(y901953)) {
                                 {
                                    scm_t_int64 n902747;
                                    n902747
                                      = SCM_I_INUM(x901952)
                                          + SCM_I_INUM(y901953);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n902747)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n902747);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_sum(x901952, y901953);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_sum(x901952, y901953);
                            }
                            
                             {
                                void * jmp902748;
                                jmp902748
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902748;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          minus:
                            
                            printf("%s : %d\n", "minus", 105);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901954;
                            SCM y901955;
                            x901954 = gp_gp_lookup(AREF(sp, 2), s901763);
                            y901955 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            if (SCM_I_INUMP(x901954) && SCM_I_INUMP(y901955)) {
                                 {
                                    scm_t_int64 n902753;
                                    n902753
                                      = SCM_I_INUM(x901954)
                                          - SCM_I_INUM(y901955);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n902753)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n902753);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_difference(x901954, y901955);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_difference(x901954, y901955);
                            }
                            
                             {
                                void * jmp902754;
                                jmp902754
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902754;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          band:
                            
                            printf("%s : %d\n", "band", 129);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901956;
                            SCM y901957;
                            x901956 = gp_gp_lookup(AREF(sp, 2), s901763);
                            y901957 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            if (SCM_I_INUMP(x901956) && SCM_I_INUMP(y901957)) {
                                 {
                                    scm_t_int64 n902759;
                                    n902759
                                      = SCM_I_INUM(x901956)
                                          & SCM_I_INUM(y901957);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n902759)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n902759);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_logand(x901956, y901957);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_logand(x901956, y901957);
                            }
                            
                             {
                                void * jmp902760;
                                jmp902760
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902760;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          bor:
                            
                            printf("%s : %d\n", "bor", 131);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901958;
                            SCM y901959;
                            x901958 = gp_gp_lookup(AREF(sp, 2), s901763);
                            y901959 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            if (SCM_I_INUMP(x901958) && SCM_I_INUMP(y901959)) {
                                 {
                                    scm_t_int64 n902765;
                                    n902765
                                      = SCM_I_INUM(x901958)
                                          | SCM_I_INUM(y901959);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n902765)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n902765);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_logior(x901958, y901959);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_logior(x901958, y901959);
                            }
                            
                             {
                                void * jmp902766;
                                jmp902766
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902766;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          xor:
                            
                            printf("%s : %d\n", "xor", 133);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901960;
                            SCM y901961;
                            x901960 = gp_gp_lookup(AREF(sp, 2), s901763);
                            y901961 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            if (SCM_I_INUMP(x901960) && SCM_I_INUMP(y901961)) {
                                 {
                                    scm_t_int64 n902771;
                                    n902771
                                      = SCM_I_INUM(x901960)
                                          ^ SCM_I_INUM(y901961);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n902771)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n902771);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_logxor(x901960, y901961);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_logxor(x901960, y901961);
                            }
                            
                             {
                                void * jmp902772;
                                jmp902772
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902772;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          modulo:
                            
                            printf("%s : %d\n", "modulo", 123);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901962;
                            SCM y901963;
                            x901962 = gp_gp_lookup(AREF(sp, 2), s901763);
                            y901963 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            if (SCM_I_INUMP(x901962) && SCM_I_INUMP(y901963)) {
                                 {
                                    scm_t_int64 n902777;
                                    n902777
                                      = SCM_I_INUM(x901962)
                                          % SCM_I_INUM(y901963);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n902777)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n902777);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_modulo(x901962, y901963);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_modulo(x901962, y901963);
                            }
                            
                             {
                                void * jmp902778;
                                jmp902778
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902778;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          mul:
                            
                            printf("%s : %d\n", "mul", 107);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901964;
                            SCM y901965;
                            x901964 = gp_gp_lookup(AREF(sp, 2), s901763);
                            y901965 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            if (SCM_I_INUMP(x901964) && SCM_I_INUMP(y901965)) {
                                 {
                                    scm_t_int64 xx902783;
                                    scm_t_int64 yy902784;
                                    xx902783 = SCM_I_INUM(x901964);
                                    yy902784 = SCM_I_INUM(y901965);
                                    
                                    
                                    
                                    if (xx902783 < 1073741824
                                          && xx902783 > -1073741824
                                               && yy902784 < 1073741824
                                                    && yy902784 > -1073741824) {
                                         {
                                            scm_t_int64 n902788;
                                            n902788 = xx902783 * yy902784;
                                            
                                            
                                            
                                            AREF(sp, 1)
                                              = SCM_I_MAKINUM(n902788);
                                        }
                                    } else {
                                        AREF(sp, 1)
                                          = scm_product(x901964, y901965);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_product(x901964, y901965);
                            }
                            
                             {
                                void * jmp902785;
                                jmp902785
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902785;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          shift_l:
                            
                            printf("%s : %d\n", "shift_l", 117);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901966;
                            SCM y901967;
                            x901966 = gp_gp_lookup(AREF(sp, 2), s901763);
                            y901967 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            if (SCM_I_INUMP(x901966) && SCM_I_INUMP(y901967)) {
                                 {
                                    scm_t_int64 xx902795;
                                    scm_t_int64 yy902796;
                                    xx902795 = SCM_I_INUM(x901966);
                                    yy902796 = SCM_I_INUM(y901967);
                                    
                                    
                                    
                                    if (xx902795 < 268435456
                                          && xx902795 > -268435456
                                               && yy902796 <= 32
                                                    && yy902796 >= 0) {
                                         {
                                            scm_t_int64 n902800;
                                            n902800 = xx902795 << yy902796;
                                            
                                            
                                            
                                            AREF(sp, 1)
                                              = SCM_I_MAKINUM(n902800);
                                        }
                                    } else {
                                        AREF(sp, 1)
                                          = scm_ash(x901966, y901967);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_ash(x901966, y901967);
                            }
                            
                             {
                                void * jmp902797;
                                jmp902797
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902797;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          shift_r:
                            
                            printf("%s : %d\n", "shift_r", 120);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901968;
                            SCM y901969;
                            x901968 = gp_gp_lookup(AREF(sp, 2), s901763);
                            y901969 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            if (SCM_I_INUMP(x901968) && SCM_I_INUMP(y901969)) {
                                 {
                                    scm_t_int64 xx902805;
                                    scm_t_int64 yy902806;
                                    xx902805 = SCM_I_INUM(x901968);
                                    yy902806 = SCM_I_INUM(y901969);
                                    
                                    
                                    
                                    if (xx902805 < 268435456
                                          && xx902805 > -268435456
                                               && yy902806 <= 32
                                                    && yy902806 >= 0) {
                                         {
                                            scm_t_int64 n902810;
                                            n902810 = xx902805 >> yy902806;
                                            
                                            
                                            
                                            AREF(sp, 1)
                                              = SCM_I_MAKINUM(n902810);
                                        }
                                    } else {
                                        AREF(sp, 1)
                                          = scm_ash(x901968,
                                                    scm_difference(y901969,
                                                                   scm_num721462));
                                    }
                                }
                            } else {
                                AREF(sp, 1)
                                  = scm_ash(x901968,
                                            scm_difference(y901969,
                                                           scm_num721462));
                            }
                            
                             {
                                void * jmp902807;
                                jmp902807
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902807;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          divide:
                            
                            printf("%s : %d\n", "divide", 109);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901970;
                            SCM y901971;
                            x901970 = gp_gp_lookup(AREF(sp, 2), s901763);
                            y901971 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                             {
                                
                                AREF(sp, 1) = SCM_BOOL_F;
                                
                                sp = sp + 1;
                            }
                            
                            AREF(sp, 1) = scm_divide(x901970, y901971);
                            
                             {
                                void * jmp902815;
                                jmp902815
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902815;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          plus2_1:
                            
                            printf("%s : %d\n", "plus2_1", 104);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901972;
                            SCM y901973;
                            x901972 = *inst__pt901760;
                            y901973 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                            if (SCM_I_INUMP(x901972) && SCM_I_INUMP(y901973)) {
                                 {
                                    scm_t_int64 n902818;
                                    n902818
                                      = SCM_I_INUM(x901972)
                                          + SCM_I_INUM(y901973);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n902818)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n902818);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_sum(x901972, y901973);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_sum(x901972, y901973);
                            }
                            
                            inst__pt901760 = inst__pt901760 + 1;
                            
                             {
                                void * jmp902819;
                                jmp902819
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902819;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          minus2_1:
                            
                            printf("%s : %d\n", "minus2_1", 106);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901974;
                            SCM y901975;
                            x901974 = *inst__pt901760;
                            y901975 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                            if (SCM_I_INUMP(x901974) && SCM_I_INUMP(y901975)) {
                                 {
                                    scm_t_int64 n902824;
                                    n902824
                                      = SCM_I_INUM(x901974)
                                          - SCM_I_INUM(y901975);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n902824)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n902824);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_difference(x901974, y901975);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_difference(x901974, y901975);
                            }
                            
                            inst__pt901760 = inst__pt901760 + 1;
                            
                             {
                                void * jmp902825;
                                jmp902825
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902825;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          mul2_1:
                            
                            printf("%s : %d\n", "mul2_1", 108);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901976;
                            SCM y901977;
                            x901976 = *inst__pt901760;
                            y901977 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                            if (SCM_I_INUMP(x901976) && SCM_I_INUMP(y901977)) {
                                 {
                                    scm_t_int64 xx902830;
                                    scm_t_int64 yy902831;
                                    xx902830 = SCM_I_INUM(x901976);
                                    yy902831 = SCM_I_INUM(y901977);
                                    
                                    
                                    
                                    if (xx902830 < 1073741824
                                          && xx902830 > -1073741824
                                               && yy902831 < 1073741824
                                                    && yy902831 > -1073741824) {
                                         {
                                            scm_t_int64 n902833;
                                            n902833 = xx902830 * yy902831;
                                            
                                            
                                            
                                            AREF(sp, 1)
                                              = SCM_I_MAKINUM(n902833);
                                        }
                                    } else {
                                        AREF(sp, 1)
                                          = scm_product(x901976, y901977);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_product(x901976, y901977);
                            }
                            
                            inst__pt901760 = inst__pt901760 + 1;
                            
                             {
                                void * jmp902832;
                                jmp902832
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902832;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          div2_1:
                            
                            printf("%s : %d\n", "div2_1", 110);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901978;
                            SCM y901979;
                            x901978 = *inst__pt901760;
                            y901979 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                            AREF(sp, 1) = scm_divide(x901978, y901979);
                            
                            inst__pt901760 = inst__pt901760 + 1;
                            
                             {
                                void * jmp902842;
                                jmp902842
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902842;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          bitand:
                            
                            printf("%s : %d\n", "bitand", 130);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901980;
                            SCM y901981;
                            x901980 = *inst__pt901760;
                            y901981 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                            if (SCM_I_INUMP(x901980) && SCM_I_INUMP(y901981)) {
                                 {
                                    scm_t_int64 n902845;
                                    n902845
                                      = SCM_I_INUM(x901980)
                                          & SCM_I_INUM(y901981);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n902845)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n902845);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_logand(x901980, y901981);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_logand(x901980, y901981);
                            }
                            
                            inst__pt901760 = inst__pt901760 + 1;
                            
                             {
                                void * jmp902846;
                                jmp902846
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902846;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          bitor:
                            
                            printf("%s : %d\n", "bitor", 132);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901982;
                            SCM y901983;
                            x901982 = *inst__pt901760;
                            y901983 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                            if (SCM_I_INUMP(x901982) && SCM_I_INUMP(y901983)) {
                                 {
                                    scm_t_int64 n902851;
                                    n902851
                                      = SCM_I_INUM(x901982)
                                          | SCM_I_INUM(y901983);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n902851)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n902851);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_logior(x901982, y901983);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_logior(x901982, y901983);
                            }
                            
                            inst__pt901760 = inst__pt901760 + 1;
                            
                             {
                                void * jmp902852;
                                jmp902852
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902852;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          xor1:
                            
                            printf("%s : %d\n", "xor1", 134);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901984;
                            SCM y901985;
                            x901984 = *inst__pt901760;
                            y901985 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                            if (SCM_I_INUMP(x901984) && SCM_I_INUMP(y901985)) {
                                 {
                                    scm_t_int64 n902857;
                                    n902857
                                      = SCM_I_INUM(x901984)
                                          ^ SCM_I_INUM(y901985);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n902857)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n902857);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_logxor(x901984, y901985);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_logxor(x901984, y901985);
                            }
                            
                            inst__pt901760 = inst__pt901760 + 1;
                            
                             {
                                void * jmp902858;
                                jmp902858
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902858;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          shiftLL:
                            
                            printf("%s : %d\n", "shiftLL", 118);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901986;
                            SCM y901987;
                            x901986 = *inst__pt901760;
                            y901987 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                            if (SCM_I_INUMP(x901986) && SCM_I_INUMP(y901987)) {
                                 {
                                    scm_t_int64 xx902863;
                                    scm_t_int64 yy902864;
                                    xx902863 = SCM_I_INUM(x901986);
                                    yy902864 = SCM_I_INUM(y901987);
                                    
                                    
                                    
                                    if (xx902863 < 268435456
                                          && xx902863 > -268435456
                                               && yy902864 <= 32
                                                    && yy902864 >= 0) {
                                         {
                                            scm_t_int64 n902866;
                                            n902866 = xx902863 << yy902864;
                                            
                                            
                                            
                                            AREF(sp, 1)
                                              = SCM_I_MAKINUM(n902866);
                                        }
                                    } else {
                                        AREF(sp, 1)
                                          = scm_ash(x901986, y901987);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_ash(x901986, y901987);
                            }
                            
                            inst__pt901760 = inst__pt901760 + 1;
                            
                             {
                                void * jmp902865;
                                jmp902865
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902865;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          shiftRL:
                            
                            printf("%s : %d\n", "shiftRL", 121);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901988;
                            SCM y901989;
                            x901988 = *inst__pt901760;
                            y901989 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                            if (SCM_I_INUMP(x901988) && SCM_I_INUMP(y901989)) {
                                 {
                                    scm_t_int64 xx902873;
                                    scm_t_int64 yy902874;
                                    xx902873 = SCM_I_INUM(x901988);
                                    yy902874 = SCM_I_INUM(y901989);
                                    
                                    
                                    
                                    if (xx902873 < 268435456
                                          && xx902873 > -268435456
                                               && yy902874 <= 32
                                                    && yy902874 >= 0) {
                                         {
                                            scm_t_int64 n902876;
                                            n902876 = xx902873 >> yy902874;
                                            
                                            
                                            
                                            AREF(sp, 1)
                                              = SCM_I_MAKINUM(n902876);
                                        }
                                    } else {
                                        AREF(sp, 1)
                                          = scm_ash(x901988,
                                                    scm_difference(y901989,
                                                                   scm_num721462));
                                    }
                                }
                            } else {
                                AREF(sp, 1)
                                  = scm_ash(x901988,
                                            scm_difference(y901989,
                                                           scm_num721462));
                            }
                            
                            inst__pt901760 = inst__pt901760 + 1;
                            
                             {
                                void * jmp902875;
                                jmp902875
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902875;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          shiftLR:
                            
                            printf("%s : %d\n", "shiftLR", 119);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901990;
                            SCM y901991;
                            x901990 = *inst__pt901760;
                            y901991 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                            if (SCM_I_INUMP(y901991) && SCM_I_INUMP(x901990)) {
                                 {
                                    scm_t_int64 xx902883;
                                    scm_t_int64 yy902884;
                                    xx902883 = SCM_I_INUM(y901991);
                                    yy902884 = SCM_I_INUM(x901990);
                                    
                                    
                                    
                                    if (xx902883 < 268435456
                                          && xx902883 > -268435456
                                               && yy902884 <= 32
                                                    && yy902884 >= 0) {
                                         {
                                            scm_t_int64 n902886;
                                            n902886 = xx902883 << yy902884;
                                            
                                            
                                            
                                            AREF(sp, 1)
                                              = SCM_I_MAKINUM(n902886);
                                        }
                                    } else {
                                        AREF(sp, 1)
                                          = scm_ash(y901991, x901990);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_ash(y901991, x901990);
                            }
                            
                            inst__pt901760 = inst__pt901760 + 1;
                            
                             {
                                void * jmp902885;
                                jmp902885
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902885;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          shiftRR:
                            
                            printf("%s : %d\n", "shiftRR", 122);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901992;
                            SCM y901993;
                            x901992 = *inst__pt901760;
                            y901993 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                            if (SCM_I_INUMP(y901993) && SCM_I_INUMP(x901992)) {
                                 {
                                    scm_t_int64 xx902893;
                                    scm_t_int64 yy902894;
                                    xx902893 = SCM_I_INUM(y901993);
                                    yy902894 = SCM_I_INUM(x901992);
                                    
                                    
                                    
                                    if (xx902893 < 268435456
                                          && xx902893 > -268435456
                                               && yy902894 <= 32
                                                    && yy902894 >= 0) {
                                         {
                                            scm_t_int64 n902896;
                                            n902896 = xx902893 >> yy902894;
                                            
                                            
                                            
                                            AREF(sp, 1)
                                              = SCM_I_MAKINUM(n902896);
                                        }
                                    } else {
                                        AREF(sp, 1)
                                          = scm_ash(y901993,
                                                    scm_difference(x901992,
                                                                   scm_num721462));
                                    }
                                }
                            } else {
                                AREF(sp, 1)
                                  = scm_ash(y901993,
                                            scm_difference(x901992,
                                                           scm_num721462));
                            }
                            
                            inst__pt901760 = inst__pt901760 + 1;
                            
                             {
                                void * jmp902895;
                                jmp902895
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902895;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          modL:
                            
                            printf("%s : %d\n", "modL", 124);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901994;
                            SCM y901995;
                            x901994 = *inst__pt901760;
                            y901995 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                            if (SCM_I_INUMP(x901994) && SCM_I_INUMP(y901995)) {
                                 {
                                    scm_t_int64 n902903;
                                    n902903
                                      = SCM_I_INUM(x901994)
                                          % SCM_I_INUM(y901995);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n902903)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n902903);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_modulo(x901994, y901995);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_modulo(x901994, y901995);
                            }
                            
                            inst__pt901760 = inst__pt901760 + 1;
                            
                             {
                                void * jmp902904;
                                jmp902904
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902904;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          modR:
                            
                            printf("%s : %d\n", "modR", 125);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901996;
                            SCM y901997;
                            x901996 = *inst__pt901760;
                            y901997 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                            if (SCM_I_INUMP(y901997) && SCM_I_INUMP(x901996)) {
                                 {
                                    scm_t_int64 n902909;
                                    n902909
                                      = SCM_I_INUM(y901997)
                                          % SCM_I_INUM(x901996);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n902909)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n902909);
                                    } else {
                                        AREF(sp, 1)
                                          = scm_modulo(y901997, x901996);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_modulo(y901997, x901996);
                            }
                            
                            inst__pt901760 = inst__pt901760 + 1;
                            
                             {
                                void * jmp902910;
                                jmp902910
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902910;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          uminus:
                            
                            printf("%s : %d\n", "uminus", 141);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901998;
                            x901998 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                            AREF(sp, 1)
                              = scm_difference(scm_num721462, x901998);
                            
                             {
                                void * jmp902915;
                                jmp902915
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902915;
                            }
                        }
                    }
                    
                     {
                        
                         {
                            
                            
                          lognot:
                            
                            printf("%s : %d\n", "lognot", 143);
                        }
                        
                        printf("sp - fp . %d\n", (fp - sp));
                        
                         {
                            SCM x901999;
                            x901999 = gp_gp_lookup(AREF(sp, 1), s901763);
                            
                            
                            
                            if (SCM_I_INUMP(x901999)) {
                                 {
                                    scm_t_int64 n902916;
                                    n902916 = ~SCM_I_INUM(x901999);
                                    
                                    
                                    
                                    if (SCM_FIXABLE(n902916)) {
                                        AREF(sp, 1) = SCM_I_MAKINUM(n902916);
                                    } else {
                                        AREF(sp, 1) = scm_lognot(x901999);
                                    }
                                }
                            } else {
                                AREF(sp, 1) = scm_lognot(x901999);
                            }
                            
                             {
                                void * jmp902917;
                                jmp902917
                                  = AREF(_soperations_s,
                                         scm2int(*(inst__pt901760)));
                                
                                
                                
                                ++inst__pt901760;
                                
                                goto *jmp902917;
                            }
                        }
                    }
                    
                     {
                        
                        
                      gt:
                        
                        printf("%s : %d\n", "gt", 76);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x902000;
                        SCM y902001;
                        x902000 = AREF(sp, 2);
                        y902001 = AREF(sp, 1);
                        
                        
                        
                         {
                            
                            AREF(sp, 1) = SCM_BOOL_F;
                            
                            AREF(sp, 2) = SCM_BOOL_F;
                            
                            sp = sp + 2;
                        }
                        
                        scm_simple_format(SCM_BOOL_T,
                                          scm_string836001,
                                          scm_list_3(x902000,
                                                     scm_gr_p836383,
                                                     y902001));
                        
                        if (SCM_I_INUMP(x902000) && SCM_I_INUMP(y902001)) {
                            if (!(x902000 > y902001)) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp902919;
                                            jmp902919
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp902919;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        } else {
                            if (scm_is_false(scm_gr_p(x902000, y902001))) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp902920;
                                            jmp902920
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp902920;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp902002;
                        jmp902002
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp902002;
                    }
                    
                     {
                        
                        
                      gtL:
                        
                        printf("%s : %d\n", "gtL", 77);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x902003;
                        SCM y902004;
                        x902003 = *inst__pt901760;
                        y902004 = AREF(sp, 1);
                        
                        
                        
                        inst__pt901760 = inst__pt901760 + 1;
                        
                         {
                            
                            AREF(sp, 1) = SCM_BOOL_F;
                            
                            sp = sp + 1;
                        }
                        
                        scm_simple_format(SCM_BOOL_T,
                                          scm_string836001,
                                          scm_list_3(x902003,
                                                     scm_gr_p836383,
                                                     y902004));
                        
                        if (SCM_I_INUMP(x902003) && SCM_I_INUMP(y902004)) {
                            if (!(x902003 > y902004)) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp902933;
                                            jmp902933
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp902933;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        } else {
                            if (scm_is_false(scm_gr_p(x902003, y902004))) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp902934;
                                            jmp902934
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp902934;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp902005;
                        jmp902005
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp902005;
                    }
                    
                     {
                        
                        
                      ltL:
                        
                        printf("%s : %d\n", "ltL", 80);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x902006;
                        SCM y902007;
                        x902006 = *inst__pt901760;
                        y902007 = AREF(sp, 1);
                        
                        
                        
                        inst__pt901760 = inst__pt901760 + 1;
                        
                         {
                            
                            AREF(sp, 1) = SCM_BOOL_F;
                            
                            sp = sp + 1;
                        }
                        
                        scm_simple_format(SCM_BOOL_T,
                                          scm_string836001,
                                          scm_list_3(x902006,
                                                     scm_less_p840162,
                                                     y902007));
                        
                        if (SCM_I_INUMP(x902006) && SCM_I_INUMP(y902007)) {
                            if (!(x902006 < y902007)) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp902949;
                                            jmp902949
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp902949;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        } else {
                            if (scm_is_false(scm_less_p(x902006, y902007))) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp902950;
                                            jmp902950
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp902950;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp902008;
                        jmp902008
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp902008;
                    }
                    
                     {
                        
                        
                      ls:
                        
                        printf("%s : %d\n", "ls", 79);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x902009;
                        SCM y902010;
                        x902009 = AREF(sp, 2);
                        y902010 = AREF(sp, 1);
                        
                        
                        
                         {
                            
                            AREF(sp, 1) = SCM_BOOL_F;
                            
                            AREF(sp, 2) = SCM_BOOL_F;
                            
                            sp = sp + 2;
                        }
                        
                        scm_simple_format(SCM_BOOL_T,
                                          scm_string836001,
                                          scm_list_3(x902009,
                                                     scm_less_p840162,
                                                     y902010));
                        
                        if (SCM_I_INUMP(x902009) && SCM_I_INUMP(y902010)) {
                            if (!(x902009 < y902010)) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp902965;
                                            jmp902965
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp902965;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        } else {
                            if (scm_is_false(scm_less_p(x902009, y902010))) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp902966;
                                            jmp902966
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp902966;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp902011;
                        jmp902011
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp902011;
                    }
                    
                     {
                        
                        
                      ge:
                        
                        printf("%s : %d\n", "ge", 82);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x902012;
                        SCM y902013;
                        x902012 = AREF(sp, 2);
                        y902013 = AREF(sp, 1);
                        
                        
                        
                         {
                            
                            AREF(sp, 1) = SCM_BOOL_F;
                            
                            AREF(sp, 2) = SCM_BOOL_F;
                            
                            sp = sp + 2;
                        }
                        
                        scm_simple_format(SCM_BOOL_T,
                                          scm_string836001,
                                          scm_list_3(x902012,
                                                     scm_geq_p844327,
                                                     y902013));
                        
                        if (SCM_I_INUMP(x902012) && SCM_I_INUMP(y902013)) {
                            if (!(x902012 >= y902013)) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp902979;
                                            jmp902979
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp902979;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        } else {
                            if (scm_is_false(scm_geq_p(x902012, y902013))) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp902980;
                                            jmp902980
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp902980;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp902014;
                        jmp902014
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp902014;
                    }
                    
                     {
                        
                        
                      geL:
                        
                        printf("%s : %d\n", "geL", 83);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x902015;
                        SCM y902016;
                        x902015 = *inst__pt901760;
                        y902016 = AREF(sp, 1);
                        
                        
                        
                        inst__pt901760 = inst__pt901760 + 1;
                        
                         {
                            
                            AREF(sp, 1) = SCM_BOOL_F;
                            
                            sp = sp + 1;
                        }
                        
                        scm_simple_format(SCM_BOOL_T,
                                          scm_string836001,
                                          scm_list_3(x902015,
                                                     scm_geq_p844327,
                                                     y902016));
                        
                        if (SCM_I_INUMP(x902015) && SCM_I_INUMP(y902016)) {
                            if (!(x902015 >= y902016)) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp902993;
                                            jmp902993
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp902993;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        } else {
                            if (scm_is_false(scm_geq_p(x902015, y902016))) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp902994;
                                            jmp902994
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp902994;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp902017;
                        jmp902017
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp902017;
                    }
                    
                     {
                        
                        
                      le:
                        
                        printf("%s : %d\n", "le", 85);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x902018;
                        SCM y902019;
                        x902018 = AREF(sp, 2);
                        y902019 = AREF(sp, 1);
                        
                        
                        
                         {
                            
                            AREF(sp, 1) = SCM_BOOL_F;
                            
                            AREF(sp, 2) = SCM_BOOL_F;
                            
                            sp = sp + 2;
                        }
                        
                        scm_simple_format(SCM_BOOL_T,
                                          scm_string836001,
                                          scm_list_3(x902018,
                                                     scm_leq_p848299,
                                                     y902019));
                        
                        if (SCM_I_INUMP(x902018) && SCM_I_INUMP(y902019)) {
                            if (!(x902018 >= y902019)) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp903009;
                                            jmp903009
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp903009;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        } else {
                            if (scm_is_false(scm_leq_p(x902018, y902019))) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp903010;
                                            jmp903010
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp903010;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp902020;
                        jmp902020
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp902020;
                    }
                    
                     {
                        
                        
                      leL:
                        
                        printf("%s : %d\n", "leL", 86);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x902021;
                        SCM y902022;
                        x902021 = *inst__pt901760;
                        y902022 = AREF(sp, 1);
                        
                        
                        
                        inst__pt901760 = inst__pt901760 + 1;
                        
                         {
                            
                            AREF(sp, 1) = SCM_BOOL_F;
                            
                            sp = sp + 1;
                        }
                        
                        scm_simple_format(SCM_BOOL_T,
                                          scm_string836001,
                                          scm_list_3(x902021,
                                                     scm_leq_p848299,
                                                     y902022));
                        
                        if (SCM_I_INUMP(x902021) && SCM_I_INUMP(y902022)) {
                            if (!(x902021 >= y902022)) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp903023;
                                            jmp903023
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp903023;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        } else {
                            if (scm_is_false(scm_leq_p(x902021, y902022))) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp903024;
                                            jmp903024
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp903024;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp902023;
                        jmp902023
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp902023;
                    }
                    
                     {
                        
                        
                      eq:
                        
                        printf("%s : %d\n", "eq", 88);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM x902024;
                        SCM y902025;
                        x902024 = AREF(sp, 2);
                        y902025 = AREF(sp, 1);
                        
                        
                        
                         {
                            
                            AREF(sp, 1) = SCM_BOOL_F;
                            
                            AREF(sp, 2) = SCM_BOOL_F;
                            
                            sp = sp + 2;
                        }
                        
                        scm_simple_format(SCM_BOOL_T,
                                          scm_string836001,
                                          scm_list_3(x902024,
                                                     scm_equal_p852271,
                                                     y902025));
                        
                        if (SCM_I_INUMP(x902024) && SCM_I_INUMP(y902025)) {
                            if (!(x902024 == y902025)) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp903039;
                                            jmp903039
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp903039;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        } else {
                            if (scm_is_false(scm_equal_p(x902024, y902025))) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp903040;
                                            jmp903040
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp903040;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        }
                    }
                    
                     {
                        void * jmp902026;
                        jmp902026
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp902026;
                    }
                    
                     {
                        
                        
                      neq:
                        
                        printf("%s : %d\n", "neq", 89);
                    }
                    
                    printf("sp - fp . %d\n", (fp - sp));
                    
                     {
                        SCM y902028;
                        SCM x902029;
                         {
                            SCM nn902027;
                            nn902027 = *inst__pt901760;
                            
                            
                            
                            ++inst__pt901760;
                            
                            if (scm_is_false(nn902027)) {
                                 {
                                    SCM xx903055;
                                    xx903055 = AREF(sp, 1);
                                    
                                    
                                    
                                     {
                                        
                                        AREF(sp, 1) = SCM_BOOL_F;
                                        
                                        sp = sp + 1;
                                    }
                                    
                                    y902028 = xx903055;
                                }
                            } else {
                                y902028 = nn902027;
                            }
                        }
                        x902029 = AREF(sp, 1);
                        
                        
                        
                        scm_simple_format(SCM_BOOL_T,
                                          scm_string836001,
                                          scm_list_3(x902029,
                                                     scm_equal_p852271,
                                                     y902028));
                        
                        if (SCM_I_INUMP(x902029) && SCM_I_INUMP(y902028)) {
                            if (x902029 == y902028) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp903053;
                                            jmp903053
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp903053;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        } else {
                            if (scm_is_true(scm_equal_p(x902029, y902028))) {
                                if (SCM_I_INUMP(p901764)) {
                                     {
                                        
                                        inst__pt901760
                                          = instructions901752
                                              + scm2int(p901764);
                                        
                                         {
                                            void * jmp903054;
                                            jmp903054
                                              = AREF(_soperations_s,
                                                     scm2int(*(inst__pt901760)));
                                            
                                            
                                            
                                            ++inst__pt901760;
                                            
                                            goto *jmp903054;
                                        }
                                    }
                                } else {
                                     {
                                        
                                        AREF(fp, 0) = p901764;
                                        
                                        sp = fp + -1;
                                        
                                        goto ret;
                                    }
                                }
                            }
                        }
                        
                         {
                            
                            AREF(sp, 1) = SCM_BOOL_F;
                            
                            sp = sp + 1;
                        }
                    }
                    
                     {
                        void * jmp902030;
                        jmp902030
                          = AREF(_soperations_s, scm2int(*(inst__pt901760)));
                        
                        
                        
                        ++inst__pt901760;
                        
                        goto *jmp902030;
                    }
                    
                    
                  ret:
                    
                    ret901768 = sp;
                }
            }
        }
        return ret901768;
    }
}

void init_prolog_vm ();

void init_prolog_vm () {
     {
        
         {
            scm_equal_p852271 = scm_from_locale_symbol("scm_equal_p");
            scm_leq_p848299 = scm_from_locale_symbol("scm_leq_p");
            scm_geq_p844327 = scm_from_locale_symbol("scm_geq_p");
            scm_less_p840162 = scm_from_locale_symbol("scm_less_p");
            scm_gr_p836383 = scm_from_locale_symbol("scm_gr_p");
            brace161892 = scm_from_locale_keyword("brace");
            scm_num721462 = scm_c_locale_stringn_to_number("0", 1, 10);
            scm_string836001 = scm_from_locale_string("~a ~a ~a~%\n");
            scm_string808963
              = scm_from_locale_string("push-scm id: ~a, value: ~a~%");
            scm_string804418 = scm_from_locale_string("push ~a ~a ~a~%");
            scm_string799446 = scm_from_locale_string("push ~a ~a~%");
            scm_string795488 = scm_from_locale_string("push ~a~%");
            scm_string780398 = scm_from_locale_string("~a = ~a~%");
            scm_string761574
              = scm_from_locale_string("RESTORE-P x : ~a tag : ~a~%");
            scm_string742443 = scm_from_locale_string("STACK: ~a~%");
            scm_string740972
              = scm_from_locale_string("MK-P p = ~a, pp = ~a~%");
            
        }
        
        vm__raw(0, 0, 0, 0, 0, 0, 1);
    }
}

