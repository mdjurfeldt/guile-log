(use-modules (logic guile-log))
(use-modules (logic guile-log umatch))

(<define> (f)
  (<values> (i x) (<abort> 'tag (<lambda> (i x) (<cc> i x))))  
  (<or-i>
   (<or> (<=> x i)      (<=> x ,(+ i i)))
   (<or> (<=> x ,(- i)) (<=> x ,(- (+ i i))))))


(<define> (test x y)
  (<prompt> 'tag #f f
    (<lambda> (tag next kk)
        (<let> ((k (kk)))
	   (<zip> (x (k 1  x))
		  (y (k 10 y)))))))

(pk
(<run> 10 (x y) (test x y)))
	 
(<define> (f2 n)
  (<abort> 'tag 
     (<lambda> (f y i)
	(<=> y ,(+ n i))
	(if (< n 14)
	    (<or>
	     (if f (f #f y (+ i 1)))
	     (f2 (+ n 1)))))))

(<define> (gen f . n)
   (<prompt> 'tag #f (<lambda> () (<apply> f n))  
      (letrec ((f (<lambda> (tag next kk)		      
		      (<cc> (kk f)))))
	f)))


;; This does not wok due to a bug in guile's peval   
(pk
(<run> 10 (x y z)
  (<values> (fx) (gen f2 10))
  (<values> (fy) (fx fx x 1))
  (<values> (fz) (fy fx y 1))
  (<values> (fz) (fz fx z 1))))
		 
  

       