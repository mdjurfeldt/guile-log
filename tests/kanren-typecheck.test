(define-module (kanren-typecheck)
  #:use-module (logic guile-log examples kanren type-inference)
  #:use-module (logic guile-log kanren)
  #:use-module (logic guile-log umatch)
  #:use-module (test-suite lib))        


(define (time x) x)

#;
(define-syntax test-check
  (syntax-rules ()
    ((_ title tested-expression expected-result)
     (begin
       (format #t "Testing ~a~%" title)
       (let* ((expected expected-result)
              (produced tested-expression))
         (or (equal? expected produced)
             (error
              (format #f
                      "(Failed: ~a~%Expected: ~a~%Computed: ~a~%"
                      'tested-expression expected produced))))))))

(define-syntax test-check
  (syntax-rules ()
    ((_ title x y)
     (begin
       (gp-clear *current-stack*)
       (with-test-prefix "start"
         (pass-if (format #f "~a" 'x)
                  (equal? x y)))))))

(test-check 'test-!-1
  (and
   (equal?
    (solution (?) (!- '() '(intc 17) int))
    'v.0)
   
   (equal?
    (solution (?) (!- '() '(intc 17) ?))
    'int))
  #t)


(test-check 'arithmetic-primitives
  (solution (?) (!- '() '(zero? (intc 24)) ?))
  'bool)

(test-check 'test-!-sub1
  (solution (?) (!- '() '(zero? (sub1 (intc 24))) ?))
  'bool)

(test-check 'test-!-+
  (solution (?)
      (!- '() '(zero? (sub1 (+ (intc 18) (+ (intc 24) (intc 50))))) ?))
  'bool)

(test-check 'test-!-2
  (and
    (equal?
      (solution (?) (!- '() '(zero? (intc 24)) ?))
      'bool)
    (equal?
      (solution (?) (!- '() '(zero? (+ (intc 24) (intc 50))) ?))
      'bool)
    (equal?
      (solution (?)
	(!- '() '(zero? (sub1 (+ (intc 18) (+ (intc 24) (intc 50))))) ?))
      'bool))
  #t)

(test-check 'test-!-3
  (solution (?) (!- '() '(if (zero? (intc 24)) (intc 3) (intc 4)) ?))
  'int)

(test-check 'if-expressions
  (solution (?)
    (!- '() '(if (zero? (intc 24)) (zero? (intc 3)) (zero? (intc 4))) ?))
  'bool)

(test-check 'variables
  (and
    (equal?
      (solution (?)
        (env '((b non-generic int) (a non-generic bool)) 'a ?))
      'bool)
    (equal?
      (solution (?)
	(!- '((a non-generic int)) '(zero? (var a)) ?))
      'bool)
    (equal?
      (solution (?)
	(!- '((b non-generic bool) (a non-generic int))
            '(zero? (var a))
            ?))
      'bool))
  #t)

(test-check 'variables-4a
  (solution (?)
    (!- '((b non-generic bool) (a non-generic int))
        '(lambda (x) (+ (var x) (intc 5)))
        ?))
  '(--> int int))

(test-check 'variables-4b
  (solution (?)
    (!- '((b non-generic bool) (a non-generic int))
        '(lambda (x) (+ (var x) (var a)))
        ?))
  '(--> int int))

(test-check 'variables-4c
  (solution (?)
    (!- '() '(lambda (a) (lambda (x) (+ (var x) (var a)))) ?))
  '(--> int (--> int int)))

(test-check 'everything-but-polymorphic-let
  (solution (?)
    (!- '() (parse
              '(lambda (f)
                 (lambda (x)
                   ((f x) x))))
        ?))
  '(-->
    (--> v.0 (--> v.0 v.1))
    (--> v.0 v.1)))

(test-check 'everything-but-polymorphic-let
  (solution (?)
    (!- '()
        (parse
          '((fix (lambda (sum)
                   (lambda (n)
                     (if (zero? n)
                         0
                         (+ n (sum (sub1 n)))))))
            10))
        ?))
  'int)

(test-check 'everything-but-polymorphic-let
  (solution (?)
    (!- '()
        (parse
          '((fix (lambda (sum)
                   (lambda (n)
                     (+ n (sum (sub1 n))))))
            10))
        ?))
  'int)

(test-check 'everything-but-polymorphic-let
  (solution (?)
    (!- '()
        (parse '((lambda (f)
                   (if (f (zero? 5))
                       (+ (f 4) 8)
                       (+ (f 3) 7)))
                 (lambda (x) x)))
        ?))
  #f)

(test-check 'polymorphic-let
  (solution (?)
    (!- '()
        (parse
          '(let ((f (lambda (x) x)))
             (if (f (zero? 5))
                 (+ (f 4) 8)
                 (+ (f 3) 7))))
        ?))
  'int)

(test-check 'with-robust-syntax
  (solution (?)
    (!- '()
        '(app
           (fix
             (lambda (sum)
               (lambda (n)
                 (if (if (zero? (var n)) (boolc #t) (boolc #f))
                     (intc 0)
                     (+ (var n) (app (var sum) (sub1 (var n))))))))
           (intc 10))
        ?))
  'int)

(test-check 'with-robust-syntax-but-long-jumps/poly-let
  (solution (?)
    (!- '()
        '(let ((f (lambda (x) (var x))))
           (if (app (var f) (zero? (intc 5)))
               (+ (app (var f) (intc 4)) (intc 8))
               (+ (app (var f) (intc 3)) (intc 7))))
        ?))
  'int)

(test-check 'type-habitation-1
  (solution (g ?)
    (!- g ? '(--> int int)))
  '(((v.0 non-generic (--> int int)) . v.1)
    (var v.0)))

(test-check 'type-habitation-2
  (solution (g h r q z y t)
    (!- g `(,h ,r (,q ,z ,y)) t))
  '(((v.0 non-generic int) . v.1)
    +
    (var v.0)
    +
    (var v.0)
    (var v.0)
    int))

(test-check 'type-habitation-3
  (and
    (equal?
      (solution (la f b)
	(!- '() `(,la (,f) ,b) '(--> int int)))
      '(lambda v.0 (var v.0)))
    (equal?
     (solution (h r q z y t u v)
	(!- '() `(,h ,r (,q ,z ,y)) `(,t ,u ,v)))
      '(lambda
        (v.0)
        +
        (var v.0)
        (var v.0)
        -->
        int
        int)))
  #t)

(test-check 'test-instantiated-1
  (and
    (equal?
      (solution (x) (++ x 16.0 8))
      '-8.0)
    (equal?
      (solution (x) (++ 10 16.0 x))
      '26.0)
    (equal?
      (solution (x) (-- 10 x 3))
      '13))
  #t)

(test-check 'test-instantiated-2
  (and
    (equal?
      (solution (x) (name 'sleep x))
      '(115 108 101 101 112))
    (equal?
      (solution (x) (name x '(115 108 101 101 112)))
      'sleep))
  #t)

