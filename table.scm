(use-modules (logic guile-log iso-prolog))
(use-modules (logic guile-log memoize))
(use-modules (logic guile-log guile-prolog ops))


(compile-prolog-string "

-table_rec.
fa(A) :- fa(A).

-table_rec.
fb(A) :- \\+fb(A).

-table_rec.
fc(A) :- AA is -A, \\+fc(AA).

-table_rec.
fd([A|B]) :- (A=1;A=0),,fd(B).


-table_rec.
fe3(a).
fe3([A,B]) :-
   fe3(A),,
   fe3(B).

ff3([U,V]) :- fe3(U),!,fe3(V).

-table_i.
fe2(a).
fe2([A,B]) :-
   fe2(A),,
   fe2(B).

ff2([U,V]) :- fe2(U),!,fe2(V).

-table.
fe(a).
fe([A,B]) :-
   fe(A),
   fe(B).

ff([U,V]) :- fe(U),!,fe(V).

-table_simple.
fe0(a).
fe0([A,B]) :-
   fe0(A),
   fe0(B).

ff0([U,V]) :- fe0(U),!,fe0(V).


minlen(X,Y,Z) :-
   length(X,NX),
   length(Y,NY),
   (
      NX < NY ->
         Z=X;
      Z=Y
   ).

-table(+,first).
fib(0,1) :- !.
fib(1,1) :- !.
fib(N,F) :-
  N > 1,
  N1 is N - 1,
  N2 is N - 2,
  fib(N1,F1),
  fib(N2,F2),
  F is F1 + F2.


-table(ignore,+,+,first).

f2(_,a,b,[a,b]).
f2(_,b,a,[b,a]).
f2(_,b,c,[b,c]).
f2(_,c,b,[c,b]).
f2(_,b,d,[b,d]).
f2(_,d,b,[d,b]).
f2(_,d,f,[d,f]).
f2(_,f,d,[f,d]).
f2(_,c,e,[c,e]).
f2(_,e,f,[e,f]).
f2(N,X,Y,P) :-
    write(f(N,X,Y)),nl,
    NN is N + 1,
    f2(NN,X,Z,P1),
    f2(NN,Z,Y,P2),   
    append(P1,P2,P).


-table(+,+,lattice(minlen)).

f(a,b,[a,b]).
f(b,a,[b,a]).
f(b,c,[b,c]).
f(c,b,[c,b]).
f(b,d,[b,d]).
f(d,b,[d,b]).
f(d,f,[d,f]).
f(f,d,[f,d]).
f(c,e,[c,e]).
f(e,f,[e,f]).
f(X,Y,P) :-
   f(X,Z,P1),
   f(Z,Y,P2),   
   append(P1,P2,P).
")
